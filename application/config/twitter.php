<?php
/**
 * twconnect library configuration
 */

$config = array(
  'consumer_key'    => '',
  'consumer_secret' => '',
  'oauth_callback'  => '/account/twitter_login' // Default callback application path
);

?>