<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
       // $this->load->library(array('twconnect'));
        require_once APPPATH . 'third_party/googleApi/Google_Client.php';
        require_once APPPATH . 'third_party/googleApi/contrib/Google_Oauth2Service.php';
        require_once APPPATH . 'third_party/facebooksdk/vendor/autoload.php';
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->load->model('Temp_order_model');
        $this->load->model('Order_model');
        $this->load->model('User_address_model');
        $this->load->model('Product_model');
        $this->load->model('Offer_model');
        $this->load->model('Offer_group_model');
        $this->load->model('Customer_group_member_model');
        $this->load->model('Offer_user_notification_model');
    }

    public function index()
    {
        
        checkFrontendSession();
        $this->data['user'] = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $this->UserID)[0];
        $this->data['pending_orders'] = $this->Order_model->getOrders("orders.Status = 1 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');

        $this->data['packed_orders'] = $this->Order_model->getOrders("orders.Status = 2 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');
        $this->data['dispatch_orders'] = $this->Order_model->getOrders("orders.Status = 3 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');
        $this->data['deliver_orders'] = $this->Order_model->getOrders("orders.Status = 4 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');
        $this->data['cancel_orders'] = $this->Order_model->getOrders("orders.Status = 5 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');
        $this->data['addresses'] = $this->User_address_model->getAddresses("user_address.UserID = " . $this->UserID);
        //print_rm($this->data['addresses']);       
                $this->data['view'] = 'frontend/page-account';
                $this->load->view('frontend/layouts/default', $this->data);
        
      
    }

    public function login(){

        $this->data['view'] = 'frontend/page-login';
        $this->load->view('frontend/layouts/default', $this->data);

    }
    public function register(){

        $this->data['view'] = 'frontend/page-register';
        $this->load->view('frontend/layouts/default', $this->data);

    }
    
    public function redirect() {

        if($this->session->userdata('login') == true){
            redirect('welcome/profile');
        }
        
        $ok = $this->twconnect->twredirect('welcome/callback');

        if (!$ok) {
            echo 'Could not connect to Twitter. Refresh the page or try again later.';
        }
        
    }


    public function twitter_login() {
        
        if($this->session->userdata('user')){
            redirect('account');
        }
        
        $ok = $this->twconnect->twprocess_callback();
        
        if ( $ok ) { redirect('account/twitter_success'); }
            else redirect (base_url());
            
    }


    public function twitter_success() {

        if($this->session->userdata('login') == true){
            redirect('welcome/profile');
        }
        
        $this->twconnect->twaccount_verify_credentials();

        
        $user_profile = $this->twconnect->tw_user_info;
        
        $this->session->set_userdata('login',true);
        

        $arr = array(
            'id' => $user_profile->id,
            'name' => $user_profile->name,
            'screen_name' => $user_profile->screen_name,
            'location' => $user_profile->location,
            'description' => $user_profile->description,
            'profile_image_url' => $user_profile->profile_image_url,
        );
        
        $this->session->set_userdata('user_profile',$arr);
        
        redirect('account');
        
    }



    public function failure() {

        if($this->session->userdata('login') == true){
            redirect('account');
        }
        
        echo '<p>Twitter connect failed</p>';
        echo '<p><a href="' . base_url() . '">Try again!</a></p>';
    }


   public function facebook_login() 
    {
        /*if($this->session->userdata('user_loggedin')){

          redirect('/home/dashboard');

         */

        if (!session_id()) {
            session_start();
        }

        $fb = new Facebook\Facebook([
            'app_id' => '259044818421887', // Replace {app-id} with your app id
            'app_secret' => '2165a69c045314827929712936163b51',
            'default_graph_version' => 'v2.2',
        ]);




        if (isset($_GET['code'])){ 

            // Get user facebook profile details

            try {

                $helper = $fb->getRedirectLoginHelper();
               if (isset($_GET['state'])) {
                    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
                }
                $accessToken = $helper->getAccessToken();



                $object = $fb->get('/me?fields=id,first_name,last_name,email,gender,locale,picture', $accessToken);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            // Preparing data for database insertion
            $body = (string) $object->getBody();
            $userProfile = json_decode($body);

            // if no email then check socialID
            if(isset($userProfile->email)) {
                $user = $this->User_model->getUserDataByEmail($userProfile->email, $this->language);
            } else {
                $user = $this->User_model->getUserDataBySocialID($userProfile->id, $this->language);
                $userProfile->email = '';
            }



            
           if (!empty($user)) {
                $this->session->set_userdata('user', $user);

                $user = (array) $user;

                if($user['RoleID'] != '1')
                {
                    if($user['IsActive'] == 0){
                        $this->session->set_flashdata('message', 'Your Account has been blocked by Administrator.');
                        redirect('account/login');
                    }
                }
                
                

               if (get_cookie('temp_order_key')) {
                   // echo 'here';exit;
                   $temp_user_id = get_cookie('temp_order_key');
                   $update = array();
                   $update['UserID'] = $this->session->userdata['user']['UserID'];
                   $update_by['UserID'] = $temp_user_id;
                   $this->Temp_order_model->update($update, $update_by);
                   delete_cookie('temp_order_key');
               }
               
                //$this->updateUserLoginStatus();
                redirect('account');
            }else{
                $parent                             = 'User_model';
                $child                              = 'User_text_model';
                $save_parent_data                   = array();
                $save_child_data                    = array();
               
                $getSortValue = $this->$parent->getLastRow('UserID');
                   
                $sort = 0;
                if(!empty($getSortValue))
                {
                   $sort = $getSortValue['SortOrder'] + 1;
                }

                
                $save_parent_data['FromSocial']     = 'facebook';
                $save_parent_data['SocialID']       = $userProfile->id;
                $save_parent_data['SortOrder']      = $sort;
                $save_parent_data['IsActive']       = 1;
                $save_parent_data['RoleID']         = 5;
                
                $save_parent_data['Email']          = $userProfile->email;
                $save_parent_data['Gender']          = (isset($userProfile->gender) ? $userProfile->gender : 'Male');
                
                $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
                $insert_id                          = $this->$parent->save($save_parent_data);

                if($insert_id > 0)
                {
                    

                    $default_lang = getDefaultLanguage();
                    $fb_first_name = (isset($userProfile->first_name) ? $userProfile->first_name : 'User');
                    $fb_last_name = (isset($userProfile->last_name) ? $userProfile->last_name : 'Name');
                    $save_child_data['FullName']                        = $fb_first_name.' '.$fb_last_name;
                    $save_child_data['UserID']                          = $insert_id;
                    $save_child_data['SystemLanguageID']                = 1;
                    $this->$child->save($save_child_data);

                                       
                    $user = $this->User_model->getUserDataByEmail($userProfile->email, $this->language);
                    $this->session->set_userdata('user', $user);
                    redirect('account');

                }else
                {
                   $this->session->set_flashdata('message', 'Something went wrong.');
                   redirect('account/login');
                }

            } 
            
            // end
            // Get logout URL
            //  $data['logoutUrl'] = $this->facebook->logout_url();
        } else {
            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(base_url().'account/facebook_login', $permissions);
            $loginUrl = htmlspecialchars($loginUrl);
            $loginUrl = str_replace("amp;", "", $loginUrl);
            header("Location: $loginUrl");
            exit;
        }
    }

    public function google_login() 
    {
        // if ($this->session->userdata('user_loggedin')) {
        //     redirect('/');
        //     print_r($$this->session->userdata('user_loggedin')); exit;
        if (!session_id()) {
            session_start();
        } else {
            $clientId = '412994844618-fvjajuvp6bfq8fi0dg7b573c8ajqimm9.apps.googleusercontent.com'; //Google client ID
            $clientSecret = 'f05-xVrxSR86MR0PdZcaji_V'; //Google client secret
            $redirectURL = base_url() . 'home/google_login';

            //Call Google API
            $gClient = new Google_Client();
            $gClient->setApplicationName('Login');
            $gClient->setClientId($clientId);
            $gClient->setClientSecret($clientSecret);
            $gClient->setRedirectUri($redirectURL);
            $google_oauthV2 = new Google_Oauth2Service($gClient);

            if (isset($_GET['code'])) {
                $gClient->authenticate($_GET['code']);
                $_SESSION['token'] = $gClient->getAccessToken();
                header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));


                if (isset($_SESSION['token'])) {
                    $gClient->setAccessToken($_SESSION['token']);
                }

                if ($gClient->getAccessToken()) {
                    $userProfile = $google_oauthV2->userinfo->get();

                    // my working 
            $user = $this->User_model->getUserDataByEmail($userProfile['email'], $this->language);
            if (!empty($user)) {

                if($user['RoleID'] != '1')
                {
                    if($user['IsActive'] == 0){
                        $this->session->set_flashdata('message', 'Your Account has been blocked by Administrator.');
                        redirect('account/login');
                    }
                    

                }
                
                $this->session->set_userdata('user', $user);

                if (get_cookie('temp_order_key')) {
                    // echo 'here';exit;
                    $temp_user_id = get_cookie('temp_order_key');
                    $update = array();
                    $update['UserID'] = $this->session->userdata['admin']['UserID'];
                    $update_by['UserID'] = $temp_user_id;
                    $this->Temp_order_model->update($update, $update_by);
                    delete_cookie('temp_order_key');
                }

                //$this->updateUserLoginStatus();
                redirect('account');
            }else{
                $parent                             = 'User_model';
                $child                              = 'User_text_model';
                $save_parent_data                   = array();
                $save_child_data                    = array();
               
                $getSortValue = $this->$parent->getLastRow('UserID');
                   
                $sort = 0;
                if(!empty($getSortValue))
                {
                   $sort = $getSortValue['SortOrder'] + 1;
                }

                
                $save_parent_data['FromSocial']     = 'gmail';
                //$save_parent_data['SocialID']       = $userProfile->id;
                $save_parent_data['SortOrder']      = $sort;
                $save_parent_data['IsActive']       = 1;
                $save_parent_data['RoleID']         = 3;
                
                $save_parent_data['Email']          = $userProfile['email'];
                $save_parent_data['Gender']         = (isset($userProfile['gender']) ? $userProfile['gender'] : '') ;
                $save_parent_data['RegistrationFeePaid'] = 'Yes';
                $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
               
                $insert_id                          = $this->$parent->save($save_parent_data);

                if($insert_id > 0)
                {
                    
                
                $this->Product_model->update( 
                        ['NotificationUserID' => $insert_id],
                        ['NotificationUserEmail' => $userProfile['email']]
                    );
                
                $default_lang = getDefaultLanguage();
                    
                    $save_child_data['FullName']  = $userProfile['given_name'].' '.$userProfile['family_name'];
                    $save_child_data['UserID']    = $insert_id;
                    $save_child_data['SystemLanguageID'] = 1;
                   $this->$child->save($save_child_data);

                                       
                    $user = $this->User_model->getUserDataByEmail($userProfile->email, $this->language);
                    $this->session->set_userdata('user', $user);                 
                    redirect('account');

                }else
                {
                   $this->session->set_flashdata('message', 'Something went wrong.');
                   redirect(base_url());
                }

            } 
            
            // end
                }
            } else {
                $url = $gClient->createAuthUrl();
                header("Location: $url");
                exit;
            }
        }
    }

    public function signUp()
    {
        $parent_tbl_data = $this->input->post();
        $this->signupValidation();
        /*if(!$this->session->userdata('OTP')){
            $response['message'] = lang('wrong_otp_inserted');
            $response['error'] = false;
            echo json_encode($response);
            exit;

        }*/
        $original_password = $parent_tbl_data['Password'];
        $parent_tbl_data['Password'] = md5($original_password);
        $parent_tbl_data['RoleID'] = 5;
        $parent_tbl_data['CompanyID'] = $this->companyData->CompanyID;;
        $parent_tbl_data['IsMobileVerified'] = 1;
        $parent_tbl_data['Mobile'] = $parent_tbl_data['Mobile'];
        $child_tbl_data['FullName'] = $parent_tbl_data['FullName'];
        unset($parent_tbl_data['FullName']);
        unset($parent_tbl_data['MobileCode']);
        unset($parent_tbl_data['ConfirmPassword']);
        $insert_id = $this->User_model->save($parent_tbl_data);
        if ($insert_id > 0) {
           // $this->session->unset_userdata('OTP');
            $child_tbl_data['UserID'] = $insert_id;
            $child_tbl_data['SystemLanguageID'] = 1;
            $this->User_text_model->save($child_tbl_data);
            $user = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $insert_id)[0];
            $this->session->set_userdata('user', $user);
           // $this->sendWelcomeEmail($user, $original_password);
            $this->updateOnlineStatus();
            $this->updateCartUserID();
            $this->checkIfUserHasOffer($insert_id);
            $response['message'] = lang('registered_successfully_signup');
            $response['redirect'] = true;
            $response['url'] = 'account/profile';
            echo json_encode($response);
            exit;
        } else {
            $response['message'] = lang('something_went_wrong');
            echo json_encode($response);
            exit;
        }

    }

    public function checkLogin()
    {
        $post_data = $this->input->post();
        //$redirect_url = $post_data['redirect_url'];
        //unset($post_data['redirect_url']);
        $this->loginValidation();
        $user_exist = $this->checkUser($post_data);
        if (!$user_exist) {
            $this->User_model->update(array('LastUnsuccessfulLogin' => date('Y-m-d H:i:s')), array('Email' => $post_data['Email']));
            $response['message'] = lang('email_password_incorrect');
            $response['error'] = true;
            echo json_encode($response);
            exit();
        } else {
            $this->updateOnlineStatus();
            $this->updateCartUserID();
            $this->checkIfUserHasOffer($this->session->userdata['user']->UserID);
            $response['message'] = lang('loggedin_successfully');
            $response['redirect'] = true;
            $response['url'] = 'account';
            echo json_encode($response);
            exit();
        }
    }

    public function profile()
    {
        checkFrontendSession();
        $this->data['user'] = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $this->UserID)[0];
        
        $this->data['view'] = 'frontend/page-account';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function orders()
    {
        checkFrontendSession();
        $this->data['user'] = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $this->UserID)[0];
        $this->data['pending_orders'] = $this->Order_model->getOrders("orders.Status = 1 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');
        $this->data['packed_orders'] = $this->Order_model->getOrders("orders.Status = 2 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');
        $this->data['dispatch_orders'] = $this->Order_model->getOrders("orders.Status = 3 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');
        $this->data['deliver_orders'] = $this->Order_model->getOrders("orders.Status = 4 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');
        $this->data['cancel_orders'] = $this->Order_model->getOrders("orders.Status = 5 AND orders.Hide = 0 AND orders.UserID = " . $this->UserID, false, 0, $this->language, 'DESC');
        $this->data['view'] = 'frontend/page-account';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function addresses()
    {
        checkFrontendSession();
        $data['user'] = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $this->UserID)[0];
        $data['addresses'] = $this->User_address_model->getAddresses("user_address.UserID = " . $this->UserID);
        //$data['primary_addresses'] = $this->User_address_model->getAddresses("user_address.IsDefault = 1 AND user_address.UserID = " . $this->UserID)[0];
        $data['view'] = 'frontend/my-addresses';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function wishlist()
    {
        checkFrontendSession();
        $data['user'] = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $this->UserID)[0];
        $data['wishlist_items'] = $this->Product_model->getWishlistItems($this->UserID, $this->language);
        $data['view'] = 'frontend/my-wishlist';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function verify_account()
    {
        $data = array();
        $code = $this->input->post('verification_code');
        if ($code == '') {
            $data['success'] = 'false';
            $data['error'] = lang('provide_verification_code');
            echo json_encode($data);
            exit();
        } else {
            $checkUser = $this->Model_user->get($code, false, 'verification_code');

            if ($checkUser != true) {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = lang('incorrect_code');
                echo json_encode($data);
                exit();
            } else {
                $data = array();
                $update_data = array();
                $update_data['verification_code'] = '';
                $update_data['is_verified'] = 1;
                $update_data_by = array();
                $update_data_by['user_id'] = $checkUser->user_id;

                $this->Model_user->update($update_data, $update_data_by);


                $data['success'] = lang('account_verify');
                $data['error'] = 'false';
                $data['redirect'] = true;
                $data['url'] = 'page/login';
                echo json_encode($data);
                exit();
            }
        }
    }

    private function loginValidation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error-field">', '</div>');

        $this->form_validation->set_rules('Email', lang('email'), 'required');
        $this->form_validation->set_rules('Password', lang('password'), 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['message'] = validation_errors();
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    private function checkUser($post_data)
    {
        $user = $this->User_model->getWithMultipleFields(array('Email' => $post_data['Email'], 'Password' => md5($post_data['Password']), 'RoleID' => 5));
        if ($user) {
            if ($user->IsActive == 0) {
                $response['message'] = lang('your_account_suspended');
                echo json_encode($response);
                exit();
            }
            $user = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $user->UserID)[0];
            $this->session->set_userdata('user', $user);
            return true;
        } else {
            return false;
        }

    }

    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();


        $update['UserID'] = get_cookie('temp_user_key');
        $update_by['UserID'] = $this->UserID;
        $this->Temp_order_model->update($update, $update_by);

        $this->updateOnlineStatus('Offline');

        $user = $this->session->userdata('user');
        //$arr_update['id'] = $user['id'];
        //$this->Model_user->update($data,$arr_update);
        $this->session->unset_userdata('user');
        //$this->session->unset_userdata('admin');
        /*unset($_SESSION['userdata']);
        unset($_SESSION['loggedin_user_id']);
         unset($_SESSION['user']);
        unset($_SESSION['FBID']);
        unset($_SESSION['FULLNAME']);
        unset($_SESSION['EMAIL']);
        unset($_SESSION['FBRLH_state']);
        session_destroy();*/
        session_destroy();
        $this->session->set_flashdata('message', lang('logged_out'));
        redirect($this->config->item('base_url'));

    }


    private function signupValidation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error-field">', '</div>');
        $this->form_validation->set_rules('FullName', lang('full_name'), 'required');
        $this->form_validation->set_rules('Mobile', lang('mobile_no'), 'trim|required');
        $this->form_validation->set_rules('Email', lang('email'), 'trim|required|valid_email|is_unique[users.Email]');
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
        $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['message'] = validation_errors();
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    public function sendWelcomeEmail($user, $password)
    {
        $email_template = get_email_template(1, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user->FullName, $message);
        $message = str_replace("{{email}}", $user->Email, $message);
        $message = str_replace("{{password}}", $password, $message);
        $data['to'] = $user->Email;
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);
    }

    public function resendVerificationCode()
    {
        $user_id = $this->input->post('user_id');
        $user = $this->Model_user->get($user_id, false, 'user_id');
        if ($user->phone != '' && $user->verification_code != '') {
            $sms_data = array();
            $sms_data['to'] = convertToEnNumbers($user->phone); // converting arabic character numbers to english format
            $sms_data['sms'] = 'Verification Code : ' . $user->verification_code;
            sendSMS($sms_data);
            echo 1;
            exit();
        } else {
            echo 0;
            exit();
        }

    }

    public function updateCartUserID()
    {
        $update['UserID'] = $this->session->userdata['user']->UserID;
        $update_by['UserID'] = get_cookie('temp_user_key');
        $this->Temp_order_model->update($update, $update_by);

        // updating user temp key in users table
        $update['TempUserKey'] = get_cookie('temp_user_key');
        $update_by['UserID'] = $this->session->userdata['user']->UserID;
        $this->User_model->update($update, $update_by);
    }

    public function updateOnlineStatus($status = 'Online')
    {
        $update['OnlineStatus'] = $status;
        $update_by['UserID'] = $this->session->userdata['user']->UserID;
        $this->User_model->update($update, $update_by);
    }

    public function checkIfUserLoggedin()
    {
        if ($this->session->userdata('user')) {
            $loggedIN = 1;
        } else {
            $loggedIN = 0;
        }
        echo $loggedIN;
        exit();
    }

    public function updateProfile()
    {
        checkFrontendSession();
        $post_data = $this->input->post();
        $FullName = $post_data['FullName'];
        unset($post_data['FullName']);
        $UserID = $this->UserID;
        if (isset($post_data['OldPassword']) && $post_data['OldPassword'] != '') {
            $user_data = $this->User_model->get($UserID, false, 'UserID');
            $posted_password = md5($post_data['OldPassword']);
            $existing_password = $user_data->Password;
            if ($posted_password !== $existing_password) {
                $response['message'] = lang('wrong_old_password');
                echo json_encode($response);
                exit();
            } elseif ($post_data['NewPassword'] !== $post_data['ConfirmPassword']) {
                $response['message'] = lang('new_old_password_not_match');
                echo json_encode($response);
                exit();
            } elseif ($post_data['NewPassword'] == $post_data['ConfirmPassword']) {
                // dump($post_data);
                $post_data['Password'] = md5($post_data['NewPassword']);
                unset($post_data['OldPassword']);
                unset($post_data['NewPassword']);
                unset($post_data['ConfirmPassword']);
                $this->User_model->update($post_data, array('UserID' => $UserID));
                $this->User_text_model->update(array('FullName' => $FullName), array('UserID' => $UserID));
                $response['message'] = lang('profile_updated');
                $response['redirect'] = true;
                $response['url'] = 'account/profile';
                echo json_encode($response);
                exit();
            } else {
                $response['message'] = lang('something_went_wrong');
                echo json_encode($response);
                exit();
            }
        } else {
            $this->User_model->update($post_data, array('UserID' => $UserID));
            $this->User_text_model->update(array('FullName' => $FullName), array('UserID' => $UserID));
            $response['message'] = lang('profile_updated');
            $response['redirect'] = true;
            $response['url'] = 'account/profile';
            echo json_encode($response);
            exit();
        }
    }

    public function resetPassword()
    {
        $Email = $this->input->post('Email');
        $user_data = $this->User_model->getWithMultipleFields(array('Email' => $Email, 'RoleID' => 5));
        // dump($user_data);
        if ($user_data) {
            $rand_pass = RandomString(6);
            $update['Password'] = md5($rand_pass);
            $this->User_model->update($update, array('Email' => $Email));
            $this->sendResetPasswordEmail($Email, $rand_pass);
            $response['reset'] = true;
            // $response['reload'] = true;
            $response['message'] = lang('password_reset_successfully');
            echo json_encode($response);
            exit();
        } else {
            $response['message'] = lang('user_not_found_with_email');
            echo json_encode($response);
            exit();
        }
    }

    private function sendResetPasswordEmail($Email, $rand_pass)
    {
        $user = $this->User_model->getUsers("users.Email = '$Email'")[0];
        $email_template = get_email_template(4, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user->FullName, $message);
        $message = str_replace("{{email}}", $user->Email, $message);
        $message = str_replace("{{password}}", $rand_pass, $message);
        $data['to'] = $user->Email;
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);
    }

    public function sendOTP()
    {
        $Mobile = $this->input->post('Mobile');
        $Email = $this->input->post('Email');
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error-field">', '</div>');
        $this->form_validation->set_rules('Mobile', lang('mobile_no'), 'trim|required|is_unique[users.Mobile]');
        $this->form_validation->set_rules('Email', lang('email'), 'trim|required|valid_email|is_unique[users.Email]');
        if ($this->form_validation->run() == FALSE) {
            $errors['status'] = false;
            $errors['message'] = validation_errors();
            echo json_encode($errors);
            exit;
        }
        $OTP = RandomString();
        $this->session->set_userdata('OTP', $OTP);
        $msg = lang('send_otp_sms')."\nPIN: $OTP";
        sendSms($Mobile, $msg);
        $response['status'] = true;
        //$response['OTP'] = $this->session->userdata('OTP'); // comment this once testing done
        $response['message'] = lang('otp_sent');
        echo json_encode($response);
        exit();
    }


    public function sendOTPUsingMobile()
    {
        $Mobile = $this->input->post('Mobile');
        
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error-field">', '</div>');
        $this->form_validation->set_rules('Mobile', lang('mobile_no'), 'trim|required|is_unique[users.Mobile]');
        
        if ($this->form_validation->run() == FALSE) {
            $errors['status'] = false;
            $errors['message'] = validation_errors();
            echo json_encode($errors);
            exit;
        }
        $OTP = RandomString();
        $this->session->set_userdata('OTP', $OTP);
        $msg = lang('send_otp_sms')."\nPIN: $OTP";
        sendSms($Mobile, $msg);
        $response['status'] = true;
       // $response['OTP'] = $this->session->userdata('OTP'); // comment this once testing done
        $response['message'] = lang('otp_sent');
        echo json_encode($response);
        exit();
    }

    public function verifyOTP()
    {
        $posted_OTP = $this->input->post('OTP');
        $session_OTP = $this->session->userdata('OTP');
        if ($posted_OTP == $session_OTP) {
            $response['status'] = true;
            $response['message'] = lang('otp_sent');
            echo json_encode($response);
            exit();
        } else {
            $response['status'] = false;
            $response['message'] = lang('wrong_otp_inserted');
            echo json_encode($response);
            exit();
        }
    }

   public function verifyOTPForSocialLogin()
    {
        $posted_OTP = $this->input->post('OTP');
        $session_OTP = $this->session->userdata('OTP');
        if ($posted_OTP == $session_OTP) {

            $this->User_model->update(array('IsMobileVerified' => 1),array('UserID' => $this->session->userdata['user']->UserID));
            $this->session->userdata['user']->IsMobileVerified = 1;
            $this->session->unset_userdata('OTP');
            $response['status'] = true;
            $response['message'] = lang('update_successfully');
            echo json_encode($response);
            exit();
        } else {
            $response['status'] = false;
            $response['message'] = lang('wrong_otp_inserted');
            echo json_encode($response);
            exit();
        }
    }

    public function checkIfUserHasOffer($UserID)
    {
        $i = 0;
        $offers_for_customer = array();
        $offers = $this->Offer_model->getAllJoinedData(false, 'OfferID', $this->language, "offers.IsActive = 1");
        if ($offers) {
            foreach ($offers as $offer) {
                $offer_groups = $this->Offer_group_model->getMultipleRows(array('OfferID' => $offer->OfferID));
                if ($offer_groups) {
                    foreach ($offer_groups as $offer_group) {
                        $customer_is_group_member = $this->Customer_group_member_model->getWithMultipleFields(array('CustomerGroupID' => $offer_group->GroupID, 'UserID' => $UserID));
                        if ($customer_is_group_member) {
                            $save_data['OfferID'] = $offer->OfferID;
                            $save_data['GroupID'] = $offer_group->GroupID;
                            $save_data['UserID'] = $UserID;
                            $checkIfOfferAlreadyAdded = $this->Offer_user_notification_model->getWithMultipleFields($save_data);
                            if (!$checkIfOfferAlreadyAdded)
                            {
                                $save_data['IsShow'] = 0;
                                $this->Offer_user_notification_model->save($save_data);
                                $offers_for_customer[$i] = $offer;
                                $i++;
                            }
                        }
                    }
                }
            }
        }

        return $offers_for_customer;
    }

    public function updateTermsAccepted()
    {
        $update['TermsAccepted'] = 1; // 1 means terms accepted
        $update_by['UserID'] = $this->session->userdata['user']->UserID;
        $this->User_model->update($update, $update_by);
        echo 1;exit();
    }

}