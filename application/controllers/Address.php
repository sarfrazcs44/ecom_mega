<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        checkFrontendSession();
        $this->load->model('Temp_order_model');
        $this->load->model('Order_model');
        $this->load->model('User_address_model');
        $this->load->model('District_model');
        $this->load->model('Product_model');
        $this->data['language'] = $this->language;

    }

    public function index()
    {

        
        $this->data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        //print_rm($this->data['cart_items']);
        if (empty($this->data['cart_items'])) {
            redirect('product');
        }
        $this->data['addresses'] = $this->User_address_model->getAddresses("user_address.UserID = ".$this->UserID);
        if ($this->data['addresses'] && count($this->data['addresses']) > 0)
        {
            $where = false;
            if(isset($_GET['city']) && $_GET['city'] != ''){
                $where  = 'stores.CityID = '.$_GET['city'];
            }
            $this->data['available_cities'] = $this->Product_model->getCities($where,$this->language);
            //print_rm($this->data['available_cities']);
            $this->data['view'] = 'frontend/checkout-address';
            $this->load->view('frontend/layouts/default', $this->data);
        } else {
            $this->session->set_flashdata("message", lang('you_have_no_saved_address'));
            redirect('account/profile?p=address');
        }
    }

    public function add()
    {
        $this->data['view'] = 'frontend/add_address';
        $this->load->view('frontend/layouts/default', $this->data);
    }



    public function edit($address_id)
    {
        $this->data['view'] = 'frontend/edit_address';
        $this->data['address'] = $this->User_address_model->getAddresses("user_address.AddressID = ".$address_id." AND user_address.UserID = " . $this->UserID)[0];
        if(empty($this->data['address'])){
            redirect('account/addresses');
        }

        $this->data['districts'] = $this->District_model->getAllJoinedData(false, 'DistrictID', $this->language, "districts.IsActive = 1 AND districts.CityID = ".$this->data['address']->CityID, 'ASC', 'SortOrder');
   
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function save()
    {
        $post_data = $this->input->post();

        if($post_data['Latitude'] == '' && $post_data['Longitude'] == ''){
            $response['message'] = lang('map_pin_required');
            echo json_encode($response);
            exit();
        }

        if(isset($post_data['IsDefault'])){
            $user_default_address = $this->User_address_model->getWithMultipleFields(array('UserID' => $this->UserID, 'IsDefault' => 1));
            if($user_default_address)
            {
                $this->User_address_model->update(array('IsDefault' => 0),array('AddressID' => $user_default_address->AddressID));
                //$post_data['IsDefault'] = 0;
            }

            $post_data['IsDefault'] = 1; 

        }else{
            $post_data['IsDefault'] = 0;
        }
        
        $user_payment_address = $this->User_address_model->getWithMultipleFields(array('UserID' => $this->UserID, 'UseForPaymentCollection' => 1));
        if ($user_payment_address)
        {
            $post_data['UseForPaymentCollection'] = 0;
        } else {
            $post_data['UseForPaymentCollection'] = 1;
        }
        $post_data['UserID'] = $this->UserID;
        $insert_id = $this->User_address_model->save($post_data);
        if ($insert_id > 0) {
            $response['message'] = lang('address_saved_successfully');
            $response['reset'] = true;
            $response['redirect'] = true;
            $response['url'] = "account";
            echo json_encode($response);
            exit();
        } else {
            $response['message'] = lang('something_went_wrong');
            echo json_encode($response);
            exit();
        }
    }

    public function update()
    {
        $post_data = $this->input->post();

        if($post_data['Latitude'] == '' && $post_data['Longitude'] == ''){
            $response['message'] = lang('map_pin_required');
            echo json_encode($response);
            exit();
        }

        $address = $this->User_address_model->getAddresses("user_address.AddressID = ".$post_data['AddressID'])[0];
        if(!empty($address)){
            if($post_data['IsDefault'] == 0 && $address->IsDefault == 1){
                $response['message'] = lang('make_default_address');
                echo json_encode($response);
                exit();
            }
        }

        if(isset($post_data['IsDefault'])){
            $user_default_address = $this->User_address_model->getWithMultipleFields(array('UserID' => $this->UserID, 'IsDefault' => 1));
            if($user_default_address)
            {
                $this->User_address_model->update(array('IsDefault' => 0),array('AddressID' => $user_default_address->AddressID));
                //$post_data['IsDefault'] = 0;
            } 

            $post_data['IsDefault'] = 1; 

        }
        
        $user_payment_address = $this->User_address_model->getWithMultipleFields(array('UserID' => $this->UserID, 'UseForPaymentCollection' => 1));
        if ($user_payment_address)
        {
            $post_data['UseForPaymentCollection'] = 0;
        } else {
            $post_data['UseForPaymentCollection'] = 1;
        }
        $post_data['UserID'] = $this->UserID;
        $this->User_address_model->update($post_data,array('AddressID' => $post_data['AddressID'],'UserID' => $this->UserID));
        $response['message'] = lang('address_saved_successfully');
        $response['reset'] = true;
        $response['redirect'] = true;
        $response['url'] = "account";
        echo json_encode($response);
        exit();
    }

    public function markAddressAsDefault()
    {
        $AddressID = $this->input->get('AddressID');
        $this->User_address_model->update(array('IsDefault' => 0), array('UserID' => $this->UserID));
        $this->User_address_model->update(array('IsDefault' => 1), array('AddressID' => $AddressID));
        $response['message'] = lang('address_set_as_default');
        echo json_encode($response);
        exit();
    }

    public function setAddressForPaymentCollection()
    {
        $AddressID = $this->input->get('AddressID');
        $this->User_address_model->update(array('UseForPaymentCollection' => 0), array('UserID' => $this->UserID));
        $this->User_address_model->update(array('UseForPaymentCollection' => 1), array('AddressID' => $AddressID));
        $response['message'] = lang('address_set_for_payment');
        echo json_encode($response);
        exit();
    }

    public function delete()
    {
        $post_data = $this->input->post();
        $addressUsedForOrder = $this->Order_model->getWithMultipleFields($post_data);
        if ($addressUsedForOrder)
        {
            $response['status'] = false;
            $response['message'] = lang('cant_remove_address_order');
            echo json_encode($response);
            exit();
        } else {
            $this->User_address_model->delete($post_data);
            $response['status'] = true;
            $response['message'] = lang('address_removed');
            echo json_encode($response);
            exit();
        }
    }

}