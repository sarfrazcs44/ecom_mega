<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

date_default_timezone_set('UTC');

class Api extends REST_Controller
{
    protected $language;
    protected $CompanyID;
    public $post_data = array();

    function __construct()
    {
        parent::__construct();
        $this->methods['user_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 5000; // 50 requests per hour per user/key

        $this->load->model('City_model');
        $this->load->model('Coupon_model');
        $this->load->model('App_setting_model');
        $this->load->model('Coupon_text_model');
        $this->load->model('Company_model');
        $this->load->model('Store_model');
        $this->load->model('Promotion_model');
        $this->load->model('District_model');
        //$this->load->model('Follower_model');
        //$this->load->model('Promotion_model');

       // $this->load->model('Invite_model');
        $this->load->model('Category_model');

        $this->load->model('Product_model');
        $this->load->model('Product_text_model');
        $this->load->model('Variant_model');
        //$this->load->model('Product_image_model');
        //$this->load->model('Product_comment_model');
      // // $this->load->model('Product_like_model');
      //  $this->load->model('Product_reported_model');
       // $this->load->model('Product_view_model');
       // $this->load->model('Product_rating_model');

        $this->load->model('Promoted_product_model');
        //$this->load->model('Order_cancellation_reason_model');

        $this->load->model('Temp_order_model');
       // $this->load->model('Order_request_model');
        $this->load->model('Order_model');
        $this->load->model('Order_item_model');

        $this->load->model('Ticket_model');
        $this->load->model('Ticket_comment_model');

      //  $this->load->model('Comment_reported_model');

       // $this->load->model('Chat_model');
       // $this->load->model('Chat_message_model');

        $this->load->model('User_model');
        $this->load->model('User_text_model');
       // $this->load->model('User_customization_model');
        $this->load->model('User_address_model');
        $this->load->model('User_notification_model');
        //$this->load->model('User_friends_notification_model');
       // $this->load->model('User_category_model');
        $this->load->model('User_wishlist_model');
        //$this->load->model('User_post_reported_model');

       // $this->load->model('Theme_model');
        $this->load->model('Page_model');
      //  $this->load->model('Feedback_model');
        $this->load->model('Site_setting_model');
        $this->load->model('Site_images_model');
        $this->load->model('Model_general');
        $this->load->model('Product_availability_model');
        $this->load->model('Order_extra_charges_model');
        //$this->load->model('User_points_history_model');
      //  $this->load->model('User_post_model');
      //  $this->load->model('Package_model');

        $this->load->helper('language');

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $this->post_data = $this->input->get();
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->post_data = $this->input->post();
        }



        if (isset($this->post_data['Language']) && $this->post_data['Language'] !== '') {
            $this->language = $this->post_data['Language'];
            unset($this->post_data['Language']);
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'EN';
            }
        }
         if (isset($this->post_data['CompanyID']) && $this->post_data['CompanyID'] !== '') {
            $this->CompanyID = $this->post_data['CompanyID'];
            unset($this->post_data['CompanyID']);
        }
        /*if (!$this->session->userdata('lang')) {
            $this->session->set_userdata('lang',$this->language);
        }*/

        $this->session->set_userdata('lang',$this->language);


        //$this->lang->load('rest_controller', $this->language);

        $this->checkAppVersion();
        $this->checkIfUserExistAndActive();
        //$this->markProductsPromotionInactive();
    }

    public function test_get()
    {
        $this->_isUserAuthorized();
        echo 'herse';
        $post_data = $this->input->get('abc');
        dump($_REQUEST);
        $fields = $this->User_model->getFields();
        print_rm($fields);
        exit;
    }
    public function videoTest_post(){
        $post_data = $this->post_data;
        $file_name = uploadVideo('ProductVideo', "uploads/products/videos/");
         $this->response([
                        'status' => 200,
                        'path' => $file_name
                    ], REST_Controller::HTTP_OK);
    }

    public function videoTestBase_post(){
        $post_data = $this->post_data;
        $file_name =  uploadFileFromBase64($post_data['ProductVideo'], "uploads/products/videos/", 'mp4');
        $this->response([
                        'status' => 200,
                        'path' => $file_name
                    ], REST_Controller::HTTP_OK);
    }
    public function postTest_post()
    {
        $post_data = $this->post_data;
        dump($post_data);
    }

    public function getAppSettings_get()
    {

        $post_data = $this->post_data;
        $fetch_by = array();

        if(isset($post_data['CompanyID'])){
            $fetch_by['CompanyID']  = $post_data['CompanyID'];

        }

        $result = $this->App_setting_model->getWithMultipleFields($fetch_by);
         $this->response([

                'status' => 200,

                'setting_detail' => $result

            ], REST_Controller::HTTP_OK);


    }

    public function testPusher_get()
    {
        $message = array('name' => 'bilal', 'message' => 'test message');
        pusher($message, true);
    }

    public function email_test_post(){
        $post_data = $this->post_data;
        $email_template = get_email_template(12, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $data['to'] = $post_data['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);
         $this->response([
                        'status' => 200,
                        'message' => 'Email Sent'
                    ], REST_Controller::HTTP_OK);
    }


    public function dataTest_post(){
        $this->load->model('Test_model');
        $post_data = $this->post_data;
        $insert_id = $this->Test_model->save(array('title' => $post_data['title']));
        $data = $this->Test_model->get($insert_id,true);
        $this->response([
                        'status' => 200,
                        'data' => $data
                    ], REST_Controller::HTTP_OK);
    }

    public function generateTokenByApi_post()
    {
        $headers = $this->input->request_headers();
        if (isset($headers['Newtoken']) && $headers['Newtoken']) {
            if (isset($headers['Userid'])) {
                $id = $headers['Userid'];
            } else {
                $id = -1;
            }
            $payload = [
                'id' => $id,
                'other' => "Generate token"
            ];

            // Load Authorization Library or Load in autoload config file


            // generte a token
            $token = $this->authorization_token->generateToken($payload);
            $this->response([
                'status' => 200,
                'token' => $token
            ], REST_Controller::HTTP_OK);
        }
    }

    private function genrateToken($user_id)
    {
        $payload = [
            'id' => $user_id,
            'other' => ''
        ];

        // Load Authorization Library or Load in autoload config file


        // generte a token
        $token = $this->authorization_token->generateToken($payload);
        return $token;

    }

    private function _isUserAuthorized($not_check_for_user = FALSE)
    {
        $result = $this->authorization_token->validateToken();
        $post_data = $this->post_data;
        if (isset($result['status']) AND $result['status'] === true) {
            if ($not_check_for_user) {
                return true;
            }

            if (isset($post_data['UserID']) && $post_data['UserID'] == $result['data']->id) {
                return true;
            } else {
                $this->response([
                    $this->config->item('rest_status_field_name') => FALSE,
                    $this->config->item('rest_message_field_name') => 'You don\'t have its access'
                ], self::HTTP_UNAUTHORIZED);
            }
        } else {
            if (isset($result['message'])) {
                $message = $result['message'];
            } else {
                $message = 'You don\'t have its access';
            }
            $this->response([
                $this->config->item('rest_status_field_name') => FALSE,
                $this->config->item('rest_message_field_name') => $message
            ], self::HTTP_UNAUTHORIZED);
        }
    }

    public function test_notification_get()
    {
        $this->_isUserAuthorized(true);
        $notification_text = 'Hello this is test notification.';
        $data['Time'] = time();
        $res = sendNotification('Booth', $notification_text, $data, 152);
        dump($res);
    }

    public function test_notification_ahsan_get()
    {
        $this->_isUserAuthorized(true);
        $notification_text = 'Hello this is test notification.';
        $data['Time'] = time();
         $token = array('fRchoF81-Po:APA91bFeAjOSyShf2Hj7fBqeDV07cNt5-Ajc63wxyrZjVBMkqequIpiiQnnCrbnytzQDnbAtLq5RcOTLCxkvP_eYOmEHNl91bQX_tudIrBj03wveQZiNBAyzAym22UA0266IyEW25zPN');
        $res = sendNotificationAhsanTest('Booth', $notification_text, $token);
        dump($res);
    }


    public function test_sms_post(){
        sendSms('+966581057444', 'this is testing.',true);
    }

    public function signUp_post()
    {
        $post_data = $this->post_data;
        $this->_isUserAuthorized(true);
        if (!empty($post_data)) {
            // $this->signUpValidation();
            $save_parent_data = array();
            $save_child_data = array();
            $parents_fields = $this->User_model->getFields();
            foreach ($post_data as $key => $value) {
                if (in_array($key, $parents_fields)) {
                    $save_parent_data[$key] = $value;
                } else {
                    $save_child_data[$key] = $value;
                }

            }

            if (isset($post_data['Email']) && $post_data['Email'] !== '') {
                $user = $this->User_model->getWithMultipleFields(array('Email' => $post_data['Email']), true);
                if ($user) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('email_already_exist')
                    ], REST_Controller::HTTP_OK);
                }
            }

            if (isset($post_data['UserName']) && $post_data['UserName'] !== '') {
                $user = $this->User_model->getWithMultipleFields(array('UserName' => $post_data['UserName']), true);
                if ($user) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('username_already_exist')
                    ], REST_Controller::HTTP_OK);
                }
            }

            
            if (isset($post_data['Mobile']) && $post_data['Mobile'] !== '') {
                $user = $this->User_model->getWithMultipleFields(array('Mobile' => $post_data['Mobile']), true);
                if ($user) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('mobile_already_exist')
                    ], REST_Controller::HTTP_OK);
                }
            }

            $save_parent_data['Password'] = md5($save_parent_data['Password']);


            /*if (isset($post_data['Image']) && $post_data['Image'] !== '') {
                ini_set('memory_limit', '-1');
                $save_parent_data['Image'] = uploadFileFromBase64($post_data['Image'], "uploads/users/");
                $save_parent_data['CompressedImage'] = compress($save_parent_data['Image'], "uploads/users/compressed/user-image-" . time() . '.png');
            }*/

            if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                $save_parent_data['Image'] = uploadImage('Image', "uploads/users/");
                //$save_parent_data['CompressedImage'] = compress($save_parent_data['Image'], "uploads/users/compressed/user-image-" . time() . '.png');
                 //$product_data['ProductVideoThumbnail'] = uploadImage('ProductVideoThumbnail', "uploads/products/videos/thumbnails/");
            } else {
                $save_parent_data['Image'] = "assets/user.png";
               // $save_parent_data['CompressedImage'] = "assets/user.png";
            }

            if (isset($_FILES['CoverImage']["name"][0]) && $_FILES['CoverImage']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                $save_parent_data['CoverImage'] = uploadImage('CoverImage', "uploads/users/");
                //$save_parent_data['CompressedCoverImage'] = compress($save_parent_data['CoverImage'], "uploads/users/compressed/user-image-" . time() . '.png');
            }


            if (isset($_FILES['DrivingLicenseImg']["name"][0]) && $_FILES['DrivingLicenseImg']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                $save_parent_data['DrivingLicenseImg'] = uploadImage('DrivingLicenseImg', "uploads/users/");
                
            }
            if (isset($_FILES['RegistrationLicenseImg']["name"][0]) && $_FILES['RegistrationLicenseImg']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                $save_parent_data['RegistrationLicenseImg'] = uploadImage('RegistrationLicenseImg', "uploads/users/");
                
            }


           

            $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = time();

           

            $insert_id = $this->User_model->save($save_parent_data);
            //echo $this->db->last_query();exit;

            if ($insert_id > 0) {


               
                $save_child_data['UserID'] = $insert_id;
                $save_child_data['SystemLanguageID'] = 1;
                $this->User_text_model->save($save_child_data);
                $this->User_model->update(array('OnlineStatus' => 'Online'), array('UserID' => $insert_id));
                $where = "users.UserID = " . $insert_id;
                $user_info = $this->User_model->getUserInfo($where);
                //echo $this->db->last_query();
                //print_rm($user_info);
                $this->sendWelcomeEmail($user_info, $post_data['Password']);
               // $this->sendVerificationForEmail($user_info);//verification email sent in above email
                //$this->givePointsToInvite($save_parent_data,$insert_id);
               // $user_info['AuthToken'] = $this->genrateToken($insert_id);
                //$user_info['CategoriesCount'] = $this->User_category_model->getRowsCount(array('UserID' => $insert_id));
               // $user_info['BoothFollowersCount'] = $this->Follower_model->getRowsCount(array('Following' => $insert_id, 'Type' => 'booth'));
              //  $user_info['UserFollowersCount'] = $this->Follower_model->getRowsCount(array('Following' => $insert_id, 'Type' => 'user'));
              //  $user_info['BoothFollowingCount'] = $this->Follower_model->getRowsCount(array('Follower' => $insert_id, 'Type' => 'booth'));
             //   $user_info['UserFollowingCount'] = $this->Follower_model->getRowsCount(array('Follower' => $insert_id, 'Type' => 'user'));
            //    $user_info['BoothAverageRating'] = boothAverageRating($insert_id);
            //    $user_info['UserAverageRating'] = userAverageRating($insert_id);
           //     $user_info['UserFollowedBooths'] = $this->Follower_model->getUserFollowedBooths($insert_id);
           //     $user_info['BoothCategoriesCount'] = $this->User_category_model->getRowsCount(array('UserID' => $insert_id, 'Type' => 'booth'));
               // $user_info['UserCategoriesCount'] = $this->User_category_model->getRowsCount(array('UserID' => $insert_id, 'Type' => 'user'));
              //  $user_info['UserSelectedCategories'] = $this->User_category_model->getSelectedCategories($user_info['UserID'], 'user', $this->language);
              //  $user_info['BoothSelectedCategories'] = $this->User_category_model->getSelectedCategories($user_info['UserID'], 'booth', $this->language);
              //  $user_info['UserFollowingBooths'] = $this->Follower_model->getFollowing($user_info['UserID'], 'booth', false, false, false, $this->language);
              //  $user_info['UserFollowingUsers'] = $this->Follower_model->getFollowing($user_info['UserID'], 'user', false, false, false, $this->language);
              //  $cart_products = $this->Temp_order_model->getJoinedDataWithOutText(false, 'ProductID', 'products', 'users.PackageExpiry >= CURDATE() AND temp_orders.UserID = ' . $user_info['UserID'] . '',true);
              //  $user_info['CartCount'] = Count($cart_products);
                $this->response([
                    'status' => 200,
                    'message' => lang('registered_successfully'),
                    'user_info' => NullToEmpty($user_info)
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);

            }


        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

    }

    private function givePointsToInvite($data,$RegisteredUserID)
    {
        $site_settings = site_settings();
        $where = "invites.Mobile = '" . $data['Mobile'] . "' OR invites.Email = '" . $data['Email'] . "'";
        $invites = $this->Invite_model->getInvitees($where);
        if ($invites) {
            foreach ($invites as $invite) {
                $user = $this->User_model->get($invite->UserID, false, 'UserID');
                if ($user) {
                    $Points = $user->Points + $site_settings->InvitePoints;
                    $this->User_model->update(array('Points' => $Points), array('UserID' => $user->UserID));

                    // saving in user_points_history table
                    $points_history_data = array();
                    $points_history_data['UserID'] = $user->UserID;
                    $points_history_data['TransactionType'] = 'Received';
                    $points_history_data['Type'] = 'Invite';
                    $points_history_data['Points'] = $site_settings->InvitePoints;
                    $points_history_data['CreatedAt'] = time();
                    $this->User_points_history_model->save($points_history_data);

                    //$message = 'A person you had invited to booth has signed up and points are added to your wallet.';
                    $user_name = ($data['LastState'] == 'user' ? $data['UserName'] : $data['BoothUserName']);
                    $message = $user_name.' has Accepted your invitation and joined the application. Your are got '.$site_settings->InvitePoints.' Points from the referral rewards. Check out your Total Rewards.';
                   


                        $notification_data = array();
                    // saving notification
                        $notification_data['LoggedInUserID'] = $user->UserID; // the person whose post is this, who will receive this notification
                        $notification_data['UserType'] = $data['LastState'];
                        $notification_data['Type'] = 'invite';
                        $notification_data['UserID'] = $RegisteredUserID; // the person who is making this notification
                       
                        $notification_data['NotificationTypeID'] = 23;

                        // check if notification is already sent to user for this
                        $already_notification_sent = $this->User_notification_model->getWithMultipleFields($notification_data);
                        if (!$already_notification_sent) {
                            
                            $notification_data['NotificationTextEn'] = '@' . $message;
                            $notification_data['NotificationTextAr'] = '@' . $message;
                            $notification_data['CreatedAt'] = time();
                            $mentioned_user_ids = array($RegisteredUserID);
                            $mentioned_user_names = array($user_name);
                            $mentioned_user_types = array('user');
                            log_notification($notification_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                            $res = sendNotification('Booth', $message, $notification_data, $user->UserID);
                        }
                }
            }
        }
    }

    private function signUpValidation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('Email', lang('email'), 'trim|required|valid_email|is_unique[users.Email]');
        $this->form_validation->set_rules('UserName', lang('username'), 'trim|required|is_unique[users.UserName]');
        if ($this->form_validation->run() == FALSE) {
            $this->response([
                'status' => 402,
                'message' => validation_errors()
            ], REST_Controller::HTTP_OK);
        } else {
            return true;
        }

    }

    public function sendOTP_post()
    {
        $post_data = $this->post_data; // UserID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $user = $this->User_model->get($post_data['UserID'], false, 'UserID');
            if ($user->IsMobileVerified == 1) {
                $this->response([
                    'status' => 402,
                    'message' => lang('your_mobile_already_verified')
                ], REST_Controller::HTTP_OK);
            }

            if ($user->OTP == '') { // means OTP is not sent before OR already verified and its empty in db
                $otp = generatePIN();
                $this->User_model->update(array('OTP' => $otp), array('UserID' => $post_data['UserID']));
                if($user->LastLangState == 'EN'){
                    $message = "Please use ".$otp." to verify your account.";
                }else{
                    $message = "يرجى استخدام رمز التحقق ".$otp." لتفعيل حسابك. ";
                }
                sendSms($user->Mobile, $message);
            } else {
                $otp = $user->OTP;
                if($user->LastLangState == 'EN'){
                    $message = "Please use ".$otp." to verify your account.";
                }else{
                    $message = "يرجى استخدام رمز التحقق ".$otp." لتفعيل حسابك. ";
                }
                sendSms($user->Mobile, $message);
            }

            $this->response([
                'status' => 200,
                'message' => lang('verification_code_sent'),
                'OTP' => $otp
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function verifyOTP_post()
    {
        $post_data = $this->post_data; // UserID, OTP
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $user = $this->User_model->get($post_data['UserID'], false, 'UserID');
            if ($post_data['OTP'] == $user->OTP) {
                $this->User_model->update(array('IsMobileVerified' => 1, 'OTP' => ''), array('UserID' => $post_data['UserID']));
                $this->response([
                    'status' => 200,
                    'message' => lang('mobile_no_verified')
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('mobile_no_failed_to_verified')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function login_post()
    {
        $post_data = $this->post_data; // Email, Password
        $this->_isUserAuthorized(true);
        if (!empty($post_data)) {
            $where = "users.RoleID = 5 AND users.Email = '" . $post_data['Email'] . "' AND users.Password  = '" . md5($post_data['Password']) . "'";
            $user_info = $this->User_model->getUserInfo($where, $this->language);
            //echo $this->db->last_query();exit;
            if ($user_info) {
                if ($user_info['IsActive'] == 0) {
                    $this->response([
                        'status' => 401,
                        'message' => lang('sorry_you_cant_login')
                    ], REST_Controller::HTTP_OK);
                }/*elseif($user_info['OnlineStatus'] == 'Online'){
                    $this->response([
                        'status' => 402,
                        'message' => lang('already_login_some')
                    ], REST_Controller::HTTP_OK);

                }*/else {
                    $this->User_model->update(array('OnlineStatus' => 'Online', 'DeviceType' => $post_data['DeviceType'], 'DeviceToken' => $post_data['DeviceToken'], 'OS' => $post_data['OS']), array('UserID' => $user_info['UserID']));

                    $user_info = $this->User_model->getUserInfo($where, $this->language);
                    $user_info['AuthToken'] = $this->genrateToken($user_info['UserID']);

                   // $user_info['ProfileCustomization'] = $this->User_customization_model->getUserProfileCustomization($user_info['UserID']);

                    // $user_info['CategoriesCount'] = $this->User_category_model->getRowsCount(array('UserID' => $user_info['UserID']));
                    //$user_info['BoothFollowersCount'] = $this->Follower_model->getRowsCount(array('Following' => $user_info['UserID'], 'Type' => 'booth'));
                   // $user_info['UserFollowersCount'] = $this->Follower_model->getRowsCount(array('Following' => $user_info['UserID'], 'Type' => 'user'));
                   // $user_info['BoothFollowingCount'] = $this->Follower_model->getRowsCount(array('Follower' => $user_info['UserID'], 'Type' => 'booth'));
                   // $user_info['UserFollowingCount'] = $this->Follower_model->getRowsCount(array('Follower' => $user_info['UserID'], 'Type' => 'user'));
                   // $user_info['BoothAverageRating'] = boothAverageRating($user_info['UserID']);
                   // $user_info['UserAverageRating'] = userAverageRating($user_info['UserID']);
                   // $user_info['UserFollowedBooths'] = $this->Follower_model->getUserFollowedBooths($user_info['UserID']);
                  //  $user_info['BoothCategoriesCount'] = $this->User_category_model->getRowsCount(array('UserID' => $user_info['UserID'], 'Type' => 'booth'));
                  //  $user_info['UserCategoriesCount'] = $this->User_category_model->getRowsCount(array('UserID' => $user_info['UserID'], 'Type' => 'user'));
                   // $user_info['UserSelectedCategories'] = $this->User_category_model->getSelectedCategories($user_info['UserID'], 'user', $this->language);
                   // $user_info['BoothSelectedCategories'] = $this->User_category_model->getSelectedCategories($user_info['UserID'], 'booth', $this->language);
                  //  $user_info['UserFollowingBooths'] = $this->Follower_model->getFollowing($user_info['UserID'], 'booth', false, false, false, $this->language);
                   // $user_info['UserFollowingUsers'] = $this->Follower_model->getFollowing($user_info['UserID'], 'user', false, false, false, $this->language);
                  //  $unread_msg_count = $this->Chat_message_model->getRowsCount(array('ReceiverID' => $user_info['UserID'], 'IsReadByReceiver' => 'no','UserType' => 'user'),false,false,'ChatID',true);
                  //  $user_info['HasUnreadMessage'] = ($unread_msg_count > 0 ? 'yes' : 'no');
                 //   $user_info['UnreadMessageCount'] = $unread_msg_count;
                 //   $user_info['UserUnreadNotificationCount'] = $this->User_notification_model->getRowsCount(array('LoggedInUserID' => $user_info['UserID'], 'IsRead' => 0,'UserType' => 'user'));
                    // $user_info['BoothUnreadNotificationCount'] = $this->User_notification_model->getRowsCount(array('LoggedInUserID' => $user_info['UserID'], 'IsRead' => 0,'UserType' => 'booth'));
                    // $user_info['IsExpire'] = 0;
                   /* if($user_info['PackageExpiry'] < Date('Y-m-d')){
                        $user_info['IsExpire'] = 1;
                    }

                    $where_for_order_count = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND orders_requests.BoothID = ".$user_info['UserID']; 

                    $user_info['BoothOrderCount'] = $this->Order_model->getRowsCount($where_for_order_count,'orders_requests','OrderID');

                    $where_for_order_count = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND orders.UserID = ".$user_info['UserID']; 

                    $user_info['UserOrderCount'] = $this->Order_model->getRowsCount($where_for_order_count,'orders_requests','OrderID');

                    $unread_msg_count_booth = $this->Chat_message_model->getRowsCount(array('ReceiverID' => $user_info['UserID'], 'IsReadByReceiver' => 'no','UserType' => 'booth'),false,false,'ChatID',true);
                    $user_info['BoothHasUnreadMessage'] = ($unread_msg_count_booth > 0 ? 'yes' : 'no');
                    $user_info['BoothUnreadMessageCount'] = $unread_msg_count_booth;

                    $cart_products = $this->Temp_order_model->getJoinedDataWithOutText(false, 'ProductID', 'products', 'users.PackageExpiry >= CURDATE() AND temp_orders.UserID = ' . $user_info['UserID'] . '',true);

                    $user_info['CartCount'] = Count($cart_products);*/

                    $this->response([
                        'status' => 200,
                        'message' => lang('logged_in_successfully'),
                        'user_info' => NullToEmpty($user_info)
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('user_not_found_with_these_login_details')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function updateProfile_post()
    {

        $post_data = $this->post_data;
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $existing_user = $this->User_model->get($post_data['UserID'], true, 'UserID');

            //$get_user_data = $this->User_model->get($post_data['UserID'],false,'UserID');


            if (isset($post_data['Email']) && $post_data['Email'] !== '' && $post_data['Email'] !== $existing_user['Email']) {
                $user = $this->User_model->getWithMultipleFields(array('Email' => $post_data['Email']), true);
                if ($user) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('email_already_exist')
                    ], REST_Controller::HTTP_OK);
                }
            }

            

            if (isset($post_data['Mobile']) && $post_data['Mobile'] !== '' && $post_data['Mobile'] !== $existing_user['Mobile']) {
                $user = $this->User_model->getWithMultipleFields(array('Mobile' => $post_data['Mobile']), true);
                if ($user) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('mobile_already_exist')
                    ], REST_Controller::HTTP_OK);
                }
            }


            $parent_tbl_data = $this->User_model->getFields();
            $child_tbl_data = $this->User_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data)) {
                    $save_parent_data[$key] = $value;


                } else if (in_array($key, $child_tbl_data)) {
                    $save_child_data[$key] = $value;
                }

            }

            if (isset($save_parent_data['Password'])) {
                $save_parent_data['Password'] = md5($save_parent_data['Password']);
            }

            $save_parent_data['UpdatedAt'] = time();


             if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                $save_parent_data['Image'] = uploadImage('Image', "uploads/users/");
                //$save_parent_data['CompressedImage'] = compress($save_parent_data['Image'], "uploads/users/compressed/user-image-" . time() . '.png');
            }

             

            if (isset($post_data['Mobile']) && $post_data['Mobile'] !== '' && $post_data['Mobile'] !== $existing_user['Mobile']) {
                $save_parent_data['IsMobileVerified'] = 0;
            }


            // extra code
            /*if (isset($post_data['Image']) && $post_data['Image'] !== '') {

                $save_parent_data['Image'] = uploadFileFromBase64($post_data['Image'], "uploads/");
            }*/

            // dump($save_parent_data);

//echo lang('coupon_invalid');exit;

            $interest_update = true;

            $update_by = array();
            $update_by['UserID'] = $post_data['UserID'];

           

            $this->User_model->update($save_parent_data, $update_by);

            if ($post_data['UserID'] > 0) {
                $update_by['SystemLanguageID'] = 1;
                $this->User_text_model->update($save_child_data, $update_by);

                

                $user_info = $this->User_model->getUserInfo('users.UserID =' . $post_data['UserID'], $this->language);

                


                $user_info['AuthToken'] = $this->genrateToken($post_data['UserID']);

               

                $this->response([
                    'status' => 200,
                    'message' => lang('profile_updated'),
                    'user_info' => NullToEmpty($user_info)
                ], REST_Controller::HTTP_OK);
                
               
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function changePassword_post()
    {
        $post_data = $this->post_data; // UserID, OldPassword, NewPassword
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $UserID = $post_data['UserID'];
            $old_password_posted = md5($post_data['OldPassword']);
            $new_password = $post_data['NewPassword'];
            $user_detail = $this->User_model->get($UserID, true, 'UserID');
            $old_password_from_db = $user_detail['Password'];
            if ($old_password_posted != $old_password_from_db) {
                $this->response([
                    'status' => 402,
                    'message' => lang('wrong_old_password')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }else if (md5($new_password) == $old_password_from_db) {
                $this->response([
                    'status' => 402,
                    'message' => lang('same_password')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }else {
                $update_by['UserID'] = $UserID;
                $update['Password'] = md5($new_password);
                $this->User_model->update($update, $update_by);
                $this->response([
                    'status' => 200,
                    'message' => lang('password_changed_successfully')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => 200,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function addToWishlist_post() // same API will be used for adding / deleting in wishlist
    {
        $post_data = $this->post_data; // UserID, ProductID, Type (add, remove)
        $this->_isUserAuthorized(true);
        if (!empty($post_data)) {
            $Type = $post_data['Type'];
            unset($post_data['Type']);
            $product_data  = $this->Product_model->get($post_data['ProductID'],false,'ProductID');
            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $product_data->UserID,'booth','user');
            if ($IsBlocked){
                 $this->response([
                        'status' => 402,
                        'message' => lang('you_cannot_do')
                    ], REST_Controller::HTTP_OK);

                 exit;
            }

            $checkProduct = $this->Product_model->getProductDetail($post_data['ProductID'],'EN',"users.PackageExpiry >= CURDATE()");
            if (empty($checkProduct)) {
                $this->response([
                    'status' => 402,
                    'message' => lang('product_not_available')
                ], REST_Controller::HTTP_OK);
            }



            if (strtolower($Type) == 'remove') {
                $this->User_wishlist_model->delete($post_data);
                $this->response([
                    'status' => 200,
                    'message' => lang('removed_from_wishlist')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $already_add_to_wishlist = $this->User_wishlist_model->getWithMultipleFields($post_data);
                if ($already_add_to_wishlist) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('already_added_to_wishlist')
                    ], REST_Controller::HTTP_OK);
                } else {

                     $IsProductActive = $this->checkIfProductActive($post_data['ProductID']);
                    if (!$IsProductActive) {
                        $this->response([
                            'status' => 402,
                            'message' => lang('product_not_available')
                        ], REST_Controller::HTTP_OK);
                        exit;
                    }
                    $inserted_id = $this->User_wishlist_model->save($post_data);
                    if ($inserted_id > 0) {
                        // saving activity
                        

                        $activity_data['UserID'] = $post_data['UserID'];
                        $activity_data['LoggedInUserID'] = $product_data->UserID;
                        $activity_data['UserType'] = 'booth';
                        //$activity_data['CommentedAs'] = 'booth';// use to check follow by
                        $activity_data['Type'] = 'added_to_wishlist';
                        $activity_data['NotificationTypeID'] = 16;
                        $activity_data['ProductID'] = $post_data['ProductID'];
                        $UserInfo = getUserInfo($activity_data['UserID'], $this->language);
                        $activity_data['NotificationTextEn'] = '@' . $UserInfo->UserName . " has added a product in wishlist";
                        $activity_data['NotificationTextAr'] = '@' . $UserInfo->UserName . " has added a product in wishlist";
                        $activity_data['CreatedAt'] = time();
                        $mentioned_user_ids = array($UserInfo->UserID);
                        $mentioned_user_names = array($UserInfo->UserName);
                        $mentioned_user_types = array('user');
                        log_friend_activity($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                        
                        sendNotification('Booth', $activity_data['NotificationTextEn'], $activity_data, $product_data->UserID);
                        //$activity_data['UserType'] = 'booth';
                        log_notification($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                        $this->response([
                            'status' => 200,
                            'message' => lang('added_to_wishlist')
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                    } else {
                        $this->response([
                            'status' => 402,
                            'message' => lang('something_went_wrong')
                        ], REST_Controller::HTTP_OK);
                    }
                }
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }


    public function addAddress_post()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $insert_id = $this->User_address_model->save($post_data);
            if ($insert_id > 0) {
                $address = $this->User_address_model->getAddresses('user_address.AddressID = ' . $insert_id, $this->language);
                $this->response([
                    'status' => 200,
                    'address_info' => $address[0]
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => 'There is something went worng'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getCategories_get()
    {
        $this->_isUserAuthorized(true);
        $post_data = $this->post_data;
        if (!empty($post_data)) {

            $return_array = array();
            if(isset($post_data['ParentID'])){
                $ParentID = $post_data['ParentID'];
            }else{
                 $ParentID = 0;
            }

            $where  = 'categories.ParentID ='.$ParentID.' AND categories.CompanyID = '.$this->CompanyID;
            $categories = getCategories($this->language,$where);
            if ($categories) {
                $return_array = $categories;
                
            } 

            $this->response([
                    'status' => 200,
                    'categories' => $return_array
                ], REST_Controller::HTTP_OK);


        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


     public function addUserPost_post()
    {
        $this->_isUserAuthorized(true);
        $post_data = $this->post_data;
        if (!empty($post_data)) {


            $user_data =  $this->User_model->getUserInfo('users.UserID =' . $post_data['UserID'], $this->language);
            if(!empty($user_data)){

                 //$get_user_data = $this->User_model->get($post_data['UserID'],false,'UserID');

            if($user_data['LastState'] == 'booth'){
                if($user_data['PackageExpiry'] < Date('Y-m-d')){
                    $this->response([
                            'status' => 402,
                            'message' => lang('please_update_your_package')
                        ], REST_Controller::HTTP_OK);

                }
            }


                if($user_data['IsEmailVerified'] == 0){
                    $this->response([
                        'status' => 402,
                        'message' => lang('email_not_verify')
                    ], REST_Controller::HTTP_OK);

                } 
                if($user_data['IsMobileVerified'] == 0){
                    $this->response([
                        'status' => 402,
                         'message' => lang('mobile_no_not_verify')
                    ], REST_Controller::HTTP_OK);

                }

                if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                     ini_set('memory_limit', '-1');
                    $post_data['Image'] = uploadImage('Image', "uploads/images/");
                }


                $post_data['CreatedAt'] = $post_data['UpdatedAt']  = time();
                $post_data['CreatedBy'] = $post_data['UpdatedBy']  = $post_data['UserID'];

                $insert_id = $this->User_post_model->save($post_data);
                if ($insert_id > 0) {
                    $result = $this->User_post_model->getPosts('user_posts.UserPostID = '.$insert_id);
                    $this->response([
                        'status' => 200,
                        'user_post_info' => $result,
                         'message' => lang('post_added_successfully'),
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK);
                }

            }else{
                 $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK);

            }


            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function updateAddress_post()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $update_by['AddressID'] = $post_data['AddressID'];
            $this->User_address_model->update($post_data, $update_by);
            $address = $this->User_address_model->getAddresses('user_address.AddressID = ' . $post_data['AddressID'], $this->language);
            $this->response([
                'status' => 200,
                'address_info' => $address[0]
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getAddress_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $address = $this->User_address_model->getAddresses('user_address.AddressID = ' . $post_data['AddressID'], $this->language);
            if ($address) {
                $this->response([
                    'status' => 200,
                    'address_info' => $address[0]
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => 'There is something went worng'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getCompanyData_get()
    {
        $this->_isUserAuthorized(true);

        $post_data = $this->post_data;

        $post_data['CompanyID'] = $this->CompanyID;

        if (!empty($post_data)) {

            $company_data = $this->Company_model->getJoinedData(true,'CompanyID','system_languages.ShortCode = "' . $this->language . '" AND companies.CompanyID = ' . $post_data['CompanyID']);
            if ($company_data) {
                $company_stores = $this->Store_model->getAllStores($this->language,'stores.CompanyID = '.$post_data['CompanyID']);
                $company_data[0]['CompanyStores'] = $company_stores;

                $this->response([
                    'status' => 200,
                    'company_data' => $company_data[0]
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => 'There is something went worng'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getStores_get()
    {
        $this->_isUserAuthorized(true);

        $post_data = $this->post_data;

        $post_data['CompanyID'] = $this->CompanyID;
        $return_array = array();

        if (!empty($post_data)) {
            $where = '';

            if(isset($post_data['CityID'])){
                $where = ' AND stores.CityID = '.$post_data['CityID'];
            }

            $company_stores = $this->Store_model->getAllStores($this->language,'stores.CompanyID = '.$post_data['CompanyID'].' '.$where.'');
            if (!empty($company_stores)) {
                
                $return_array = $company_stores;

                
            } 

            $this->response([
                    'status' => 200,
                    'stores' => $return_array
                ], REST_Controller::HTTP_OK);


        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getAllAddress_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $address = $this->User_address_model->getAddresses('user_address.UserID = ' . $post_data['UserID'], $this->language);
            $this->response([
                'status' => 200,
                'address_info' => !empty($address) ? $address : array()
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function getUserPost_get()
    {
        $this->_isUserAuthorized(true);
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $result = $this->User_post_model->getPosts('users.PackageExpiry >= CURDATE() AND user_posts.UserID = '.$post_data['UserID']);
            $return_array = array();
            if($result){
                $return_array = $result;
            }
            $this->response([
                'status' => 200,
                'user_posts' => $return_array
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function deleteAddress_post()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $deleted_by['AddressID'] = $post_data['AddressID'];
            $this->User_address_model->update(array('Hide' => 1), $deleted_by); // soft deleting this address
            $this->response([
                'status' => 200,
                'message' => lang('deleted_successfully')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function deleteProduct_post()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        $where = 'products.ProductID = '.$post_data['ProductID'].' AND orders_requests.OrderStatusID != 4 AND orders_requests.OrderStatusID != 8 AND orders_requests.OrderStatusID != 5 AND orders_requests.OrderStatusID != 6 AND orders_requests.OrderStatusID != 9';

        $check_order = $this->Order_model->getOrdersDetailForProductDelete($where);
        if($check_order){
            $this->response([
                'status' => 402,
                'message' => lang('product_is_in_cart')
            ], REST_Controller::HTTP_OK);
        }

        if (!empty($post_data)) {
            $this->Product_model->update(array('Hide' => 1), array('ProductID' => $post_data['ProductID']));
            $deleted_by['ProductID'] = $post_data['ProductID'];
            $this->Temp_order_model->delete($deleted_by);
            $this->Product_like_model->delete($deleted_by);
            $this->Product_comment_model->delete($deleted_by);
            $this->Product_reported_model->delete($deleted_by);
            $this->User_wishlist_model->delete($deleted_by);
            $this->User_notification_model->delete($deleted_by);
            $this->User_friends_notification_model->delete($deleted_by);
            $this->User_post_model->delete($deleted_by);
            $this->response([
                'status' => 200,
                'message' => lang('deleted_successfully')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function deletePost_post()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        

        if (!empty($post_data)) {
           
            $deleted_by['UserPostID'] = $post_data['UserPostID'];
            $this->User_post_model->delete($deleted_by);
            $this->response([
                'status' => 200,
                'message' => lang('deleted_successfully')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function likeProduct_post() // same api is being used for dislike also
    {
        $post_data = $this->post_data; // ProductID, UserID , Type (Like, Dislike)
        $this->_isUserAuthorized();
        $type = $post_data['Type'];
        unset($post_data['Type']);
        $product_data  = $this->Product_model->get($post_data['ProductID'],false,'ProductID');
        $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $product_data->UserID,'booth','user');
        if ($IsBlocked) {
             $this->response([
                    'status' => 402,
                    'message' => lang('you_cannot_do')
                ], REST_Controller::HTTP_OK);

             exit;
        }

        $checkProduct = $this->Product_model->getProductDetail($post_data['ProductID'],'EN',"users.PackageExpiry >= CURDATE()");
        if (empty($checkProduct)) {
            $this->response([
                'status' => 402,
                'message' => lang('product_not_available')
            ], REST_Controller::HTTP_OK);
        }

        if (!empty($post_data)) {
            if (isset($type) && strtolower($type) == 'dislike') {
                $this->Product_like_model->delete($post_data);

                $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $post_data['ProductID']);
                $LikesCount = 0;
                if (!empty($likes)) {
                    if (isset($post_data['UserID'])) {
                        $my_arr = array();
                        $l_count = 0;
                        foreach ($likes as $key => $value) {
                            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                            if ($IsBlocked) {
                                continue;
                            }
                            $my_arr[$l_count] = $value;
                            $l_count++;
                        }
                        $likes = $my_arr;
                    }
                    $LikesCount = count($likes);
                }

                $this->response([
                    'status' => 200,
                    'message' => lang('product_disliked_successfully'),
                    'LikesCount' => (string)$LikesCount
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            } elseif (isset($type) && strtolower($type) == 'like') {
                $already_liked = $this->Product_like_model->getWithMultipleFields($post_data);
                if ($already_liked) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('product_already_liked')
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->Product_like_model->save($post_data);
                    $product_detail = $this->Product_model->get($post_data['ProductID'], true, 'ProductID');
                    if ($product_detail['UserID'] !== $post_data['UserID']) {

                        // saving notification
                        $data['LoggedInUserID'] = $product_detail['UserID']; // the person whose post is this, who will receive this notification
                        $data['UserType'] = 'booth';
                        $data['Type'] = 'like';
                        $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                        $data['ProductID'] = $post_data['ProductID'];
                        $data['NotificationTypeID'] = 1;

                        // check if notification is already sent to user for this
                        $already_notification_sent = $this->User_notification_model->getWithMultipleFields($data);
                        if (!$already_notification_sent) {
                            $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                            $UserInfo = getUserInfo($data['UserID'], $this->language);
                            $data['NotificationTextEn'] = '@' . $UserInfo->UserName . " has liked your post";
                            $data['NotificationTextAr'] = '@' . $UserInfo->UserName . " قام بالإعجاب بمنتجك";
                            $data['CreatedAt'] = time();
                            $mentioned_user_ids = array($UserInfo->UserID);
                            $mentioned_user_names = array($UserInfo->UserName);
                            $mentioned_user_types = array('user');
                            log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                            $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $product_detail['UserID']);
                           // print_rm($res);
                        }
                    }

                    // saving activity
                    $activity_data['UserID'] = $post_data['UserID'];
                    $activity_data['UserType'] = 'user';
                    $activity_data['CommentedAs'] = 'user';//to check followed by

                    $activity_data['Type'] = 'like';
                    $activity_data['NotificationTypeID'] = 21;
                    $activity_data['ProductID'] = $post_data['ProductID'];
                    $UserInfo = getUserInfo($activity_data['UserID'], $this->language);
                    $activity_data['NotificationTextEn'] = '@' . $UserInfo->UserName . " has liked a product";
                    $activity_data['NotificationTextAr'] = '@' . $UserInfo->UserName . " قام بالاعجاب بمنتجك";
                    $activity_data['CreatedAt'] = time();
                    $mentioned_user_ids = array($UserInfo->UserID);
                    $mentioned_user_names = array($UserInfo->UserName);
                    $mentioned_user_types = array('user');
                    log_friend_activity($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);

                    $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $post_data['ProductID']);
                    $LikesCount = 0;
                    if (!empty($likes)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $l_count = 0;
                            foreach ($likes as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$l_count] = $value;
                                $l_count++;
                            }
                            $likes = $my_arr;
                        }
                        $LikesCount = count($likes);
                    }

                    $this->response([
                        'status' => 200,
                        'message' => lang('product_liked_successfully'),
                        'LikesCount' => (string)$LikesCount
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);

        }
    }

    public function productComment_post()
    {
        $post_data = $this->post_data; // ProductID, UserID, Comment, CommentedAs, MentionedUsers (Comma Separated)
        $this->_isUserAuthorized(true);
        if (!empty($post_data)) {
            $product_data  = $this->Product_model->get($post_data['ProductID'],false,'ProductID');
            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $product_data->UserID,false,$post_data['CommentedAs']);// here we are checking my type
            if ($IsBlocked) {
                 $this->response([
                        'status' => 402,
                        'message' => lang('you_cannot_do')
                    ], REST_Controller::HTTP_OK);

                 exit;
            }

             $checkProduct = $this->Product_model->getProductDetail($post_data['ProductID'],'EN',"users.PackageExpiry >= CURDATE()");
            if (empty($checkProduct)) {
                $this->response([
                    'status' => 402,
                    'message' => lang('product_not_available')
                ], REST_Controller::HTTP_OK);
            }
            $MentionedUsers = $post_data['MentionedUsers'];
            unset($post_data['MentionedUsers']);
            if ($MentionedUsers != '') {
                $MentionedUsers = explode(',', $MentionedUsers);
                $MentionedUserID = array();
                $MentionedUserName = array();
                $MentionedUserType = array();
                if (!empty($MentionedUsers)) {
                    $i = 0;
                    foreach ($MentionedUsers as $mentionedUser) {
                        // checking if this username exist in db
                        $UserNameExist = $this->User_model->getWithMultipleFields(array('UserName' => $mentionedUser));
                        $BoothUserNameExist = $this->User_model->getWithMultipleFields(array('BoothUserName' => $mentionedUser));
                        if ($UserNameExist) {
                            $MentionedUserID[$i] = $UserNameExist->UserID;
                            $MentionedUserName[$i] = $UserNameExist->UserName;
                            $MentionedUserType[$i] = 'user';
                            $i++;
                        } elseif ($BoothUserNameExist) {
                            $MentionedUserID[$i] = $BoothUserNameExist->UserID;
                            $MentionedUserName[$i] = $BoothUserNameExist->BoothUserName;
                            $MentionedUserType[$i] = 'booth';
                            $i++;
                        }
                    }
                }

                if (!empty($MentionedUserID)) {
                    $post_data['MentionedUserID'] = implode(',', $MentionedUserID);
                    $post_data['MentionedUserName'] = implode(',', $MentionedUserName);
                    $post_data['MentionedUserType'] = implode(',', $MentionedUserType);
                }
            }
            $post_data['CreatedAt'] = time();
            // dump($post_data);
            /*$start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }*/

            $inserted_id = $this->Product_comment_model->save($post_data);
            if ($inserted_id > 0) {
                $product_detail = $this->Product_model->get($post_data['ProductID'], true, 'ProductID');

                if ($product_detail['UserID'] !== $post_data['UserID']) {
                    // saving notification
                    $data['LoggedInUserID'] = $product_detail['UserID']; // the person whose post is this, who will receive this notification
                    $data['UserType'] = 'booth';
                    $data['ActivityDoneAs'] = (isset($post_data['CommentedAs']) ? $post_data['CommentedAs'] : '');
                    $data['Type'] = 'comment';
                    $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                    $data['ProductID'] = $post_data['ProductID'];
                    $data['ProductCommentID'] = $inserted_id;
                    $data['NotificationTypeID'] = 2;
                    $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                    $UserInfo = getUserInfo($data['UserID'], $this->language);
                    $data['NotificationTextEn'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " has commented on your post";
                    $data['NotificationTextAr'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " قام بالتعليق على منتجك";
                    $data['CreatedAt'] = time();
                    $mentioned_user_ids = array($UserInfo->UserID);
                    $mentioned_user_names = array(($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ));
                    $mentioned_user_types = array($post_data['CommentedAs']);
                    log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                    $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $product_detail['UserID']);
                }

                // saving activity

                $activity_data['UserID'] = $post_data['UserID'];
                $activity_data['CommentedAs'] = $post_data['CommentedAs'];
               // $activity_data['ActivityDoneAs'] = (isset($post_data['CommentedAs']) ? $post_data['CommentedAs'] : '');
                $activity_data['UserType'] = 'user';
                $activity_data['Type'] = 'product_comment';
                $activity_data['NotificationTypeID'] = 18;
                $activity_data['ProductID'] = $post_data['ProductID'];
                $activity_data['ProductCommentID'] = $inserted_id;
                $UserInfo = getUserInfo($activity_data['UserID'], $this->language);
                $activity_data['NotificationTextEn'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " has commented on a product";
                $activity_data['NotificationTextAr'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ). " قام بالتعليق على منتجك";
                $activity_data['CreatedAt'] = time();
                $mentioned_user_ids = array($UserInfo->UserID);
                $mentioned_user_names = array(($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ));
                $mentioned_user_types = array($post_data['CommentedAs']);
                log_friend_activity($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);

                $comment_detail = $this->Product_comment_model->getProductComments("product_comments.ProductCommentID = " . $inserted_id, $this->language);
                $comment_detail = $comment_detail[0];
                $mentioned_users_info = array();
                if ($comment_detail->MentionedUserID !== '') {
                    $user_ids = explode(',', $comment_detail->MentionedUserID);
                    $user_names = explode(',', $comment_detail->MentionedUserName);
                    $user_types = explode(',', $comment_detail->MentionedUserType);
                    $j = 0;
                    foreach ($user_ids as $key => $id) {

                        // saving notification
                        $data['LoggedInUserID'] = $id;
                        $data['UserType'] = $user_types[$key];
                        $data['ActivityDoneAs'] = (isset($post_data['CommentedAs']) ? $post_data['CommentedAs'] : '');
                        $data['Type'] = 'comment';
                        $data['UserID'] = $post_data['UserID'];
                        $data['ProductID'] = $post_data['ProductID'];
                        $data['ProductCommentID'] = $inserted_id;
                        $data['NotificationTypeID'] = 5;
                        //$LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                       // echo $data['UserID'];
                        $UserInfo = getUserInfo($data['UserID'], $this->language);
                        //print_rm($UserInfo);
                        $data['NotificationTextEn'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " has mentioned you in a comment";
                        $data['NotificationTextAr'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " قام باضافتك في تعليق";
                        $data['CreatedAt'] = time();
                        $mentioned_user_ids = array($UserInfo->UserID);
                        $mentioned_user_names = array(($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ));
                        $mentioned_user_types = array($post_data['CommentedAs']);
                        log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                        $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $id);

                        $userInfoForComment = $this->User_model->getUsers("users.UserID = " . $id, $this->language);
                        if (!empty($userInfoForComment)) {
                            $userInfoForComment = $userInfoForComment[0];
                            $mentioned_users_info[$j]['UserID'] = $userInfoForComment->UserID;
                            $mentioned_users_info[$j]['FullName'] = $userInfoForComment->FullName;
                            $mentioned_users_info[$j]['MentionedName'] = $user_names[$j];
                            $mentioned_users_info[$j]['MentionedUserType'] = $user_types[$j];
                        }
                        $j++;
                    }
                }

                if (isset($mentioned_users_info[0]) && !empty($mentioned_users_info[0])) {
                    $comment_detail->MentionedUsersInfo = $mentioned_users_info;
                } else {
                    $comment_detail->MentionedUsersInfo = array();
                }

                $this->response([
                    'status' => 200,
                    'message' => lang('comment_saved_successfully'),
                    'data' => $comment_detail
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function deleteProductComment_post() // QuestionID
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $deleted_by['ProductCommentID'] = $post_data['ProductCommentID'];
            $deleted_by['UserID'] = $post_data['UserID'];
            $this->Product_comment_model->delete($deleted_by);

            $deleted_report['CommentID'] = $post_data['ProductCommentID'];
            $deleted_report['ReportType'] = 'product';
            $this->Comment_reported_model->delete($deleted_report);

            $deleted_notification['Type'] = 'comment';
            $deleted_notification['UserID'] = $post_data['UserID'];
            $deleted_notification['ProductID'] = $post_data['ProductID'];
            $deleted_notification['ProductCommentID'] = $post_data['ProductCommentID'];
            $this->User_notification_model->delete($deleted_notification);

            $this->response([
                'status' => 200,
                'message' => lang('deleted_successfully')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getProductComments_post()
    {
        $post_data = $this->post_data; // ProductID, UserID (Optional)
        $this->_isUserAuthorized(true);
        if (!empty($post_data)) {
            $start = 0;
            $limit = 20;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }

            $comments = $this->Product_comment_model->getProductComments('product_comments.ProductID = ' . $post_data['ProductID'], $this->language, 'product_comments.ProductCommentID', 'DESC', $limit, $start);

            if (isset($post_data['UserID'])) {
                if (count($comments) > 0) {
                    $my_arr = array();
                    $i = 0;
                    $BlockedUserType = false;
                    if(isset($post_data['BlockedUserType'])){
                        $BlockedUserType = $post_data['BlockedUserType'];
                    }
                    foreach ($comments as $key => $value) {
                        $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID,$BlockedUserType);
                        if ($IsBlockedAsBooth) {
                            continue;
                        }
                        $my_arr[$i] = $value;
                        $i++;
                    }
                    $comments = $my_arr;
                }
            }

            if (!empty($comments)) {
                $i = 0;
                foreach ($comments as $comment) {
                    $user_ids = explode(',', $comment->MentionedUserID);
                    $user_names = explode(',', $comment->MentionedUserName);
                    $user_types = explode(',', $comment->MentionedUserType);
                    $mentioned_users_info = array();
                    $j = 0;
                    foreach ($user_ids as $id) {
                        $userInfoForComment = $this->User_model->getUsers("users.UserID = " . $id, $this->language);
                        if (!empty($userInfoForComment)) {
                            $userInfoForComment = $userInfoForComment[0];
                            $mentioned_users_info[$j]['UserID'] = $userInfoForComment->UserID;
                            $mentioned_users_info[$j]['FullName'] = $userInfoForComment->FullName;
                            $mentioned_users_info[$j]['MentionedName'] = $user_names[$j];
                            $mentioned_users_info[$j]['MentionedUserType'] = $user_types[$j];
                        }
                        $j++;
                    }

                    if (isset($mentioned_users_info[0]) && !empty($mentioned_users_info[0])) {
                        $comments[$i]->MentionedUsersInfo = $mentioned_users_info;
                    } else {
                        $comments[$i]->MentionedUsersInfo = array();
                    }

                    $i++;
                }
            }

            $this->response([
                'status' => 200,
                'comments' => !empty($comments) ? $comments : array()
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function getUserOrdersLikesWishlist_get()
    {
        $this->_isUserAuthorized(true);
        $post_data = $this->post_data; // UserID, OtherUserID (Optional)
        if (!empty($post_data)) {
            // send this OtherUserID if viewing other user's profile
            $send_like_data = true;
            $send_wishlist_data = true;
            $send_order_data = true;
            
            if (isset($post_data['OtherUserID'])) {
                $UserIDForData = $post_data['OtherUserID'];
                $getUserProfile = $this->User_model->get($post_data['OtherUserID'],true,'UserID');
                if($getUserProfile['IsLikesPrivate'] == 1){
                    $send_like_data = false;
                }
                if($getUserProfile['IsWishListPrivate'] == 1){
                    $send_wishlist_data = false;
                }
                if($getUserProfile['IsOrdersPrivate'] == 1){
                    $send_order_data = false;
                 }
                /*$get_by = array();
                $get_by['Type'] = 'user';
                $get_by['Follower'] = $post_data['UserID'];
                $get_by['Following'] = $post_data['OtherUserID'];
                $is_user_following = $this->Follower_model->getWithMultipleFields($get_by);
                if(!$is_user_following){
                        
                }*/
            } else {
                $UserIDForData = $post_data['UserID'];
                
            }
            $user_liked_products = $this->Product_like_model->getUserLikedProducts($UserIDForData, $this->language);
            $user_wishlist_products = $this->User_wishlist_model->getUserWishlistProducts($UserIDForData, $this->language);

            $filtered_user_liked_products = array();
            if (!empty($user_liked_products) && $send_like_data) {
                $i = 0;
                foreach ($user_liked_products as $user_liked_product) {
                    $UserID = $UserIDForData;
                    $BlockedUserID = $user_liked_product->BoothID;
                    $user_blocked = $this->checkIfUserBlocked($UserID, $BlockedUserID,'booth','user');
                    if ($user_blocked) {
                        continue;
                    }
                    $filtered_user_liked_products[$i] = $user_liked_product;
                    $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $user_liked_product->ProductID));
                    $filtered_user_liked_products[$i]->ProductImages = $product_images ? $product_images : array();
                    $i++;
                }
            }

            $filtered_user_wishlist_products = array();
            if (!empty($user_wishlist_products) && $send_wishlist_data) {
                $i = 0;
                foreach ($user_wishlist_products as $user_wishlist_product) {
                    $UserID = $UserIDForData;
                    $BlockedUserID = $user_wishlist_product->BoothID;
                    $user_blocked = $this->checkIfUserBlocked($UserID, $BlockedUserID,'booth','user');
                    if ($user_blocked) {
                        continue;
                    }
                    $filtered_user_wishlist_products[$i] = $user_wishlist_product;
                    $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $user_wishlist_product->ProductID));
                    $filtered_user_wishlist_products[$i]->ProductImages = $product_images ? $product_images : array();
                    $i++;
                }
            }
            $completed_orders = array();
            if($send_order_data){
                $get_orders_by['UserID'] = $UserIDForData;
                $get_orders_by['OrderStatus'] = 'Completed';
                $completed_orders = $this->getOrdersForUser($get_orders_by, true); 
            }

            

            $this->response([
                'status' => 200,
                'orders' => $completed_orders,
                'liked_products' => $filtered_user_liked_products,
                'wishlist_products' => $filtered_user_wishlist_products
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function customizeProfile_post()
    {
        $post_data = $this->post_data; // UserID, ThemeID, ColorCode, TopStyleID, BottomStyleID, VatPercentage, ContactDays ( Comma seperated days like Saturday, Monday, Wednesday), About, IsProfileCustomized, ContactTimeFrom, ContactTimeTo, Mobile, HideContactNo (0 for no,1 for yes)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {


            $get_user_data = $this->User_model->get($post_data['UserID'],false,'UserID');
            if($get_user_data->LastState == 'booth'){

                if($get_user_data->PackageExpiry < Date('Y-m-d')){
                    $this->response([
                            'status' => 402,
                            'message' => lang('please_update_your_package')
                        ], REST_Controller::HTTP_OK);

                }
            }
            // updating user data
            $user_text_data['About'] = $post_data['About'];
            $user_data['ContactDays'] = $post_data['ContactDays'];
            $user_data['ContactTimeFrom'] = $post_data['ContactTimeFrom'];
            $user_data['ContactTimeTo'] = $post_data['ContactTimeTo'];
            if (isset($post_data['Mobile']) && $post_data['Mobile'] !== '') {
                $user_data['Mobile'] = $post_data['Mobile'];
            }
            $user_data['HideContactNo'] = $post_data['HideContactNo'];
            $user_data['IsProfileCustomized'] = 1;
           /* if (isset($post_data['BoothImage']) && $post_data['BoothImage'] !== '') {
                ini_set('memory_limit', '-1');
                $user_data['BoothImage'] = uploadFileFromBase64($post_data['BoothImage'], "uploads/users/");
                $user_data['CompressedBoothImage'] = compress($user_data['BoothImage'], "uploads/users/compressed/user-image-b" . time() . '.png');
            }*/

            if (isset($_FILES['BoothImage']["name"][0]) && $_FILES['BoothImage']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                $user_data['BoothImage'] = uploadImage('BoothImage', "uploads/users/");
                $user_data['CompressedBoothImage'] = compress($user_data['BoothImage'], "uploads/users/compressed/user-image-b" . time() . '.png');
                 //$product_data['ProductVideoThumbnail'] = uploadImage('ProductVideoThumbnail', "uploads/products/videos/thumbnails/");
            }

           /* if (isset($post_data['BoothCoverImage']) && $post_data['BoothCoverImage'] !== '') {
                ini_set('memory_limit', '-1');
                $user_data['BoothCoverImage'] = uploadFileFromBase64($post_data['BoothCoverImage'], "uploads/users/");
                $user_data['CompressedBoothCoverImage'] = compress($user_data['BoothCoverImage'], "uploads/users/compressed/user-image-" . time() . '.png');
            }*/


             if (isset($_FILES['BoothCoverImage']["name"][0]) && $_FILES['BoothCoverImage']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                $user_data['BoothCoverImage'] = uploadImage('BoothCoverImage', "uploads/users/");
                $user_data['CompressedBoothCoverImage'] = compress($user_data['BoothCoverImage'], "uploads/users/compressed/user-image-" . time() . '.png');
            }

            $this->User_model->update($user_data, array('UserID' => $post_data['UserID']));
            $this->User_text_model->update($user_text_data, array('UserID' => $post_data['UserID']));

            $alreadyCustomized = $this->User_customization_model->getWithMultipleFields(array('UserID' => $post_data['UserID']));
            if ($alreadyCustomized) {
                // updating the data
                $customize_data['ThemeID'] = $post_data['ThemeID'];
                $customize_data['ColorCode'] = $post_data['ColorCode'];
                $customize_data['TopStyleID'] = $post_data['TopStyleID'];
                $customize_data['BottomStyleID'] = $post_data['BottomStyleID'];
                $customize_data['VatPercentage'] = $post_data['VatPercentage'];
                $this->User_customization_model->update($customize_data, array('UserID' => $post_data['UserID']));
                $user_info = $this->User_model->getUserInfo('users.UserID =' . $post_data['UserID'], $this->language);
                $user_info['ProfileCustomization'] = $this->User_customization_model->getUserProfileCustomization($post_data['UserID']);

                $this->response([
                    'status' => 200,
                    'message' => lang('profile_customized_successfully'),
                    'user_info' => $user_info
                ], REST_Controller::HTTP_OK);
            } else {
                // saving the data
                $customize_data['UserID'] = $post_data['UserID'];
                $customize_data['ThemeID'] = $post_data['ThemeID'];
                $customize_data['ColorCode'] = $post_data['ColorCode'];
                $customize_data['TopStyleID'] = $post_data['TopStyleID'];
                $customize_data['BottomStyleID'] = $post_data['BottomStyleID'];
                $customize_data['VatPercentage'] = $post_data['VatPercentage'];
                $inserted_id = $this->User_customization_model->save($customize_data);
                if ($inserted_id > 0) {
                    $user_info = $this->User_model->getUserInfo('users.UserID =' . $post_data['UserID'], $this->language);
                    $user_info['ProfileCustomization'] = $this->User_customization_model->getUserProfileCustomization($post_data['UserID']);
                    $this->response([
                        'status' => 200,
                        'message' => lang('profile_customized_successfully'),
                        'user_info' => $user_info
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK);
                }
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function forgotPassword_post()
    {
        $post_data = $this->post_data; // Query (This can be email or mobile no)
        $this->_isUserAuthorized(true);
        if (!empty($post_data)) {
            $user_with_email = $this->User_model->get($post_data['Query'], true, 'Email');
            $user_with_mobile = $this->User_model->get($post_data['Query'], true, 'Mobile');
            if ($user_with_email || $user_with_mobile) {
                if ($user_with_email) {
                    $this->sendForgotPasswordEmail($user_with_email);
                    $this->response([
                        'status' => 200,
                        'message' => lang('password_reset_link_sent_in_email')
                    ], REST_Controller::HTTP_OK);
                } elseif ($user_with_mobile) {
                    // $this->sendForgotPasswordSMS($user_with_mobile);
                    $this->sendForgotPasswordEmail($user_with_mobile);
                    $this->response([
                        'status' => 200,
                        'message' => lang('password_reset_link_sent_in_mobile')
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('user_not_found')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function invite_post()
    {
        $post_data = $this->post_data; // UserID, Mobile, Email
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $mobile_already_invited = false;
            $user_exist_with_mobile = false;
            $email_already_invited = false;
            $user_exist_with_email = false;
            if (isset($post_data['Mobile']) && $post_data['Mobile'] != '') {
                $mobile_already_invited = $this->Invite_model->getWithMultipleFields(array('UserID', $post_data['UserID'], 'Mobile' => $post_data['Mobile']));
                $user_exist_with_mobile = $this->User_model->getWithMultipleFields(array('Mobile' => $post_data['Mobile']));
            }
            if (isset($post_data['Email']) && $post_data['Email'] != '') {
                $email_already_invited = $this->Invite_model->getWithMultipleFields(array('UserID', $post_data['UserID'], 'Email' => $post_data['Email']));
                $user_exist_with_email = $this->User_model->getWithMultipleFields(array('Email' => $post_data['Email']));
            }
            if ($mobile_already_invited || $email_already_invited) {
                if ($mobile_already_invited) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('invitation_already_sent_mobile')
                    ], REST_Controller::HTTP_OK);
                } elseif ($email_already_invited) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('invitation_already_sent_email')
                    ], REST_Controller::HTTP_OK);
                }
            } elseif ($user_exist_with_mobile || $user_exist_with_email) {
                if ($user_exist_with_mobile) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('user_already_exist_with_mobile')
                    ], REST_Controller::HTTP_OK);
                } elseif ($user_exist_with_email) {
                    $this->response([
                        'status' => 402,
                        'message' => lang('user_already_exist_with_email')
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $inserted_id = $this->Invite_model->save($post_data);
                if ($inserted_id > 0) {
                    if (isset($post_data['Email']) && $post_data['Email'] != '') {
                        $email_template = get_email_template(12, $this->language);
                        $subject = $email_template->Heading;
                        $message = $email_template->Description;
                        $data['to'] = $post_data['Email'];
                        $data['subject'] = $subject;
                        $data['message'] = email_format($message);
                        sendEmail($data);
                    }
                    // send sms to invited user
                    $this->response([
                        'status' => 200,
                        'message' => lang('invitation_sent')
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK);
                }
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function categories_get()
    {
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $post_data = $this->post_data;
        $categories_array = array();
        $where = 'categories.IsActive = 1 AND categories.ParentID = 0 AND system_languages.ShortCode = "' . $this->language . '"';
        $categories = $this->Category_model->getJoinedData(true, 'CategoryID', $where, 'ASC', 'categories.CategoryID', true);
        if ($categories) {
            $i = 0;
            foreach ($categories as $category) {
                $category['Level'] = 1;
                $categories_array[$i] = $category;
                $where = 'categories.IsActive = 1 AND categories.ParentID = ' . $category['CategoryID'] . ' AND system_languages.ShortCode = "' . $this->language . '"';
                $sub_categories = $this->Category_model->getJoinedData(true, 'CategoryID', $where, 'ASC', 'categories.CategoryID', true);
                if ($sub_categories) {
                    $j = 0;
                    foreach ($sub_categories as $sub_category) {
                        $sub_category['Level'] = 2;
                        $categories_array[$i]['SubCategories'][$j] = $sub_category;
                        $where = 'categories.IsActive = 1 AND categories.ParentID = ' . $sub_category['CategoryID'] . ' AND system_languages.ShortCode = "' . $this->language . '"';
                        $sub_sub_categories = $this->Category_model->getJoinedData(true, 'CategoryID', $where, 'ASC', 'categories.CategoryID', true);
                        if ($sub_sub_categories) {
                            $k = 0;
                            foreach ($sub_sub_categories as $sub_sub_category) {
                                $sub_sub_category['Level'] = 3;
                                $categories_array[$i]['SubCategories'][$j]['SubSubCategories'][$k] = $sub_sub_category;
                                $k++;
                            }
                        } else {
                            $categories_array[$i]['SubCategories'][$j]['SubSubCategories'] = array();
                        }
                        $j++;
                    }
                } else {
                    $categories_array[$i]['SubCategories'] = array();
                }
                $i++;
            }
        }
        $this->response([
            'status' => 200,
            'categories' => $categories_array
        ], REST_Controller::HTTP_OK);


    }

    public function getCities_get()
    {
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $post_data = $this->post_data;
        $where = 'cities.IsActive = 1 AND system_languages.ShortCode = "' . $this->language . '"';

        $cities = $this->City_model->getJoinedData(true, 'CityID', $where, 'ASC', 'cities_text.Title', true);

        $this->response([
            'status' => 200,
            'cities' => $cities
        ], REST_Controller::HTTP_OK);

    }

    public function getDistricts_get(){
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $post_data = $this->post_data;
        $districts = $this->District_model->getAllJoinedData(true, 'DistrictID', $this->language, "districts.IsActive = 1 AND districts.CityID = ".$post_data['CityID'], 'ASC', 'SortOrder');
         $this->response([
            'status' => 200,
            'districts' => $districts
        ], REST_Controller::HTTP_OK);
    }


    public function packages_get()
    {
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $post_data = $this->post_data;
        $where = 'packages.IsActive = 1 AND system_languages.ShortCode = "EN"';

        $packages = $this->Package_model->getJoinedData(true, 'PackageID', $where);

        $this->response([
            'status' => 200,
            'packages' => $packages
        ], REST_Controller::HTTP_OK);

    }

    public function cancellation_reasons_get()
    {
        $post_data = $this->post_data;
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }

        if(isset($post_data['Type'])){
            $type = $post_data['Type'];
        }else{
            $type = 'Buyer';
        }
        $cancellation_reasons = $this->Order_cancellation_reason_model->getMultipleRows(array('IsActive' => 1,'Type' => $type));
        $this->response([
            'status' => 200,
            'cancellation_reasons' => $cancellation_reasons
        ], REST_Controller::HTTP_OK);

    }


    public function getUserDetail_get(){
         $this->_isUserAuthorized(true);
         $post_data = $this->post_data; // UserID, OtherUserID (Optional, only send when accessing other user profile)
         if (!empty($post_data)) {

             if (isset($post_data['OtherUserID']) && $post_data['OtherUserID'] > 0) {
                $UserIDForData = $post_data['UserID'];
             }else{
                $UserIDForData = $post_data['UserID'];

             }

            $user_info = $this->User_model->getUserInfo('users.UserID =' . $UserIDForData, $this->language);
            if (!empty($user_info)) {
                $user_info['AuthToken'] = $this->genrateToken($post_data['UserID']);
                    $this->response([
                        'status' => 200,
                        'user_info' => NullToEmpty($user_info)
                    ], REST_Controller::HTTP_OK);
            }else{
                 $this->response([
                        'status' => 402,
                        'message' => lang('user_not_found_with_these_login_details')
                    ], REST_Controller::HTTP_OK);

            }
         } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getUserDetail_bk_get()
    {
        $this->_isUserAuthorized(true);
        $post_data = $this->post_data; // UserID, OtherUserID (Optional, only send when accessing other user profile)
        if (!empty($post_data)) {
            // send this OtherUserID if viewing other user's profile
            if (isset($post_data['OtherUserID']) && $post_data['OtherUserID'] > 0) {

                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $post_data['OtherUserID'],$post_data['Type'],$post_data['UserType']);
                if ($IsBlocked) {
                   $this->response([
                        'status' => 402,
                        'message' => lang('you_cannot_do')
                    ], REST_Controller::HTTP_OK);
                }
                unset($post_data['UserType']);

                if($post_data['UserID'] != $post_data['OtherUserID']){

                     $existing_user = $this->User_model->get($post_data['OtherUserID'], true, 'UserID');

               

                    if($existing_user['LastState'] == 'booth' && $existing_user['PackageExpiry'] < Date('Y-m-d')){
                        $this->response([
                                'status' => 402,
                                'message' => lang('please_update_your_package')
                            ], REST_Controller::HTTP_OK);

                    }

                }
                $check_is_follower['Follower'] = $post_data['OtherUserID'];
                $check_is_follower['Following'] = $post_data['UserID'];
                $check_is_follower['Type'] = $post_data['Type'];
                $is_follower = $this->Follower_model->getWithMultipleFields($check_is_follower);
                if ($is_follower) {
                    $Follower = 1;
                } else {
                    $Follower = 0;
                }

                $check_is_following['Follower'] = $post_data['UserID'];
                $check_is_following['Following'] = $post_data['OtherUserID'];
                $check_is_following['Type'] = $post_data['Type'];
                $is_following = $this->Follower_model->getWithMultipleFields($check_is_following);
                if ($is_following) {
                    $Following = 1;
                } else {
                    $Following = 0;
                }

                $UserIDForData = $post_data['OtherUserID'];

            } else {
                $UserIDForData = $post_data['UserID'];
            }


            if (isset($UserIDForData) && $UserIDForData > 0) {
                $result = $this->User_model->getUserInfo('users.UserID =' . $UserIDForData, $this->language);
                //print_rm($result);
                if (!empty($result)) {
                    $user_info = $result;
                    if (isset($post_data['OtherUserID'])) {
                        $user_info['Follower'] = $Follower;
                        $user_info['Following'] = $Following;
                    }
                    $user_info['ProfileCustomization'] = $this->User_customization_model->getUserProfileCustomization($UserIDForData);
                    $user_info['BoothFollowersCount'] = $this->Follower_model->getRowsCount(array('Following' => $UserIDForData, 'Type' => 'booth'));
                    $user_info['UserFollowersCount'] = $this->Follower_model->getRowsCount(array('Following' => $UserIDForData, 'Type' => 'user'));
                    $user_info['BoothFollowingCount'] = $this->Follower_model->getRowsCount(array('Follower' => $UserIDForData, 'Type' => 'booth'));
                    $user_info['UserFollowingCount'] = $this->Follower_model->getRowsCount(array('Follower' => $UserIDForData, 'Type' => 'user'));
                    $user_info['BoothAverageRating'] = boothAverageRating($UserIDForData);
                    $user_info['UserAverageRating'] = userAverageRating($UserIDForData);
                    $user_info['UserFollowedBooths'] = $this->Follower_model->getUserFollowedBooths($UserIDForData);
                    $user_info['BoothCategoriesCount'] = $this->User_category_model->getRowsCount(array('UserID' => $UserIDForData, 'Type' => 'booth'));
                    $user_info['UserCategoriesCount'] = $this->User_category_model->getRowsCount(array('UserID' => $UserIDForData, 'Type' => 'user'));
                    $user_info['UserSelectedCategories'] = $this->User_category_model->getSelectedCategories($user_info['UserID'], 'user', $this->language);
                    $user_info['BoothSelectedCategories'] = $this->User_category_model->getSelectedCategories($user_info['UserID'], 'booth', $this->language);
                    $user_info['IsExpire'] = 0;
                    if($user_info['PackageExpiry'] < Date('Y-m-d')){
                        $user_info['IsExpire'] = 1;
                    }
                    // $user_info['UserFollowingBooths'] = $this->Follower_model->getFollowing($user_info['UserID'], 'booth', false, false, false, $this->language);
                    // $user_info['UserFollowingUsers'] = $this->Follower_model->getFollowing($user_info['UserID'], 'user', false, false, false, $this->language);
                    $unread_msg_count = $this->Chat_message_model->getRowsCount(array('ReceiverID' => $user_info['UserID'], 'IsReadByReceiver' => 'no','UserType' => 'user'),false,false,'ChatID',true);
                    $user_info['HasUnreadMessage'] = ($unread_msg_count > 0 ? 'yes' : 'no');
                    $user_info['UnreadMessageCount'] = $unread_msg_count;
                    $user_info['UserUnreadNotificationCount'] = $this->User_notification_model->getRowsCount(array('LoggedInUserID' => $user_info['UserID'], 'IsRead' => 0,'UserType' => 'user'));
                     $user_info['BoothUnreadNotificationCount'] = $this->User_notification_model->getRowsCount(array('LoggedInUserID' => $user_info['UserID'], 'IsRead' => 0,'UserType' => 'booth'));

                    $where_for_order_count = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND orders_requests.BoothID = ".$user_info['UserID']; 

                    $user_info['BoothOrderCount'] = $this->Order_model->getRowsCount($where_for_order_count,'orders_requests','OrderID');

                    $where_for_order_count = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND orders.UserID = ".$user_info['UserID']; 

                    $user_info['UserOrderCount'] = $this->Order_model->getRowsCount($where_for_order_count,'orders_requests','OrderID');

                    

                    if (isset($post_data['OtherUserID']) && $post_data['OtherUserID'] > 0) {
                        $BoothCouponCheck = $this->Coupon_model->getRowsCount(array('UserID' => $post_data['OtherUserID']));
                        if ($BoothCouponCheck > 0) {
                            $user_info['BoothCouponCheck'] = 1;
                        } else {
                            $user_info['BoothCouponCheck'] = 0;
                        }
                    } else {
                        $BoothCouponCheck = $this->Coupon_model->getRowsCount(array('UserID' => $post_data['UserID']));
                        if ($BoothCouponCheck > 0) {
                            $user_info['BoothCouponCheck'] = 1;
                        } else {
                            $user_info['BoothCouponCheck'] = 0;
                        }
                    }


                    $unread_msg_count_booth = $this->Chat_message_model->getRowsCount(array('ReceiverID' => $user_info['UserID'], 'IsReadByReceiver' => 'no','UserType' => 'booth'),false,false,'ChatID',true);
                    $user_info['BoothHasUnreadMessage'] = ($unread_msg_count_booth > 0 ? 'yes' : 'no');
                    $user_info['BoothUnreadMessageCount'] = $unread_msg_count_booth;

                    $cart_products = $this->Temp_order_model->getJoinedDataWithOutText(false, 'ProductID', 'products', 'users.PackageExpiry >= CURDATE() AND temp_orders.UserID = ' . $user_info['UserID'] . '',true);

                    $user_info['CartCount'] = Count($cart_products);
                    $user_info['AuthToken'] = $this->genrateToken($post_data['UserID']);
                    $this->response([
                        'status' => 200,
                        'user_info' => NullToEmpty($user_info)
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('user_not_found')
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('user_not_found')
                ], REST_Controller::HTTP_OK);
            }

        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function search_post()
    {
        $post_data = $this->post_data; // UserID (Optional), Type (Booths, Products, Questions), Keyword (To Search By)
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        if (!empty($post_data)) {
            if (isset($post_data['Type'])) {
                $search_response = array();
                $keyword = $post_data['Keyword'];
                if ($post_data['Type'] === 'Booths') {
                    $where = "(users_text.BoothName LIKE '%$keyword%' OR users.BoothUserName LIKE '%$keyword%' OR categories_text.Title LIKE '%$keyword%') AND users.PackageExpiry >= CURDATE()";

                    
                    $sort_by = 'users.UserID';
                    $sort_as = 'DESC';
                    if(isset($post_data['SortBy'])){
                            $sort_by = $post_data['SortBy'];
                    }
                    if(isset($post_data['SortAs'])){
                            $sort_as = $post_data['SortAs'];
                    }

                    if(isset($post_data['CategoryID'])){
                        $where .= ' AND user_categories.CategoryID = '.$post_data['CategoryID'];
                    }

                    if(isset($post_data['CityID'])){
                        $where .= ' AND cities.CityID = '.$post_data['CityID'];
                    }

                     $where .= ' AND users.BoothUserName != ""';

                    $search_response = $this->User_model->getUsers($where, $this->language,$sort_by,$sort_as,false,0,$category_join = true);
                    if (!empty($search_response)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $i = 0;
                            foreach ($search_response as $key => $value) {
                                $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID, 'booth');
                                if ($IsBlockedAsBooth) {
                                    continue;
                                }
                                $my_arr[$i] = $value;
                                $i++;
                            }
                            $search_response = $my_arr;
                        }
                    }

                    if (!empty($search_response)) {
                        $i = 0;
                        foreach ($search_response as $value) {
                            $user_categories_arr = array();
                            $user_categories = $this->User_category_model->getSelectedCategories($value->UserID, 'booth', $this->language);
                            if ($user_categories) {
                                foreach ($user_categories as $user_category) {
                                    $user_categories_arr[] = $user_category->Title;
                                }
                            }
                            $search_response[$i]->SelectedCategories = (!empty($user_categories_arr) ? implode(',', $user_categories_arr) : '');
                            $i++;
                        }
                    }

                } elseif ($post_data['Type'] === 'Products') {
                    $where = "(products_text.Title LIKE '%$keyword%' OR users_text.BoothName LIKE '%$keyword%' OR users.BoothUserName LIKE '%$keyword%' OR ssct.Title LIKE '%$keyword%' OR sct.Title LIKE '%$keyword%' OR ct.Title LIKE '%$keyword%') AND  users.PackageExpiry >= CURDATE()";
                     
                    $sort_by = 'products_text.Title';
                    $sort_as = 'ASC';
                    if(isset($post_data['SortBy'])){
                            $sort_by = $post_data['SortBy'];
                    }
                    if(isset($post_data['SortAs'])){
                            $sort_as = $post_data['SortAs'];
                    }

                    if(isset($post_data['CategoryID'])){
                        $where .= ' AND products.CategoryID = '.$post_data['CategoryID'];
                    }

                    if(isset($post_data['CityID'])){
                        $where .= ' AND users.CityID = '.$post_data['CityID'];//checking booth city
                    }


                    if(isset($post_data['CityID'])){
                        $where .= ' AND cities.CityID = '.$post_data['CityID'];
                    }
                     $where .= ' AND products.Hide = 0';

                    $search_response = $this->Product_model->getProducts($where, $this->language,false,0,$sort_by,$sort_as);
                    if (!empty($search_response)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $i = 0;
                            foreach ($search_response as $key => $value) {
                                $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID, 'booth');
                                if ($IsBlockedAsBooth) {
                                    continue;
                                }

                                $check_wishlist = array();
                                $check_wishlist['UserID'] = $post_data['UserID'];
                                $check_wishlist['ProductID'] = $value->ProductID;
                                $check_wishlist = $this->User_wishlist_model->getWithMultipleFields($check_wishlist);
                                if ($check_wishlist) {
                                    $value->IsInWishlist = '1';
                                } else {
                                    $value->IsInWishlist = '0';
                                }
                                $my_arr[$i] = $value;
                                $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $value->ProductID));
                                $my_arr[$i]->ProductImages = $product_images ? $product_images : array();
                                $my_arr[$i]->ProductAverageRating = productAverageRating($value->ProductID);
                                $my_arr[$i]->ProductRatingsCount = productAverageRating($value->ProductID, true);
                                $i++;
                            }
                            $search_response = $my_arr;
                        }
                    }
                } elseif ($post_data['Type'] === 'Questions') {
                    $where = "(questions.QuestionDescription LIKE '%$keyword%' OR users_text.FullName LIKE '%$keyword%' OR users.UserName LIKE '%$keyword%' OR cities_text.Title LIKE '%$keyword%' OR sct.Title LIKE '%$keyword%' OR ct.Title LIKE '%$keyword%') AND users.PackageExpiry >= CURDATE()";
                    $search_response = $this->Question_model->getQuestions($where, $this->language);
                    if (!empty($search_response)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $i = 0;
                            foreach ($search_response as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value['UserID']);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$i] = $value;
                                $question_images = $this->Question_image_model->getMultipleRows(array('QuestionID' => $value['QuestionID']));
                                $my_arr[$i]['QuestionImages'] = $question_images ? $question_images : array();
                                $i++;
                            }
                            $search_response = $my_arr;
                        }
                    }

                    if (!empty($search_response)) {
                        $i = 0;
                        foreach ($search_response as $question) {
                            $comments = $this->Question_comment_model->getQuestionComments("question_comments.QuestionID = " . $question['QuestionID'], $this->language);
                            $CommentCount = 0;
                            if (!empty($comments)) {
                                if (isset($post_data['UserID'])) {
                                    $my_arr = array();
                                    $c_count = 0;
                                    foreach ($comments as $key => $value) {
                                        $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                        if ($IsBlocked) {
                                            continue;
                                        }
                                        $my_arr[$c_count] = $value;
                                        $c_count++;
                                    }
                                    $comments = $my_arr;
                                }
                                $CommentCount = count($comments);
                            }
                            $search_response[$i]['CommentCount'] = (string)$CommentCount;
                            $i++;
                        }
                    }

                } elseif ($post_data['Type'] === 'People') {
                    $where = "(users_text.FullName LIKE '%$keyword%' OR users.UserName LIKE '%$keyword%'  OR categories_text.Title LIKE '%$keyword%')";
                    $sort_by = 'users.UserID';
                    $sort_as = 'DESC';
                    if(isset($post_data['SortBy'])){
                            $sort_by = $post_data['SortBy'];
                    }
                    if(isset($post_data['SortAs'])){
                            $sort_as = $post_data['SortAs'];
                    }

                    if(isset($post_data['CategoryID'])){
                        $where .= ' AND user_categories.CategoryID = '.$post_data['CategoryID'];
                    }


                    if(isset($post_data['CityID'])){
                        $where .= ' AND cities.CityID = '.$post_data['CityID'];
                    }
                     $where .= ' AND users.UserName != ""';

                    $search_response = $this->User_model->getUsers($where, $this->language,$sort_by,$sort_as,false,0,$category_join = true);
                    if (!empty($search_response)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $i = 0;
                            foreach ($search_response as $key => $value) {
                                $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID, 'user');
                                if ($IsBlockedAsBooth) {
                                    continue;
                                }
                                $my_arr[$i] = $value;
                                $i++;
                            }
                            $search_response = $my_arr;
                        }
                    }

                    if (!empty($search_response)) {
                        $i = 0;
                        foreach ($search_response as $value) {
                            $user_categories_arr = array();
                            $user_categories = $this->User_category_model->getSelectedCategories($value->UserID, 'user', $this->language);
                            if ($user_categories) {
                                foreach ($user_categories as $user_category) {
                                    $user_categories_arr[] = $user_category->Title;
                                }
                            }
                            $search_response[$i]->SelectedCategories = (!empty($user_categories_arr) ? implode(',', $user_categories_arr) : '');
                            $i++;
                        }
                    }
                }
                $this->response([
                    'status' => 200,
                    'data' => $search_response
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function checkCity_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // PlaceID, UserID
        $user_info = array();
        if (!empty($post_data)) {
            $city = $this->City_model->get($post_data['PlaceID'], false, 'CityPlaceID');
            if ($city) {
                $this->response([
                    'status' => 200,
                    'message' => 'City exists'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => 'City not found'
                ], REST_Controller::HTTP_OK);
            }

        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getPageDetail_get()
    {
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $post_data = $this->post_data;
        $page_info = array();
        $page_info_arr = array();
        if (!empty($post_data)) {


            $where = ' AND pages.CompanyID = '.$this->CompanyID.' AND pages.Type = "'.$post_data['Type'].'"';


            if (isset($post_data['PageID'])){
              $where .= " AND pages.PageID = " . $post_data['PageID'];
            }

            $result = $this->Page_model->getJoinedData(true, 'PageID', 'system_languages.ShortCode = "'.$this->language.'"'.$where.'');
            //$site_settings = $this->Site_setting_model->get(1, false, 'SiteSettingID');
            //echo $this->db->last_query();exit;

            if (!empty($result)) {
                $page_info = $result;
                // dump($page_info);
                if (isset($post_data['Multiple']) && $post_data['Multiple'] == TRUE) {
                    // there are 0,1 indexes in arrays when using Multiple true so removing them by loop
                    foreach ($page_info as $key => $value) {
                        $page_info_arr[$value['PageID']] = $value;
                        //$page_info_arr[$value['PageID']]['Description'] = str_replace(array("\n\r", "\n", "\r"), '', $value['Description']);
                    }
                    $page_info = $page_info_arr;
                } else {
                    $page_info = $page_info[0];
                    //$page_info['Description'] = str_replace(array("\n\r", "\n", "\r"), '', $page_info['Description']);
                }
            }

            $this->response([
                'status' => 200,
                'page_data' => $page_info/*,
                'open_time' => $site_settings->OpenTime,
                'close_time' => $site_settings->CloseTime,
                'loyalty_factor' => $site_settings->LoyaltyFactor*/
            ], REST_Controller::HTTP_OK);

        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }



    public function getSiteSettings_get()
    {
         $site_settings = $this->Site_setting_model->get(1, false, 'SiteSettingID');

            $this->response([
                'status' => 200,
                'page_data' => $site_settings
            ], REST_Controller::HTTP_OK);

    }

    public function giveFeedback_post()
    {
        $post_data = $this->post_data; // FullName, Email, MobileNo, Message
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $return_message = lang('feedback_sent_successfully');
        if (!empty($post_data)) {
            unset($post_data['UserID']);
            $post_data['CreatedAt'] = time();
            if(isset($post_data['OrderNumber']) && $post_data['OrderNumber'] != ''){
                $check_already = $this->Feedback_model->get($post_data['OrderNumber'],true,'OrderNumber');
                if($check_already){
                        $this->response([
                        'status' => 402,
                        'message' => lang('already_complained')
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                        exit;
                }

                $return_message = 'Complaint against this order number sent successfully. You will receive an email with 24 hours.';


            }
            $inserted_id = $this->Feedback_model->save($post_data);
            if ($inserted_id > 0) {
                $this->response([
                    'status' => 200,
                    'message' => $return_message
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function logout_get()
    {
        $post_data = $this->post_data; // UserID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $this->User_model->update(array('OnlineStatus' => 'Offline', 'DeviceToken' => ''), array('UserID' => $post_data['UserID']));
            $this->response([
                'status' => 200,
                'message' => lang('user_logged_out_successfully')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getProducts_get()
    {
        $post_data = $this->post_data; // UserID, OtherUserID (Optional) CategoryID (This will be Sub-Sub CategoryID), OutOfStock, IsActive (All these are optional)
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }

        $limit = 15;
        if(!isset($post_data['PageNo'])){
            $post_data['PageNo'] = 1;
        }
        if($post_data['PageNo'] == 1){
            $start = 0;
        }else{
            $start = $limit * $post_data['PageNo'];
        }
        

        // send this OtherUserID if viewing other user's profile
        
        $where = "products.IsActive = 1 AND products.Hide = 0 AND products.CompanyID = ".$this->CompanyID;

        

        if (isset($post_data['CategoryID']) && $post_data['CategoryID'] > 0) {// CategoryID is actually sub sub category here
            $where .= " AND products.CategoryID = ".$post_data['CategoryID'];
        }


        if (isset($post_data['SubCategoryID']) && $post_data['SubCategoryID'] > 0) {
            $where .= " AND products.SubCategoryID = ".$post_data['SubCategoryID'];
        }
        if (isset($post_data['OutOfStock']) && $post_data['OutOfStock'] > 0) {
            $where .= " AND products.OutOfStock = " . $post_data['OutOfStock'];
        }

        if (isset($post_data['BrandID']) && $post_data['BrandID'] > 0) {
            $where .= " AND products.BrandID = " . $post_data['BrandID'];
        }

        if (isset($post_data['ProductID']) && $post_data['ProductID'] > 0) {
            $where .= " AND products.ProductID = " . $post_data['ProductID'];
        }

        if (isset($post_data['IsFeatured'])) {
            $where .= " AND products.IsFeatured = " . $post_data['IsFeatured'];
        }

        if (isset($post_data['Search'])) {
            $where .= " AND products_text.Title LIKE '%".$post_data['Search']."%'";
        }
         
        
        $products = $this->Product_model->getProducts($where, $this->language,$limit,$start);
        //echo $this->db->last_query();exit;
        $products_arr = array();
        if ($products) {
            $i = 0;
            foreach ($products as $product) {
                  $attributes_array = array();
                 


                if($product->VariantData != ''){

                   

                    $variants_json = json_decode($product->VariantData);
                     
                    $variants = explode(',',$variants_json->VariantID);
 
                    $attributes = $variants_json->Attribute;
                    


                    
                    $j = 0;
                     
                    foreach ($attributes as $key => $value) {

                        $att_data = $this->Variant_model->getJoinedData(false, 'VariantID', "variants.VariantID = " . $key . " AND system_languages.ShortCode = '" . $this->language . "'",'ASC','variants_text.Title',true)[0];
                      
                        $attributes_array[$j]['AttributeID'] = $key;
                        $attributes_array[$j]['VariantID'] = $att_data->ParentID;
                        $attributes_array[$j]['VariantTitle'] = variantName($att_data->ParentID,$this->language);
                        $attributes_array[$j]['AttributeTitle'] = $att_data->Title;
                        $attributes_array[$j]['Price'] = $value;
                        $j++;
                        
                    }

                   

                    $product->VariantData = $attributes_array;

                }else{
                     $product->VariantData = $attributes_array;
                }
                $products_arr[$i] = $product;

                $product_images = $this->Site_images_model->getMultipleRows(array('FileID' => $product->ProductID,'ImageType' => 'product'));
                /*$comments = $this->Product_comment_model->getProductComments("product_comments.ProductID = " . $product->ProductID, $this->language);
                $CommentCount = 0;
                if (!empty($comments)) {
                    if (isset($post_data['UserID'])) {
                        $my_arr = array();
                        $c_count = 0;
                        foreach ($comments as $key => $value) {
                            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                            if ($IsBlocked) {
                                continue;
                            }
                            $my_arr[$c_count] = $value;
                            $c_count++;
                        }
                        $comments = $my_arr;
                    }
                    $CommentCount = count($comments);
                }

                $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $product->ProductID);
                $LikesCount = 0;
                if (!empty($likes)) {
                    if (isset($post_data['UserID'])) {
                        $my_arr = array();
                        $l_count = 0;
                        foreach ($likes as $key => $value) {
                            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                            if ($IsBlocked) {
                                continue;
                            }
                            $my_arr[$l_count] = $value;
                            $l_count++;
                        }
                        $likes = $my_arr;
                    }
                    $LikesCount = count($likes);
                }

                $check_already_liked['UserID'] = $post_data['UserID'];
                $check_already_liked['ProductID'] = $product->ProductID;
                $check_like = $this->Product_like_model->getWithMultipleFields($check_already_liked);
                if ($check_like) {
                    $products_arr[$i]->IsLiked = '1';
                } else {
                    $products_arr[$i]->IsLiked = '0';
                }*/

                $products_arr[$i]->ProductImages = $product_images ? $product_images : array();
               // $products_arr[$i]->LikesCount = (string)$LikesCount;
                //$products_arr[$i]->CommentCount = (string)$CommentCount;
                $i++;
            }
        }
        $this->response([
            'status' => 200,
            'products' => $products_arr
        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }

    public function getPromotions_get(){
        $this->_isUserAuthorized(true);
        $where = "promotions.IsActive = 1 AND promotions.Hide = 0 AND promotions.CompanyID = ".$this->CompanyID;
        $promoted_products = $this->Promotion_model->getPromotions($where, $this->language);
        $this->response([
            'status' => 200,
            'promotions' => $promoted_products
        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }

    public function promotedProducts_post()
    {
        $this->_isUserAuthorized(true);
         $where = "products.IsActive = 1 AND products.Hide = 0 AND users.PackageExpiry >= CURDATE()";
        $promoted_products = $this->Promoted_product_model->getPromotedProducts($where, $this->language);
        $this->response([
            'status' => 200,
            'promoted_products' => $promoted_products
        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }

    public function productDetail_get()
    {
        $post_data = $this->post_data; // UserID, ProductID
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }




        $where = "users.PackageExpiry >= CURDATE() AND products.ProductID = " . $post_data['ProductID'];
        $product = $this->Product_model->getProducts($where, $this->language);
        if (!empty($product)) {

             $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $product[0]->UserID,'booth',$post_data['UserType']);// here we are checking my type
            if ($IsBlocked) {
                 $this->response([
                        'status' => 402,
                        'message' => lang('you_cannot_do')
                    ], REST_Controller::HTTP_OK);

                 exit;
            }

            unset($post_data['UserType']);

            // product viewed by user logic here
            if (isset($post_data['UserID'])) {
                $IsProductViewed = $this->Product_view_model->getWithMultipleFields($post_data);
                if (!$IsProductViewed) {
                    $product_viewed_data['ProductID'] = $post_data['ProductID'];
                    $product_viewed_data['UserID'] = $post_data['UserID'];
                    $product_viewed_data['ViewedAt'] = time();
                    $this->Product_view_model->save($product_viewed_data);
                }
            }

            $check_if_product_promoted['ProductID'] = $post_data['ProductID'];
            $check_if_product_promoted['PromotionExpiresAt >'] = time();
            $IsProductPromoted = $this->Promoted_product_model->getWithMultipleFields($check_if_product_promoted);
            $product = $product[0];
            $product_images_m = $this->Product_image_model->getMultipleRows(array('ProductID' => $post_data['ProductID']));
            $comments = $this->Product_comment_model->getProductComments("product_comments.ProductID = " . $post_data['ProductID'], $this->language);
            $CommentCount = 0;
            if (!empty($comments)) {
                if (isset($post_data['UserID'])) {
                    $my_arr = array();
                    $c_count = 0;
                    foreach ($comments as $key => $value) {
                        $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                        if ($IsBlocked) {
                            continue;
                        }
                        $my_arr[$c_count] = $value;
                        $c_count++;
                    }
                    $comments = $my_arr;
                }
                $CommentCount = count($comments);
            }

            $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $post_data['ProductID']);
            $LikesCount = 0;
            if (!empty($likes)) {
                if (isset($post_data['UserID'])) {
                    $my_arr = array();
                    $l_count = 0;
                    foreach ($likes as $key => $value) {
                        $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                        if ($IsBlocked) {
                            continue;
                        }
                        $my_arr[$l_count] = $value;
                        $l_count++;
                    }
                    $likes = $my_arr;
                }
                $LikesCount = count($likes);
            }

            $product_views = $this->Product_view_model->getMultipleRows(array('ProductID' => $post_data['ProductID']));
            $ViewsCount = 0;
            if (!empty($product_views)) {
                if (isset($post_data['UserID'])) {
                    $my_arr = array();
                    $v_count = 0;
                    foreach ($product_views as $key => $value) {
                        $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                        if ($IsBlocked) {
                            continue;
                        }
                        $my_arr[$v_count] = $value;
                        $v_count++;
                    }
                    $product_views = $my_arr;
                }
                $ViewsCount = count($product_views);
            }

            if (isset($post_data['UserID'])) {
                $check_already_liked['UserID'] = $post_data['UserID'];
                $check_already_liked['ProductID'] = $post_data['ProductID'];
                $check_like = $this->Product_like_model->getWithMultipleFields($check_already_liked);
                if ($check_like) {
                    $product->IsLiked = '1';
                } else {
                    $product->IsLiked = '0';
                }
            } else {
                $product->IsLiked = '0';
            }
            if (isset($post_data['UserID'])) {
                $check_wishlist['UserID'] = $post_data['UserID'];
                $check_wishlist['ProductID'] = $post_data['ProductID'];
                $check_wishlist = $this->User_wishlist_model->getWithMultipleFields($check_wishlist);
                if ($check_wishlist) {
                    $product->IsInWishlist = '1';
                } else {
                    $product->IsInWishlist = '0';
                }
            } else {
                $product->IsInWishlist = '0';
            }

            $product_comments = $this->Product_comment_model->getProductComments('product_comments.ProductID = ' . $post_data['ProductID'], $this->language, 'product_comments.ProductCommentID', 'DESC', 3, 0);

            if (isset($post_data['UserID'])) {
                if (count($product_comments) > 0) {
                    $my_arr = array();
                    $i = 0;
                    foreach ($product_comments as $key => $value) {
                        $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                        if ($IsBlockedAsBooth) {
                            continue;
                        }
                        $my_arr[$i] = $value;
                        $i++;
                    }
                    $product_comments = $my_arr;
                }
            }

            if (!empty($product_comments)) {
                $i = 0;
                foreach ($product_comments as $comment) {
                    $user_ids = explode(',', $comment->MentionedUserID);
                    $user_names = explode(',', $comment->MentionedUserName);
                    $user_types = explode(',', $comment->MentionedUserType);
                    $mentioned_users_info = array();
                    $j = 0;
                    foreach ($user_ids as $id) {
                        $userInfoForComment = $this->User_model->getUsers("users.UserID = " . $id, $this->language);
                        if (!empty($userInfoForComment)) {
                            $userInfoForComment = $userInfoForComment[0];
                            $mentioned_users_info[$j]['UserID'] = $userInfoForComment->UserID;
                            $mentioned_users_info[$j]['FullName'] = $userInfoForComment->FullName;
                            $mentioned_users_info[$j]['MentionedName'] = $user_names[$j];
                            $mentioned_users_info[$j]['MentionedUserType'] = $user_types[$j];
                        }
                        $j++;
                    }

                    if (isset($mentioned_users_info[0]) && !empty($mentioned_users_info[0])) {
                        $product_comments[$i]->MentionedUsersInfo = $mentioned_users_info;
                    } else {
                        $product_comments[$i]->MentionedUsersInfo = array();
                    }

                    $i++;
                }
            }

            $similar_products = $this->Product_model->getSimilarProducts("products.CategoryID = " . $product->CategoryID, $this->language, 'DESC', 10, 0);
            if (isset($post_data['UserID'])) {
                if (count($similar_products) > 0) {
                    $my_arr = array();
                    $i = 0;
                    foreach ($similar_products as $key => $value) {
                        if($value->ProductID != $post_data['ProductID']){
                            $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                            if ($IsBlockedAsBooth) {
                                continue;
                            }
                            $my_arr[$i] = $value;
                            $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $value->ProductID));
                            $my_arr[$i]->ProductImage = $product_images ? $product_images[0]->ProductCompressedImage : '';
                            $my_arr[$i]->ProductAverageRating = productAverageRating($value->ProductID);
                            $my_arr[$i]->ProductRatingsCount = productAverageRating($value->ProductID, true);

                            $i++;
                        }
                    }
                    $similar_products = $my_arr;
                }
            }

            $product->Comments = !empty($product_comments) ? $product_comments : array();
            $product->IsPromoted = $IsProductPromoted ? '1' : '0';
            $product->ProductImages = $product_images_m ? $product_images_m : array();
            $product->LikesCount = (string)$LikesCount;
            $product->CommentCount = (string)$CommentCount;
            $product->ViewsCount = (string)$ViewsCount;
            $product->ProductAverageRating = productAverageRating($post_data['ProductID']);
            $product->ProductRatingsCount = productAverageRating($post_data['ProductID'], true);
            $product->OrderCount = $this->Order_item_model->getProductOrderCount($post_data['ProductID']);
            $this->response([
                'status' => 200,
                'product' => $product,
                'similar_products' => $similar_products
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('please_update_your_package')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getProductRatings_post()
    {
        $post_data = $this->post_data; // UserID, ProductID
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }

        if (!empty($post_data)) {
            $product_ratings = productRatings($post_data['ProductID']);
            $product_rating['ProductAverageRating'] = productAverageRating($post_data['ProductID']);
            $product_rating['ProductRatingsCount'] = productAverageRating($post_data['ProductID'], true);
            $product_rating['ProductRated1Count'] = $product_ratings['rating_1'];
            $product_rating['ProductRated2Count'] = $product_ratings['rating_2'];
            $product_rating['ProductRated3Count'] = $product_ratings['rating_3'];
            $product_rating['ProductRated4Count'] = $product_ratings['rating_4'];
            $product_rating['ProductRated5Count'] = $product_ratings['rating_5'];
            $product_ratings = $this->Product_rating_model->getProductRatings("product_ratings.ProductID = " . $post_data['ProductID']);
            $product_rating['ProductRatingsByUsers'] = $product_ratings;
            $this->response([
                'status' => 200,
                'product' => $product_rating
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getBoothRatings_post()
    {
        $post_data = $this->post_data; // UserID, BoothID
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }

        if (!empty($post_data)) {
            $booth_ratings = boothRatings($post_data['BoothID']);
            $booth_rating['BoothAverageRating'] = boothAverageRating($post_data['BoothID']);
            $booth_rating['BoothRatingsCount'] = boothAverageRating($post_data['BoothID'], true);
            $booth_rating['BoothRated1Count'] = $booth_ratings['rating_1'];
            $booth_rating['BoothRated2Count'] = $booth_ratings['rating_2'];
            $booth_rating['BoothRated3Count'] = $booth_ratings['rating_3'];
            $booth_rating['BoothRated4Count'] = $booth_ratings['rating_4'];
            $booth_rating['BoothRated5Count'] = $booth_ratings['rating_5'];
            $booth_rating['BoothRatings'] = $this->User_model->getBoothRatings("orders_requests.BoothID = " . $post_data['BoothID']);
            $this->response([
                'status' => 200,
                'booth_rating' => $booth_rating
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function getUserRatings_post()
    {
        $post_data = $this->post_data; // UserID, BoothID
        $UserID = $post_data['UserID'];
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }

        if(isset($post_data['OtherUserID'])){
            $UserID = $post_data['OtherUserID'];
        }

        if (!empty($post_data)) {
            $user_ratings = userRatings($post_data['UserID']);
            $user_rating['UserAverageRating'] = userAverageRating($UserID);
            $user_rating['UserRatingsCount'] = userAverageRating($UserID, true);
            $user_rating['UserRated1Count'] = $user_ratings['rating_1'];
            $user_rating['UserRated2Count'] = $user_ratings['rating_2'];
            $user_rating['UserRated3Count'] = $user_ratings['rating_3'];
            $user_rating['UserRated4Count'] = $user_ratings['rating_4'];
            $user_rating['UserRated5Count'] = $user_ratings['rating_5'];
            $user_rating['UserRatings'] = $this->User_model->getUserRatings("orders.UserID = " . $UserID);
            $this->response([
                'status' => 200,
                'booth_rating' => $user_rating
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function productsRegardingCategories_post() // For Home Tab 1
    {
        $post_data = $this->post_data; // UserID, CategoryID (This will be Sub-Sub CategoryID), OutOfStock, IsActive (All these are optional)
        $random = false;
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized(true);
        } else {
            $this->_isUserAuthorized(true);
            $random = true;
        }

        // $IsGuest = true;
        $user_categories_arr = array();
        $start = 0;
        $limit = 10;

        if (isset($post_data['Start'])) {
            $start = $post_data['Start'];
        }
        $Type = 'user';
        if (isset($post_data['Type'])) {
            $Type = $post_data['Type'];
        }

        $where = "users.PackageExpiry >= CURDATE() AND products.IsActive = 1 AND products.Hide = 0";
        if (isset($post_data['UserID']) && $post_data['UserID'] > 0) {
            $user_categories = $this->User_category_model->getMultipleRows(array('UserID' => $post_data['UserID'], 'Type' => $Type));
            if ($user_categories) {
                foreach ($user_categories as $user_category) {
                    $user_categories_arr[] = $user_category->CategoryID;
                }
            }
            // $where .= " AND user_categories.Type = 'user' AND user_categories.UserID = " . $post_data['UserID'];
            // $IsGuest = false;
        }else{
             $user_categories = $this->Category_model->getAll();
             if ($user_categories) {
                foreach ($user_categories as $user_category) {
                    $user_categories_arr[] = $user_category->CategoryID;
                }
            }
        }

        $questions_arr = array();
        if (!empty($user_categories_arr)) {
            $questions = $this->Question_model->getQuestions("users.PackageExpiry >= CURDATE()", $this->language, $user_categories_arr, 'categories');
            if ($questions) {
                $i = 0;
                foreach ($questions as $question) {
                    $questions_arr[$i] = $question;
                    $questions_arr[$i]['ItemType'] = 'Question';
                    $question_images = $this->Question_image_model->getMultipleRows(array('QuestionID' => $question['QuestionID']));
                    $comments = $this->Question_comment_model->getQuestionComments("question_comments.QuestionID = " . $question['QuestionID'], $this->language);
                    $CommentCount = 0;
                    if (!empty($comments)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $c_count = 0;
                            foreach ($comments as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$c_count] = $value;
                                $c_count++;
                            }
                            $comments = $my_arr;
                        }
                        $CommentCount = count($comments);
                    }
                    $questions_arr[$i]['CommentCount'] = (string)$CommentCount;
                    $questions_arr[$i]['QuestionImages'] = $question_images ? $question_images : array();
                    // $questions_arr[$i]['QuestionComments'] = $question_comments ? $question_comments : array();
                    $i++;
                }
            }
        }


        $where_user_post = '1 = 1 AND users.PackageExpiry >= CURDATE()';



        if (isset($post_data['CategoryID']) && $post_data['CategoryID'] > 0) {
            $where .= " AND products.CategoryID = " . $post_data['CategoryID'];

             $where_user_post .= ' AND user_posts.CategoryID = '.$post_data['CategoryID'];

        }

        if (isset($post_data['SubCategoryID']) && $post_data['SubCategoryID'] > 0) {
              $where_user_post .= ' AND user_posts.SubCategoryID = '.$post_data['SubCategoryID'];
        }



        if (isset($post_data['OutOfStock']) && $post_data['OutOfStock'] > 0) {
            $where .= " AND products.OutOfStock = " . $post_data['OutOfStock'];
        }
        if (isset($post_data['IsActive']) && $post_data['IsActive'] > 0) {
            $where .= " AND products.IsActive = " . $post_data['IsActive'];
        }


        // $where = "products.UserID = " . $post_data['UserID'] . " AND products.CategoryID = " . $post_data['CategoryID'] . " AND OutOfStock = " . $post_data['OutOfStock'] . " AND IsActive = " . $post_data['IsActive'];
        $promoted_products_arr = array();
        if ($start == 0) // only send this array data 1st time when api is called
        {
            if (!empty($user_categories_arr) || true) {// here we don't want product with categories now
               // $where_promoted = "products.Hide = 0 AND promoted_products.PromotionExpiresAt > " . time();
                $where_promoted = false;//"products.Hide = 0";
                /*$promoted_products = $this->Promoted_product_model->getPromotedProductsForUserFollowCategories($where_promoted, $this->language, $user_categories_arr,'categories',$random);*/
                $promoted_products = $this->Promoted_product_model->getPromotedProductsForUserFollowCategories($where_promoted, $this->language, false,'categories',$random);
               // echo $this->db->last_query();exit;
                if ($promoted_products) {
                    $promoted_products = NullToEmpty($promoted_products);
                    $i = 0;
                    foreach ($promoted_products as $promoted_product) {
                        $promoted_products_arr[$i] = $promoted_product;
                        // $promoted_products_arr[$i]['IsPromoted'] = '1';
                        $promoted_products_arr[$i]['IsPromoted'] = ($promoted_product['IsPromotedProduct'] == 1 && $promoted_product['IsPromotionApproved'] == 1 ? 1 : 0);
                        $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $promoted_product['ProductID']));
                        $promoted_products_arr[$i]['ProductImages'] = $product_images ? $product_images : array();
                        $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $promoted_product['ProductID']);
                        $LikesCount = 0;
                        if (!empty($likes)) {
                            if (isset($post_data['UserID'])) {
                                $my_arr = array();
                                $l_count = 0;
                                foreach ($likes as $key => $value) {
                                    $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                    if ($IsBlocked) {
                                        continue;
                                    }
                                    $my_arr[$l_count] = $value;
                                    $l_count++;
                                }
                                $likes = $my_arr;
                            }
                            $LikesCount = count($likes);
                        }
                        $comments = $this->Product_comment_model->getProductComments("product_comments.ProductID = " . $promoted_product['ProductID'], $this->language);
                        $CommentCount = 0;
                        if (!empty($comments)) {
                            if (isset($post_data['UserID'])) {
                                $my_arr = array();
                                $c_count = 0;
                                foreach ($comments as $key => $value) {
                                    $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                    if ($IsBlocked) {
                                        continue;
                                    }
                                    $my_arr[$c_count] = $value;
                                    $c_count++;
                                }
                                $comments = $my_arr;
                            }
                            $CommentCount = count($comments);
                        }

                        $promoted_products_arr[$i]['LikesCount'] = (string)$LikesCount;
                        $promoted_products_arr[$i]['CommentCount'] = (string)$CommentCount;
                        $i++;
                    }
                }
            }
        }

        $products_arr = array();
        if (!empty($user_categories_arr)) {
            $products = $this->Product_model->getProductsForUserFollowCategory($where, $this->language, false, 0, 'products_text.Title', 'ASC', $user_categories_arr,$random);
            


            if ($products) {
                /*usort($products, function ($item1, $item2) {
                    return $item2['IsPromotionApproved'] <=> $item1['IsPromotionApproved'];
                });*/
                $i = 0;
                foreach ($products as $product) {
                    $check_if_product_promoted['ProductID'] = $product['ProductID'];
                    $check_if_product_promoted['PromotionExpiresAt >'] = time();
                    $IsProductPromoted = $this->Promoted_product_model->getWithMultipleFields($check_if_product_promoted);
                    $products_arr[$i] = $product;
                    $products_arr[$i]['ItemType'] = 'Product';

                    if (isset($post_data['UserID'])) {
                        $check_already_liked['UserID'] = $post_data['UserID'];
                        $check_already_liked['ProductID'] = $product['ProductID'];
                        $check_like = $this->Product_like_model->getWithMultipleFields($check_already_liked);
                        if ($check_like) {
                            $products_arr[$i]['IsLiked'] = '1';
                        } else {
                            $products_arr[$i]['IsLiked'] = '0';
                        }
                    } else {
                        $products_arr[$i]['IsLiked'] = '0';
                    }

                    // $products_arr[$i]['IsPromoted'] = $IsProductPromoted ? '1' : '0';
                    $products_arr[$i]['IsPromoted'] = ($product['IsPromotedProduct'] == 1 && $product['IsPromotionApproved'] == 1 ? 1 : 0);
                    $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $product['ProductID']));
                    $products_arr[$i]['ProductImages'] = $product_images ? $product_images : array();
                    $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $product['ProductID']);
                    $LikesCount = 0;
                    if (!empty($likes)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $l_count = 0;
                            foreach ($likes as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$l_count] = $value;
                                $l_count++;
                            }
                            $likes = $my_arr;
                        }
                        $LikesCount = count($likes);
                    }
                    $comments = $this->Product_comment_model->getProductComments("product_comments.ProductID = " . $product['ProductID'], $this->language);
                    $CommentCount = 0;
                    if (!empty($comments)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $c_count = 0;
                            foreach ($comments as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$c_count] = $value;
                                $c_count++;
                            }
                            $comments = $my_arr;
                        }
                        $CommentCount = count($comments);
                    }
                    $products_arr[$i]['LikesCount'] = (string)$LikesCount;
                    $products_arr[$i]['CommentCount'] = (string)$CommentCount;
                    $i++;
                }
            }
        }

        if (isset($post_data['UserID'])) {
            if (count($promoted_products_arr) > 0) {
                $my_arr = array();
                $i = 0;
                foreach ($promoted_products_arr as $key => $value) {
                    $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value['UserID'], 'booth');
                    if (!$IsBlockedAsBooth) {
                        $my_arr[$i] = $value;
                        $i++;
                    }
                }
                $promoted_products_arr = $my_arr;
            }

            if (count($products_arr) > 0) {
                $my_arr = array();
                $i = 0;
                foreach ($products_arr as $key => $value) {
                    $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value['UserID'], 'booth');
                    if (!$IsBlockedAsBooth) {
                        $my_arr[$i] = $value;
                        $i++;
                    }
                }
                $products_arr = $my_arr;
            }

            if (count($questions_arr) > 0) {
                $my_arr = array();
                $i = 0;
                foreach ($questions_arr as $key => $value) {
                    $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value['UserID'],'user');
                    if (!$IsBlocked) {
                        $my_arr[$i] = $value;
                        $i++;
                    }
                }
                $questions_arr = $my_arr;
            }
        }



        $user_post_data = $this->User_post_model->getPosts($where_user_post);
        $return_post_data = array();

        if(!empty($user_post_data)){
            foreach($user_post_data as $user_post){

                $user_post['ItemType'] = 'Post';
                $return_post_data[] = $user_post;


            }
        }



        $response_data = $this->sortProductsQuestions($products_arr, $questions_arr, $start, $limit,$return_post_data);

        if(!empty($response_data)){
            usort($response_data, function ($item1, $item2) {
                return $item2['IsPromotionApproved'] <=> $item1['IsPromotionApproved'];
            });
        }


        $this->response([
            'status' => 200,
            'promoted_products' => $promoted_products_arr,
            'products' => $response_data
        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }

    public function productsRegardingBooth_post() // For Home Tab 2
    {
        $post_data = $this->post_data; // UserID, CategoryID (This will be Sub-Sub CategoryID), OutOfStock, IsActive (All these are optional)
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }

        // $IsGuest = true;
        $user_followed_booths_arr = array();
        $start = 0;
        $limit = 10;

        if (isset($post_data['Start'])) {
            $start = $post_data['Start'];
        }

        $where = "users.PackageExpiry >= CURDATE() AND products.IsActive = 1 AND products.Hide = 0";
        if (isset($post_data['UserID']) && $post_data['UserID'] > 0) {
            $user_followed_booths = $this->Follower_model->getMultipleRows(array('Follower' => $post_data['UserID'], 'Type' => 'booth'));
            if ($user_followed_booths) {
                foreach ($user_followed_booths as $followed_booth) {
                    $user_followed_booths_arr[] = $followed_booth->Following;
                }
            }
            /*$where .= " AND user_followers.Type = 'booth' AND user_followers.Follower = " . $post_data['UserID'];
            $IsGuest = false;*/
        }
        if (isset($post_data['CategoryID']) && $post_data['CategoryID'] > 0) {
            $where .= " AND products.CategoryID = " . $post_data['CategoryID'];
        }
        if (isset($post_data['OutOfStock']) && $post_data['OutOfStock'] > 0) {
            $where .= " AND products.OutOfStock = " . $post_data['OutOfStock'];
        }
        if (isset($post_data['IsActive']) && $post_data['IsActive'] > 0) {
            $where .= " AND products.IsActive = " . $post_data['IsActive'];
        }

        // $where = "products.UserID = " . $post_data['UserID'] . " AND products.CategoryID = " . $post_data['CategoryID'] . " AND OutOfStock = " . $post_data['OutOfStock'] . " AND IsActive = " . $post_data['IsActive'];

        $promoted_products_arr = array();
        if (!empty($user_followed_booths_arr) && $start == 0){
            //$where_promoted = "products.Hide = 0 AND promoted_products.PromotionExpiresAt > " . time();
            $where_promoted = "products.Hide = 0 ";
            $promoted_products = $this->Promoted_product_model->getPromotedProductsForUserFollowBooth($where_promoted, $this->language, $user_followed_booths_arr);
            if ($promoted_products) {
                $i = 0;
                foreach ($promoted_products as $promoted_product) {
                    $promoted_products_arr[$i] = $promoted_product;
                    // $promoted_products_arr[$i]['IsPromoted'] = '1';
                    $promoted_products_arr[$i]['IsPromoted'] = ($promoted_product['IsPromotedProduct'] == 1 && $promoted_product['IsPromotionApproved'] == 1 ? 1 : 0);
                    $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $promoted_product['ProductID']));
                    $promoted_products_arr[$i]['ProductImages'] = $product_images ? $product_images : array();
                    $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $promoted_product['ProductID']);
                    $LikesCount = 0;
                    if (!empty($likes)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $l_count = 0;
                            foreach ($likes as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$l_count] = $value;
                                $l_count++;
                            }
                            $likes = $my_arr;
                        }
                        $LikesCount = count($likes);
                    }
                    $comments = $this->Product_comment_model->getProductComments("product_comments.ProductID = " . $promoted_product['ProductID'], $this->language);
                    $CommentCount = 0;
                    if (!empty($comments)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $c_count = 0;
                            foreach ($comments as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$c_count] = $value;
                                $c_count++;
                            }
                            $comments = $my_arr;
                        }
                        $CommentCount = count($comments);
                    }
                    $promoted_products_arr[$i]['LikesCount'] = (string)$LikesCount;
                    $promoted_products_arr[$i]['CommentCount'] = (string)$CommentCount;
                    $i++;
                }
            }
        }


        $products_arr = array();
        if (!empty($user_followed_booths_arr)) {
            $products = $this->Product_model->getProductsForUserFollowBooth($where, $this->language, $limit, $start, 'products_text.Title', 'ASC', $user_followed_booths_arr);
            if ($products) {
                $i = 0;
                foreach ($products as $product) {

                    $check_if_product_promoted['ProductID'] = $product['ProductID'];
                    $check_if_product_promoted['PromotionExpiresAt >'] = time();
                    $IsProductPromoted = $this->Promoted_product_model->getWithMultipleFields($check_if_product_promoted);
                    $products_arr[$i] = $product;
                    $products_arr[$i]['ItemType'] = 'Product';

                    if (isset($post_data['UserID'])) {
                        $check_already_liked['UserID'] = $post_data['UserID'];
                        $check_already_liked['ProductID'] = $product['ProductID'];
                        $check_like = $this->Product_like_model->getWithMultipleFields($check_already_liked);
                        if ($check_like) {
                            $products_arr[$i]['IsLiked'] = '1';
                        } else {
                            $products_arr[$i]['IsLiked'] = '0';
                        }
                    } else {
                        $products_arr[$i]['IsLiked'] = '0';
                    }

                    // $products_arr[$i]['IsPromoted'] = $IsProductPromoted ? '1' : '0';
                    $products_arr[$i]['IsPromoted'] = ($product['IsPromotedProduct'] == 1 && $product['IsPromotionApproved'] == 1 ? 1 : 0);
                    $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $product['ProductID']));
                    $products_arr[$i]['ProductImages'] = $product_images ? $product_images : array();

                    $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $product['ProductID']);
                    $LikesCount = 0;
                    if (!empty($likes)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $l_count = 0;
                            foreach ($likes as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$l_count] = $value;
                                $l_count++;
                            }
                            $likes = $my_arr;
                        }
                        $LikesCount = count($likes);
                    }
                    $comments = $this->Product_comment_model->getProductComments("product_comments.ProductID = " . $product['ProductID'], $this->language);
                    $CommentCount = 0;
                    if (!empty($comments)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $c_count = 0;
                            foreach ($comments as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$c_count] = $value;
                                $c_count++;
                            }
                            $comments = $my_arr;
                        }
                        $CommentCount = count($comments);
                    }
                    $products_arr[$i]['LikesCount'] = (string)$LikesCount;
                    $products_arr[$i]['CommentCount'] = (string)$CommentCount;
                    $i++;
                }
            }
        }

        if (isset($post_data['UserID'])) {
            if (count($promoted_products_arr) > 0) {
                $my_arr = array();
                $i = 0;
                foreach ($promoted_products_arr as $key => $value) {
                    $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value['UserID'], 'booth');
                    if (!$IsBlockedAsBooth) {
                        $my_arr[$i] = $value;
                        $i++;
                    }
                }
                $promoted_products_arr = $my_arr;
            }

            if (count($products_arr) > 0) {
                $my_arr = array();
                $i = 0;
                foreach ($products_arr as $key => $value) {
                    $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value['UserID'], 'booth');
                    if (!$IsBlockedAsBooth) {
                        $my_arr[$i] = $value;
                        $i++;
                    }
                }
                $products_arr = $my_arr;
            }
        }

        /*if(!empty($products_arr)){
            usort($products_arr, function ($item1, $item2) {
                return $item2['IsPromotionApproved'] <=> $item1['IsPromotionApproved'];
            });
        }*/
        $this->response([
            'status' => 200,
            'promoted_products' => $promoted_products_arr,
            'products' => $products_arr
        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }

    private function sortProductsQuestions($products_arr, $questions_arr, $start, $limit,$user_post_data = array())
    {
        $new_products_arr = array();
        if (!empty($questions_arr)) {
            foreach ($questions_arr as $question) {
                $question['IsPromotionApproved'] = 0;
                $new_products_arr[$question['QuestionAskedAt']] = $question;
            }
        }

        if (!empty($products_arr)) {
            foreach ($products_arr as $product) {
                $new_products_arr[$product['CreatedAt']] = $product;
            }
        }

        if (!empty($user_post_data)) {
            foreach ($user_post_data as $user_post) {
                $user_post['IsPromotionApproved'] = 0;
                $new_products_arr[$user_post['CreatedAt']] = $user_post;
            }
        }

        krsort($new_products_arr);

        $new_products_arr2 = array();
        if (!empty($new_products_arr)) {
            $i = 0;
            foreach ($new_products_arr as $item) {
                $new_products_arr2[$i] = $item;
                $i++;
            }
        }

        // dump($new_products_arr2);

        return !empty($new_products_arr2) ? array_slice($new_products_arr2, $start, $limit) : array();
    }

    public function addProduct_post()
    {
        $post_data = $this->post_data; // UserID, CategoryID (Sub-Sub Category ID), ProductVideo, ProductPrice, DeliveryCharges, DeliveryTime, ProductType(New, Used), OutOfStock(0,1), IsActive(0,1), Title, ProductBrandName, ProductDescription, Currency, CurrencySymbol, ProductImagesCount, ProductImage1, ProductImage2 etc..
        $this->_isUserAuthorized();
        set_time_limit(0);
        if (!empty($post_data)) {
            $this->checkIfUserVerified($post_data['UserID'], 'Both');
            $product_data['UserID'] = $post_data['UserID'];
            $product_data['CategoryID'] = $post_data['CategoryID'];

            //if (isset($post_data['ProductVideoThumbnail']) && $post_data['ProductVideoThumbnail'] !== '') {
            if (isset($_FILES['ProductVideoThumbnail']["name"][0]) && $_FILES['ProductVideoThumbnail']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                //$product_data['ProductVideoThumbnail'] = uploadFileFromBase64($post_data['ProductVideoThumbnail'], "uploads/products/videos/thumbnails/");
                 $product_data['ProductVideoThumbnail'] = uploadImage('ProductVideoThumbnail', "uploads/products/videos/thumbnails/");
            }
            /*if (isset($post_data['ProductVideo']) && $post_data['ProductVideo'] !== '') {
                ini_set('memory_limit', '-1');
                $product_data['ProductVideo'] = uploadFileFromBase64($post_data['ProductVideo'], "uploads/products/videos/", 'mp4');
            }*/
            if (isset($_FILES['ProductVideo']["name"][0]) && $_FILES['ProductVideo']["name"][0] != '') {
                
                $product_data['ProductVideo'] = uploadVideo('ProductVideo', "uploads/products/videos/");

            }
            $product_data['ProductPrice'] = $post_data['ProductPrice'];
            $product_data['Currency'] = $post_data['Currency'];
            $product_data['CurrencySymbol'] = $post_data['CurrencySymbol'];
            $product_data['DeliveryCharges'] = $post_data['DeliveryCharges'];
            $product_data['DeliveryTime'] = $post_data['DeliveryTime'];
            $product_data['ProductType'] = $post_data['ProductType'];
            $product_data['OutOfStock'] = $post_data['OutOfStock'];
            $product_data['IsActive'] = $post_data['IsActive'];
            $product_data['CreatedAt'] = time();
            $product_data['UpdatedAt'] = time();

            $inserted_id = $this->Product_model->save($product_data);
            if ($inserted_id > 0) {
                $system_languages = getSystemLanguages();
                foreach ($system_languages as $system_language) {
                    $product_text_data['ProductID'] = $inserted_id;
                    $product_text_data['Title'] = $post_data['Title'];
                    $product_text_data['ProductBrandName'] = $post_data['ProductBrandName'];
                    $product_text_data['ProductDescription'] = $post_data['ProductDescription'];
                    $product_text_data['ProductReturnPolicy'] = (isset($post_data['ProductReturnPolicy']) ? $post_data['ProductReturnPolicy'] : '');
                    $product_text_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                    $product_text_data['CreatedAt'] = time();
                    $product_text_data['UpdatedAt'] = time();
                    $this->Product_text_model->save($product_text_data);
                }

                // Uploading Product Images
                ini_set('memory_limit', '-1');
                $product_images_count = (isset($post_data['ProductImagesCount']) && $post_data['ProductImagesCount'] > 0 ? $post_data['ProductImagesCount'] : 0);
                for ($i = 1; $i <= $product_images_count; $i++) {
                    $filename = date('Ymdhsi') . rand(9999, 99999999999);
                    $product_image_data['ProductID'] = $inserted_id;
                   // $product_image_data['ProductImage'] = uploadFileFromBase64($post_data['ProductImage' . $i], "uploads/products/");
                    $product_image_data['ProductImage'] = uploadImage('ProductImage'.$i, "uploads/products/");
                    $product_image_data['ProductCompressedImage'] = compress($product_image_data['ProductImage'], "uploads/products/compressed/product-image-" . $filename . '.png');
                    $this->Product_image_model->save($product_image_data);
                }

                // saving activity
                $activity_data['UserID'] = $post_data['UserID'];
                $activity_data['UserType'] = 'user';
                 $activity_data['CommentedAs'] = 'user';//to check followed by
                $activity_data['Type'] = 'product_added';
                $activity_data['NotificationTypeID'] = 22;
                $activity_data['ProductID'] = $inserted_id;
                $UserInfo = getUserInfo($activity_data['UserID'], $this->language);
                $activity_data['NotificationTextEn'] = '@' . $UserInfo->UserName . " has liked a product";
                $activity_data['NotificationTextAr'] = '@' . $UserInfo->UserName . " قام بالاعجاب بمنتجك";
                $activity_data['CreatedAt'] = time();
                $mentioned_user_ids = array($UserInfo->UserID);
                $mentioned_user_names = array($UserInfo->UserName);
                $mentioned_user_types = array('user');
                log_friend_activity($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);

                $products = $this->Product_model->getProducts("products.ProductID = " . $inserted_id, $this->language);
                $products[0]->ProductImages = $this->Product_image_model->getMultipleRows(array('ProductID' => $inserted_id));
                $this->response([
                    'status' => 200,
                    'message' => lang('product_added_successfully'),
                    'product' => $products[0]
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function updateProduct_post()
    {
        $post_data = $this->post_data;
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $save_parent_data = array();
            $save_child_data = array();
            $parents_fields = $this->Product_model->getFields();
            $child_fields = $this->Product_text_model->getFields();
            foreach ($post_data as $key => $value) {
                if (in_array($key, $parents_fields)) {
                    $save_parent_data[$key] = $value;
                } elseif (in_array($key, $child_fields)) {
                    $save_child_data[$key] = $value;
                }

            }

            if (isset($post_data['ProductVideoThumbnail']) && $post_data['ProductVideoThumbnail'] !== '') {
                ini_set('memory_limit', '-1');
                $product_data['ProductVideoThumbnail'] = uploadFileFromBase64($post_data['ProductVideoThumbnail'], "uploads/products/videos/thumbnails/");
            }

           /* if (isset($post_data['ProductVideo']) && $post_data['ProductVideo'] !== '') {
                ini_set('memory_limit', '-1');
                $save_parent_data['ProductVideo'] = uploadFileFromBase64($post_data['ProductVideo'], "uploads/products/videos/", 'mp4');
            }
            */

            if (isset($_FILES['ProductVideo']["name"][0]) && $_FILES['ProductVideo']["name"][0] != '') {
                
                $product_data['ProductVideo'] = uploadVideo('ProductVideo', "uploads/products/videos/");

            }

            $save_parent_data['UpdatedAt'] = time();
            $save_child_data['UpdatedAt'] = time();
            $update_by['ProductID'] = $post_data['ProductID'];
            $this->Product_model->update($save_parent_data, $update_by);
            $this->Product_text_model->update($save_child_data, $update_by);

            // Removing some of the images
            if (isset($post_data['DeletedImagesIDs']) && $post_data['DeletedImagesIDs'] != '') {
                $DeletedImagesIDs = explode(',', $post_data['DeletedImagesIDs']);
                $this->Product_image_model->deleteIn('ProductImageID', $DeletedImagesIDs);
            }

            // update cart

            $this->Temp_order_model->update(array('IsEdited' => 1), array('ProductID' => $post_data['ProductID']));

            // Uploading Product Images
            ini_set('memory_limit', '-1');
            $product_images_count = (isset($post_data['ProductImagesCount']) && $post_data['ProductImagesCount'] > 0 ? $post_data['ProductImagesCount'] : 0);
            for ($i = 1; $i <= $product_images_count; $i++) {
                $product_image_data['ProductID'] = $post_data['ProductID'];
                $product_image_data['ProductImage'] = uploadFileFromBase64($post_data['ProductImage' . $i], "uploads/products/");
                $product_image_data['ProductCompressedImage'] = compress($product_image_data['ProductImage'], "uploads/products/compressed/product-image-" . time() . '.png');
                $this->Product_image_model->save($product_image_data);
            }

            $products = $this->Product_model->getProducts("products.ProductID = " . $post_data['ProductID'], $this->language);
            $products[0]->ProductImages = $this->Product_image_model->getMultipleRows(array('ProductID' => $post_data['ProductID']));
            $this->response([
                'status' => 200,
                'message' => lang('product_updated_successfully'),
                'product' => $products[0]
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function updateVideoForProduct_post()
    {
        $post_data = $this->post_data; // UserID, ProductID, ProductVideo
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            if (isset($post_data['ProductVideoThumbnail']) && $post_data['ProductVideoThumbnail'] !== '') {
                ini_set('memory_limit', '-1');
                $update['ProductVideoThumbnail'] = uploadFileFromBase64($post_data['ProductVideoThumbnail'], "uploads/products/videos/thumbnails/");
            }
            if (isset($post_data['ProductVideo']) && $post_data['ProductVideo'] !== '') {
                ini_set('memory_limit', '-1');
                $update['ProductVideo'] = uploadFileFromBase64($post_data['ProductVideo'], "uploads/products/videos/", 'mp4');
            }
            $update['UpdatedAt'] = time();
            $update_by['ProductID'] = $post_data['ProductID'];
            $this->Product_model->update($update, $update_by);
            $this->response([
                'status' => 200,
                'message' => lang('product_updated_successfully')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function reportProduct_post()
    {
        $post_data = $this->post_data; // UserID (who is reporting), ProductID, ReportReason
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $product_data  = $this->Product_model->get($post_data['ProductID'],false,'ProductID');
            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $product_data->UserID);
            if ($IsBlocked){
                 $this->response([
                        'status' => 402,
                        'message' => lang('you_cannot_do')
                    ], REST_Controller::HTTP_OK);

                 exit;
            }
            $user_reported = $this->Product_reported_model->getWithMultipleFields(array('UserID' => $post_data['UserID'], 'ProductID' => $post_data['ProductID']));
            if ($user_reported) {
                $this->response([
                    'status' => 402,
                    'message' => lang('product_already_reported')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            $post_data['ReportedAt'] = time();
            $this->Product_reported_model->save($post_data);
            $this->response([
                'status' => 200,
                'message' => lang('product_reported_successfully')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }


    public function reportPost_post()
    {
        $post_data = $this->post_data; // UserID (who is reporting), ProductID, ReportReason
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $post_data  = $this->User_post_model->get($post_data['UserPostID'],false,'UserPostID');
            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $post_data->UserID);
            if ($IsBlocked){
                 $this->response([
                        'status' => 402,
                        'message' => lang('you_cannot_do')
                    ], REST_Controller::HTTP_OK);

                 exit;
            }
            $user_reported = $this->User_post_reported_model->getWithMultipleFields(array('UserID' => $post_data['UserID'], 'UserPostID' => $post_data['UserPostID']));
            if ($user_reported) {
                $this->response([
                    'status' => 402,
                    'message' => lang('post_already_reported')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            $post_data['ReportedAt'] = time();
            $this->User_post_reported_model->save($post_data);
            $this->response([
                'status' => 200,
                'message' => lang('post_reported_successfully')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function getCartItems_get()
    {
        
        $post_data = $this->post_data; // UserID//
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $cart_items_arr = array();
            $cart_items = $this->Temp_order_model->getCartItems($post_data['UserID'], 'EN');

            if (!empty($cart_items)) {
               
                foreach ($cart_items as  $value) {
                    $images = $this->Site_images_model->getMultipleRows(array('FileID' => $value->ProductID,'ImageType' => 'product'),true);

                    if($images)
                    {

                        $value->ProductImages = $images;
                    }else{
                        $value->ProductImages = array();
                    }
                    $cart_items_arr[] = $value;
                }
            }
            $this->response([
                'status' => 200,
                'cart_items' => $cart_items_arr
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function addToCart_post()
    {
        $post_data = $this->post_data; 
        $post_data['CompanyID'] = $this->CompanyID;
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $IsProductActive = $this->checkIfProductActive($post_data['ProductID']);
            if (!$IsProductActive) {
                $this->response([
                    'status' => 402,
                    'message' => lang('not_available')
                ], REST_Controller::HTTP_OK);
            }


            $checkProduct = $this->Product_model->getProductDetail($post_data['ProductID'],'EN');
            if (empty($checkProduct)) {
                $this->response([
                    'status' => 402,
                    'message' => lang('not_available')
                ], REST_Controller::HTTP_OK);
            }






            $product_data  = $this->Product_model->get($post_data['ProductID'],false,'ProductID');
            
            $fetch_by['UserID'] = $post_data['UserID'];
            $fetch_by['ProductID'] = $post_data['ProductID'];
            $already_added = $this->Temp_order_model->getWithMultipleFields($fetch_by);
            if ($already_added) {
                $update['Quantity'] = $already_added->Quantity + 1;
                $update_by['TempOrderID'] = $already_added->TempOrderID;
                $this->Temp_order_model->update($update, $update_by);
                $insert_id = $already_added->TempOrderID;
            } else {
                
                $insert_id = $this->Temp_order_model->save($post_data);
            }
            if ($insert_id > 0) {
                $this->response([
                    'status' => 200,
                    'message' => lang('added_to_basket')
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function updateCart_post()
    {
        $post_data = $this->post_data; // TempOrderID, ProductQuantity
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $TempOrder = $this->Temp_order_model->get($post_data['TempOrderID'], true, 'TempOrderID');
            $IsProductActive = $this->checkIfProductActive($TempOrder['ProductID']);
            if (!$IsProductActive) {
                $this->response([
                    'status' => 402,
                    'message' => lang('not_available')
                ], REST_Controller::HTTP_OK);
            }
            $update['ProductQuantity'] = $post_data['Quantity'];
            $update_by['TempOrderID'] = $post_data['TempOrderID'];
            $this->Temp_order_model->update($update, $update_by);
            $this->response([
                'status' => 200,
                'message' => lang('cart_updated')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function deleteCartItem_get()
    {
        $post_data = $this->post_data; // TempOrderID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $deleted_by['TempOrderID'] = $post_data['TempOrderID'];
            $this->Temp_order_model->delete($deleted_by);
            $this->response([
                'status' => 200,
                'message' => lang('deleted_successfully')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }



     public function placeOrder_post()
    {
        $post_data = $this->post_data;

        if (!empty($post_data)) {
            
            $data['cart_items'] = $this->Temp_order_model->getCartItems($post_data['UserID'], $this->language);
            if (empty($data['cart_items'])) {

                $this->response([
                    'status' => 402,
                    'message' => lang('no_items_in_basket')
                ], REST_Controller::HTTP_OK);
                
            }
        $order_data['UserID'] = $post_data['UserID'];
        $show_error = false;
        $error_message = '';
        $model_html = '';
        $show_model = false;

        
        if (isset($post_data['CollectFromStore']) && $post_data['CollectFromStore'] == 1) {
            $order_data['CollectFromStore'] = 1;
        } else {
            $order_data['CollectFromStore'] = 0;
            if(!isset($post_data['AddressID'])){
                $fetch_address_by['UserID'] = $post_data['UserID'];
                $fetch_address_by['IsDefault'] = 1;
                $address = $this->User_address_model->getWithMultipleFields($fetch_address_by);
                $order_data['AddressID'] = $address->AddressID;
                $user_city_id = $address->CityID;

            }else{
                 $order_data['AddressID'] = $post_data['AddressID'];
            }
            

           

            $order_data['ShipmentMethodID'] = $post_data['ShipmentMethodIDForBooking'];
        }
        //$order_data['BranchDeliveryDistrictID'] = $_GET['BranchDeliveryDistrictID'];
        $order_data['StoreID'] = $post_data['StoreID'];
         $order_data['CompanyID'] = $this->CompanyID;
        $order_data['CreatedAt'] = date('Y-m-d H:i:s');
        $order_data['OrderNumber'] = time() . $post_data['UserID'];
        $order_data['PaymentMethod'] = $post_data['PaymentMethodForBooking'];
        $insert_id = $this->Order_model->save($order_data);
        if ($insert_id > 0) {
            // $this->sendThankyouEmailToCustomer($insert_id);//email sending in botton
            $get_store_data = $this->Store_model->get($post_data['StoreID'],false,'StoreID');
            $order_item_data = array();
            $total_amount = 0;
            foreach ($data['cart_items'] as $product) {
                $order_item_data[] = [
                    'OrderID' => $insert_id,
                    'ProductID' => $product->ProductID,
                    'Quantity' => $product->Quantity,
                    'Amount' => $product->TempItemPrice,
                    'ItemType' => $product->ItemType,
                    'CustomizedOrderProductIDs' => $product->CustomizedOrderProductIDs,
                    'CustomizedShapeImage' => $product->CustomizedShapeImage,
                    'CustomizedBoxID' => $product->CustomizedBoxID,
                    'IsCorporateItem' => $product->IsCorporateItem,
                    'Ribbon' => $product->Ribbon
                ];
                $total_amount += $product->Quantity * $product->TempItemPrice;

                // increase product purchase count here
                $product_dt_for_purchase_count = $this->Product_model->get($product->ProductID, false, 'ProductID');
                $PurchaseCount = (int)$product_dt_for_purchase_count->PurchaseCount + (int)$product->Quantity;
                $this->Product_model->update(array('PurchaseCount' => $PurchaseCount), array('ProductID' => $product->ProductID));


                //update product quantity

                if($get_store_data){

                    $availability_product = $this->Product_availability_model->getWithMultipleFields(array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                    if($availability_product){
                        $quantity = $availability_product->Quantity - $product->Quantity;
                        $this->Product_availability_model->update(array('Quantity' => $quantity),array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));

                    }

                }


            }

            if (isset($post_data['OrderCoupon']) && $post_data['OrderCoupon']) {
                
                $coupon_code = $post_data['CouponCode'];
                $coupon_discount_percentage = $post_data['DiscountPercentage'];
                $coupon_discount_availed = ($post_data['DiscountPercentage'] / 100) * $total_amount;
                $total_amount = $total_amount - $coupon_discount_availed;

                // reducing coupon usage count
                $coupon_detail = $this->Coupon_model->getWithMultipleFields(array('CouponID' => $post_data['CouponID']), true);
                $update['UsageCount'] = $coupon_detail['UsageCount'] - 1;
                $update_by['CouponID'] = $post_data['CouponID'];
                $this->Coupon_model->update($update, $update_by);
            } else {
                $coupon_code = '';
                $coupon_discount_percentage = 0;
                $coupon_discount_availed = 0;
            }

            $ShipmentMethodIDForBooking = $post_data['ShipmentMethodIDForBooking'];
            $shipping_amount = 0;
            if($order_data['CollectFromStore'] != 1){
                $shipment_method = getSelectedShippingMethodDetail($ShipmentMethodIDForBooking, $this->language);
                if ($shipment_method) {
                    $shipping_id = $shipment_method->TaxShipmentChargesID;
                    $shipping_title = $shipment_method->Title;
                    $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                    if ($shipment_method->Type == 'Fixed') {
                        $shipping_amount = $shipment_method->Amount;
                    } elseif ($shipment_method->Type == 'Percentage') {
                        $shipping_amount = ($shipment_method->Amount / 100) * ($total_amount);
                    }
                    $order_extra_charges['OrderID'] = $insert_id;
                    $order_extra_charges['TaxShipmentChargesID'] = $shipping_id;
                    $order_extra_charges['Title'] = $shipping_title;
                    $order_extra_charges['Factor'] = $shipping_factor;
                    $order_extra_charges['Amount'] = $shipping_amount;
                    $this->Order_extra_charges_model->save($order_extra_charges);
                }
            }
            

            $total_tax = 0;
            $taxes = getTaxShipmentCharges('Tax');
            foreach ($taxes as $tax) {
                $tax_id = $tax->TaxShipmentChargesID;
                $tax_title = $tax->Title;
                $tax_factor = $tax->Type == 'Fixed' ? number_format($tax->Amount, 2) . ' SAR' : $tax->Amount . '%';
                if ($tax->Type == 'Fixed') {
                    $tax_amount = $tax->Amount;
                } elseif ($tax->Type == 'Percentage') {
                    $tax_amount = ($tax->Amount / 100) * ($total_amount + $shipping_amount);
                }
                $total_tax += $tax_amount;
                $order_extra_charges['OrderID'] = $insert_id;
                $order_extra_charges['TaxShipmentChargesID'] = $tax_id;
                $order_extra_charges['Title'] = $tax_title;
                $order_extra_charges['Factor'] = $tax_factor;
                $order_extra_charges['Amount'] = $tax_amount;
                $this->Order_extra_charges_model->save($order_extra_charges);
            }
            $total_amount = $total_amount + $shipping_amount + $total_tax;

            $order_data['CouponCodeUsed'] = $coupon_code;
            $order_data['CouponCodeDiscountPercentage'] = $coupon_discount_percentage;
            $order_data['DiscountAvailed'] = $coupon_discount_availed;
            $order_data['TotalShippingCharges'] = $shipping_amount;
            $order_data['TotalTaxAmount'] = $total_tax;
            $order_data['TotalAmount'] = $total_amount;
            $order_data['OrderNumber'] = str_pad($insert_id, 5, '0', STR_PAD_LEFT);
            $this->Order_item_model->insert_batch($order_item_data);
            $this->Order_model->update($order_data, array('OrderID' => $insert_id));
            $deleted_by['UserID'] = $post_data['UserID'];
            $this->Temp_order_model->delete($deleted_by);
           // $this->sendOrderConfirmationToCustomer($insert_id);
            //$order_data_pusher = $this->Order_model->get($insert_id, true, 'OrderID');
           // pusher($order_data_pusher, "Ecommerce_Order_Channel", "Ecommerce_Order_Event"); //Need to create Ecommerce on pusher channel
           // $response['message'] = lang('payment_page_redirect');

             $this->response([
                    'status' => 200,
                    'message' => lang('thank_you_for_order')
                ], REST_Controller::HTTP_OK);
            
           
        } else {

             $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            
        }

            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        
    }

    public function getProductAvailability_get(){

        $post_data = $this->post_data; // UserID,StoreID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {

            $return_array = array();

            $cart_items = $this->Temp_order_model->getCartItems($post_data['UserID'], $this->language);

            if(empty($cart_items)){
                $this->response([
                        'status' => 402,
                        'message' => lang('your_cart_is_empty')
                    ], REST_Controller::HTTP_OK);
            }

             foreach($cart_items as $key => $product){
                $product_data = $this->Product_model->getStoreAvailability('product_store_availability.StoreID = '.$post_data['StoreID'].' AND products.ProductID = '.$product->ProductID);

                if($product_data && $product->Quantity <= $product_data['AvailableQuantity']){

                    $return_array[$key]['ProductID'] = $product->ProductID;
                    $return_array[$key]['Title'] = $product_data['Title'];
                    $return_array[$key]['IsAvailable'] = 1;
                    $return_array[$key]['QuantityUpdate'] = 0;
                    $return_array[$key]['IsDeletedFromCart'] = 0;
                    $return_array[$key]['Message'] = lang('prodcut_is_available');

                }elseif($product_data && $product_data['AvailableQuantity'] <  $product->Quantity && $product_data['AvailableQuantity'] > 0){
                     $this->Temp_order_model->update(array('Quantity' => $product_data['AvailableQuantity']),array('TempOrderID' => $product->TempOrderID));

                    $return_array[$key]['ProductID'] = $product->ProductID;
                    $return_array[$key]['Title'] = $product_data['Title'];
                    $return_array[$key]['IsAvailable'] = 1;
                    $return_array[$key]['QuantityUpdate'] = 1;
                    $return_array[$key]['IsDeletedFromCart'] = 0;
                    $return_array[$key]['Message'] = lang('product_quantity_updated_in_cart');

                }elseif($product_data && $product_data['AvailableQuantity'] == 0){

                   // $this->Temp_order_model->delete(array('TempOrderID' => $product->TempOrderID));

                    $return_array[$key]['ProductID'] = $product->ProductID;
                    $return_array[$key]['Title'] = $product_data['Title'];
                    $return_array[$key]['IsAvailable'] = 1;
                    $return_array[$key]['QuantityUpdate'] = 0;
                    $return_array[$key]['IsDeletedFromCart'] = 0;
                    $return_array[$key]['Message'] = lang('product_deleted_from_cart');

                }elseif(!$product_data){
                   // $this->Temp_order_model->delete(array('TempOrderID' => $product->TempOrderID));

                    $return_array[$key]['ProductID'] = $product->ProductID;
                    $return_array[$key]['Title'] = $product_data['Title'];
                    $return_array[$key]['IsAvailable'] = 1;
                    $return_array[$key]['QuantityUpdate'] = 0;
                    $return_array[$key]['IsDeletedFromCart'] = 0;
                    $return_array[$key]['Message'] = lang('product_deleted_from_cart');

                }


            }

           $this->response([
                    'status' => 200,
                    'products' => $return_array
                ], REST_Controller::HTTP_OK);   


        }else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

    }


    public function getShippingMethods_get(){

        $return_array = array();
        $shipment_methods = getTaxShipmentCharges('Shipment');
        if($shipment_methods){
            $return_array = $shipment_methods;;
        }
        $this->response([
                    'status' => 200,
                    'shipping_methods' => $return_array
                ], REST_Controller::HTTP_OK);
    }

    public function applyCoupon_post()
    {
        $post_data = $this->post_data; // UserID, CouponCode, Amount (On which discount is being checked), OrderRequestID (against which this coupon is being applied)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $coupon = $this->Coupon_model->getWithMultipleFields(array('CouponCode' => $post_data['CouponCode'],'CompanyID' => $this->CompanyID), true);
            if ($coupon) {
                /*if ($coupon['UserID'] > 0) { // It means this coupon is seller specific
                    $order_request_detail = $this->Order_request_model->get($post_data['OrderRequestID'], true, 'OrderRequestID');
                    if ($coupon['UserID'] !== $order_request_detail['BoothID']) {
                        $this->response([
                            'status' => 402,
                            'message' => lang('coupon_not_available_for_seller')
                        ], REST_Controller::HTTP_OK);
                    }
                }*/
                $today = date('d-m-Y');
                $todays_timestamp = strtotime($today);
                if ($coupon['ExpiryDate'] >= $todays_timestamp) {
                    if ($coupon['DiscountType'] == 'Percentage') {
                        $discount = ($coupon['DiscountFactor'] / 100) * $post_data['Amount'];
                        $response_data['DiscountedAmount'] = $post_data['Amount'] - $discount;
                        $response_data['DiscountAvailed'] = $discount;
                        $response_data['DiscountAppliedFactor'] = $coupon['DiscountFactor'] . '%';
                    } /*else {
                        $response_data['DiscountedAmount'] = $post_data['Amount'] - $coupon['DiscountFactor'];
                        $response_data['DiscountAvailed'] = $coupon['DiscountFactor'];
                        $response_data['DiscountAppliedFactor'] = $coupon['DiscountFactor'] . ' SAR';
                    }*/
                    //$update['UsageCount'] = $coupon['UsageCount'] - 1;
                    //$update_by['CouponID'] = $coupon['CouponID'];
                   // $this->Coupon_model->update($update, $update_by);
                    $response_data['CouponID'] = $coupon['CouponID'];
                    $response_data['DiscountedAmount'] = (string)$response_data['DiscountedAmount'];
                    $response_data['DiscountAvailed'] = (string)$response_data['DiscountAvailed'];
                    $response_data['DiscountAppliedFactor'] = (string)$response_data['DiscountAppliedFactor'];
                    $this->response([
                        'status' => 200,
                        'message' => lang('coupon_applied'),
                        'coupon' => $response_data
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('coupon_expired')
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('coupon_invalid')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }



    public function placeOrder_old_post()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $where = 'orders.UserID = ' . $post_data['UserID'] . ' AND orders_requests.OrderRequestRating = 0 and orders_requests.OrderStatusID = 4';
            $order_request = $this->Order_request_model->getOrdersRequests($where, $this->language);
            if ($order_request) {
                $this->response([
                    'status' => 200,
                    'message' => lang('please_rate_booth_first'),
                    'OrderRequestID' => $order_request[0]['OrderRequestID']
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            $InActiveProductsCount = 0;
            $this->checkIfUserVerified($post_data['UserID'], 'Both');
            $cart_products = $this->Temp_order_model->getJoinedDataWithOutText(false, 'ProductID', 'products', 'users.PackageExpiry >= CURDATE() AND temp_orders.UserID = ' . $post_data['UserID'] . '',true);
            $booth_array = array();
            if ($cart_products) {
                foreach ($cart_products as $key => $value) {
                    if (!in_array($value->BoothID, $booth_array)) {
                        $booth_array[] = $value->BoothID;
                    }
                }
                $order_data['UserID'] = $post_data['UserID'];
                $order_data['AddressID'] = $post_data['AddressID'];
                $order_data['CreatedAt'] = $order_data['UpdatedAt'] = time();
                $insert_id = $this->Order_model->save($order_data);
                if ($insert_id > 0) {
                    $product_ids_for_activity = array();
                    foreach ($booth_array as $key => $value) {
                        $OrderTrackID = str_pad($insert_id, 5, '0', STR_PAD_LEFT);
                        $order_request_data = [
                            'OrderID' => $insert_id,
                            'OrderStatusID' => 1,
                            'OrderTrackID' => $OrderTrackID,
                            'BoothID' => $value
                        ];
                        $user_customization = $this->User_customization_model->getWithMultipleFields(array('UserID' => $value));
                        $TotalAmount = 0;
                        $ActualDeliveryCharges = 0;
                        $order_request_id = $this->Order_request_model->save($order_request_data);
                        $update_order['OrderTrackID'] = str_pad($order_request_id, 5, '0', STR_PAD_LEFT);
                        $update_order['OrderLastStatusID'] = 1;
                        $update_order_by['OrderRequestID'] = $order_request_id;
                        $this->Order_request_model->update($update_order, $update_order_by);
                        $order_item_data = array();
                        foreach ($cart_products as $product) {
                            if ($product->BoothID == $value) {
                                $IsProductActive = $this->checkIfProductActive($product->ProductID);
                                if ($IsProductActive) {
                                    $TotalAmount += $product->ProductPrice * $product->ProductQuantity;
                                    $ActualDeliveryCharges += $product->DeliveryCharges;//$product->DeliveryCharges * $product->ProductQuantity;
                                    $order_item_data['OrderRequestID'] = $order_request_id;
                                    $order_item_data['ProductID'] = $product->ProductID;
                                    $order_item_data['Quantity'] = $product->ProductQuantity;
                                    $order_item_data['Price'] = $product->ProductPrice;
                                    $this->Order_item_model->save($order_item_data);
                                    $product_ids_for_activity[] = $product->ProductID;
                                } else {
                                    $InActiveProductsCount++;
                                }
                            }
                        }

                        // update order request invoice details
                        $update_order_request_data['TotalAmount'] = $TotalAmount;
                        $update_order_request_data['ActualDeliveryCharges'] = $ActualDeliveryCharges;
                        $update_order_request_data['VatPercentageApplied'] = $user_customization->VatPercentage;
                        $update_order_request_data['VatAmountApplied'] = ($update_order_request_data['TotalAmount'] * $update_order_request_data['VatPercentageApplied']) / 100;
                        $update_order_request_data['GrandTotal'] = $update_order_request_data['TotalAmount'] + $update_order_request_data['ActualDeliveryCharges'] + $update_order_request_data['VatAmountApplied'];
                        $update_order_request_data_by['OrderRequestID'] = $order_request_id;
                        $this->Order_request_model->update($update_order_request_data, $update_order_request_data_by);
                    }
                    $deleted_by['UserID'] = $post_data['UserID'];
                    $this->Temp_order_model->delete($deleted_by);
                    $return_array = $this->Order_model->get($insert_id, true, 'OrderID');
                    $order_request = $this->Order_request_model->getMultipleRows(array('OrderID' => $insert_id), true);
                    
                    

                    

                    // saving activity
                    $activity_data['UserID'] = $post_data['UserID'];
                    $activity_data['UserType'] = 'user';
                     $activity_data['CommentedAs'] = 'user';//to check followed by
                    $activity_data['Type'] = 'order';
                    $activity_data['NotificationTypeID'] = 17;
                    if (!empty($product_ids_for_activity)) {
                        $activity_data['ProductID'] = implode(',', $product_ids_for_activity);
                    }
                    

                    $UserInfo = getUserInfo($activity_data['UserID'], $this->language);//customer info
                    $activity_data['NotificationTextEn'] = '@' . $UserInfo->UserName . " has placed an order containing following products";
                    $activity_data['NotificationTextAr'] = '@' . $UserInfo->UserName . " has placed an order containing following products";
                    $activity_data['CreatedAt'] = time();
                    $mentioned_user_ids = array($UserInfo->UserID);
                    $mentioned_user_names = array($UserInfo->UserName);
                    $mentioned_user_types = array('user');
                    log_friend_activity($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                    $customer_name = $UserInfo->UserName;

                    $i = 0;
                    foreach ($order_request as $key => $value) {
                        $return_array['order_requests'][$i] = $value;
                        $i++;


                        $booth_info = array();
                        $booth_info = getUserInfo($value['BoothID'], $this->language);
                        //print_rm($booth_info);
                        
                        $this->sendOrderEmailToBooth($booth_info,$value);
                    }

                    // send order request notification to booths
                    $this->sendOrderNotificationToBooths($return_array);
                    $this->sendThankyouEmailToCustomer($insert_id);

                    // Sending sms to customer
                    $user_data = $this->User_model->get($post_data['UserID'], false, 'UserID');
                    if($user_data->LastLangState == 'EN'){
                       $message = "Hi, thanks for placing an order with Booth! You will be notified once the seller approves your order.";
                        
                        
                    }else{
                        $message = "مرحبا، شكرا لطلبك من بوث
سيتم اشعارك بمجرد تأكيد البوث لطلبك.";
                    }
                    
                    sendSms($user_data->Mobile, $message);

                    $this->response([
                        'status' => 200,
                        'message' => $InActiveProductsCount == 0 ? lang('your_order_sent_for_approval') : lang('your_order_sent_for_approval_removed_products'),
                        'order_data' => $return_array
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $success['status'] = 402;
                $success['message'] = lang('your_cart_is_empty');
                echo json_encode($success);
                exit;
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function sendOrderEmailToBooth($booth_info,$order_info)
    {
        $data = array();
        $requests = array();
        //$data['order_items'] =  $order_details  =  $this->Order_model->getOrdersDetailWithCustomerDetail($order_id);
      
        //print_rm($order_details);

        $order_details  =  $this->Order_model->getOrdersRequest('orders_requests.OrderRequestID = '.$order_info['OrderRequestID'].' AND orders.OrderID = '.$order_info['OrderID']);

        foreach ($order_details as $key => $value) {
              $requests[$key] = $value;

              $requests[$key]['items'] = $this->Order_model->getOrderItems($value['OrderRequestID']);
              //echo $this->db->last_query();exit;
        }

       
        $data['requests'] = $requests;
        //$data['completed'] = true;
        

        $email_template = get_email_template(17, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $booth_info->BoothUserName, $message);
        $message = str_replace("{{customer_name}}", $order_details[0]['FullName'], $message);
        $message = str_replace("{{track_id}}", $order_info['OrderTrackID'], $message);
        
        $data['message'] = $message;

        $message = $this->load->view('emails/order_confirmation_email',$data, true);
        
        //echo email_format($message);exit;

       
        $data['to'] = $booth_info->Email;
        $data['subject'] = $subject;
        $data['message'] = email_format($message,false);
        sendEmail($data);
    }


    public function sendThankyouEmailToCustomer($order_id)
    {
        $data = array();
        $requests = array();
        //$data['order_items'] =  $order_details  =  $this->Order_model->getOrdersDetailWithCustomerDetail($order_id);
      
        //print_rm($order_details);

        $order_details  =  $this->Order_model->getOrdersRequest('orders.OrderID = '.$order_id);

        foreach ($order_details as $key => $value) {
              $requests[$key] = $value;

              $requests[$key]['items'] = $this->Order_model->getOrderItems($value['OrderRequestID']);
              //echo $this->db->last_query();exit;
        }

       
        $data['requests'] = $requests;
        $data['completed'] = true;
        

        $email_template = get_email_template(15, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $order_details[0]['FullName'], $message);
        
        $data['message'] = $message;

        $message = $this->load->view('emails/order_confirmation_email',$data, true);
        
        //echo email_format($message);exit;

       
        $data['to'] = $order_details[0]['UserEmail'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message,false);
        sendEmail($data);
    }

    private function sendOrderNotificationToBooths($order_details)
    {
        $order_requests = $order_details['order_requests'];
        if (!empty($order_requests)) {
            foreach ($order_requests as $order_request) {
                $data['LoggedInUserID'] = $order_request['BoothID']; // the person who will receive this notification
                $data['UserType'] = 'booth';
                $data['Type'] = 'order';
                $data['UserID'] = $order_details['UserID']; // the person who is making this notification
                $data['OrderID'] = $order_details['OrderID'];
                $data['OrderRequestID'] = $order_request['OrderRequestID'];
                $data['NotificationTypeID'] = 8;
                $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                $lang_state = $LoggedInUserInfo->LastLangState;
                $booth_mobile = $LoggedInUserInfo->Mobile;
                





                $UserInfo = getUserInfo($data['UserID'], $this->language);
                $data['NotificationTextEn'] = "You have received a new order request from @" . $UserInfo->UserName . " with order track number " . $order_request['OrderTrackID'];
                $data['NotificationTextAr'] = "لقد استلمت طلب جديد رقم  " . $order_request['OrderTrackID'];
                $data['CreatedAt'] = time();
                $mentioned_user_ids = array($UserInfo->UserID);
                $mentioned_user_names = array($UserInfo->UserName);
                $mentioned_user_types = array('user');
                log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request['BoothID']);

                if($lang_state == 'EN'){
                       $message = "Hi, you have received an order #".$order_request['OrderTrackID']." from " . $UserInfo->UserName . " 
Please approve the order to proceed.";
                        
                        
                    }else{
                        $message = "مرحبا، تم استلام طلب  ".$order_request['OrderTrackID']."# من بوث  " . $UserInfo->UserName . "!
الرجاء قراءة تفاصيل الطلب و التأكيد للمتابعة! ";
                    }
                    
                    sendSms($booth_mobile, $message);

                //send sms to booths

              //  Hi, you have received an order#.......from @...... Booth!
//Please approve the order to proceed.
            }
        }
    }

    public function orderItems_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $return_array = $this->Order_model->getOrderDetail($post_data['OrderRequestID'], $this->language);
            $return_array['order_items'] = $this->Order_model->getOrderItems($post_data['OrderRequestID'], $this->language);
            $this->response([
                'status' => 200,
                'order_data' => $return_array
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function getOrders_get(){
        $post_data = $this->post_data; // UserID, Type (user, driver), OrderStatus (Pending, Completed, Cancelled)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $where = '';
            if(isset($post_data['Status'])){

                if($post_data['Status'] == 'pending'){
                    $where = ' AND orders.Status = 1';
                }elseif($post_data['Status'] == 'packed'){
                    $where = ' AND orders.Status = 2';
                }elseif($post_data['Status'] == 'dispatched'){
                    $where = ' AND orders.Status = 3';
                }elseif($post_data['Status'] == 'delivered'){
                    $where = ' AND orders.Status = 4 AND orders.IsPosOrder = 0';
                }elseif($post_data['Status'] == 'pos'){
                    $where = ' AND orders.Status = 4 AND orders.IsPosOrder = 1';
                }elseif($post_data['Status'] == 'cancelled'){
                    $where = ' AND orders.Status = 5';
                }elseif($post_data['Status'] == 'cancelled_not_collect'){
                    $where = ' AND orders.Status = 5 AND orders.CollectFromStore = 1';
                }elseif($post_data['Status'] == 'returned'){
                    $where = ' AND orders.Status = 6';
                }

            }
            $orders = array();
            if (isset($post_data['Type'])) {
                if ($post_data['Type'] === 'user') {
                    $orders = $this->Order_model->getOrders('orders.UserID = '.$post_data['UserID'].''.$where);
                } elseif ($post_data['Type'] === 'driver') {
                    $orders = $this->Order_model->getOrders('orders.DriverID = '.$post_data['UserID'].''.$where);
                }
            }
            $this->response([
                'status' => 200,
                'orders' => $orders
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function getOrderDetail_get(){
        $post_data = $this->post_data; // UserID, Type (user, driver), OrderStatus (Pending, Completed, Cancelled)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $where = '';
            
            $order = array();
            $order_detail = $this->Order_model->getOrders('orders.OrderID = '.$post_data['OrderID'])[0];

            $order['order_detail'] = $order_detail;
            $order['order_extra_charges'] = $this->Order_extra_charges_model->getMultipleRows(array('OrderID' => $post_data['OrderID']));
            $order['payment_address'] = $this->User_address_model->getAddresses("user_address.AddressID = " . $order_detail->AddressIDForPaymentCollection);
            $order['order_items'] = getOrderItems($post_data['OrderID']);
            
            $this->response([
                'status' => 200,
                'order_data' => $order
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getOrders_bk_post()
    {
        $post_data = $this->post_data; // UserID, Type (booth, user), OrderStatus (Pending, Completed, Cancelled)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $orders = array();
            if (isset($post_data['Type'])) {
                if ($post_data['Type'] === 'booth') {
                    $orders = $this->getOrdersForBooth($post_data);
                } elseif ($post_data['Type'] === 'user') {
                    $orders = $this->getOrdersForUser($post_data);
                }
            }
            $this->response([
                'status' => 200,
                'orders' => $orders
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    private function getOrdersForBooth($post_data)
    {
        $where_order_status = "";
        if (isset($post_data['OrderStatus']) && $post_data['OrderStatus'] != '') {
            if ($post_data['OrderStatus'] === 'Pending') {
                $where_order_status = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND ";
            } elseif ($post_data['OrderStatus'] === 'Completed') {
                $where_order_status = "orders_requests.OrderStatusID = 4 AND ";
            } elseif ($post_data['OrderStatus'] === 'Cancelled') {
                $where_order_status = "(orders_requests.OrderStatusID = 5 OR orders_requests.OrderStatusID = 6 OR orders_requests.OrderStatusID = 8) AND ";
            } else {
                $where_order_status = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND ";
            }
        }
        $orders_arr = array();

        $where = $where_order_status . "orders_requests.BoothID = " . $post_data['UserID'];
        $order_requests = $this->Order_request_model->getOrdersRequests($where, $this->language);
        if ($order_requests) {
            $i = 0;
            foreach ($order_requests as $order_request) {
                $orders_arr[$i] = $order_request;
                $order_items = $this->Order_item_model->getOrderItems("order_items.OrderRequestID = " . $order_request['OrderRequestID']);
                if ($order_items) {
                    $j = 0;
                    foreach ($order_items as $order_item) {
                        $product_images = getProductImages($order_item['ProductID']);
                        $orders_arr[$i]['OrderItems'][$j] = $order_item;
                        $orders_arr[$i]['OrderItems'][$j]['ProductImage'] = $product_images[0]->ProductCompressedImage;

                        $orders_arr[$i]['OrderItems'][$j]['IsProductRatedByUser'] = '0';

                        $j++;
                    }
                } else {
                    $orders_arr[$i]['OrderItems'] = array();
                }
                $i++;
            }
        }

        $my_arr = array();
        if (!empty($orders_arr)) {
            foreach ($orders_arr as $item) {
                $my_arr[] = $item;
            }
        }

        return $my_arr;
    }

    private function getOrdersForUser($post_data, $filter = false)
    {
        $where_order_status = "";
        if (isset($post_data['OrderStatus']) && $post_data['OrderStatus'] != '') {
            if ($post_data['OrderStatus'] === 'Pending') {
                $where_order_status = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND ";
            } elseif ($post_data['OrderStatus'] === 'Completed') {
                $where_order_status = "orders_requests.OrderStatusID = 4 AND ";
            } elseif ($post_data['OrderStatus'] === 'Cancelled') {
                $where_order_status = "(orders_requests.OrderStatusID = 5 OR orders_requests.OrderStatusID = 6 OR orders_requests.OrderStatusID = 8) AND ";
            } else {
                $where_order_status = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND ";
            }
        }
        $orders_arr = array();

        $where = $where_order_status . "orders.UserID = " . $post_data['UserID'];
        $order_requests = $this->Order_request_model->getOrdersRequests($where, $this->language);
        if ($order_requests) {
            $i = 0;
            foreach ($order_requests as $order_request) {
                $orders_arr[$i] = $order_request;
                $order_items = $this->Order_item_model->getOrderItems("order_items.OrderRequestID = " . $order_request['OrderRequestID']);
                if ($order_items) {
                    $j = 0;
                    foreach ($order_items as $order_item) {
                        $product_images = getProductImages($order_item['ProductID']);
                        $orders_arr[$i]['OrderItems'][$j] = $order_item;
                        $orders_arr[$i]['OrderItems'][$j]['ProductImage'] = $product_images[0]->ProductCompressedImage;

                        $check_already_rated['ProductID'] = $order_item['ProductID'];
                        $check_already_rated['UserID'] = $post_data['UserID'];
                        $check_already_rated['OrderRequestID'] = $order_request['OrderRequestID'];
                        $is_product_rated = $this->Product_rating_model->getWithMultipleFields($check_already_rated);
                        $orders_arr[$i]['OrderItems'][$j]['IsProductRatedByUser'] = $is_product_rated ? '1' : '0';

                        $j++;
                    }
                } else {
                    $orders_arr[$i]['OrderItems'] = array();
                }
                $i++;
            }
        }

        $my_arr = array();
        if (!empty($orders_arr)) {
            foreach ($orders_arr as $item) {
                $my_arr[] = $item;
            }
        }

        return $my_arr;
    }

    public function getOrderRequestDetail_post()
    {
        $post_data = $this->post_data; // UserID, OrderRequestID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $where = 'orders_requests.OrderRequestID = ' . $post_data['OrderRequestID'];
            $order_request = $this->Order_request_model->getOrdersRequests($where, $this->language);
            if ($order_request) {
                $order_request = $order_request[0];
                $order_items = $this->Order_item_model->getOrderItems("order_items.OrderRequestID = " . $order_request['OrderRequestID']);
                if ($order_items) {
                    $i = 0;
                    foreach ($order_items as $order_item) {
                        $product_images = getProductImages($order_item['ProductID']);
                        $order_request['OrderItems'][$i] = $order_item;
                        $order_request['OrderItems'][$i]['ProductImage'] = $product_images[0]->ProductCompressedImage;
                        $i++;
                    }
                } else {
                    $order_request['OrderItems'] = array();
                }
            }
            $this->response([
                'status' => 200,
                'order_request' => $order_request
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }



    public function updateOrderStatus_post()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // UserID, OrderRequestID, OrderStatus (2 => Pending, 3 => Dispatched, 4 => Delivered, 5 => Cancelled)
        if (!empty($post_data)) {
            if (isset($post_data['OrderStatus']) && $post_data['OrderStatus'] != '') {
                $OrderStatus = $post_data['OrderStatus'];
                if ($OrderStatus == 2) // Approved - Pending Payment
                {
                   // $this->approveOrder($post_data);
                } elseif ($OrderStatus == 3) // Approved - Payment Done
                {
                    //$this->paymentDoneForOrder($post_data);
                } elseif ($OrderStatus == 4) // Order Completed
                {
                    $this->markOrderAsDelivered($post_data);
                } elseif ($OrderStatus == 5) // Cancelled By Seller
                {
                   // $this->orderCancelledBySeller($post_data);
                } elseif ($OrderStatus == 6) // Cancelled By Seller
                {
                    $this->orderReturned($post_data);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }



    private function markOrderAsDelivered($post_data = array())
    {

        $OrderID = $post_data['OrderID'];
        $OTP     = $post_data['OTP'];


        $OrderData = $this->Order_model->get($OrderID,true,'OrderID');
        //print_rm($OrderData);

        if($OrderData['DeliveryOTP'] == $OTP){
            $this->Order_model->update(array('Status' => 4),array('OrderID' => $OrderID));
            $this->response([
                'status' => 200,
                'message' => lang('update_successfully')
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }

        

        



    }

    private function orderReturned($post_data = array())
    {

        $OrderID = $post_data['OrderID'];
        $return_reason     = $post_data['ReturnedReason'];

        $this->Order_model->update(array('Status' => 6,'ReturnedReason' => $return_reason),array('OrderID' => $OrderID));
            $this->response([
                'status' => 200,
                'message' => lang('update_successfully')
            ], REST_Controller::HTTP_OK);

        



    }

    

    public function updateOrderStatus_old_post()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // UserID, OrderRequestID, OrderStatus (2 => Approved - Pending Payment, 3 => Approved - Payment Done, 4 => Order Completed, 5 => Cancelled By Seller, 6 => Cancelled By Buyer, 7 => Order Dispatched), VerificationCode (will only come with order status 4), OrderCancellationReasonID (will only come with order status 5 AND 6)
        if (!empty($post_data)) {
            if (isset($post_data['OrderStatus']) && $post_data['OrderStatus'] != '') {
                $OrderStatus = $post_data['OrderStatus'];
                if ($OrderStatus == 2) // Approved - Pending Payment
                {
                    $this->approveOrder($post_data);
                } elseif ($OrderStatus == 3) // Approved - Payment Done
                {
                    $this->paymentDoneForOrder($post_data);
                } elseif ($OrderStatus == 4) // Order Completed
                {
                    $this->markOrderAsCompleted($post_data);
                } elseif ($OrderStatus == 5) // Cancelled By Seller
                {
                    $this->orderCancelledBySeller($post_data);
                } elseif ($OrderStatus == 6) // Cancelled By Buyer
                {
                    $this->orderCancelledByBuyer($post_data);
                } elseif ($OrderStatus == 7) // Order Dispatched
                {
                    $this->markOrderAsDispatched($post_data);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    private function approveOrder($post_data)
    {
        $OrderRequest = $this->Order_request_model->get($post_data['OrderRequestID'], true, 'OrderRequestID');
        $Order = $this->Order_model->get($OrderRequest['OrderID'], true, 'OrderID');
        if ($OrderRequest['OrderStatusID'] == 1) {

            $update['OrderLastStatusID'] = $OrderRequest['OrderStatusID'];
            $update['OrderStatusID'] = 2;
            $update['DeliveryDays'] = $post_data['DeliveryDays'];
            $update['TotalAmount'] = $post_data['TotalAmount']; // This will be total of all items in order
            $update['Discount'] = $post_data['Discount'];
            $update['VatPercentageApplied'] = $post_data['VatPercentageApplied'];
            $update['VatAmountApplied'] = (($post_data['TotalAmount'] - $post_data['Discount']) * $post_data['VatPercentageApplied']) / 100;
            $update['ActualDeliveryCharges'] = $post_data['ActualDeliveryCharges'];
            $update['AdditionalDeliveryCharges'] = $post_data['AdditionalDeliveryCharges'];
            $update['GrandTotal'] = $post_data['TotalAmount'] + $post_data['ActualDeliveryCharges'] + (1 * $post_data['AdditionalDeliveryCharges']) - $post_data['Discount'] + $update['VatAmountApplied'];

            if ($update['GrandTotal'] > 0) {
                $update_by['OrderRequestID'] = $post_data['OrderRequestID'];
                $this->Order_request_model->update($update, $update_by);
                $OrderRequest = $this->Order_request_model->get($post_data['OrderRequestID'], true, 'OrderRequestID');
                // Sending notification To Customer
                $data['LoggedInUserID'] = $Order['UserID']; // the person who will receive this notification
                $data['UserType'] = 'user';
                $data['Type'] = 'order';
                $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                $data['OrderID'] = $OrderRequest['OrderID'];
                $data['OrderRequestID'] = $OrderRequest['OrderRequestID'];
                $data['NotificationTypeID'] = 11;
                $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                $UserInfo = getUserInfo($data['UserID'], $this->language);
                $data['NotificationTextEn'] = "@"."".$UserInfo->BoothUserName." has approved your order request with order track number " . $OrderRequest['OrderTrackID'];
                $data['NotificationTextAr'] = "وافق البائع على طلبك مع رقم تتبع الطلب " . $OrderRequest['OrderTrackID'];
                $data['CreatedAt'] = time();
                
                $mentioned_user_ids = array($data['UserID']);
                $mentioned_user_names = array($UserInfo->BoothUserName);
                $mentioned_user_types = array('booth');
                log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                $data['OrderStatus'] = 'Approved - Pending Payment';
                $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $Order['UserID']);
               // $this->SendOrderApprovalEmailToCustomer($Order['UserID'], $OrderRequest['OrderTrackID']);
                $this->response([
                    'status' => 200,
                    'message' => lang('order_approved'),
                    'order_request_detai' => $OrderRequest
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('order_status_cant_be_changed')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('order_status_cant_be_changed')
            ], REST_Controller::HTTP_OK);
        }
    }

    private function paymentDoneForOrder($post_data)
    {
        $OrderRequest = $this->Order_request_model->get($post_data['OrderRequestID'], true, 'OrderRequestID');
        $Order = $this->Order_model->get($OrderRequest['OrderID'], true, 'OrderID');
        if ($OrderRequest['OrderStatusID'] == 2) {
            if (isset($post_data['PromoCodeUsed']) && isset($post_data['PromoCodeDiscount']) && $post_data['PromoCodeDiscount'] > 0) {
                /*$update['PromoCodeUsed'] = $post_data['PromoCodeUsed'];
                $update['PromoCodeDiscount'] = $post_data['PromoCodeDiscount'];
                $update['GrandTotal'] = $OrderRequest['GrandTotal'] - $post_data['PromoCodeDiscount'];*/

                /*$update['VatAmountApplied'] = (($OrderRequest['TotalAmount'] - $OrderRequest['Discount'] - $OrderRequest['PromoCodeDiscount']) * $OrderRequest['VatPercentageApplied']) / 100;
                $update['GrandTotal'] = $OrderRequest['TotalAmount'] - $OrderRequest['Discount'] - $OrderRequest['PromoCodeDiscount'] + $OrderRequest['ActualDeliveryCharges'] + (1 * $OrderRequest['AdditionalDeliveryCharges']) + $update['VatAmountApplied'];*/
            }
            $update['OrderLastStatusID'] = $OrderRequest['OrderStatusID'];
            $update['OrderStatusID'] = 3;
            $update['DeliveryDate'] = getDateBySkippingWeekend($OrderRequest['DeliveryDays']);
            $update['PointsUsed'] = isset($post_data['PointsUsed']) ? $post_data['PointsUsed'] : 0;
            $update['PromoCodeUsed'] = isset($post_data['PromoCodeUsed']) ? $post_data['PromoCodeUsed'] : '';
            $update['PromoCodeDiscount'] = isset($post_data['PromoCodeDiscount']) && $post_data['PromoCodeDiscount'] > 0 ? $post_data['PromoCodeDiscount'] : 0;

            $update['VatAmountApplied'] = (($OrderRequest['TotalAmount'] - $OrderRequest['Discount'] - $update['PromoCodeDiscount'] - $update['PointsUsed']) * $OrderRequest['VatPercentageApplied']) / 100;
            $update['GrandTotal'] = $OrderRequest['TotalAmount'] - $OrderRequest['Discount'] - $update['PromoCodeDiscount'] - $update['PointsUsed'] + $OrderRequest['ActualDeliveryCharges'] + (1 * $OrderRequest['AdditionalDeliveryCharges']) + $update['VatAmountApplied'];

            $update['PaymentMethod'] = $post_data['PaymentMethod'];
            $update_by['OrderRequestID'] = $post_data['OrderRequestID'];
            $this->Order_request_model->update($update, $update_by);

            // Deducting used points for user
            if ($update['PointsUsed'] > 0) {
                $user = $this->User_model->get($Order['UserID'], false, 'UserID');
                $Points = $user->Points - $update['PointsUsed'];
                if ($Points >= 0) { // we don't want to do points in negative
                    $this->User_model->update(array('Points' => $Points), array('UserID' => $user->UserID));

                    // saving in user_points_history table
                    $points_history_data = array();
                    $points_history_data['UserID'] = $user->UserID;
                    $points_history_data['TransactionType'] = 'Used';
                    $points_history_data['Type'] = 'Order';
                    $points_history_data['Points'] = $update['PointsUsed'];
                    $points_history_data['OrderDate'] = $Order['CreatedAt'];
                    $points_history_data['OrderNumber'] = $OrderRequest['OrderTrackID'];
                    $points_history_data['CreatedAt'] = time();
                    $this->User_points_history_model->save($points_history_data);
                }
            }

            // Sending notification To Booth
            $data['LoggedInUserID'] = $OrderRequest['BoothID']; // the person who will receive this notification
            $data['UserType'] = 'booth';
            $data['Type'] = 'order';
            $data['UserID'] = $Order['UserID']; // the person who is making this notification
            $data['OrderID'] = $OrderRequest['OrderID'];
            $data['OrderRequestID'] = $OrderRequest['OrderRequestID'];
            $data['NotificationTypeID'] = 12;
            $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
            $UserInfo = getUserInfo($data['UserID'], $this->language);
            $data['NotificationTextEn'] = "Order No. ".$OrderRequest['OrderTrackID']." has been confirmed. You can dispatch the shipment for delivery.";
            $data['NotificationTextAr'] = "
تم تأكيد رقم الطلب ".$OrderRequest['OrderTrackID']." ، يمكنك الآن إرسال الشحنة";
            $data['CreatedAt'] = time();
            $data['OrderStatus'] = 'Approved - Confirmed';
            $mentioned_user_ids = array();
            $mentioned_user_names = array();
            $mentioned_user_types = array();
            log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
            $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $OrderRequest['BoothID']);
            //$this->SendOrderPaymentDoneEmailToCustomer($Order['UserID'], $OrderRequest['OrderTrackID']);
            //$this->SendOrderPaymentDoneEmailToBooth($OrderRequest['BoothID'], $OrderRequest['OrderTrackID']);//just send notification
            $OrderRequest = $this->Order_request_model->get($post_data['OrderRequestID'], true, 'OrderRequestID');
            $this->response([
                'status' => 200,
                'message' => lang('order_payment_done'),
                'order_request' => $OrderRequest
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('order_status_cant_be_changed')
            ], REST_Controller::HTTP_OK);
        }
    }

    private function markOrderAsDispatched($post_data)
    {
        $OrderRequest = $this->Order_request_model->get($post_data['OrderRequestID'], true, 'OrderRequestID');
        $Order = $this->Order_model->get($OrderRequest['OrderID'], true, 'OrderID');
        if ($OrderRequest['OrderStatusID'] == 3) {
            $verification_code = random_password(4, true);
            $update['OrderLastStatusID'] = $OrderRequest['OrderStatusID'];
            $update['OrderStatusID'] = 7;
            $update['OrderRequestVerificationCode'] = $verification_code;
            $update_by['OrderRequestID'] = $post_data['OrderRequestID'];
            $this->Order_request_model->update($update, $update_by);

            // Sending notification To Customer
            $data['LoggedInUserID'] = $Order['UserID']; // the person who will receive this notification
            $data['UserType'] = 'user';
            $data['Type'] = 'order';
            $data['UserID'] = $post_data['UserID']; // the person who is making this notification
            $data['OrderID'] = $OrderRequest['OrderID'];
            $data['OrderRequestID'] = $OrderRequest['OrderRequestID'];
            $data['NotificationTypeID'] = 14;
            $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
            $UserInfo = getUserInfo($data['UserID'], $this->language);

            $BoothInfo = getUserInfo($OrderRequest['BoothID'], $this->language);
            $data['NotificationTextEn'] = "@".$BoothInfo->BoothUserName." has dispatched your order request with order track number " . $OrderRequest['OrderTrackID'];
            $data['NotificationTextAr'] = "أرسل البائع طلب طلبك مع رقم تتبع الطلب " . $OrderRequest['OrderTrackID'];
            $data['CreatedAt'] = time();
            $data['OrderStatus'] = 'Dispatched';
            $mentioned_user_ids = array($BoothInfo->UserID);
            $mentioned_user_names = array($BoothInfo->BoothUserName);
            $mentioned_user_types = array('booth');
            log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
            //$res = sendNotification('Booth', $data['NotificationTextEn'], $data, $Order['UserID']);//only send sms here
            $this->SendOrderDispatchedEmailToCustomer($Order['UserID'], $OrderRequest['OrderTrackID'], $verification_code,$BoothInfo->BoothUserName);
            $this->response([
                'status' => 200,
                'message' => lang('order_dispatched')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('order_status_cant_be_changed')
            ], REST_Controller::HTTP_OK);
        }
    }

    private function markOrderAsCompleted($post_data)
    {
        $OrderRequest = $this->Order_request_model->get($post_data['OrderRequestID'], true, 'OrderRequestID');
        $Order = $this->Order_model->get($OrderRequest['OrderID'], true, 'OrderID');
        if ($OrderRequest['OrderStatusID'] == 7) {
            if (isset($post_data['VerificationCode']) && ($post_data['VerificationCode'] == $OrderRequest['OrderRequestVerificationCode'])) {
                $update['OrderLastStatusID'] = $OrderRequest['OrderStatusID'];
                $update['OrderStatusID'] = 4;
                $update_by['OrderRequestID'] = $post_data['OrderRequestID'];
                $this->Order_request_model->update($update, $update_by);

                // Sending notification To Customer
                $data['LoggedInUserID'] = $Order['UserID']; // the person who will receive this notification
                $data['UserType'] = 'user';
                $data['Type'] = 'order';
                $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                $data['OrderID'] = $OrderRequest['OrderID'];
                $data['OrderRequestID'] = $OrderRequest['OrderRequestID'];
                $data['NotificationTypeID'] = 13;
                $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                $UserInfo = getUserInfo($data['UserID'], $this->language);
                $data['NotificationTextEn'] = "Your order request has completed with order track number " . $OrderRequest['OrderTrackID'];
                $data['NotificationTextAr'] = "اكتمل طلب الطلب برقم تتبع الطلب " . $OrderRequest['OrderTrackID'];
                $data['CreatedAt'] = time();
                $data['OrderStatus'] = 'Completed';
                $mentioned_user_ids = array();
                $mentioned_user_names = array();
                $mentioned_user_types = array();
                log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $Order['UserID']);
                $this->SendOrderCompletedEmailToCustomer($Order['UserID'], $OrderRequest['OrderTrackID']);
                $this->SendOrderCompletedEmailToBooth($OrderRequest['BoothID'], $OrderRequest['OrderTrackID']);
                $this->response([
                    'status' => 200,
                    'message' => lang('order_completed')
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('order_status_cant_be_changed_wrong_code')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('order_status_cant_be_changed')
            ], REST_Controller::HTTP_OK);
        }
    }

    private function orderCancelledBySeller($post_data)
    {
        $OrderRequest = $this->Order_request_model->get($post_data['OrderRequestID'], true, 'OrderRequestID');
        $Order = $this->Order_model->get($OrderRequest['OrderID'], true, 'OrderID');
        if ($OrderRequest['OrderStatusID'] == 1) { // Pending approval OR approved but payment pending
            $update['OrderLastStatusID'] = $OrderRequest['OrderStatusID'];
            $update['OrderStatusID'] = 5;
            $update['OrderCancellationReasonID'] = $post_data['OrderCancellationReasonID'];
            $update['OrderCancellationReasonIfOther
'] = (isset($post_data['OrderCancellationReasonIfOther
']) ? $post_data['OrderCancellationReasonIfOther
'] : '');
            $update_by['OrderRequestID'] = $post_data['OrderRequestID'];
            $this->Order_request_model->update($update, $update_by);

            // Sending notification To Customer
            $data['LoggedInUserID'] = $Order['UserID']; // the person who will receive this notification
            $data['UserType'] = 'user';
            $data['Type'] = 'order';
            $data['UserID'] = $post_data['UserID']; // the person who is making this notification
            $data['OrderID'] = $OrderRequest['OrderID'];
            $data['OrderRequestID'] = $OrderRequest['OrderRequestID'];
            $data['NotificationTypeID'] = 9;
            $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
            $UserInfo = getUserInfo($data['UserID'], $this->language);
            $BoothInfo = getUserInfo($OrderRequest['BoothID'], $this->language);
            
            $data['NotificationTextEn'] = "@".$BoothInfo->BoothUserName." has cancelled your order request with order track number " . $OrderRequest['OrderTrackID'];
            $data['NotificationTextAr'] = " قام البائع بإلغاء طلب طلبك مع رقم تتبع الطلب " . $OrderRequest['OrderTrackID'];
            $data['CreatedAt'] = time();
             $data['OrderStatus'] = 'Cancelled By Seller';
            $mentioned_user_ids = array($BoothInfo->UserID);
            $mentioned_user_names = array($BoothInfo->BoothUserName);
            $mentioned_user_types = array('booth');
            log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
            $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $Order['UserID']);
            $this->SendOrderCancelEmailToCustomer($Order['UserID'], $OrderRequest['OrderTrackID']);
            $this->response([
                'status' => 200,
                'message' => lang('order_cancelled')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('order_cant_be_cancelled')
            ], REST_Controller::HTTP_OK);
        }
    }

    private function orderCancelledByBuyer($post_data)
    {
        $OrderRequest = $this->Order_request_model->get($post_data['OrderRequestID'], true, 'OrderRequestID');
        $Order = $this->Order_model->get($OrderRequest['OrderID'], true, 'OrderID');
        if ($OrderRequest['OrderStatusID'] == 1 || $OrderRequest['OrderStatusID'] == 2) { // Pending approval OR approved but payment pending
            $update['OrderLastStatusID'] = $OrderRequest['OrderStatusID'];
            $update['OrderStatusID'] = 6;
            $update['OrderCancellationReasonID'] = $post_data['OrderCancellationReasonID'];
            $update_by['OrderRequestID'] = $post_data['OrderRequestID'];
            $update['OrderCancellationReasonIfOther
'] = (isset($post_data['OrderCancellationReasonIfOther
']) ? $post_data['OrderCancellationReasonIfOther
'] : '');
            $this->Order_request_model->update($update, $update_by);

            // Sending notification To Booth
            $data['LoggedInUserID'] = $OrderRequest['BoothID']; // the person who will receive this notification
            $data['UserType'] = 'booth';
            $data['Type'] = 'order';
            $data['UserID'] = $post_data['UserID']; // the person who is making this notification
            $data['OrderID'] = $OrderRequest['OrderID'];
            $data['OrderRequestID'] = $OrderRequest['OrderRequestID'];
            $data['NotificationTypeID'] = 10;
            $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
            $UserInfo = getUserInfo($data['UserID'], $this->language);
            $data['NotificationTextEn'] = "@".$UserInfo->UserName." has cancelled an order request with order track number " . $OrderRequest['OrderTrackID'];
            $data['NotificationTextAr'] = "قام المشتري بإلغاء طلب طلب برقم تتبع الطلب " . $OrderRequest['OrderTrackID'];
            $data['CreatedAt'] = time();
            $data['OrderStatus'] = 'Cancelled By Buyer';
            $mentioned_user_ids = array($UserInfo->UserID);
            $mentioned_user_names = array($UserInfo->UserName);
            $mentioned_user_types = array('user');
            log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
            $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $OrderRequest['BoothID']);
            $this->SendOrderCancelEmailToBooth($OrderRequest['BoothID'], $OrderRequest['OrderTrackID']);
            $this->response([
                'status' => 200,
                'message' => lang('order_cancelled')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('order_cant_be_cancelled')
            ], REST_Controller::HTTP_OK);
        }
    }

    private function SendOrderApprovalEmailToCustomer($UserID, $OrderTrackID)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID, $this->language);
        $email_template = get_email_template(8, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['FullName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
        sendSms($user_info['Mobile'], $message);
    }

    private function SendOrderPaymentDoneEmailToCustomer($UserID, $OrderTrackID)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID, $this->language);
        $email_template = get_email_template(9, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['FullName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
        sendSms($user_info['Mobile'], $message);
    }

    private function SendOrderPaymentDoneEmailToBooth($BoothID, $OrderTrackID)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $BoothID, $this->language);
        $email_template = get_email_template(13, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['BoothName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to booth
        sendSms($user_info['Mobile'], $message);
    }

    public function SendOrderCompletedEmailToCustomer($UserID, $OrderTrackID)
    {
        

        $order_details  =  $this->Order_model->getOrdersRequest('orders_requests.OrderTrackID = '.$OrderTrackID);

        foreach ($order_details as $key => $value) {
              $requests[$key] = $value;

              $requests[$key]['items'] = $this->Order_model->getOrderItems($value['OrderRequestID']);
              //echo $this->db->last_query();exit;
        }

       
        $data['requests'] = $requests;

        

        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID, $this->language);
        $email_template = get_email_template(10, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['FullName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        
        $data['message'] = $message;

        $message = $this->load->view('emails/order_confirmation_email',$data, true);




        
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        $data['message'];
        sendEmail($data);

        // Sending sms to customer
        //sendSms($user_info['Mobile'], $message);// just send notification and email here
    }

    public function SendOrderCompletedEmailToBooth($UserID, $OrderTrackID)
    {
        

        
        

        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID, $this->language);
        $email_template = get_email_template(16, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['FullName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        $data['message'];
        sendEmail($data);

        // Sending sms to customer
        //sendSms($user_info['Mobile'], $message);// just send notification and email here
    }

    private function SendOrderDispatchedEmailToCustomer($UserID, $OrderTrackID, $VerificationCode,$BoothName = '')
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID, $this->language);
        $email_template = get_email_template(11, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['FullName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $message = str_replace("{{verification_code}}", $VerificationCode, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer

        if($user_info['LastLangState'] == 'EN'){
            $message = "Hi, your order #".$OrderTrackID." from ".$BoothName." has been dispatched! Please submit the delivery code(".$VerificationCode.") to the booth once you receive your order to mark the order completed.";
        }else{
            $message = "
مرحبا، طلبك #".$OrderTrackID." من بوث ".$BoothName." في الطريق اليك!
الرجاء تسليم كود التوصيل(".$VerificationCode.") للبوث عند استلام طلبك ليتم اغلاق الطلب!";
        }



        sendSms($user_info['Mobile'], $message);
    }

    private function SendOrderCancelEmailToCustomer($UserID, $OrderTrackID)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID, $this->language);
        $email_template = get_email_template(5, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['FullName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
       // sendSms($user_info['Mobile'], $message);//only send email and notification
    }

    private function SendOrderCancelEmailToBooth($UserID, $OrderTrackID)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID, $this->language);
        $email_template = get_email_template(6, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['BoothName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
       // sendSms($user_info['Mobile'], $message);//just send email and notification
    }

    public function createTicket_post()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // UserID, OrderRequestID, Message
        if (!empty($post_data)) {
            $checkTicket = $this->Ticket_model->get($post_data['OrderRequestID'], false, 'OrderRequestID');
            if ($checkTicket) {
                $this->response([
                    'status' => 402,
                    'message' => lang('ticket_already_raised')
                ], REST_Controller::HTTP_OK);
            } else {
                $ticket_data['OrderRequestID'] = $post_data['OrderRequestID'];
                $ticket_data['TicketNumber'] = str_pad($post_data['OrderRequestID'], 5, '0', STR_PAD_LEFT);
                $ticket_data['CreatedAt'] = time();
                $ticket_id = $this->Ticket_model->save($ticket_data);
                if ($ticket_id > 0) {
                    $ticket_comment_data['TicketID'] = $ticket_id;
                    $ticket_comment_data['UserID'] = $post_data['UserID'];
                    $ticket_comment_data['Message'] = $post_data['Message'];
                    $ticket_comment_data['CreatedAt'] = time();
                    $this->Ticket_comment_model->save($ticket_comment_data);
                    $this->response([
                        'status' => 200,
                        'message' => lang('ticket_raised')
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function themes_get()
    {
        $post_data = $this->post_data;
        $this->_isUserAuthorized();
        $themes_arr = array();
        $themes = $this->Theme_model->getMultipleRows(array('IsActive' => 1), true);
        if ($themes) {
            $i = 0;
            foreach ($themes as $theme) {
                $themes_arr[$i] = $theme;
                $tops = $this->Model_general->getMultipleRows('theme_styles', array('ParentID' => 0, 'ThemeID' => $theme['ThemeID'], 'IsActive' => 1), true, 'asc', 'ThemeStyleID');
                if ($tops) {
                    $j = 0;
                    foreach ($tops as $top) {
                        $themes_arr[$i]['tops'][$j] = $top;
                        $bottoms = $this->Model_general->getMultipleRows('theme_styles', array('ParentID' => $top['ThemeStyleID'], 'ThemeID' => $theme['ThemeID'], 'IsActive' => 1), true, 'asc', 'ThemeStyleID');
                        if ($bottoms) {
                            $k = 0;
                            foreach ($bottoms as $bottom) {
                                $themes_arr[$i]['tops'][$j]['bottoms'][$k] = $bottom;
                                $k++;
                            }
                        } else {
                            $themes_arr[$i]['tops'][$j]['bottoms'] = array();
                        }
                        $j++;
                    }
                } else {
                    $themes_arr[$i]['tops'] = array();
                }

                $i++;
            }
        }
        $this->response([
            'status' => 200,
            'themes' => $themes_arr
        ], REST_Controller::HTTP_OK);
    }

    public function getQuestions_post()
    {
        $post_data = $this->post_data; // UserID, QuestionID (Optional)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            $questions_arr = array();
            $user_categories_arr = array();
            $Type  = 'booth';
            if(isset($post_data['Type'])){
                 $Type  = $post_data['Type'];
            }
            $user_categories = $this->User_category_model->getMultipleRows(array('UserID' => $post_data['UserID'], 'Type' => $Type));
            if ($user_categories) {
                foreach ($user_categories as $user_category) {
                    $user_categories_arr[] = $user_category->CategoryID;
                }
                if (isset($post_data['QuestionID']) && $post_data['QuestionID'] > 0) {
                    $where = "questions.QuestionID = " . $post_data['QuestionID'];
                    $user_categories_arr = false;
                } else {
                    $where = "questions.UserID != " . $post_data['UserID'];
                }

                $where .= " AND users.PackageExpiry >= CURDATE()";
                $questions = $this->Question_model->getQuestions($where, $this->language, $user_categories_arr, 'categories', $limit, $start);


                if ($questions) {
                    $i = 0;
                    foreach ($questions as $question) {
                        $questions_arr[$i] = $question;
                        $questions_arr[$i]['ItemType'] = 'Question';
                        $question_images = $this->Question_image_model->getMultipleRows(array('QuestionID' => $question['QuestionID']));
                        $comments = $this->Question_comment_model->getQuestionComments("question_comments.QuestionID = " . $question['QuestionID'], $this->language);
                        $CommentCount = 0;
                        if (!empty($comments)) {
                            if (isset($post_data['UserID'])) {
                                $my_arr = array();
                                $c_count = 0;
                                foreach ($comments as $key => $value) {
                                    $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                    if ($IsBlocked) {
                                        continue;
                                    }
                                    $my_arr[$c_count] = $value;
                                    $c_count++;
                                }
                                $comments = $my_arr;
                            }
                            $CommentCount = count($comments);
                        }
                        $questions_arr[$i]['CommentCount'] = (string)$CommentCount;
                        $questions_arr[$i]['QuestionImages'] = $question_images ? $question_images : array();
                        $i++;
                    }
                }
            }

            if (isset($post_data['QuestionID']) && $post_data['QuestionID'] > 0) {
                $questions_arr = $questions_arr[0];
            }

            $this->response([
                'status' => 200,
                'questions' => $questions_arr
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function askQuestion_post()
    {
        $post_data = $this->post_data; // UserID, CategoryID, QuestionDescription, ProductImagesCount, ProductImage1, ProductImage2, ProductImage3 etc...
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $question_data['UserID'] = $post_data['UserID'];
            $question_data['CategoryID'] = $post_data['CategoryID'];
            $question_data['QuestionDescription'] = $post_data['QuestionDescription'];
            $question_data['QuestionAskedAt'] = time();
            $inserted_id = $this->Question_model->save($question_data);
            if ($inserted_id > 0) {
                // Uploading Question Images
               $activity_data = array();
                $activity_data['UserID'] = $post_data['UserID'];
               // $activity_data['ActivityDoneAs'] = (isset($post_data['CommentedAs']) ? $post_data['CommentedAs'] : '');
                $activity_data['UserType'] = $post_data['CommentedAs'];
                $activity_data['CommentedAs'] = $post_data['CommentedAs'];
                $activity_data['Type'] = 'ask_question';
                $activity_data['NotificationTypeID'] = 1;
                $activity_data['QuestionID'] = $inserted_id;
                $UserInfo = getUserInfo($activity_data['UserID'], $this->language);
                $activity_data['NotificationTextEn'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " asked a question";
                $activity_data['NotificationTextAr'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " قام بإضافة استفسار";
                $activity_data['CreatedAt'] = time();
                $mentioned_user_ids = array($UserInfo->UserID);

                $mentioned_user_names = array(($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ));
                $mentioned_user_types = array($post_data['CommentedAs']);
                log_friend_activity($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);

                ini_set('memory_limit', '-1');
                $question_images_count = (isset($post_data['ProductImagesCount']) && $post_data['ProductImagesCount'] > 0 ? $post_data['ProductImagesCount'] : 0);
                for ($i = 1; $i <= $question_images_count; $i++) {
                    $filename = date('Ymdhsi') . rand(9999, 99999999999);
                    $question_image_data['QuestionID'] = $inserted_id;
                    $question_image_data['Image'] = uploadImage('ProductImage' . $i, "uploads/products");//uploadFileFromBase64($post_data['ProductImage' . $i], "uploads/products/");
                    $question_image_data['CompressedImage'] = compress($question_image_data['Image'], "uploads/products/compressed/product-image-" . $filename . '.png');
                    $this->Question_image_model->save($question_image_data);
                }
                $question = $this->Question_model->get($inserted_id, false, 'QuestionID');
                $question_images = $this->Question_image_model->getMultipleRows(array('QuestionID' => $question->QuestionID));
                $question->QuestionImages = $question_images ? $question_images : array();
                $this->response([
                    'status' => 200,
                    'message' => lang('save_successfully'),
                    'question' => $question
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function deleteQuestion_post() // QuestionID
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $deleted_by['QuestionID'] = $post_data['QuestionID'];
            $this->Question_model->delete($deleted_by);
            $this->Question_comment_model->delete($deleted_by);
            $this->Question_image_model->delete($deleted_by);
            $this->Question_reported_model->delete($deleted_by);
            $this->User_notification_model->delete($deleted_by);
            $this->User_friends_notification_model->delete($deleted_by);

            $this->response([
                'status' => 200,
                'message' => lang('deleted_successfully')
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function questionComment_post()
    {
        $post_data = $this->post_data; // QuestionID, UserID, Comment, CommentedAs, MentionedUsers (Comma Separated)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $MentionedUsers = $post_data['MentionedUsers'];
            unset($post_data['MentionedUsers']);
             $question_data  = $this->Question_model->get($post_data['QuestionID'],false,'QuestionID');
            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $question_data->UserID,$post_data['CommentedAs']);
            if ($IsBlocked) {
                 $this->response([
                        'status' => 402,
                        'message' => lang('you_cannot_do')
                    ], REST_Controller::HTTP_OK);

                 exit;
            }

            $checkQuestion = $this->Question_model->getQuestionDetail("users.PackageExpiry >= CURDATE() AND questions.QuestionID = ".$post_data['QuestionID']);
            if (!empty($checkQuestion)) {
                $this->response([
                    'status' => 402,
                    'message' => lang('you_cannot_do')
                ], REST_Controller::HTTP_OK);
            }
            if ($MentionedUsers != '') {
                $MentionedUsers = explode(',', $MentionedUsers);
                $MentionedUserID = array();
                $MentionedUserName = array();
                $MentionedUserType = array();
                if (!empty($MentionedUsers)) {
                    $i = 0;
                    foreach ($MentionedUsers as $mentionedUser) {
                        // checking if this username exist in db
                        $UserNameExist = $this->User_model->getWithMultipleFields(array('UserName' => $mentionedUser));
                        $BoothUserNameExist = $this->User_model->getWithMultipleFields(array('BoothUserName' => $mentionedUser));
                        if ($UserNameExist) {
                            $MentionedUserID[$i] = $UserNameExist->UserID;
                            $MentionedUserName[$i] = $UserNameExist->UserName;
                            $MentionedUserType[$i] = 'user';
                            $i++;
                        } elseif ($BoothUserNameExist) {
                            $MentionedUserID[$i] = $BoothUserNameExist->UserID;
                            $MentionedUserName[$i] = $BoothUserNameExist->BoothUserName;
                            $MentionedUserType[$i] = 'booth';
                            $i++;
                        }
                    }
                }

                if (!empty($MentionedUserID)) {
                    $post_data['MentionedUserID'] = implode(',', $MentionedUserID);
                    $post_data['MentionedUserName'] = implode(',', $MentionedUserName);
                    $post_data['MentionedUserType'] = implode(',', $MentionedUserType);
                }
            }
            $post_data['CreatedAt'] = time();
            $inserted_id = $this->Question_comment_model->save($post_data);
            if ($inserted_id > 0) {
                $question_detail = $this->Question_model->get($post_data['QuestionID'], true, 'QuestionID');

                if ($question_detail['UserID'] !== $post_data['UserID']) {
                    // saving notification
                    $data['LoggedInUserID'] = $question_detail['UserID']; // the person whose post is this, who will receive this notification
                    $data['UserType'] = 'user';
                    $data['Type'] = 'comment';
                    $data['ActivityDoneAs'] = (isset($post_data['CommentedAs']) ? $post_data['CommentedAs'] : '');
                    $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                    $data['QuestionID'] = $post_data['QuestionID'];
                    $data['QuestionCommentID'] = $inserted_id;
                    $data['NotificationTypeID'] = 7;
                    $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                    $UserInfo = getUserInfo($data['UserID'], $this->language);
                    $data['NotificationTextEn'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " anwered your question";
                    $data['NotificationTextAr'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " اضف اجابة على استفسارك";
                    $data['CreatedAt'] = time();
                    $mentioned_user_ids = array($UserInfo->UserID);
                    $mentioned_user_names = array(($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ));
                    $mentioned_user_types = array($post_data['CommentedAs']);
                    log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                    $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $question_detail['UserID']);
                }

                // saving activity
                $activity_data['CommentedAs'] = $post_data['CommentedAs'];
                $activity_data['UserID'] = $post_data['UserID'];
               // $activity_data['ActivityDoneAs'] = (isset($post_data['CommentedAs']) ? $post_data['CommentedAs'] : '');
                $activity_data['UserType'] = 'user';
                $activity_data['Type'] = 'question_comment';
                $activity_data['NotificationTypeID'] = 19;
                $activity_data['QuestionID'] = $post_data['QuestionID'];
                $activity_data['QuestionCommentID'] = $inserted_id;
                $UserInfo = getUserInfo($activity_data['UserID'], $this->language);
                $activity_data['NotificationTextEn'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " anwered your question";
                $activity_data['NotificationTextAr'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " اضف اجابة على استفسارك";
                $activity_data['CreatedAt'] = time();
                $mentioned_user_ids = array($UserInfo->UserID);

                $mentioned_user_names = array(($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ));
                $mentioned_user_types = array($post_data['CommentedAs']);
                log_friend_activity($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);

                $comment_detail = $this->Question_comment_model->getQuestionComments("question_comments.QuestionCommentID = " . $inserted_id, $this->language);
                $comment_detail = $comment_detail[0];
                $mentioned_users_info = array();
                if ($comment_detail->MentionedUserID !== '') {
                    $user_ids = explode(',', $comment_detail->MentionedUserID);
                    $user_names = explode(',', $comment_detail->MentionedUserName);
                    $user_types = explode(',', $comment_detail->MentionedUserType);
                    $j = 0;
                    foreach ($user_ids as $key => $id) {

                        // saving notification
                        $data['LoggedInUserID'] = $id; // the person whose post is this, who will receive this notification
                        $data['UserType'] = $user_types[$key];
                        $data['ActivityDoneAs'] = (isset($post_data['CommentedAs']) ? $post_data['CommentedAs'] : '');
                        $data['Type'] = 'comment';
                        $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                        $data['QuestionID'] = $post_data['QuestionID'];
                        $data['QuestionCommentID'] = $inserted_id;
                        $data['NotificationTypeID'] = 5;
                        $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                        $UserInfo = getUserInfo($data['UserID'], $this->language);
                        $data['NotificationTextEn'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " has mentioned you in an answer";
                        $data['NotificationTextAr'] = '@' . ($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ) . " اضافك في اجابة على استفسار";
                        $data['CreatedAt'] = time();
                        $mentioned_user_ids = array($UserInfo->UserID);
                        $mentioned_user_names = array(($post_data['CommentedAs'] == 'user' ? $UserInfo->UserName : $UserInfo->BoothUserName ));
                        $mentioned_user_types = array($post_data['CommentedAs']);
                        log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                        $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $id);

                        $userInfoForComment = $this->User_model->getUsers("users.UserID = " . $id, $this->language);
                        if (!empty($userInfoForComment)) {
                            $userInfoForComment = $userInfoForComment[0];
                            $mentioned_users_info[$j]['UserID'] = $userInfoForComment->UserID;
                            $mentioned_users_info[$j]['FullName'] = $userInfoForComment->FullName;
                            $mentioned_users_info[$j]['MentionedName'] = $user_names[$j];
                            $mentioned_users_info[$j]['MentionedUserType'] = $user_types[$j];
                        }
                        $j++;
                    }
                }

                if (isset($mentioned_users_info[0]) && !empty($mentioned_users_info[0])) {
                    $comment_detail->MentionedUsersInfo = $mentioned_users_info;
                } else {
                    $comment_detail->MentionedUsersInfo = array();
                }


                $this->response([
                    'status' => 200,
                    'message' => lang('comment_saved_successfully'),
                    'data' => $comment_detail
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function deletequestionComment_post()
    {
        $post_data = $this->post_data; // UserID, CommentID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            if (isset($post_data['QuestionCommentID']) && $post_data['QuestionCommentID'] > 0) {
                $this->Question_comment_model->delete(array('QuestionCommentID' => $post_data['QuestionCommentID'], 'UserID' => $post_data['UserID']));
                
                $deleted_report['CommentID'] = $post_data['QuestionCommentID'];
                $deleted_report['ReportType'] = 'question';
                $deleted_report['UserID'] = $post_data['UserID'];
                $this->Comment_reported_model->delete($deleted_report);


                $deleted_report['CommentID'] = $post_data['QuestionCommentID'];
                $deleted_report['ReportType'] = 'question';
                $this->Comment_reported_model->delete($deleted_report);

                $deleted_notification['Type'] = 'comment';
                $deleted_notification['UserID'] = $post_data['UserID'];
                $deleted_notification['QuestionID'] = $post_data['QuestionID'];
                $deleted_notification['QuestionCommentID'] = $post_data['QuestionCommentID'];
                $this->User_notification_model->delete($deleted_notification);

                $this->response([
                    'status' => 200,
                    'message' => lang('answear_deleted_successfully')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getQuestionComments_post()
    {
        $post_data = $this->post_data; // QuestionID, UserID (Optional)
        $this->_isUserAuthorized(true);
        if (!empty($post_data)) {
            $start = 0;
            $limit = 20;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }

            $comments = $this->Question_comment_model->getQuestionComments('question_comments.QuestionID = ' . $post_data['QuestionID'], $this->language, 'question_comments.QuestionCommentID', 'DESC', $limit, $start);

            if (isset($post_data['UserID'])) {
                if (count($comments) > 0) {
                    $my_arr = array();
                    $i = 0;
                    foreach ($comments as $key => $value) {
                        $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID,$post_data['BlockedUserType']);
                        if (!$IsBlockedAsBooth) {
                            $my_arr[$i] = $value;
                            $i++;
                        }
                    }
                    $comments = $my_arr;
                }
            }

            if (!empty($comments)) {
                $i = 0;
                foreach ($comments as $comment) {
                    $user_ids = explode(',', $comment->MentionedUserID);
                    $user_names = explode(',', $comment->MentionedUserName);
                    $user_types = explode(',', $comment->MentionedUserType);
                    $mentioned_users_info = array();
                    $j = 0;
                    foreach ($user_ids as $id) {
                        $userInfoForComment = $this->User_model->getUsers("users.UserID = " . $id, $this->language);
                        if (!empty($userInfoForComment)) {
                            $userInfoForComment = $userInfoForComment[0];
                            $mentioned_users_info[$j]['UserID'] = $userInfoForComment->UserID;
                            $mentioned_users_info[$j]['FullName'] = $userInfoForComment->FullName;
                            $mentioned_users_info[$j]['MentionedName'] = $user_names[$j];
                            $mentioned_users_info[$j]['MentionedUserType'] = $user_types[$j];
                        }
                        $j++;
                    }

                    if (isset($mentioned_users_info[0]) && !empty($mentioned_users_info[0])) {
                        $comments[$i]->MentionedUsersInfo = $mentioned_users_info;
                    } else {
                        $comments[$i]->MentionedUsersInfo = array();
                    }

                    $i++;
                }
            }

            $this->response([
                'status' => 200,
                'comments' => !empty($comments) ? $comments : array()
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }


    public function reportQuestion_post()
    {
        $post_data = $this->post_data; // UserID (who is reporting), QuestionID, ReportReason
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $user_reported = $this->Question_reported_model->getWithMultipleFields(array('UserID' => $post_data['UserID'], 'QuestionID' => $post_data['QuestionID']));
            if ($user_reported) {
                $this->response([
                    'status' => 402,
                    'message' => lang('question_already_reported')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            $post_data['ReportedAt'] = time();
            $this->Question_reported_model->save($post_data);
            $this->response([
                'status' => 200,
                'message' => lang('question_reported_successfully')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function reportComment_post()
    {
        $post_data = $this->post_data; // UserID (who is reporting), CommentID, ReportReason, ReportType
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            
            $user_reported = $this->Comment_reported_model->getWithMultipleFields(array('UserID' => $post_data['UserID'], 'CommentID' => $post_data['CommentID'], 'ReportType' => $post_data['ReportType']));
            if ($user_reported) {
                $this->response([
                    'status' => 402,
                    'message' => lang('answear_comment_already_reported')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $commentquestion =  $this->Question_comment_model->getQuestionComments('question_comments.QuestionCommentID = ' . $post_data['CommentID']);
            $commentproduct = $this->Product_comment_model->getProductComments("product_comments.ProductCommentID = " . $post_data['CommentID']);

            if($commentquestion) {
                $post_data['ReportedAt'] = time();
                $this->Comment_reported_model->save($post_data);
                $this->response([
                    'status' => 200,
                    'message' => lang('answear_comment_reported_successfully')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } elseif ($commentproduct) {
                $post_data['ReportedAt'] = time();
                $this->Comment_reported_model->save($post_data);
                $this->response([
                    'status' => 200,
                    'message' => lang('answear_comment_reported_successfully')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 404,
                    'message' => lang('answear_comment_notfound')
                ], REST_Controller::HTTP_OK);                
            }
            
            
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function suggestedBooths_post()
    {
        $post_data = $this->post_data; // UserID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $user = $this->User_model->get($post_data['UserID'], false, 'UserID');
            $type = 'booth';
            if(isset($post_data['Type'])){
                $type = $post_data['Type'];
            }
            $suggested_booths = $this->User_category_model->getSuggestedBooths($post_data['UserID'],  $this->language,$type);
            $this->response([
                'status' => 200,
                'suggested_booths' => (count($suggested_booths) > 0 ? $suggested_booths : array())
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getCategoriesForUser_post()
    {
        $post_data = $this->post_data; // UserID, Type
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $response_data_arr = array();
            $user_categories = $this->User_category_model->getMultipleRows(array('UserID' => $post_data['UserID'], 'Type' => $post_data['Type']));
            if ($user_categories) {
                foreach ($user_categories as $user_category) {
                    $user_categories_arr[] = $user_category->CategoryID;
                }

                $parent_categories = $this->Category_model->getParentCategories($user_categories_arr);
                $parent_categories_arr = false;
                if ($parent_categories) {
                    $i = 0;
                    foreach ($parent_categories as $parent_category) {
                        $where_p = "categories.CategoryID = " . $parent_category['ParentID'] . " AND system_languages.ShortCode = '" . $this->language . "'";
                        $parent_categories_arr[$i] = $this->Category_model->getJoinedData(true, 'CategoryID', $where_p)[0];
                        $i++;
                    }
                }
                if ($parent_categories_arr) {
                    $i = 0;
                    foreach ($parent_categories_arr as $parent_category) {
                        $response_data_arr[$i] = $parent_category;
                        // getting all sub categories of this category
                        $where1 = 'categories.ParentID = ' . $parent_category['CategoryID'];
                        $parent_sub_categories = $this->Category_model->getAllJoinedData(true, 'CategoryID', $this->language, $where1, 'ASC', 'SortOrder');
                        if ($parent_sub_categories) {
                            $j = 0;
                            foreach ($parent_sub_categories as $parent_sub_category) {
                                if (in_array($parent_sub_category['CategoryID'], $user_categories_arr)) {
                                    $response_data_arr[$i]['SubCategories'][$j] = $parent_sub_category;
                                    $where2 = 'categories.ParentID = ' . $parent_sub_category['CategoryID'];
                                    $parent_sub_sub_categories = $this->Category_model->getAllJoinedData(true, 'CategoryID', $this->language, $where2, 'ASC', 'SortOrder');
                                    if ($parent_sub_sub_categories) {
                                        $k = 0;
                                        foreach ($parent_sub_sub_categories as $parent_sub_sub_category) {
                                            $response_data_arr[$i]['SubCategories'][$j]['SubSubCategories'][$k] = $parent_sub_sub_category;
                                            $k++;
                                        }
                                    } else {
                                        $response_data_arr[$i]['SubCategories'][$j]['SubSubCategories'] = array();
                                    }
                                    $j++;
                                }
                            }
                        } else {
                            $response_data_arr[$i]['SubCategories'] = array();
                        }
                        $i++;
                    }
                }
            }
            $this->response([
                'status' => 200,
                'user_categories' => $response_data_arr
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getSubCategoriesForUser_post()
    {
        $post_data = $this->post_data; // UserID, Type
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $user_sub_categories = array();
            $user_categories = $this->User_category_model->getMultipleRows(array('UserID' => $post_data['UserID'], 'Type' => $post_data['Type']));
            if ($user_categories) {
                foreach ($user_categories as $user_category) {
                    $user_categories_arr[] = $user_category->CategoryID;
                }
                $user_sub_categories = $this->Category_model->getCategoriesWhereIn($user_categories_arr, $this->language);
            }
            $this->response([
                'status' => 200,
                'user_sub_categories' => $user_sub_categories
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getNotifications_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // UserID, Type (user, booth)
        if (!empty($post_data)) {

            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }

            if (isset($post_data['Type'])) {
                $type = $post_data['Type'];
            } else {
                $type = 'user';
            }

            $where = "user_notifications.LoggedInUserID = " . $post_data['UserID'] . " AND user_notifications.UserType = '$type'";
            $notifications = $this->User_notification_model->getNotifications($where, $start, $limit, $this->language);
            $notifications_arr = array();
            if ($notifications) {
                $i = 0;
                foreach ($notifications as $notification) {
                    $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $notification['UserID'], $type);
                    if ($IsBlockedAsBooth) {
                        continue;
                    }
                    $notifications_arr[$i] = $notification;
                    $product_image = '';
                    $question_image = '';
                    if ($notification['ProductID'] > 0) {
                        $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $notification['ProductID']));
                        if ($product_images) {
                            $product_image = $product_images[0]->ProductCompressedImage;
                        }
                    }
                    if ($notification['QuestionID'] > 0) {
                        $question_images = $this->Question_image_model->getMultipleRows(array('QuestionID' => $notification['QuestionID']));
                        if ($question_images) {
                            $question_image = $question_images[0]->CompressedImage;
                        }
                    }
                    $notifications_arr[$i]['ProductImage'] = $product_image;
                    $notifications_arr[$i]['QuestionImage'] = $question_image;


                    $user_ids = explode(',', $notification['MentionedUserID']);
                    $user_names = explode(',', $notification['MentionedUserName']);
                    $user_types = explode(',', $notification['MentionedUserType']);
                    $mentioned_users_info = array();
                    $j = 0;
                    foreach ($user_ids as $id) {
                        $userInfoForComment = $this->User_model->getUsers("users.UserID = " . $id, $this->language);
                        if (!empty($userInfoForComment)) {
                            $userInfoForComment = $userInfoForComment[0];
                            $mentioned_users_info[$j]['UserID'] = $userInfoForComment->UserID;
                            $mentioned_users_info[$j]['FullName'] = $userInfoForComment->FullName;
                            $mentioned_users_info[$j]['MentionedName'] = $user_names[$j];
                            $mentioned_users_info[$j]['MentionedUserType'] = isset($user_types[$j]) ? $user_types[$j] : 'user';
                        }
                        $j++;
                    }

                    if (isset($mentioned_users_info[0]) && !empty($mentioned_users_info[0])) {
                        $notifications_arr[$i]['MentionedUsersInfo'] = $mentioned_users_info;
                    } else {
                        $notifications_arr[$i]['MentionedUsersInfo'] = array();
                    }


                    $i++;
                }
            }
            $this->response([
                'status' => 200,
                'notifications' => NullToEmpty($notifications_arr)
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getFriendsActivities_get() // For home tab 3
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // UserID, Type (user, booth)
        if (!empty($post_data)) {

            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }

             $where = "user_friends_activities.LoggedInUserID = " . $post_data['UserID'];

            if (isset($post_data['Type'])) {
                $type = $post_data['Type'];
               //$where .= " AND user_friends_activities.UserType = '$type'";
            } 

           // $where = "user_friends_activities.LoggedInUserID = " . $post_data['UserID'] . " AND user_friends_activities.UserType = '$type'";
            //$where = "user_friends_activities.LoggedInUserID = " . $post_data['UserID'];
            $activities = $this->User_friends_notification_model->getActivities($where, $start, $limit, $this->language);

            $activities_arr = array();
            if ($activities) {
                $i = 0;
                foreach ($activities as $activity) {

                    if($activity['Type'] == 'like'){
                            if($activity['IsLikesPrivate'] == 1){
                                 continue;
                            }

                    }elseif($activity['Type'] == 'added_to_wishlist'){
                        if($activity['IsWishListPrivate'] == 1){
                                 continue;
                            }
                        
                    }elseif($activity['Type'] == 'order'){
                        if($activity['IsOrdersPrivate'] == 1){
                                 continue;
                            }
                        
                    }

                    // Block work here
                    $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $activity['UserID']);
                    if ($IsBlockedAsBooth) {
                        continue;
                    }

                    $get_following_data = array();
                    $get_following_data['Follower'] = $post_data['UserID'];
                    $get_following_data['Following'] = $activity['UserID'];
                    if (isset($post_data['Type'])){
                        //$get_following_data['Type'] = $post_data['Type'];
                    }

                    $check_if_following = $this->Follower_model->getMultipleRows($get_following_data);
                    if(!$check_if_following){
                        continue;
                    }

                    if($check_if_following && count($check_if_following) != 2){
                        $check_type = $check_if_following[0]->Type;
                        if($activity['MentionedUserType'] == $check_type){



                            $activities_arr[$i] = $activity;

                            // Products Work
                            if ($activity['ProductID'] !== '') {
                                $ProductIDs = explode(',', $activity['ProductID']);
                                if (!empty($ProductIDs)) {
                                    $j = 0;
                                    foreach ($ProductIDs as $pID) {
                                        $product = $this->Product_model->getProducts("products.ProductID = " . $pID, $this->language);
                                        if ($product) {
                                            $product = (array)$product[0];
                                            $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $pID));
                                            $product['ProductImages'] = $product_images ? $product_images : array();
                                            $activities_arr[$i]['Product'][$j] = $product;
                                        } else {
                                            $activities_arr[$i]['Product'][$j] = array();
                                        }
                                        $j++;
                                    }
                                }
                            } else {
                                $activities_arr[$i]['Product'] = array();
                            }

                            // Product Comment Work
                            if ($activity['ProductCommentID'] > 0) {
                                $product_comment = $this->Product_comment_model->getProductComments("product_comments.ProductCommentID = " . $activity['ProductCommentID'], $this->language);
                                if ($product_comment) {
                                    $activities_arr[$i]['ProductComment'] = (array)$product_comment[0];
                                } else {
                                    $activities_arr[$i]['ProductComment'] = array();
                                }
                            } else {
                                $activities_arr[$i]['ProductComment'] = array();
                            }

                            // Question Work
                            if ($activity['QuestionID'] > 0) {
                                $question = $this->Question_model->getQuestions("questions.QuestionID = " . $activity['QuestionID'], $this->language);
                                if ($question) {
                                    $question = (array)$question[0];
                                    $question_images = $this->Question_image_model->getMultipleRows(array('QuestionID' => $activity['QuestionID']));
                                    $question['QuestionImages'] = $question_images ? $question_images : array();
                                    $activities_arr[$i]['Question'] = $question;
                                } else {
                                    $activities_arr[$i]['Question'] = array();
                                }
                            } else {
                                $activities_arr[$i]['Question'] = array();
                            }

                            // Question Comment Work
                            if ($activity['QuestionCommentID'] > 0) {
                                $question_comment = $this->Question_comment_model->getQuestionComments("question_comments.QuestionCommentID = " . $activity['QuestionCommentID'], $this->language);
                                if ($question_comment) {
                                    $activities_arr[$i]['QuestionComment'] = (array)$question_comment[0];;
                                } else {
                                    $activities_arr[$i]['QuestionComment'] = array();
                                }
                            } else {
                                $activities_arr[$i]['QuestionComment'] = array();
                            }


                            $user_ids = explode(',', $activity['MentionedUserID']);
                            $user_names = explode(',', $activity['MentionedUserName']);
                            $user_types = explode(',', $activity['MentionedUserType']);
                            $mentioned_users_info = array();
                            $j = 0;
                            foreach ($user_ids as $id) {
                                $userInfoForComment = $this->User_model->getUsers("users.UserID = " . $id, $this->language);
                                if (!empty($userInfoForComment)) {
                                    $userInfoForComment = $userInfoForComment[0];
                                    $mentioned_users_info[$j]['UserID'] = $userInfoForComment->UserID;
                                    $mentioned_users_info[$j]['FullName'] = $userInfoForComment->FullName;
                                    $mentioned_users_info[$j]['MentionedName'] = $user_names[$j];
                                    $mentioned_users_info[$j]['MentionedUserType'] = $user_types[$j];
                                }
                                $j++;
                            }

                            if (isset($mentioned_users_info[0]) && !empty($mentioned_users_info[0])) {
                                $activities_arr[$i]['MentionedUsersInfo'] = $mentioned_users_info;
                            } else {
                                $activities_arr[$i]['MentionedUsersInfo'] = array();
                            }


                            $i++; 
                        }


                    }else
                    {

                        $activities_arr[$i] = $activity;

                    // Products Work
                        if ($activity['ProductID'] !== '') {
                            $ProductIDs = explode(',', $activity['ProductID']);
                            if (!empty($ProductIDs)) {
                                $j = 0;
                                foreach ($ProductIDs as $pID) {
                                    $product = $this->Product_model->getProducts("products.ProductID = " . $pID, $this->language);
                                    if ($product) {
                                        $product = (array)$product[0];
                                        $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $pID));
                                        $product['ProductImages'] = $product_images ? $product_images : array();
                                        $activities_arr[$i]['Product'][$j] = $product;
                                    } else {
                                        $activities_arr[$i]['Product'][$j] = array();
                                    }
                                    $j++;
                                }
                            }
                        } else {
                            $activities_arr[$i]['Product'] = array();
                        }

                        // Product Comment Work
                        if ($activity['ProductCommentID'] > 0) {
                            $product_comment = $this->Product_comment_model->getProductComments("product_comments.ProductCommentID = " . $activity['ProductCommentID'], $this->language);
                            if ($product_comment) {
                                $activities_arr[$i]['ProductComment'] = (array)$product_comment[0];
                            } else {
                                $activities_arr[$i]['ProductComment'] = array();
                            }
                        } else {
                            $activities_arr[$i]['ProductComment'] = array();
                        }

                        // Question Work
                        if ($activity['QuestionID'] > 0) {
                            $question = $this->Question_model->getQuestions("questions.QuestionID = " . $activity['QuestionID'], $this->language);
                            if ($question) {
                                $question = (array)$question[0];
                                $question_images = $this->Question_image_model->getMultipleRows(array('QuestionID' => $activity['QuestionID']));
                                $question['QuestionImages'] = $question_images ? $question_images : array();
                                $activities_arr[$i]['Question'] = $question;
                            } else {
                                $activities_arr[$i]['Question'] = array();
                            }
                        } else {
                            $activities_arr[$i]['Question'] = array();
                        }

                        // Question Comment Work
                        if ($activity['QuestionCommentID'] > 0) {
                            $question_comment = $this->Question_comment_model->getQuestionComments("question_comments.QuestionCommentID = " . $activity['QuestionCommentID'], $this->language);
                            if ($question_comment) {
                                $activities_arr[$i]['QuestionComment'] = (array)$question_comment[0];;
                            } else {
                                $activities_arr[$i]['QuestionComment'] = array();
                            }
                        } else {
                            $activities_arr[$i]['QuestionComment'] = array();
                        }


                        $user_ids = explode(',', $activity['MentionedUserID']);
                        $user_names = explode(',', $activity['MentionedUserName']);
                        $user_types = explode(',', $activity['MentionedUserType']);
                        $mentioned_users_info = array();
                        $j = 0;
                        foreach ($user_ids as $id) {
                            $userInfoForComment = $this->User_model->getUsers("users.UserID = " . $id, $this->language);
                            if (!empty($userInfoForComment)) {
                                $userInfoForComment = $userInfoForComment[0];
                                $mentioned_users_info[$j]['UserID'] = $userInfoForComment->UserID;
                                $mentioned_users_info[$j]['FullName'] = $userInfoForComment->FullName;
                                $mentioned_users_info[$j]['MentionedName'] = $user_names[$j];
                                $mentioned_users_info[$j]['MentionedUserType'] = $user_types[$j];
                            }
                            $j++;
                        }

                        if (isset($mentioned_users_info[0]) && !empty($mentioned_users_info[0])) {
                            $activities_arr[$i]['MentionedUsersInfo'] = $mentioned_users_info;
                        } else {
                            $activities_arr[$i]['MentionedUsersInfo'] = array();
                        }

                    $i++;

                    }

                    
                    

                    
                }
            }
            //print_rm($activities_arr);
            $this->response([
                'status' => 200,
                'activities' => NullToEmpty($activities_arr)
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function markNotificationAsRead_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // UserID, NotificationID
        $update['IsRead'] = 1;
        $update_by['UserNotificationID'] = $post_data['NotificationID'];
        $this->User_notification_model->update($update, $update_by);
        $where = "user_notifications.LoggedInUserID = " . $post_data['UserID'];
        $notifications = $this->User_notification_model->getNotifications($where, 0, false, $this->language);
        $this->response([
            'status' => 200,
            'message' => lang('notification_marked_as_read'),
            'notifications' => NullToEmpty($notifications)
        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }

    public function follow_post()
    {
        $post_data = $this->post_data; //UserID (ID of the person who is following), Following (comma separated ids of users being followed), Follow (1 for follow, 0 for unfollow), Type (user, booth)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {

            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $post_data['Following'],$post_data['Type'],'user');
            if ($IsBlocked) {
                 $this->response([
                        'status' => 402,
                        'message' => lang('you_cannot_do')
                    ], REST_Controller::HTTP_OK);

                 exit;
            }
            if (isset($post_data['Follow']) && $post_data['Follow'] == 0) { // for unfollowing case
                $unfollow_data['Follower'] = $post_data['UserID'];
                $unfollow_data['Following'] = $post_data['Following'];
                $unfollow_data['Type'] = $post_data['Type'];
                $this->Follower_model->delete($unfollow_data);
                $this->response([
                    'status' => 200,
                    'message' => lang('unfollowed_successfully')
                ], REST_Controller::HTTP_OK);
            } else { // for following case
                $Followings = explode(',', $post_data['Following']);
                foreach ($Followings as $following) {
                    $follow_data['Follower'] = $post_data['UserID'];
                    $follow_data['Following'] = $following;
                    $follow_data['Type'] = $post_data['Type'];
                    $check_already = $this->Follower_model->getWithMultipleFields($follow_data);
                    if(!$check_already){
                        $this->Follower_model->save($follow_data);
                        $this->sendFollowNotification($follow_data);
                    }
                    
                }
                $this->response([
                    'status' => 200,
                    'message' => lang('followed_successfully')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getFollowing_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // UserID, OtherUserID (Optional), Type (user, booth), Search
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;
            $search = false;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }
            if (isset($post_data['Search']) && $post_data['Search'] !== '') {
                $search = $post_data['Search'];
            }

            if (isset($post_data['OtherUserID'])) {
                $UserIDForData = $post_data['OtherUserID'];
            } else {
                $UserIDForData = $post_data['UserID'];
            }

            $response['total_following'] = $this->Follower_model->getTotalFollowing($UserIDForData, $post_data['Type']);
            $response['following'] = $this->Follower_model->getFollowing($UserIDForData, $post_data['Type'], $start, $limit, $search, $this->language);

            if ($response['following']) {
                $i = 0;
                foreach ($response['following'] as $user) {
                    $following = 0;
                    $being_followed = 0;
                    $checkIfFollow = $this->Follower_model->getWithMultipleFields(array('Follower' => $UserIDForData, 'Following' => $user['UserID'], 'Type' => $post_data['Type']));
                    $checkIfFollowing = $this->Follower_model->getWithMultipleFields(array('Follower' => $user['UserID'], 'Following' => $UserIDForData, 'Type' => $post_data['Type']));
                    if ($checkIfFollow) {
                        $following = 1;
                    }
                    if ($checkIfFollowing) {
                        $being_followed = 1;
                    }
                    $response['following'][$i]['Follower'] = $following;
                    $response['following'][$i]['Following'] = $being_followed;
                    $i++;
                }
            } else {
                $response['following'] = array();
            }

            if (count($response['following']) > 0) {
                $my_arr = array();
                $i = 0;
                foreach ($response['following'] as $key => $value) {
                    $IsBlockedAsBooth = $this->checkIfUserBlocked($UserIDForData, $value['UserID']);
                    if (!$IsBlockedAsBooth) {
                        $my_arr[$i] = $value;
                        $i++;
                    }
                }
                $response['following'] = $my_arr;
            }

            $this->response([
                'status' => 200,
                'data' => $response
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    public function getFollowers_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // UserID, Type (user, booth), Search
        if (!empty($post_data)) {

            $start = 0;
            $limit = 10;
            $search = false;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }
            if (isset($post_data['Search']) && $post_data['Search'] !== '') {
                $search = $post_data['Search'];
            }

            if (isset($post_data['OtherUserID'])) {
                $UserIDForData = $post_data['OtherUserID'];
            } else {
                $UserIDForData = $post_data['UserID'];
            }

            $response['total_follower'] = $this->Follower_model->getTotalFollower($UserIDForData, $post_data['Type']);
            $response['followers'] = $this->Follower_model->getFollower($UserIDForData, $post_data['Type'], $start, $limit, $search, $this->language);

            if ($response['followers']) {
                $i = 0;
                foreach ($response['followers'] as $user) {
                    $following = 0;
                    $being_followed = 0;
                    $checkIfFollow = $this->Follower_model->getWithMultipleFields(array('Follower' => $UserIDForData, 'Following' => $user['UserID'], 'Type' => $post_data['Type']));
                    $checkIfFollowing = $this->Follower_model->getWithMultipleFields(array('Follower' => $user['UserID'], 'Following' => $UserIDForData, 'Type' => $post_data['Type']));
                    if ($checkIfFollow) {
                        $following = 1;
                    }
                    if ($checkIfFollowing) {
                        $being_followed = 1;
                    }
                    $response['followers'][$i]['Follower'] = $following;
                    $response['followers'][$i]['Following'] = $being_followed;
                    $i++;
                }
            } else {
                $response['followers'] = array();
            }

            if (count($response['followers']) > 0) {
                $my_arr = array();
                $i = 0;
                foreach ($response['followers'] as $key => $value) {
                    $IsBlockedAsBooth = $this->checkIfUserBlocked($UserIDForData, $value['UserID']);
                    if (!$IsBlockedAsBooth) {
                        $my_arr[$i] = $value;
                        $i++;
                    }
                }
                $response['followers'] = $my_arr;
            }

            $this->response([
                'status' => 200,
                'data' => $response
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    public function reportUser_post()
    {
        $post_data = $this->post_data; // UserID (who is reporting), ReportedUserID, ReportReason, ReportedUserType (user, booth)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $user_reported = $this->Model_general->getSingleRow('users_reported', array('UserID' => $post_data['UserID'], 'ReportedUserID' => $post_data['ReportedUserID'], 'ReportedUserType' => $post_data['ReportedUserType']));
            if ($user_reported) {
                $this->response([
                    'status' => 402,
                    'message' => lang('user_already_reported')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            $post_data['ReportedAt'] = time();
            $this->Model_general->save('users_reported', $post_data);
            $this->response([
                'status' => 200,
                'message' => lang('user_reported_successfully')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function blockUser_post()
    {
        $post_data = $this->post_data; // UserID (who is blocking), BlockedUserID, Type(block, unblock), BlockedUserType(user, booth)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $type = $post_data['Type'];
            unset($post_data['Type']);
            if (strtolower($type) == 'block') {

                $user_blocked = $this->Model_general->getSingleRow('users_blocked', $post_data);
                if ($user_blocked) // checking if user is already blocked
                {
                    $this->response([
                        'status' => 402,
                        'message' => lang('user_already_blocked')
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
                $post_data['BlockedAt'] = time();

                 if($post_data['BlockedUserType'] == 'booth' && $post_data['UserType'] == 'user'){
                                $where = '';
                                $where_order_status = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND orders_requests.BoothID = ".$post_data['BlockedUserID']." AND ";
                                $where = $where_order_status . "orders.UserID = " . $post_data['UserID'];
                                $order_requests = $this->Order_request_model->getOrdersRequests($where, $this->language);
                                if($order_requests){
                                    $this->response([
                                        'status' => 402,
                                        'message' => 'You can\'t block this booth'
                                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                                    exit;
                                }
                    }

                    if($post_data['BlockedUserType'] == 'user' && $post_data['UserType'] == 'booth'){
                                $where = '';
                                $where_order_status = "(orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2 OR orders_requests.OrderStatusID = 3 OR orders_requests.OrderStatusID = 7) AND orders_requests.BoothID = ".$post_data['UserID']." AND ";
                                $where = $where_order_status . "orders.UserID = " . $post_data['BlockedUserID'];
                                $order_requests = $this->Order_request_model->getOrdersRequests($where, $this->language);
                                if($order_requests){
                                    $this->response([
                                        'status' => 402,
                                        'message' => 'You can\'t block this user'
                                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                                    exit;
                                }
                    }
                $insert_id = $this->Model_general->save('users_blocked', $post_data);
                if($insert_id > 0){
                    $deleted_by = array();
                    $deleted_by['Follower'] = $post_data['UserID'];
                    $deleted_by['Following'] = $post_data['BlockedUserID'];
                    $deleted_by['Type'] = $post_data['BlockedUserType'];
                    $this->Model_general->deleteWhere('user_followers', $deleted_by);
                    $deleted_by['Follower'] = $post_data['BlockedUserID'];
                    $deleted_by['Following'] = $post_data['UserID'];
                    $this->Model_general->deleteWhere('user_followers', $deleted_by);
                    if($post_data['BlockedUserType'] == 'booth' && $post_data['UserType'] == 'user'){

                        $deleted_by = array();
                        $deleted_by['UserID'] = $post_data['UserID'];
                        $deleted_by['BoothID'] = $post_data['BlockedUserID'];
                        $this->Model_general->deleteWhere('temp_orders', $deleted_by);
                    }
                    

                }
                $this->response([
                    'status' => 200,
                    'message' => lang('user_blocked_successfully')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } elseif (strtolower($type) == 'unblock') {
                $this->Model_general->deleteWhere('users_blocked', $post_data);
                $this->response([
                    'status' => 200,
                    'message' => lang('user_unblocked_successfully')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function blockedUsers_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->post_data; // UserID
        if (isset($post_data['UserID']) && $post_data['UserID'] > 0) {
            $UserType = false;
            if(isset($post_data['UserType'])){
                $UserType = $post_data['UserType'];
            }
            $blocked_users = $this->User_model->getBlockedUsers($post_data['UserID'],$UserType);
            $this->response([
                'status' => 200,
                'blocked_users' => $blocked_users
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => 402,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    private function checkIfUserExistAndActive()
    {
        $post_data = $this->post_data;
        if (isset($post_data['UserID']) && $post_data['UserID'] > 0) {
            $fetch_user['UserID'] = $post_data['UserID'];
            $user_exist = $this->User_model->getWithMultipleFields($fetch_user, true);
            if (!$user_exist) {
                $this->response([
                    'status' => 401,
                    'message' => "User not found."
                ], REST_Controller::HTTP_OK);
            } elseif ($user_exist && $user_exist['IsActive'] == 0) {
                $this->response([
                    'status' => 401,
                    'message' => "Sorry! You can't use this app right now. Please contact admin for further details."
                ], REST_Controller::HTTP_OK);
            }
        }
    }

    private function checkIfUserActive()
    {
        $post_data = $this->post_data;
        if (isset($post_data['UserID']) && $post_data['UserID'] > 0) {
            $fetch_user['UserID'] = $post_data['UserID'];
            $user = $this->User_model->getWithMultipleFields($fetch_user, true);
            if ($user['IsActive'] == 0) {
                $this->response([
                    'status' => 401,
                    'message' => "Sorry! You can't use this feature right now. Please contact admin for further details."
                ], REST_Controller::HTTP_OK);
            }
        }
    }

    private function sendWelcomeEmail($user_info, $password)
    {
       // print_rm($user_info);
        $email_template = get_email_template(1, $this->language);
        $verify_link = base_url() . 'page/verifyEmail?verification_token=' . base64_encode($user_info['UserID']) . '&lang=' . $this->language;
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['FullName'], $message);
        $message = str_replace("{{email}}", $user_info['Email'], $message);
        $message = str_replace("{{link}}", $verify_link, $message);
        //$message = str_replace("{{password}}", $password, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        $data['message'];
        sendEmail($data,false,$user_info['CompanyID']);
    }

    /*private function sendVerificationForEmail($user_info)
    {
        if ($user_info['Email'] !== '') {
            $verify_link = base_url() . 'index/verifyEmail?verification_token=' . base64_encode($user_info['UserID']) . '&lang=' . $this->language;
            $email_template = get_email_template(7, $this->language);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{user_name}}", $user_info['UserName'], $message);
            $message = str_replace("{{link}}", $verify_link, $message);
            $data['to'] = $user_info['Email'];
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }*/

    private function sendForgotPasswordEmail($user_info)
    {
        $this->User_model->update(array('OnlineStatus' => 'Offline'),array('UserID' => $user_info['UserID']));
        $link = base_url() . 'index/forgotPassword?verification_token=' . base64_encode($user_info['UserID']) . '&lang=' . $this->language;
        $email_template = get_email_template(4, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{link}}", $link, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data,false,$user_info['CompanyID']);
    }

    private function sendForgotPasswordSMS($user_info)
    {
        $link = base_url() . 'index/forgotPassword?verification_token=' . base64_encode($user_info['UserID']) . '&lang=' . $this->language;
        $email_template = get_email_template(4, $this->language);
        $message = $email_template->Description;
        $message = str_replace("{{link}}", $link, $message);
        sendSms($user_info['Mobile'], $message);
    }

    private function checkIfUserBlocked($UserID1, $UserID2, $BlockedUserType = false, $UserType = false)
    {
        $CI = &get_Instance();
        $CI->load->model('Model_general');
        $check_block_by_1['UserID'] = $UserID1;
        $check_block_by_1['BlockedUserID'] = $UserID2;
        if ($BlockedUserType) {
            $check_block_by_1['BlockedUserType'] = $BlockedUserType;
        }
        if ($UserType) {
            $check_block_by_1['UserType'] = $UserType;
        }
        $check_block_by_2['UserID'] = $UserID2;
        $check_block_by_2['BlockedUserID'] = $UserID1;
        if ($BlockedUserType) {
            $check_block_by_2['BlockedUserType'] = $BlockedUserType;
        }
        if ($UserType) {
            $check_block_by_2['UserType'] = $UserType;
        }
        $user1_blocked_user2 = $CI->Model_general->getSingleRow('users_blocked', $check_block_by_1);
        $user2_blocked_user1 = $CI->Model_general->getSingleRow('users_blocked', $check_block_by_2);
        if ($user1_blocked_user2 || $user2_blocked_user1) {
            return true;
        } else {
            return false;
        }
    }

    private function checkAppVersion()
    {
        if (isset($this->post_data['NoVersion'])) {
            unset($this->post_data['NoVersion']);
            return true;
        }
        if (!empty($this->post_data)) {
            $site_settings = site_settings();
            if (isset($this->post_data['IOSAppVersion'])) {
                if ($site_settings->IOSAppVersion > $this->post_data['IOSAppVersion']) {
                    $this->response([
                        'status' => 408,
                        'message' => "An update is available to your app."
                    ], REST_Controller::HTTP_OK);
                } else {
                    unset($this->post_data['IOSAppVersion']);
                }
            } elseif (isset($this->post_data['AndroidAppVersion'])) { // This will be version code
                if ($site_settings->AndroidAppVersion > $this->post_data['AndroidAppVersion']) {
                    $this->response([
                        'status' => 408,
                        'message' => "An update is available to your app."
                    ], REST_Controller::HTTP_OK);
                } else {
                    unset($this->post_data['AndroidAppVersion']);
                }
            } else {
                $this->response([
                    'status' => 402,
                    'message' => "App version not sent"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => 402,
                'message' => "App version not sent"
            ], REST_Controller::HTTP_OK);
        }
    }

    private function sendFollowNotification($post_data)
    {
        // saving notification
        $data['LoggedInUserID'] = $post_data['Following']; // the person who will receive this notification
        $data['UserType'] = $post_data['Type'];
        $data['Type'] = 'follow';
        $data['UserID'] = $post_data['Follower']; // the person who is making this notification
        $data['NotificationTypeID'] = 4;
        $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
       // print_rm($LoggedInUserInfo);
        $logged_in_user_id = $LoggedInUserInfo->UserID;
        $logged_in_user_name = ($post_data['Type'] == 'booth' ? $LoggedInUserInfo->BoothUserName : $LoggedInUserInfo->UserName);
        $UserInfo = getUserInfo($data['UserID'], $this->language);

       

        // check if notification is already sent to user for this
        $already_notification_sent = $this->User_notification_model->getWithMultipleFields($data);
        if (!$already_notification_sent) {
           
            $data['NotificationTextEn'] = '@' . $UserInfo->UserName . " started following you";
            $data['NotificationTextAr'] = '@' . $UserInfo->UserName . " قام بمتابعتك";
            $data['CreatedAt'] = time();
            $mentioned_user_ids = array($UserInfo->UserID,$logged_in_user_id);
            $mentioned_user_names = array($UserInfo->UserName,$logged_in_user_name);
            $mentioned_user_types = array('user',$post_data['Type']);
            log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
            $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $post_data['Following']);
           
             
        }
        
        // saving activity
        $activity_data['UserID'] = $post_data['Follower'];
        $activity_data['UserType'] = $post_data['Type'];
        $activity_data['Type'] = 'follow';
        $activity_data['NotificationTypeID'] = 20;
        $UserInfo = getUserInfo($activity_data['UserID'], $this->language);
        $activity_data['NotificationTextEn'] = '@' . $UserInfo->UserName . " started following you";
        $activity_data['NotificationTextAr'] = '@' . $UserInfo->UserName . " قام بمتابعتك";
        $activity_data['CreatedAt'] = time();
        $mentioned_user_ids = array($UserInfo->UserID,$logged_in_user_id);
        $mentioned_user_names = array($UserInfo->UserName,$logged_in_user_name);
        $mentioned_user_types = array('user',$post_data['Type']);
        $activity_data['CommentedAs'] = $post_data['Type'];// this is only because need to get only type following user
        log_friend_activity($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types, true,$logged_in_user_name);
    }

    private function checkIfUserVerified($UserID, $Check = 'Both') // Email, Mobile, Both
    {
        // check if user is verified
        $user = $this->User_model->get($UserID, false, 'UserID');
        if (($Check == 'Email' || $Check == 'Both') && $user->IsEmailVerified == 0) {
            $this->response([
                'status' => 402,
                'message' => lang('please_verify_your_email')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

        if (($Check == 'Mobile' || $Check == 'Both') && $user->IsMobileVerified == 0) {
            $this->response([
                'status' => 402,
                'message' => lang('please_verify_your_mobile')
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    private function checkIfProductActive($ProductID)
    {
        $ProductActive = $this->Product_model->getWithMultipleFields(array('ProductID' => $ProductID, 'IsActive' => 1, 'Hide' => 0));
        if ($ProductActive) {
            return true;
        } else {
            return false;
        }
    }

    public function getChatRooms_post()
    {
        $post_data = $this->post_data; // UserID
        $this->_isUserAuthorized(true);

        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $type = false;
            if (isset($post_data['Type'])) {
                $type = $post_data['Type'];
            }
            // getting all chat rooms for this user
            $chatRooms = $this->Chat_model->getChatRooms($post_data['UserID'], $start, $limit,$type);

            // dump($chatRooms);

            $return_array = array();

            if ($chatRooms) {
                // getting last message for each chat
                $i = 0;
                foreach ($chatRooms as $chatRoom) {
                    $message = $this->Chat_message_model->getLastMessageForThisChatRoom($chatRoom['ChatID']);
                    

                    $return_array[$i]['ChatID'] =  $chatRoom['ChatID'];
                    $return_array[$i]['ConversationSenderID'] = $chatRoom['ConversationSenderID'];
                    
                    $return_array[$i]['ConversationSenderName'] = ($chatRoom['Type'] == 'user' ? $chatRoom['ConversationSenderName'] : $chatRoom['ConversationSenderBoothName']);
                 
                    $return_array[$i]['ConversationSenderUserName'] = ($chatRoom['Type'] == 'user' ? $chatRoom['ConversationSenderUserName'] : $chatRoom['ConversationSenderBoothUserName']);
                   
                    $return_array[$i]['ConversationSenderImage'] = ($chatRoom['Type'] == 'user' ? $chatRoom['ConversationSenderImage'] : $chatRoom['ConversationSenderBoothImage']);
                  


                    $return_array[$i]['ConversationReceiverID'] = $chatRoom['ConversationReceiverID'];


                    $return_array[$i]['ConversationReceiverName'] =  ($chatRoom['ReceiverType'] == 'user' ? $chatRoom['ConversationReceiverName'] : $chatRoom['ConversationReceiverBoothName']);
                    //$chatRooms[$i]['ConversationReceiverBoothName'] = $chatRoom['ConversationReceiverBoothName'];
                    $return_array[$i]['ConversationReceiverUserName'] = ($chatRoom['ReceiverType'] == 'user' ? $chatRoom['ConversationReceiverUserName'] : $chatRoom['ConversationReceiverBoothUserName']);
                   // $chatRooms[$i]['ConversationReceiverBoothUserName'] = $chatRoom['ConversationReceiverBoothUserName'];
                    $return_array[$i]['ConversationReceiverImage'] = ($chatRoom['ReceiverType'] == 'user' ? $chatRoom['ConversationReceiverImage'] : $chatRoom['ConversationReceiverBoothImage']);
                    //$chatRooms[$i]['ConversationReceiverBoothImage'] = $chatRoom['ConversationReceiverBoothImage'];



                    $return_array[$i]['Type'] =  $chatRoom['Type'];

                    $return_array[$i]['ReceiverType'] = $chatRoom['ReceiverType'];
                    $return_array[$i]['ChatMessageID'] = $message['ChatMessageID'];
                    //$return_array[$i]['SenderImage'] = $message['SenderImage'];
                    //$return_array[$i]['ReceiverImage'] = $message['ReceiverImage'];
                    $return_array[$i]['SenderID'] = $message['SenderID'];
                    $return_array[$i]['ReceiverID'] = $message['ReceiverID'];
                    $return_array[$i]['IsReadBySender'] = $message['IsReadBySender'];
                    $return_array[$i]['IsReadByReceiver'] = $message['IsReadByReceiver'];
                   // $return_array[$i]['SenderName'] = $message['SenderName'];
                    //$return_array[$i]['SenderUserName'] = $message['SenderUserName'];
                    //$return_array[$i]['SenderBoothUserName'] = $message['SenderBoothUserName'];
                    //$return_array[$i]['SenderBoothName'] = $message['SenderBoothName'];
                    //$chatRooms[$i]['SenderImage'] = $message['SenderImage'];
                    //$return_array[$i]['SenderBoothImage'] = $message['SenderBoothImage'];


                    //$return_array[$i]['ReceiverName'] = $message['ReceiverName'];
                    //$return_array[$i]['ReceiverUserName'] = $message['ReceiverUserName'];
                    //$return_array[$i]['ReceiverBoothUserName'] = $message['ReceiverBoothUserName'];
                   // $return_array[$i]['ReceiverBoothName'] = $message['ReceiverBoothName'];
                   // $chatRooms[$i]['ReceiverImage'] = $message['ReceiverImage'];
                    //$chatRooms[$i]['ReceiverBoothImage'] = $message['ReceiverBoothImage'];


                    $return_array[$i]['Image'] = $message['Image'];
                    $return_array[$i]['CompressedImage'] = $message['CompressedImage'];
                    $return_array[$i]['VideoThumbnail'] = $message['VideoThumbnail'];
                    $return_array[$i]['Video'] = $message['Video'];
                    $return_array[$i]['Message'] = $message['Message'];
                    $return_array[$i]['CreatedAt'] = $message['CreatedAt'];
                    $unread_msg_count = $this->Chat_message_model->getRowsCount(array('ChatID' => $chatRoom['ChatID'], 'ReceiverID' => $post_data['UserID'], 'IsReadByReceiver' => 'no'),false,false,false,true);
                    $return_array[$i]['HasUnreadMessage'] = ($unread_msg_count > 0 ? 'yes' : 'no');
                    $return_array[$i]['UnreadMessageCount'] = $unread_msg_count;
                    $i++;
                }

                // blocked work here
                $filtered_chat_rooms = array();
                $i = 0;
                foreach ($return_array as $chatRoom) {
                    $UserID = $chatRoom['ConversationSenderID'];
                    $BlockedUserID = $chatRoom['ConversationReceiverID'];
                    $Type = $chatRoom['Type'];
                    $BlockedReceiverType = $chatRoom['ReceiverType'];
                    $user_blocked = $this->checkIfUserBlocked($UserID, $BlockedUserID,$Type,$BlockedReceiverType);
                    if ($user_blocked) {
                        continue;
                    }
                    $filtered_chat_rooms[$i] = $chatRoom;
                    $i++;
                }

                // dump($chatRooms);
                $this->response([
                    'status' => 200,
                    'ChatRooms' => $filtered_chat_rooms
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 200,
                    'ChatRooms' => array()
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function startChat_post()
    {
        $post_data = $this->post_data; // UserID (Who is starting chat, This will be SenderID in DB), ReceiverID, Type (Type of user initiating chat as, booth OR user)
        $this->_isUserAuthorized(true);

        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $post_data['ReceiverID'],$post_data['Type'],$post_data['ReceiverType']);
            if ($IsBlocked) {
                 $this->response([
                        'status' => 402,
                        'message' => 'You can\'t do this activity.'
                    ], REST_Controller::HTTP_OK);

                 exit;
            }

            /*$where = "((chats.SenderID = " . $post_data['UserID'] . " AND chats.ReceiverID = " . $post_data['ReceiverID'] . " AND chats.Type = '".$post_data['Type']."' AND chats.ReceiverType = '".$post_data['ReceiverType']."') OR (chats.SenderID = " . $post_data['ReceiverID'] . " AND chats.ReceiverID = " . $post_data['UserID'] . " AND chats.Type = '".$post_data['ReceiverType']."' AND chats.ReceiverType = '".$post_data['Type']."'))";*/
            $where = "((chats.SenderID = " . $post_data['UserID'] . " AND chats.ReceiverID = " . $post_data['ReceiverID'] . " AND chats.Type = '".$post_data['Type']."' AND chats.ReceiverType = '".$post_data['ReceiverType']."') OR (chats.SenderID = " . $post_data['ReceiverID'] . " AND chats.ReceiverID = " . $post_data['UserID'] . " AND chats.Type = '".$post_data['ReceiverType']."' AND chats.ReceiverType = '".$post_data['Type']."'))";
            $chat_dates = $this->Chat_message_model->getChatMessagesDates($where);
           // echo $this->db->last_query();exit;
           // print_rm($chat_dates);
            if ($chat_dates) // if chat exist then get all messages of this chat and send in response
            {
                $i = 0;
                $chat_id = false;
                $chat_messages_arr = array();
                foreach ($chat_dates as $chat_date) {
                    $chat_id = $chat_date['ChatID'];
                    $chat_messages_arr[$i]['Date'] = strtotime($chat_date['MessageDate']);
                    $where = "c.ChatID = ".$chat_date['ChatID']." AND DATE_FORMAT(FROM_UNIXTIME(cm.CreatedAt), '%Y-%m-%d') = '" . $chat_date['MessageDate'] . "'";
                    $chat_messages_arr[$i]['Messages'] = $this->Chat_message_model->getChatMessages($where);
                    $i++;
                }
                //print_rm($chat_messages_arr);
                $this->response([
                    'status' => 200,
                    'ChatID' => $chat_id,
                    'ChatMessages' => $chat_messages_arr
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                // create a new chat
                $chat_data['Type'] = $post_data['Type'];
                $chat_data['ReceiverType'] = $post_data['ReceiverType'];
                $chat_data['SenderID'] = $post_data['UserID'];
                $chat_data['ReceiverID'] = $post_data['ReceiverID'];
                $chat_data['LastActivityAt'] = time();
                $chat_id = $this->Chat_model->save($chat_data);
                if ($chat_id > 0) {
                    $this->response([
                        'status' => 200,
                        'ChatID' => (string)$chat_id,
                        'ChatMessages' => array()
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK);
                }
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function deleteChatRoom_get()
    {
        $post_data = $this->post_data; // UserID, ChatID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $msg_count = $this->Chat_message_model->getRowsCount(array('ChatID' => $post_data['ChatID']));
            if ($msg_count > 0) {
                // do nothing as there are already messages within this chat room
                $this->response([
                    'status' => 402,
                    'message' => lang('chat_room_cant_be_deleted')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $deleted_by['ChatID'] = $post_data['ChatID'];
                $this->Chat_model->delete($deleted_by);
                $this->response([
                    'status' => 200,
                    'message' => lang('chat_room_deleted')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function sendMessage_post()
    {
        $post_data = $this->post_data; // ChatID, UserID (This will be SenderID in DB), ReceiverID, Message, Image (Optional), CreatedAt
        $this->_isUserAuthorized(true);

        if (!empty($post_data)) {

            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $post_data['ReceiverID'],$post_data['UserType'],$post_data['SenderType']);
            if ($IsBlocked) {
               $this->response([
                    'status' => 402,
                    'message' => lang('cannot_send_message')
                ], REST_Controller::HTTP_OK);
            }

            if($post_data['SenderType'] == 'booth' && isset($post_data['is_order']) && $post_data['is_order'] != 1){
                $existing_user = $this->User_model->get($post_data['UserID'], true, 'UserID');

               

                if($existing_user['PackageExpiry'] < Date('Y-m-d')){
                    $this->response([
                            'status' => 402,
                            'message' => lang('please_update_your_package')
                        ], REST_Controller::HTTP_OK);

                }


            }

            if($post_data['UserType'] == 'booth' && isset($post_data['is_order']) && $post_data['is_order'] != 1){
                $existing_user = $this->User_model->get($post_data['ReceiverID'], true, 'UserID');

               

                if($existing_user['PackageExpiry'] < Date('Y-m-d')){
                    $this->response([
                            'status' => 402,
                            'message' => lang('please_update_your_package')
                        ], REST_Controller::HTTP_OK);

                }


            }
            unset($post_data['is_order']);
            unset($post_data['SenderType']);
            $post_data['SenderID'] = $post_data['UserID'];







            $post_data['IsReadBySender'] = 'yes';
            $post_data['IsReadByReceiver'] = 'no';
            $post_data['CreatedAt'] = time();
            if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                $post_data['Image'] = uploadImage('Image', "uploads/users/");
                $post_data['CompressedImage'] = compress($post_data['Image'], "uploads/users/compressed/user-image-" . time() . '.png');
            }
            /*if (isset($post_data['VideoThumbnail']) && $post_data['VideoThumbnail'] !== '') {
                ini_set('memory_limit', '-1');
                $post_data['VideoThumbnail'] = uploadFileFromBase64($post_data['VideoThumbnail'], "uploads/products/videos/thumbnails/");
            }
            if (isset($post_data['Video']) && $post_data['Video'] !== '') {
                ini_set('memory_limit', '-1');
                $post_data['Video'] = uploadFileFromBase64($post_data['Video'], "uploads/products/videos/", 'mp4');
            }
            */
            if (isset($_FILES['VideoThumbnail']["name"][0]) && $_FILES['VideoThumbnail']["name"][0] != '') {
                ini_set('memory_limit', '-1');
                $post_data['VideoThumbnail'] =  uploadImage('VideoThumbnail', "uploads/products/videos/thumbnails/");
            }
            /*if (isset($post_data['ProductVideo']) && $post_data['ProductVideo'] !== '') {
                ini_set('memory_limit', '-1');
                $product_data['ProductVideo'] = uploadFileFromBase64($post_data['ProductVideo'], "uploads/products/videos/", 'mp4');
            }*/
            if (isset($_FILES['Video']["name"][0]) && $_FILES['Video']["name"][0] != '') {
                
                $post_data['Video'] = uploadVideo('Video', "uploads/products/videos/");

            }
            $SenderUserName = $post_data['SenderUserName'];
            unset($post_data['UserID']);
            unset($post_data['SenderUserName']);
            $chat_message_id = $this->Chat_message_model->save($post_data);
            if ($chat_message_id > 0) // if chat message is saved successfully
            {
                // getting this msg detail
                $message = $this->Chat_message_model->getChatMessages("cm.ChatMessageID = " . $chat_message_id);
                $message = $message[0];

                // updating last activity Time for chat
                $chat_data['LastActivityAt'] = time();
                $this->Chat_model->update($chat_data, array('ChatID' => $post_data['ChatID']));
                $today = date('Y-m-d');
                $chat_messages_arr['Date'] = strtotime($today);
                $chat_messages_arr['Message'] = $message;



                // send pusher call here
                pusher($chat_messages_arr, 'Booth_Channel_Chat_' . $post_data['ReceiverID'], 'Booth_Event_Chat_' . $post_data['ReceiverID']);
                
                $where = "users.UserID = " . $post_data['ReceiverID'];
                $user_info = $this->User_model->getUserInfo($where);
                if($post_data['UserType'] == 'user'){
                    $ReceiverUserName = $user_info['UserName'];
                }else{
                    $ReceiverUserName = $user_info['BoothUserName'];
                }
                $data['NotificationTextEn'] = "(".$ReceiverUserName."): ".$SenderUserName.": ".$post_data['Message']."";
                //$data['NotificationTextAr'] = '@' . $UserInfo->UserName . " started following you";
                //$data['CreatedAt'] = time();
               // $mentioned_user_ids = array($UserInfo->UserID,$logged_in_user_id);
               // $mentioned_user_names = array($UserInfo->UserName,$logged_in_user_name);
                //$mentioned_user_types = array('user',$post_data['Type']);
                //log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);

                $res = sendNotification('Booth', $data['NotificationTextEn'], $post_data, $post_data['ReceiverID']);
                /*// send notification to receiver
                $data['Type'] = 'message';
                $data['LoggedInUserID'] = $post_data['ReceiverID']; // the person who will receive this notification
                $data['UserID'] = $post_data['SenderID']; // the person who is making this notification
                $data['GroupID'] = 0;
                $data['PostID'] = 0;
                $data['PostCommentID'] = 0;
                $data['NotificationTypeID'] = 11;
                $data['CreatedAt'] = time();
                // $mentioned_user_ids = array();
                            $mentioned_user_names = array();
                            $mentioned_user_types = array();
                            log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                $user = $this->User_text_model->get($post_data['SenderID'], true, 'UserID');
                $notification_text = $user['FullName'] . " has sent you a message";
                $res = sendNotification('Hobbies', $notification_text, $data, $post_data['ReceiverID']);*/

                $this->response([
                    'status' => 200,
                    'message' => $chat_messages_arr
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function markAllMsgReadForChat_post()
    {
        $post_data = $this->post_data; // UserID, ChatID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $chat_msg_data['IsReadByReceiver'] = 'yes';
            $this->Chat_message_model->updateWhere($chat_msg_data, array('ChatID' => $post_data['ChatID'], 'ReceiverID' => $post_data['UserID']));
            $this->response([
                'status' => 200,
                'message' => "All messages marked as read for this chat\nجميع الرسائل وضعت عليها علامة للقراءة لهذه الدردشة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function checkIfUnreadMsgForUser_get()
    {
        $post_data = $this->post_data; // UserID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $unread_msg_count = $this->Chat_message_model->getRowsCount(array('ReceiverID' => $post_data['UserID'], 'IsReadByReceiver' => 'no'),false,false,false,true);
            $this->response([
                'status' => 200,
                'UnreadMessageCount' => $unread_msg_count
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function promoteMyProduct_post()
    {
        $post_data = $this->post_data; // UserID, ProductID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $check_already_promoted_product = $this->Product_model->getWithMultipleFields(array('UserID' => $post_data['UserID'], 'IsPromotedProduct' => 1));
            if ($check_already_promoted_product) {
                $this->response([
                    'status' => 402,
                    'message' => lang('product_already_promoted_for_store')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $current = time();
                $expiry_date = date('Y-m-d H:i:s', strtotime('+1 day', $current));
                $update['IsPromotedProduct'] = 1;
                $update['ProductPromotionExpiresAt'] = strtotime($expiry_date);
                $update['IsPromotionApproved'] = 1; // Pending admin approval
                $update_by['ProductID'] = $post_data['ProductID'];
                $this->Product_model->update($update, $update_by);
                $this->response([
                    'status' => 200,
                    'message' => lang('product_promoted')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getBoothPromotedProduct_get()
    {
        $post_data = $this->post_data; // UserID, BoothID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $where = "products.UserID = " . $post_data['BoothID'] . " AND products.IsPromotedProduct = 1 AND products.IsPromotionApproved = 1 AND products.ProductPromotionExpiresAt > " . time();
            $product = $this->Product_model->getProducts($where, $this->language);
            if ($product) {
                $product = $product[0];
                $check_if_product_promoted['ProductID'] = $product->ProductID;
                $check_if_product_promoted['PromotionExpiresAt >'] = time();
                $IsProductPromoted = $this->Promoted_product_model->getWithMultipleFields($check_if_product_promoted);
                $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $product->ProductID));
                $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $product->ProductID);
                $LikesCount = 0;
                if (!empty($likes)) {
                    if (isset($post_data['UserID'])) {
                        $my_arr = array();
                        $l_count = 0;
                        foreach ($likes as $key => $value) {
                            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                            if ($IsBlocked) {
                                continue;
                            }
                            $my_arr[$l_count] = $value;
                            $l_count++;
                        }
                        $likes = $my_arr;
                    }
                    $LikesCount = count($likes);
                }

                $comments = $this->Product_comment_model->getProductComments("product_comments.ProductID = " . $product->ProductID, $this->language);
                $CommentCount = 0;
                if (!empty($comments)) {
                    if (isset($post_data['UserID'])) {
                        $my_arr = array();
                        $c_count = 0;
                        foreach ($comments as $key => $value) {
                            $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                            if ($IsBlocked) {
                                continue;
                            }
                            $my_arr[$c_count] = $value;
                            $c_count++;
                        }
                        $comments = $my_arr;
                    }
                    $CommentCount = count($comments);
                }

                if (isset($post_data['UserID'])) {
                    $check_already_liked['UserID'] = $post_data['UserID'];
                    $check_already_liked['ProductID'] = $product->ProductID;
                    $check_like = $this->Product_like_model->getWithMultipleFields($check_already_liked);
                    if ($check_like) {
                        $product->IsLiked = '1';
                    } else {
                        $product->IsLiked = '0';
                    }
                } else {
                    $product->IsLiked = '0';
                }
                if (isset($post_data['UserID'])) {
                    $check_wishlist['UserID'] = $post_data['UserID'];
                    $check_wishlist['ProductID'] = $product->ProductID;
                    $check_wishlist = $this->User_wishlist_model->getWithMultipleFields($check_wishlist);
                    if ($check_wishlist) {
                        $product->IsInWishlist = '1';
                    } else {
                        $product->IsInWishlist = '0';
                    }
                } else {
                    $product->IsInWishlist = '0';
                }

                $product_comments = $this->Product_comment_model->getProductComments('product_comments.ProductID = ' . $product->ProductID, $this->language, 'product_comments.ProductCommentID', 'DESC', 3, 0);

                if (isset($post_data['UserID'])) {
                    if (count($product_comments) > 0) {
                        $my_arr = array();
                        $i = 0;
                        foreach ($product_comments as $key => $value) {
                            $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                            if (!$IsBlockedAsBooth) {
                                $my_arr[$i] = $value;
                                $i++;
                            }
                        }
                        $product_comments = $my_arr;
                    }
                }

                $product->Comments = !empty($product_comments) ? $product_comments : array();
                $product->IsPromoted = $IsProductPromoted ? '1' : '0';
                $product->ProductImages = $product_images ? $product_images : array();
                $product->LikesCount = (string)$LikesCount;
                $product->CommentCount = (string)$CommentCount;
                $product->ProductAverageRating = productAverageRating($product->ProductID);
                $product->ProductRatingsCount = productAverageRating($product->ProductID, true);
                $this->response([
                    'status' => 200,
                    'product' => $product
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('promoted_product_not_found')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    private function markProductsPromotionInactive() // set for cronjob later
    {
        $fetch_by['IsPromotedProduct'] = 1;
        $fetch_by['ProductPromotionExpiresAt <'] = time();
        $products = $this->Product_model->getMultipleRows($fetch_by);
        if ($products) {
            foreach ($products as $product) {
                $update['IsPromotedProduct'] = 0;
                $update['IsPromotionApproved'] = 0;
                $update_by['ProductID'] = $product->ProductID;
                $this->Product_model->update($update, $update_by);
            }
        }
    }

    public function rateOrderRequest_post()
    {
        $post_data = $this->post_data; // UserID, OrderRequestID, OrderRequestRating, OrderRequestReview
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $order_request = $this->Order_request_model->get($post_data['OrderRequestID'], false, 'OrderRequestID');
            if ($order_request->OrderRequestRating > 0) {
                $this->response([
                    'status' => 402,
                    'message' => lang('seller_already_rated')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {

                // notification
                $order_data = $this->Order_model->get($order_request->OrderID,false,'OrderID');
                $user_data = $this->User_model->get($post_data['UserID'],false,'UserID');
                $user_name = $user_data->UserName;
                $notification_data = array();
                // saving notification
                    $notification_data['LoggedInUserID'] = $order_request->BoothID; // the person whose post is this, who will receive this notification
                    $notification_data['UserType'] = 'booth';
                    $notification_data['Type'] = 'rate_booth_for_order_request';
                    $notification_data['UserID'] =  $order_data->UserID;// the person who is making this notification
                    $notification_data['OrderRequestID'] = $post_data['OrderRequestID']; 
                   
                    $notification_data['NotificationTypeID'] = 23;

                    // check if notification is already sent to user for this
                    $already_notification_sent = $this->User_notification_model->getWithMultipleFields($notification_data);
                    if (!$already_notification_sent) {
                        
                        $notification_data['NotificationTextEn'] = '@' . $user_name .' rated your booth.';
                        $notification_data['NotificationTextAr'] = '@' . $user_name .' rated your booth.';
                        $notification_data['CreatedAt'] = time();
                        $mentioned_user_ids = array($order_data->UserID);
                        $mentioned_user_names = array($user_name);
                        $mentioned_user_types = array('user');
                        //log_notification($notification_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                       // $res = sendNotification('Booth', $notification_data['NotificationTextEn'], $notification_data, $order_request->BoothID);
                    }


            //end notification 
                $update['OrderRequestRating'] = $post_data['OrderRequestRating'];
                $update['OrderRequestReview'] = $post_data['OrderRequestReview'];
                $update_by['OrderRequestID'] = $post_data['OrderRequestID'];
                $this->Order_request_model->update($update, $update_by);
                $this->response([
                    'status' => 200,
                    'message' => lang('seller_rated')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function rateUserForOrderRequest_post()
    {
        $post_data = $this->post_data; // UserID, OrderRequestID, UserOrderRequestRating, UserOrderRequestReview
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $order_request = $this->Order_request_model->get($post_data['OrderRequestID'], false, 'OrderRequestID');
            if ($order_request->UserOrderRequestRating > 0) {
                $this->response([
                    'status' => 402,
                    'message' => lang('buyer_already_rated')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $update['UserOrderRequestRating'] = $post_data['UserOrderRequestRating'];
                $update['UserOrderRequestReview'] = $post_data['UserOrderRequestReview'];
                $update_by['OrderRequestID'] = $post_data['OrderRequestID'];
                $this->Order_request_model->update($update, $update_by);

                // notification
                $order_data = $this->Order_model->get($order_request->OrderID,false,'OrderID');
                $booth_data = $this->User_model->get($post_data['UserID'],false,'UserID');
                $booth_name = $booth_data->BoothUserName;
                $notification_data = array();
                    // saving notification
                        $notification_data['LoggedInUserID'] = $order_data->UserID; // the person whose post is this, who will receive this notification
                        $notification_data['UserType'] = 'user';
                        $notification_data['Type'] = 'rate_user_for_order_request';
                        $notification_data['UserID'] = $order_request->BoothID; // the person who is making this notification
                        $notification_data['OrderRequestID'] = $post_data['OrderRequestID']; 
                       
                        $notification_data['NotificationTypeID'] = 23;

                        // check if notification is already sent to user for this
                        $already_notification_sent = $this->User_notification_model->getWithMultipleFields($notification_data);
                        if (!$already_notification_sent) {
                            
                            $notification_data['NotificationTextEn'] = $message = '@' . $booth_name .' rated you.';
                            $notification_data['NotificationTextAr'] = '@' . $booth_name .' rated you.';
                            $notification_data['CreatedAt'] = time();
                            $mentioned_user_ids = array($order_request->BoothID);
                            $mentioned_user_names = array($booth_name);
                            $mentioned_user_types = array('booth');
                            //log_notification($notification_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                            //$res = sendNotification('Booth', $notification_data['NotificationTextEn'], $notification_data, $order_data->UserID);
                        }


                //end notification 








                $this->response([
                    'status' => 200,
                    'message' => lang('buyer_rated')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function rateProduct_post()
    {
        $post_data = $this->post_data; // ProductID, UserID, OrderRequestID, Rating, Review
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $check_already_rated['ProductID'] = $post_data['ProductID'];
            $check_already_rated['UserID'] = $post_data['UserID'];
            $check_already_rated['OrderRequestID'] = $post_data['OrderRequestID'];
            $product_rating = $this->Product_rating_model->getWithMultipleFields($check_already_rated);
            if ($product_rating) {
                $this->response([
                    'status' => 402,
                    'message' => lang('product_already_rated')
                ], REST_Controller::HTTP_OK);
            } else {
                $inserted_id = $this->Product_rating_model->save($post_data);
                if ($inserted_id > 0) {

                    // notification
                $product_data = $this->Product_model->get($post_data['ProductID'],false,'ProductID');
                $user_data = $this->User_model->get($post_data['UserID'],false,'UserID');
                $user_name = $user_data->UserName;
                $notification_data = array();
                // saving notification
                    $notification_data['LoggedInUserID'] = $product_data->UserID; // the person whose post is this, who will receive this notification
                    $notification_data['UserType'] = 'booth';
                    $notification_data['Type'] = 'rate_product';
                    $notification_data['UserID'] =  $post_data['UserID'];// the person who is making this notification
                    $notification_data['ProductID'] = $post_data['ProductID']; 
                   
                    $notification_data['NotificationTypeID'] = 23;

                    // check if notification is already sent to user for this
                    $already_notification_sent = $this->User_notification_model->getWithMultipleFields($notification_data);
                    if (!$already_notification_sent) {
                        
                        $notification_data['NotificationTextEn'] = '@' . $user_name .' rated your product.';
                        $notification_data['NotificationTextAr'] = '@' . $user_name .' rated your product.';
                        $notification_data['CreatedAt'] = time();
                        $mentioned_user_ids = array($post_data['UserID']);
                        $mentioned_user_names = array($user_name);
                        $mentioned_user_types = array('user');
                        //log_notification($notification_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                       // $res = sendNotification('Booth', $notification_data['NotificationTextEn'], $notification_data, $product_data->UserID);
                    }


            //end notification 


                    $this->response([
                        'status' => 200,
                        'message' => lang('product_rated')
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 402,
                        'message' => lang('something_went_wrong')
                    ], REST_Controller::HTTP_OK);
                }
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getPointsHistory_post()
    {
        $post_data = $this->post_data; // UserID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $points_history = $this->User_points_history_model->getMultipleRows(array('UserID' => $post_data['UserID']));
            $this->response([
                'status' => 200,
                'message' => $points_history ? $points_history : array()
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function updateAppStatus_post()
    {
        $post_data = $this->post_data; // UserID, AppStatus (0 means background, 1 means foreground)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            $this->User_model->update(array('AppStatus' => $post_data['AppStatus']), array('UserID' => $post_data['UserID']));
            $this->response([
                'status' => 200,
                'message' => "App status updated."
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getPromoCodes_post()
    {
        $post_data = $this->post_data; // UserID
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }

        if (isset($post_data['OtherUserID'])) {
        
            $OtherUserID = $post_data['OtherUserID'];
            $coupons = $this->Coupon_model->getAllJoinedData(false, 'CouponID', $this->language, "coupons.UserID = " . $post_data['OtherUserID'],'DESC','CreatedAt');
        
        } else {
            $coupons = $this->Coupon_model->getAllJoinedData(false, 'CouponID', $this->language, "coupons.UserID = " . $post_data['UserID'],'DESC','CreatedAt');   
        }

        $this->response([
            'status' => 200,
            'coupons' => !empty($coupons) ? $coupons : array()
        ], REST_Controller::HTTP_OK);
    }

    public function createPromoCode_post()
    {
        $post_data = $this->post_data; // Title, UserID (Logged in UserID), CouponCode, DiscountType (Fixed, Percentage), DiscountFactor, ExpiryDate, UsageCount, IsActive (1, 0)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {

            $existing_user = $this->User_model->get($post_data['UserID'], true, 'UserID');

            //$get_user_data = $this->User_model->get($post_data['UserID'],false,'UserID');
            if($existing_user['LastState'] == 'booth'){
                if($existing_user['PackageExpiry'] < Date('Y-m-d')){
                    $this->response([
                            'status' => 402,
                            'message' => lang('please_update_your_package')
                        ], REST_Controller::HTTP_OK);

                }
            }

            $this->form_validation->set_rules('CouponCode', 'Promocode', 'required|is_unique[coupons.CouponCode]');
            if ($this->form_validation->run() == FALSE) {

                $this->response([
                    'status' => 402,
                    'message' => lang('coupon_must_contain_unique_value')
                ], REST_Controller::HTTP_OK);

                exit;
                
            }

            $save_parent_data = array();
            $save_child_data = array();
            $getSortValue = $this->Coupon_model->getLastRow('CouponID');
            $sort = 0;
            if (!empty($getSortValue)) {

                $sort = $getSortValue['SortOrder'] + 1;
            }
            $UserID = $post_data['UserID'];

            $count = $this->Coupon_model->getRowsCount(array('UserID' => $UserID));

            if ($count >= 3) {
                $this->response([
                    'status' => 402,
                    'message' => lang('promocode_limit')
                ], REST_Controller::HTTP_OK);
                exit;
            }


            $couponcode = $post_data['CouponCode'];
            $save_parent_data['UserID'] = $UserID;
            $save_parent_data['CouponCode'] = $couponcode;
            //$save_parent_data['UsageCount'] = $post_data['UsageCount'];
            //$save_parent_data['TotalCount'] = $post_data['UsageCount'];
            $save_parent_data['DiscountType'] = $post_data['DiscountType'];
            $save_parent_data['DiscountFactor'] = $post_data['DiscountFactor'];
            $save_parent_data['ExpiryDate'] = strtotime($post_data['ExpiryDate']);
            $save_parent_data['IsActive'] = $post_data['IsActive'];
            $save_parent_data['SortOrder'] = $sort;
            $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $insert_id = $this->Coupon_model->save($save_parent_data);
            if ($insert_id > 0) {
                $system_languages = getSystemLanguages();
                foreach ($system_languages as $system_language) {
                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['CouponID'] = $insert_id;
                    $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $this->Coupon_text_model->save($save_child_data);
                }

                $a = $this->Follower_model->getFollower($UserID, 'booth');
                $UserInfo = getUserInfo($UserID, $this->language);
                $BoothUserName = $UserInfo->BoothUserName;

                $activity_data = array();

                foreach ($a as $user) {

                    $activity_data['UserType'] = 'user';
                    $activity_data['Type'] = 'promocode';
                    $activity_data['NotificationTypeID'] = 20;
                    $activity_data['LoggedInUserID'] = $user['UserID'];
                    $activity_data['UserID'] = $UserID;
                    $activity_data['CouponID'] = $insert_id;
                    $activity_data['NotificationTextEn'] = '@'. $BoothUserName .' has created a new Promo code '.$couponcode.'. Shop now to avail the discounts';
                    $activity_data['NotificationTextAr'] = '@'. $BoothUserName .' قام بإنشاء كود خصم جديد '.$couponcode.'.  تسوق الآن واستغل الخصم!';

                    $activity_data['CreatedAt'] = time();
                    $mentioned_user_ids = array($UserID);
                    $mentioned_user_names = array($BoothUserName);
                    $mentioned_user_types = array('booth');

                    log_notification($activity_data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                    $res = sendNotification('Booth', $activity_data['NotificationTextEn'], $activity_data, $user['UserID']);

                    //$this->User_notification_model->save($activity_data);
                }



                $coupons = $this->Coupon_model->getAllJoinedData(false, 'CouponID', $this->language, "coupons.CouponID = " . $insert_id);
                $this->response([
                    'status' => 200,
                    'message' => lang('coupon_created_successfully'),
                    'coupon' => $coupons[0]
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function updatePromoCode_post()
    {
        $post_data = $this->post_data; // UserID, CouponID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            if (isset($post_data['CouponID']) && $post_data['CouponID'] > 0) {
                $save_parent_data = array();
                $save_child_data = array();
                $parents_fields = $this->Coupon_model->getFields();
                $child_fields = $this->Coupon_text_model->getFields();
                foreach ($post_data as $key => $value) {
                    if (in_array($key, $parents_fields)) {
                        $save_parent_data[$key] = $value;
                    } elseif (in_array($key, $child_fields)) {
                        $save_child_data[$key] = $value;
                    }
                }
                $save_parent_data['UpdatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $update_by['CouponID'] = $post_data['CouponID'];
                $this->Coupon_model->update($save_parent_data, $update_by);
                $this->Coupon_text_model->update($save_child_data, $update_by);
                $coupons = $this->Coupon_model->getAllJoinedData(false, 'CouponID', $this->language, "coupons.CouponID = " . $post_data['CouponID']);
                $this->response([
                    'status' => 200,
                    'message' => lang('coupon_updated_successfully'),
                    'coupon' => $coupons[0]
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getPromoCodeDetail_post()
    {
        $post_data = $this->post_data; // UserID, CouponID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            if (isset($post_data['CouponID']) && $post_data['CouponID'] > 0) {
                $coupons = $this->Coupon_model->getAllJoinedData(false, 'CouponID', $this->language, "coupons.CouponID = " . $post_data['CouponID']);
                $this->response([
                    'status' => 200,
                    'message' => lang('coupon_updated_successfully'),
                    'coupon' => $coupons[0]
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function deletePromoCode_post()
    {
        $post_data = $this->post_data; // UserID, CouponID
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            if (isset($post_data['CouponID']) && $post_data['CouponID'] > 0) {
                $this->Coupon_text_model->delete(array('CouponID' => $post_data['CouponID']));
                $this->Coupon_model->delete(array('CouponID' => $post_data['CouponID']));
                
                $deleted_by['CouponID'] = $post_data['CouponID'];
                $this->User_notification_model->delete($deleted_by);
                
                $this->response([
                    'status' => 200,
                    'message' => lang('coupon_deleted_successfully')
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getProductDisclaimer_get()
    {
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $site_settings = site_settings();
        $this->response([
            'status' => 200,
            'ProductDisclaimer' => $site_settings->ProductDisclaimer
        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }

    public function getPromoCodeDisclaimer_get()
    {
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $site_settings = site_settings();
        $this->response([
            'status' => 200,
            'PromoCodeDisclaimer' => $site_settings->PromoCodeDisclaimer
        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }

    public function checkIfRatingPending_post()
    {
        $post_data = $this->post_data; // UserID, Type (user, booth)
        $this->_isUserAuthorized();
        if (!empty($post_data)) {
            if ($post_data['Type'] == 'booth') {
                $where = 'orders_requests.BoothID = ' . $post_data['UserID'] . ' AND orders_requests.OrderRequestRating = 0';
                $order_request = $this->Order_request_model->getOrdersRequests($where, $this->language);
                $this->response([
                    'status' => 200,
                    'RatingPending' => $order_request ? $order_request[0]['OrderRequestID'] : 0
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } elseif ($post_data['Type'] == 'user') {
                $where = 'orders.UserID = ' . $post_data['UserID'] . ' AND orders_requests.UserOrderRequestRating = 0';
                $order_request = $this->Order_request_model->getOrdersRequests($where, $this->language);
                $this->response([
                    'status' => 200,
                    'RatingPending' => $order_request ? $order_request[0]['OrderRequestID'] : 0
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => 402,
                    'message' => lang('something_went_wrong')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getProductsForSearchGrid_post()
    {
        $post_data = $this->post_data; // UserID
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        if (!empty($post_data)) {
            $start = 0;
            //$limit = 12;
            $limit = false;

            if (isset($post_data['Start'])) {
               // $start = $post_data['Start'];
            }

            $user_categories_arr = false;
            $where = "users.PackageExpiry >= CURDATE() AND products.IsActive = 1 AND products.Hide = 0";
            if(isset($post_data['ProductID'])){
                $where .=" AND products.ProductID = ".$post_data['ProductID']; 
            }
            if (isset($post_data['UserID']) && $post_data['UserID'] > 0) {
                //$user_categories = $this->User_category_model->getMultipleRows(array('UserID' => $post_data['UserID'], 'Type' => 'user'));//old logic
                $user_categories = $this->Category_model->getAll();// we need to get data over all category base
                if ($user_categories) {
                    foreach ($user_categories as $user_category) {
                        $user_categories_arr[] = $user_category->CategoryID;
                    }
                }
            }
            $products_arr = array();
            $products = $this->Product_model->getProductsForUserFollowCategory($where, $this->language, $limit, $start, 'products_text.Title', 'ASC', $user_categories_arr,true);
            if ($products) {
                $i = 0;
                foreach ($products as $product) {
                    $products_arr[$i] = $product;
                    $products_arr[$i]['ItemType'] = 'Product';

                    if (isset($post_data['UserID'])) {
                        $check_already_liked['UserID'] = $post_data['UserID'];
                        $check_already_liked['ProductID'] = $product['ProductID'];
                        $check_like = $this->Product_like_model->getWithMultipleFields($check_already_liked);
                        if ($check_like) {
                            $products_arr[$i]['IsLiked'] = '1';
                        } else {
                            $products_arr[$i]['IsLiked'] = '0';
                        }


                        $check_wishlist = array();
                        $check_wishlist['UserID'] = $post_data['UserID'];
                        $check_wishlist['ProductID'] = $product['ProductID'];
                        $check_wishlist = $this->User_wishlist_model->getWithMultipleFields($check_wishlist);
                        if ($check_wishlist) {
                            $products_arr[$i]['IsInWishlist'] = '1';
                        } else {
                            $products_arr[$i]['IsInWishlist'] = '0';
                        }





                    } else {
                        $products_arr[$i]['IsLiked'] = '0';
                        $products_arr[$i]['IsInWishlist'] = '0';
                    }

                    $product_images = $this->Product_image_model->getMultipleRows(array('ProductID' => $product['ProductID']));
                    $products_arr[$i]['ProductImages'] = $product_images ? $product_images : array();
                    $likes = $this->Product_like_model->getProductLikesWhere("product_likes.ProductID = " . $product['ProductID']);
                    $LikesCount = 0;
                    if (!empty($likes)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $l_count = 0;
                            foreach ($likes as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$l_count] = $value;
                                $l_count++;
                            }
                            $likes = $my_arr;
                        }
                        $LikesCount = count($likes);
                    }
                    $comments = $this->Product_comment_model->getProductComments("product_comments.ProductID = " . $product['ProductID'], $this->language);
                    $CommentCount = 0;
                    if (!empty($comments)) {
                        if (isset($post_data['UserID'])) {
                            $my_arr = array();
                            $c_count = 0;
                            foreach ($comments as $key => $value) {
                                $IsBlocked = $this->checkIfUserBlocked($post_data['UserID'], $value->UserID);
                                if ($IsBlocked) {
                                    continue;
                                }
                                $my_arr[$c_count] = $value;
                                $c_count++;
                            }
                            $comments = $my_arr;
                        }
                        $CommentCount = count($comments);
                    }
                    $products_arr[$i]['LikesCount'] = (string)$LikesCount;
                    $products_arr[$i]['CommentCount'] = (string)$CommentCount;
                    $i++;
                }
            }

            if (count($products_arr) > 0) {
                if (isset($post_data['UserID'])) {
                    $my_arr = array();
                    $i = 0;
                    foreach ($products_arr as $key => $value) {
                        $IsBlockedAsBooth = $this->checkIfUserBlocked($post_data['UserID'], $value['UserID'], 'booth');
                        if (!$IsBlockedAsBooth) {
                            $my_arr[$i] = $value;
                            $i++;
                        }
                    }
                    $products_arr = $my_arr;
                }
            }
            $this->response([
                'status' => 200,
                'products' => $products_arr
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

}