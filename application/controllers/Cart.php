<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        //checkFrontendSession();
        $this->load->model('Temp_order_model');
        $this->load->model('User_wishlist_model');
        $this->load->model('Product_model');
        $this->load->model('Coupon_model');
        $this->data['language'] = $this->language;
    }

    public function index()
    {
        //$data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);

       // print_rm($this->data['cart_items']);
        
       // $data['wishlist_items'] = $this->Product_model->getWishlistItems($this->UserID, $this->language);
        if (empty($this->data['cart_items'])) {
            //$this->session->set_flashdata('message', lang('your_cart_is_empty'));
             $this->data['view'] = 'frontend/empty-cart';
            $this->load->view('frontend/layouts/default', $this->data);
        }else{

            $shipment_method = getTaxShipmentCharges('Shipment', true);
            if ($shipment_method) {
                $this->session->set_userdata('ShipmentMethodIDForBooking', $shipment_method->TaxShipmentChargesID);
            } else {
                $this->session->set_userdata('ShipmentMethodIDForBooking', 0);
            }

            $this->session->set_userdata('PaymentMethodForBooking', 'COD');
            //$this->session->set_userdata('PaymentMethodForBooking', 'PayTab');

            

            $this->data['view'] = 'frontend/cart/checkout-cart';
            $this->load->view('frontend/layouts/default', $this->data);

        }
        
    }

    public function addToCart()
    {
        $fetch_by['UserID'] = $this->UserID;
        $fetch_by['ProductID'] = $this->input->post('ProductID');
        $fetch_by['ItemType'] = $this->input->post('ItemType');
        $posted_product_quantity = $this->input->post('Quantity');
        $posted_product_price = $this->input->post('TempItemPrice');
        $already_added = $this->Temp_order_model->getWithMultipleFields($fetch_by);
        if ($already_added) {
            $update['Quantity'] = $already_added->Quantity + $posted_product_quantity;
            $update_by['TempOrderID'] = $already_added->TempOrderID;
            $this->Temp_order_model->update($update, $update_by);
            $response['status'] = true;
            $response['message'] = lang('added_to_basket');
            $response['total_cart_items'] = getTotalProduct($this->UserID);
            echo json_encode($response);
            exit();
        } else {
            $save_data['UserID'] = $this->UserID;
            $save_data['Quantity'] = $posted_product_quantity;
            $save_data['ProductID'] = $this->input->post('ProductID');
            $save_data['ItemType'] = $this->input->post('ItemType');
            $save_data['CompanyID'] = $this->companyData->CompanyID;
           // $save_data['IsCorporateItem'] = $this->input->post('IsCorporateItem');
            $save_data['TempItemPrice'] = $posted_product_price;
            $insert_id = $this->Temp_order_model->save($save_data);
            if ($insert_id > 0) {
                $response['status'] = true;
                $response['message'] = lang('added_to_basket');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('something_went_wrong');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            }
        }
    }

    public function updateCart()
    {
        $update['Quantity'] = $this->input->post('Quantity');
        $update_by['TempOrderID'] = $this->input->post('TempOrderID');
        $price = $this->input->post('Price');


        $this->Temp_order_model->update($update, $update_by);
        $response['total'] = number_format($update['Quantity'] * $price, 2);
        $response['status'] = true;
        $response['message'] = lang('basket_updated');
        $response['total_cart_items'] = getTotalProduct($this->UserID);
        echo json_encode($response);
        exit();
    }

    public function removeFromCart()
    {
        $deleted_by['TempOrderID'] = $this->input->post('TempOrderID');
        if($deleted_by['TempOrderID'] == -1){
            $this->Temp_order_model->delete(array('UserID' => $this->UserID));
        }else{
            $this->Temp_order_model->delete($deleted_by);
        }
        
        $response['status'] = true;
        $response['reload'] = true;
        $response['message'] = lang('removed_from_basket');
        echo json_encode($response);
        exit();
    }

    public function addToWishlist()
    {
        if ($this->session->userdata('user')) {
            $post_data = $this->input->get();
            $data['UserID'] = $this->session->userdata['user']->UserID;
            $data['ItemID'] = $post_data['ID'];
            $data['ItemType'] = $post_data['Type'];
            $wishlist = $this->User_wishlist_model->getWithMultipleFields($data);
            if ($wishlist) {
                $this->User_wishlist_model->delete($data);
                $response['class'] = "p_unliked";
                $response['message'] = lang('removed_from_wishlist');
                echo json_encode($response);
                exit();
            } else {
                $saved_id = $this->User_wishlist_model->save($data);
                if ($saved_id > 0) {

                    $deleted_by['UserID'] = $this->session->userdata['user']->UserID;
                    $deleted_by['ProductID'] = $post_data['ID'];
                    $this->Temp_order_model->delete($deleted_by);
                    $response['class'] = "p_liked";
                    $response['message'] = lang('added_to_wishlist');
                    echo json_encode($response);
                    exit();
                } else {
                    $response['class'] = "";
                    $response['message'] = lang('something_went_wrong');
                    echo json_encode($response);
                    exit();
                }
            }
        } else {
            $response['class'] = "";
            $response['message'] = lang('need_to_login_wishlist');
            echo json_encode($response);
            exit();
        }
    }

    public function applyCoupon()
    {
        $post_data = $this->input->post(); // CouponCode, Date (timestamp of the date when coupon is being applied)
        $coupon = $this->Coupon_model->getWithMultipleFields(array('CouponCode' => $post_data['CouponCode']), true);
        if ($coupon) {
            $today = date('Y-m-d');
            $expire = $coupon['ExpiryDate'];
            $today_dt = new DateTime($today);
            $expire_dt = new DateTime($expire);
            if ($expire_dt > $today_dt && $coupon['UsageCount'] > 0) {
                // saving coupon in session
                $this->session->set_userdata('order_coupon', $coupon);
                // $update['UsageCount'] = $coupon['UsageCount'] - 1;
                // $update_by['CouponID'] = $coupon['CouponID'];
                // $this->Coupon_model->update($update, $update_by);
                $response['status'] = true;
                $response['message'] = lang('coupon_applied');
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('coupon_expired');
                echo json_encode($response);
                exit();
            }
        } else {
            $response['status'] = false;
            $response['message'] = lang('coupon_invalid');
            echo json_encode($response);
            exit();
        }
    }

    public function removeCoupon()
    {
        $this->session->unset_userdata('order_coupon');
        $response['message'] = lang('coupon_cleared');
        echo json_encode($response);
        exit();
    }

    public function addChocoBoxToCart()
    {
        $product_ids = $this->input->post('ProductIDs');
        // dump($exploded_p_ids);
        if ($product_ids == '') {
            $response['status'] = false;
            $response['message'] = lang('please_add_items_to_box');
            $response['total_cart_items'] = getTotalProduct($this->UserID);
            echo json_encode($response);
            exit();
        } else {
            $fetch_by['UserID'] = $this->UserID;
            $fetch_by['CustomizedOrderProductIDs'] = $product_ids;
            $fetch_by['ItemType'] = $this->input->post('ItemType');
            $fetch_by['CustomizedBoxID'] = $this->input->post('CustomizedBoxID');
            $posted_product_quantity = $this->input->post('Quantity');
            $posted_product_price = $this->input->post('TempItemPrice');
            $already_added = $this->Temp_order_model->getWithMultipleFields($fetch_by);
            if ($already_added) {
                $update['Quantity'] = $already_added->Quantity + $posted_product_quantity;
                $update_by['TempOrderID'] = $already_added->TempOrderID;
                $this->Temp_order_model->update($update, $update_by);
                $response['status'] = true;
                $response['message'] = lang('added_to_basket');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            } else {
                $save_data['UserID'] = $this->UserID;
                $save_data['Quantity'] = $posted_product_quantity;
                $save_data['CustomizedOrderProductIDs'] = $product_ids;
                $save_data['ItemType'] = $this->input->post('ItemType');
                $save_data['CustomizedBoxID'] = $this->input->post('CustomizedBoxID');
                $save_data['Ribbon'] = $this->input->post('RibbonColor');
                $save_data['TempItemPrice'] = $posted_product_price;
                $insert_id = $this->Temp_order_model->save($save_data);
                if ($insert_id > 0) {
                    $response['status'] = true;
                    $response['message'] = lang('added_to_basket');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    echo json_encode($response);
                    exit();
                } else {
                    $response['status'] = false;
                    $response['message'] = lang('something_went_wrong');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    echo json_encode($response);
                    exit();
                }
            }
        }
    }

    public function addCustomizedShapeToCart()
    {
        if (isset($_FILES['CustomizedShapeImage']["name"]) && $_FILES['CustomizedShapeImage']["name"] != '') {
            $save_data['UserID'] = $this->UserID;
            $save_data['Quantity'] = $this->input->post('Quantity');
            $save_data['CustomizedShapeImage'] = uploadImage('CustomizedShapeImage', 'uploads/images/');
            if ($save_data['CustomizedShapeImage'] == '')
            {
                $response['status'] = false;
                $response['message'] = lang('upload_allowed_file_type');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            }
            $save_data['ItemType'] = $this->input->post('ItemType');
            $save_data['TempItemPrice'] = $this->input->post('TempItemPrice');
            $insert_id = $this->Temp_order_model->save($save_data);
            if ($insert_id > 0) {
                $response['status'] = true;
                $response['message'] = lang('added_to_basket');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('something_went_wrong');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            }
        } else {
            $response['status'] = false;
            $response['message'] = lang('upload_shape_image');
            $response['total_cart_items'] = getTotalProduct($this->UserID);
            echo json_encode($response);
            exit();
        }
    }


}