<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkFrontendSession();
        $this->load->model('Temp_order_model');
        $this->load->model('User_address_model');
        $this->load->model('Tax_shipment_charges_model');
        $this->load->model('Order_model');
        $this->load->model('Product_model');
        $this->data['language'] = $this->language;

    }

    public function index()
    {

        redirect(base_url('address'));//login change to that page
        if (isset($_REQUEST['p']) && $_REQUEST['p'] == 'checkout') {
            $this->checkout_address();
        }

        $this->data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($this->data['cart_items'])) {
            redirect('product');
        }
        $this->data['address'] = $this->User_address_model->getAddresses("user_address.IsDefault = 1 AND user_address.UserID = " . $this->UserID);
        $this->data['address_for_payment_collection'] = $this->User_address_model->getAddresses("user_address.UseForPaymentCollection = 1 AND user_address.UserID = " . $this->UserID);

        // checking here if collect from store cookie is not set and default address and payment collection address are not found then set flash message to select address properly and redirect to same page again
        if (!isset($_COOKIE['CollectFromStore'])) {
            if (empty($this->data['address']) || empty($this->data['address_for_payment_collection'])) {
                $this->session->set_flashdata('message', 'You must select address for delivery and payment collection in order to proceed with checkout');
                redirect(base_url('address'));
            }
        }

        $this->data['available_cities'] = $this->Product_model->getCities(false,$this->language);

        $this->data['view'] = 'frontend/checkout-payment';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function checkout_address()
    {
        $this->data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($this->data['cart_items'])) {
            redirect(base_url());
        }
        $this->data['address'] = $this->User_address_model->getAddresses("user_address.IsDefault = 1 AND user_address.UserID = " . $this->UserID);
        $this->data['view'] = 'frontend/checkout-address';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function thank_you($order_id = '')
    {
        $this->session->unset_userdata('order_coupon');
        $this->session->unset_userdata('DeliveryStoreID');
        $this->session->unset_userdata('DeliveryStoreTitle');
        $order = new Order_model();
        // $this->data['order'] = $this->Order_model->getUserLastOrderDetails($this->UserID, $this->language);
        if($order_id == ''){
            $where = "orders.UserID = " . $this->UserID;
        }else{
            $where = "orders.OrderID = " . base64_decode($order_id);
        }
        


        $this->data['order'] = $order->getOrders($where, 1, 0, $this->language, 'DESC')[0];
        
        //$this->data['payment_address'] = $this->User_address_model->getAddresses("user_address.AddressID = " . $this->data['order']->AddressIDForPaymentCollection)[0];
        $this->data['view'] = 'frontend/checkout-done';
        $this->load->view('frontend/layouts/default', $this->data);
    }
    public function pay_tab($order_id)
    {
        $order_id = base64_decode($order_id);
        $this->session->unset_userdata('order_coupon');
        $order = new Order_model();
        // $this->data['order'] = $this->Order_model->getUserLastOrderDetails($this->UserID, $this->language);
        $where = "orders.OrderID = " . $order_id ;
        $this->data['order'] = $order->getOrders($where, 1, 0, $this->language, 'DESC')[0];

        if($this->data['order']->TransactionID != ''){
            $this->session->set_flashdata('message', lang('already_amount_paid'));
            redirect(base_url());
        }
        //print_rm($this->data['order']);
        $this->data['view'] = 'frontend/checkout-payment-paytab';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function changeShipmentMethod()
    {
        $ShipmentMethodID = $this->input->post('ShipmentMethodID');
        $this->session->set_userdata('ShipmentMethodIDForBooking', $ShipmentMethodID);
        $where = "tax_shipment_charges.TaxShipmentChargesID = " . $ShipmentMethodID;
        $shipment_method = $this->Tax_shipment_charges_model->getAllJoinedData(false, 'TaxShipmentChargesID', $this->language, $where);
        $response['message'] = lang('shipment_method_changed');
        echo json_encode($response);
        exit();
    }
    public function unsetShipmentMethod()
    {
        
        $this->session->unset_userdata('ShipmentMethodIDForBooking');
        
    }

    public function changePaymentMethod()
    {
        $PaymentMethod = $this->input->post('PaymentMethod');
        $this->session->set_userdata('PaymentMethodForBooking', $PaymentMethod);
        $response['message'] = lang('payment_method_changed');
        echo json_encode($response);
        exit();
    }

    public function changeDeliveryStoreID()
    {
        $StoreID = $this->input->post('StoreID');
        $StoreTitle = $this->input->post('StoreTitle');
        $this->session->set_userdata('DeliveryStoreID', $StoreID);
        $this->session->set_userdata('DeliveryStoreTitle', $StoreTitle);
        $response['message'] = lang('delivery_store_id_selected');
        echo json_encode($response);
        exit();
    }

    public function unsetDeliveryStoreID()
    {
        
        $this->session->unset_userdata('DeliveryStoreID');
        $this->session->unset_userdata('DeliveryStoreTitle');
        $response['message'] = lang('payment_method_changed');
        echo json_encode($response);
        exit();
        
    }

}