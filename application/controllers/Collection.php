<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collection extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Collection_model');
        $this->load->model('Collection_rating_model');
        $this->load->model('Collection_review_model');
    }

    public function index()
    {
        $data['collections'] = $this->Collection_model->getAllJoinedData(false, 'CollectionID', $this->language);
        $data['view'] = 'frontend/collections';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function detail($collection_title)
    {
        $collection_id = collectionID($collection_title);
        $data['collection'] = $this->Collection_model->getJoinedData(false, 'CollectionID', "collections.CollectionID = " . $collection_id . " AND system_languages.ShortCode = '" . $this->language . "'");
        
        $data['result'] = $data['collection'][0];
        $where = '';
        if (empty($data['collection']))
        {
            redirect(base_url());
        }
        if ($this->session->userdata('user'))
        {
            $data['rating'] = $this->Collection_rating_model->getWithMultipleFields(array('CollectionID' => $collection_id, 'UserID' => $this->session->userdata['user']->UserID));
        } else {
            $data['rating'] = false;
        }
        if ($this->session->userdata('user'))
        {
            $data['review'] = $this->Collection_review_model->getWithMultipleFields(array('CollectionID' => $collection_id, 'UserID' => $this->session->userdata['user']->UserID));
            $where = 'collection_reviews.UserID != '.$this->session->userdata['user']->UserID.' AND ';
        } else {
            $data['review'] = false;
        }
        $where = "$where collection_reviews.CollectionID = ".$collection_id;
        $data['all_reviews'] = $this->Collection_review_model->getJoinedDataWithOtherTable(false, 'UserID', 'users_text', $where);
        $data['collection'] = $data['collection'][0];
        $data['collection_images'] = get_images($collection_id, 'collection');
        $data['view'] = 'frontend/single-collection';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function giveRating()
    {
        $post_data = $this->input->post(); // CollectionID, UserID, Rating
        $alreadyRated = $this->Collection_rating_model->getWithMultipleFields(array('CollectionID' => $post_data['CollectionID'], 'UserID' => $post_data['UserID']));
        if ($alreadyRated)
        {
            $response['status'] = false;
            $response['message'] = "You have already rated this collection.";
            echo json_encode($response);
            exit();
        } else {
            $inserted_id = $this->Collection_rating_model->save($post_data);
            if ($inserted_id > 0)
            {
                $response['status'] = true;
                $response['message'] = "Thank you for rating this collection.";
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = "Something went wrong while rating this collection.";
                echo json_encode($response);
                exit();
            }
        }
    }

    public function saveReview()
    {
        $post_data = $this->input->post(); // Title, Comment, CollectionID
        $post_data['UserID'] = $this->session->userdata['user']->UserID;
        $post_data['CreatedAt'] = date('Y-m-d H:i:s');
        $alreadyReviewed = $this->Collection_review_model->getWithMultipleFields(array('CollectionID' => $post_data['CollectionID'], 'UserID' => $post_data['UserID']));
        if ($alreadyReviewed) {
            $response['message'] = "You have already reviewed this collection.";
            echo json_encode($response);
            exit();
        } else {
            $inserted_id = $this->Collection_review_model->save($post_data);
            if ($inserted_id > 0)
            {
                $response['reset'] = true;
                $response['reload'] = true;
                $response['message'] = "Thank you for giving your review on this collection.";
                echo json_encode($response);
                exit();
            } else {
                $response['message'] = "Something went wrong while giving review on this collection.";
                echo json_encode($response);
                exit();
            }
        }
    }

}