<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corporate extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Page_model');
        $this->load->model('Home_slider_image_model');
        $this->load->model('Product_model');
        $this->load->model('Contact_request_model');
    }

    public function index()
    {
        $data['menu'] = 'corporate';
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 8 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['corporate_products'] = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, 'products.IsCorporateProduct = 1 AND products.IsFeatured = 1');
        $data['view'] = 'frontend/corporate';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function products()
    {
        $this->data['menu'] = 'corporate';
        $this->data['corporate_products'] = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, 'products.IsCorporateProduct = 1');

        if (empty($this->data['corporate_products'])) {
            redirect(base_url('corporate'));
        }

        $this->data['view'] = 'frontend/corporate-products';
        $this->load->view('frontend/layouts/default', $this->data);
    }
}