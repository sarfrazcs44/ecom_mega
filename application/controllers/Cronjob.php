<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Riyadh');

class Cronjob extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->Model('Temp_order_model');
        $this->load->Model('Order_model');
    }

    public function index()
    {
        echo 'This is Ecommerce cronjob controller called';
    }


    public function sendAbandonedCartNotifications()
    {
        $abandoned_carts = $this->Temp_order_model->getAbandonedCart();
        if (count($abandoned_carts) > 0) {
            foreach ($abandoned_carts as $abandoned_cart) {
                if ($abandoned_cart->Email != '') {
                    $data['to'] = $abandoned_cart->Email;
                    $data['subject'] = 'Abandoned cart at ecommerce';
                    $message = "Dear " . $abandoned_cart->FullName . ",<br>You have ".$abandoned_cart->CartQuantityCount." items in your cart. Please login and place your order at<br>" . base_url();
                    $data['message'] = email_format($message);
                    sendEmail($data);
                }
            }
        }
        $response['status'] = true;
        $response['message'] = "Cronjob run successfully";
        echo json_encode($response);
        exit();
    }


    public function cancelledOrderNotCollectedFromStore(){
        $o_date = Date('Y-m-d', strtotime("-3 days"));
       
        $where = "orders.Status != 4 AND orders.Status != 5 AND orders.CollectFromStore = 1 AND DATE(orders.CreatedAt) < '".$o_date."'";
        $orders = $this->Order_model->getOrdersForCronJob($where, false, 0, 'EN', 'DESC');
        
        if(!empty($orders)){
            foreach($orders as $order){
                $this->Order_model->update(array('Status' => 5),array('OrderID' => $order->OrderID));
                $this->SendStatusChangedEmailToCustomer($order);
                $this->SendStatusChangedEmailToStoreAdmin($order);
            }
        }
    }

    private function SendStatusChangedEmailToCustomer($order_info)
    {
        if ($order_info->Email !== '') {
            $email_template = get_email_template(8);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->FullName, $message);
            $message = str_replace("{{order_no}}", $order_info->OrderNumber, $message);
           
            $data['to'] = $order_info->Email;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }

    private function SendStatusChangedEmailToStoreAdmin($order_info)
    {
        if ($order_info->StoreAdminEmail !== '') {
            $email_template = get_email_template(9);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->StoreAdminFullName, $message);
            $message = str_replace("{{order_no}}", $order_info->OrderNumber, $message);
           
            $data['to'] = $order_info->StoreAdminEmail;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }

}