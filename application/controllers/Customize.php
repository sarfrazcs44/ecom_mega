<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customize extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Page_model');
        $this->load->model('Home_slider_image_model');
        $this->load->model('Product_model');
        $this->load->model('Contact_request_model');
        $this->load->model('Box_model');
        $this->load->model('Customize_model');
    }

    public function index()
    {
        $data['menu'] = 'customize';
        //$data['products'] = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, "products.PriceType = 'kg'");
        $data['customizations'] = $this->Customize_model->getAllJoinedData(false, 'CustomizeID', $this->language,'customizes.IsActive = 1');
        $data['view'] = 'frontend/customize';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function boxes($type = '')
    {
        if ($type == 'choco_box' || $type == 'choco_message') {
            $data['menu'] = 'customize';
            $data['type'] = $type;
            $data['boxes'] = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, "boxes.IsActive = 1");;
            $data['view'] = 'frontend/customize-boxes';
            $this->load->view('frontend/layouts/default', $data);
        } else {
            redirect(base_url('customize'));
        }
    }

    public function choco_box($BoxID = '', $RibbonColor = '')
    {
        $BoxID = base64_decode($BoxID);
        if (!is_numeric($BoxID)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
        $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $BoxID . " AND system_languages.ShortCode = '" . $this->language . "'");
        if (!$box) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
        $data['box'] = $box[0];
        $all_products = $this->Product_model->getProducts('products.IsCustomizedProduct = 1', $this->language);
        $products = array();
        foreach ($all_products as $product) {
            $BoxIDs = explode(',', $product->BoxIDs);
            if (in_array($BoxID, $BoxIDs)) {
                $products[] = $product;
            }
        }

        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 11 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['RibbonColor'] = base64_decode($RibbonColor);
        $data['products'] = $products;
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-box';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function choco_message($BoxID = '', $RibbonColor = '')
    {
        $BoxID = base64_decode($BoxID);
        if (!is_numeric($BoxID)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }

        $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $BoxID . " AND system_languages.ShortCode = '" . $this->language . "'");
        if (!$box) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }

        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 10 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['RibbonColor'] = base64_decode($RibbonColor);
        $data['box'] = $box[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-message';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function choco_shape()
    {
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 12 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-shape';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function getChocoboxDetail()
    {
        $box_id = $this->input->post('box_id');
        $box_type = $this->input->post('box_type');
        $ribbon = $this->input->post('ribbon');
        $html = '<div class="row">
                        <h4>' . $box_type . ' Detail</h4>';
        if ($box_type == 'Chocobox') {
            $ProductIDs = $this->input->post('ProductIDs');
            $ProductIDs = explode(',', $ProductIDs);
            foreach ($ProductIDs as $ProductID) {
                $product = $this->Product_model->getProductDetail($ProductID, $this->language);
                $html .= '<div class="col-md-2 chocobox_item">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="' . base_url(get_images($product->ProductID, 'product', false)) . '" style="width: 61px;height: 59px;">
                                </div>
                                    <h4>' . $product->Title . '</h4>
                                    <h5><strong>' . number_format($product->Price, 2) . '</strong> ' . lang('sar') . '</h5>';
                if ($product->OutOfStock == 1) {
                    $html .= '<small style="font-weight: bold;color: red;">' . lang('out_of_stock') . '</small>';
                }
                $html .= '</div>
                        </div>';
            }
        } elseif ($box_type == 'Choco Message') {
            $ProductIDs = $this->input->post('ProductIDs');
            $ProductIDs = explode(',', $ProductIDs);
            foreach ($ProductIDs as $ProductID) {
                $html .= '<div class="col-md-2 chocomsg_item">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="' . front_assets() . 'images/' . strtolower($ProductID) . '.png' . '" style="width: 61px;height: 59px;">
                                </div>';
                $html .= '</div>
                        </div>';
            }
        }
        $html .= '</div>';
        $html .= '<div class="clearfix"></div><br><br>';
        $html .= '<div class="row"><h4>' . lang('box_packaging_detail') . '</h4>';
        if (isset($box_id) && $box_id > 0) {
            $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $box_id . " AND system_languages.ShortCode = '" . $this->language . "'");
            if ($box) {
                $html .= '<div class="col-md-12"><h5>' . lang('box_title') . ' <b>' . $box[0]->Title . '</b></h5></div>';
                $html .= '<div class="col-md-12"><h5>' . lang('box_price') . ' <b>' . $box[0]->BoxPrice . '</b></h5></div>';
                $html .= '<div class="col-md-12"><h5>' . lang('box_capacity') . ' <b>' . $box[0]->BoxSpace . ' ' . lang('pieces') . '</b></h5></div>';
                $html .= '<div class="col-md-12"><img src="' . base_url($box[0]->BoxImage) . '" alt="' . $box[0]->Title . '" style="width: 400px;"></div>';
                $html .= '<div class="col-md-12"><a href="' . base_url($box[0]->BoxImage) . '" target="_blank">' . lang('enlarge_image') . '</a></div>';
            }
        }
        $html .= '</div><br><br>';
        $html .= '<div class="row"><div class="col-md-12">Ribbon: </div><div class="col-md-12"><img src="'.front_assets('images/'.$ribbon.'.png').'"></div></div>';
        echo $html;
        exit();

    }

    public function getChocoboxes()
    {
        $html = '<h4>' . lang('box_packaging_detail') . '</h4>';
        $html .= '<p>' . lang('select_box_to_continue') . '</p><br>';
        $type = $this->input->post('type');
        $boxes = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, "boxes.IsActive = 1");
        foreach ($boxes as $box) {
            $html .= '<div class="row">';
            $html .= '<div class="col-md-4">
                                    <div class="row">';
            $html .= '<div class="col-md-12"><h5>' . lang('box_title') . ' <b>' . $box->Title . '</b></h5></div>';
            $html .= '<div class="col-md-12"><h5>' . lang('box_price') . ' <b>' . $box->BoxPrice . '</b></h5></div>';
            $html .= '<div class="col-md-12"><h5>' . lang('box_capacity') . ' <b>' . $box->BoxSpace . ' ' . lang('pieces') . '</b></h5></div>';
            $html .= '</div>
                        </div>';
            $html .= '<a title="' . lang('click_to_proceed_with_box') . '" href="' . base_url('customize') . '/' . $type . '/' . base64_encode($box->BoxID) . '"><div class="col-md-8"><img src="' . base_url($box->BoxImage) . '" alt="' . $box->Title . '" style="width: 400px;"></div></a>';
            $html .= '</div>';
            $html .= '<hr>';
        }
        echo $html;
        exit();
    }
}