<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotificationCronJob extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('User_model');
        $this->load->Model('User_text_model');
        $this->load->Model('Notification_model');
        $this->load->Model('Notification_cron_model');
        $this->load->Model('User_notification_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
    }

    public function index()
    {
        echo "This is Notification Cron Job controller";
    }

    public function sendNotificationUsingCronJob()
    {
        $notifications = $this->Notification_cron_model->getAll();
        if($notifications){
            $data = array();
            foreach ($notifications as $key => $value){
                $limit = 300;
                $start = $value->Page * $limit;
                $total_now  =  $limit + $start;
                $RoleID = $value->RoleID;
                $title = $value->Title;
                $message = $value->Message;
                $notification_Cron_Job_id = $value->NotificationCronJobID;
                $notification_type = $value->NotificationType;
                $where = 'users.RoleID = '.$value->RoleID.' And users.CompanyID = '.$value->CompanyID.'';
                $users = $this->Notification_cron_model->getUsersDataForSendingNotification($where,$limit,$start);
                //echo $this->db->last_query();exit;
                //print_rm($users);
                if ($users && count($users) > 0) {
                     $other_data = array();
                    foreach ($users as $user) {
                        $check_already = $this->User_notification_model->getWithMultipleFields(array('UserID' => $user['UserID'],'NotificationCronJobID' => $notification_Cron_Job_id));
                        if(!$check_already){
                            
                       
                            $notification_status = 'Done';
                            if($user['SystemLanguageID'] == 'EN'){

                                $title = $value->Title;
                                $message = $value->Message;
                            }else{

                                $title = $value->TitleAr;
                                $message = $value->Message;

                            }
                            if ($notification_type == 'SMS') {
                                    if ($user['Mobile'] != '') {
                                        $sms_text = $title . "\n" . $message;
                                        $sms_text = "Dear " . $user['FullName'] . "\n" . $message;
                                       // sendSms($user['Mobile'], $sms_text);
                                    }
                                } elseif ($notification_type == 'Email') {
                                    if ($user['Email'] != '') {
                                        $data['to'] = $user['Email'];
                                        $data['subject'] = $title;
                                        $message = "Dear " . $user['FullName'] . ",<br>" . $message;
                                        $data['message'] = email_format($message);
                                        //sendEmail($data);
                                    }
                                } elseif ($notification_type == 'Push'){
                                    if ($user['DeviceToken'] != '') {
                                        
                                        sendNotification($title,$message,$data,$user->UserID);
                                    }
                            } 



                            $other_data[] = [
                                'NotificationCronJobID' => $notification_Cron_Job_id,
                                'NotificationTitleEn' => $title,
                                'NotificationTextEn' => $message,
                                'UserID' => $user['UserID'],
                                'NotificationStatus' => $notification_status,
                                'CreatedAt' => date('Y-m-d H:i:s')
                            ];
                        }

                     }

                     if(!empty($other_data)){
                        $this->User_notification_model->insert_batch($other_data);
                     }

                    
                }else{

                    $this->Notification_cron_model->delete(array('NotificationCronJobID' => $value->NotificationCronJobID));

                }

        //end
                $update_array = array();
                $update_array['Page'] = $value->Page + 1;
                $this->Notification_cron_model->update($update_array,array('NotificationCronJobID' => $value->NotificationCronJobID));

                if($value->TotalUser <= $total_now){

                     $this->Notification_cron_model->delete(array('NotificationCronJobID' => $value->NotificationCronJobID));

                }
                
            }
        }

    }
}