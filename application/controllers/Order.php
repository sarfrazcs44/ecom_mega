<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        checkFrontendSession();
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->model('Order_item_model');
        $this->load->model('Temp_order_model');
        $this->load->model('User_address_model');
        $this->load->model('Order_extra_charges_model');
        $this->load->model('Coupon_model');
        $this->load->model('Product_model');
        $this->load->model('Store_model');
        $this->load->model('Product_availability_model');
        $this->data['language'] = $this->language;

    }

    public function placeOrder()
    {
        $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($data['cart_items'])) {
            $response['message'] = lang('no_items_in_basket');
            $response['redirect'] = true;
            $response['url'] = 'cart';
            echo json_encode($response);
            exit();
        }
        $order_data['UserID'] = $this->UserID;
        $show_error = false;
        $error_message = '';
        $model_html = '';
        $show_model = false;

        foreach($data['cart_items'] as $product){

            $product_data = $this->Product_model->getStoreAvailability('product_store_availability.StoreID = '.$_GET['StoreID'].' AND products.ProductID = '.$product->ProductID);
            //echo $this->db->last_query();
            
            if($product->PriceType == 'kg'){
                    $quantity = $product->Quantity * 1000;
                    $unit = ($this->language == 'AR' ? 'غرام' : 'Grams');
                }else{
                    $quantity = $product->Quantity;
                    $unit = 'pcs';
                }

            $product_image = get_images($product->ProductID, 'product',false);
            if($product_data && $product->Quantity <= $product_data['AvailableQuantity']){

                
                
                $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong> '.$product->Title.'<br> '.$quantity.' '.$unit.'</strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p>'.($product->Quantity * $product->TempItemPrice).' SR</p>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }elseif($product_data && $product_data['AvailableQuantity'] <  $product->Quantity && $product_data['AvailableQuantity'] > 0){
                $show_model = true;
                $this->Temp_order_model->update(array('Quantity' => $product_data['AvailableQuantity']),array('TempOrderID' => $product->TempOrderID));
                $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong>'.$product->Title.' <br> <span class="red">'.$quantity.' '.$unit.'</span> <span class="green">'.$product_data['AvailableQuantity'].' peace only</span></strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p><span class="red">'.($product->Quantity * $product->TempItemPrice).' SR</span></p>
                                            <p><span class="green">'.($product_data['AvailableQuantity'] * $product->TempItemPrice).' SR</span></p>
                                            <button href="javascript:void(0);" onclick="removeIt(\'cart/removeFromCart\', \'TempOrderID\', '.$product->TempOrderID.');" class="crs-btn btn-dark"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }elseif($product_data && $product_data['AvailableQuantity'] == 0){
                $show_model = true;
                $this->Temp_order_model->delete(array('TempOrderID' => $product->TempOrderID));
                 $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong>'.$product->Title.' <br> <span class="red">'.$quantity.' '.$unit.'</span> </strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p><span class="green">Sold out</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }elseif(!$product_data){
                $show_model = true;
                $this->Temp_order_model->delete(array('TempOrderID' => $product->TempOrderID));
                 $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong>'.$product->Title.' <br> <span class="red">'.$quantity.' '.$unit.'</span> </strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p><span class="green">Sold out</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>';

            }


            

        }

        if($show_model){
            $response['model_html'] = $model_html;
            $response['show_model'] = true;
            echo json_encode($response);
            exit();

        }
        //print_rm($data['cart_items']);
        if (isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1) {
            $order_data['CollectFromStore'] = 1;
            //$user_city_id = $this->session->userdata['admin']['CityID'];
        } else {
            $order_data['CollectFromStore'] = 0;
            $fetch_address_by['UserID'] = $this->UserID;
            $fetch_address_by['IsDefault'] = 1;
            $address = $this->User_address_model->getWithMultipleFields($fetch_address_by);
            $order_data['AddressID'] = $address->AddressID;
            $user_city_id = $address->CityID;

            // saving address to collect payment
            /*$fetch_payment_address_by['UserID'] = $this->UserID;
            $fetch_payment_address_by['UseForPaymentCollection'] = 1;
            $address_for_payment = $this->User_address_model->getWithMultipleFields($fetch_payment_address_by);
            if ($address_for_payment) {
                $order_data['AddressIDForPaymentCollection'] = $address_for_payment->AddressID;
            } else {
                $order_data['AddressIDForPaymentCollection'] = $address->AddressID;
            }*/

            $order_data['ShipmentMethodID'] = $this->session->userdata('ShipmentMethodIDForBooking');
        }
        //$order_data['BranchDeliveryDistrictID'] = $_GET['BranchDeliveryDistrictID'];
        $order_data['StoreID'] = $_GET['StoreID'];
         $order_data['CompanyID'] = $this->companyData->CompanyID;
        $order_data['CreatedAt'] = date('Y-m-d H:i:s');
        $order_data['OrderNumber'] = time() . $this->UserID;
        $order_data['PaymentMethod'] = $this->session->userdata('PaymentMethodForBooking');
        $insert_id = $this->Order_model->save($order_data);
        if ($insert_id > 0) {
            // $this->sendThankyouEmailToCustomer($insert_id);//email sending in botton
            $get_store_data = $this->Store_model->get($_GET['StoreID'],false,'StoreID');
            $order_item_data = array();
            $total_amount = 0;
            foreach ($data['cart_items'] as $product) {
                $order_item_data[] = [
                    'OrderID' => $insert_id,
                    'ProductID' => $product->ProductID,
                    'Quantity' => $product->Quantity,
                    'Amount' => $product->TempItemPrice,
                    'ItemType' => $product->ItemType,
                    'CustomizedOrderProductIDs' => $product->CustomizedOrderProductIDs,
                    'CustomizedShapeImage' => $product->CustomizedShapeImage,
                    'CustomizedBoxID' => $product->CustomizedBoxID,
                    'IsCorporateItem' => $product->IsCorporateItem,
                    'Ribbon' => $product->Ribbon
                ];
                $total_amount += $product->Quantity * $product->TempItemPrice;

                // increase product purchase count here
                $product_dt_for_purchase_count = $this->Product_model->get($product->ProductID, false, 'ProductID');
                $PurchaseCount = (int)$product_dt_for_purchase_count->PurchaseCount + (int)$product->Quantity;
                $this->Product_model->update(array('PurchaseCount' => $PurchaseCount), array('ProductID' => $product->ProductID));


                //update product quantity

                if($get_store_data){

                    $availability_product = $this->Product_availability_model->getWithMultipleFields(array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                    if($availability_product){
                        $quantity = $availability_product->Quantity - $product->Quantity;
                        $this->Product_availability_model->update(array('Quantity' => $quantity),array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));

                    }

                }


            }

            if ($this->session->userdata('order_coupon')) {
                $order_coupon = $this->session->userdata('order_coupon');
                $coupon_code = $order_coupon['CouponCode'];
                $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total_amount;
                $total_amount = $total_amount - $coupon_discount_availed;

                // reducing coupon usage count
                $coupon_detail = $this->Coupon_model->getWithMultipleFields(array('CouponID' => $order_coupon['CouponID']), true);
                $update['UsageCount'] = $coupon_detail['UsageCount'] - 1;
                $update_by['CouponID'] = $order_coupon['CouponID'];
                $this->Coupon_model->update($update, $update_by);
            } else {
                $coupon_code = '';
                $coupon_discount_percentage = 0;
                $coupon_discount_availed = 0;
            }

            $ShipmentMethodIDForBooking = $this->session->userdata('ShipmentMethodIDForBooking');
            $shipping_amount = 0;
            if($order_data['CollectFromStore'] != 1){
                $shipment_method = getSelectedShippingMethodDetail($ShipmentMethodIDForBooking, $this->language);
                if ($shipment_method) {
                    $shipping_id = $shipment_method->TaxShipmentChargesID;
                    $shipping_title = $shipment_method->Title;
                    $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                    if ($shipment_method->Type == 'Fixed') {
                        $shipping_amount = $shipment_method->Amount;
                    } elseif ($shipment_method->Type == 'Percentage') {
                        $shipping_amount = ($shipment_method->Amount / 100) * ($total_amount);
                    }
                    $order_extra_charges['OrderID'] = $insert_id;
                    $order_extra_charges['TaxShipmentChargesID'] = $shipping_id;
                    $order_extra_charges['Title'] = $shipping_title;
                    $order_extra_charges['Factor'] = $shipping_factor;
                    $order_extra_charges['Amount'] = $shipping_amount;
                    $this->Order_extra_charges_model->save($order_extra_charges);
                }
            }
            

            $total_tax = 0;
            $taxes = getTaxShipmentCharges('Tax');
            foreach ($taxes as $tax) {
                $tax_id = $tax->TaxShipmentChargesID;
                $tax_title = $tax->Title;
                $tax_factor = $tax->Type == 'Fixed' ? number_format($tax->Amount, 2) . ' SAR' : $tax->Amount . '%';
                if ($tax->Type == 'Fixed') {
                    $tax_amount = $tax->Amount;
                } elseif ($tax->Type == 'Percentage') {
                    $tax_amount = ($tax->Amount / 100) * ($total_amount + $shipping_amount);
                }
                $total_tax += $tax_amount;
                $order_extra_charges['OrderID'] = $insert_id;
                $order_extra_charges['TaxShipmentChargesID'] = $tax_id;
                $order_extra_charges['Title'] = $tax_title;
                $order_extra_charges['Factor'] = $tax_factor;
                $order_extra_charges['Amount'] = $tax_amount;
                $this->Order_extra_charges_model->save($order_extra_charges);
            }
            $total_amount = $total_amount + $shipping_amount + $total_tax;

            $order_data['CouponCodeUsed'] = $coupon_code;
            $order_data['CouponCodeDiscountPercentage'] = $coupon_discount_percentage;
            $order_data['DiscountAvailed'] = $coupon_discount_availed;
            $order_data['TotalShippingCharges'] = $shipping_amount;
            $order_data['TotalTaxAmount'] = $total_tax;
            $order_data['TotalAmount'] = $total_amount;
            $order_data['OrderNumber'] = str_pad($insert_id, 5, '0', STR_PAD_LEFT);
            $this->Order_item_model->insert_batch($order_item_data);
            $this->Order_model->update($order_data, array('OrderID' => $insert_id));
            $deleted_by['UserID'] = $this->UserID;
            $this->Temp_order_model->delete($deleted_by);
           // $this->sendOrderConfirmationToCustomer($insert_id);
            //$order_data_pusher = $this->Order_model->get($insert_id, true, 'OrderID');
           // pusher($order_data_pusher, "Ecommerce_Order_Channel", "Ecommerce_Order_Event"); //Need to create Ecommerce on pusher channel
           // $response['message'] = lang('payment_page_redirect');
            $response['message'] = lang('thank_you_for_order');
            $response['redirect'] = true;
            $response['show_model'] = false;
            
           // $response['url'] = 'checkout/pay_tab/'.base64_encode($insert_id);
            $response['url'] = 'checkout/thank_you/'.base64_encode($insert_id);
            echo json_encode($response);
            exit();
           
        } else {
            $response['message'] = lang('something_went_wrong');
            $response['redirect'] = false;
            $response['show_model'] = false;
            echo json_encode($response);
            exit();
            
        }
    }


    public function updatePaymentStatus(){
        //print_rm($_REQUEST);
        if(isset($_REQUEST['transaction_id']) && isset($_REQUEST['order_id'])){

            $this->Order_model->update(array('TransactionID' => $_REQUEST['transaction_id'],'Hide' => 0),array('OrderID' => $_REQUEST['order_id']));

            $this->sendOrderConfirmationToCustomer($_REQUEST['order_id']);
            $order_data_pusher = $this->Order_model->get($_REQUEST['order_id'], true, 'OrderID');
            pusher($order_data_pusher, "Ecommerce_Order_Channel", "Ecommerce_Order_Event"); //Need to create Ecommerce on pusher channel
           
            $this->session->set_flashdata('message', lang('thank_you_for_order'));
            $this->session->set_flashdata('message_type','info');
            redirect(base_url('checkout/thank_you/'.base64_encode($_REQUEST['order_id'])));

        }
    }

    public function CancelPaymentPaymentOfOrder(){
       
        if(isset($_REQUEST['order_id'])){

            $this->Order_item_model->delete(array('OrderID' => $_REQUEST['order_id']));
            $this->Order_model->delete(array('OrderID' => $_REQUEST['order_id']));

            

        }

        redirect(base_url());
    }

    public function sendOrderConfirmationToCustomer($OrderID)
    {
        $user = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $this->UserID);
        $order = $this->Order_model->get($OrderID, false, 'OrderID');
        if ($user) {
            // sending email
            if ($user[0]->Email != '') {
                $data['to'] = $user[0]->Email;
                $data['subject'] = 'Order received at Ecommerce';
                $data['message'] = get_order_invoice($OrderID, 'print');
               // $data['message'] = get_order_invoice_new($OrderID);
                sendEmail($data);
            }

            // sending sms
            if ($user[0]->Mobile != '') {
                $msg = "Dear " . $user[0]->FullName . ", Your order is placed successfully with order # " . $order->OrderNumber . "\n";
                $msg .= "Invoice: " . base_url() . "page/invoice/" . base64_encode($OrderID);
                //sendSms($user[0]->Mobile, $msg);
            }
        }
    }

    public function cancelOrder()
    {
        $OrderID = $this->input->post('OrderID');
        $this->Order_model->update(array('Status' => 5), array('OrderID' => $OrderID));
        $this->sendOrderCancellationToAdmin($OrderID);
        $response['message'] = lang('order_cancelled');
        $response['redirect'] = true;
        $response['url'] = 'account/profile?p=orders';
        echo json_encode($response);
        exit();
    }

    private function sendOrderCancellationToAdmin($OrderID)
    {
        $admin_users = $this->User_model->getJoinedData(false, 'UserID', "users.RoleID = 1");
        $order = $this->Order_model->get($OrderID, false, 'OrderID');
        if ($admin_users) {
            foreach ($admin_users as $user) {
                // sending email
                if ($user->Email != '') {
                    $data['to'] = $user->Email;
                    $data['subject'] = 'Order cancelled at Ecommerce';
                    $data['message'] = email_format("Dear " . $user->FullName . ", an order is cancelled by user with order # " . $order->OrderNumber);
                    sendEmail($data);
                }

                // sending sms
                if ($user->Mobile != '') {
                    $msg = "Dear " . $user->FullName . ", an order is cancelled by user with order # " . $order->OrderNumber;
                    sendSms($user[0]->Mobile, $msg);
                }
            }
        }
    }

}