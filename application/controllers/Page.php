<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Page_model');
        $this->load->model('Brand_model');
        $this->load->model('Home_slider_image_model');
        $this->load->model('Product_model');
        $this->load->model('Service_model');
        $this->load->model('Team_model');
        $this->load->model('Collection_model');
        $this->load->model('Contact_request_model');
        $this->load->model('Offer_user_notification_model');
        $this->load->model('Customize_model');
        $this->load->model('Order_model');
        $this->load->model('Faq_model');
        $this->load->model('User_model');
        $this->data['language'] = $this->language;
    }

    public function index()
    {
        //redirect(base_url('cms/account/login'));
        $this->home();
    }

    public function home()
    {
        $this->data['marquee'] = true;
        $this->data['menu'] = 'home';
        $this->data['homepage'] = $this->Page_model->getAllJoinedData(false,'PageID',$this->language, 'pages.CompanyID = '.$this->companyData->CompanyID.' AND pages.Type = "Home"');
        //print_rm($this->data['homepage']);
       // $data['slider_images'] = $this->Home_slider_image_model->getAllJoinedData(false, 'HomeSliderImageID', $this->language);
       // $data['featured_products'] = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, 'products.IsFeatured = 1 AND products.IsCustomizedProduct = 0');
        //$data['collections_featured'] = $this->Collection_model->getAllJoinedData(false, 'CollectionID', $this->language, 'collections.IsActive = 1');
        // $data['collections'] = $this->Collection_model->getAllJoinedData(false, 'CollectionID', $this->language);
       // $data['customizations'] = $this->Customize_model->getAllJoinedData(false, 'CustomizeID', $this->language,'customizes.IsActive = 1');
        $this->data['brands'] = $this->Brand_model->getBrandData(false, $this->language,'brands.IsActive = 1 AND brands.CompanyID = '.$this->session->userdata['company']->CompanyID);
        
        $this->data['featured_categories'] = $this->Category_model->getCompanyData(false,"categories.CompanyID = ".$this->session->userdata['company']->CompanyID . " AND categories.ParentID = 0 AND categories.IsFeatured = 1 AND system_languages.ShortCode = '" . $this->language . "' AND categories.IsActive = 1 AND categories.Hide = 0",'ASC','categories_text.Title');

        //print_rm($this->data['featured_companies']);
        $where = 'products.IsActive = 1 AND products.Type != "AddOn" AND products.IsFeatured = 1 AND products.CompanyID = '.$this->session->userdata['company']->CompanyID;
        
        $limit = 10;
        
        $this->data['products'] = $this->Product_model->getProducts($where, $this->language,$limit,0,'products_text.Title','ASC',[0,1,2,3,4,5]);
        $this->data['newProducts'] = $this->Product_model->getProducts($where, $this->language,3,0,'products_text.Title','DESC',[0,1,2,3,4,5]);
        $this->data['bestSellingProducts'] = $this->Product_model->getBestSellingProducts('products.IsActive = 1 AND products.Type != "AddOn" AND products.CompanyID = '.$this->session->userdata['company']->CompanyID, $this->language,$limit,0,'products_text.Title','ASC',[0,1,2,3,4,5]);
        //print_rm($this->data['bestSellingProducts']);
        //echo $this->db->last_query();exit;
        //print_rm($this->session->userdata('company'));
        $page_value = '1';
        if($this->session->userdata('company')){
            
            $page_value = $this->session->userdata['company']->Homepage;
            
        }
        $this->data['view'] = 'frontend/index-'.$page_value;
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function about_us()
    {
        $this->data['menu'] = 'about-us';
        $this->data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 1 AND system_languages.ShortCode = '" . $this->language . "'")[0];

        $this->data['page_images'] = get_images($this->data['result']->PageID, 'page');

        $this->data['our_teams'] = $this->Team_model->getAllJoinedData(false,'TeamID',$this->language, 'teams.CompanyID = '.$this->companyData->CompanyID);

        $this->data['about_page'] = $this->Page_model->getAllJoinedData(false,'PageID',$this->language, 'pages.CompanyID = '.$this->companyData->CompanyID.' AND pages.Type = "About us"');

        $this->data['about_us'] = $this->Page_model->getAllJoinedData(false, 'PageID', $this->language,'pages.CompanyID ='.$this->companyData->CompanyID.' AND pages.Type = "About us"');

        $this->data['services'] = $this->Service_model->getAllJoinedData(false, 'ServiceID', $this->language,'services.CompanyID ='.$this->companyData->CompanyID);
        //print_rm($this->data['about_us']);

        $this->data['view'] = 'frontend/page-about';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function return_policy()
    {
        $data['menu'] = 'return_policy';
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 13 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['view'] = 'frontend/return_policy';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function privacy_policy()
    {
        $data['menu'] = 'privacy_policy';
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 15 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['view'] = 'frontend/privacy_policy';
        $this->load->view('frontend/layouts/default', $data);
    }

     public function site_map()
    {
        $data['menu'] = 'site-map';
        
        $data['view'] = 'frontend/site_map';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function products()
    {
        $data['menu'] = 'product';
        $product_value = '';
        /*if($this->session->userdata('company')){
            
            $product_value = '-'.$this->session->userdata['company']->Product;
            
        }
        */
        $data['view'] = 'frontend/products'.$product_value;
        $this->load->view('frontend/layouts/default', $data);
    }

     public function faq()
    {
        $data['menu'] = 'faq';
        
        $data['faqs'] = $this->Faq_model->getAllJoinedData(false,'FaqID',$this->language,'faqs.IsActive = 1');
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 14 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        
        $data['view'] = 'frontend/faq';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function contact_us()
    {
        $this->data['menu'] = 'contact-us';
        //$this->data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.CompanyID =  AND system_languages.ShortCode = '" . $this->language . "'")[0];

        $this->data['result'] = $this->Page_model->getAllJoinedData(false,'PageID',$this->language, 'pages.CompanyID = '.$this->companyData->CompanyID.' AND pages.Type = "Contact us"');
        //print_rm($this->data['result']);
        $this->data['view'] = 'frontend/page-contact';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function saveContactForm()
    {
        $post_data = $this->input->post();
        /*$siteKey = $post_data["g-recaptcha-response"];
        unset($post_data["g-recaptcha-response"]);
        $res = captchaVerify($siteKey);
        if ($res->success != true) {
            $response['message'] = lang('must_use_captcha');
            $response['reset'] = false;
            echo json_encode($response);
            exit;
        } else {
            if (isset($_FILES['CV']["name"]) && $_FILES['CV']["name"] != '') {

                // checking file extension
                $allowed = array('pdf', 'doc', 'docx', 'PDF', 'DOC', 'DOCX');
                $filename = $_FILES['CV']['name'];
                $filename = strtolower($filename);
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    $response['reset'] = false;
                    $response['message'] = lang("only_pdf_doc_file_format");
                    echo json_encode($response);
                    exit();
                }

                $post_data['CV'] = uploadImage("CV", "uploads/");
            }*/
            $post_data['CompanyID'] = $this->companyData->CompanyID;
            $post_data['CreatedAt'] = time();
            $insert_id = $this->Contact_request_model->save($post_data);
            if ($insert_id > 0) {
                $response['reset'] = true;
                $response['message'] = lang('feed_back_submitted_submitted_successfully');
                echo json_encode($response);
                exit();
            } else {
                $response['reset'] = false;
                $response['message'] = lang('some_thing_went_wrong');//"Something went wrong. Please try again.";
                echo json_encode($response);
                exit();
            }
        
    }

    public function newsletter_subscribe()
    {
        $Email = $this->input->post('Email'); // Email
        $subscribed = newsletter_subscribe($Email);
        if ($subscribed['http_code'] == 200) {
            $response['reset'] = true;
            $response['message'] = lang('thank_you_subscribe_our_news_letter');
            echo json_encode($response);
            exit();
        } else {
            $response['reset'] = false;
            $response['message'] = lang("you_have_enter_invalid_email");
            echo json_encode($response);
            exit();
        }
    }

    public function search()
    {

        $search_value = $this->input->get('key');
        $data['products'] = $this->Product_model->searchProducts($search_value, $this->language);
        $data['search'] = true;
        $data['view'] = 'frontend/products';
        $this->load->view('frontend/layouts/default', $data);


    }

    public function markOfferAsRead()
    {
        $update_by['OfferID'] = $_GET['OfferID'];
        $update_by['UserID'] = $this->session->userdata['user']->UserID;
        $update['IsShow'] = 1;
        $this->Offer_user_notification_model->update($update, $update_by);
        $success['status'] = true;
        $success['message'] = 'Offer marked as read successfully';
        echo json_encode($success);
        exit;
    }

    /*public function invoice($OrderID)
    {
        $OrderID = base64_decode($OrderID);
        $order_html = get_order_invoice($OrderID, 'print');
        echo $order_html;
        exit();
    }*/


    public function invoice($OrderID)
    {
        checkFrontendSession();
        $OrderID = base64_decode($OrderID);
        
        $order_details = $this->Order_model->getOrders("orders.OrderID = $OrderID");
        $data['order'] = $order_details[0];
        //print_rm($data['order']);
       
        $order_html = $this->load->view('frontend/invoice', $data, true);
        echo $order_html;
        exit();
    }

    public function download($OrderID)
    {
        $OrderID = base64_decode($OrderID);
        $order_html = get_order_invoice($OrderID);
        generate_pdf($order_html, $OrderID);
    }

    public function verifyEmail()
    {
        if (isset($_GET['verification_token']) && $_GET['verification_token'] != '') {
            $UserID = base64_decode($_GET['verification_token']);
            $user_info = $this->User_model->getJoinedData(true, 'UserID', 'users.UserID =' . $UserID);
            if ($user_info) {
                $this->User_model->update(array('IsEmailVerified' => 1), array('UserID' => $UserID));
                echo "Your email is verified successfully. You can get back to the app to continue using it.";
                exit();
            } else {
                echo "Sorry! We couldn't verify you as your verification process failed.";
                exit();
            }
        } else {
            echo "Sorry! We couldn't verify you as your verification process failed.";
            exit();
        }
    }

}