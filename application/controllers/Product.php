<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Page_model');
        $this->load->model('Category_model');
        $this->load->model('Collection_model');
        $this->load->model('Home_slider_image_model');
        $this->load->model('Variant_model');
        $this->load->model('Product_model');
        $this->load->model('Contact_request_model');
        $this->load->model('Product_rating_model');
        $this->load->model('Product_review_model');
        $this->load->model('Search_tag_model');
        $this->load->model('Offer_model');
        $this->load->model('Tag_model');
        $this->load->model('What_inside_model');
        $this->load->model('Nutrition_product_model');
        $this->data['language'] = $this->language;
    }



    public function category($title){
        $category = explode('-',$title);
        $category_id = str_replace('c','',end($category));
        $this->data['CategoryID'] = $category_id;
        $this->data['categories'] = subCategories($category_id,$this->language);
        $this->data['view'] = 'frontend/category';
        $this->load->view('frontend/layouts/default', $this->data);
    }
    public function listing(){
        $this->data['view'] = 'frontend/list-product';
        $this->load->view('frontend/layouts/default', $this->data);
    }
    public function index($param = '',$sub_category=0)
    {  

        if($param != ''){
            $category = explode('-',$param);

            $this->data['category_id'] = end($category);
        }else{
             $this->data['category_id'] = -1;
        }

        $this->data['menu'] = 'product';
        $this->data['sub_category'] = $sub_category;

        $where = 'products.IsActive = 1 AND products.IsFeatured = 1 AND products.CompanyID = '.$this->session->userdata['company']->CompanyID;
        
        
        $this->data['newProducts'] = $this->Product_model->getProducts($where, $this->language,3,0,'products_text.Title','DESC',[0,1,2,3,4,5]);


        $this->data['variants'] = $this->Variant_model->getAllJoinedData(false, 'VariantID', $this->language, 'variants.ParentID = 0 AND variants.CompanyID =' . $this->session->userdata['company']->CompanyID);

       
        $product_value = '1';
        /*if($this->session->userdata('company')){
            
            $product_value = $this->session->userdata['company']->Product;
            
        }
        */


        $this->data['view'] = 'frontend/products-'.$product_value;
        $this->load->view('frontend/layouts/default', $this->data);
    }
    public function product_listing()
    {   
        
        $categoryid = $this->input->get('CategoryID');
        $sub_category = $this->input->get('SubCategory');
        //echo $categoryid.'helllo';exit;

        $where = 'products.IsActive = 1 AND products.CompanyID = '. $this->session->userdata['company']->CompanyID;

        if (isset($_GET['limit']) && $_GET['limit'] != '')
        {
            $limit = $_GET['limit'];

        } else {
            $limit = 50;
        }
        
       
        $page = '';
        
        if ($categoryid > 0 && $sub_category == 0 )
        {
             $where .= ' AND products.CategoryID = '. $categoryid ;

        }else if($categoryid > 0 && $sub_category == 1){
             $where .= ' AND products.SubCategoryID = '. $categoryid ;

        }

         if (isset($_GET['page']) && $_GET['page'] != '')
        {
             $page = $_GET['page'];

        } else {
            $page = 1;
        }
        $start = ($page - 1) * $limit;
        $sort_by = 'products.CreatedAt';
        $sort_as = 'DESC';

       
        $attribute ='';

        
        
        
        if($_GET['attribute'] && $_GET['attribute'] != ''){
            $attribute = rtrim($_GET['attribute'],',');
            $where .= ' AND (  ';
            $attributes = explode(',',$attribute);
            foreach($attributes as $attribute_id){
                $where .= ' products.AttributeForSearchProduct like "%'.$attribute_id.'%" OR';
            }
            $where = rtrim($where,'OR');
            $where .= ' )';
        }
        
        
        
        if($_GET['sorting'] && $_GET['sorting'] != ''){

            if($_GET['sorting'] == 'IsFeatured'){
                $sort_by = 'products.IsFeatured';
                $sort_as = 'DESC';
            }
            if ($_GET['sorting'] == 'Price ASC') {
                $sort_by = 'products.Price';
                $sort_as = 'ASC';
            }
            if ($_GET['sorting'] == 'Price DESC') {
                $sort_by = 'products.Price';
                $sort_as = 'DESC';
            }
            if ($_GET['sorting'] == 'Rating') {
                $sort_by = 'product_ratings.Rating';
                $sort_as = 'DESC';
            }

        }

        
        $this->data['products'] = $this->Product_model->getProducts($where, $this->language,$limit,$start,$sort_by,$sort_as,[0,1,2,3,4,5]);
        //print_rm($this->data['products']);
        //echo $this->db->last_query();
        $this->data['total_count'] = count($this->Product_model->getProducts($where, $this->language,false,false,'products_text.Title','ASC',[0,1,2,3,4,5]));
        //print_rm($this->data['total_count']);
        $this->data['page'] = $page;

        
        $html = $this->load->view('frontend/product/product_html', $this->data, true);
        $paggination = $this->load->view('frontend/product_pagination', $this->data, true);
        $response = array('html' => $html,'total_product' =>  $this->data['total_count'],'paggination' => $paggination);
        echo json_encode($response);

    }
    public function index_bk($collection = '')
    {
        $this->data['menu'] = 'product';
        $sort_by = 'products_text.Title';
        $sort_as = 'ASC';
        if (isset($_GET['sort']) && $_GET['sort'] != '')
        {
            if ($_GET['sort'] == 'price_highest_to_lowest')
            {
                $sort_by = 'products.Price';
                $sort_as = 'DESC';
            }

            if ($_GET['sort'] == 'price_lowest_to_higher')
            {
                $sort_by = 'products.Price';
                $sort_as = 'ASC';
            }

            if ($_GET['sort'] == 'product_older_to_newer')
            {
                $sort_by = 'products.ProductID';
                $sort_as = 'ASC';
            }

            if ($_GET['sort'] == 'product_newer_to_older ')
            {
                $sort_by = 'products.ProductID';
                $sort_as = 'DESC';
            }

            if ($_GET['sort'] == 'purchased_highest_to_lowest')
            {
                $sort_by = 'products.PurchaseCount';
                $sort_as = 'DESC';
            }

            if ($_GET['sort'] == 'purchased_lowest_to_higher')
            {
                $sort_by = 'products.PurchaseCount';
                $sort_as = 'ASC';
            }
        }
        if (isset($_GET['q']) && $_GET['q'] != '') {
            $sub_category_id  = explode('-s',$_GET['q']);
            $sub_category_id   = end($sub_category_id);
            $this->data['sub_category_data'] = $this->Category_model->get($sub_category_id,true,'CategoryID');
            $this->data['category_data'] = $this->Category_model->get($this->data['sub_category_data']['ParentID'],true,'CategoryID');
            $this->data['url'] = $_GET['q'];
          
            $this->data['products'] = $this->getFilteredProducts($_GET['q'], $sort_by, $sort_as);
            $this->data['countproducts'] = count($this->data['products']);
        } else {
            // $limit = 12;
            $limit = false;
            redirect(base_url());//because now we are dealing with categories

            if ($collection != '') {

                $this->data['collection_data'] = $this->Collection_model->get($collection, false, 'CollectionID');
                if (!$this->data['collection_data']) {
                    redirect(base_url('product'));
                }
                $this->data['products'] = array();//$this->Product_model->getProductsOfCollection($this->data['collection_data']->ProductID,$this->language,$limit,0);
                // $this->data['countproducts']  = $this->Product_model->getProductsOfCollection($this->data['collection_data']->ProductID,$this->language,false,0,true);


                $this->data['countproducts'] = 0;//$this->data['countproducts'][0]->Total;
                $this->data['CollectionID'] = $this->data['collection_data']->CollectionID;
            } else {
                $this->data['products'] = $this->Product_model->getProducts('products.IsCustomizedProduct = 0 AND products.IsActive = 1', $this->language, $limit, 0, $sort_by, $sort_as);

                $this->data['countproducts'] = $this->Product_model->getCountProducts('products.IsCustomizedProduct = 0 AND products.IsActive = 1', $this->language, false, 0);
                $this->data['countproducts'] = $this->data['countproducts']->Total;

            }
        }


        //$this->data['collections'] = $this->Collection_model->getAllJoinedData(false, 'CollectionID', $this->language);
        $this->data['categories'] = $this->Category_model->getAllJoinedData(false, 'CategoryID', $this->language, 'categories.ParentID = 0 AND categories.IsActive = 1');
        $this->data['offers'] = $this->Offer_model->getAllJoinedData(false, 'OfferID', $this->language, 'offers.IsActive = 1 AND DATE(ValidTo) > "' . Date('Y-m-d') . '" AND IsForAll = 1');
        $this->data['offers_for_you'] = array();
        if ($this->session->userdata('user')) {
            //print_rm($this->session->userdata('user'));
            $this->data['offers_for_you'] = $this->Offer_model->getOfferForUser($this->session->userdata['user']->UserID);
        }
        //print_rm($this->data['offers_for_you']);
        $where_tags = '';
        $this->data['tags'] = false;
        if($this->data['products']){
            $get_tag_ids = array_column($this->data['products'], 'TagIDs');
            foreach ($get_tag_ids as $key => $value) {
               if($value == ''){
                 unset($get_tag_ids[$key]);
               }
            }
            
            if(!empty($get_tag_ids)){
                $where_tags = ' AND tags.TagID IN ('.implode(',',$get_tag_ids).')';
            }
            
            $this->data['tags'] = $this->Tag_model->getAllJoinedData(false, 'TagID', $this->language, 'tags.IsActive = 1'.$where_tags);
        }

       // $this->data['tags'] = $this->Tag_model->getAllJoinedData(false, 'TagID', $this->language, 'tags.IsActive = 1'.$where_tags);

        $this->data['view'] = 'frontend/products';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    private function getFilteredProducts($q, $sort_by, $sort_as)
    {
        $filtered_products = array();
        $url_sub_categories = explode(' ', $q);
        if (count($url_sub_categories) > 0) {
            $category_ids = array();
            $collection_ids = array();
            $products_by_categories = array();
            $products_by_collections = array();
            foreach ($url_sub_categories as $url_sub_category) {
                $sub_str = substr($url_sub_category, strrpos($url_sub_category, '-') + 1);
                if (strpos($sub_str, 's') !== false) {
                    $category_ids[] = getNumberFromString($sub_str);
                } elseif (strpos($sub_str, 'c') !== false) {
                    $collection_ids[] = getNumberFromString($sub_str);
                }
            }
            if (count($category_ids) > 0) {

                $products_by_categories = $this->Product_model->getProductsWhereIn($category_ids, $this->language, false, 0, $sort_by, $sort_as);
            }
            if (count($collection_ids) > 0) {
                $products_by_collections = $this->Collection_model->getCollectionProducts($collection_ids);
                if ($products_by_collections && count($products_by_collections) > 0) {
                    $collection_products_arr = array();
                    foreach ($products_by_collections as $product) {
                        $product_ids = explode(',', $product->ProductID);
                        $collection_products = $this->Product_model->getProductsOfCollection($product_ids, $this->language, false, 0, false, $sort_by, $sort_as);
                        foreach ($collection_products as $collection_product) {
                            $collection_products_arr[] = $collection_product;
                        }
                    }

                }
                $products_by_collections = $collection_products_arr;
            }
            $filtered_products = array_merge($products_by_categories, $products_by_collections);
            $filtered_products = array_unique($filtered_products, SORT_REGULAR);
        }
        return $filtered_products;
    }

    public function getAllProducts($page, $limit, $start, $ul_cls)
    {
        $products = $this->Product_model->getProducts('products.IsCustomizedProduct = 0 AND products.IsActive = 1', $this->language, $limit, $start);
        $countproducts = $this->Product_model->getCountProducts('products.IsCustomizedProduct = 0 AND products.IsActive = 1', $this->language, false, $start);
        $total_product_db = $countproducts->Total;
        $total_product = 0;
        $html = '';
        if (!empty($products)) {
            $html .= product_html($products, $ul_cls);
            $total_product = COUNT($products) * $page;
        }
        $response['html'] = $html;
        $response['page'] = $page;
        $response['count_product'] = $total_product_db;
        $response['total_now'] = $total_product;
        if ($total_product <= $total_product_db && COUNT($products) != 0) {
            $response['page'] = $page + 1;
        }
        echo json_encode($response);
        exit();
    }

    /*public function getMoreProducts()
    {

        $collection = $this->input->post('CollectionID');
        $offer = $this->input->post('OfferID');
        $collection_categories = $this->input->post('CollectionSubCategories');
        $offer_categories = $this->input->post('OfferSubCategories');
        $sub_categories = $this->input->post('SubCategories');


        $page = $this->input->post('Page');
        $limit = 12;
        $start = $limit * $page;
        $products = array();
        $products1 = array();
        $products2 = array();
        $countproducts_first = 0;
        $total_product_db = 0;
        //print_rm($sub_categories);
        if (empty($offer) && empty($collection) && empty($collection_categories) && empty($sub_categories) && empty($offer_categories)) {
            $this->getAllProducts($page, $limit, $start);
        }


        $where = '1 = 1';
        $where1 = '1 = 1';
        $product_ids = array();


        if (!empty($offer)) {

            $offer_products = $this->Offer_model->getOfferProducts($offer);


            if ($offer_products) {

                foreach ($offer_products as $product) {
                    $product_id_offer = explode(',', $product->ProductID);

                    foreach ($product_id_offer as $key => $value) {
                        if (!in_array($value, $product_ids)) {
                            $product_ids[] = $value;
                        }

                    }
                }

            }


            $a = implode(',', $product_ids);
            $where1 .= " AND products.ProductID IN ($a)";

            $b = implode(',', $offer_categories);
            $where1 .= " AND products.SubCategoryID IN ($b)";

            $products = $this->Product_model->getProducts($where1, $this->language, $limit, $start);
            $countproducts = $this->Product_model->getCountProducts($where1, $this->language, false, $start);
            $total_product_db = $countproducts->Total;

        }

        $product_ids = array();

        if (!empty($collection)) {
            $collection_products = $this->Collection_model->getCollectionProducts($collection);

            if ($collection_products) {

                foreach ($collection_products as $product) {
                    $product_id = explode(',', $product->ProductID);

                    foreach ($product_id as $key => $value) {
                        if (!in_array($value, $product_ids)) {
                            $product_ids[] = $value;
                        }

                    }
                }

            }


            $a = implode(',', $product_ids);
            $where .= " AND products.ProductID IN ($a)";

            $b = implode(',', $collection_categories);
            $where .= " AND products.SubCategoryID IN ($b)";

            $products2 = $this->Product_model->getProducts($where, $this->language, $limit, $start);
            $countproducts2 = $this->Product_model->getCountProducts($where, $this->language, false, $start);
            $total_product_db = $countproducts2->Total;
            if (!empty($products2)) {
                $products = array_merge($products, $products2);
                //$countproducts = $total_product_db = $countproducts_first + $total_product_db;
            }

        }


        $html = '';


        if (!empty($sub_categories)) {
            $where2 = '1 = 1';
            $c = implode(',', $sub_categories);
            $where2 .= " AND products.SubCategoryID IN ($c)";
            if (!empty($collection)) {
                $a = implode(',', $product_ids);
                $where2 .= " AND products.ProductID NOT IN ($a)";
            }

            $products3 = $this->Product_model->getProducts($where2, $this->language, $limit, $start);
            $countproducts3 = $this->Product_model->getCountProducts($where2, $this->language, false, $start);
            $total_product_db = $total_product_db + $countproducts3->Total;

            if (!empty($products3)) {
                $products = array_merge($products, $products3);
                //$countproducts = $total_product_db = $countproducts_first + $total_product_db;
            }

        }

        $total_product = 0;

        if (!empty($products)) {
            $type_of_item = "'Product'";

            $html .= '<div class="row">';

            foreach ($products as $key => $p) {


                $html .= '<div class="col-md-3 single_product">';
                $html .= '<div class="inbox">';
                $html .= '<div class="imgbox">';
                $html .= '<img src="' . base_url(get_images($p->ProductID, 'product', false)) . '">';
                $html .= '</div>';
                $html .= '<a href="' . base_url() . 'product/detail/' . productTitle($p->ProductID) . '">';
                $html .= '<h4>' . $p->Title . '</h4>';

                $html .= '<h5><strong data-price="' . number_format($p->Price, 2) . '" data-pid="'.$p->ProductID.'">' . number_format($p->Price, 2) . '</strong> SAR</h5>';
                if ($p->OutOfStock == 1) {
                    $html .= '<small style="font-weight: bold;color: red;">'.lang('out_of_stock').'</small>';
                }
                $html .= '</a>';
                $html .= '<a title="'.lang('click_to_add_to_wishlist').'" href="javascript:void(0);"
                                   onclick="addToWishlist(' . $p->ProductID . ', ' . $type_of_item . ');"><i
                                            class="fa fa-heart ' . isLiked($p->ProductID, 'product') . '" id="item' . $p->ProductID . '" aria-hidden="true"></i></a>';
                $html .= '<a href="javascript:void(0);" title="'.lang('click_to_add_to_cart').'"
                                   onclick="addWishlistToCart(' . $p->ProductID . ', ' . $type_of_item . ', '.$p->Price.');">
                                    <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                </a>';
                $html .= '</div></div>';
            }
            $html .= '</div>';

            $total_product = COUNT($products) * $page;

        }


        $response['html'] = $html;
        $response['page'] = $page;


        $response['count_product'] = $total_product_db;
        $response['total_now'] = $total_product;

        if ($total_product <= $total_product_db && COUNT($products) != 0) {

            $response['page'] = $page + 1;
        }
        echo json_encode($response);
        exit();


    }*/

    public function getMoreProducts()
    {

        $Featured = $this->input->post('Featured');
        $SubCategoryID = $this->input->post('SubCategoryID');
        $TagID = $this->input->post('TagID');
        $Rating = $this->input->post('Rating');
        $Price = $this->input->post('Price');
        $offer = $this->input->post('OfferID');

        //$offer_categories = $this->input->post('OfferSubCategories');
        //$sub_categories = $this->input->post('SubCategories');




        $page = $this->input->post('Page');
        $ul_cls = $this->input->post('ul_cls');

        if (isset($ul_cls) && ($ul_cls == 'items_grid' || $ul_cls == 'items_list'))
        {
            $ul_cls1 = $ul_cls;
        } else {
            $ul_cls1 = 'items_grid';
        }

        $limit = 12;
        $start = $limit * $page;
        $my_products_arr = array();
        $products = array();
        $products1 = array();
        $products2 = array();
        $countproducts_first = 0;
        $total_product_db = 0;
        $html ='<p class="alert alert-danger">'.lang('no_product_available').'</p>';
        $total_product = 0;
        //print_rm($sub_categories);
        /*if (empty($offer) && empty($collection) && empty($collection_categories) && empty($sub_categories) && empty($offer_categories)) {
            $this->getAllProducts($page, $limit, $start, $ul_cls1);
        }
*/



        $where = '1 = 1 AND products.IsActive = 1 AND products.IsCustomizedProduct = 0 AND FIND_IN_SET('.$SubCategoryID.', products.SubCategoryID) <> 0';

        if($Featured == 'true'){
            $where .= ' AND products.IsFeatured = 1';
        }

        $find_in = '';

        if(!empty($TagID)){
            foreach ($TagID as $key => $value){
                 if($key == 0){
                    $find_in .= ' AND ( ';
                 }else{
                    $find_in .= ' OR ';
                 }
                $find_in .= 'FIND_IN_SET('.$value.', products.TagIDs) <> 0';

                
            }
            if($find_in  != ''){
                $find_in .= ' )';
                $where .= $find_in;
            }

        }


        if($Price != 0){
            $price_rang = explode('-',$Price);
            $where .= ' AND  products.Price BETWEEN '.$price_rang[0].' AND '.$price_rang[1].' ';
        }
        //echo $where;exit;

        $product_ids = array();

        if (!empty($offer)) {

            $offer_products = $this->Offer_model->getOfferProducts($offer);


            if ($offer_products) {

                foreach ($offer_products as $product) {
                    $product_id_offer = explode(',', $product->ProductID);

                    foreach ($product_id_offer as $key => $value) {
                        if (!in_array($value, $product_ids)) {
                            $product_ids[] = $value;
                        }

                    }
                }

            }


            $a = implode(',', $product_ids);
            $where .= " AND products.ProductID IN ($a)";

           

        }
        

        $products = $this->Product_model->getProducts($where, $this->language, $limit, $start,'products_text.Title','ASC',$Rating);

       //echo $this->db->last_query();exit;
        $countproducts = $this->Product_model->getCountProducts($where, $this->language, false, $start);
        $total_product_db = $countproducts->Total;


        

        if (!empty($products)) {
            $html = '';
            $html .= product_html($products, $ul_cls1);
            $total_product = COUNT($products) * $page;
        }


        $response['html'] = $html;
        $response['page'] = $page;


        $response['count_product'] = $total_product_db;
        $response['total_now'] = $total_product;

        if ($total_product <= $total_product_db && COUNT($products) != 0) {

            $response['page'] = $page + 1;
        }
        echo json_encode($response);
        exit();


    }


    public function getMoreProductsSearch()
    {
        $sub_categories = $this->input->post('SubCategories');
        $search_key = $this->input->post('SearchValue');
        $page = $this->input->post('Page');

        // saving searched tag in db
        $this->Search_tag_model->save(array('SearchTag' => $search_key, 'UserID' => $this->UserID, 'SearchedAt' => date('Y-m-d H:i:s')));

        $limit = 12;
        $start = $limit * $page;
        $products = array();
        $countproducts_first = 0;
        $total_product_db = 0;
        //print_rm($sub_categories);
        if (empty($sub_categories)) {
            // $this->getAllProducts($page,$limit,$start);
        }


        $where = "1 = 1 AND products.IsActive = 1 AND products.IsCustomizedProduct = 0 AND products_text.Title LIKE '%$search_key%'";


        $html = '';


        if (!empty($sub_categories)) {

            $c = implode(',', $sub_categories);
            $where .= " AND products.SubCategoryID IN ($c)";


        }

        $products = $this->Product_model->getProducts($where, $this->language, $limit, $start);
        $countproducts = $this->Product_model->getCountProducts($where, $this->language, false, $start);
        $total_product_db = $total_product_db + $countproducts->Total;

        $total_product = 0;
        $response['no_result'] = false;
        if (!empty($products)) {
            $html .= product_html($products);
            $total_product = COUNT($products) * $page;
        } else {
            $response['no_result'] = true;
        }


        $response['html'] = $html;
        $response['page'] = $page;


        $response['count_product'] = $total_product_db;
        $response['total_now'] = $total_product;

        if ($total_product <= $total_product_db && COUNT($products) != 0) {

            $response['page'] = $page + 1;
        }
        echo json_encode($response);
        exit();


    }

    public function detail($product_title, $offer_id = "")
    {   
        $this->data['menu'] = 'product';
        $product = explode('-',$product_title);
        $product_id = str_replace('c','',end($product));
        
        
        //$product_id = productID($product_title);
        $this->data['product'] = $this->Product_model->getProducts("products.ProductID = " . $product_id . " AND products.CompanyID = " . $this->companyData->CompanyID . " AND system_languages.ShortCode = '" . $this->language . "'",false ,false, false, false,false,[0,1,2,3,4,5]);
        //print_rm($this->data['product']);
        $this->data['product-bk'] = $this->Product_model->getJoinedData(false, 'ProductID', "products.ProductID = " . $product_id . " AND system_languages.ShortCode = '" . $this->language . "'");
        if (empty($this->data['product'])) {
            redirect(base_url('product'));
        }
        //$this->data['result'] = $this->data['product'][0];
       // $this->data['total_rating'] = $this->Product_rating_model->getWithMultipleFields(array('ProductID' => $product_id));

        
        $where = '';
        //print_rm($this->data['result']);
        if ($this->session->userdata('user')) {
            $this->data['rating'] = $this->Product_rating_model->getWithMultipleFields(array('ProductID' => $product_id, 'UserID' => $this->session->userdata['user']->UserID));
        } else {
            $this->data['rating'] = false;
        }
        if ($this->session->userdata('user')) {
            $this->data['review'] = $this->Product_review_model->getWithMultipleFields(array('ProductID' => $product_id, 'UserID' => $this->session->userdata['user']->UserID));
            //print_rm($this->data['review']);
            $where = 'product_reviews.UserID != ' . $this->session->userdata['user']->UserID . ' AND ';
        } else {
            $this->data['review'] = false;

        }
        //print_rm($this->data['review']);
        $where .= "$where product_reviews.ProductID = " . $product_id;
        $this->data['all_reviews'] = $this->Product_review_model->getUserJoinedData(false, 'UserID', 'users_text', $where);
        //print_rm($this->data['all_reviews']);
        //echo $this->db->last_query();
        $this->data['product'] = $this->data['product'][0];
        //print_rm($this->data['product']);
        $this->data['product_images'] = get_images($product_id, 'product');
        $this->data['offer_id'] = $offer_id;

        $this->data['available_cities'] = $this->Product_model->getCities('product_store_availability.ProductID = '.$product_id,$this->language);
        $this->data['whats_inside'] = $this->What_inside_model->getMultipleRows(array('ProductID' => $product_id),false);
       //print_rm($this->data['rating']);exit;
        $this->data['product_nutritions'] = $this->Nutrition_product_model->productNutrition($product_id, $this->language);

        if($this->data['product']->CrossSellingProductID != ''){
            $where_cross_selling = 'products.IsActive = 1 AND products.CompanyID = '.$this->companyData->CompanyID . ' AND products.ProductID IN ('.$this->data['product']->CrossSellingProductID.')';

            $this->data['products'] = $this->Product_model->getProducts($where_cross_selling, $this->language,5,0,'products_text.Title','ASC',[0,1,2,3,4,5]);
        }else{
            $this->data['products'] = array();
        }

        $where_tags = '';
        $this->data['tags'] = [];
        if($this->data['product']->TagIDs != ''){
            
            $where_tags = ' AND tags.TagID IN ('.$this->data['product']->TagIDs.')';
            $this->data['tags'] = $this->Tag_model->getAllJoinedData(false, 'TagID', $this->language, 'tags.IsActive = 1'.$where_tags);
        }
        //print_rm($this->data['tags']);
        $this->data['variants'] = array();
        $this->data['variants_attributes_prices'] = array();

        if($this->data['product']->VariantData != ''){
            $variantData= json_decode($this->data['product']->VariantData,true);
            $variant_id = explode(',',$variantData['VariantID'] );
            $attribute_array = array_keys($variantData['Attribute']);
            $attribute_ids = implode(',',$attribute_array);
            $this->data['variants_attributes_prices'] = $variantData['Attribute'];
            $this->data['variants'] = $this->Variant_model->getAllJoinedData(true, 'VariantID', $this->language, 'variants.VariantID IN ('.$variantData['VariantID'].')');

            foreach($this->data['variants'] as $key => $value){
                $this->data['variants'][$key]['attributes'] = $this->Variant_model->getAllJoinedData(true, 'VariantID', $this->language, 'variants.ParentID = '.$value['VariantID'].' AND variants.VariantID IN ('.$attribute_ids.')');

            }

        

        }

       
        
        
        $this->data['view'] = 'frontend/product/products-full';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    /*public function giveRating()
    {
        $post_data = $this->input->post(); // ProductID, UserID, Rating
        $alreadyRated = $this->Product_rating_model->getWithMultipleFields(array('ProductID' => $post_data['ProductID'], 'UserID' => $post_data['UserID']));
        if ($alreadyRated) {
            $response['status'] = false;
            $response['message'] = lang('you_have_already_rate_this_product');
            echo json_encode($response);
            exit();
        } else {
            $inserted_id = $this->Product_rating_model->save($post_data);
            if ($inserted_id > 0) {
                $response['status'] = true;
                $response['message'] = lang('thank_you_for_rating');//"Thank you for rating this product.";
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('some_thing_went_wrong');//"Something went wrong while rating this product.";
                echo json_encode($response);
                exit();
            }
        }
    }*/

    public function giveRating()
    {
        $post_data = $this->input->post(); // ProductID, UserID, Rating
        $alreadyRated = $this->Product_rating_model->getWithMultipleFields(array('ProductID' => $post_data['ProductID'], 'UserID' => $post_data['UserID']));
        if ($alreadyRated) {
            $this->Product_rating_model->update(array('Rating' => $post_data['Rating']), array('ProductID' => $post_data['ProductID'], 'UserID' => $post_data['UserID']));
            $response['status'] = true;
            $response['message'] = lang('updated_rating');
            echo json_encode($response);
            exit();
        } else {
            $inserted_id = $this->Product_rating_model->save($post_data);
            if ($inserted_id > 0) {
                $response['status'] = true;
                $response['message'] = lang('thank_you_for_rating');//"Thank you for rating this product.";
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('some_thing_went_wrong');//"Something went wrong while rating this product.";
                echo json_encode($response);
                exit();
            }
        }
    }

    public function saveReview()
    {
        $post_data = $this->input->post(); // Title, Comment, ProductID
        $post_data['UserID'] = $this->session->userdata['user']->UserID;
        $post_data['CreatedAt'] = date('Y-m-d H:i:s');
        $alreadyReviewed = $this->Product_review_model->getWithMultipleFields(array('ProductID' => $post_data['ProductID'], 'UserID' => $post_data['UserID']));
        if ($alreadyReviewed) {
            $response['message'] = lang('you_have_already_given_review');
            echo json_encode($response);
            exit();
        } else {
            $inserted_id = $this->Product_review_model->save($post_data);
            if ($inserted_id > 0) {
                $response['reset'] = true;
                $response['reload'] = true;
                $response['message'] = lang('giving_your_review');
                echo json_encode($response);
                exit();
            } else {
                $response['message'] = lang('some_thing_went_wrong');
                echo json_encode($response);
                exit();
            }
        }
    }


}
