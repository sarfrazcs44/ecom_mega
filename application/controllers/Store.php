<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Store_model');
    }

    public function index()
    {
        $data['menu'] = 'store';
        $data['stores'] = $this->Store_model->getAllStores($this->language);
        //print_rm($data['stores']);
        $data['view'] = 'frontend/stores';
        $this->load->view('frontend/layouts/default', $data);
    }
}