<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbandonedCart extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('Temp_order_model');
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'TempOrderID';
        $this->data['Table'] = 'temp_orders';
        $this->data['language'] = $this->language;
    }


    public function index()
    {
        $this->data['abandoned_carts'] = $this->Temp_order_model->getAbandonedCart();
       // print_rm($this->data['abandoned_carts']);
        $this->data['abandoned_carts_anonymous'] = $this->Temp_order_model->getAbandonedCartAnonymous();
        $this->data['view'] = 'backend/abandoned_cart/manage';

        if (isset($_GET['status']) && $_GET['status'] !== '')
        {
            $status = $_GET['status'];
        } else {
            $status = 'by_user';
        }
        if($status == 'by_user'){
        $this->data['columns'] = "[
            { data: 'FullName' },
            { data: 'Email' },
            { data: 'CartProductsCount' },
            { data: 'CartQuantityCount' },
            { data: 'SubTotal' },
            { data: 'Action' }
        ]";
        } else {
            $this->data['columns'] = "[
            { data: 'UserID' },
            { data: 'CartProductsCount' },
            { data: 'CartQuantityCount' }
        ]";
        }

        $this->data['customID'] = $status;
        $this->data['url_status'] = $status;

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function sendAbandonedCartNotificationToUser()
    {
        $email_sent = false;
        $UserID = $this->input->post('UserID');
        $abandoned_carts = $this->Temp_order_model->getAbandonedCart();
        if (count($abandoned_carts) > 0) {
            foreach ($abandoned_carts as $abandoned_cart) {
                if ($abandoned_cart->UserID == $UserID && $abandoned_cart->Email != '') {
                    $data['to'] = $abandoned_cart->Email;
                    $data['subject'] = 'Abandoned cart at ecommerce';
                    $message = "Dear " . $abandoned_cart->FullName . ",<br>You have " . $abandoned_cart->CartQuantityCount . " items in your cart. Please login and place your order at<br>" . base_url();
                    $data['message'] = email_format($message);
                    $email_sent = sendEmail($data);
                    break;
                }
            }
        }
        $response['status'] = true;
        if ($email_sent) {
            $response['message'] = 'User notified via email successfully.';
        } else {
            $response['message'] = 'Something went wrong while notifying user.';
        }
        echo json_encode($response);
        exit();
    }

    public function getEntries(){

    
     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $status = $request->get('customID');
     
    
     // Total records
     $where = '1 = 1';

     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= " AND ".$this->data['Table'].'.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }
 $where .= " AND ".$this->data['Table'].'.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
    

     


    
    

    

     if($status == 'by_user'){
        $totalRecords = $this->Temp_order_model->getCountAbandonedCart($where);
     
            if($searchValue != ''){
                $where = $where. " AND (users_text.FullName like '%".$searchValue."%' OR users.Email like '%".$searchValue."%')";
        
            }


         $totalRecordswithFilter = $this->Temp_order_model->getCountAbandonedCart($where);// need to set here filer count

          $records =  $this->Temp_order_model->getAbandonedCart($where,$rowperpage,$start);
        } else  {
             
           
            $totalRecords = $this->Temp_order_model->getCountAbandonedCartAnonymous($where);

     
             if($searchValue != ''){
                $where = $where. " AND (temp_orders.UserID like '%".$searchValue."%')";
            }

          $totalRecordswithFilter = $this->Temp_order_model->getCountAbandonedCartAnonymous($where);// need to set here filer count

         $records = $this->Temp_order_model->getAbandonedCartAnonymous($where,$rowperpage,$start);
     }

    
    


     $data_arr = array();
     foreach ($records as $key => $value) {
        $action = '';


        

        if (checkUserRightAccess(87, $this->session->userdata['admin']['UserID'], 'CanEdit')|| checkUserRightAccess(87, $this->session->userdata['admin']['UserID'], 'CanDelete')) { 

            $action .= '<a href="javascript:void(0);"
                                                           class="btn btn-simple btn-warning btn-icon edit"
                                                           onclick="notifyUser('.$value->UserID.');"><i
                                                                    class="material-icons"
                                                                    title="Click to notify this user via email">dvr</i>
                                                            <div class="ripple-container"></div>
                                                        </a>';
        }
         

        if ($status == 'by_user'){
         $data_arr[] = array(
          "FullName" => $value->FullName,
          "Email" => '<a href="mailto:'.$value->Email.'">'.$value->Email.'</a>',
          "CartProductsCount" => $value->CartProductsCount,
          "CartQuantityCount" => $value->CartQuantityCount,
          "SubTotal" => $value->SubTotal,
          "Action" => $action
        );
        }else{
            $data_arr[] = array(
          "UserID" => $value->UserID,
          "CartProductsCount" => $value->CartProductsCount,
          "CartQuantityCount" => $value->CartQuantityCount,
        );
        }
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }

}