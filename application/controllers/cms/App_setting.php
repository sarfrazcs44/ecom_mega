<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('UTC');

class App_setting extends Base_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
       checkAdminSession();
        $this->load->model('App_setting_model');
        $this->load->model('Product_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
       
        
       // $this->data['language'] = $this->language;
    }

    public function index() {

        if($this->session->userdata['admin']['CompanyID'] > 0){
            $this->edit($this->session->userdata['admin']['CompanyID']);
        }else{
            $this->data['view'] = 'backend/app_setting/manage';
            $this->data['companies'] = $this->App_setting_model->getAll();
            $this->load->view('backend/layouts/default',$this->data);
        }
        
    }
    
    public function edit($company_id) {


        $fetch_by = array();

        if($this->session->userdata['admin']['CompanyID'] > 0){
            $company_id = $this->session->userdata['admin']['CompanyID'];
        }

        $fetch_by['CompanyID'] = $company_id;


        $this->data['result'] = $this->App_setting_model->getWithMultipleFields($fetch_by);
        $this->data['products'] = $this->Product_model->getAllProductsForBackend($fetch_by,4,0);

        if (!$this->data['result']) {
            redirect(base_url('cms/user'));
        }
        $this->data['view'] = 'backend/app_setting/edit';


        $this->data['company_id'] = $company_id;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action() {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {

            case 'update';
                //$this->validate();
                $this->update();
                break;
        }
    }

    

    private function update() {
        $post_data = $this->input->post();
        $update_by['AppSettingID'] = decode_url($post_data['id']);

        $app_setting_data = $this->App_setting_model->get($update_by['AppSettingID'],false,'AppSettingID');
        $filename = "uploads/images/" . $app_setting_data->CompanyID . "/";
        if (!file_exists($filename)) {
            mkdir("uploads/images/" . $app_setting_data->CompanyID , 0777);
            exit;
        } 

        if($app_setting_data->CompanyID > 0){
            $images_path = "uploads/images/".$app_setting_data->CompanyID."/";
        }else{
            $images_path = "uploads/images/";
        }


        if(isset($post_data['ShowActualProducts'])){
            $post_data['ShowActualProducts'] = 1;
        }else{
            $post_data['ShowActualProducts'] = 0;
        }

        


        
        unset($post_data['form_type']);
        unset($post_data['id']);
        

        if (isset($_FILES['SplashLogo']["name"][0]) && $_FILES['SplashLogo']["name"][0] != '') {
            $post_data['SplashLogo']        = $this->uploadImage("SplashLogo", "uploads/images/".$app_setting_data->company_id."");
        }
        if (isset($_FILES['SplashBackgroundImage']["name"][0]) && $_FILES['SplashBackgroundImage']["name"][0] != '') {
            $post_data['SplashBackgroundImage']        = $this->uploadImage("SplashBackgroundImage", $images_path);
        }
         if (isset($_FILES['LoginBackgroundImage']["name"][0]) && $_FILES['LoginBackgroundImage']["name"][0] != '') {
            $post_data['LoginBackgroundImage']        = $this->uploadImage("LoginBackgroundImage", $images_path);
        }

        if (isset($_FILES['LoginLogo']["name"][0]) && $_FILES['LoginLogo']["name"][0] != '') {
            $post_data['LoginLogo']        = $this->uploadImage("LoginLogo", $images_path);
        }


        if (isset($_FILES['MainElementsBackgroundImage']["name"][0]) && $_FILES['MainElementsBackgroundImage']["name"][0] != '') {
            $post_data['MainElementsBackgroundImage']        = $this->uploadImage("MainElementsBackgroundImage", $images_path);
        }


        if (isset($_FILES['TabImageHomeIcon']["name"][0]) && $_FILES['TabImageHomeIcon']["name"][0] != '') {
            $post_data['TabImageHomeIcon']        = $this->uploadImage("TabImageHomeIcon", $images_path);
        }


        if (isset($_FILES['TabImageCatalougeIcon']["name"][0]) && $_FILES['TabImageCatalougeIcon']["name"][0] != '') {
            $post_data['TabImageCatalougeIcon']        = $this->uploadImage("TabImageCatalougeIcon", $images_path);
        }


        if (isset($_FILES['TabImageOrdersIcon']["name"][0]) && $_FILES['TabImageOrdersIcon']["name"][0] != '') {
            $post_data['TabImageOrdersIcon']        = $this->uploadImage("TabImageOrdersIcon", $images_path);
        }


        if (isset($_FILES['TabImageSettingIcon']["name"][0]) && $_FILES['TabImageSettingIcon']["name"][0] != '') {
            $post_data['TabImageSettingIcon']        = $this->uploadImage("TabImageSettingIcon", $images_path);
        }


        if (isset($_FILES['TabImageHomeHoverIcon']["name"][0]) && $_FILES['TabImageHomeHoverIcon']["name"][0] != '') {
            $post_data['TabImageHomeHoverIcon']        = $this->uploadImage("TabImageHomeHoverIcon", $images_path);
        }


        if (isset($_FILES['TabImageCatalougeHoverIcon']["name"][0]) && $_FILES['TabImageCatalougeHoverIcon']["name"][0] != '') {
            $post_data['TabImageCatalougeHoverIcon']        = $this->uploadImage("TabImageCatalougeHoverIcon", $images_path);
        }


        if (isset($_FILES['TabImageOrdersHoverIcon']["name"][0]) && $_FILES['TabImageOrdersHoverIcon']["name"][0] != '') {
            $post_data['TabImageOrdersHoverIcon']        = $this->uploadImage("TabImageOrdersHoverIcon", $images_path);
        }


        if (isset($_FILES['TabImageSettingHoverIcon']["name"][0]) && $_FILES['TabImageSettingHoverIcon']["name"][0] != '') {
            $post_data['TabImageSettingHoverIcon']        = $this->uploadImage("TabImageSettingHoverIcon", $images_path);
        }


        if (isset($_FILES['TabImageHomeSelectedIcon']["name"][0]) && $_FILES['TabImageHomeSelectedIcon']["name"][0] != '') {
            $post_data['TabImageHomeSelectedIcon']        = $this->uploadImage("TabImageHomeSelectedIcon", $images_path);
        }


        if (isset($_FILES['TabImageCatalougeSelectedIcon']["name"][0]) && $_FILES['TabImageCatalougeSelectedIcon']["name"][0] != '') {
            $post_data['TabImageCatalougeSelectedIcon']        = $this->uploadImage("TabImageCatalougeSelectedIcon", $images_path);
        }


        if (isset($_FILES['TabImageOrdersSelectedIcon']["name"][0]) && $_FILES['TabImageOrdersSelectedIcon']["name"][0] != '') {
            $post_data['TabImageOrdersSelectedIcon']        = $this->uploadImage("TabImageOrdersSelectedIcon", $images_path);
        }

        if (isset($_FILES['TabImageSettingSelectedIcon']["name"][0]) && $_FILES['TabImageSettingSelectedIcon']["name"][0] != '') {
            $post_data['TabImageSettingSelectedIcon']        = $this->uploadImage("TabImageSettingSelectedIcon", $images_path);
        }



        

        // echo date_default_timezone_get();exit();

       
        
        $this->App_setting_model->update($post_data, $update_by);

        $success['error'] = false;
       // $success['reload'] = true;
        $success['success'] = 'Updated Successfully';
        echo json_encode($success);
        exit;
    }

    /*private function uploadImage($file_key, $path,$id=false,$multiple = false)
    {


       
          $data = array();
         $extension=array("png","jpg","jpeg");
         foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
            {
                $file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
                $file_tmp=$_FILES[$file_key]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                if(in_array($ext,$extension))
                {
                       
                        move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
                        if(!$multiple){
                            return $path.$file_name;
                        }
                        /*$data['DestinationID'] = $id; 
                        $data['ImagePath'] = $path.$file_name; 
                        $this->Model_destination_image->save($data);*/
                    
               /* }else{
                    $success['error'] = 'false';
                    $success['success'] = 'Only PNG Allowed';
                    echo json_encode($success);
                    exit;

                }
               
            }
            return true;


    }*/

    
}
