<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'CouponID';
        $this->data['Table'] = 'coupons';


    }


    public function index()
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['columns'] = "[
            { data: 'Title' },
            { data: 'CouponCode' },
            { data: 'UsageCount' },
            { data: 'DiscountPercentage' },
            { data: 'ExpiryDate' },
            { data: 'IsActive' },
            { data: 'Action' }
        ]";


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {

        if (!checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = array();
        $save_child_data = array();

        if($this->session->userdata['admin']['RoleID'] != 1){

        $save_parent_data['CompanyID'] = $this->session->userdata['admin']['CompanyID'];
        
        }

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }


        $save_parent_data['CouponCode'] = $post_data['CouponCode'];
        $save_parent_data['UsageCount'] = $post_data['UsageCount'];
        $save_parent_data['DiscountPercentage'] = $post_data['DiscountPercentage'];
        $save_parent_data['ExpiryDate'] = $post_data['ExpiryDate'];
        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);


        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {


            // $default_lang = getDefaultLanguage();
            $system_languages = getSystemLanguages();
            foreach ($system_languages as $system_language) {


                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['Description'];
                $save_child_data[$this->data['TableKey']] = $insert_id;
                $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);
            }


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {


        if (!checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_child_data = array();
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {


                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);


                $this->$parent->update($save_parent_data, $update_by);
                $save_child_data['Title'] = $post_data['Title'];

                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);

            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    $save_child_data['Title'] = $post_data['Title'];


                    $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->update($save_child_data, $update_by);

                } else {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->save($save_child_data);
                }


            }

            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }



    public function getEntries(){

    $parent                             = $this->data['Parent_model'];
     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $orderChild = false;

     if ($columnName == 'Title') {
         $orderChild = true;
     }
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $ParentID = $request->get('customID');
    
     // Total records
     $where = ''.$this->data['Table'].'_text.SystemLanguageID = 1';
     
     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND '.$this->data['Table'].'.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }


     $totalRecords = $this->$parent->getRowsCountWithChild($this->data['TableKey'],$where);


    
    

    

     if($searchValue != ''){
        $where = $where. " AND (".$this->data['Table']."_text.Title like '%".$searchValue."%' OR ".$this->data['Table'].".CouponCode like '%".$searchValue."%'  OR ".$this->data['Table'].".UsageCount like '%".$searchValue."%' OR ".$this->data['Table'].".DiscountPercentage like '%".$searchValue."%' OR ".$this->data['Table'].".ExpiryDate like '%".$searchValue."%')";
     }



     $totalRecordswithFilter = $this->$parent->getRowsCountWithChild($this->data['TableKey'],$where);// need to set here filer count
     $records =  $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language,$where,$columnSortOrder,$columnName,$orderChild,$rowperpage,$start);


     $data_arr = array();
     foreach ($records as $key => $value) {
        $action = '';


        

        if (checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanEdit')) { 

            $action .= '<a href="'.base_url('cms/' . $this->data['ControllerName'] . '/edit/' . $value->CouponID).'"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons" title="Edit">dvr</i>
                                                            <div class="ripple-container"></div>
                                                        </a>';
        }

        if (checkUserRightAccess(57, $this->session->userdata['admin']['UserID'], 'CanDelete')) { 

             $action .= '<a href="javascript:void(0);"" onclick="deleteRecord('.$value->CouponID.',\'cms/'.$this->data['ControllerName'].'/action\',\'\');" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>';
            
        }
         

         
        $data_arr[] = array(
            "Title" => $value->Title,
            "CouponCode" => $value->CouponCode,
            "UsageCount" => $value->UsageCount,
            "DiscountPercentage" => $value->DiscountPercentage,
            "ExpiryDate" => $value->ExpiryDate,
            "IsActive" => ($value->IsActive ? lang('yes') : lang('no')),
            "Action" => $action
        );
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }


}