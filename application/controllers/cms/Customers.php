<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
        $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';
    }

    public function index()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        //$this->data['all_customers'] = $this->User_model->getUsers("users.RoleID = 5", $this->language);
        //$this->data['online_customers'] = $this->User_model->getUsers("users.RoleID = 5 AND users.OnlineStatus = 'Online'", $this->language);

        if (isset($_GET['status']) && $_GET['status'] !== '')
        {
            $status = $_GET['status'];
        } else {
            $status = 'all_customers';
        }

        $this->data['columns'] = "[
            { data: 'FullName' },
            { data: 'Email' },
            { data: 'Mobile' },
            { data: 'IsActive' },
            { data: 'Action' }

        ]";

        $this->data['customID'] = $status;
        $this->data['url_status'] = $status;
        $this->load->view('backend/layouts/default', $this->data);
    }

    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;
            case 'suspend';
                $this->suspend();
                break;
            case 'activate';
                $this->activate();
                break;

        }
    }

    private function delete()
    {
        if (!checkUserRightAccess(71, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    private function suspend()
    {
        $update_by = array();
        $update_by['UserID'] = $this->input->post('id');
        $where = 'users.UserID = '.$update_by['UserID'].' AND users.IsActive = 0';
        $this->User_model->update(array('IsActive' => '0'), $update_by);

        // send suspension email
        $user_data = $this->User_model->getJoinedData(true,'UserID',$where);
        //print_rm($user_data);
        $user_data = $user_data[0];
        $email_data['to'] = $user_data['Email'];



        $email_template = get_email_template(16);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_data['FullName'], $message);
       


        $email_data['subject'] = $subject;
        $email_data['message'] = email_format($message);
        sendEmail($email_data);

        $success['error'] = 'false';
        $success['success'] = 'Suspended Successfully';
        echo json_encode($success);
        exit;
    }

    private function activate()
    {
        $update_by = array();
        $update_by['UserID'] = $this->input->post('id');
        $where = 'users.UserID = '.$update_by['UserID'].' AND users.IsActive = 1';
        $this->User_model->update(array('IsActive' => '1'), $update_by);

        // send activation email
        $user_data = $this->User_model->getJoinedData(true,'UserID',$where);
        //print_rm($user_data);
        $user_data = $user_data[0];
        $email_data['to'] = $user_data['Email'];


        $email_template = get_email_template(17);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_data['FullName'], $message);
       


        $email_data['subject'] = $subject;
        $email_data['message'] = email_format($message);
        sendEmail($email_data);

        $success['error'] = 'false';
        $success['success'] = 'Activated Successfully';
        echo json_encode($success);
        exit;

    }

    public function getEntries(){

     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $status = $request->get('customID');

    
     // Total records
     $where = false;

     

        if ($status == 'online_customers'){

            $where = "users.RoleID = 5 AND users.OnlineStatus = 'Online'";
        } else{

            $where = "users.RoleID = 5";
        } 

     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND '.$this->data['Table'].'.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }
     

     $totalRecords = $this->User_model->getUsersCount($where,$this->language);

    

    

    

     if($searchValue != ''){
        $where .= " AND (users_text.FullName like '%".$searchValue."%' OR users.Mobile like '%".$searchValue."%' OR users.Email like '%".$searchValue."%')";
     }



     $totalRecordswithFilter = $this->User_model->getUsersCount($where,$this->language);
     $records =  $this->User_model->getUsers($where,$rowperpage,$start,$this->language,'DESC');

        
     

        $data_arr = array();
     foreach ($records as $key => $value) {
        $action = '';


        

        if ($value->IsActive == '1') { 

            $action .= '<a href="javascript:void(0);" onclick="suspendUser('.$value->UserID.',\'cms/'.$this->data['ControllerName'].'/action\',\'\');"><button class="btn btn-icon waves-effect waves-light btn-danger m-b-5">Suspend</button></a>';
        }

        if ($value->IsActive =='0') {

            $action .='<a href="javascript:void(0);" onclick="activateUser('.$value->UserID.',\'cms/'.$this->data['ControllerName'].'/action\',\'\');"><button class="btn btn-icon waves-effect waves-light btn-primary m-b-5">Activate</button></a>';
        }

        if (checkUserRightAccess(70, $this->session->userdata['admin']['UserID'], 'CanDelete')) { 

             $action .='<a href="javascript:void(0);"" onclick="deleteRecord('.$value->UserID.',\'cms/'.$this->data['ControllerName'].'/action\',\'\');"><button class="btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                                    <i class="fa fa-remove"></i></button></a>';
            
        }

        

        $data_arr[] = array(
          "FullName" => $value->FullName,
          "Email" => $value->Email,
          "Mobile" => $value->Mobile,
          "IsActive" => ($value->IsActive ? lang('yes') : lang('no')),
          "Action" => $action
        );
        
     
}


     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }


}