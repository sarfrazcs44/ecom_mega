<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('UTC');

class Dashboard extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->load->model('Dashboard_model');
        $this->load->model('Order_model');
    }

    public function index($year = '')
    {
        
        if($year == ''){
            $year = date('Y');
        }
        $this->data['year'] = $year;
        $total_orders       = $this->Order_model->getAll();
        if($total_orders){
            $total_orders   = count($total_orders);
        }else{
            $total_orders   = 0;
        }
        $new_orders         = $this->countAllOrders();
        $completed_orders   = $this->countAllOrders(5);
        $cancel_orders      = $this->countAllOrders(6);
        
        
        $scheduled_orders = 0;
        
        
        $this->data['TotalOrders']    = $total_orders;
        $this->data['TotalNewOrders'] = $new_orders;
        $this->data['TodayOrdersCount'] = $scheduled_orders;
        $this->data['CompletedOrdersCount'] = $completed_orders;
        $this->data['CancelOrdersCount'] = $cancel_orders;
        $TotalSales = $this->Order_model->getTotalsales($year);
        //print_rm($TotalSales);

        $this->data['TotalSales'] = array();
        if(!empty($TotalSales)){
            $TotalSales = array_column($TotalSales, 'TotalSales','SalesMonth');
            for($i=0;$i<=11;$i++){
                if(isset($TotalSales[$i+1])){
                    $this->data['TotalSales'][$i] =  $TotalSales[$i+1];
                }else{
                    $this->data['TotalSales'][$i] =  0;
                }
            }

        }
        $TotalOrdersInMonths = $this->Order_model->getTotalOrders($year);
        $this->data['TotalOrdersInMonths'] = array();
        if(!empty($TotalOrdersInMonths)){
            $TotalOrdersInMonths = array_column($TotalOrdersInMonths, 'TotalOrders','OrderMonth');
            for($i=0;$i<=11;$i++){
                if(isset($TotalOrdersInMonths[$i+1])){
                    $this->data['TotalOrdersInMonths'][$i] =  $TotalOrdersInMonths[$i+1];
                }else{
                    $this->data['TotalOrdersInMonths'][$i] =  0;
                }
            }

        }

        //print_rm($this->data['TotalOrdersInMonths']);

        
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    private function countAllOrders($status = 1)
    {
        
        $fetch_by = array();
        $fetch_by['Status'] = $status;
        $new_orders = $this->Order_model->getMultipleRows($fetch_by,false);
        if($new_orders){
            return count($new_orders);
        }else{
            return 0;
        }
    }

    private function bookingsForToday()
    {
        $language = 'EN';
        $Date_timestamp = time();
        $Date = date('Y-m-d', $Date_timestamp);
        $where_scheduled = "DATE_FORMAT(FROM_UNIXTIME(bookings.BookingTime), '%Y-%m-%d') = '$Date'";
        $scheduled_orders = $this->Booking_model->getBookings($where_scheduled, true, false, 0, $language);
        return $scheduled_orders;
    }

    private function getCompletedOrders()
    {
        $language = 'EN';
        $where_completed = "bookings.Status = 5";
        $completed_orders = $this->Booking_model->getBookings($where_completed, false, false, 0, $language);
        return $completed_orders;
    }

    private function getOverdueOrders()
    {
        $language = 'EN';
        $where_overdue = "bookings.Status = 7";
        $overdue_bookings = $this->Booking_model->getBookings($where_overdue, false, false, 0, $language);
        return $overdue_bookings;
    }

}