<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Order_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = 'invoice';


    }

    public function index()
    {
        $this->data['invoices'] = $this->Order_model->getOrders();
        $this->data['view'] = 'backend/invoice/manage';

        $this->data['columns'] = "[
            { data: 'FullName' },
            { data: 'Mobile' },
            { data: 'Email' },
            { data: 'OrderNumber' },
            { data: 'OrderStatusEn' },
            { data: 'TotalAmount' },
            { data: 'UserCity' },
            { data: 'CreatedAt' },
            { data: 'Action' }
        ]";

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function download($OrderID)
    {
        $order_html = get_order_invoice($OrderID);
        generate_pdf($order_html, $OrderID);
    }

    public function getEntries(){

     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     
    
     // Total records
     $where = false;

     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'orders.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }



     $totalRecords = $this->Order_model->getOrdersCount($where,$this->language);



    
    

    

     if($searchValue != ''){
        $where = "users_text.FullName like '%".$searchValue."%' OR users.Mobile like '%".$searchValue."%' OR users.Email like '%".$searchValue."%' OR orders.OrderNumber like '%".$searchValue."%' OR cities_text.Title like '%".$searchValue."%'";
     }



     $totalRecordswithFilter = $this->Order_model->getOrdersCount($where,$this->language);
     $records =  $this->Order_model->getOrders($where,$rowperpage,$start,$this->language,'DESC');


     $data_arr = array();
     foreach ($records as $key => $invoice) {
        $status = $invoice->Status;
        if ($status == 1) {
            $class = "btn btn-sm";
        } else if ($status == 2) {
            $class = "btn btn-primary btn-sm";
        } else if ($status == 3) {
            $class = "btn btn-warning btn-sm";
        }else if ($status == 4) {
            $class = "btn btn-success btn-sm";
        } else if ($status == 5) {
            $class = "btn btn-danger btn-sm";
        }
        $action = '';


        

        if (checkUserRightAccess(66, $this->session->userdata['admin']['UserID'], 'CanEdit')) { 

            $action .= '<a href="'.base_url('cms/orders/invoice/' . $invoice->OrderID).'"
                                                       class="btn btn-simple btn-warning btn-icon"
                                                       target="_blank"><i
                                                                class="material-icons"
                                                                title="View order invoice">receipt</i>
                                                        <div class="ripple-container"></div>
                                                    </a>';
        }
        /*if (checkUserRightAccess(66, $this->session->userdata['admin']['UserID'], 'CanEdit')) {

            $action .='<a href="'.base_url('cms/invoice/download/' . $invoice->OrderID).'"
                                                       class="btn btn-simple btn-warning btn-icon"><i
                                                                class="material-icons"
                                                                title="Download order invoice">save_alt</i>
                                                        <div class="ripple-container"></div>
                                                    </a>';
        }*/
         

         
         $data_arr[] = array(
          "FullName" => $invoice->FullName,
          "Mobile" => $invoice->Mobile,
          "Email" => $invoice->Email,
          "OrderNumber" => $invoice->OrderNumber,
          "OrderStatusEn" => '<button class="'.$class.'">
                                                    '.$invoice->OrderStatusEn.'
                                                    <div class="ripple-container"></div>
                                                </button>',
          "TotalAmount" => number_format($invoice->TotalAmount, 2).' SAR',
          "UserCity" => $invoice->UserCity,
          "CreatedAt" => $invoice->CreatedAt,
          "Action" => $action
        );
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }

}