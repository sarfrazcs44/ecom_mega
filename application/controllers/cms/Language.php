<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends Base_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
           
		parent::__construct();
		checkAdminSession();
                
                $this->load->Model([
			'System_language_model'
		]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = 'System_language_model';
              
                $this->data['TableKey'] = 'SystemLanguageID';
                $this->data['Table'] = 'system_languages';
       
		
	}
	 
    
    public function index()
	{
          $parent                             = $this->data['Parent_model'];
         
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        
          $this->data['results'] = $this->$parent->getAll(false);
          
          $this->load->view('backend/layouts/default',$this->data);
	}
    public function add()
    {
         if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
       
        
        
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
	{
        if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        $this->data['result']		    = $this->$parent->get($id,false,'SystemLanguageID');
	
        
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
       
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data[$this->data['TableKey']] 	 = $id;
	$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->update();
          break;
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('SystemLanguageTitle', lang('title'), 'required');
        $this->form_validation->set_rules('ShortCode', lang('short_code'), 'required|is_unique[system_languages.ShortCode]');
      
      
        



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        
        if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
       
        $save_parent_data                   = array();
       
        
    
        $save_parent_data['IsActive']                  = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['IsDefault']                  = (isset($post_data['IsDefault']) ? 1 : 0 );
        if($save_parent_data['IsDefault']  == 1){
            $update = array();
            $udapte['IsDefault'] = 0;
            $this->$parent->update($udapte,array('IsDefault' => 1));
        }
        $save_parent_data['SystemLanguageTitle']      = $post_data['SystemLanguageTitle'];
        $save_parent_data['ShortCode']                = $post_data['ShortCode']; 

        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');		
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];


        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {
                $path = APPPATH . "language/";
                if (!is_dir($path.$post_data['ShortCode'])) {
                    mkdir($path.$post_data['ShortCode'], 0777, TRUE);
                    $new_path = $path.$post_data['ShortCode'].'/';
                    $data = "<?php
                                ?>";
                    write_file($new_path. 'rest_controller_lang' . '.php', $data);
                }
            
            
                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
	}
    
        private function update()
	{
		


                if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanEdit')){
                $errors['error'] =  lang('you_dont_have_its_access');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/'.$this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }
                $post_data = $this->input->post();
                $parent                             = $this->data['Parent_model'];
          
                if(isset($post_data[$this->data['TableKey']])){
                    $id = $post_data[$this->data['TableKey']];
                    $this->data['result']		    = $this->$parent->get($id,false,'SystemLanguageID');
                  
	
        
                if(!$this->data['result']){
                   $errors['error'] =  lang('some_thing_went_wrong');
                   $errors['success'] =   false;
                   $errors['redirect'] = true;
                   $errors['url'] = 'cms/'.$this->router->fetch_class();
                   echo json_encode($errors);
                   exit;
                }
                
                
            
                
	        unset($post_data['form_type']);
		$save_parent_data                   = array();
               
                $save_parent_data['SystemLanguageTitle']      = $post_data['SystemLanguageTitle'];
                $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                $save_parent_data['IsDefault']       = (isset($post_data['IsDefault']) ? 1 : 0 );
               
                if(isset($this->data['result']->IsDefault) && $this->data['result']->IsDefault == 1 && $save_parent_data['IsDefault'] == 0){
                   $errors['error'] =  lang('please_select_default_value_language_first');
                   $errors['success'] =   false;
                  
                   echo json_encode($errors);
                   exit;
                }
                
                
                $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');		
                $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                if($save_parent_data['IsDefault']  == 1){
                    $update = array();
                    $udapte['IsDefault'] = 0;
                    $this->$parent->update($udapte,array('IsDefault' => 1));
                }
                $update_by  = array();
                $update_by[$this->data['TableKey']]  = $id;
                    



                $this->$parent->update($save_parent_data,$update_by);
		
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;  
                
                
              
              
           
		
        


        
	}else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    
    
    
    
   /*
    
    private function delete(){
        
         if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    */
    
	

}