<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                $this->load->dbforge();
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            ucfirst($this->router->fetch_class()).'_text_model',
            ucfirst($this->router->fetch_class()).'_rights_model',
            ucfirst($this->router->fetch_class()).'s_users_rights_model',
                        'Role_model',
                        'User_model'
        ]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
                $this->data['TableKey'] = 'ModuleID';
                $this->data['Table'] = 'modules';
       
        
    }
     
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
          $child                              = $this->data['Child_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';

          $this->data['columns'] = "[
            { data: 'Title' },
            { data: 'Slug' },
            { data: 'IsActive' },
            { data: 'Action' }
        ]";
          
          $this->load->view('backend/layouts/default',$this->data);
    }
    public function add()
    {
        $parent                             = $this->data['Parent_model'];
        

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
       
        $this->data['results'] = $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language,'modules.ParentID = 0');
        
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
    {
        
        $parent                             = $this->data['Parent_model'];
        $this->data['result']           = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
    
        
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
       
        $this->data['modules'] = $this->$parent->getAllJoinedData(false,'ModuleID',$this->language,'modules.ParentID = 0');
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data['ModuleID']      = $id;
    $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->validate();
                $this->update();
          break;
            case 'delete':
                //$this->validate();
                $this->delete();
          break; 
                
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique['.$this->data['Table'].'_text.Title]');
        $this->form_validation->set_rules('Slug', lang('slug'), 'required|is_unique['.$this->data['Table'].'.Slug]');
        



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }
       
        $save_parent_data['IconClass']      = $post_data['IconClass'];
        $save_parent_data['Slug']           = $post_data['Slug'];
        $save_parent_data['ParentID']       = $post_data['ParentID'];
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );


        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];


        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {
                
            
            
            
                $default_lang = getDefaultLanguage();
                
                
                
                $save_child_data['Title']                        = $post_data['Title'];
                $save_child_data[$this->data['TableKey']]        = $insert_id;
                $save_child_data['SystemLanguageID']             = $default_lang->SystemLanguageID;
                $save_child_data['CreatedAt']                    = $save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);
                
                 $roles = $this->Role_model->getAll();
                foreach($roles as $key => $value)
                {

                        
                        if($value->RoleID == 1 || $value->RoleID == 2){
                            $rights_all = 1;
                        }else{
                            $rights_all = 0;
                        }
                    
                        $other_data[] = [
                                'ModuleID'  => $insert_id,
                                'RoleID'    => $value->RoleID,
                                'CanView'   => $rights_all,
                                'CanAdd'    => $rights_all,
                                'CanEdit'   => $rights_all,
                                'CanDelete' => $rights_all,
                                'CreatedAt' => date('Y-m-d H:i:s'),
                                'CreatedBy' => $this->session->userdata['admin']['UserID'],
                                'UpdatedAt' => date('Y-m-d H:i:s'),
                                'UpdatedBy' => $this->session->userdata['admin']['UserID']
                        ];



                }
                
                
               
                $this->Module_rights_model->insert_batch($other_data);


                $users = $this->User_model->getAll();
                foreach($users as $key => $value)
                {

                        
                        if($value->RoleID == 1 || $value->RoleID == 2){
                            $rights_all = 1;
                        }else{
                            $rights_all = 0;
                        }
                    
                        $other_user_data[] = [
                                'ModuleID'  => $insert_id,
                                'UserID'    => $value->UserID,
                                'CanView'   => $rights_all,
                                'CanAdd'    => $rights_all,
                                'CanEdit'   => $rights_all,
                                'CanDelete' => $rights_all,
                                'CreatedAt' => date('Y-m-d H:i:s'),
                                'CreatedBy' => $this->session->userdata['admin']['UserID'],
                                'UpdatedAt' => date('Y-m-d H:i:s'),
                                'UpdatedBy' => $this->session->userdata['admin']['UserID']
                        ];



                }

                $this->Modules_users_rights_model->insert_batch($other_user_data);

                $FileName = $post_data['Slug'];

                if(isset($post_data['CreateController']))
                {
                    $this->writeANewFile($FileName,$insert_id);
                }
                $this->writeModelFile($FileName, $post_data);//creating model
                $this->writeModelTextFile($FileName, $post_data);//creating model text
                if(isset($post_data['CreateView']))
                {
                    $this->writeAddFileInNewFolder($FileName); //Creating add file
                    $this->writeEditFileInNewFolder($FileName); //Creating edit file
                    $this->writeManageFileInNewFolder($FileName,$insert_id); //Creating manage file
                }
                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }
    
        private function update()
    {
        
                $post_data = $this->input->post();
                $parent                             = $this->data['Parent_model'];
                $child                              = $this->data['Child_model'];
                if(isset($post_data[$this->data['TableKey']])){
                    $id = base64_decode($post_data[$this->data['TableKey']]);
                    $this->data['result']           = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
    
        
                if(!$this->data['result']){
                   $errors['error'] =  lang('some_thing_went_wrong');
                   $errors['success'] =   false;
                   $success['redirect'] = true;
                   $success['url'] = 'cms/'.$this->router->fetch_class();
                   echo json_encode($errors);
                   exit;
                }

            
                
            unset($post_data['form_type']);
        $save_parent_data                   = array();
                $save_child_data                    = array();
                if(isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1){
                    
                   
                    
                    $save_parent_data['IconClass']      = $post_data['IconClass'];
                    $save_parent_data['Slug']           = $post_data['Slug'];
                    $save_parent_data['ParentID']       = $post_data['ParentID'];
                    $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                    $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');      
                    $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                    
                    
                    
                    $this->$parent->update($save_parent_data,$update_by);
                    $save_child_data['Title']                        = $post_data['Title'];
                   
                    $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];
                    
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $this->$child->update($save_child_data,$update_by);
                    
                }else{
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $get_data = $this->$child->getWithMultipleFields($update_by);
                    
                    if($get_data){
                        
                        $save_child_data['Title']                        = $post_data['Title'];
                       
                        
                        $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];



                        $this->$child->update($save_child_data,$update_by);
                        
                    }else{
                        
                        $save_child_data['Title']                        =  $post_data['Title'];
                        $save_child_data[$this->data['TableKey']]        =  base64_decode($post_data[$this->data['TableKey']]);
                        $save_child_data['SystemLanguageID']             =  base64_decode($post_data['SystemLanguageID']);
                        $save_child_data['CreatedAt']                    =  $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['CreatedBy']                    =  $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];



                        $this->$child->save($save_child_data);
                    }
                    
                    
                    
                    
                }
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;  
                
                
              
              
           
        
        


        
    }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    
    
    
    
   
    
    private function delete(){
        
        
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->Module_rights_model->delete($deleted_by);
        $this->Modules_users_rights_model->delete($deleted_by);
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    
    
    
    
    
    private function writeANewFile($FileName,$ModuleID) {
        
        $path = APPPATH . "controllers/cms/";
        $UCaseClassname = ucfirst($FileName);
        $LCaseClassname = lcfirst($FileName);
        $data = "<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ".$UCaseClassname." extends Base_Controller {
    public  \$data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                \$this->load->Model([
            ucfirst(\$this->router->fetch_class()).'_model',
            ucfirst(\$this->router->fetch_class()).'_text_model'
        ]);
                
                
                
                
                \$this->data['language']      = \$this->language;
                \$this->data['ControllerName'] = \$this->router->fetch_class();
                \$this->data['Parent_model']   = ucfirst(\$this->router->fetch_class()).'_model';
                \$this->data['Child_model']    = ucfirst(\$this->router->fetch_class()).'_text_model';
                \$this->data['TableKey'] = '".$UCaseClassname."ID';
                \$this->data['Table'] = '".$this->getPluralPrase($LCaseClassname,2)."';
       
        
    }
     
    
    public function index()
    {
          \$parent                             = \$this->data['Parent_model'];
          \$child                              = \$this->data['Child_model'];
          \$this->data['view'] = 'backend/'.\$this->data['ControllerName'].'/manage';
        
          \$this->data['results'] = \$this->\$parent->getAllJoinedData(false,\$this->data['TableKey'],\$this->language);
          
          \$this->load->view('backend/layouts/default',\$this->data);
    }
    public function add()
    {
         if(!checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanAdd')){
             \$this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.\$this->router->fetch_class())); 
        }
        \$parent                             = \$this->data['Parent_model'];
        

        \$this->data['view'] = 'backend/'.\$this->data['ControllerName'].'/add';
       
        
        
        \$this->load->view('backend/layouts/default',\$this->data);
    }
    
    public function edit(\$id = '')
    {
        if(!checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanEdit')){
             \$this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.\$this->router->fetch_class())); 
        }
        \$parent                             = \$this->data['Parent_model'];
        \$this->data['result']          = \$this->\$parent->getJoinedData(false,\$this->data['TableKey'],\$this->data['Table'].'.'.\$this->data['TableKey'].'='.\$id,'DESC','');
    
        
        if(!\$this->data['result']){
           redirect(base_url('cms/'.\$this->router->fetch_class())); 
        }
        
       
        
       
        \$this->data['view'] = 'backend/'.\$this->data['ControllerName'].'/edit';
        \$this->data[\$this->data['TableKey']]   = \$id;
    \$this->load->view('backend/layouts/default',\$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        \$form_type = \$this->input->post('form_type');
        switch(\$form_type){
            case 'save':
                \$this->validate();
                \$this->save();
          break; 
            case 'update':
                \$this->update();
          break;
            case 'delete':
                \$this->delete();
          break;      
                 
        }
    }
    
    
    private function validate(){
        \$errors = array();
        \$this->form_validation->set_error_delimiters('<div class=\"error\">', '</div>');

        \$this->form_validation->set_rules('Title', lang('title'), 'required|is_unique['.\$this->data['Table'].'_text.Title]');
      
        



        if (\$this->form_validation->run() == FALSE)
        {
            \$errors['error'] = validation_errors();
            \$errors['success'] = false;
            echo json_encode(\$errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        
        if(!checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanAdd')){
            \$errors['error'] =  lang('you_dont_have_its_access');
            \$errors['success'] = false;
            \$errors['redirect'] = true;
            \$errors['url'] = 'cms/'.\$this->router->fetch_class();
            echo json_encode(\$errors);
            exit;
        }
        \$post_data                          = \$this->input->post();
        \$parent                             = \$this->data['Parent_model'];
        \$child                              = \$this->data['Child_model'];
        \$save_parent_data                   = array();
        \$save_child_data                    = array();
       
        \$getSortValue = \$this->\$parent->getLastRow(\$this->data['TableKey']);
           
        \$sort = 0;
        if(!empty(\$getSortValue))
        {
           
            \$sort = \$getSortValue['SortOrder'] + 1;
        }
       
        
        \$save_parent_data['SortOrder']      = \$sort;
        \$save_parent_data['IsActive']       = (isset(\$post_data['IsActive']) ? 1 : 0 );


        \$save_parent_data['CreatedAt']      = \$save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');        
        \$save_parent_data['CreatedBy']      = \$save_parent_data['UpdatedBy']    = \$this->session->userdata['admin']['UserID'];


        \$insert_id                          = \$this->\$parent->save(\$save_parent_data);
        if(\$insert_id > 0)
            {
                
            
            
            
            \$system_languages = getSystemLanguages();
            foreach (\$system_languages as \$system_language) {
                    \$save_child_data['Title']                        = \$post_data['Title'];
                    \$save_child_data[\$this->data['TableKey']]        = \$insert_id;
                    \$save_child_data['SystemLanguageID']             = \$system_language->SystemLanguageID;
                    \$save_child_data['CreatedAt']                    = \$save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
                    \$save_child_data['CreatedBy']                    = \$save_child_data['UpdatedBy']    = \$this->session->userdata['admin']['UserID'];
                    \$this->\$child->save(\$save_child_data);
                }
                
                
                
               
              
                \$success['error']   = false;
                \$success['success'] = lang('save_successfully');
                \$success['redirect'] = true;
                \$success['url'] = 'cms/'.\$this->router->fetch_class().'/edit/'.\$insert_id;
                echo json_encode(\$success);
                exit;


            }else
            {
                \$errors['error'] =  lang('some_thing_went_wrong');
                \$errors['success'] = false;
                echo json_encode(\$errors);
                exit;
            }
    }
    
        private function update()
    {
        


                if(!checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanEdit')){
                \$errors['error'] =  lang('you_dont_have_its_access');
                \$errors['success'] = false;
                \$errors['redirect'] = true;
                \$errors['url'] = 'cms/'.\$this->router->fetch_class();
                echo json_encode(\$errors);
                exit;
            }
                \$post_data = \$this->input->post();
                \$parent                             = \$this->data['Parent_model'];
                \$child                              = \$this->data['Child_model'];
                if(isset(\$post_data[\$this->data['TableKey']])){
                    \$id = base64_decode(\$post_data[\$this->data['TableKey']]);
                    \$this->data['result']          = \$this->\$parent->getJoinedData(false,\$this->data['TableKey'],\$this->data['Table'].'.'.\$this->data['TableKey'].'='.\$id,'DESC','');
    
        
                if(!\$this->data['result']){
                   \$errors['error'] =  lang('some_thing_went_wrong');
                   \$errors['success'] =   false;
                   \$errors['redirect'] = true;
                   \$errors['url'] = 'cms/'.\$this->router->fetch_class();
                   echo json_encode(\$errors);
                   exit;
                }

            
                
            unset(\$post_data['form_type']);
        \$save_parent_data                   = array();
                \$save_child_data                    = array();
                if(isset(\$post_data['IsDefault']) && \$post_data['IsDefault'] == 1){
                    
                   
                   
                    \$save_parent_data['IsActive']       = (isset(\$post_data['IsActive']) ? 1 : 0 );
                    \$save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');     
                    \$save_parent_data['UpdatedBy']      = \$this->session->userdata['admin']['UserID'];
                    
                    \$update_by  = array();
                    \$update_by[\$this->data['TableKey']]  = base64_decode(\$post_data[\$this->data['TableKey']]);
                    
                    
                    
                    
                    \$this->\$parent->update(\$save_parent_data,\$update_by);
                    \$save_child_data['Title']                        = \$post_data['Title'];
                   
                    \$save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                    \$save_child_data['UpdatedBy']                    = \$this->session->userdata['admin']['UserID'];
                    
                    \$update_by['SystemLanguageID']       =  base64_decode(\$post_data['SystemLanguageID']);
                    
                    \$this->\$child->update(\$save_child_data,\$update_by);
                    
                }else{
                    
                    \$update_by  = array();
                    \$update_by[\$this->data['TableKey']]  = base64_decode(\$post_data[\$this->data['TableKey']]);
                    \$update_by['SystemLanguageID']       =  base64_decode(\$post_data['SystemLanguageID']);
                    
                    \$get_data = \$this->\$child->getWithMultipleFields(\$update_by);
                    
                    if(\$get_data){
                        
                        \$save_child_data['Title']                        = \$post_data['Title'];
                       
                        
                        \$save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        \$save_child_data['UpdatedBy']    = \$this->session->userdata['admin']['UserID'];



                        \$this->\$child->update(\$save_child_data,\$update_by);
                        
                    }else{
                        
                        \$save_child_data['Title']                        =  \$post_data['Title'];
                        \$save_child_data[\$this->data['TableKey']]        =  base64_decode(\$post_data[\$this->data['TableKey']]);
                        \$save_child_data['SystemLanguageID']             =  base64_decode(\$post_data['SystemLanguageID']);
                        \$save_child_data['CreatedAt']                    =  \$save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        \$save_child_data['CreatedBy']                    =  \$save_child_data['UpdatedBy']                    = \$this->session->userdata['admin']['UserID'];



                        \$this->\$child->save(\$save_child_data);
                    }
                    
                    
                    
                    
                }
        
              \$success['error']   = false;
              \$success['success'] = lang('update_successfully');
        
              echo json_encode(\$success);
              exit;  
                
                
              
              
           
        
        


        
    }else{
            \$errors['error'] =  lang('some_thing_went_wrong');
            \$errors['success'] = false;
            \$errors['redirect'] = true;
            \$errors['url'] = 'cms/'.\$this->router->fetch_class();
            echo json_encode(\$errors);
            exit;
            
        }
     }
    
    
    
    
   
    
    private function delete(){
        
         if(!checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanDelete')){
            \$errors['error'] =  lang('you_dont_have_its_access');
           \$errors['success'] = false;
           
            echo json_encode(\$errors);
            exit;
        }
        \$parent                             = \$this->data['Parent_model'];
        \$child                              = \$this->data['Child_model'];
        
        \$deleted_by = array();
        \$deleted_by[\$this->data['TableKey']] = \$this->input->post('id');
        \$this->\$child->delete(\$deleted_by);
        \$this->\$parent->delete(\$deleted_by);
       
        
        
        
        \$success['error']   = false;
        \$success['success'] = lang('deleted_successfully');
        
        echo json_encode(\$success);
        exit;
    }
    
    
    

}";
        $result = write_file($path.$UCaseClassname.'.php', $data);
        $result_Controller = json_encode($result);
        
        /*if($result_Controller){
            
             $this->writeModelFile($FileName);//creating model
             $this->writeModelTextFile($FileName);//creating model text
             $this->writeAddFileInNewFolder($FileName); //Creating add file
             $this->writeEditFileInNewFolder($FileName); //Creating edit file
             $this->writeManageFileInNewFolder($FileName,$ModuleID); //Creating manage file
        }*/
        
        //Start writing in Lang file
        $LangFilepath = APPPATH . "language/EN/";
        $my_file = $LangFilepath.'rest_controller_lang.php'; //filename
        $handle = fopen($my_file, 'a') or die('Cannot open file:  '.$my_file);
        $lang['modules']                = 'Modules';
        $lang['module']                = 'Module';
        $AddData = "\n"."\$lang['add_".$LCaseClassname."']            = 'Add ".$UCaseClassname."';";
        fwrite($handle, $AddData);
        $PluralName = "\n"."\$lang['".$LCaseClassname."s']            = '".$UCaseClassname."s';";
        fwrite($handle, $PluralName);
        $SigularName = "\n"."\$lang['".$LCaseClassname."']            = '".$UCaseClassname."';";
        fwrite($handle, $SigularName);
        //End
    }
    
    private function writeAddFileInNewFolder($FileName) {
        $path = APPPATH . "views/backend/";
        if (!is_dir($path.$FileName)) {
            mkdir($path.$FileName, 0777, TRUE);
        }
        $newFilePath = $path.$FileName.'/';
        $data = "<div class=\"content\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"card\">
                    <div class=\"card-header card-header-icon\" data-background-color=\"purple\">
                        <i class=\"material-icons\">person</i>
                    </div>
                    <div class=\"card-content\">
                        <h4 class=\"card-title\"><?php echo lang('add').' '.lang(\$ControllerName);?></h4>
                        <div class=\"toolbar\">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action=\"<?php echo base_url();?>cms/<?php echo \$ControllerName; ?>/action\" method=\"post\" onsubmit=\"return false;\" class=\"form_data\" enctype=\"multipart/form-data\" data-parsley-validate novalidate>
                            <input type=\"hidden\" name=\"form_type\" value=\"save\">


                            <div class=\"row\">

                                <div class=\"col-md-6\">
                                    <div class=\"form-group label-floating\">
                                        <label class=\"control-label\" for=\"Title\"><?php echo lang('title'); ?></label>
                                        <input type=\"text\" name=\"Title\" required  class=\"form-control\" id=\"Title\">
                                    </div>
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-sm-4 checkbox-radios\">
                                    <div class=\"form-group label-floating\">
                                        <div class=\"checkbox\">
                                            <label for=\"IsActive\">
                                                <input name=\"IsActive\" value=\"1\" type=\"checkbox\" id=\"IsActive\" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class=\"form-group text-right m-b-0\">
                                <button class=\"btn btn-primary waves-effect waves-light\" type=\"submit\">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href=\"<?php echo base_url();?>cms/<?php echo \$ControllerName;?>\">
                                    <button type=\"button\" class=\"btn btn-default waves-effect m-l-5\">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>";
        $result = write_file($newFilePath . 'add' . '.php', $data);
        return json_encode($result);
    }
    
    
    private function writeEditFileInNewFolder($FileName) {
        $path = APPPATH . "views/backend/";
        if (!is_dir($path.$FileName)) {
            mkdir($path.$FileName, 0777, TRUE);
        }
        $newFilePath = $path.$FileName.'/';
        $data = "<?php

\$languages = getSystemLanguages();
\$lang_tabs = '';
\$lang_data = '';
if(!empty(\$languages)){
    foreach(\$languages as \$key => \$language){
        \$common_fields = '';
        \$common_fields2 = '';
        if(\$key == 0){
         
        \$common_fields2 = '<div class=\"col-sm-4 checkbox-radios\">
                                    <div class=\"form-group label-floating\">
                                        <div class=\"checkbox\">
                                            <label for=\"IsActive\">
                                                <input name=\"IsActive\" value=\"1\" type=\"checkbox\" id=\"IsActive\" '.((isset(\$result[\$key]->IsActive) && \$result[\$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>';
        }

        \$lang_tabs .= '<li class=\"'.(\$key == 0 ? 'active' : '').'\">
                                        <a href=\"#'.\$language->SystemLanguageTitle.'\" data-toggle=\"tab\">
                                            '.\$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        \$lang_data .= '<div class=\"tab-pane '.(\$key == 0 ? 'active' : '').'\" id=\"'.\$language->SystemLanguageTitle.'\">
                      <form action=\"'.base_url().'cms/'.\$ControllerName.'/action\" method=\"post\" onsubmit=\"return false;\" class=\"form_data\" enctype=\"multipart/form-data\" data-parsley-validate novalidate> 
                                                    <input type=\"hidden\" name=\"form_type\" value=\"update\">
                                                    <input type=\"hidden\" name=\"SystemLanguageID\" value=\"'.base64_encode(\$language->SystemLanguageID).'\">
                                                    <input type=\"hidden\" name=\"'.\$TableKey.'\" value=\"'.base64_encode(\$result[0]->\$TableKey).'\">
                                                    <input type=\"hidden\" name=\"IsDefault\" value=\"'.\$language->IsDefault.'\">

                                                   
                                                    <div class=\"row\">
                                                        
                                                        <div class=\"col-md-6\">
                                                            <div class=\"form-group label-floating\">
                                                                <label class=\"control-label\" for=\"Title'.\$key.'\">'.lang('title').'</label>
                                                                <input type=\"text\" name=\"Title\" parsley-trigger=\"change\" required  class=\"form-control\" id=\"Title'.\$key.'\" value=\"'.((isset(\$result[\$key]->Title)) ? \$result[\$key]->Title : '').'\">
                                                               
                                                            </div>
                                                        </div>
                                                         '.\$common_fields2.'
                                                    </div>
                                                    
                                                   
                                                    

                                                    <div class=\"form-group text-right m-b-0\">
                                                        <button class=\"btn btn-primary waves-effect waves-light\" type=\"submit\">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href=\"'.base_url().'cms/'.\$ControllerName.'\">
                                                        <button type=\"button\" class=\"btn btn-default waves-effect m-l-5\">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>

<div class=\"content\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-lg-12 col-md-12\">
                <div class=\"card\">
                    <div class=\"card-header\">
                        <h5 class=\"card-title\">Edit <?php echo \$result[0]->Title; ?> </h5>
                    </div>
                    <div class=\"card-content\">
                        <div class=\"row\">
                            <div class=\"col-md-2\">
                                <ul class=\"nav nav-pills nav-pills-rose nav-stacked\">
                                    <?php echo \$lang_tabs; ?>
                                </ul>
                            </div>
                            <div class=\"col-md-10\">
                                <div class=\"tab-content\">
                                    <?php echo \$lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>";
        $result = write_file($newFilePath . 'edit' . '.php', $data);
        return json_encode($result);
    }
    
    
    private function writeModelFile($FileName, $post_data) {

        if(isset($post_data['CreateTable']))
        {
           
           $fields = array(
                            ucfirst($FileName).'ID' => array(
                                                     'type' => 'INT',
                                                     'constraint' => 11, 
                                                     'auto_increment' => TRUE
                                                    
                                              ),
                            'SortOrder' => array(
                                                     'type' => 'INT',
                                                     'constraint' => '11',
                                              ),
                            'Hide' => array(
                                                     'type' =>'TINYINT',
                                                     'constraint' => '4',
                                                     
                                              ),
                            'IsActive' => array(
                                                     'type' => 'TINYINT',
                                                    'constraint' => '4',
                                              ),
                            'CreatedAt' => array(
                                                     'type' => 'DATETIME',
                                                     
                                              ),
                            'UpdatedAt' => array(
                                                     'type' => 'DATETIME',
                                                     
                                              ), 
                            'CreatedBy' => array(
                                                     'type' => 'INT',
                                                     'constraint' => '11',
                                              ), 
                             'UpdatedBy' => array(
                                                     'type' => 'INT',
                                                     'constraint' => '11',
                                              )                  
                    );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key(ucfirst($FileName).'ID', TRUE);
            $this->dbforge->create_table(lcfirst($this->getPluralPrase($FileName,2)), TRUE);

        }

        if(isset($post_data['CreateModel']))
        {
            $path = APPPATH . "models/";
           // $this->dbforge->add_key(ucfirst($FileName).'ID', TRUE);
            $data = "<?php
                Class ".ucfirst($FileName)."_model extends Base_Model
                {
                    public function __construct()
                    {
                        parent::__construct(\"".lcfirst($this->getPluralPrase($FileName,2))."\");

                    }


                }";
            $result = write_file($path . ucfirst($FileName). '_model' . '.php', $data);
            return json_encode($result);
        }
    }
    
    private function writeModelTextFile($FileName, $post_data) {

        if(isset($post_data['CreateTable']))
        {
            $fields = array(
                            ucfirst($FileName).'TextID' => array(
                                                     'type' => 'INT',
                                                     'constraint' => 11,
                                                     'auto_increment' => TRUE 
                                                    
                                                     
                                              ),
                           ucfirst($FileName).'ID' => array(
                                                     'type' => 'INT',
                                                     'constraint' => '11',
                                              ),
                            'Title' => array(
                                                     'type' =>'VARCHAR',
                                                     'constraint' => '255',
                                                     
                                              ),
                            'SystemLanguageID' => array(
                                                     'type' => 'INT',
                                                    'constraint' => '11',
                                              ),
                            'CreatedAt' => array(
                                                     'type' => 'DATETIME',
                                                     
                                              ),
                            'UpdatedAt' => array(
                                                     'type' => 'DATETIME',
                                                     
                                              ), 
                            'CreatedBy' => array(
                                                     'type' => 'INT',
                                                     'constraint' => '11',
                                              ), 
                             'UpdatedBy' => array(
                                                     'type' => 'INT',
                                                     'constraint' => '11',
                                              )                  
                    );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key(ucfirst($FileName).'TextID', TRUE); 
            $this->dbforge->create_table(lcfirst($this->getPluralPrase($FileName,2).'_text'), TRUE);
            $this->db->query(add_foreign_key(lcfirst($this->getPluralPrase($FileName,2).'_text'), ucfirst($FileName).'ID', lcfirst($this->getPluralPrase($FileName,2)).'('.ucfirst($FileName).'ID'.')', 'CASCADE', 'CASCADE'));

        }

        if(isset($post_data['CreateModel']))
        {
            $path = APPPATH . "models/";

           // $this->dbforge->add_key(ucfirst($FileName).'ID', TRUE);
            $data = "<?php
                Class ".ucfirst($FileName)."_text_model extends Base_Model
                {
                    public function __construct()
                    {
                         parent::__construct(\"".lcfirst($this->getPluralPrase($FileName,2))."_text\");
                    }


                }";
            $result = write_file($path . ucfirst($FileName).'_text_model' . '.php', $data);
            return json_encode($result);
        }
    }
    
    
    private function writeManageFileInNewFolder($FileName,$ModuleID) {
        $UCaseClassname = ucfirst($FileName);
        $path = APPPATH . "views/backend/";
        if (!is_dir($path.$FileName)) {
            mkdir($path.$FileName, 0777, TRUE);
        }
        $newFilePath = $path.$FileName.'/';
        $data = "<div class=\"content\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"card\">
                    <div class=\"card-header card-header-icon\" data-background-color=\"purple\">
                        <i class=\"material-icons\">assignment</i>
                    </div>
                    <div class=\"card-content\">
                        <h4 class=\"card-title\"><?php echo lang(\$ControllerName.'s'); ?></h4>
                        <div class=\"toolbar\">
                            <a href=\"<?php echo base_url('cms/'.\$ControllerName.'/add');?>\">
                                <button type=\"button\" class=\"btn btn-primary waves-effect w-md waves-light m-b-5\"><?php echo lang('add_'.\$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class=\"material-datatables\">
                            <table id=\"datatables\" class=\"table table-striped table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">
                                <thead>
                                <tr>

                                    <th><?php echo lang('title');?></th>

                                    <th><?php echo lang('is_active');?></th>


                                    <?php if(checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if(\$results){
                                    foreach(\$results as \$value){ ?>
                                        <tr id=\"<?php echo \$value->".$UCaseClassname."ID;?>\">

                                            <td><?php echo \$value->Title; ?></td>


                                            <td><?php echo (\$value->IsActive ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanDelete')){?> 
                                            <td>
                                                <?php if(checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                    <a href=\"<?php echo base_url('cms/'.\$ControllerName.'/edit/'.\$value->".$UCaseClassname."ID);?>\" class=\"btn btn-simple btn-warning btn-icon edit\"><i class=\"material-icons\" title=\"Edit\">edit</i><div class=\"ripple-container\"></div></a>
                                                <?php } ?>
                                               
                                                <?php if(checkUserRightAccess($ModuleID,\$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href=\"javascript:void(0);\" onclick=\"deleteRecord('<?php echo \$value->".$UCaseClassname."ID;?>','cms/<?php echo \$ControllerName; ?>/action','')\" class=\"btn btn-simple btn-danger btn-icon remove\"><i class=\"material-icons\" title=\"Delete\">close</i><div class=\"ripple-container\"></div></a>
                                                <?php } ?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src=\"<?php echo base_url();?>assets/backend/js/datatable.js\"></script>";
        $result = write_file($newFilePath . 'manage' . '.php', $data);
        return json_encode($result);
    }
    
    
    
    
    private  function getPluralPrase($phrase,$value){
    $plural='';
    if($value>1){
        for($i=0;$i<strlen($phrase);$i++){
            if($i==strlen($phrase)-1){
                $plural.=($phrase[$i]=='y')? 'ies':(($phrase[$i]=='s'|| $phrase[$i]=='x' || $phrase[$i]=='z' || $phrase[$i]=='ch' || $phrase[$i]=='sh')? $phrase[$i].'es' :$phrase[$i].'s');
            }else{
                $plural.=$phrase[$i];
            }
        }
        return $plural;
    }
    return $phrase;
}


    public function getEntries(){

    $parent                             = $this->data['Parent_model'];
     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $orderChild = false;

     if ($columnName == 'Title') {
         $orderChild = true;
     }
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $ParentID = $request->get('customID');
    
     // Total records
     $where = ''.$this->data['Table'].'_text.SystemLanguageID = 1 AND modules.hide = 0';


     $totalRecords = $this->$parent->getRowsCountWithChild($this->data['TableKey'],$where);


    
    

    

     if($searchValue != ''){
        $where = $where. " AND (".$this->data['Table']."_text.Title like '%".$searchValue."%' OR ".$this->data['Table'].".Slug like '%".$searchValue."%')";
     }



     $totalRecordswithFilter = $this->$parent->getRowsCountWithChild($this->data['TableKey'],$where);// need to set here filer count
     $records =  $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language,$where,$columnSortOrder,$columnName,$orderChild,$rowperpage,$start);


     $data_arr = array();
     foreach ($records as $key => $value) {
        $action = '';


        


            $action .= '<a href="'.base_url('cms/' . $this->data['ControllerName'] . '/edit/' . $value->ModuleID).'"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons" title="Edit">dvr</i>
                                                            <div class="ripple-container"></div>
                                                        </a>';
        


             $action .= '<a href="javascript:void(0);"" onclick="deleteRecord('.$value->ModuleID.',\'cms/'.$this->data['ControllerName'].'/action\',\'\');" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>';
            
        
         

         
         $data_arr[] = array(
          "Title" => $value->Title,
          "Slug" => $value->Slug,
           "IsActive" => ($value->IsActive ? lang('yes') : lang('no')),
           "Action" => $action
        );
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }

}