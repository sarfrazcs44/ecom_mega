<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->data['language'] = $this->language;
    }


    public function index()
    {
        $this->data['view'] = 'backend/newsletter/manage';
        $this->data['results'] = get_mailchimp_subscribers();
        // dump($this->data['results']);
        $this->load->view('backend/layouts/default', $this->data);
    }

}