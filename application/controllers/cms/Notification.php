<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('User_model');
        $this->load->Model('User_text_model');
        $this->load->Model('City_model');
        $this->load->Model('Notification_cron_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';
    }

    public function index()
    {
        $this->data['users'] = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 3');
        $this->data['technicians'] = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 2');
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', 'Title', 'required');
        //$this->form_validation->set_rules('TitleAr', 'Title AR', 'required');
        $this->form_validation->set_rules('Message', 'Message', 'required');
        //$this->form_validation->set_rules('DescriptionAr', 'Description AR', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    public function sendNotification_bk()
    {
        $post_data = $this->input->post();
        $user_type = $post_data['UsersType'];
        $title = $post_data['Title'];
        $message = $post_data['Message'];
        $notification_type = $post_data['NotificationType'];
        $users = false;
        $where = '';

        if ($this->session->userdata['admin']['RoleID'] != 1){
            $where .= ' AND '.$this->data['Table'].'.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }


        if ($user_type == 'Stores') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 2'.$where); // Store Warehouse User RoleID = 2
        } elseif ($user_type == 'Drivers') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 3'.$where); // Driver User RoleID = 3
        } elseif ($user_type == 'Admins') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 4'.$where); // Store Admin User RoleID = 4
        } elseif ($user_type == 'Customers') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 5'.$where); // Customer User RoleID = 5
        }

        if ($users && count($users) > 0) {
            foreach ($users as $user) {
                if ($notification_type == 'SMS') {
                    if ($user->Mobile != '') {
                        $sms_text = $title . "\n" . $message;
                        // $sms_text = "Dear " . $user->FullName . "\n" . $message;
                        sendSms($user->Mobile, $sms_text);
                    }
                } elseif ($notification_type == 'Email') {
                    if ($user->Email != '') {
                        $data['to'] = $user->Email;
                        $data['subject'] = $title;
                        // $message = "Dear " . $user->FullName . ",<br>" . $message;
                        $data['message'] = email_format($message);
                        sendEmail($data);
                    }
                } elseif ($notification_type == 'Push'){
                    if ($user->DeviceToken != '') {
                        
                        sendNotification($title,$message,$data,$user->UserID);
                    }
            } 


            }
        }

        $success['error'] = false;
        $success['success'] = 'Notifications sent successfully';
        $success['redirect'] = false;
        echo json_encode($success);
        exit;
    }

    public function sendNotification()
    {
        $post_data = $this->input->post();
        $this->validate();
        $user_type = $post_data['NotificationTo'];
        $title = $post_data['Title'];
        $message = $post_data['Message'];
        $notification_type = $post_data['NotificationType'];
        $users = false;

        

        $where = '';

        if ($this->session->userdata['admin']['RoleID'] != 1){
            $where .= ' AND '.$this->data['Table'].'.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }


        if ($user_type == 'Stores') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 2'.$where); // Store Warehouse User RoleID = 2
            $RoleID= 2;
        } elseif ($user_type == 'Drivers') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 3'.$where); // Driver User RoleID = 3
            $RoleID= 3;
        } elseif ($user_type == 'Admins') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 4'.$where); // Store Admin User RoleID = 4
            $RoleID= 4;
        } elseif ($user_type == 'Customers') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 5'.$where); // Customer User RoleID = 5
            $RoleID= 5;
        }
        
        $save_cron_setting = array();

        if($this->session->userdata['admin']['RoleID'] != 1){

        $save_cron_setting['CompanyID'] = $this->session->userdata['admin']['CompanyID'];
        
        }

        $save_cron_setting['RoleID'] = $RoleID;
        $save_cron_setting['Title'] = $post_data['Title'];
        $save_cron_setting['Message'] = $post_data['Message'];
        //$save_cron_setting['DescriptionAr'] = $post_data['DescriptionAr'];
        $save_cron_setting['NotificationType'] = $post_data['NotificationType'];
        $save_cron_setting['TotalUser'] = count($users);
        $save_cron_setting['Page'] = 0;
        if($save_cron_setting['TotalUser'] > 0){

            $save_cron_setting['CreatedAt'] = date('Y-m-d H:i:s');
            $save_cron_setting['NotificationFrom'] = $this->session->userdata['admin']['UserID'];
            $other_data = array();
            

            
            $this->Notification_cron_model->save($save_cron_setting);

            

            
            //echo $this->db->last_query();exit;
            //print_rm($users);exit;



            


            $success['error'] = false;
            $success['success'] = 'Notification sent successfully';
            $success['redirect'] = false;
            echo json_encode($success);
            exit;

        }else{
            $success['error'] = 'No user found';
            $success['success'] = false;
            echo json_encode($success);
            exit;

        }
    }

}