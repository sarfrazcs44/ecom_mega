<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offer extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model',
            'Category_model',
            'Product_model',
            'Customer_group_model',
            'Offer_group_model',
            'Offer_user_notification_model',
            'Customer_group_member_model',
            'User_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'OfferID';
        $this->data['Table'] = 'offers';


    }


    public function index()
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['columns'] = "[
            { data: 'Title' },
            { data: 'ValidFrom' },
            { data: 'ValidTo' },
            { data: 'IsActive' },
            { data: 'Action' }
        ]";


      

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        $where = '';
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND products.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        if (!checkUserRightAccess(84, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['categories'] = $this->Category_model->getAllJoinedData(false, 'CategoryID', $this->language, 'categories.ParentID = 0');
        $this->data['products'] = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, 'products.IsActive = 1 AND products.IsCustomizedProduct = 0'.$where);
        $this->data['groups'] = $this->Customer_group_model->getAll();


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(84, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


        // $this->data['categories'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.ParentID = 0');

        //  $this->data['subcategories'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.ParentID = '.$this->data['result'][0]->CategoryID);


        $this->data['products'] = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, 'products.IsActive = 1 AND products.IsCustomizedProduct = 0');
        $this->data['c_groups'] = $this->Customer_group_model->getAll();
        $customer_groups = $this->Offer_group_model->getMultipleRows(array('OfferID' => $id));
        $this->data['customer_groups'] = array();
        if ($customer_groups) {
            $this->data['customer_groups'] = array_column($customer_groups, 'GroupID');
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');
        $this->form_validation->set_rules('ValidFrom', lang('FromDate'), 'required');
        $this->form_validation->set_rules('ValidTo', lang('ToDate'), 'required');
        $this->form_validation->set_rules('ProductID[]', lang('product'), 'required');

        $this->form_validation->set_rules('CustomerGroupID[]', lang('customer_groups'), 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {

        if (!checkUserRightAccess(84, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = array();
        $save_child_data = array();

        if($this->session->userdata['admin']['RoleID'] != 1){

        $save_parent_data['CompanyID'] = $this->session->userdata['admin']['CompanyID'];
        
        }

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);


        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }


        /*if ((isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] == '') || COUNT($_FILES['Image']['name']) > 7) {
            $errors['error'] = lang('please_choose_image');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;

        }*/


        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
        //$save_parent_data['IsFeatured'] = (isset($post_data['IsFeatured']) ? 1 : 0);
        //$save_parent_data['OutOfStock']       = (isset($post_data['OutOfStock']) ? 1 : 0 );
        $save_parent_data['ValidFrom'] = date('Y-m-d', strtotime($post_data['ValidFrom']));
        $save_parent_data['ValidTo'] = date('Y-m-d', strtotime($post_data['ValidTo']));
        $save_parent_data['ProductID'] = implode(',', $post_data['ProductID']);
        $save_parent_data['DiscountType'] = $post_data['DiscountType'];
        $save_parent_data['Discount'] = $post_data['Discount'];
        // $save_parent_data['CategoryID']            = $post_data['CategoryID'];
        // $save_parent_data['SubCategoryID']            = $post_data['SubCategoryID'];
        if ($this->input->post('CustomerGroupID')) {
                $groups = $post_data['CustomerGroupID'];
                if (!empty($groups)) {
                    if($groups[0] == 0){
                        $save_parent_data['IsForAll'] = 1;

                    }
                }
            }


        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];



        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {
            /* if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                 $this->uploadImage('Image', 'uploads/images/', $insert_id, 'offer', true);
             }*/

            if ($this->input->post('CustomerGroupID')) {
                $groups = $post_data['CustomerGroupID'];
                if (!empty($groups)) {
                    if($groups[0] != 0){
                        $save_group_data = array();
                        $save_user_n_data = array();
                        foreach ($groups as $value) {

                            $get_users = $this->Customer_group_member_model->getMultipleRows(array('CustomerGroupID' => $value));
                            if ($get_users) {
                                foreach ($get_users as $user) {
                                    $save_user_n_data[] = ['OfferID' => $insert_id, 'GroupID' => $value, 'UserID' => $user->UserID];
                                }
                            }

                            $save_group_data[] = ['OfferID' => $insert_id, 'GroupID' => $value];

                        }


                        $this->Offer_user_notification_model->insert_batch($save_user_n_data);
                        $this->Offer_group_model->insert_batch($save_group_data);

                    }else{

                            $get_users = $this->User_model->getMultipleRows(array('RoleID' => 5));
                            if ($get_users) {
                                foreach ($get_users as $user) {
                                    $save_user_n_data[] = ['OfferID' => base64_decode($post_data[$this->data['TableKey']]), 'GroupID' => 0, 'UserID' => $user->UserID];
                                }
                                $this->Offer_user_notification_model->insert_batch($save_user_n_data);
                            }

                    }

                    }
                    
            }


            // $default_lang = getDefaultLanguage();
            $system_languages = getSystemLanguages();
            foreach ($system_languages as $system_language) {


                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['Description'];
                //$save_child_data['Ingredients'] = $post_data['Ingredients'];
                // $save_child_data['Specifications'] = $post_data['Specifications'];
                // $save_child_data['Tags'] = $post_data['Tags'];
                // $save_child_data['Keywords'] = $post_data['Keywords'];

                // $save_child_data['MetaTags'] = $post_data['MetaTags'];
                // $save_child_data['MetaKeywords'] = $post_data['MetaKeywords'];
                // $save_child_data['MetaDescription'] = $post_data['MetaDescription'];
                $save_child_data[$this->data['TableKey']] = $insert_id;
                $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);
            }


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {


        if (!checkUserRightAccess(84, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_child_data = array();
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {
                /* if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                     $product_images = get_images($id, 'offer');

                     $total_images = COUNT($product_images) + COUNT($_FILES['Image']['name']);

                     if ($total_images > 7) {
                         $errors['error'] = lang('please_choose_image');
                         $errors['success'] = false;
                         echo json_encode($errors);
                         exit;
                     }


                 }*/


                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
                $save_parent_data['ValidFrom'] = date('Y-m-d', strtotime($post_data['ValidFrom']));
                $save_parent_data['ValidTo'] = date('Y-m-d', strtotime($post_data['ValidTo']));
                $save_parent_data['DiscountType'] = $post_data['DiscountType'];
                $save_parent_data['Discount'] = $post_data['Discount'];
                // $save_parent_data['IsFeatured'] = (isset($post_data['IsFeatured']) ? 1 : 0);
                // $save_parent_data['OutOfStock']       = (isset($post_data['OutOfStock']) ? 1 : 0 );
                //$save_parent_data['Price']            = $post_data['Price'];
                //  $save_parent_data['CategoryID']            = $post_data['CategoryID'];
                //  $save_parent_data['SubCategoryID']            = $post_data['SubCategoryID'];
                $save_parent_data['ProductID'] = implode(',', $post_data['ProductID']);
                /*if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {

                    if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                        $this->uploadImage('Image', 'uploads/images/', base64_decode($post_data['OfferID']), 'offer', true);
                    }
                }*/
                if ($this->input->post('CustomerGroupID')) {
                $groups = $post_data['CustomerGroupID'];
                if (!empty($groups)) {
                    if($groups[0] == 0){
                        $save_parent_data['IsForAll'] = 1;

                    }else{
                        $save_parent_data['IsForAll'] = 0;

                    }
                }
            }
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $this->Offer_group_model->delete(array('OfferID' => base64_decode($post_data[$this->data['TableKey']])));
                $this->Offer_user_notification_model->delete(array('OfferID' => base64_decode($post_data[$this->data['TableKey']])));
                if ($this->input->post('CustomerGroupID')) {
                    $groups = $post_data['CustomerGroupID'];
                    if (!empty($groups)) {
                        if($groups[0] != 0){
                        $save_group_data = array();
                        foreach ($groups as $value) {
                            $get_users = $this->Customer_group_member_model->getMultipleRows(array('CustomerGroupID' => $value));
                            if ($get_users) {
                                foreach ($get_users as $user) {
                                    $save_user_n_data[] = ['OfferID' => base64_decode($post_data[$this->data['TableKey']]), 'GroupID' => $value, 'UserID' => $user->UserID];
                                }
                            }

                            $save_group_data[] = ['OfferID' => base64_decode($post_data[$this->data['TableKey']]), 'GroupID' => $value];

                        }

                        $this->Offer_user_notification_model->insert_batch($save_user_n_data);
                        $this->Offer_group_model->insert_batch($save_group_data);

                    }else{

                            $get_users = $this->User_model->getMultipleRows(array('RoleID' => 5));

                            if ($get_users) {
                                foreach ($get_users as $user) {
                                    $save_user_n_data[] = ['OfferID' => base64_decode($post_data[$this->data['TableKey']]), 'GroupID' => 0, 'UserID' => $user->UserID];
                                }
                                $this->Offer_user_notification_model->insert_batch($save_user_n_data);

                            }

                    }
                }
                }


                $this->$parent->update($save_parent_data, $update_by);
                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['Description'];

                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);

            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Description'] = $post_data['Description'];


                    $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->update($save_child_data, $update_by);

                } else {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Description'] = $post_data['Description'];

                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->save($save_child_data);
                }


            }

            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(84, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }



     public function getEntries(){

     $parent                             = $this->data['Parent_model'];
     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $orderChild = false;

     if ($columnName == 'Title') {
         $orderChild = true;
     }
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $ParentID = $request->get('customID');
    
     // Total records
     $where = ''.$this->data['Table'].'_text.SystemLanguageID = 1';

    if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND '.$this->data['Table'].'.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
    }


     $totalRecords = $this->$parent->getRowsCountWithChild($this->data['TableKey'],$where);


    
    

    

     if($searchValue != ''){
        $where = $where. " AND (".$this->data['Table']."_text.Title like '%".$searchValue."%' OR ".$this->data['Table'].".ValidFrom like '%".$searchValue."%'  OR ".$this->data['Table'].".ValidTo like '%".$searchValue."%')";
     }



     $totalRecordswithFilter = $this->$parent->getRowsCountWithChild($this->data['TableKey'],$where);// need to set here filer count
     $records =  $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language,$where,$columnSortOrder,$columnName,$orderChild,$rowperpage,$start);


     $data_arr = array();
     foreach ($records as $key => $value) {
        $action = '';


        

        if (checkUserRightAccess(84, $this->session->userdata['admin']['UserID'], 'CanEdit')) { 

            $action .= '<a href="'.base_url('cms/' . $this->data['ControllerName'] . '/edit/' . $value->OfferID).'"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons" title="Edit">dvr</i>
                                                            <div class="ripple-container"></div>
                                                        </a>';
        }

        if (checkUserRightAccess(84, $this->session->userdata['admin']['UserID'], 'CanDelete')) { 

             $action .= '<a href="javascript:void(0);"" onclick="deleteRecord('.$value->OfferID.',\'cms/'.$this->data['ControllerName'].'/action\',\'\');" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>';
            
        }
         

         
         $data_arr[] = array(
          "Title" => $value->Title,
          "ValidFrom" => $value->ValidFrom,
          "ValidTo" => $value->ValidTo,
           "IsActive" => ($value->IsActive ? lang('yes') : lang('no')),
           "Action" => $action
        );
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }

}