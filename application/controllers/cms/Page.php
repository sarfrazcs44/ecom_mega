<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model',
            'User_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'PageID';
        $this->data['Table'] = 'pages';


    }


    public function index()
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['columns'] = "[
            { data: 'Title' },
            { data: 'IsActive' },
            { data: 'Action' }
        ]";


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');
        //print_rm($this->data['result']);

        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }





        if($this->data['result'][0]->Type == "About us")
        {
            $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit-about';
        } 
        elseif ($this->data['result'][0]->Type == "Contact us")
        {
            $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit-contact';
        }
        elseif ($this->data['result'][0]->Type == "Home") 
        {
            $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit-home';
        }
        else
        {
            $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        }



        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {

        if (!checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = array();
        $save_child_data = array();

        if($this->session->userdata['admin']['RoleID'] != 1){

        $save_parent_data['CompanyID'] = $this->session->userdata['admin']['CompanyID'];
        
        }

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }

        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $save_parent_data['Image'] = $this->uploadImage('Image', 'uploads/images/');
        }

        if (isset($_FILES['BannerImage']["name"][0]) && $_FILES['BannerImage']["name"][0] != '') {
            $save_parent_data['BannerImage'] = $this->uploadImage('BannerImage', 'uploads/images/');
        }

        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);


        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {


            // $default_lang = getDefaultLanguage();
            $system_languages = getSystemLanguages();
            foreach ($system_languages as $system_language) {


                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['Description'];
                if (isset($post_data['ShortDescription']))
                {
                    $save_child_data['ShortDescription'] = $post_data['ShortDescription'];
                }
                $save_child_data['MetaTags'] = $post_data['MetaTags'];
                $save_child_data['MetaKeywords'] = $post_data['MetaKeywords'];
                $save_child_data['MetaDescription'] = $post_data['MetaDescription'];
                $save_child_data[$this->data['TableKey']] = $insert_id;
                $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);
            }


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {


        if (!checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            $IsDefault = $post_data['IsDefault'];


            unset($post_data['form_type']);
            unset($post_data['IsDefault']);
            $save_parent_data = array();
            $save_child_data = array();

             $parents_fields = $this->Page_model->getFields();
                 foreach ($post_data as $key => $value) {
                    if (in_array($key, $parents_fields)) {
                        if($key != 'PageID'){
                             $save_parent_data[$key] = $value;
                        }
                       
                    } else {

                        if($key == 'SystemLanguageID'){

                            $save_child_data[$key] = base64_decode($value);
                        }else{
                             $save_child_data[$key] = $value;
                        }
                       
                    }

                }




                 $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                 $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];



            if ($IsDefault) {



                if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                    if($this->data['result'][0]->Type == 'About us'){
                         $this->uploadImage('Image', 'uploads/images/', base64_decode($post_data['PageID']), 'page', true);
                    }else{
                        $save_parent_data['Image'] = $this->uploadImage('Image', 'uploads/images/');
                    }
                    
                }

                if (isset($_FILES['BannerImage']["name"][0]) && $_FILES['BannerImage']["name"][0] != '') {
                    $save_parent_data['BannerImage'] = $this->uploadImage('BannerImage', 'uploads/images/');
                }

                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
                $save_parent_data['SectionOneActive'] = (isset($post_data['SectionOneActive']) ? 1 : 0);
                $save_parent_data['SectionTwoActive'] = (isset($post_data['SectionTwoActive']) ? 1 : 0);
                $save_parent_data['SectionThreeActive'] = (isset($post_data['SectionThreeActive']) ? 1 : 0);
                $save_parent_data['SectionFourActive'] = (isset($post_data['SectionFourActive']) ? 1 : 0);
                $save_parent_data['SectionFiveActive'] = (isset($post_data['SectionFiveActive']) ? 1 : 0);
                $save_parent_data['SectionSixActive'] = (isset($post_data['SectionSixActive']) ? 1 : 0);
                $save_parent_data['SectionSevenActive'] = (isset($post_data['SectionSevenActive']) ? 1 : 0);
                $save_parent_data['SectionEightActive'] = (isset($post_data['SectionEightActive']) ? 1 : 0);
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);






                $this->$parent->update($save_parent_data, $update_by);
               
                

                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);



                $this->$child->update($save_child_data, $update_by);

            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    

                   
                    $this->$child->update($save_child_data, $update_by);

                } else {

                   
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                   

                    $this->$child->save($save_child_data);
                }


            }

            // if terms and conditions page is being updated, PageID = 9
            if (isset($update_by['PageID']) && $update_by['PageID'] == 9)
            {
                // terms and condition accepted logic for user here, mark all users TermsAccepted 0
                $update_terms['TermsAccepted'] = 0;
                $update_terms_by['TermsAccepted'] = 1;
                $this->User_model->update($update_terms, $update_terms_by);
            }

            $success['error'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class().'/edit/'.base64_decode($post_data[$this->data['TableKey']]);
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


    public function getEntries(){

    $parent                             = $this->data['Parent_model'];
     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $orderChild = false;

     if ($columnName == 'Title') {
         $orderChild = true;
     }
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $ParentID = $request->get('customID');
    
     // Total records
     $where = 'pages_text.SystemLanguageID = 1 AND pages.hide = 0';

     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND '.$this->data['Table'].'.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }


     $totalRecords = $this->$parent->getRowsCountWithChild($this->data['TableKey'],$where);
     //print_rm($totalRecords);

    
    

    

     if($searchValue != ''){
        $where = $where. " AND (".$this->data['Table']."_text.Title like '%".$searchValue.")";
     }



     $totalRecordswithFilter = $this->$parent->getRowsCountWithChild($this->data['TableKey'],$where);// need to set here filer count
     $records =  $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language,$where,$columnSortOrder,$columnName,$orderChild,$rowperpage,$start);
     

     $data_arr = array();
     foreach ($records as $key => $value) {
        $action = '';


        

        if (checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanEdit')) { 

            $action .= '<a href="'.base_url('cms/' . $this->data['ControllerName'] . '/edit/' . $value->PageID).'"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons" title="Edit">dvr</i>
                                                            <div class="ripple-container"></div>
                                                        </a>';
        }

        if (checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanDelete')) { 

             $action .= '<a href="javascript:void(0);"" onclick="deleteRecord('.$value->PageID.',\'cms/'.$this->data['ControllerName'].'/action\',\'\');" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>';
            
        }

        if ($value->PageID == 14){
            $action .= '<a href="'.base_url('cms/faq').'"class="btn btn-simple btn-warning btn-icon edit">FAQ list</a>';
        }
         

         
         $data_arr[] = array(
          "Title" => $value->Title,
           "IsActive" => ($value->IsActive ? lang('yes') : lang('no')),
           "Action" => $action
        );
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }


}