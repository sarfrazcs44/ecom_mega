<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        checkModuleRights($this->uri->segment(3),32);
       
        $this->load->model('Order_model');
        $this->load->model('Pos_model');
        $this->load->model('Product_model');
        $this->load->model('Category_model');
        $this->load->model('Temp_order_model');
        $this->load->model('Site_setting_model');
        $this->load->model('User_model');
        $this->load->model('Order_item_model');
        $this->load->model('User_address_model');
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['language'] = $this->language;
    }


    public function index()
    {

        $this->data['view'] = 'backend/pos/manage';

        $this->data['columns'] = "[
            { data: 'Type' },
            { data: 'TransactionUser' },
            { data: 'Description' },
            { data: 'Action' },
            { data: 'Invoice' },
            { data: 'TotalAmount' }
        ]";

        $where = '1 = 1';

       if($this->session->userdata['admin']['RoleID'] != 1){
          $where .= ' AND point_of_sales.CompanyID ='.$this->session->userdata['admin']['CompanyID'];
       }

        if($this->input->post()){

            $post_data = $this->input->post();

            if(isset($post_data['From']) && $post_data['From'] != ''){
                $where .= ' AND DATE(CreatedAt) >= "'.date('Y-m-d',strtotime($post_data['From'])).'"';
            }
            if(isset($post_data['To']) && $post_data['To'] != ''){
                $where .= ' AND DATE(CreatedAt) <= "'.date('Y-m-d',strtotime($post_data['To'])).'"';
            }

            $this->data['post_data'] = $post_data;

        }else{

            $where .= ' AND Month(CreatedAt) = "'.date('m').'" AND Year(CreatedAt) = "'.date('Y').'"';

        }




        $this->data['results'] = $this->Pos_model->getData($where);
        //echo $this->db->last_query();exit;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        
        $this->data['view'] = 'backend/pos/add';
        $this->load->view('backend/layouts/default',$this->data);
    }
    
     public function cart()
    {
         

        $fetch_by = array();
        $fetch_by['is_active']       = 1;
        $where = false;
        $category_where = '1 = 1';
        
        if($this->session->userdata['admin']['CompanyID'] > 0){

          $category_where = 'categories.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
            
          
            //$fetch_by['CompanyID']       = $this->session->userdata['admin']['CompanyID'];
            $where = 'products.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
            //$this->data['products'] = $this->Model_product->getMultipleRows($fetch_by);
        }else{

            //$this->data['products'] = $this->Model_product->getAll();
        }


        $this->data['products'] = $this->Product_model->getAllProductsForBackend($fetch_by,40,0);


        $this->data['total_products']  = $this->Product_model->getCountProducts($where);

        $this->data['total_products']  = $this->data['total_products']->Total;




          
        $this->data['categories'] = $this->Category_model->getAllJoinedData(false, 'CategoryID', $this->language,$category_where);

       
       
        $this->data['cart_products']     = $this->Temp_order_model->getCartData($this->session->userdata['admin']['UserID']);


        $this->data['vat_setting'] = $this->Site_setting_model->get(1,false,'SiteSettingID');

        
        $this->data['view'] = 'backend/pos/cart';
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function fetch_products(){
        $post_data = $this->input->post();

        $fetch_by = array();
       
        if(isset($post_data['category_id']) && $post_data['category_id'] != ''){
            $fetch_by['CategoryID']       = $post_data['category_id'];
        }
        if(isset($post_data['sub_category_id']) && $post_data['sub_category_id'] != ''){
            $fetch_by['SubCategoryID']       = $post_data['sub_category_id'];
        }

        if($this->session->userdata['admin']['CompanyID'] > 0){

         
            
          
            $fetch_by['CompanyID']       = $this->session->userdata['admin']['CompanyID'];
            
        }

        $limit = 40;

        $start_index = $limit * $post_data['page'];

        $products      = $this->Product_model->getAllProductsForBackend($fetch_by,$limit,$start_index);


        $html = '';

        $success = array();

        $success['show_load_more']   = false;

        if($products){ 
            foreach($products as $product){ 
                  $product_image = get_images($product['ProductID'],'product',false);
                   $image = base_url()."uploads/product_images/dummy-img.png";    
                   if($product_image)
                   {
                        if(file_exists($product_image)){
                                            
                           $image = base_url().$product_image;
                        }
                   }


                    

                 $html .= '<div class="col-md-2"><div class="singleProduct position-relative text-center search_product">
                                        <div class="dropdown">
                                            <a class="px-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v text-secondary"></i>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a class="dropdown-item" href="'.base_url('cms/product/edit/'.$product['ProductID']).'">View</a>
                                                <a class="dropdown-item" href="javascript:void(0);" onclick="deleteRecord("'.$product['ProductID'].'","cms/product/action","")">Delete</a>
                                            </div>
                                        </div>
                                        <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="'.$image.'" alt="product" height="83" width="83" ></div>
                                        <p class="m-0 text-danger bigger text-truncate" title="'.$product['CategoryTitle'].'">'.$product['CategoryTitle'].'</p>
                                        <p class="text-dark my-1">'.$product['Title'].'</p>
                                        <p class="m-0 text-success bigger">'.$product['Price'].' SAR</p>
                                        <div class="text-center"><input type="button" class="add_to_cart btn btn-primary" data-product-id="'.$product['ProductID'].'" value="Add to cart"></div>
                                    </div> </div>';

             }  

             if(count($products) == $limit){
                $success['show_load_more']   = true;
            }

         }

        


         $success['html']   = $html;
         echo json_encode($success);
         exit;

        

    }


    public function add_to_cart(){
        
        $data = array();
        $vat_setting = $this->Site_setting_model->get(1,false,'SiteSettingID');

        $post_data = $this->input->post();
        $post_data['Quantity'] = 1;


        


        $fetch_by = array();
        $fetch_by['UserID'] = $post_data['UserID'] = $this->session->userdata['admin']['UserID'];
        $fetch_by['ProductID'] = $this->input->post('ProductID');

        $already_added = $this->Temp_order_model->getWithMultipleFields($fetch_by);
        if ($already_added) {
            $update = array();
            $update_by = array();

            $update['Quantity'] = $already_added->Quantity + $post_data['Quantity'];
            $update_by['TempOrderID'] = $already_added->TempOrderID;

            $this->Temp_order_model->update($update, $update_by);


            $insert_id = $already_added->TempOrderID;

        } else {
            $insert_id = $this->Temp_order_model->save($post_data);
        }


        


        if ($insert_id > 0) {


            $cart_products     = $this->Temp_order_model->getCartData($this->session->userdata['admin']['UserID']);

            $html = '';
            $total = 0;
            $pop_up_html = '';


            



            $html .='<div class="haveScroll slimscrollleft" style="max-height:520px;">
                  <ul class="m-0 p-0">';
                              
                              if($cart_products){
                                       foreach ($cart_products as $key => $value) { 
                                          $total += $value['Price'] * $value['Quantity'];
                                          $product_image = get_images($value['ProductID'],'product',false);
                                           $image = base_url()."uploads/product_images/dummy-img.png";    
                                           if($product_image)
                                           {
                                                if(file_exists($product_image)){
                                                                    
                                                   $image = base_url().$product_image;
                                                }
                                           }
                                           
                    $html .= '<li class="d-flex py-2">
                        <div class="imgBox mr-3"><img class="rounded" src="'.$image.'" alt="product" height="45" width="45" ></div>
                        <div class="text">
                           <p class="m-0 smallFontSize lineHeight1-3">
                              <span class="text-danger d-block w-100 strong bold">dsfdsa</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">'.$value['Title'].'</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">Price per unit '.$value['Price'].' SAR</span>
                           </p>
                        </div>
                        <div class="dropdown  dropleft">
                           <a class="px-2" href="#" role="button" id="deleteCart_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-ellipsis-v text-secondary"></i>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="deleteCart_1" style="width:auto;">
                              <a class="dropdown-item w-100 delete_cart" href="javascript:void(0);" data-temp-order-id = "'.$value['TempOrderID'].'">Delete</a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="d-flex justify-content-between align-items-center mt-2 mb-4 px-1 pb-2 plusMinusPrice">
                           <div class="form-group mb-0 mr-3">
                              <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected position-relative">



                                 <span class="input-group-btn input-group-prepend">
                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto left-0 largeFontSize bootstrap-touchspin-down btn-minus" data-temp-order-id="'.$value['TempOrderID'].'" data-product-id="'.$value['ProductID'].'" type="button"><i class="typcn typcn-minus"></i></button>
                                 </span>


                                 <input id="demo0" type="text"  name="quantity" value="'.$value['Quantity'].'"  data-bts-min="0" data-bts-max="100" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-primary" data-bts-button-up-class="btn btn-primary" class="form-control text-center strong bold largeFontSize text-dark bg-light px-4 quantity'.$value['ProductID'].'">
                                 <span class="input-group-btn input-group-append">



                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto right-0 largeFontSize bootstrap-touchspin-up btn-plus" data-temp-order-id="'.$value['TempOrderID'].'" data-product-id="'.$value['ProductID'].'" type="button"><i class="typcn typcn-plus"></i></button>
                                 </span>
                              </div>
                           </div>
                           <div class="shadow bg-white rounded-pill text-center text-success">
                              <p class="m-0 ">
                                    <span class="d-block largeFontSize w-100 strong bold">'.($value['Price'] * $value['Quantity']).'</span>
                                    <span class="d-block smallerFontSize  w-100 ">SAR</span>
                              </p>
                           </div>
                        </div>
                        
                     </li>';
                     } 

                      $html .= '</ul>
               </div><div class="row">
                  <div class="tbody card-box shadow w-100 m-0 px-3">
                     <div class="px-3">
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$total.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center border-bottom border-success">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Tax (VAT 15%)</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$vat_setting->Vat.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>Grand Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>'.($vat_setting->Vat + $total).' SAR</strong></p></div>
                        </div>
                     </div>
                     <button type="button" class="btn btn-primary waves-effect w-md waves-light mb-0 mt-2 w-100 get_popup" >Check Out</button>
                  </div>
               </div>';



               $pop_up_html .= '<div class="epmtyClass">
                              <div class="">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Cashier </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.$this->session->userdata['admin']['FullName'].'</b></span>
                                 </p>
                              </div>
                              
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Items </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.count($cart_products).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Purchased on</b></span>
                                    <span class="text-dark largeFontSize  d-block w-100 "><b>'.(date('d, M Y')).' </b></span>
                                    <span class="text-dark  d-block w-100"><b>'.(date('H:m:i')).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-4">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Bill</b></span>
                                 <table class="w-100">
                                    <tr>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-dark  d-block w-100"><b>Total</b></span>
                                                <span class="text-dark  d-block w-100"><b>Tax (VAT 15%)</b></span>
                                                <span class="text-dark  d-block w-100"><b>Grand Total</b></span>
                                             </p>
                                          </div>
                                       </td>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-success  d-block w-100"><b>'.$total.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.$vat_setting->Vat.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.($vat_setting->Vat + $total).' SAR</b></span>
                                             </p>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                                 </p>
                              </div>
                           </div>';



                 }


                    




               



               


            



            $success['html']   = $html;
            $success['pop_up_html']   = $pop_up_html;
            echo json_encode($success);
            exit;


            


        } else {


        $errors['error'] = 'There is something went wrong';
        $errors['success'] = 'false';
        echo json_encode($errors);
        exit;


        }

        
    }



    public function get_popup_detail(){

        $data = array();
        $vat_setting = $this->Site_setting_model->get(1,false,'SiteSettingID');
        $post_data = $this->input->post();

        

       


        


            $cart_products     = $this->Temp_order_model->getCartData($this->session->userdata['admin']['UserID']);

            
            $total = 0;
            $pop_up_html = '';


            



            
                              
                              if($cart_products){
                                       foreach ($cart_products as $key => $value) { 
                                          $total += $value['Price'] * $value['Quantity'];
                                          $product_image = get_images($value['ProductID'],'product',false);
                                           $image = base_url()."uploads/product_images/dummy-img.png";    
                                           if($product_image)
                                           {
                                                if(file_exists($product_image)){
                                                                    
                                                   $image = base_url().$product_image;
                                                }
                                           }
                                           
                    
                                }


               $pop_up_html .= '<div class="wrap">
                              <div class="">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Cashier </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.$this->session->userdata['admin']['FullName'].'</b></span>
                                 </p>
                              </div>
                              
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Items </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.count($cart_products).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Purchased on</b></span>
                                    <span class="text-dark largeFontSize  d-block w-100 "><b>'.(date('d, M Y')).' </b></span>
                                    <span class="text-dark  d-block w-100"><b>'.(date('H:m:i')).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-4">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Bill</b></span>
                                 <table class="w-100">
                                    <tr>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-dark  d-block w-100"><b>Total</b></span>
                                                <span class="text-dark  d-block w-100"><b>Tax (VAT 15%)</b></span>
                                                <span class="text-dark  d-block w-100"><b>Grand Total</b></span>
                                             </p>
                                          </div>
                                       </td>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-success  d-block w-100"><b>'.$total.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.$vat_setting->Vat.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.($vat_setting->Vat + $total).' SAR</b></span>
                                             </p>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                                 </p>
                              </div>
                           </div>';



                 

             }


                    




               



               


            



           
            $success['pop_up_html']   = $pop_up_html;
            echo json_encode($success);
            exit;


            


        

    }


    public function update_cart(){

        $data = array();
        $vat_setting = $this->Site_setting_model->get(1,false,'SiteSettingID');
        $post_data = $this->input->post();

        $update = array();
        $update_by = array();
        $fetch_by = array();

        //$update['product_quantity'] = $post_data['product_quantity'];
        $update_by['TempOrderID'] = $post_data['temp_order_id'];
        $fetch_by['TempOrderID'] = $post_data['temp_order_id'];

        
        $update['Quantity'] = $post_data['product_quantity'];

        $this->Temp_order_model->update($update, $update_by);

       


        if ($post_data['TempOrderID'] > 0) {


            $cart_products     = $this->Temp_order_model->getCartData($this->session->userdata['admin']['UserID']);

            $html = '';
            $total = 0;
            $pop_up_html = '';


            



            $html .='<div class="haveScroll slimscrollleft" style="max-height:520px;">
                  <ul class="m-0 p-0">';
                              
                              if($cart_products){
                                       foreach ($cart_products as $key => $value) { 
                                          $total += $value['Price'] * $value['Quantity'];
                                          $product_image = get_images($value['ProductID'],'product',false);
                                           $image = base_url()."uploads/product_images/dummy-img.png";    
                                           if($product_image)
                                           {
                                                if(file_exists($product_image)){
                                                                    
                                                   $image = base_url().$product_image;
                                                }
                                           }
                                           
                    $html .= '<li class="d-flex py-2">
                        <div class="imgBox mr-3"><img class="rounded" src="'.$image.'" alt="product" height="45" width="45" ></div>
                        <div class="text">
                           <p class="m-0 smallFontSize lineHeight1-3">
                              
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">'.$value['Title'].'</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">Price per unit '.$value['Price'].' SAR</span>
                           </p>
                        </div>
                        <div class="dropdown  dropleft">
                           <a class="px-2" href="#" role="button" id="deleteCart_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-ellipsis-v text-secondary"></i>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="deleteCart_1" style="width:auto;">
                              <a class="dropdown-item w-100 delete_cart" href="javascript:void(0);" data-temp-order-id = "'.$value['TempOrderID'].'">Delete</a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="d-flex justify-content-between align-items-center mt-2 mb-4 px-1 pb-2 plusMinusPrice">
                           <div class="form-group mb-0 mr-3">
                              <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected position-relative">



                                 <span class="input-group-btn input-group-prepend">
                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto left-0 largeFontSize bootstrap-touchspin-down btn-minus" data-temp-order-id="'.$value['TempOrderID'].'" data-product-id="'.$value['ProductID'].'" type="button"><i class="typcn typcn-minus"></i></button>
                                 </span>


                                 <input id="demo0" type="text"  name="quantity" value="'.$value['Quantity'].'"  data-bts-min="0" data-bts-max="100" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-primary" data-bts-button-up-class="btn btn-primary" class="form-control text-center strong bold largeFontSize text-dark bg-light px-4 quantity'.$value['ProductID'].'">
                                 <span class="input-group-btn input-group-append">



                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto right-0 largeFontSize bootstrap-touchspin-up btn-plus" data-temp-order-id="'.$value['TempOrderID'].'" data-product-id="'.$value['ProductID'].'" type="button"><i class="typcn typcn-plus"></i></button>
                                 </span>
                              </div>
                           </div>
                           <div class="shadow bg-white rounded-pill text-center text-success">
                              <p class="m-0 ">
                                    <span class="d-block largeFontSize w-100 strong bold">'.($value['Price'] * $value['Quantity']).'</span>
                                    <span class="d-block smallerFontSize  w-100 ">SAR</span>
                              </p>
                           </div>
                        </div>
                        
                     </li>';
                     } 

                      $html .= '</ul>
               </div><div class="row">
                  <div class="tbody card-box shadow w-100 m-0 px-3">
                     <div class="px-3">
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$total.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center border-bottom border-success">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Tax (VAT 15%)</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$vat_setting->Vat.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>Grand Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>'.($vat_setting->Vat + $total).' SAR</strong></p></div>
                        </div>
                     </div>
                     <button type="button" class="btn btn-primary waves-effect w-md waves-light mb-0 mt-2 w-100 get_popup">Check Out</button>
                  </div>
               </div>';



               $pop_up_html .= '<div class="w-100">
                              <div class="">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Cashier </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.$this->session->userdata['admin']['FullName'].'</b></span>
                                 </p>
                              </div>
                              
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Items </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.count($cart_products).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Purchased on</b></span>
                                    <span class="text-dark largeFontSize  d-block w-100 "><b>'.(date('d, M Y')).' </b></span>
                                    <span class="text-dark  d-block w-100"><b>'.(date('H:m:i')).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-4">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Bill</b></span>
                                 <table class="w-100">
                                    <tr>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-dark  d-block w-100"><b>Total</b></span>
                                                <span class="text-dark  d-block w-100"><b>Tax (VAT 15%)</b></span>
                                                <span class="text-dark  d-block w-100"><b>Grand Total</b></span>
                                             </p>
                                          </div>
                                       </td>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-success  d-block w-100"><b>'.$total.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.$vat_setting->Vat.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.($vat_setting->Vat + $total).' SAR</b></span>
                                             </p>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                                 </p>
                              </div>
                           </div>';



                 }


                    




               



               


            



            $success['html']   = $html;
            $success['pop_up_html']   = $pop_up_html;
            echo json_encode($success);
            exit;


            


        } else {


        $errors['error'] = 'There is something went wrong';
        $errors['success'] = 'false';
        echo json_encode($errors);
        exit;


        }

    }

    public function delete_cart_item(){
        $vat_setting = $this->Site_setting_model->get(1,false,'SiteSettingID');
        $post_data = $this->input->post();
        

        $deleted_by = array();
        $deleted_by['TempOrderID'] = $post_data['temp_order_id'];
        $this->Temp_order_model->delete($deleted_by);


        $cart_products     = $this->Temp_order_model->getCartData($this->session->userdata['admin']['UserID']);

            $html = '';
            $total = 0;
            $pop_up_html = '';


            



            $html .='<div class="haveScroll slimscrollleft" style="max-height:520px;">
                  <ul class="m-0 p-0">';
                              
                              if($cart_products){
                                       foreach ($cart_products as $key => $value) { 
                                          $total += $value['Price'] * $value['Quantity'];
                                          $product_image = get_images($value['ProductID'],'product',false);
                                           $image = base_url()."uploads/product_images/dummy-img.png";    
                                           if($product_image)
                                           {
                                                if(file_exists($product_image)){
                                                                    
                                                   $image = base_url().$product_image;
                                                }
                                           }
                                           
                    $html .= '<li class="d-flex py-2">
                        <div class="imgBox mr-3"><img class="rounded" src="'.$image.'" alt="product" height="45" width="45" ></div>
                        <div class="text">
                           <p class="m-0 smallFontSize lineHeight1-3">
                              
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">'.$value['Title'].'</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">Price per unit '.$value['Price'].' SAR</span>
                           </p>
                        </div>
                        <div class="dropdown  dropleft">
                           <a class="px-2" href="#" role="button" id="deleteCart_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-ellipsis-v text-secondary"></i>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="deleteCart_1" style="width:auto;">
                              <a class="dropdown-item w-100 delete_cart" href="javascript:void(0);" data-temp-order-id = "'.$value['TempOrderID'].'">Delete</a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="d-flex justify-content-between align-items-center mt-2 mb-4 px-1 pb-2 plusMinusPrice">
                           <div class="form-group mb-0 mr-3">
                              <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected position-relative">



                                 <span class="input-group-btn input-group-prepend">
                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto left-0 largeFontSize bootstrap-touchspin-down btn-minus" data-temp-order-id="'.$value['TempOrderID'].'" data-product-id="'.$value['ProductID'].'" type="button"><i class="typcn typcn-minus"></i></button>
                                 </span>


                                 <input id="demo0" type="text"  name="quantity" value="'.$value['Quantity'].'"  data-bts-min="0" data-bts-max="100" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-primary" data-bts-button-up-class="btn btn-primary" class="form-control text-center strong bold largeFontSize text-dark bg-light px-4 quantity'.$value['ProductID'].'">
                                 <span class="input-group-btn input-group-append">



                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto right-0 largeFontSize bootstrap-touchspin-up btn-plus" data-temp-order-id="'.$value['TempOrderID'].'" data-product-id="'.$value['ProductID'].'" type="button"><i class="typcn typcn-plus"></i></button>
                                 </span>
                              </div>
                           </div>
                           <div class="shadow bg-white rounded-pill text-center text-success">
                              <p class="m-0 ">
                                    <span class="d-block largeFontSize w-100 strong bold">'.($value['Price'] * $value['Quantity']).'</span>
                                    <span class="d-block smallerFontSize  w-100 ">SAR</span>
                              </p>
                           </div>
                        </div>
                        
                     </li>';
                     } 

                      $html .= '</ul>
               </div><div class="row">
                  <div class="tbody card-box shadow w-100 m-0 px-3">
                     <div class="px-3">
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$total.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center border-bottom border-success">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Tax (VAT 15%)</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$vat_setting->Vat.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>Grand Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>'.($vat_setting->Vat + $total).' SAR</strong></p></div>
                        </div>
                     </div>
                     <button type="button" class="btn btn-primary waves-effect w-md waves-light mb-0 mt-2 w-100 get_popup" >Check Out</button>
                  </div>
               </div>';



               $pop_up_html .= '<div class="w-100">
                              <div class="">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Cashier </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.$this->session->userdata['admin']['FullName'].'</b></span>
                                 </p>
                              </div>
                              
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Items </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.count($cart_products).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Purchased on</b></span>
                                    <span class="text-dark largeFontSize  d-block w-100 "><b>'.(date('d, M Y')).' </b></span>
                                    <span class="text-dark  d-block w-100"><b>'.(date('H:m:i')).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-4">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Bill</b></span>
                                 <table class="w-100">
                                    <tr>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-dark  d-block w-100"><b>Total</b></span>
                                                <span class="text-dark  d-block w-100"><b>Tax (VAT 15%)</b></span>
                                                <span class="text-dark  d-block w-100"><b>Grand Total</b></span>
                                             </p>
                                          </div>
                                       </td>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-success  d-block w-100"><b>'.$total.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.$vat_setting->Vat.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.($vat_setting->Vat + $total).' SAR</b></span>
                                             </p>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                                 </p>
                              </div>
                           </div>';



                 }


                    




               



               


            



            $success['html']   = $html;
            $success['pop_up_html']   = $pop_up_html;
            echo json_encode($success);
            exit;





    }


    public function place_order(){
        $data = array();

        $post_data = $this->input->post();
        $vat_setting = $this->Site_setting_model->get(1,false,'SiteSettingID');


        


           $get_user_data = $this->User_address_model->getAddresses('users.Mobile = "'.$post_data['mobile'].'"');
           if($get_user_data){


                $post_data['UserID'] = $get_user_data[0]->UserID;
                $address_id = $get_user_data[0]->AddressID;
                $user_name = $get_user_data[0]->FullName;
                if($address_id == ''){
                    $address_id = -1;
                }
           }else{
                $post_data['UserID'] = -1;
                $address_id = -1;
                $user_name = 'Guest';

           }


            

            $cart_products = $this->Temp_order_model->getCartData($this->session->userdata['admin']['UserID']);


            if ($cart_products) {
               

                

                 

                
                $order_data = array();

                $order_data['UserID'] = $post_data['UserID'];
                $order_data['AddressID'] = $address_id;
                $order_data['CreatedAt']  = date('Y-m-d H:i:s');
               
                $order_data['CollectFromStore'] = 1;

                $order_data['IsPosOrder'] = 1;
                $order_data['Status'] = 4;
                $order_data['Description'] = $post_data['additional_note'];

                $order_data['TotalTaxAmount'] = $vat_setting->Vat;
                $order_data['TotalShippingCharges'] = 0;
               // $order_data['total_items'] = count($cart_products);
                $order_data['TotalAmount'] = 0;
                $order_data['PaymentMethod'] = 'COD';
                
                
                $insert_id = $this->Order_model->save($order_data);
                if ($insert_id > 0) {
                    
                    $post_data['TotalAmount'] = 0;
                    $return_array = array();
                    $return_array = $this->Order_model->get($insert_id, true, 'OrderID');
                    $order_item_data = array();
                    foreach ($cart_products as $product) {
                    $post_data['TotalAmount'] = $post_data['TotalAmount'] + ($product['Price'] * $product['Quantity']);

                        $order_item_data[] = ['OrderID' => $insert_id,
                            'ProductID' => $product['ProductID'],
                            'Quantity' => $product['Quantity'],
                            'Amount' => $product['Price']
                        ];

                    }

                    $post_data['TotalAmount'] =  $post_data['TotalAmount'] + $vat_setting->Vat;

                    $this->Order_item_model->insert_batch($order_item_data);


                    $update_order = array();
                    $update_order_by = array();
                    $update_order_by['OrderID'] = $insert_id;
                   // $update_order['order_track_id'] = $order_data['order_track_id'] . $insert_id;
                    $update_order['OrderNumber'] = str_pad($insert_id, 5, '0', STR_PAD_LEFT);

                    $update_order['TotalAmount'] = $post_data['TotalAmount'];
                    $this->Order_model->update($update_order, $update_order_by);
                    




                    
                    $deleted_by = array();
                    $deleted_by['UserID'] = $this->session->userdata['admin']['UserID'];
                    $this->Temp_order_model->delete($deleted_by);

                   

                    // pos entry
                    //$user_data = $this->Model_user->get($post_data['user_id'],false,'user_id');
                    $pos_data = array();
                    $pos_data['TransactionUser'] = $user_name;
                    $pos_data['Type'] = 'Credit';
                    $pos_data['Description'] = 'Pos Purchase';
                    $pos_data['TotalAmount'] = $post_data['TotalAmount'];
                    $pos_data['CreatedAt'] = date('Y-m-d H:i');
                    $pos_data['OrderID'] = $insert_id;
                    $pos_data['CompanyID'] = $this->session->userdata['admin']['CompanyID'];
                    
                    $this->Pos_model->save($pos_data);


                    $success['error']   = 'false';
                    $success['success'] = 'Order Place Successfully';
                    $success['reload'] = true;
                    
                    
                    echo json_encode($success);
                    exit;

                    

                } else {
                    $errors['error'] = 'There is something went wrong';
                    $errors['success'] = 'false';
                    echo json_encode($errors);
                    exit;

                   
                }


            } else {

                $errors['error'] = 'There is something went wrong';
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
            }

        
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'save';
                $this->validate();
                $this->save();
          break; 
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('TransactionUser', 'Done By', 'required');
        $this->form_validation->set_rules('TotalAmount', 'Total Amount', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        $post_data = $this->input->post();
    
        
        
        unset($post_data['form_type']);
        $post_data['CreatedAt']    = date('Y-m-d H:i:s');      
        
        if (isset($_FILES['image']["name"][0]) && $_FILES['image']["name"][0] != '') {
            $post_data['Invoice']         = $this->uploadImage("image", "uploads/pos/");
        }
        $post_data['CompanyID'] = $this->session->userdata['admin']['CompanyID'];
        $insert_id = $this->Pos_model->save($post_data);
        if($insert_id > 0)
        {
            
            $success['error']   = false;
            $success['success'] = 'Save Successfully';
            $success['redirect'] = true;
            $success['url'] = 'cms/pos'; 
            
            
            echo json_encode($success);
            exit;
            
            
        }else
        {
            $errors['error'] = 'There is something went wrong';
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }


    /*private function uploadImage($file_key, $path,$id=false,$multiple = false)
    {


       
          $data = array();
         $extension=array("jpeg","jpg","png","gif");
         foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
            {
                $file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
                $file_tmp=$_FILES[$file_key]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                if(in_array($ext,$extension))
                {
                       
                        move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
                        if(!$multiple){
                            return $path.$file_name;
                        }
                        /*$data['DestinationID'] = $id; 
                        $data['ImagePath'] = $path.$file_name; 
                        $this->Model_destination_image->save($data);*/
                    
                /*}
               
            }
            return true;


    }*/

     private function delete(){
        
        $get_data = $this->Pos_model->get($this->input->post('id'),false,'PointOfSaleID');
        if(file_exists($get_data->Invoice)) {
              unlink($get_data->Invoice);
        }
      
        $deleted_by = array();
        $deleted_by['PointOfSaleID'] = $this->input->post('id');
        $this->Pos_model->delete($deleted_by);
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }


    public function getEntries(){

     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
    
     // Total records
     $where = '1 = 1';

     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND point_of_sales.CompanyID ='.$this->session->userdata['admin']['CompanyID'];
     }


     $totalRecords = $this->Pos_model->getDataCount($where);


    
    

    

     if($searchValue != ''){
        $where = $where. " AND (point_of_sales.Type like '%".$searchValue."%' OR point_of_sales.Description like '%".$searchValue."%' OR point_of_sales.TransactionUser like '%".$searchValue."%' OR point_of_sales.TotalAmount like '%".$searchValue."%')";
     }



     $totalRecordswithFilter = $this->Pos_model->getDataCount($where);// need to set here filer count
     $records =  $this->Pos_model->getData($where,$rowperpage,$start,'DESC');


     $data_arr = array();
     foreach ($records as $key => $value) {
        //print_rm($records);
        $action = '';
        $invoice = '';

        if($value->OrderID > 0) {
            $invoice .= '<a href="'.base_url('cms/orders/invoice/'.$value->OrderID).'" target="_blank">
                                                        <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>';
        }else{
            $invoice .= '<a href="'.base_url($value->Invoice).'" target="_blank">
                                                        <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>';
        }

        

        if ($value->Type == 'Debit') { 

            $action .= '<a href="javascript:void(0);"
                                                           class="btn btn-simple btn-warning btn-icon edit"
                                                           onclick="deleteRecord('.$value->PointOfSaleID.',\'cms/'.$this->data['ControllerName'].'/action\',\'\')"><button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button>
                                                        </a>';
        }

        
         

         
         $data_arr[] = array(
          "Type" => $value->Type,
          "TransactionUser" => $value->TransactionUser,
          "Description" => $value->Description,
          "Action" => $action,
          "Invoice" => $invoice,
          "TotalAmount" => $value->TotalAmount
        );
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }

    


}