<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();
        $this->load->model('Product_model');
        $this->load->model('Order_model');
        $this->load->model('User_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();


    }

    public function index()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        //$brandss= $this->Product_model->getSalesByBrand();
        //echo $this->db->last_query();
        $this->data['view'] = 'backend/report/reports_sections';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function best_sellers()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getProducts('products.PurchaseCount > 0', 'EN', false, 0, $sort_by = 'products.PurchaseCount', $sort_as = 'DESC');
        $this->data['view'] = 'backend/report/best_sellers';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function products_in_cart()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $where = '';
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'products.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Product_model->getProductsInCart($where);
        //echo $this->db->last_query();
        //print_rm($this->data['results']);
        $this->data['view'] = 'backend/report/products_in_cart';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function products_reviews()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $where = '';
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'product_reviews.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Product_model->getProductsReviews($where);
        
        $this->data['view'] = 'backend/report/products_reviews';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function product_reviews($product_id)
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getReviews('products.ProductID = '.$product_id);
        
        $this->data['view'] = 'backend/report/products_reviews_detail';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function customers_reviews_count()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $where = '';
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'product_reviews.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Product_model->getCountCustomerReviews($where);
        //print_rm($this->data['results']);
        
        $this->data['view'] = 'backend/report/customers_reviews';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function customer_reviews($user_id)
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getReviews('product_reviews.UserID = '.$user_id);
        
        $this->data['view'] = 'backend/report/customer_reviews_detail';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function out_of_stock()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getProducts("products.OutOfStock = 1");
        $this->data['view'] = 'backend/report/out_of_stock';
        $this->load->view('backend/layouts/default', $this->data);
    }


     public function customer_orders()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";
            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }
        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
        }else{
            $this->data['Interval'] = 'Year';
        }
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'orders.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Order_model->getTotalOrdersOfCustomer($where,$this->data['Interval']);
        //echo $this->db->last_query();exit;
       // echo $this->data['Interval'];exit;

        $this->data['view'] = 'backend/report/customer_orders';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function new_account_orders()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "WHERE DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            if($this->data['Interval'] == 'Year'){
                $group_by = 'GROUP BY Year(orders.CreatedAt)';
            }else if($this->data['Interval'] == 'Month'){
                $group_by = 'GROUP BY Month(orders.CreatedAt),Year(orders.CreatedAt)';
            }else if($this->data['Interval'] == 'Day'){
                $group_by = 'GROUP BY Day(orders.CreatedAt),Month(orders.CreatedAt),Year(orders.CreatedAt)';
            }

        }else{
            $this->data['Interval'] = 'Year';
            $group_by = 'GROUP BY Year(orders.CreatedAt)';
        }
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'AND orders.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Order_model->getTotalNewAccountOrdersOfCustomers($where,$group_by);
        //print_rm($this->data['results']);
       //echo $this->db->last_query();exit;
       // echo $this->data['Interval'];exit;

        $this->data['view'] = 'backend/report/customer_new_account_orders';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function orders()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'orders.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Order_model->getOrdersReport($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/orders';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function tax()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'orders.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Order_model->getOrdersReport($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/tax';
        $this->load->view('backend/layouts/default', $this->data);
    }

     public function shipping()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'orders.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Order_model->getOrdersReport($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/shipping';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function coupons()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'orders.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Order_model->getCouponReport($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/coupons';
        $this->load->view('backend/layouts/default', $this->data);
    }

    


    public function order_products()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= 'orders.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
        }
        $this->data['results'] = $this->Order_model->getProductOrderCount($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/order_products';
        $this->load->view('backend/layouts/default', $this->data);
    }

    /*public function index()
    {
        if (!checkUserRightAccess(71, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['view'] = 'backend/customer_group/add';
        $this->load->view('backend/layouts/default', $this->data);
    }*/

    public function salesByItems()
    {
        
        
        $this->data['view'] = 'backend/report/sales_by_items';

        $this->data['columns'] = "[
            { data: 'Title' },
            { data: 'SKU' },
            { data: 'PurchaseCount' }
        ]";
        if (isset($_GET['order']) && $_GET['order'] !== '')
        {
            $order = $_GET['order'];
        } else {
            $order = false;
        }

        $this->data['customID'] = $order;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function getSalesByItems(){


     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $orderChild = false;

     if ($columnName == 'Title') {
         $orderChild = true;
     }
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $order = $request->get('customID');
    
     // Total records
     $where = 'products_text.SystemLanguageID = 1';

     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND products.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }


     $totalRecords = $this->Product_model->getRowsCountWithChild('ProductID',$where);


    if ($order == 'desc'){
        $columnSortOrder = 'DESC';
        $columnName = 'PurchaseCount';
        $orderChild = false;
     }
     if ($order == 'asc'){
        $columnSortOrder = 'ASC';
        $columnName = 'PurchaseCount';
        $orderChild = false;
     }
    

    

     if($searchValue != ''){
        $where = $where. " AND (products_text.Title like '%".$searchValue."%' OR products.Price like '%".$searchValue."%'  OR products.PurchaseCount like '%".$searchValue."%')";
     }



     $totalRecordswithFilter = $this->Product_model->getRowsCountWithChild('ProductID',$where);// need to set here filer count
     $records =  $this->Product_model->getAllJoinedData(false,'ProductID',$this->language,$where,$columnSortOrder,$columnName,$orderChild,$rowperpage,$start);


     $data_arr = array();
     foreach ($records as $key => $value) {
        $action = '';


        

        
         

         
         $data_arr[] = array(
          "Title" => $value->Title,
          "SKU" => $value->SKU,
          "PurchaseCount" => $value->PurchaseCount
        );
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }

    public function salesByBrand()
    {
        
        
        $this->data['view'] = 'backend/report/sales_by_brand';

        $this->data['columns'] = "[
            
            { data: 'BrandTitle'},
            { data: 'TotalSales' }
        ]";
        
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function getSalesByBrand(){


     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $orderChild = false;

     if ($columnName == 'Title') {
         $orderChild = true;
     }
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $ParentID = $request->get('customID');
    
     // Total records
     $where = '1 = 1';

     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND products.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }


     $totalRecords = $this->Product_model->countSalesByBrand($where);


    
    

    

     if($searchValue != ''){
        $where = $where. " AND (brands_text.Title like '%".$searchValue."%' OR products.PurchaseCount like '%".$searchValue."%')";
     }

     

     $totalRecordswithFilter = $this->Product_model->countSalesByBrand($where);// need to set here filer count
     $records =  $this->Product_model->getSalesByBrand(false,$this->language,$where,$columnSortOrder,$columnName,false,$rowperpage,$start);
     //echo $this->db->last_query();

     $data_arr = array();
     foreach ($records as $key => $value) {
        $action = '';


        

        
         

         
         $data_arr[] = array(
          
          "BrandTitle" => $value->BrandTitle,
          
          "TotalSales" => $value->TotalSales
        );
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }

    public function salesByMonth($year = '')
    {
        if($year == ''){
            $year = date('Y');
        }
        $this->data['year'] = $year;
        
        $this->data['view'] = 'backend/report/sales_by_month';

        $this->data['columns'] = "[
            
            { data: 'Month'},
            { data: 'Year' },
            { data: 'TotalSales' }
        ]";
        
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function getSalesByMonth(){


     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $orderChild = false;

     if ($columnName == 'Title') {
         $orderChild = true;
     }
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $ParentID = $request->get('customID');
    
     $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'Julu', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');

     // Total records
     $where = '1 = 1';

     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND products.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }

     
    $year = date('Y');
        

     $totalRecords = $this->Order_model->getTotalsales(false,true);
     


    
    

    

     /*if($searchValue != ''){
        $where = $where. " AND (brands_text.Title like '%".$searchValue."%' OR products.PurchaseCount like '%".$searchValue."%')";
     }*/
     

     

     $totalRecordswithFilter = $this->Order_model->getTotalsales(false,true);// need to set here filer count
     $records =  $this->Order_model->getTotalsales(false);
     //echo $this->db->last_query();

     $data_arr = array();
     foreach ($records as $key => $value) {
        $action = '';


        

        
         

         
         $data_arr[] = array(
          
          "Month" => $months[$value->SalesMonth],
          "Year" => $value->SalesYear,
          
          "TotalSales" => $value->TotalSales
        );
     }




     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }

}