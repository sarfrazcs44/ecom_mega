<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requests extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model('Requests_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'RequestsID';
        $this->data['Table'] = 'requestses';
    }


    public function index()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['results'] = $this->Requests_model->getAllRequests();
        // dump($this->data['results']);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->Requests_model->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


}