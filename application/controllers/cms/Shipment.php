<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipment extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->model('Model_general');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = 'shipment';


    }

    public function index()
    {
        $this->data['view'] = 'backend/shipment/manage';
        //$this->data['driver_shipments'] = $this->Order_model->getOrders("orders.DriverID  > 0 AND orders.ShippedThroughApi = 0");
        //$this->data['api_shipments'] = $this->Order_model->getOrders("orders.DriverID  = 0 AND orders.ShippedThroughApi = 1");



        if (isset($_GET['status']) && $_GET['status'] !== '')
        {
            $status = $_GET['status'];
        } else {
            $status = 'driver_shipments';
        }

        if($status == 'driver_shipments'){
            $this->data['columns'] = "[
            { data: 'AssignedDriverName' },
            { data: 'AssignedDriverMobile' },
            { data: 'FullName' },
            { data: 'Mobile' },
            { data: 'OrderNumber' },
            { data: 'OrderStatusEn' },
            { data: 'TotalAmount' },
            { data: 'CreatedAt' },
            { data: 'Action' }
        ]";

        }else{
            $this->data['columns'] = "[
            { data: 'FullName' },
            { data: 'Mobile' },
            { data: 'OrderNumber' },
            { data: 'OrderStatusEn' },
            { data: 'TotalAmount' },
            { data: 'CreatedAt' },
            { data: 'Action' }
        ]";

        }
       

       

       

        

        $this->data['customID'] = $status;

        

        $this->data['url_status'] = $status;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function getEntries(){

     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $status = $request->get('customID');

    
     // Total records
     $where = false;

     

        if ($status == 'driver_shipments'){
            $where = "orders.DriverID  > 0 AND orders.ShippedThroughApi = 0";
        } else {
            $where = "orders.DriverID  = 0 AND orders.ShippedThroughApi = 1";
        }

     if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND orders.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }

     $totalRecords = $this->Order_model->getOrdersCount($where,$this->language);

    

    

    

     if($searchValue != ''){
        $where .= " AND (dut.FullName like '%".$searchValue."%' OR du.Mobile like '%".$searchValue."%' OR users_text.FullName like '%".$searchValue."%' OR users.Mobile like '%".$searchValue."%' OR orders.OrderID like '%".$searchValue."%' OR order_statuses.OrderStatusEn like '%".$searchValue."%' OR orders.TotalAmount like '%".$searchValue."%' OR orders.CreatedAt like '%".$searchValue."%')";
     }



     $totalRecordswithFilter = $this->Order_model->getOrdersCount($where,$this->language);
     $records =  $this->Order_model->getOrders($where,$rowperpage,$start,$this->language,'DESC');
     

        $data_arr = array();
     foreach ($records as $key => $order) {
        $order_status = $order->Status;
        if ($order_status == 1) {
            $class = "btn btn-sm";
        } else if ($order_status == 2) {
            $class = "btn btn-primary btn-sm";
        } else if ($order_status == 3) {
            $class = "btn btn-warning btn-sm";
        }else if ($order_status == 4) {
            $class = "btn btn-success btn-sm";
        } else if ($order_status == 5) {
            $class = "btn btn-danger btn-sm";
        }
        $action = '';


        

        $action .= '<a href="'.base_url('cms/orders/view/' . $order->OrderID).'"
                                                       class="btn btn-simple btn-warning btn-icon edit"><i
                                                                class="material-icons"
                                                                title="View order details">assignment</i>
                                                        <div class="ripple-container"></div>
                                                    </a>';
         

         if ($status == 'driver_shipments')
         {
         $data_arr[] = array(
          "AssignedDriverName" => '<a href="'.base_url('cms/user/edit') . '/' . $order->DriverID.'"target="_blank" title="Click to view driver details">'.ucfirst($order->AssignedDriverName).'
                                                    </a>',
          "AssignedDriverMobile" => $order->AssignedDriverMobile,
          "FullName" => $order->FullName,
          "Mobile" => $order->Mobile,
          "OrderNumber" => '<a href=" '.base_url() . 'cms/orders/view/' . $order->OrderID.'"target="_blank"title="Click to view order details"> '.$order->OrderNumber.'</a>',
          "OrderStatusEn" => '<button class="'.$class.'">'.$order->OrderStatusEn.'<div class="ripple-container"></div></button>',
          "TotalAmount" => number_format($order->TotalAmount, 2).' SAR',
          "CreatedAt" => $order->CreatedAt,
          "Action" => $action
        );
        } else {
        $data_arr[] = array(
          "FullName" => $order->FullName,
          "Mobile" => $order->Mobile,
          "OrderNumber" => '<a href=" '.base_url() . 'cms/orders/view/' . $order->OrderID.'"target="_blank"title="Click to view order details"> '.$order->OrderNumber.'</a>',
          "OrderStatusEn" => '<button class="'.$class.'">'.$order->OrderStatusEn.'<div class="ripple-container"></div></button>',
          "TotalAmount" => number_format($order->TotalAmount, 2).' SAR',
          "CreatedAt" => $order->CreatedAt,
          "Action" => $action
          );
     }
     
}


     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }


}