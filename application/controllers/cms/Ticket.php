<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Ticket_model');
        $this->load->model('Ticket_comment_model');
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->model('Model_general');
        $this->load->model('User_address_model');
        $this->load->model('Order_extra_charges_model');
        $this->data['ControllerName'] = 'ticket';
        $this->data['language'] = $this->language;
    }

    public function index()
    {
        $this->data['view'] = 'backend/ticket/manage';
        /*$this->data['ongoing_tickets'] = $this->Ticket_model->getTickets("tickets.IsClosed = 0");
        $this->data['closed_tickets'] = $this->Ticket_model->getTickets("tickets.IsClosed = 1");
        $this->data['reopened_tickets'] = $this->Ticket_model->getTickets("tickets.IsClosed = 2");*/

            if (isset($_GET['status']) && $_GET['status'] !== '')
        {
            $status = $_GET['status'];
        } else {
            $status = 'ongoing_tickets';
        }

        $this->data['columns'] = "[
            { data: 'FullName' },
            { data: 'Email' },
            { data: 'TicketNumber' },
            { data: 'TicketCreatedAt' },
            { data: 'OrderNumber' },
            { data: 'OrderStatusEn' },
            { data: 'TotalAmount' },
            { data: 'Action' }
        ]";

        

        $this->data['customID'] = $status;
        $this->data['url_status'] = $status;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function createTicket()
    {
        $post_data = $this->input->post();
        $checkIfAlreadyExist = $this->Ticket_model->getWithMultipleFields($post_data);
        if ($checkIfAlreadyExist) {
            $response['status'] = false;
            $response['message'] = "A ticket is already generated against this booking.";
            echo json_encode($response);
            exit();
        } else {
            $post_data['TicketNumber'] = RandomString(9) . $post_data['OrderID'];
            $post_data['CreatedAt'] = date('Y-m-d H:i:s');
            $inserted_id = $this->Ticket_model->save($post_data);
            if ($inserted_id > 0) {
                $ticket_comment['TicketID'] = $inserted_id;
                $ticket_comment['UserID'] = 0;
                $ticket_comment['Message'] = "Welcome to ecommerce support. How may I help you?";
                $ticket_comment['IsRead'] = 1;
                $ticket_comment['CreatedAt'] = date('Y-m-d H:i:s');
                $this->Ticket_comment_model->save($ticket_comment);
                $response['status'] = true;
                $response['message'] = "A ticket is raised against this booking.";
                $response['ComplaintDetail'] = "Complain No. " . $post_data['TicketNumber'] . "<span>Submitted on " . date('d-m-Y h:i:s A', strtotime($post_data['CreatedAt'])) . "</span>";
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = "Something went wrong when generating ticket against this booking.";
                echo json_encode($response);
                exit();
            }
        }
    }

    public function view($TicketID)
    {
        $messages = $this->Ticket_comment_model->getTicketComments(array('TicketID' => $TicketID));
        $ticket = $this->Ticket_model->getTickets("tickets.TicketID = " . $TicketID);
        $order = $this->Order_model->get($ticket[0]->OrderID, false, 'OrderID');
        $this->data['ticket'] = $ticket;
        $this->data['messages'] = $messages;
        $this->data['OrderNumber'] = $order->OrderNumber;
        $this->data['order_statuses'] = $this->Model_general->getAll('order_statuses', false, 'ASC', 'OrderStatusID');
        $this->data['order_extra_charges'] = $this->Order_extra_charges_model->getMultipleRows(array('OrderID' => $ticket[0]->OrderID));
        $this->data['TotalAmount'] = number_format($order->TotalAmount, 2) . ' SAR';
        $this->data['payment_address'] = $this->User_address_model->getAddresses("user_address.AddressID = " . $ticket[0]->AddressIDForPaymentCollection);
        $this->data['order_items'] = getOrderItems($ticket[0]->OrderID);
        $this->data['view'] = 'backend/ticket/view';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function saveMessage()
    {
        $post_data = $this->input->post(); // Message, TicketID
        $post_data['UserID'] = 0;
        $post_data['IsRead'] = 1;
        $post_data['CreatedAt'] = date('Y-m-d H:i:s');
        $inserted_id = $this->Ticket_comment_model->save($post_data);
        if ($inserted_id > 0) {

            //$this->notifyCustomer($post_data['TicketID'],$post_data['Message']);

            $messages = $this->Ticket_comment_model->getTicketComments(array('TicketID' => $post_data['TicketID']));
            $ticket = $this->Ticket_model->get($post_data['TicketID'], false, 'TicketID');
            $order = $this->Order_model->get($ticket->OrderID, false, 'OrderID');
            $html = '';
            foreach ($messages as $message) {
                $WhoSaid = ($message->UserID == 0) ? "Support" : "Customer";
                $class = $message->UserID == 0 ? "label-danger" : "label-success";
                $right_left = $message->UserID == 0 ? "pull-right" : "pull-left";
                $SentReceived = ($message->UserID == 0) ? "msgsent" : "msgreceive";
                $html .= '<div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="timeline-panel ' . $right_left . '">
                                            <div class="timeline-body">
                                                <p><span class="label ' . $class . '">' . $WhoSaid . '</span>&nbsp;&nbsp;&nbsp;&nbsp;' . $message->Message . '</p>
                                            </div>
                                            <small>
                                                <i class="ti-time"></i> ' . date('d.m.Y h:i:s A', strtotime($message->CreatedAt)) . '
                                            </small>
                                        </div>
                                    </div>
                                </div>';
            }
            $response['html'] = $html;
            $response['message'] = "Message sent successfully.";
            $this->pusherCall($messages, $post_data['TicketID']);
            echo json_encode($response);
            exit();
        } else {
            $response['message'] = "Something went wrong when sending message.";
            echo json_encode($response);
            exit();
        }
    }


    private function notifyCustomer($ticket_id,$message){
        $order_detail  = $this->Order_model->getOrders('tickets.TicketID = '.$ticket_id);

        if($order_detail){
            $order_detail = $order_detail[0];
            if($order_detail->OnlineStatus == 'Offline'){
                $this->ticketResponseMail($order_detail,$message);

                 // sending sms
                if ($order_detail->Mobile != '') {
                    $msg = "Dear " . $order_detail->FullName . ", ecommerce response to your ticket has ordered no. " . $order_detail->OrderNumber . "\n";
                    
                    sendSms($order_detail->Mobile, $msg);
                }
                
            }
        }
    }

    private function ticketResponseMail($order_info,$ticket_message)
    {
        if ($order_info->Email !== '') {
            $email_template = get_email_template(11);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->FullName, $message);
            $message = str_replace("{{order_no}}", $order_info->OrderNumber, $message);
            $message = str_replace("{{message}}", $ticket_message, $message);
           
            $data['to'] = $order_info->Email;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }

    private function pusherCall($messages, $TicketID)
    {
        $html = "";
        foreach ($messages as $message) {
            $WhoSaid = ($message->UserID == 0) ? "Support" : "Customer";
            $SentReceived = ($message->UserID == 0) ? "msgreceive" : "msgsent";
            $html .= '<div class="' . $SentReceived . '">
                        <p>' . $message->Message . '</p>
                    </div>';
        }
        $pusher_data['my_html'] = $html;
        $pusher_data['TicketID'] = $TicketID;
        pusher($pusher_data, "Ecommerce_Ticket_Channel", "Ecommerce_Ticket_Event");
        // pusher($pusher_data, 'my-channel', 'my-event');
    }

    public function update()
    {
        $post_data = $this->input->post();
        $this->Ticket_model->update(array('IsClosed' => $post_data['IsClosed']), array('TicketID' => $post_data['TicketID']));
        if ($post_data['IsClosed'] == 2) {
            $response['message'] = "Ticket reopened successfully.";
        } elseif ($post_data['IsClosed'] == 1) {
            $response['message'] = "Ticket closed successfully.";
        }
        echo json_encode($response);
        exit();
    }

    public function getEntries(){

     $request = $this->input;
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length"); // Rows display per page

     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');

     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $status = $request->get('customID');
     
    
     // Total records
     $where = false;

     

        if ($status == 'ongoing_tickets'){

            $where = "tickets.IsClosed = 0";

        }  elseif ($status == 'reopened_tickets' AND $this->session->userdata['admin']['RoleID'] == 1) 
        {
            $where = "tickets.IsClosed = 2";
        } else {
            $where = "tickets.IsClosed = 1";
        }

    if($this->session->userdata['admin']['RoleID'] != 1){

        $where .= ' AND tickets.CompanyID = '.$this->session->userdata['admin']['CompanyID'];
     }

     

     $totalRecords = $this->Ticket_model->getTicketsCount($where,$this->language);





    

     if($searchValue != ''){
        $where .= " AND (users_text.FullName like '%".$searchValue."%' OR user_address.Email like '%".$searchValue."%' OR tickets.TicketID like '%".$searchValue."%' OR tickets.CreatedAt like '%".$searchValue."%' OR orders.OrderID like '%".$searchValue."%' OR order_statuses.OrderStatusEn like '%".$searchValue."%' OR orders.TotalAmount like '%".$searchValue."%')";
     }



     $totalRecordswithFilter = $this->Ticket_model->getTicketsCount($where,$this->language);
     $records =  $this->Ticket_model->getTickets($where,$rowperpage,$start,$this->language,'DESC');

     $class = '';
        $data_arr = array();
     foreach ($records as $key => $order) {
        $order_status = $order->Status;
        if ($order_status == 1) {
            $class = "btn btn-sm";
        } else if ($order_status == 2) {
            $class = "btn btn-primary btn-sm";
        } else if ($order_status == 3) {
            $class = "btn btn-warning btn-sm";
        }else if ($order_status == 4) {
            $class = "btn btn-success btn-sm";
        } else if ($order_status == 5) {
            $class = "btn btn-danger btn-sm";
        }
        $action = '';


        

        $action .= '<a href="'.base_url('cms/orders/view/' . $order->TicketID).'"
                                                       class="btn btn-simple btn-warning btn-icon edit"><i
                                                                class="material-icons"
                                                                title="View order details">assignment</i>
                                                        <div class="ripple-container"></div>
                                                    </a>';
         

         
         $data_arr[] = array(
          "FullName" => $order->FullName,
          "Email" => $order->Email,
          "TicketNumber" => $order->TicketNumber,
          "TicketCreatedAt" => $order->TicketCreatedAt,
          "OrderNumber" => $order->OrderNumber,
          "OrderStatusEn" => '<button class="'.$class.'">'.$order->OrderStatusEn.'<div class="ripple-container"></div></button>',
          "TotalAmount" => $order->TotalAmount,
          "Action" => $action
        );
        
}


     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );

     echo json_encode($response);
     exit;

    }

}