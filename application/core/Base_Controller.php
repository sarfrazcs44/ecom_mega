<?php

class Base_Controller extends CI_Controller
{
    protected $language;
    protected $UserID;
    protected $companyData;
    protected $categories;
    protected $data;


    public function __construct()
    {
        parent::__construct();
        $this->load->Model('Category_model');
        $this->load->Model('Company_model');
        $this->load->Model('Temp_order_model');
        $this->load->Model('Menu_model');
        $this->load->helper('cookie');
        $this->data = array();
        if ($this->session->userdata('lang')) {
            $this->language = $this->session->userdata('lang');
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'EN';
            }
        }
        if ($this->session->userdata('user')) {
            $this->UserID = $this->session->userdata['user']->UserID;
        } else {
            if (!get_cookie('temp_user_key')) {
                $this->UserID = time();
                $cookie = array(
                    'name' => 'temp_user_key',
                    'value' => $this->UserID,
                    'expire' => time() + 86500,
                );
                set_cookie($cookie);
            } else {
                $this->UserID = get_cookie('temp_user_key');
            }
        }

        if ($this->session->userdata('company')) {
            $this->companyData = $this->session->userdata('company');
        } else {
            $currentDomain = $_SERVER['SERVER_NAME'];
            $this->companyData = $this->Company_model->getAllJoinedData(false,'CompanyID',$this->language,'companies.DomainName = "'.$currentDomain.'"');
            $this->session->set_userdata('company',$this->companyData[0]);
            $this->companyData  = $this->companyData[0];
        }
        //unset($_SESSION['company']);
        /*if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 5)) {
            // last request was more than 30 minutes ago
            unset($_SESSION['company']);
            session_destroy($_SESSION['company']);
        }
        $_SESSION['LAST_ACTIVITY'] = time();*/
        
        
        //print_rm($this->companyData);
        

        
        $this->data['menu_items'] = $this->Menu_model->getAllJoinedData(false,'MenuID',false,'menus.CompanyID='.$this->session->userdata['company']->CompanyID);
        //print_rm($this->data['menu_items']);
        $this->data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        //print_rm($this->data['cart_items']);
        $this->data['site_setting'] = $this->getSiteSetting();
        if($this->uri->segment(1) == 'cms'){
            $this->language = 'EN';
        }

        $this->data['categories'] = getCompanyCategories(0,$this->session->userdata['company']->CompanyID,$this->language);

        $this->data['Menucategories'] = MenuCategories(0,$this->session->userdata['company']->CompanyID,$this->language);
        

        $this->data['user_total_product'] = 0;
                    if ($this->session->userdata('user')) {
                        $user_id = $this->session->userdata['user']->UserID;
                    } else {
                        $user_id = get_cookie('temp_user_key');
                    }
                   $this->data['user_total_product']  = getTotalProduct($user_id);
    }

    public function changeLanguage($language)
    {
        $this->load->Model('System_language_model');
        $fetch_by['ShortCode'] = $language;
        $result = $this->System_language_model->getWithMultipleFields($fetch_by);
        if (!$result) {

            $default_lang = getDefaultLanguage();
            $language = $default_lang->ShortCode;
        }
        $this->session->set_userdata('lang', $language);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function getSiteSetting()
    {

        $this->load->model('Site_setting_model');
        return $this->Site_setting_model->get(1, false, 'SiteSettingID');
    }

    public function uploadImage($file_key, $path, $id = false, $type = false, $multiple = false)
    {
        $data = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
            $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
            $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {

                move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);
                if (!$multiple) {
                    return $path . $file_name;
                } else {
                    $this->load->model('Site_images_model');
                    $data['FileID'] = $id;
                    $data['ImageType'] = $type;
                    $data['ImageName'] = $path . $file_name;
                    $this->Site_images_model->save($data);
                }
                /* $data['DestinationID'] = $id; 
                  $data['ImagePath'] = $path.$file_name;
                  $this->Site_images_model->save($data); */
            }
        }
        return true;
    }

    public function DeleteImage()
    {
        $deleted_by = array();
        $ImagePath = $this->input->post('image_path');
        $deleted_by['SiteImageID'] = $this->input->post('image_id');
        if (file_exists($ImagePath)) {
            unlink($ImagePath);
        }
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


    public function deleteImage2(){
        
        $this->load->Model('Site_images_model');
        

        $deleted_by = array();
        $deleted_by['SiteImageID'] = $this->input->post('id');
        
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;

    }


    

}
