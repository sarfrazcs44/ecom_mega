<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use MailchimpAPI\Mailchimp;

function currentDate()
{
    return date('Y-m-d H:i:s');
}



function createInputField($input_name='Title',$col_size = 6,$title = 'Title',$value = '',$readonly = ''){

    return '<div class="col-md-'.$col_size.'">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">'.$title.'</label>
                                        <input type="text" name="'.$input_name.'" required  class="form-control" id="'.$input_name.'" value="'.$value.'" '.$readonly.'>
                                    </div>
                                </div>';
}
function createDescriptionField($input_name='Description',$col_size = 12,$title = 'Description',$value=''){

    return '                <div class="row">
                                <div class="col-md-'.$col_size.'">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">'.$title.'</label>
                                        <textarea class="form-control textarea" name="'.$input_name.'" id="'.$input_name.'" style="height: 100px;">'.$value.'</textarea>

                                    </div>
                                </div>
                            </div>';
}
function createCheckbox($input_name='IsActive',$title = 'IsActive',$is_checked = 0){

    return '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="togglebutton">
                                                <label for="'.$input_name.'">
                                                 <input name="'.$input_name.'" value="1" type="checkbox" id="'.$input_name.'" ' . (($is_checked == 1) ? 'checked' : '') . '/> '.$title.'
                                                </label>
                                            </div>
                                    </div>
                                </div>';
}


function print_rm($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}

function dump($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}

function convertEmptyToNA($data)
{
    if (is_array($data)) // checking if array is a multi-dimensional one, if so then checking for each row
    {
        foreach ($data as $key => $value) {
            if ($value == '') {
                $data[$key] = "N/A";
            }
        }
    } elseif (is_object($data)) {
        foreach ($data as $key => $value) {
            if ($value == '') {
                $data->$key = "N/A";
            }
        }
    }
    return $data;
}

function checkModuleRights($action = '',$module_id){
    return true;
    $CI = &get_Instance();
    $CI->load->model('Module_rights_model');

    $fetch_by = array();

    $fetch_by['CompanyID'] = $CI->session->userdata['admin']['CompanyID'];
    if($fetch_by['CompanyID'] == ''){
        $fetch_by['CompanyID'] = 0;
    }
    $fetch_by['RoleID'] = $CI->session->userdata['admin']['RoleID'];
    $fetch_by['ModuleID'] = $module_id;

    $redirect = 'url';


    if($action == 'action'){
        $form_type = $CI->input->post('form_type');
        $redirect = 'ajax';
        if($form_type == 'save'){
            $action = 'add';
        }elseif($form_type == 'update'){
            $action = 'edit';
        }elseif($form_type == 'delete'){
            $action = 'delete';
        }elseif($form_type == 'delete_specification'){
            $action = 'delete';
        }elseif($form_type == 'delete_image'){
            $action = 'delete';
        }elseif($form_type == 'suspend'){
            $action = 'edit';
        }elseif($form_type == 'activate'){
            $action = 'edit';
        }elseif($form_type == 'verifyUser'){
            $action = 'edit';
        }elseif($form_type == 'add_to_cart'){
            $action = 'edit';
        }

    }elseif($action == 'cart'){
        $action = 'view';
    }elseif($action == 'add_to_cart'){
        $action = 'edit';
    }elseif($action == 'update_cart'){
        $action = 'edit';
    }elseif($action == 'fetch_products'){
        $action = 'edit';
    }elseif($action == 'get_popup_detail'){
        $action = 'edit';
    }elseif($action == 'place_order'){
        $action = 'edit';
    }elseif($action == 'delete_cart_item'){
        $action = 'delete';
    }





    if($action == ''){
        $action = 'view';
    }

    $fetch_by['can_'.$action] = 1;

    $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);

   
    if($result){
         return true;

    }else{

        if($redirect == 'ajax'){
            $errors['error'] = 'You don\'t have its access';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;

        }else{

            $CI->session->set_flashdata('message', 'You don\'t have its access');
            redirect(base_url('cms/dashboard'));
            
        }



       
    }



}

function getMonths($language)
{

    if ($language == 'EN') {

        return array('Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    } elseif ($language == 'AR') {
        return array('يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيو', 'يولي', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر');
    }
}


function checkRightAccess($module_id, $role_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Module_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['RoleID'] = $role_id;
    $fetch_by[$can] = 1;

    $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}


function getCollectionProductsSubCategories($collection, $language_cod)
{

    $CI = &get_Instance();
    $CI->load->model('Collection_model');
    $result = $CI->Collection_model->getCollectionProductsSubCategories($collection, $language_cod);
    //echo $CI->db->last_query();
    if (!empty($result)) {
        return $result;
    } else {
        return false;
    }

}

function getActiveModules(){
    $CI = &get_Instance();
   
    $CI->load->model('Module_model');
    $modules_return_array = array();

     $modules = $CI->Module_model->getAllJoinedData(true,'ModuleID','EN','modules.IsActive = 1 AND modules.Hide = 0');
    if(!empty($modules)){
        $modules_return_array = $modules;
    }

    return $modules_return_array;
}


function getOfferProductsSubCategories($offer, $language_cod)
{

    $CI = &get_Instance();
    $CI->load->model('Offer_model');
    $result = $CI->Offer_model->getOfferProductsSubCategories($offer, $language_cod);
    //echo $CI->db->last_query();
    if (!empty($result)) {
        return $result;
    } else {
        return false;
    }

}


function NullToEmpty($data)
{
    $returnArr = array();
    if (isset($data[0])) // checking if array is a multi-dimensional one, if so then checking for each row
    {
        $i = 0;
        foreach ($data as $row) {
            if (is_object($row)) {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i]->$key = "";
                    } else {
                        $returnArr[$i]->$key = $value;
                    }
                }
            } else {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i][$key] = "";
                    } else {
                        $returnArr[$i][$key] = $value;
                    }
                }
            }
            $i++;
        }
    } else {
        if (is_object($data)) {
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr->$key = "";
                } else {
                    $returnArr->$key = $value;
                }
            }
        } else {
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr[$key] = "";
                } else {
                    $returnArr[$key] = $value;
                }
            }
        }
    }
    return $returnArr;
}

function checkUserRightAccess($module_id, $user_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Modules_users_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['UserID'] = $user_id;
    $fetch_by[$can] = 1;

    $result = $CI->Modules_users_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getSystemLanguages()
{
    $CI = &get_Instance();
    $CI->load->model('System_language_model');
    $languages = $CI->System_language_model->getAllLanguages();
    return $languages;
}

function getDefaultLanguage()
{
    $CI = &get_Instance();
    $CI->load->Model('System_language_model');
    $fetch_by = array();
    $fetch_by['IsDefault'] = 1;
    $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
    return $result;
}

function getLanguageBy($fetch_by)
{
    $CI = &get_Instance();
    $CI->load->Model('System_language_model');
    $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
    return $result;
}

function categoryName($category_id, $language)
{
    $CI = &get_Instance();
    $CI->load->Model('Category_model');

    $result = $CI->Category_model->getJoinedData(false, 'CategoryID', "categories.CategoryID = " . $category_id . " AND system_languages.ShortCode = '" . $language . "'");
    if ($result) {
        return $result[0]->Title;
    } else {
        return '';
    }
    //return $result;
}
function subCategories($category_id, $language)
{
    $CI = &get_Instance();
    $CI->load->Model('Category_model');

    $result = $CI->Category_model->getJoinedData(false, 'CategoryID', "categories.ParentID = " . $category_id . " AND system_languages.ShortCode = '" . $language . "' AND categories.IsActive = 1 AND categories.Hide = 0",'ASC','categories_text.Title',true);
    //echo $CI->db->last_query();
    if ($result) {
        return $result;
    } else {
        return '';
    }
    //return $result;
}
function MenuSubCategories($category_id, $language)
{
    $CI = &get_Instance();
    $CI->load->Model('Category_model');

    $result = $CI->Category_model->getJoinedData(false, 'CategoryID', "categories.ParentID = " . $category_id . " AND system_languages.ShortCode = '" . $language . "' AND categories.IsActive = 1 AND categories.AddToMenu = 1 AND categories.Hide = 0",'ASC','categories_text.Title',true,6,0);
    //echo $CI->db->last_query();
    if ($result) {
        return $result;
    } else {
        return '';
    }
    //return $result;
}

function encode_url($string, $key="", $url_safe=TRUE)
{
    return strtr(base64_encode($string), '+/=', '._-');
}


function decode_url($string, $key="")
{
    return base64_decode(strtr($string, '._-', '+/='));
}

function CompanyName($company_id,$language){

    $CI = &get_Instance();
    $CI->load->model('Company_model');
    $result = $CI->Company_model->getJoinedData(false,'CompanyID',"companies.CompanyID = " . $company_id . " AND system_languages.ShortCode = '" . $language . "'");


    return $result[0]->Title;

}

function CompanyData($company_id){

    $CI = &get_Instance();
    $CI->load->model('Company_model');
    $result = $CI->Company_model->getJoinedData(false,'CompanyID',"companies.CompanyID = " . $company_id . " AND system_languages.ShortCode = 'EN'");


    return $result[0];

}
function getCompanyCategories($category_id, $company_id ,$language)
{
    $CI = &get_Instance();
    $CI->load->Model('Category_model');

        $result = $CI->Category_model->getCompanyData(false, "categories.CompanyID = ". $company_id . " AND categories.ParentID = " . $category_id . " AND system_languages.ShortCode = '" . $language . "' AND categories.IsActive = 1 AND categories.Hide = 0",'ASC','categories_text.Title',true);

    //echo $CI->db->last_query();exit;
    if ($result) {
        return $result;
    } else {
        return '';
    }
    //return $result;
}
function MenuCategories($category_id, $company_id ,$language)
{
    $CI = &get_Instance();
    $CI->load->Model('Category_model');

        $result = $CI->Category_model->getCompanyData(false, "categories.CompanyID = ". $company_id . " AND categories.ParentID = " . $category_id . " AND system_languages.ShortCode = '" . $language . "' AND categories.IsActive = 1 AND categories.AddToMenu = 1 AND categories.Hide = 0",'ASC','categories_text.Title',true,3,0);

    //echo $CI->db->last_query();exit;
    if ($result) {
        return $result;
    } else {
        return '';
    }
    //return $result;
}

function variantName($variant_id, $language)
{
    $CI = &get_Instance();
    $CI->load->Model('Variant_model');

    $result = $CI->Variant_model->getJoinedData(false, 'VariantID', "variants.VariantID = " . $variant_id . " AND system_languages.ShortCode = '" . $language . "'");
    if ($result) {
        return $result[0]->Title;
    } else {
        return '';
    }
    //return $result;
}
function getAttribute($variant_id, $language)
{
    $CI = &get_Instance();
    $CI->load->Model('Variant_model');

    $result = $CI->Variant_model->getJoinedData(false, 'VariantID', "variants.ParentID = " . $variant_id . " AND system_languages.ShortCode = '" . $language . "' AND variants.IsActive = 1 AND variants.Hide = 0",'ASC','variants_text.Title',true);
    //echo $CI->db->last_query();
    if ($result) {
        return $result;
    } else {
        return '';
    }
    //return $result;
}


function getAllActiveModules($role_id, $system_language_id, $where)
{
    $CI = &get_Instance();
    $CI->load->Model('Module_rights_model');
    $result = $CI->Module_rights_model->getModulesWithRights($role_id, $system_language_id, $where);
    return $result;
}


function getActiveUserModule($user_id, $system_language_id, $where)
{
    $CI = &get_Instance();
    $CI->load->Model('Modules_users_rights_model');
    $result = $CI->Modules_users_rights_model->getModulesWithRights($user_id, $system_language_id, $where);
    return $result;
}

function countStoreUser($store_id)
{
    $CI = &get_Instance();
    $CI->load->Model('User_model');
    $fetch_by = array();
    $fetch_by['StoreID'] = $store_id;
    $result = $CI->User_model->getMultipleRows($fetch_by);
    if ($result) {
        return count($result);
    } else {
        return '0';
    }

}

function checkAdminSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('admin')) {
        return true;

    } else {
        redirect($CI->config->item('base_url') . 'cms');
    }
}

function checkFrontendSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('user')) {

        if($CI->session->userdata['user']->IsMobileVerified == 0){
            redirect($CI->config->item('base_url').'?verify_mobile=true');
        }
        return true;

    } else {
        redirect($CI->config->item('base_url'));
    }
}


function sendEmail($data = array(),$file_name = false,$company_id = 0)
{
    $CI = &get_Instance();
    if($company_id > 0){

        $company_data = CompanyData($company_id);

        $config["protocol"] = $company_data->Protocol;
        $config["smtp_host"] = $company_data->Host;
        $config["smtp_port"] = $company_data->Port;
        $config["smtp_user"] = $company_data->User;
        $config["smtp_pass"] = $company_data->Pass;
        $from_email = $company_data->FromEmail;

    }else{
        $config["protocol"] = "smtp";
        $config["smtp_host"] = "ssl://smtp.gmail.com";
        $config["smtp_port"] = 465;
        $config["smtp_user"] = "info@ecommerce.com";
        $config["smtp_pass"] = "";
        $from_email = 'info@ecommerce.com';
    }
    
    $config["charset"] = "utf-8";
    $config["mailtype"] = "html";
    $CI->load->library('email',$config);
    $CI->email->from($from_email, site_title());
    $CI->email->to($data['to']);
    $CI->email->subject($data['subject']);
    $CI->email->message($data['message']);
    if($file_name){
         $CI->email->attach($file_name);
    }
    $CI->email->set_mailtype('html');
    //print_rm($data);
    if ($CI->email->send()) {
        return true;
    } else {
        return false;
    }
}

function sendSmsOfficial() // This is official function kept here so we can use it anywhere we want. It has all the functionality unifonic supports
{
    require FCPATH . '/vendor/Unifonic/Autoload.php';
    $client = new \Unifonic\API\Client();
    try {
        $response = $client->Messages->Send('923368809300', 'ecommerce sms testing', 'Ecommerce'); // send regular massage
        dump($response);
        //$response = $client->Account->GetBalance();
        //$response = $client->Account->getSenderIDStatus('Arabic');
        //$response = $client->Account->getSenderIDs();
        //$response = $client->Account->GetAppDefaultSenderID();
        //$response = $client->Messages->Send('recipient','messageBody','senderName');
        //$response = $client->Messages->SendBulkMessages('96650*******,9665*******','Hello','UNIFONIC');
        //$response = $client->Messages->GetMessageIDStatus('9459*******');
        //$response = $client->Messages->GetMessagesReport();
        //$response = $client->Messages->GetMessagesDetails();
        //$response = $client->Messages->GetScheduled();
        echo '<pre>';
        print_r($response);
    } catch (Exception $e) {
        echo $e->getCode();
    }
}

function sendSms($mobile_no, $msg, $debug = false) // Provide mobile no with country code, AppsID is configured in vendor/Unifonic/config.php
{
    // return true;
    require_once FCPATH . '/vendor/Unifonic/Autoload.php';
    $client = new \Unifonic\API\Client();
    try {
        $msg = $msg . "\nFrom: Ecommerce.";
        $response = $client->Messages->Send($mobile_no, $msg, 'Ecommerce');
        if ($debug) // If this is true and message sent in try block then it will dump response here
        {
            dump($response);
        }
        if (isset($response->Status) && ($response->Status == 'Queued' || $response->Status == 'Sent' || $response->Status == 'Delivered')) {
            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        $error = $e->getCode();
        if ($debug) // If this is true and message failed to sent in try block then it will echo error message here
        {
            echo $error;
        }
        return false;
    }
}


function RandomString($digit = 4)
{
    $characters = '123456789123456789123456789123456789123456789';
    $randstring = '';
    for ($i = 0; $i < $digit; $i++) {
        $randstring .= $characters[rand(0, 40)];
    }
    return $randstring;
}

function generatePIN($digits = 4)
{
    $i = 0; //counter
    $pin = ""; //our default pin is blank.
    while ($i < $digits) {
        //generate a random number between 0 and 9.
        $pin .= mt_rand(0, 9);
        $i++;
    }
    return $pin;
}

function log_notification($data)
{
    $CI = &get_Instance();
    $CI->load->model('User_notification_model');
    $result = $CI->User_notification_model->save($data);
    if ($result > 0) {
        return true;
    } else {
        return false;
    }
}

function sendPushNotificationToAndroid($title, $message, $registatoin_ids, $data = array())
{
    $android_fcm_key = 'AIzaSyAGAGqUTZ233iG81Tqj6hWFLz6XbJWNJSg';

    $sendData['title'] = $title;
    $sendData['body'] = $message;
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array(
        "registration_ids" => $registatoin_ids,
        "content_available" => true,
        "priority" => "high",
        "notification" => array
        (
            "title" => $title,
            "body" => $message,
            "sound" => "default"
        ),
        "data" => array
        (
            "body" => $message,
            "notificationKey" => $registatoin_ids,
            "priority" => "high",
            "sound" => "default",
            "notification_data" => $data
        ),
    );

    $headers = array(
        'Authorization:key=' . $android_fcm_key,
        'Content-Type: application/json'
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);

    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

function sendPushNotificationToIOS($title, $message, $register_keys, $data = array())
{
    $ios_fcm_key = 'AIzaSyAGAGqUTZ233iG81Tqj6hWFLz6XbJWNJSg';

    $fields = array(
        //"to" => $gcm_ios_mobile_reg_key,
        "registration_ids" => $register_keys, //1000 per request logic is pending
        "content_available" => true,
        "priority" => "high",
        "notification" => array(
            "body" => strip_tags($message),
            "title" => $title,
            "sound" => "default"
        ),
        "data" => array
        (
            "body" => $message,
            "notificationKey" => $register_keys,
            "priority" => "high",
            "sound" => "default",
            "notification_data" => $data
        ),
    );


    $url = 'https://gcm-http.googleapis.com/gcm/send'; //note: its different than android.


    $headers = array(
        'Authorization: key=' . $ios_fcm_key,
        'Content-Type: application/json'
    );


    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        // echo 'abc';
        die('Curl failed: ' . curl_error($ch));
    }
    // echo 'cdf';
    //print_r($result);exit();
    // Close connection
    curl_close($ch);
    return $result;
}

function sendNotification($title, $message, $data, $user_id)
{
    $CI = &get_Instance();
    $CI->load->model('User_model');
    $res = 'Device token not found!';
    $user = $CI->User_model->get($user_id, true, 'UserID');
    if ($user['DeviceToken'] != '') {
        $token = array($user['DeviceToken']);
        if (strtolower($user['DeviceType']) == 'android') {
            $res = sendPushNotificationToAndroid($title, $message, $token, $data);
        } elseif (strtolower($user['DeviceType']) == 'ios') {
            $res = sendPushNotificationToIOS($title, $message, $token, $data);
        }
    }
    return $res;
}

function pusher($data, $channel, $event, $debug = false)
{
    require FCPATH . '/vendor/autoload.php';

    $options = array(
        'cluster' => 'ap2',
        'useTLS' => true
    );

    //$pusher = new Pusher\Pusher(
    // $app_key,
    //$app_secret,
    // $app_id,
    //array('cluster' => $app_cluster) );

    $pusher = new Pusher\Pusher(
        'a796cb54d7c4b4ae4893',
        'd84121cb5083a62950b6',
        '724300',
        $options
    );

    $response = $pusher->trigger($channel, $event, $data);
    return $response;
}

function convertTimestampToLocalDatetime($timestamp)
{
    // first converting timestamp to GMT date time
    $datetime = gmdate("Y-m-d H:i:s", $timestamp);

    // setting default timezone
    if (isset($_COOKIE['system_timezone'])) {
        $current_timezone = $_COOKIE['system_timezone'];
    } else {
        $current_timezone = 'Asia/Riyadh';
    }

    // creating date time object from the date time coming in UTC format
    $utc_date = DateTime::createFromFormat(
        'Y-m-d H:i:s',
        $datetime,
        new DateTimeZone('UTC')
    );
    $acst_date = clone $utc_date;

    // setting timezone to local timezone for UTC time coming above
    $acst_date->setTimeZone(new DateTimeZone($current_timezone));

    // formatting datetime
    $original_time = $utc_date->format('Y-m-d H:i:s');
    $converted_local_time = $acst_date->format('Y-m-d H:i:s');
    return $converted_local_time;
}

function getFormattedDateTime($timestamp, $format)
{
    return date($format, strtotime(convertTimestampToLocalDatetime($timestamp)));
}

function getPageContent($page_id, $lang = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('Page_model');
    $result = $CI->Page_model->getJoinedData(false, 'PageID', "pages.PageID = " . $page_id . " AND system_languages.ShortCode = '" . $lang . "'")[0];
    return $result;
}

function site_settings()
{
    $CI = &get_Instance();
    $CI->load->model('Site_setting_model');
    return $CI->Site_setting_model->get(1, false, 'SiteSettingID');
}

function site_title()
{
    $CI = &get_Instance();
    $CI->load->model('Site_setting_model');
    return $site_setting = $CI->Site_setting_model->get(1, false, 'SiteSettingID')->SiteName;
    // return $site_setting->SiteName;
}

function my_site_url()
{
    $input = base_url();

// in case scheme relative URI is passed, e.g., //www.google.com/
    $input = trim($input, '/');

// If scheme not included, prepend it
    if (!preg_match('#^http(s)?://#', $input)) {
        $input = 'http://' . $input;
    }

    $urlParts = parse_url($input);

// remove www
    $domain = preg_replace('/^www\./', '', $urlParts['host']);

    return $domain;

// output: google.co.uk
}

function generateBarcode($text, $file_name, $type = 'image')
{
    require FCPATH . '/vendor/autoload.php';
    $time = time();
    $file_path = "uploads/barcode/$file_name.png";
    // $generator = new Picqer\Barcode\BarcodeGeneratorSVG();
    // $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
    // $generator = new Picqer\Barcode\BarcodeGeneratorJPG();
    // $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
    // return $generator->getBarcode('081231723897', $generator::TYPE_CODE_128);
    if ($type == 'image') {
        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        $barcode = $generator->getBarcode($text, $generator::TYPE_CODE_128);
        file_put_contents($file_path, $barcode);
        return $file_path;
    } elseif ($type == 'html') {
        $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
        return $generator->getBarcode($text, $generator::TYPE_CODE_128);
    }
}

function generate_pdf($html, $filename = 'invoice')
{
    require FCPATH . '/vendor/autoload.php';
    $filename = 'Invoice-' . $filename . "-" . date('YmdHis') . ".pdf";
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->SetTitle('Order Receipt');
    $mpdf->WriteHTML($html);
    $mpdf->Output($filename, 'I');
}

function booking_html($booking_info)
{
    $html = '<table cellspacing="0" width="100%" style="width:100%">';
    $html .= '<tr><td>OrderNumber </td><td>' . $booking_info['OrderNumber'] . '</td></tr>';
    $html .= '<tr><td>Status </td><td>' . $booking_info['BookingStatusEn'] . '</td></tr>';
    $html .= '<tr><td>Address </td><td>' . $booking_info['Address'] . '</td></tr>';
    $html .= '<tr><td>ServiceCost </td><td>' . $booking_info['ServiceCost'] . '</td></tr>';
    $html .= '<tr><td>CategoryTitle </td><td>' . $booking_info['CategoryTitle'] . '</td></tr>';
    $html .= '</table>';
    return $html;
}

function emailTemplate($msg)
{
    $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ecommerce</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}

#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>

<body>

<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<p style="font-family:sans-serif;font-size:14px;">' . $msg . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Ecommerce</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">b-u.schopfen.com</a>
</span>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>';

    return $html;
}

function custom_encode($str)
{
    return substr(json_encode($str), 1, -1);
}

function custom_decode($str)
{
    return json_decode(sprintf('"%s"', $str));
}

function front_assets($path = "")
{
    return base_url() . 'assets/frontend/' . $path;
}

function captchaVerify($siteKey)
{
    $secret = '6LeqI5EUAAAAAPUXA4Ficz4i2tYwJ96G9N25hLBL';
    $data = array(
        'secret' => $secret,
        'response' => $siteKey
    );

    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    $captcha_success = json_decode($response);
    return $captcha_success;
}

function validateMobileNumber($number)
{
    // $url = "http://apilayer.net/api/validate?access_key=d64c34a988c8a6224928b5673d59c22f&number=923368809300&country_code=&format=1";
    $data = array(
        'access_key' => 'd64c34a988c8a6224928b5673d59c22f',
        'number' => $number
    );
    $query_str = http_build_query($data);
    $url = "http://apilayer.net/api/validate?" . $query_str;
    echo $url;
    exit();
    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "http://apilayer.net/api/validate");
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    $verify_response = json_decode($response);
    dump($verify_response);
    return $verify_response;
}

function getCities($language = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('City_model');
    $cities = $CI->City_model->getAllJoinedData(false, 'CityID', $language, "cities.IsActive = 1", 'ASC', 'SortOrder');
    return $cities;
}


function getBranchDistrict($district_ids,$language = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('District_model');
    $districts = $CI->District_model->getAllJoinedData(false, 'DistrictID', $language, "districts.DistrictID IN (".$district_ids.")", 'ASC', 'SortOrder');
    //echo $CI->db->last_query();
    return $districts;
}

function productTitle($product_id)
{
    $CI = &get_Instance();
    $CI->load->model('Product_model');
    $result = $CI->Product_model->getJoinedData(false, 'ProductID', "products.ProductID = " . $product_id . " AND system_languages.ShortCode = 'EN'");
    if (empty($result)) {
        redirect(base_url('product'));
    }
    $title = str_replace(" ", "-", $result[0]->Title);
    return strtolower($title);
}

function productID($product_title)
{
    $CI = &get_Instance();
    $CI->load->model('Product_model');
    $title = str_replace("-", " ", $product_title);
    $result = $CI->Product_model->getJoinedData(false, 'ProductID', "products_text.Title = '" . $product_title . "' AND system_languages.ShortCode = 'EN'");
    if (empty($result)) {
        $result = $CI->Product_model->getJoinedData(false, 'ProductID', "products_text.Title = '" . $title . "' AND system_languages.ShortCode = 'EN'");
        if(empty($result)){
            redirect(base_url('product'));
        }
        
    }
    return $result[0]->ProductID;
}

function getUserID()
{
    $CI = &get_Instance();
    return $CI->UserID;
}

function collectionTitle($collection_id)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_model');
    $result = $CI->Collection_model->getJoinedData(false, 'CollectionID', "collections.CollectionID = " . $collection_id . " AND system_languages.ShortCode = 'EN'");
    if (empty($result)) {
        redirect(base_url());
    }
    $title = str_replace(" ", "-", $result[0]->Title);
    return strtolower($title);
}

function collectionID($collection_title)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_model');
    $title = str_replace("-", " ", $collection_title);
    $result = $CI->Collection_model->getJoinedData(false, 'CollectionID', "collections_text.Title = '" . $title . "' AND system_languages.ShortCode = 'EN'");
    if (empty($result)) {
        redirect(base_url());
    }
    return $result[0]->CollectionID;
}

function get_images($id, $type = 'product', $multiple = true)
{
    $CI = &get_Instance();
    $CI->load->model('Site_images_model');
    $images = $CI->Site_images_model->getMultipleRows(array('FileID' => $id, 'ImageType' => strtolower($type)));
    if ($images) {
        if ($multiple) {
            return $images;
        } else {
            return $images[0]->ImageName;
        }
    } else {

        if ($multiple) {
            return [];
        } else {
            return '';
        }
        
    }
}

function whats_inside_collection($id)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_model');
    $CI->load->model('Product_model');
    $collection = $CI->Collection_model->get($id, false, 'CollectionID');
    if ($collection) {
        $ProductID = $collection->ProductID;
        if ($ProductID != '') {
            $products_ids = explode(',', $ProductID);
            $products = $CI->Product_model->getProductsOfCollection($products_ids);
            return $products;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function getOrderItems($order_id)
{
    $CI = &get_Instance();
    $CI->load->model('Order_item_model');
    $items = $CI->Order_item_model->getOrdersItems($order_id);
    return $items;
}

function newsletter_subscribe($email, $debug = false)
{
    require FCPATH . '/vendor/autoload.php';
    $mailchimp = new Mailchimp('510a3c6f572f02e7a006fd0c680140e4-us18');
    $post_params = [
        'email_address' => $email,
        'status' => 'subscribed'
    ];
    $mailchimp = $mailchimp->lists('b2ef677cb5')->members()->post($post_params);
    // $response->deserialize(); // returns a deserialized (to php object) resource returned by API
    // $response->getHttpCode(); // returns an integer representation of the HTTP response code
    // $response->getHeaders(); // returns response headers as an array of key => value pairs
    // $response->getBody(); // return the raw text body of the response
    $response = json_decode($mailchimp->getBody());
    if ($debug) {
        dump($response);
    }
    //print_rm($response);
    $http_code = $mailchimp->getHttpCode();
    $retArr['http_code'] = $http_code;
    $retArr['title'] = (isset($response->title) ? $response->title: '');
    $retArr['detail'] = (isset($response->detail) ? $response->detail: '');
    return $retArr;
}

function get_mailchimp_subscribers()
{
    require FCPATH . '/vendor/autoload.php';
    $mailchimp = new Mailchimp('217b3351972fdd6cdf41e0464ead92c2-us20');
    $mailchimp = $mailchimp->lists('49ea478394')->members()->get();
    $response = json_decode($mailchimp->getBody());
    return $response->members;
}

function getTotalProduct($user_id)
{
    $CI = &get_Instance();
    $CI->load->model('Temp_order_model');
    $result = $CI->Temp_order_model->getTotalProduct($user_id);
    return $result[0]->CartProductsCount > 0 ? $result[0]->CartProductsCount : 0;
}

function isLiked($id, $type)
{
    $CI = &get_Instance();
    $CI->load->model('User_wishlist_model');
    if ($CI->session->userdata('user')) {
        $data['UserID'] = $CI->session->userdata['user']->UserID;
        $data['ItemID'] = $id;
        $data['ItemType'] = $type;
        $wishlist = $CI->User_wishlist_model->getWithMultipleFields($data);
        if ($wishlist) {
            return "p_liked";
        } else {
            return "p_unliked";
        }
    } else {
        return "p_unliked";
    }
}

function getLanguage()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('lang')) {
        $language = $CI->session->userdata('lang');
    } else {
        $result = getDefaultLanguage();
        if ($result) {
            $language = $result->ShortCode;
        } else {
            $language = 'EN';
        }
    }

    return $language;
}

function format_message_for_client($message, $user_id)
{
    $message->WhoSaid = ($message->UserID == $user_id) ? "Customer" : "Support";
    $message->SentReceived = ($message->UserID == $user_id) ? "msgsent" : "msgreceive";
    return $message;
}

function format_message_for_admin($message, $user_id)
{
    $message->WhoSaid = ($message->UserID == $user_id) ? "Support" : "Customer";
    $message->EvenOdd = ($message->UserID == $user_id) ? "even" : "odd";
    return $message;
}

function getTaxShipmentCharges($type = 'Shipment', $OnlyDefault = false) // type == Shipment, Tax
{
    $CI = &get_Instance();
    $CI->load->model('Tax_shipment_charges_model');
    $lang = getLanguage();
    $tax_shipment = new Tax_shipment_charges_model();
    if ($OnlyDefault) {
        $where = "tax_shipment_charges.ChargesType = '" . $type . "' AND tax_shipment_charges.IsDefault = 1";
        $defaultCharge = $tax_shipment->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return (isset($defaultCharge[0]) ? $defaultCharge[0] : false);
    } else {
        $where = "tax_shipment_charges.ChargesType = '" . $type . "' AND tax_shipment_charges.IsActive = 1";
        $charges = $tax_shipment->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return $charges;
    }
}

function getTaxShipmentChargesByID($type = 'Shipment',$ID ) // type == Shipment, Tax
{
    $CI = &get_Instance();
    $CI->load->model('Tax_shipment_charges_model');
    $lang = getLanguage();
    $tax_shipment = new Tax_shipment_charges_model();
    $where = "tax_shipment_charges.ChargesType = '" . $type . "' AND tax_shipment_charges.TaxShipmentChargesID = " . $ID . "";
        
    $defaultCharge = $tax_shipment->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return (isset($defaultCharge[0]) ? $defaultCharge[0] : false);
}

function getUserAddress($AddressIDForPaymentCollection){
    $CI = &get_Instance();
    $CI->load->model('User_address_model');
    $payment_address = $this->User_address_model->getAddresses("user_address.AddressID = " . $AddressIDForPaymentCollection);
    return $payment_address;
}

function orderExtraCharges($order_id){
    $CI = &get_Instance();
    $CI->load->model('Order_extra_charges_model');
    $result = $CI->Order_extra_charges_model->getMultipleRows(array('OrderID' => $order_id));
    return $result;
}

function getSelectedShippingMethodDetail($ShipmentMethodID, $lang)
{
    $CI = &get_Instance();
    $CI->load->model('Tax_shipment_charges_model');
    if ($ShipmentMethodID > 0) {
        $where = "tax_shipment_charges.TaxShipmentChargesID = " . $ShipmentMethodID;
        $shipment_method = $CI->Tax_shipment_charges_model->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return (isset($shipment_method[0]) ? $shipment_method[0] : false);
    } else {
        $shipment_method = false;
    }
    return $shipment_method;
}

function productAverageRating($ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Product_rating_model');
    $rating = $CI->Product_rating_model->getAverageRating($ProductID);
    return round($rating->average_rating, 1);
}

function collectionAverageRating($CollectionID)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_rating_model');
    $rating = $CI->Collection_rating_model->getAverageRating($CollectionID);
    return round($rating->average_rating, 1);
}

function productRatings($ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Product_rating_model');
    $total_ratings_count = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID));
    if ($total_ratings_count > 0) {
        $rating_count_1 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 1));
        $rating_count_2 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 2));
        $rating_count_3 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 3));
        $rating_count_4 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 4));
        $rating_count_5 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 5));
        $response_arr['rating_1'] = round(($rating_count_1 / $total_ratings_count) * 100);

        $response_arr['rating_2'] = round(($rating_count_2 / $total_ratings_count) * 100);
        $response_arr['rating_3'] = round(($rating_count_3 / $total_ratings_count) * 100);
        $response_arr['rating_4'] = round(($rating_count_4 / $total_ratings_count) * 100);
        $response_arr['rating_5'] = round(($rating_count_5 / $total_ratings_count) * 100);

         $response_arr['rating_1_count'] = $rating_count_1;
         $response_arr['rating_2_count'] = $rating_count_2;
         $response_arr['rating_3_count'] = $rating_count_3;
         $response_arr['rating_4_count'] = $rating_count_4;
         $response_arr['rating_5_count'] = $rating_count_5;



        $response_arr['total_ratings_count'] = $total_ratings_count;
    } else {
        $response_arr['rating_1'] = 0;
        $response_arr['rating_2'] = 0;
        $response_arr['rating_3'] = 0;
        $response_arr['rating_4'] = 0;
        $response_arr['rating_5'] = 0;


        $response_arr['rating_1_count']  = 0;
        $response_arr['rating_2_count'] = 0;
        $response_arr['rating_3_count'] = 0;
        $response_arr['rating_4_count'] = 0;
        $response_arr['rating_5_count'] = 0;
        $response_arr['total_ratings_count'] = 0;
    }
    return $response_arr;
}

function collectionRatings($CollectionID)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_rating_model');
    $total_ratings_count = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID));
    if ($total_ratings_count > 0) {
        $rating_count_1 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 1));
        $rating_count_2 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 2));
        $rating_count_3 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 3));
        $rating_count_4 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 4));
        $rating_count_5 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 5));
        $response_arr['rating_1'] = ($rating_count_1 / $total_ratings_count) * 100;
        $response_arr['rating_2'] = ($rating_count_2 / $total_ratings_count) * 100;
        $response_arr['rating_3'] = ($rating_count_3 / $total_ratings_count) * 100;
        $response_arr['rating_4'] = ($rating_count_4 / $total_ratings_count) * 100;
        $response_arr['rating_5'] = ($rating_count_5 / $total_ratings_count) * 100;
        $response_arr['total_ratings_count'] = $total_ratings_count;
    } else {
        $response_arr['rating_1'] = 0;
        $response_arr['rating_2'] = 0;
        $response_arr['rating_3'] = 0;
        $response_arr['rating_4'] = 0;
        $response_arr['rating_5'] = 0;
        $response_arr['total_ratings_count'] = 0;
    }
    return $response_arr;
}

function uploadImage($file_key, $path)
{
    $extension = array("jpeg", "jpg", "png");
    $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name']);
    $file_tmp = $_FILES[$file_key]["tmp_name"];
    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
    if (in_array($ext, $extension)) {
        move_uploaded_file($file_tmp, $path . $file_name);
        return $path . $file_name;
    } else {
        return '';
    }
}

function get_order_invoice($order_id, $type = 'invoice')
{
    $CI = &get_Instance();
    $CI->load->model('Order_model');
    $CI->load->model('User_address_model');
    $order_details = $CI->Order_model->getOrders("orders.OrderID = $order_id");
    $data['order'] = $order_details[0];
   // $data['payment_address'] = $CI->User_address_model->getAddresses("user_address.AddressID = " . $order_details[0]->AddressIDForPaymentCollection)[0];
    $data['type'] = $type;
    $order_html = $CI->load->view('frontend/emails/order_confirmation', $data, true);
    return $order_html;
}

function get_order_invoice_new($OrderID)
    {
       
        $CI = &get_Instance();
        $CI->load->model('Order_model');
        
        $order_details = $CI->Order_model->getOrders("orders.OrderID = $OrderID");
        $data['order'] = $order_details[0];
        //print_rm($data['order']);
       
        $order_html = $CI->load->view('frontend/invoice', $data, true);
        return $order_html;
        
    }

function email_format($content)
{
    $CI = &get_Instance();
    $data['content'] = $content;
    $html = $CI->load->view('frontend/emails/general_email', $data, true);
    return $html;
}

function get_email_template($template_id, $lang = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('Email_template_model');
    $where = "email_templates.Email_templateID = " . $template_id . " AND system_languages.ShortCode = '" . $lang . "'";
    $template = $CI->Email_template_model->getJoinedData(false, 'Email_templateID', $where);
    return $template[0];
}

function get_unread_orders()
{
    $CI = &get_Instance();
    $CI->load->model('Order_model');
    $result = $CI->Order_model->getMultipleRowsWithSort(array('IsRead' => 0), 'orders.OrderID', 'DESC');
    $retArr['result'] = $result;
    $retArr['result_count'] = $result ? count($result) : 0;
    return $retArr;
}

function getCategories($language,$where = '1 = 1')
{
    $CI = &get_Instance();
    $CI->load->model('Category_model');
    return $CI->Category_model->getAllJoinedData(true, 'CategoryID', $language, ''.$where.' AND categories.IsActive = 1');

}

function getAddressDetail($AddressID)
{
    $CI = &get_Instance();
    $CI->load->model('User_address_model');
    $address = $CI->User_address_model->getAddresses("user_address.AddressID = " . $AddressID);
    return $address[0];
}

function getUserOffers($language)
{
    $CI = &get_Instance();
    $CI->load->model('Offer_user_notification_model');
    if ($CI->session->userdata('user')) {
        $UserID = $CI->session->userdata['user']->UserID;
        $user_offers = $CI->Offer_user_notification_model->getUserOffers($UserID, $language);
        return $user_offers;
    } else {
        return false;
    }
}

function IsProductUnderOffer($product_id)
{
    // this function is not final yet
    $CI = &get_Instance();
    $CI->load->model('Offer_user_notification_model');
    $html = '';
    if ($CI->session->userdata('user')) {
        $UserID = $CI->session->userdata['user']->UserID;
        $user_offers = $CI->Offer_user_notification_model->getAllUserOffers($UserID);
        if ($user_offers and count($user_offers) > 0) {
            foreach ($user_offers as $user_offer) {
                $ProductIDs = explode(',', $user_offer->ProductID);
                if (in_array($product_id, $ProductIDs)) {
                    $html = '<h4 class="offered_product" title="' . $user_offer->Description . '">' . $user_offer->Title . '</h4>';
                    // $offer_title = $user_offer->Title;
                    // $offer_description = $user_offer->Description;
                    break;
                }
            }
        }
    }
    return $html;
}

function IsProductPurchased($UserID, $ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Order_item_model');
    $items = $CI->Order_item_model->getOrdersItemsWhere($UserID, $ProductID);
    if (count($items) > 0) {
        return true;
    } else {
        return false;
    }
}

function getNumberFromString($string)
{
    return preg_replace('/[^0-9]/', '', $string);
}

function TermsAcceptedByUser()
{
    $CI = &get_Instance();
    $CI->load->model('User_model');
    if ($CI->session->userdata('user')) {
        $data['UserID'] = $CI->session->userdata['user']->UserID;
        $data['TermsAccepted'] = 1; // 1 means terms accepted
        $termsAccepted = $CI->User_model->getWithMultipleFields($data);
        if ($termsAccepted) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

function get_current_url()
{
    $CI =& get_instance();
    $url = $CI->config->site_url($CI->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}

function getURLIds()
{
    $category_ids = array();
    $collection_ids = array();
    if (isset($_GET['q']) && $_GET['q'] != '')
    {
        $url_sub_categories = explode(' ', $_GET['q']);
        if (count($url_sub_categories) > 0) {
            foreach ($url_sub_categories as $url_sub_category) {
                $sub_str = substr($url_sub_category, strrpos($url_sub_category, '-') + 1);
                if (strpos($sub_str, 's') !== false) {
                    $category_ids[] = getNumberFromString($sub_str);
                } elseif (strpos($sub_str, 'c') !== false) {
                    $collection_ids[] = getNumberFromString($sub_str);
                }
            }
        }
    }

    $myArr['category_ids'] = $category_ids;
    $myArr['collection_ids'] = $collection_ids;
    return $myArr;
}

function product_html($products, $ul_cls = 'items_grid')
{


    $html = "";
    $return_no_product = TRUE;
    
    $html .= '<div class="row">
                        <div class="col-md-12">
                        <ul class="'.$ul_cls.'" id="productsContainer">';
    $type_of_item = "'Product'";
    foreach ($products as $key => $p) {

        $offer_product = checkProductIsInAnyOffer($p->ProductID);
        $ratings = productRatings($p->ProductID);
        $ProductDiscountedPrice = $p->Price;
        $Price = $p->Price;
        if(!empty($offer_product)){
            
            

            $DiscountType = $offer_product['DiscountType'];
            $DiscountFactor = $offer_product['Discount'];
            if ($DiscountType == 'percentage') {
                $Discount = ($DiscountFactor / 100) * $Price;
                if ($Discount > $Price) {
                    $ProductDiscountedPrice = 0;
                } else {
                    $ProductDiscountedPrice = $Price - $Discount;
                }
            } elseif ($DiscountType == 'per item') {
                $Discount = $DiscountFactor;
                if ($Discount > $Price) {
                    $ProductDiscountedPrice = 0;
                } else {
                    $ProductDiscountedPrice = $Price - $DiscountFactor;
                }
            } else {
                $Discount = 0;
                if ($Discount > $Price) {
                    $ProductDiscountedPrice = 0;
                } else {
                    $ProductDiscountedPrice = $Price;
                }
            }

        }
            

        $return_no_product = FALSE;    

        if ($p->IsCorporateProduct == 1 && $p->CorporateMinQuantity > 0) {
            $IsCorporateItem = 1;
        } else {
            $IsCorporateItem = 0;
        }

        if ($p->PriceType == 'kg') {
            $PriceType = lang('price_type_kg');
        } else {
            $PriceType = lang('price_type_item');
        }

        $html .= '<li class="single_product">';
        $html .= '<div class="inbox">';
        $html .= '<a href="' . base_url() . 'product/detail/' . productTitle($p->ProductID) . '"><div class="imgbox" style="background-image:url('. base_url(get_images($p->ProductID, 'product', false)) .')">';
        $html .= '<img src="' . base_url(get_images($p->ProductID, 'product', false)) . '">';
        $html .= '</div></a>';
        $html .= '<a href="' . base_url() . 'product/detail/' . productTitle($p->ProductID) . '">';
        $html .= '<h4>' . $p->Title . ' </h4>';
        $html .= '<h5>'.($ProductDiscountedPrice != $Price ? '<del>'.$Price.' '.lang('SAR').'</del>' : '').' <strong data-price="' . number_format($ProductDiscountedPrice, 2). '" data-pid="'.$p->ProductID.'">' . number_format($ProductDiscountedPrice, 2) . '</strong> '.lang('SAR').'</h5>'.$PriceType;
        if ($p->OutOfStock == 1) {
            $html .= '<small style="font-weight: bold;color: red;">' . lang('out_of_stock') . '</small>';
        }
        $html .= '<div class="description">'.$p->Description.'</div>';
        $html .= '</a>';
        $html .= '<a title="' . lang('click_to_add_to_wishlist') . '" href="javascript:void(0);"
                                   onclick="addToWishlist(' . $p->ProductID . ', ' . $type_of_item . ');"><i
                                            class="fa fa-heart ' . isLiked($p->ProductID, 'product') . '" id="item' . $p->ProductID . '" aria-hidden="true"></i></a>';
        $html .= '<a href="javascript:void(0);" title="' . lang('click_to_add_to_cart') . '"
                                   onclick="addWishlistToCart(' . $p->ProductID . ', ' . $type_of_item . ', ' . $p->Price . ', ' . $IsCorporateItem . ');">
                                    <img src="' . front_assets() . 'images/shopping_basket_black_small.png" class="add_wishlist_to_cart" height="24" width="24" />
                                </a>';
        $html .= '<p class="edStarts text-center"><span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span><span>('.$ratings['total_ratings_count'].')</span></p></div></li>';
    }
    if($return_no_product){
        $html ='<p class="alert alert-danger">'.lang('no_product_available').'</p>';
        return $html;
    }

    $html .= '</ul>
                            </div>
                        </div>';
    return $html;
}
//      <i class="fa fa-cart-plus add_wishlist_to_cart"></i>

function checkProductIsInAnyOffer($product_id){
    $CI = &get_Instance();
    $CI->load->model('Offer_model');
    $offer = $CI->Offer_model->getAllJoinedData(true, 'OfferID', 'EN', 'offers.IsActive = 1 AND DATE(ValidTo) > "' . Date('Y-m-d') . '" AND IsForAll = 1 AND ProductID LIKE "%'.$product_id.'%"');
    $return_offer = array();
    if($offer){
        $return_offer = $offer[0]; 
        
    }else{

            if ($CI->session->userdata('user')) {
            
                $offer_for_you = $CI->Offer_model->getOfferForUser($CI->session->userdata['user']->UserID);
                if(!empty($offer_for_you)){
                    $offer_ids = array_column($offer_for_you, 'OfferID');
                    $offer = $CI->Offer_model->getAllJoinedData(true, 'OfferID', 'EN', 'offers.IsActive = 1 AND DATE(ValidTo) > "' . Date('Y-m-d') . '" AND offers.OfferID IN ('.implode(',',$offer_ids).') AND ProductID LIKE "%'.$product_id.'%"');
                    if($offer){
                        $return_offer = $offer[0];
                    }

                }
            }
    }

    return $return_offer;

}
