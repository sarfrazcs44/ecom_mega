<?php
    Class Brand_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("brands");

        }
        public function getBrandData($as_array = false, $system_language_code = false ,$where = false, $sort = 'ASC', $sort_field = 'SortOrder', $custom_sort = false)// please for now not usable function in text model
        {

        
        $this->db->select('count(products.ProductID) AS TotalProducts,brands.*, brands_text.*');
        $this->db->join('brands_text', 'brands.BrandID = brands_text.BrandID' );
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = brands_text.SystemLanguageID', 'Left');
        $this->db->JOIN('products', 'products.BrandID = brands.BrandID' ,'Left');

        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('brands.Hide', 0);

        if ($custom_sort) {
            $this->db->order_by($sort_field, $sort);
        } else {
            $this->db->order_by('system_languages.IsDefault', 'DESC');
        }
        $this->db->group_by('brands.BrandID');
        $result = $this->db->get($this->table);

        if ($as_array) {
            //return $result->result_array();
            $data = $result->result_array();
        } else {
            $data = $result->result();
        }


        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/


        return $data;
        //return $result->result();
        }



    }