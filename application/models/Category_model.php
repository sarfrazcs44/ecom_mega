<?php
    Class Category_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("categories");

        }


        public function getCompanyData($as_array = false, $where = false, $sort = 'ASC', $sort_field = 'SortOrder', $custom_sort = false,$limit = false,$start = 0)// please for now not usable function in text model
        {

        
        $this->db->select('count(products.ProductID) AS TotalProducts,categories.*, categories_text.*');
        $this->db->join('categories_text', 'categories.CategoryID = categories_text.CategoryID' );
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = ' . $this->table . '_text.SystemLanguageID', 'Left');
        $this->db->JOIN('products', 'products.CategoryID = categories.CategoryID' ,'Left');

        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.Hide', 0);

        if ($custom_sort) {
            $this->db->order_by($sort_field, $sort);
        } else {
            $this->db->order_by('system_languages.IsDefault', 'DESC');
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->group_by('categories.CategoryID');
        $result = $this->db->get($this->table);

        if ($as_array) {
            //return $result->result_array();
            $data = $result->result_array();
        } else {
            $data = $result->result();
        }


        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/


        return $data;
        //return $result->result();
        }

    }