<?php
    Class Collection_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("collections");

        }



        public function getCollectionProductsSubCategories($collection,$language_code){

        	$collection = json_decode($collection);

        	$this->db->select('products.SubCategoryID,categories.CategoryID,categories_text.Title');
        	$this->db->from('products');
        	$this->db->join('categories','categories.CategoryID = products.SubCategoryID');
        	$this->db->join('categories_text','categories_text.CategoryID = categories.CategoryID');
        	$this->db->join('system_languages','system_languages.SystemLanguageID = categories_text.SystemLanguageID');
       		$this->db->where('system_languages.ShortCode',$language_code);
       		$products  = explode(',',$collection->ProductID);
       		if(COUNT($products)){
       			$this->db->where_in('products.ProductID',$products);

       		}else{
       			$this->db->where('products.ProductID',$collection->ProductID);

       		}

       		
       		return $this->db->get()->result();



        }



        public function getCollectionProducts($collection){

          

          $this->db->select('collections.ProductID');
          $this->db->from('collections');
          
          
          if(COUNT($collection) > 1)
          {
            $this->db->where_in('collections.CollectionID',$collection);

          }else{
            $this->db->where('collections.CollectionID',$collection[0]);

          }

          
          return $this->db->get()->result();



        }


    }