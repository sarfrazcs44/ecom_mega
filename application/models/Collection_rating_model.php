<?php
Class Collection_rating_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("collection_ratings");

    }

    public function getAverageRating($CollectionID)
    {
        $this->db->select('AVG(Rating) as average_rating');
        $this->db->from('collection_ratings');
        $this->db->where('CollectionID', $CollectionID);
        $result = $this->db->get();
        return $result->row();
    }

}