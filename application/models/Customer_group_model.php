<?php

Class Customer_group_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("customer_groups");

    }

    public function getCustomerGroups($where = false, $limit = false, $start = 0, $sort = 'DESC')
    {

        $this->db->select('*');
        $this->db->from('customer_groups');
       

        if ($where) {
            $this->db->where($where);
        }

        $this->db->group_by('customer_groups.CustomerGroupID');
        $this->db->order_by('customer_groups.CreatedAt', $sort);

        if ($limit) {
            $this->db->limit($limit, $start);
        }


        $result = $this->db->get();

        //echo $this->db->last_query();exit();
        return $result->result();


    }


}