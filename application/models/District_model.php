<?php
    Class District_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("districts");

        }



        public function getAllDistricts($language_code,$city_id = false,$is_active = false,$where = false, $sort_field = 'SortOrder',$sort = 'ASC',$limit = false,$start = 0,$child_table_sort = false){
        $this->db->select('districts.*,districts_text.Title,cities_text.Title as CityTitle');
        $this->db->from('districts');
        $this->db->join('districts_text','districts.DistrictID = districts_text.DistrictID');

        $this->db->join('system_languages','system_languages.SystemLanguageID = districts_text.SystemLanguageID');

        $this->db->join('cities','cities.CityID = districts.CityID','Left');
        $this->db->join('cities_text','cities_text.CityID = cities.CityID');
        
        $this->db->where('system_languages.ShortCode',$language_code);
        if($is_active){
           $this->db->where('districts.IsActive',$is_active); 
        }
        
       
        $this->db->where('districts.Hide',0);
        if($city_id){
           $this->db->where('districts.CityID',$city_id); 
        }
        $this->db->group_by('districts.DistrictID');
        

        if ($where) {
            $this->db->where($where);
        }
        $this->db->where($this->table . '.Hide', '0');
        if($limit){
            $this->db->limit($limit, $start);
        }
     
        if($child_table_sort){
            $this->db->order_by('districts_text.'.$sort_field, $sort);
        }
        
        //$this->db->where('system_languages.ShortCode',$language_code);
        
        
        //$this->db->where('districts.Hide',0);
        
        return $this->db->get()->result();
        
        
    }
        
        
    
    

    }