<?php

Class Model_general extends CI_Model

{

    public function __construct()

    {

        parent::__construct();

    }


    public function getRow($id, $table_name, $as_array = false)

    {

        $result = $this->db->get_where($table_name, array('id' => $id));

        //echo $this->db->last_query(); exit();

        if ($result->num_rows() > 0) {

            $row = $result->row_array();


            if ($as_array) {

                return $row;

            }


            foreach ($row as $col => $val) {

                $this->$col = $val;

            }


            return $this;

        } else {

            return false;

        }

    }


    public function getSingleRow($table_name, $field, $as_array = false)

    {

        $result = $this->db->get_where($table_name, $field);


        if ($result->num_rows() > 0) {

            $row = $result->row_array();


            if ($as_array) {

                return $row;

            }


            foreach ($row as $col => $val) {

                $this->$col = $val;

            }


            return $this;

        } else {

            return false;

        }

    }


    public function getAll($table_name, $as_array = false, $idOrderBy = 'asc', $orderBy = 'id')

    {


        $this->db->order_by($orderBy, $idOrderBy);


        $result = $this->db->get($table_name);


        if ($as_array) {

            return $result->result_array();

        }


        return $result->result();

    }


    public function getMultipleRows($table_name, $fields, $as_array = false, $idOrderBy = 'asc', $orderBy = 'id', $limit = false, $start = 0)

    {


        $this->db->order_by($orderBy, $idOrderBy);

        if ($limit) {
            $this->db->limit($limit, $start);
        }


        $result = $this->db->get_where($table_name, $fields);


        //echo $this->db->last_query(); exit();


        if ($result->num_rows() > 0) {


            if ($as_array) {

                return $result->result_array();

            }


            return $result->result();

        } else {

            return false;

        }

    }

    /* Insert user */

    public function save($table, $data)
    {
        $this->db->set($data);
        $this->db->insert($table);
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {

            return $insertId;
        } else {
            return false;
        }
    }

    public function updateRow($table_name, $data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update($table_name, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateAllRows($table_name, $data)
    {
        $this->db->where(1, 1);
        $this->db->update($table_name, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateWhere($table_name, $data, $fields)
    {
        $this->db->where($fields);
        $this->db->update($table_name, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    public function approveProject($id)
    {

        $query = $this->db->query("UPDATE projects SET active_status = 1 WHERE  id = '" . $id . "'");

        return true;


    }


    public function verifyrequests()
    {

        $query = $this->db->query("SELECT * FROM  users where is_verified = 1");

        return $query->result();


    }


    public function deleteRow($table_name, $id)
    {
        $this->db->where('id', $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }

    public function deleteMultipleRow($table_name, $field, $id)
    {
        $this->db->where($field, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }

    public function deleteWhere($table_name, $fields)
    {
        $this->db->where($fields);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }


    public function validateLogin($username, $password)
    {
        $q = "select * from users where username = '" . $username . "' and password = '" . $password . "' and status = 'active' and user_type_id != 1";
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getRecordsCount($table_name)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function getRecordsCountWhere($table_name, $fields)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($fields);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getMonthlyTimelogsForEmployee()
    {
        $month = date('m');
        $year = date('Y');
        $sql = "SELECT t.*, CONCAT(e.first_name,' ',e.last_name) as employee_name,
                CASE WHEN t.checkin_time IS NULL THEN '' ELSE DATE_FORMAT(t.checkin_time, '%l:%i:%s %p') END AS checkin_time,
                CASE WHEN t.checkout_time IS NULL THEN '' ELSE DATE_FORMAT(t.checkout_time, '%l:%i:%s %p') END AS checkout_time,
                CASE WHEN t.checkin_time != '00:00:00' THEN 1 ELSE 0 END AS checked_in, 
                CASE WHEN t.checkout_time != '00:00:00' THEN 1 ELSE 0 END AS checked_out, 
                CASE WHEN t.seating_hours IS NULL THEN '' ELSE t.seating_hours END AS seating_hours,
                DATE_FORMAT(t.timelog_date, '%a, %D %M %Y') as timelog_date
                FROM timelogs t JOIN employees e ON t.employee_id = e.id AND MONTH(t.timelog_date) = $month AND YEAR(t.timelog_date) = $year";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAllUsers()
    {
        $sql = "SELECT u.*, CONCAT(e.first_name,' ',e.last_name) as employee_name, e.email as employee_email, a.name as admin_name, a.email as admin_email, ut.title as user_type FROM users u LEFT JOIN employees e ON u.id = e.user_id LEFT JOIN admins a ON u.id=a.user_id LEFT JOIN user_types ut ON u.user_type_id=ut.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAllEmployees()
    {
        $sql = "SELECT e.*, ut.title as user_type FROM users u JOIN employees e ON u.id = e.user_id JOIN user_types ut ON u.user_type_id=ut.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAllAdminUsers()
    {
        $sql = "SELECT a.*,u.username FROM admins a LEFT JOIN users u ON a.user_id=u.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAllMessages()
    {
        $sql = "SELECT e.*, ma.subject as subject, ma.message as message, ma.created_at as received_at, ma.id as message_id FROM message_administrator ma JOIN employees e ON ma.employee_id = e.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAllTasks($project_id = "")
    {
        $base_url = base_url();
        $sql = "SELECT t.id as task_id, p.id as project_id, p.title as project_title, t.deadline as task_deadline, t.title as task_title, t.status as task_status, t.description as task_description, e.id as responsible_person_id, CONCAT(e.first_name,' ',e.last_name) as responsible_person_name, CONCAT('$base_url',e.image) as responsible_person_image, (SELECT count(*) FROM comments c JOIN tasks ta ON c.task_id = ta.id) as task_comments_count FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id JOIN projects p ON t.project_id = p.id JOIN employees e ON ta.employee_id = e.id";
        if ($project_id > 0)
        {
            $sql .= " AND t.project_id = $project_id";
        }
        $sql .= " GROUP BY t.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAllLeaveRequests()
    {
        $sql = "SELECT lr.*, CONCAT(e.first_name,' ',e.last_name) as employee_name, e.id as employee_id, lt.leave_type as type_of_leave from leave_requests lr JOIN employees e ON lr.employee_id = e.id JOIN leave_types lt ON lr.leave_type_id = lt.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAllShortLeaveRequests()
    {
        $sql = "SELECT slr.*, CONCAT(e.first_name,' ',e.last_name) as employee_name, e.id as employee_id from short_leave_requests slr JOIN employees e ON slr.employee_id = e.id AND slr.correction_requested = 0";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getShortLeaveCorrectionRequests()
    {
        $sql = "SELECT slr.*, CONCAT(e.first_name,' ',e.last_name) as employee_name, e.id as employee_id from short_leave_requests slr JOIN employees e ON slr.employee_id = e.id AND slr.correction_requested = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getTimelogCorrectionRequests()
    {
        $sql = "SELECT tcr.*, t.timelog_date, CONCAT(e.first_name,' ',e.last_name) as employee_name, e.id as employee_id from timelog_correction_requests tcr JOIN employees e ON tcr.employee_id = e.id JOIN timelogs t ON tcr.timelog_id = t.id ORDER BY tcr.id DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getTaskDetail($task_id)
    {
        $base_url = base_url();
        $sql = "SELECT t.id as task_id, p.id as project_id, p.title as project_title, t.deadline as task_deadline, t.title as task_title, t.status as task_status, t.description as task_description, e.id as responsible_person_id, CONCAT(e.first_name,' ',e.last_name) as responsible_person_name, CONCAT('$base_url',e.image) as responsible_person_image, (SELECT count(*) FROM comments c JOIN tasks ta ON c.task_id = ta.id) as task_comments_count, (SELECT count(*) FROM task_checklist tc JOIN tasks ta1 ON tc.task_id = ta1.id) as checklist_count FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id JOIN projects p ON t.project_id = p.id JOIN employees e ON ta.employee_id = e.id AND ta.task_id = $task_id GROUP BY t.id";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getTaskChecklist($task_id)
    {
        $base_url = base_url();
        $sql = "SELECT tc.*, CONCAT(e.first_name,' ',e.last_name) as employee_name, CONCAT('$base_url',e.image) as employee_image FROM task_checklist tc JOIN employees e ON tc.employee_id = e.id AND tc.task_id = $task_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getAllCommentsForTask($task_id)
    {
        $base_url = base_url();
        $sql = "SELECT e.id as employee_id, CONCAT( e.first_name,  ' ', e.last_name ) AS employee_name, CONCAT('$base_url',e.image) as employee_image, e.designation as designation, c.id as comment_id, c.comment as comment, c.has_attachment as has_attachment, ca.type AS file_type, CONCAT('$base_url',ca.file) AS file, c.created_at as commented_at FROM comments c LEFT JOIN employees e ON c.employee_id = e.id LEFT JOIN comment_attachments ca ON c.id = ca.comment_id WHERE c.task_id = $task_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getAllNotifications($employee_id)
    {
        $sql = "SELECT nc.id, nc.type, nc.employee_id, nc.project_id, nc.task_id, nc.title, nc.notification, nc.created_at as original_created_at, DATE_FORMAT(nc.created_at, '%a, %D %M %Y %l:%i %p') as created_at, p.title as project_title from notification_center nc JOIN projects p ON nc.project_id = p.id WHERE employee_id = $employee_id ORDER BY original_created_at DESC LIMIT 50";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function checkIfAnyTaskIsActive($employee_id)
    {
        $sql = "SELECT * FROM task_activity WHERE employee_id = $employee_id AND from_time IS NOT NULL AND to_time IS NULL";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


}