<?php
Class Notification_cron_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("notification_cron_job_setting");

    }



    public function getUsersDataForSendingNotification($where = false,$limit = false,$start =0)// please for now not usable function in text model
    {

        $first_field = 'users' . '.' . 'UserID';
        $second_field = 'users' . '_text.' . 'UserID';
        $this->db->select('users' . '.*, ' . 'users' . '_text.*');
        $this->db->join('users' . '_text', $first_field . ' = ' . $second_field);
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID', 'Left');

        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.Hide', 0);


        if ($limit) {
            $this->db->limit($limit, $start);
        }

       
         

        $result = $this->db->get('users');

       
        return $result->result_array();
    }


}