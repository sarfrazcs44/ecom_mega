<?php

Class Offer_user_notification_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("offers_users_notification");

    }

    public function getAllUserOffers($UserID, $system_language_code = 'EN')
    {
        $today = date('Y-m-d');
        $this->db->select('offers_users_notification.*,offers.*,offers_text.*');
        $this->db->from('offers_users_notification');
        $this->db->join('offers', 'offers_users_notification.OfferID = offers.OfferID');
        $this->db->join('offers_text', 'offers.OfferID = offers_text.OfferID');
        $this->db->join('system_languages as slot', 'slot.SystemLanguageID = offers_text.SystemLanguageID', 'Left');
        $this->db->where('offers_users_notification.UserID', $UserID);
        $this->db->where('offers.ValidFrom <=', $today);
        $this->db->where('offers.ValidTo >=', $today);
        $this->db->where('offers.IsActive', 1);
        if ($system_language_code) {
            $this->db->where('slot.ShortCode', $system_language_code);
        } else {
            $this->db->where('slot.IsDefault', '1');
        }
        $this->db->order_by('offers_users_notification.OfferID');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return false;
        }
    }

    public function getUserOffers($UserID, $system_language_code = 'EN')
    {
        $today = date('Y-m-d');
        $this->db->select('offers_users_notification.*,offers_text.*');
        $this->db->from('offers_users_notification');
        $this->db->join('offers', 'offers_users_notification.OfferID = offers.OfferID');
        $this->db->join('offers_text', 'offers.OfferID = offers_text.OfferID');
        $this->db->join('system_languages as slot', 'slot.SystemLanguageID = offers_text.SystemLanguageID', 'Left');
        $this->db->where('offers_users_notification.UserID', $UserID);
        $this->db->where('offers_users_notification.IsShow', 0);
        $this->db->where('offers.ValidFrom <=', $today);
        $this->db->where('offers.ValidTo >=', $today);
        $this->db->where('offers.IsActive', 1);
        if ($system_language_code) {
            $this->db->where('slot.ShortCode', $system_language_code);
        } else {
            $this->db->where('slot.IsDefault', '1');
        }
        $this->db->group_by('offers_users_notification.OfferID');
        $this->db->order_by('offers_users_notification.OfferID');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return false;
        }
    }


}