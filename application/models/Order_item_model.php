<?php
Class Order_item_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("order_items");

    }

    public function getOrdersItemsOld($order_id)
    {
        $this->db->select('orders.*, order_items.*, products.*, products_text.*');
        $this->db->from('orders');
        $this->db->join('order_items', 'orders.OrderID = order_items.OrderID', 'LEFT');
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID', 'LEFT');
        $this->db->join('system_languages as slpt', 'slpt.SystemLanguageID = products_text.SystemLanguageID', 'Left');
        $this->db->where('slpt.IsDefault', '1');
        $this->db->where('orders.OrderID', $order_id);
        $this->db->group_by('order_items.OrderItemID');
        $result = $this->db->get();
        return $result->result();


    }

    public function getOrdersItems($order_id, $system_language_code = 'EN')
    {
        $this->db->select("orders.*, order_items.*,products_text.Title,products.SKU,products_text.Description,products.Price,products.PriceType,products.IsCorporateProduct");
        $this->db->from('order_items');
        $this->db->join('orders', 'order_items.OrderID = orders.OrderID', 'LEFT');
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID', 'LEFT');
        $this->db->join("system_languages slpt", "products_text.SystemLanguageID = slpt.SystemLanguageID AND slpt.ShortCode = '$system_language_code'", "LEFT");
        $this->db->where('orders.OrderID', $order_id);
        $this->db->group_by('order_items.OrderItemID');
        $result = $this->db->get();
        return $result->result();


    }

    public function getOrdersItemsWhere($UserID, $ProductID, $system_language_code = 'EN')
    {
        $this->db->select("orders.*, order_items.*,products_text.Title,products.Price,products.IsCorporateProduct");
        $this->db->from('order_items');
        $this->db->join('orders', 'order_items.OrderID = orders.OrderID', 'LEFT');
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID', 'LEFT');
        $this->db->join("system_languages slpt", "products_text.SystemLanguageID = slpt.SystemLanguageID AND slpt.ShortCode = '$system_language_code'", "LEFT");
        $this->db->where('order_items.ProductID', $ProductID);
        $this->db->where('order_items.ItemType', 'Product');
        $this->db->where('orders.UserID', $UserID);
        $this->db->group_by('order_items.OrderItemID');
        $result = $this->db->get();
        return $result->result();


    }
}