<?php
Class Pos_model extends Base_Model
{
	public function __construct()
	{
		parent::__construct("point_of_sales");
		
	}


	public function getData($where = false,$limit = false, $start = 0, $sort = 'DESC')
    {
        $this->db->select('*');
        $this->db->from('point_of_sales');
        if($where){
        	$this->db->where($where);
        }


        $this->db->order_by('point_of_sales.CreatedAt', $sort);

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        return $this->db->get()->result();
    }
    
    
    public function getDataCount($where = false)
    {

        $this->db->select('*');
        $this->db->from('point_of_sales');
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        return $query->num_rows();
    }
	
		
}