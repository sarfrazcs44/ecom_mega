<?php

Class Product_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("products");

    }

    public function getProducts($where = false, $system_language_code = false, $limit = false, $start = 0, $sort_by = 'products_text.Title', $sort_as = 'ASC',$rating = array())
    {

        $langData= getLanguageBy(array('ShortCode' => $system_language_code));

        if(!empty($rating)){
            $having = '(';
            $this->db->select('IFNULL(AVG(product_ratings.Rating),0) as Rating');
            $this->db->join('product_ratings', 'products.ProductID = product_ratings.ProductID','left');
            foreach ($rating as $key => $value) {
               $having .= 'Rating = '.$value.' OR ';
            }
            $having  = rtrim($having,' OR ');
            $having  .= ')';

            $this->db->having($having);
        }
        $this->db->select('products.*,products_text.*,site_images.ImageName,categories_text.Title as CategoryTitle,subCategory_text.Title as SubCategoryTitle,brands_text.Title as BrandTitle');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('brands_text', 'products.BrandID = brands_text.BrandID');
        $this->db->Join('site_images', 'products.ProductID = site_images.FileID AND site_images.ImageType = "product"','Left');
        $this->db->join('categories_text subCategory_text', 'products.CategoryID = subCategory_text.CategoryID AND subCategory_text.SystemLanguageID ='.$langData->SystemLanguageID);

        $this->db->join('categories_text', 'products.CategoryID = categories_text.CategoryID AND categories_text.SystemLanguageID ='.$langData->SystemLanguageID);


        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $this->db->order_by($sort_by, $sort_as);
        $this->db->group_by('products.ProductID');


        $result = $this->db->get();
        return $result->result();
    }


    public function getBestSellingProducts($where = false, $system_language_code = false, $limit = false, $start = 0, $sort_by = 'products_text.Title', $sort_as = 'DESC',$rating = array())
    {

        $langData= getLanguageBy(array('ShortCode' => $system_language_code));

        if(!empty($rating)){
            $having = '(';
            $this->db->select('IFNULL(AVG(product_ratings.Rating),0) as Rating');
            $this->db->join('product_ratings', 'products.ProductID = product_ratings.ProductID','left');
            foreach ($rating as $key => $value) {
               $having .= 'Rating = '.$value.' OR ';
            }
            $having  = rtrim($having,' OR ');
            $having  .= ')';

            $this->db->having($having);
        }
        $this->db->select('count(order_items.ProductID) AS OrderCounts,  products.*,products_text.*, site_images.ImageName, categories_text.Title as CategoryTitle, subCategory_text.Title as SubCategoryTitle, brands_text.Title as BrandTitle');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = '.$langData->SystemLanguageID);
        $this->db->join('order_items', 'products.ProductID = order_items.ProductID');
        $this->db->join('brands_text', 'products.BrandID = brands_text.BrandID AND brands_text.SystemLanguageID ='.$langData->SystemLanguageID);
        $this->db->Join('site_images', 'products.ProductID = site_images.FileID AND site_images.ImageType = "product"','Left');
        $this->db->join('categories_text subCategory_text', 'products.CategoryID = subCategory_text.CategoryID AND subCategory_text.SystemLanguageID ='.$langData->SystemLanguageID);

        $this->db->join('categories_text', 'products.CategoryID = categories_text.CategoryID AND categories_text.SystemLanguageID ='.$langData->SystemLanguageID);


        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $this->db->order_by($sort_by, $sort_as);
        $this->db->group_by('products.ProductID');


        $result = $this->db->get();
        return $result->result();
    }

    public function getStoreAvailability($where = false, $system_language_code = 'EN')
    {
        $this->db->select('products.*,products_text.*,product_store_availability.StoreID,product_store_availability.Quantity as AvailableQuantity');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('product_store_availability', 'products.ProductID = product_store_availability.ProductID','left');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);

        


        $result = $this->db->get();
        return $result->row_array();
    }


    public function getCountProducts($where = false, $system_language_code = 'EN', $limit = false, $start = 0)
    {
        $this->db->select('COUNT(products.ProductID) as Total');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);

        if ($limit) {
            $this->db->limit($limit, $start);
        }


        $result = $this->db->get();
        return $result->row();
    }

    public function getProductsOfCollection($product_ids, $system_language_code = 'EN', $limit = false, $start = 0, $count = false)
    {
        if ($count) {
            $this->db->select('COUNT(products.ProductID) as Total');

        } else {
            $this->db->select('products.*,products_text.*');

        }


        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where_in('products.ProductID', $product_ids);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        return $result->result();
    }

    public function getWishlistItems($UserID, $system_language_code = 'EN')
    {
        $this->db->select("user_wishlist.*,products_text.Title,products.ProductID,products.Price,products.IsCorporateProduct");
        $this->db->from('user_wishlist');
        $this->db->join('products', 'user_wishlist.ItemID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('user_wishlist.UserID', $UserID);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        return $result->result();
    }

    public function searchProducts($value, $system_language_code = 'EN')
    {
        $this->db->select('products.*,products_text.*');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->like('products_text.Title', $value, 'both');
        $this->db->where('products.IsCustomizedProduct', 0);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        return $result->result();
    }

    public function getProductDetail($product_id, $system_language_code = 'EN')
    {
        $this->db->select('products.*,products_text.*');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('products.ProductID', $product_id);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        return $result->row();
    }

    public function getProductsWhereIn($category_ids = false, $system_language_code = 'EN', $limit = false, $start = 0, $sort_by = 'products_text.Title', $sort_as = 'ASC')
    {
        $this->db->select('products.*,products_text.*');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');

        //$this->db->where_in('products.SubCategoryID', $category_ids);
        $find_in = '';
        foreach ($category_ids as $key => $value){
             if($key == 0){
                $find_in .= '( ';
             }else{
                $find_in .= ' OR ';
             }
            $find_in .= 'FIND_IN_SET('.$value.', products.SubCategoryID)';

            
        }
        if($find_in  != ''){
            $find_in .= ' )';
            $this->db->where($find_in);
        }
        
        //$this->db->where('FIND_IN_SET($category_ids, products.SubCategoryID) > 0');

        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('products.IsActive', 1);

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $this->db->order_by($sort_by, $sort_as);


        $result = $this->db->get();
        //echo $this->db->last_query();exit;
        
        return $result->result();
    }

    public function getProductsInCart($where = false)
    {
        $this->db->select("Count(order_items.ProductID) as TotalCart,order_items.*,products_text.Title,products.SKU,products.Price");
        $this->db->from('order_items');
        
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = 1', 'LEFT');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('order_items.ProductID');
        $result = $this->db->get();
        return $result->result();


    }

    public function getProductsReviews($where = false)
    {
        $this->db->select("Count(product_reviews.ProductID) as TotalReviews,products_text.Title,products.SKU,products.ProductID");
        $this->db->from('product_reviews');
        
        $this->db->join('products', 'product_reviews.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = 1', 'LEFT');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('product_reviews.ProductID');
        $result = $this->db->get();
        return $result->result();


    }

    public function getReviews($where = false)
    {
        $this->db->select("product_reviews.*,product_reviews.Title As ReviewTitle,products_text.Title,products.SKU,products.ProductID");
        $this->db->from('product_reviews');
        
        $this->db->join('products', 'product_reviews.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = 1', 'LEFT');
        if($where){
            $this->db->where($where);
        }
        
        
        $result = $this->db->get();
        return $result->result();


    }


    public function getCities($where = false,$language = 'EN')
    {
        $selected_lang = getLanguageBy(array('ShortCode' => $language));
        $this->db->select("cities.CityID,cities_text.Title as CityTitle,stores.DistrictID,stores.StoreID,stores.Latitude,stores.Longitude,stores_text.Address,stores_text.Title as StoreTitle,product_store_availability.Quantity");
        $this->db->from('cities');
        
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = '.$selected_lang->SystemLanguageID);
        $this->db->join('stores', 'stores.CityID = cities.CityID');
        $this->db->join('stores_text', 'stores.StoreID = stores_text.StoreID AND stores_text.SystemLanguageID = '.$selected_lang->SystemLanguageID);
        $this->db->join('product_store_availability', 'product_store_availability.StoreID = stores.StoreID','left');
        if($where){
            $this->db->where($where);
        }


        $this->db->order_by('cities_text.Title','ASC');
        $this->db->group_by('stores.StoreID');
        
        
        $result = $this->db->get();

        return $result->result();


    }

    public function getCountCustomerReviews($where = false)
    {
        $this->db->select("Count(product_reviews.UserID) as TotalReviews,users.UserID,users_text.FullName");
        $this->db->from('product_reviews');
        
        $this->db->join('users', 'product_reviews.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1', 'LEFT');
        if($where){
            $this->db->where($where);
        }
        $this->db->group_by('product_reviews.UserID');
        $result = $this->db->get();
        return $result->result();


    }

    public function getAllProductsForBackend($data , $limit = false,$start = 0, $system_language_code = 'EN')
  {
      
      $this->db->select('p.*,pt.*,brands_text.Title as BrandTitle,ct.Title as CategoryTitle,c.Image as CategoryImage,sb.Image as SubCategoryImage,sbt.Title as SubCategoryTitle');
      $this->db->from('products p');
      $this->db->join('products_text pt','p.ProductID = pt.ProductID','left');
      $this->db->join('categories c','p.CategoryID = c.CategoryID','left');
      $this->db->join('categories_text ct','c.CategoryID = ct.CategoryID','left');


      $this->db->join('categories sb','p.SubCategoryID = sb.CategoryID','left');
      $this->db->join('categories_text sbt','sb.CategoryID = sbt.CategoryID','left');
      $this->db->join('brands', 'p.BrandID = brands.BrandID','left');
       $this->db->join('brands_text', 'brands.BrandID = brands_text.BrandID','left');
      
      $this->db->join('system_languages', 'pt.SystemLanguageID = system_languages.SystemLanguageID');
      $this->db->where('system_languages.ShortCode', $system_language_code);
      
      if(isset($data['CategoryID'])){
          $this->db->where('p.CategoryID',$data['CategoryID']);
      }
      if(isset($data['ProductID'])){
          $this->db->where('p.ProductID',$data['ProductID']);
      }
      if(isset($data['SubCategoryID'])){
          $this->db->where('p.SubCategoryID',$data['SubCategoryID']);
      }
      
      
      if(isset($data['IsFeatured'])){
          $this->db->where('p.IsFeatured',$data['IsFeatured']);
      }

      if(isset($data['CompanyID'])){
          $this->db->where('p.CompanyID',$data['CompanyID']);
      }

      if ($limit)
      {
          $this->db->limit($limit, $start);
      }
      
      
      return $this->db->get()->result_array();
          
      
  }


public function getSalesByBrand($as_array = false, $system_language_code = false, $where = false, $sort = 'ASC', $sort_field = 'SortOrder',$child_table_sort = false,$limit = false,$start = 0)
    {
        $this->db->select('SUM(products.PurchaseCount) as TotalSales,brands_text.Title as BrandTitle');
        
       
        $this->db->join('brands_text', 'products.BrandID = brands_text.BrandID AND SystemLanguageID = 1');
        
       
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where($this->table . '.Hide', '0');
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        $this->db->order_by('products.' . $sort_field, $sort);
        $this->db->group_by('products.BrandID');
        $result = $this->db->get($this->table);
        if ($as_array) {

            $data = $result->result_array();
        } else {
            $data = $result->result();
        }
        
        return $data;
    }

    function countSalesByBrand($where = false)
    {

       
        


        $this->db->select('SUM(products.PurchaseCount) as TotalSales,brands_text.Title as BrandTitle');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('brands_text', 'products.BrandID = brands_text.BrandID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');





        if($where){
            $this->db->where($where);
        }
        $this->db->group_by('products.BrandID');
        $query = $this->db->get();
        return $query->num_rows();
    }



}