<?php

Class Product_rating_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("product_ratings");

    }

    public function getAverageRating($ProductID)
    {
        $this->db->select('AVG(Rating) as average_rating');
        $this->db->from('product_ratings');
        $this->db->where('ProductID', $ProductID);
        $result = $this->db->get();
        return $result->row();
    }

}