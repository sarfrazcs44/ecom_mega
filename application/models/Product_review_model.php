<?php
Class Product_review_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("product_reviews");

    }

    public function getUserJoinedData($as_array = false, $join_field, $second_table, $where = false)// please for now not usable function in text model
    {

        $first_field = $this->table . '.' . $join_field;
        $second_field = $second_table . '.' . $join_field;
        $this->db->select($this->table . '.*, ' . $second_table . '.*,users.*');
        $this->db->join($second_table, $first_field . ' = ' . $second_field);
        $this->db->join('users','product_reviews.UserID = users.UserID');
        if ($where) {
            $this->db->where($where);
        }

        //$this->db->order_by($this->table.'_cod.Code','asc');
        $result = $this->db->get($this->table);
        //echo $this->db->last_query();exit();
        if ($as_array) {
            // return $result->result_array();
            return $data = $result->result_array();
        }

        $data = $result->result();

        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/

        /*foreach ($data as $row) {
            if($as_array) {
                if($row['Code'] == ''){
                    $row['Code'] = $this->GetDefaultLangData($join_field , $row[$join_field]);
                }
            } else {
                if($row->Code == ''){
                    $row->Code = $this->GetDefaultLangData($join_field , $row->$join_field);
                }
            }

        }*/

        return $data;
        //return $result->result();
    }

}