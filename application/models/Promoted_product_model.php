<?php

Class Promoted_product_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("promoted_products");

    }

    public function getPromotedProducts($where = false, $system_language_code = 'EN', $data_array = false, $type = 'categories')
    {
        $this->db->select('users.*,users_text.*,products.*,products_text.*,cities_text.Title as CityName, ssct.Title as SubSubCategoryName, sct.Title as SubCategoryName, ct.Title as CategoryName, promotions.PromotionID, promotions.PromotionImage, promotions_text.Title as PromotionTitle, promotions_text.Description as PromotionDescription');

        $this->db->from('promoted_products');

        $this->db->join('promotions', 'promoted_products.PromotionID = promotions.PromotionID', 'LEFT');
        $this->db->join('promotions_text', 'promotions.PromotionID = promotions_text.PromotionID');

        $this->db->join('products', 'promoted_products.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');

        $this->db->join('users', 'users.UserID = products.UserID', 'LEFT');
        $this->db->join('users_text', 'users_text.UserID = users.UserID AND users_text.SystemLanguageID = 1');

        $this->db->join('cities', 'cities.CityID = users.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID');

        $this->db->join('countries', 'cities.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('countries_text', 'countries.CountryID = countries_text.CountryID');

        // sub sub category
        $this->db->join('categories ssc', 'products.CategoryID = ssc.CategoryID', 'LEFT');
        $this->db->join('categories_text ssct', 'ssc.CategoryID = ssct.CategoryID');

        // sub category
        $this->db->join('categories sc', 'ssc.ParentID = sc.CategoryID', 'LEFT');
        $this->db->join('categories_text sct', 'sc.CategoryID = sct.CategoryID');

        // category
        $this->db->join('categories c', 'sc.ParentID = c.CategoryID', 'LEFT');
        $this->db->join('categories_text ct', 'c.CategoryID = ct.CategoryID');

        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->join('system_languages slctt', 'cities_text.SystemLanguageID = slctt.SystemLanguageID');
        $this->db->join('system_languages slpt', 'countries_text.SystemLanguageID = slpt.SystemLanguageID');
        $this->db->join('system_languages slssct', 'ssct.SystemLanguageID = slssct.SystemLanguageID');
        $this->db->join('system_languages slsct', 'sct.SystemLanguageID = slsct.SystemLanguageID');
        $this->db->join('system_languages slct', 'ct.SystemLanguageID = slct.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }

        if ($data_array) {
            if ($type === 'categories') {
                $this->db->where_in('ssc.ParentID', $data_array);
            } elseif ($type === 'booths') {
                $this->db->where_in('products.UserID', $data_array);
            }
        }

        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('slctt.ShortCode', $system_language_code);
        $this->db->where('slpt.ShortCode', $system_language_code);
        $this->db->where('slssct.ShortCode', $system_language_code);
        $this->db->where('slsct.ShortCode', $system_language_code);
        $this->db->where('slct.ShortCode', $system_language_code);

        $this->db->group_by('products.ProductID');

        $this->db->order_by('products_text.Title', 'ASC');


        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getPromotedProductsForUserFollowBooth($where = false, $system_language_code = 'EN', $data_array = false, $type = 'categories')
    {
        $this->db->select('users.*,users_text.*,products.*,products_text.*,cities_text.Title as CityName, ssct.Title as SubSubCategoryName, sct.Title as SubCategoryName, ct.Title as CategoryName,promotions.PromotionUrl,promotions.PromotionType ,promotions.PromotionID, promotions.PromotionImage, promotions_text.Title as PromotionTitle, promotions_text.Description as PromotionDescription');

        $this->db->from('promoted_products');

        $this->db->join('promotions', 'promoted_products.PromotionID = promotions.PromotionID', 'LEFT');
        $this->db->join('promotions_text', 'promotions.PromotionID = promotions_text.PromotionID');

        $this->db->join('products', 'promoted_products.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');

        $this->db->join('users', 'users.UserID = products.UserID', 'LEFT');
        $this->db->join('users_text', 'users_text.UserID = users.UserID AND users_text.SystemLanguageID = 1');

        $this->db->join('cities', 'cities.CityID = users.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID');

        $this->db->join('countries', 'cities.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('countries_text', 'countries.CountryID = countries_text.CountryID');

        // sub sub category
        $this->db->join('categories ssc', 'products.CategoryID = ssc.CategoryID', 'LEFT');
        $this->db->join('categories_text ssct', 'ssc.CategoryID = ssct.CategoryID');

        // sub category
        $this->db->join('categories sc', 'ssc.ParentID = sc.CategoryID', 'LEFT');
        $this->db->join('categories_text sct', 'sc.CategoryID = sct.CategoryID');

        // category
        $this->db->join('categories c', 'sc.ParentID = c.CategoryID', 'LEFT');
        $this->db->join('categories_text ct', 'c.CategoryID = ct.CategoryID');

        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->join('system_languages slctt', 'cities_text.SystemLanguageID = slctt.SystemLanguageID');
        $this->db->join('system_languages slpt', 'countries_text.SystemLanguageID = slpt.SystemLanguageID');
        $this->db->join('system_languages slssct', 'ssct.SystemLanguageID = slssct.SystemLanguageID');
        $this->db->join('system_languages slsct', 'sct.SystemLanguageID = slsct.SystemLanguageID');
        $this->db->join('system_languages slct', 'ct.SystemLanguageID = slct.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }

        $this->db->where_in('products.UserID', $data_array);

        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('slctt.ShortCode', $system_language_code);
        $this->db->where('slpt.ShortCode', $system_language_code);
        $this->db->where('slssct.ShortCode', $system_language_code);
        $this->db->where('slsct.ShortCode', $system_language_code);
        $this->db->where('slct.ShortCode', $system_language_code);

        $this->db->group_by('products.ProductID');

        $this->db->order_by('products.CreatedAt', 'DESC');
        $this->db->order_by('products_text.Title', 'ASC');


        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getPromotedProductsForUserFollowCategories($where = false, $system_language_code = 'EN', $data_array = false, $type = 'categories',$random = false)
    {
        $this->db->select('users.*,users_text.*,products.*,products_text.*,cities_text.Title as CityName, ssct.Title as SubSubCategoryName, sct.Title as SubCategoryName, ct.Title as CategoryName,promotions.PromotionUrl, promotions.PromotionType,promotions.PromotionID, promotions.PromotionImage, promotions_text.Title as PromotionTitle, promotions_text.Description as PromotionDescription');

        //$this->db->from('promoted_products');
        $this->db->from('promotions');

        if($system_language_code == 'EN'){
            $language = 1;
        }else{
            $language = 2;
        }

        
        $this->db->join('promotions_text', 'promotions.PromotionID = promotions_text.PromotionID');
        $this->db->join('promoted_products', 'promoted_products.PromotionID = promotions.PromotionID', 'LEFT');

        $this->db->join('products', 'promoted_products.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID','LEFT');

        $this->db->join('users', 'users.UserID = products.UserID', 'LEFT');
        $this->db->join('users_text', 'users_text.UserID = users.UserID AND users_text.SystemLanguageID = 1','LEFT');

        $this->db->join('cities', 'cities.CityID = users.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID','LEFT');

        $this->db->join('countries', 'cities.CountryID = countries.CountryID', 'LEFT');
        $this->db->join('countries_text', 'countries.CountryID = countries_text.CountryID','LEFT');

        // sub sub category
        $this->db->join('categories ssc', 'products.CategoryID = ssc.CategoryID', 'LEFT');
        $this->db->join('categories_text ssct', 'ssc.CategoryID = ssct.CategoryID','LEFT');

        // sub category
        $this->db->join('categories sc', 'ssc.ParentID = sc.CategoryID', 'LEFT');
        $this->db->join('categories_text sct', 'sc.CategoryID = sct.CategoryID','LEFT');

        // category
        $this->db->join('categories c', 'sc.ParentID = c.CategoryID', 'LEFT');
        $this->db->join('categories_text ct', 'c.CategoryID = ct.CategoryID','LEFT');

        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID AND products_text.SystemLanguageID = '.$language,'LEFT');
        $this->db->join('system_languages slctt', 'cities_text.SystemLanguageID = slctt.SystemLanguageID AND cities_text.SystemLanguageID = '.$language,'LEFT');
        $this->db->join('system_languages slpt', 'countries_text.SystemLanguageID = slpt.SystemLanguageID AND countries_text.SystemLanguageID = '.$language,'LEFT');
        $this->db->join('system_languages slssct', 'ssct.SystemLanguageID = slssct.SystemLanguageID AND ssct.SystemLanguageID = '.$language,'LEFT');
        $this->db->join('system_languages slsct', 'sct.SystemLanguageID = slsct.SystemLanguageID AND sct.SystemLanguageID = '.$language,'LEFT');
        $this->db->join('system_languages slct', 'ct.SystemLanguageID = slct.SystemLanguageID AND ct.SystemLanguageID = '.$language,'LEFT');

        if ($where) {
            $this->db->where($where);
        }
        if($data_array){
            $this->db->where_in('ssc.ParentID', $data_array);
        }
        

        /*$this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('slctt.ShortCode', $system_language_code);
        $this->db->where('slpt.ShortCode', $system_language_code);
        $this->db->where('slssct.ShortCode', $system_language_code);
        $this->db->where('slsct.ShortCode', $system_language_code);
        $this->db->where('slct.ShortCode', $system_language_code);*/

        $this->db->group_by('promotions.PromotionID');
        
         if($random){
            $this->db->order_by('products.ProductID','DESC');
        }else{
            $this->db->order_by('products_text.Title', 'ASC');
        }
        


        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getMostSellingProducts($system_language_code = 'EN', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('promoted_products.*, products.*, products_text.*, COUNT(order_items.ProductID) as OrderCount');
        $this->db->from('promoted_products');
        $this->db->join('order_items', 'promoted_products.ProductID = order_items.ProductID', 'LEFT');
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->having('OrderCount >', 0);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->group_by('order_items.ProductID');
        $this->db->order_by('OrderCount', $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return array();
        }
    }

    public function getMostLikedProducts($system_language_code = 'EN', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('promoted_products.*, products.*, products_text.*, COUNT(product_likes.ProductID) as LikeCount');
        $this->db->from('promoted_products');
        $this->db->join('product_likes', 'promoted_products.ProductID = product_likes.ProductID', 'LEFT');
        $this->db->join('products', 'product_likes.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->having('LikeCount >', 0);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->group_by('product_likes.ProductID');
        $this->db->order_by('LikeCount', $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return array();
        }
    }

    public function getProductsViews($system_language_code = 'EN', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('promoted_products.*, products.*, products_text.*, COUNT(product_views.ProductID) as ViewCount');
        $this->db->from('promoted_products');
        $this->db->join('product_views', 'promoted_products.ProductID = product_views.ProductID', 'LEFT');
        $this->db->join('products', 'product_views.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->having('ViewCount >', 0);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->group_by('product_views.ProductID');
        $this->db->order_by('ViewCount', $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return array();
        }
    }

    public function getRecentlyAddedProducts($system_language_code = 'EN', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('promoted_products.*, products.*, products_text.*');
        $this->db->from('promoted_products');
        $this->db->join('products', 'promoted_products.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->order_by('promoted_products.PromotedProductID', $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        } else {
            return array();
        }
    }

}