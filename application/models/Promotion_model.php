<?php
    Class Promotion_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("promotions");

        }




        public function getPromotions($where = false, $system_language_code = false, $limit = false, $start = 0, $sort_by = 'promotions.SortOrder', $sort_as = 'ASC',$rating = array())
	    {

	        $langData= getLanguageBy(array('ShortCode' => $system_language_code));

	        if(!empty($rating)){
	            $having = '(';
	            $this->db->select('IFNULL(AVG(product_ratings.Rating),0) as Rating');
	            $this->db->join('product_ratings', 'products.ProductID = product_ratings.ProductID','left');
	            foreach ($rating as $key => $value) {
	               $having .= 'Rating = '.$value.' OR ';
	            }
	            $having  = rtrim($having,' OR ');
	            $having  .= ')';

	            $this->db->having($having);
	        }
	        $this->db->select('promotions.*,promotions_text.*,products_text.Title as ProductTitle,site_images.ImageName');
	        $this->db->from('promotions');
	        $this->db->join('promotions_text', 'promotions.PromotionID = promotions_text.PromotionID');
	        $this->db->join('promoted_products', 'promotions.PromotionID = promoted_products.PromotionID','left');
	        $this->db->join('products', 'products.ProductID = promoted_products.ProductID','left');
	        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = '.$langData->SystemLanguageID.'','left');
	      
	        $this->db->Join('site_images', 'products.ProductID = site_images.FileID AND site_images.ImageType = "product"','Left');
	        


	        $this->db->join('system_languages', 'promotions_text.SystemLanguageID = system_languages.SystemLanguageID','left');

	        if ($where) {
	            $this->db->where($where);
	        }
	        $this->db->where('system_languages.ShortCode', $system_language_code);

	        if ($limit) {
	            $this->db->limit($limit, $start);
	        }

	        $this->db->order_by($sort_by, $sort_as);
	        $this->db->group_by('promotions.PromotionID');


	        $result = $this->db->get();
	        
	        return $result->result();
	    }


    }