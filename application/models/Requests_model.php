<?php

Class Requests_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("package_requests");

    }

    public function getAllRequests($where = false)
    {
        $this->db->select('package_requests.*,users.Email,users.Mobile,users_text.FullName,packages_text.Title');
        $this->db->from('package_requests');

        $this->db->join('users','package_requests.UserID = users.UserID');
        $this->db->join('users_text','users.UserID = users_text.UserID');

        $this->db->join('packages','package_requests.PackageID = packages.PackageID','left');
        $this->db->join('packages_text','packages.PackageID = packages_text.PackageID','left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = packages_text.SystemLanguageID','Left');
        $this->db->where('system_languages.IsDefault', '1');
        if($where){
            $this->db->where($where);
        }
        $this->db->order_by('package_requests.PackageRequestID','DESC');


        return $this->db->get()->result_array();
    }


}