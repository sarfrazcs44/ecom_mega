<?php

Class Temp_order_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("temp_orders");

    }

    public function getTotalProduct($user_id)
    {
        $this->db->select('COUNT(ProductID) as CartProductsCount, SUM(Quantity) as CartQuantityCount');
        $this->db->from('temp_orders');
        $this->db->where('UserID', $user_id);
        return $this->db->get()->result();
    }

    public function getCartItems($user_id, $system_language_code = 'EN')
    {
        if($system_language_code == 'EN'){
            $lang_id = 1;
        }else{
            $lang_id = 2;
        }
        $this->db->select("IFNULL(AVG(product_ratings.Rating),0) as Rating,temp_orders.TempOrderID,temp_orders.Quantity,temp_orders.TempItemPrice,temp_orders.ItemType,temp_orders.ProductID,temp_orders.CustomizedOrderProductIDs,products_text.Title,products.Price,products.PriceType,temp_orders.CustomizedShapeImage,temp_orders.CustomizedBoxID,products.IsCorporateProduct,products.CorporateMinQuantity,temp_orders.IsCorporateItem,temp_orders.Ribbon,site_images.ImageName");
        $this->db->from('temp_orders');
        $this->db->join('products', 'temp_orders.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = '.$lang_id .'', 'LEFT');
        $this->db->join('product_ratings', 'products.ProductID = product_ratings.ProductID','left');
        $this->db->Join('site_images', 'products.ProductID = site_images.FileID AND site_images.ImageType = "product"','Left');
        
       // $this->db->join("system_languages slpt", "products_text.SystemLanguageID = slpt.SystemLanguageID AND slpt.ShortCode = '$system_language_code'", "LEFT");
        $this->db->where('temp_orders.UserID', $user_id);
        $this->db->group_by('temp_orders.TempOrderID');
        /*if ($system_language_code) {
            $this->db->where('slpt.ShortCode', $system_language_code);
        } else {
            $this->db->where('slpt.IsDefault', '1');
        }*/
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();


    }

    public function getAbandonedCart($where = '', $limit = false, $start = 0)
    {
        $this->db->select("users.UserID,users.OnlineStatus,users.Mobile,users.Email,users_text.FullName,COUNT(temp_orders.ProductID) as CartProductsCount, SUM(temp_orders.Quantity) as CartQuantityCount, SUM(temp_orders.Quantity * temp_orders.TempItemPrice) as SubTotal");
        $this->db->from('temp_orders');
        $this->db->join('users', 'temp_orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        
        if ($where != '') {
            $this->db->where($where);
        }
        $this->db->group_by('temp_orders.UserID');
        $this->db->order_by('CartQuantityCount');

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();


    }
    public function getCountAbandonedCart($where = false)
    {
        $this->db->select("users.UserID,users.OnlineStatus,users.Mobile,users.Email,users_text.FullName,COUNT(temp_orders.ProductID) as CartProductsCount, SUM(temp_orders.Quantity) as CartQuantityCount, SUM(temp_orders.Quantity * temp_orders.TempItemPrice) as SubTotal");
        $this->db->from('temp_orders');
        $this->db->join('users', 'temp_orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        return $query->num_rows();

    }


     public function getCountAbandonedCartAnonymous($where = '')
    {

       
        if($where != ''){
            $where = $where." AND";
        }

        $sql = "SELECT temp_orders.UserID, COUNT(temp_orders.ProductID) as CartProductsCount, SUM(temp_orders.Quantity) as CartQuantityCount FROM temp_orders WHERE ".$where." UserID NOT IN (SELECT UserID FROM users) HAVING CartProductsCount > 0 AND CartQuantityCount > 0";
        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    public function getAbandonedCartAnonymous($where = '', $limit = false, $start = 0)
    {

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        if($where != ''){
            $where = $where." AND";
        }

        $sql = "SELECT temp_orders.UserID, COUNT(temp_orders.ProductID) as CartProductsCount, SUM(temp_orders.Quantity) as CartQuantityCount FROM temp_orders WHERE ".$where." UserID NOT IN (SELECT UserID FROM users) HAVING CartProductsCount > 0 AND CartQuantityCount > 0";
        $query = $this->db->query($sql);

        return $query->result();
    }
    
    
    public function getCartData($user_id, $system_language_code = 'EN'){
        
        $this->db->select('temp_orders.TempOrderID,temp_orders.Quantity,products.*,products_text.*,ct.Title as CategoryTitle,sct.Title as SubCategoryTitle');
        $this->db->from('temp_orders');
        $this->db->join('products','temp_orders.ProductID = products.ProductID','left');
        $this->db->join('products_text','products.ProductID = products_text.ProductID','left');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');

        $this->db->join('categories c','c.CategoryID = products.CategoryID','left');
        $this->db->join('categories_text ct','c.CategoryID = ct.CategoryID','left');

        $this->db->join('categories sc','sc.CategoryID = products.SubCategoryID','left');
        $this->db->join('categories_text sct','sc.CategoryID = sct.CategoryID','left');
        $this->db->where('temp_orders.UserID',$user_id);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->group_by('products.ProductID');


        return $this->db->get()->result_array();
        
        
    }

}
