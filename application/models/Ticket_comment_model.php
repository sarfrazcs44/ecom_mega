<?php

Class Ticket_comment_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("ticket_comments");
    }

    public function getTicketComments($fields)
    {
        $this->db->order_by('CommentID', 'ASC');
        $result = $this->db->get_where($this->table, $fields);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }


}