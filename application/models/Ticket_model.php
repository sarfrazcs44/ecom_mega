<?php

Class Ticket_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("tickets");

    }

    public function getTickets($where = false, $limit = false, $start = 0, $system_language_code = false, $sort = 'DESC')
    {
        $this->db->select('tickets.*,tickets.CreatedAt as TicketCreatedAt,orders.*,orders.CreatedAt as OrderCreatedAt,users.*,users_text.*,order_statuses.*, cities_text.Title as UserCity, user_address.*,act.Title as AddressCity, districts_text.Title as AddressDistrict, dut.FullName as AssignedDriverName, du.Email as AssignedDriverEmail, du.Mobile as AssignedDriverMobile, tax_shipment_charges_text.Title as ShipmentMethodTitle');
        $this->db->from('tickets');
        $this->db->join('orders', 'tickets.OrderID = orders.OrderID', 'LEFT');
        $this->db->join('order_statuses', 'orders.Status = order_statuses.OrderStatusID', 'LEFT');
        $this->db->join('user_address', 'orders.AddressID = user_address.AddressID', 'LEFT');
        $this->db->join('users', 'orders.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1', 'LEFT');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID AND users_text.SystemLanguageID = 1', 'LEFT');
        // address city
        $this->db->join('cities ac', 'user_address.CityID = ac.CityID', 'LEFT');
        $this->db->join('cities_text act', 'ac.CityID = act.CityID AND users_text.SystemLanguageID = 1', 'LEFT');
        // address district
        $this->db->join('districts', 'user_address.DistrictID = districts.DistrictID', 'LEFT');
        $this->db->join('districts_text', 'districts.DistrictID = districts_text.DistrictID AND users_text.SystemLanguageID = 1', 'LEFT');

        // driver details
        $this->db->join('users du', 'orders.DriverID = du.UserID', 'LEFT');
        $this->db->join('users_text dut', 'du.UserID = dut.UserID AND dut.SystemLanguageID = 1', 'LEFT');

        // Shipment Method
        $this->db->join('tax_shipment_charges', 'orders.ShipmentMethodID = tax_shipment_charges.TaxShipmentChargesID', 'LEFT');
        $this->db->join('tax_shipment_charges_text', 'tax_shipment_charges.TaxShipmentChargesID = tax_shipment_charges_text.TaxShipmentChargesID AND tax_shipment_charges_text.SystemLanguageID = 1', 'LEFT');

        if ($where) {
            $this->db->where($where);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->order_by('tickets.TicketID', 'DESC');
        $this->db->group_by('tickets.TicketID');
        $result = $this->db->get();
        return $result->result();
    }


    public function getTicketsCount($where = false, $system_language_code = false)
    {
        $this->db->select('tickets.*,tickets.CreatedAt as TicketCreatedAt,orders.*,orders.CreatedAt as OrderCreatedAt,users.*,users_text.*,order_statuses.*, cities_text.Title as UserCity, user_address.*,act.Title as AddressCity, districts_text.Title as AddressDistrict, dut.FullName as AssignedDriverName, du.Email as AssignedDriverEmail, du.Mobile as AssignedDriverMobile, tax_shipment_charges_text.Title as ShipmentMethodTitle');
        $this->db->from('tickets');
        $this->db->join('orders', 'tickets.OrderID = orders.OrderID', 'LEFT');
        $this->db->join('order_statuses', 'orders.Status = order_statuses.OrderStatusID', 'LEFT');
        $this->db->join('user_address', 'orders.AddressID = user_address.AddressID', 'LEFT');
        $this->db->join('users', 'orders.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1', 'LEFT');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID AND users_text.SystemLanguageID = 1', 'LEFT');
        // address city
        $this->db->join('cities ac', 'user_address.CityID = ac.CityID', 'LEFT');
        $this->db->join('cities_text act', 'ac.CityID = act.CityID AND users_text.SystemLanguageID = 1', 'LEFT');
        // address district
        $this->db->join('districts', 'user_address.DistrictID = districts.DistrictID', 'LEFT');
        $this->db->join('districts_text', 'districts.DistrictID = districts_text.DistrictID AND users_text.SystemLanguageID = 1', 'LEFT');

        // driver details
        $this->db->join('users du', 'orders.DriverID = du.UserID', 'LEFT');
        $this->db->join('users_text dut', 'du.UserID = dut.UserID AND dut.SystemLanguageID = 1', 'LEFT');

        // Shipment Method
        $this->db->join('tax_shipment_charges', 'orders.ShipmentMethodID = tax_shipment_charges.TaxShipmentChargesID', 'LEFT');
        $this->db->join('tax_shipment_charges_text', 'tax_shipment_charges.TaxShipmentChargesID = tax_shipment_charges_text.TaxShipmentChargesID AND tax_shipment_charges_text.SystemLanguageID = 1', 'LEFT');

    
        if ($where) {
            $this->db->where($where);
        }

        $this->db->group_by('tickets.TicketID');
       

        


        $result = $this->db->get();

       

       //echo $result->num_rows();exit;
        return $result->num_rows();


    
    }


}