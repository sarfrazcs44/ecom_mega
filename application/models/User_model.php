<?php

Class User_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("users");

    }

    public function getUserDetails($where, $system_language_code = 'EN')
    {

        $this->db->select('users.*,users_text.FullName,cities_text.Title as CityTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('cities', 'cities.CityID = users.CityID', 'Left');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        $this->db->where($where);
        $this->db->where('system_languages.ShortCode', $system_language_code);

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->row_array();


    }

    public function getUserInfo($where, $system_language_code = 'EN')
    {
        $this->db->select('users.*,users_text.FullName,cities_text.Title as CityTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('cities', 'cities.CityID = users.CityID', 'Left');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID','Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


        $this->db->where($where);

       // $this->db->where('system_languages.IsDefault', '1');

        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        $result = $this->db->get();
        return $result->row_array();


    }

    public function getUserData($data, $system_language_code = false)
    {

        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('roles', 'roles.RoleID = users.RoleID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
        // $this->db->where($where);
        $this->db->where('users.Email', $data['Email']);
        //$this->db->or_where('users.UName',$data['Email']);
        $this->db->where('users.Password', $data['Password']);

        /*if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }*/
        $this->db->where('system_languages.IsDefault', '1');
        return $this->db->get()->row_array();


    }

    public function getUsers($where = false, $limit = false, $start = 0, $system_language_code = 'EN', $sort = 'DESC')
    {

        $this->db->select('users.*,users_text.FullName,cities_text.Title as CityTitle,roles_text.Title as RoleTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('roles_text', 'users.RoleID = roles_text.RoleID');
        $this->db->join('cities_text', 'cities_text.CityID = users.CityID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        $this->db->join('system_languages slct', 'slct.SystemLanguageID = cities_text.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }

        $this->db->where('system_languages.IsDefault', '1');

        if ($system_language_code) {
            $this->db->where('slct.ShortCode', $system_language_code);
        } else {
            $this->db->where('slct.IsDefault', '1');
        }


        $this->db->group_by('users.UserID');
        $this->db->order_by('users.CreatedAt', $sort);

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        return $result->result();


    }

    public function getUsersCount($where = false, $system_language_code = 'EN')
    {

        $this->db->select('users.*,users_text.FullName,cities_text.Title as CityTitle,roles_text.Title as RoleTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('roles_text', 'users.RoleID = roles_text.RoleID');
        $this->db->join('cities_text', 'cities_text.CityID = users.CityID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        $this->db->join('system_languages slct', 'slct.SystemLanguageID = cities_text.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }

        $this->db->where('system_languages.IsDefault', '1');

        if ($system_language_code) {
            $this->db->where('slct.ShortCode', $system_language_code);
        } else {
            $this->db->where('slct.IsDefault', '1');
        }


        $this->db->group_by('users.UserID');
        $result = $this->db->get();
        return $result->num_rows();


    }

    public function getBackendUsers($where = false)
    {
        $this->db->select('users.*,users_text.FullName,roles_text.Title as RoleTitle, stores_text.Title as StoreTitle, cities_text.Title as CityTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('stores_text', 'users.StoreID = stores_text.StoreID AND users_text.SystemLanguageID = 1', 'LEFT');
        $this->db->join('cities_text', 'cities_text.CityID = users.CityID', 'LEFT');
        $this->db->join('roles_text', 'users.RoleID = roles_text.RoleID AND users_text.SystemLanguageID = 1');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('users.UserID');
        $result = $this->db->get();
        return $result->result();
    }


    public function getUserDataByEmail($email,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        
        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
       // $this->db->where($where);
       $this->db->where('users.Email',$email);
      
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row();
    }

    public function getUserDataBySocialID($socialID,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        
        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
       // $this->db->where($where);
       $this->db->where('users.SocialID',$socialID);
      
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row();
    }


}

?>