<?php
    Class Wallet_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("wallets");

        }


        public function getwalletdata($where = false, $limit = false, $start = 0, $sort = 'DESC')       
        {

            $this->db->select('*');
            $this->db->from('wallets');
           

            if ($where) {
                $this->db->where($where);
            }

            $this->db->group_by('wallets.WalletID');
            $this->db->order_by('wallets.CreatedAt', $sort);

            if ($limit) {
                $this->db->limit($limit, $start);
            }


            $result = $this->db->get();

            //echo $this->db->last_query();exit();
            return $result->result();


        }


    }