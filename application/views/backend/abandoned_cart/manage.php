<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Abandoned Carts</h4>
                        <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="<?php echo($url_status == 'by_user' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/AbandonedCart?status=by_user'); ?>">By User</a>
                                </li>
                                <li class="<?php echo($url_status == 'by_anonymous' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/AbandonedCart?status=by_anonymous'); ?>">By Anonymous</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="user">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <?php if($url_status == 'by_user'){ ?>
                                         <th>Customer</th>
                                        <th>Email</th>
                                        
                                        <th>Products</th>
                                        <th>Quantity</th>
                                        <th>Subtotal</th>
                                        <th>Action</th>

                                    <?php } else{ ?>
                                        <th>User Temp Key</th>
                                        <th>Total Products In Cart</th>
                                        <th>Total Quantity In Cart</th>
                                    <?php } ?>   
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    var columns = <?php echo $columns; ?>;
    ParentID = '<?php echo $url_status; ?>';
    var CallUrl = '<?php echo $ControllerName;?>';
</script>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {
        $('table.datatable').DataTable();
    });

    function notifyUser(UserID) {
        showCustomLoader();
        $.ajax({
            type: "POST",
            url: base_url + 'cms/abandonedCart/sendAbandonedCartNotificationToUser',
            data: {'UserID': UserID},
            dataType: "json",
            success: function (result) {
                hideCustomLoader();
                showSuccess(result.message);
            }
        });
    }
</script>