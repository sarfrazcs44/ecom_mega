<link href="<?php echo base_url(); ?>assets/backend/css/components.css" rel="stylesheet"/>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Mobile Application Design </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            
                            <div class="col-md-12">
                                <div class="page-title-box m-0">
        
         <ol class="breadcrumb p-0 m-0">
            <li>
               <a href="#"><button type="button" class="btn btn-primary waves-effect w-md waves-light m-0">btn</button></a>
               <a href="#"><button type="button" class="btn btn-secondary waves-effect w-md waves-light m-0">btn</button></a>
            </li>
         </ol>
      </div>
      <div class="card-box pl-4 pr-5">
         <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item active">
               <a class="nav-link text-dark text-capitalize bg-transparent px-3 py-2 active" id="step_1-tab" data-toggle="tab" href="#step_1" role="tab" aria-controls="step_1" aria-selected="true">Design Style</a>
            </li>
            <li class="nav-item">
               <a class="nav-link text-dark text-capitalize bg-transparent px-3 py-2" id="step_2-tab" data-toggle="tab" href="#step_2" role="tab" aria-controls="step_2" aria-selected="false">Main Screen &amp; Elements</a>
            </li>
            <li class="nav-item">
               <a class="nav-link text-dark text-capitalize bg-transparent px-3 py-2" id="step_3-tab" data-toggle="tab" href="#step_3" role="tab" aria-controls="step_3" aria-selected="false">Application Information</a>
            </li>
         </ul>
          <form action="<?php echo base_url();?>cms/app_setting/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                           <input type="hidden" name="form_type" value="update">
                           <input type="hidden" name="id" value="<?php echo encode_url($result->AppSettingID);?>">
                           <div class="alert" id="validatio-msg" style="display: none;"></div>
         <div class="tab-content" id="myTabContent">
            <div class="tab-pane active" id="step_1" role="tabpanel" aria-labelledby="step_1-tab">
               <div class="row mobileScreensEd">
                  <div class="col-md-4 my-3  d-flex align-items-center">
                     <div class="wrapper">
                        <p class="text-danger largeFontSize font-weight-bold">Input Style</p>
                        <p class="text-dark mediumFontSize lineHeight1-3">
                           Select the style of the input Fields and Buttons that you want in form pages of your application. They will be shown throughout the application on;
                        </p>
                        <p class="text-dark mediumFontSize lineHeight1-3 mb-5">
                           - Login
                           <br>
                           - Registration
                           <br>
                           - Adding Address
                           <br>
                           - Selecting Quantity of Order
                           <br>
                           - Buttons will be shown in many pages
                        </p>
                        <p class="text-dark mediumFontSize lineHeight1-3">
                           Select the style from bellow:
                        </p>
                        <div class="radio pl-2 mb-3">
                           <input type="radio" name="FieldStyle" id="rdo_11" value="Full Round" <?php echo ($result->FieldStyle == 'Full Round' ? 'checked' : ''); ?>>
                           <label for="rdo_11" class="text-dark largeFontSize lineHeight1-3 font-weight-bold m-0">Full Rounded Style</label>
                        </div>
                        <div class="radio pl-2 mb-3">
                           <input type="radio" name="FieldStyle" id="rdo_12" value="Half Round" <?php echo ($result->FieldStyle == 'Half Round' ? 'checked' : ''); ?>>
                           <label for="rdo_12" class="text-dark largeFontSize lineHeight1-3 font-weight-bold m-0">Half Rounded Style</label>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-8 d-flex justify-content-around">
                     <div class="mbl-screen d-flex align-items-center mr-2 my-3">
                        <div class="singWrapper">
                           <p class="text-danger text-center largeFontSize font-weight-bold  mb-5 pb-3">Full Round Style</p>
                           <div class="form-group">
                              <input type="text" class="form-control" placeholder=" Text Input Field" disabled="true">
                           </div>
                           <div class="rounderChkOption mb-4 mt-4">
                              <div class="radio pl-2 mb-1">
                                 <input type="radio" name="rdoFullRounded1_" id="rdoFullRounded_11" value="option1" disabled="true">
                                 <label for="rdoFullRounded_11" class="text-dark largeFontSize lineHeight1-3 font-weight-bold m-0">Unselected Radio input</label>
                              </div>
                              <div class="radio pl-2 mb-1">
                                 <input type="radio" name="rdoFullRounded1_" id="rdoFullRounded_12" value="option2" checked disabled="true">
                                 <label for="rdoFullRounded_12" class="text-dark largeFontSize lineHeight1-3 font-weight-bold m-0">Selected Radio input</label>
                              </div>
                           </div>
                           <div class="rounderChkOption mb-4 mt-4">
                              <div class="checkbox checkbox-dark pl-2 ineHeight1-3 mb-1">
                                 <input id="checkbox6c_1" type="checkbox" disabled="true">
                                 <label for="checkbox6c_1" class="text-dark largeFontSize font-weight-bold m-0">Unselected CheckBox input</label>
                              </div>
                              <div class="checkbox checkbox-dark pl-2 ineHeight1-3 mb-1">
                                 <input id="checkbox6c" type="checkbox" checked disabled="true">
                                 <label for="checkbox6c" class="text-dark largeFontSize font-weight-bold m-0">Selected CheckBox input</label>
                              </div>
                           </div>
                           <div class="form-group">
                              <textarea name="" id="" cols="30" rows="10" class="form-control" placeholder="Description text input Field" disabled="true"></textarea>
                           </div>
                           <div class="d-flex justify-content-between">
                              <button type="button" class="btn btn-danger px-2 mx-auto">Positive Button</button>
                              <button type="button" class="btn btn-dark px-2 mx-auto">Negative Button</button>
                           </div>
                        </div>
                     </div>
                     <div class="mbl-screen d-flex align-items-center ml-2 my-3">
                        <div class="singWrapper">
                           <p class="text-danger text-center largeFontSize font-weight-bold  mb-5 pb-3">Half Round Style</p>
                           <div class="form-group">
                              <input type="text" class="form-control rounded" placeholder=" Text Input Field" disabled="true">
                           </div>
                           <div class="rounderChkOption mb-4 mt-4">
                              <div class="radio pl-2 mb-1">
                                 <input type="radio" name="rdoFullRounded1_" id="rdoFullRounded_11" value="option1" disabled="true">
                                 <label for="rdoFullRounded_11" class="text-dark largeFontSize lineHeight1-3 font-weight-bold m-0">Unselected Radio input</label>
                              </div>
                              <div class="radio pl-2 mb-1">
                                 <input type="radio" name="rdoFullRounded1_" id="rdoFullRounded_12" value="option2" checked disabled="true">
                                 <label for="rdoFullRounded_12" class="text-dark largeFontSize lineHeight1-3 font-weight-bold m-0">Selected Radio input</label>
                              </div>
                           </div>
                           <div class="rounderChkOption mb-4 mt-4">
                              <div class="checkbox checkbox-dark pl-2 ineHeight1-3 mb-1">
                                 <input id="checkbox6c_1" type="checkbox" disabled="true">
                                 <label for="checkbox6c_1" class="text-dark largeFontSize font-weight-bold m-0">Unselected CheckBox input</label>
                              </div>
                              <div class="checkbox checkbox-dark pl-2 ineHeight1-3 mb-1">
                                 <input id="checkbox6c" type="checkbox" checked disabled="true">
                                 <label for="checkbox6c" class="text-dark largeFontSize font-weight-bold m-0">Selected CheckBox input</label>
                              </div>
                           </div>
                           <div class="form-group">
                              <textarea name="" id="" cols="30" rows="10" class="form-control rounded" placeholder="Description text input Field" disabled="true"></textarea>
                           </div>
                           <div class="d-flex justify-content-between">
                              <button type="button" class="btn btn-danger px-2 mx-auto rounded">Positive Button</button>
                              <button type="button" class="btn btn-dark px-2 mx-auto rounded">Negative Button</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="my-4 d-flex justify-content-between align-items-center">
                  <button type="button" class="btn btn-light px-4 w-auto text-left text-white border-0">Proceed to Next Step</button>
                  <button type="submit" class="btn btn-primary waves-effect w-md waves-light m-0">Save</button>
               </div>
            </div>
            <div class="tab-pane" id="step_2" role="tabpanel" aria-labelledby="step_2-tab">
               <div class="row mobileScreensEd">
                  <div class="col-md-6">
                     <div class="row">
                        <?php 
                                          $splash_background_image = '';
                                          if($result->SplashBackgroundImage != ''){
                                            if(file_exists($result->SplashBackgroundImage)){
                                                  $splash_background_image = base_url().$result->SplashBackgroundImage;
                                            }
                                          
                                          }
                                          
                                          
                                          $splash_logo = '';
                                          if($result->SplashLogo != ''){
                                            if(file_exists($result->SplashLogo)){
                                                  $splash_logo = base_url().$result->SplashLogo;
                                            }
                                          
                                          }
                                          
                                          $login_background_image = '';
                                          if($result->LoginBackgroundImage != ''){
                                            if(file_exists($result->LoginBackgroundImage)){
                                                  $login_background_image = base_url().$result->LoginBackgroundImage;
                                            }
                                          
                                          }
                                          
                                          
                                          $login_logo = '';
                                          if($result->LoginLogo != ''){
                                            if(file_exists($result->LoginLogo)){
                                                  $login_logo = base_url().$result->LoginLogo;
                                            }
                                          
                                          }

                                          $main_elements_background_image = '';
                                          if($result->MainElementsBackgroundImage != ''){
                                            if(file_exists($result->MainElementsBackgroundImage)){
                                                  $main_elements_background_image = base_url().$result->MainElementsBackgroundImage;
                                            }
                                          
                                          }
                                          
                                          
                                          
                                          ?>
                        <div class="col-md-6 pr-0">
                           <div class="mbl-screen d-flex align-items-center bg-success" style="min-width:auto; background-image:url('<?php echo $splash_background_image; ?>');">
                              <div class="form-group m-0 w-100 ">
                                 <div class="dropify-wrapper border-0 bg-transparent h-auto pt-0 pb-2">
                                    <div class="dropify-message" style="background-image:url('<?php echo $splash_logo; ?>');">
                                       <i class="fa fa-image position-relative fa-2x mb-2 text-danger"></i>
                                       <p class=" text-danger lineHeight1-3">Upload your Logo</p>
                                       <p class="dropify-error">Ooops, something wrong appended.</p>
                                    </div>
                                    <div class="dropify-loader"></div>
                                    <div class="dropify-errors-container">
                                       <ul>
                                       </ul>
                                    </div>
                                    <input type="file" class="dropify" name="splash_logo[]" data-height="300">
                                    <button type="button" class="dropify-clear">Remove</button>
                                    <div class="dropify-preview">
                                       <span class="dropify-render"></span>
                                       <div class="dropify-infos">
                                          <div class="dropify-infos-inner">
                                             <p class="dropify-filename"><i class="fa fa-image position-relative fa-2x mb-2 text-danger"></i> <span class="dropify-filename-inner"></span></p>
                                             <p class="dropify-infos-message text-danger">Upload your Logo</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="haveColorButton text-white text-center position-absolute left-0 right-0 bottom-0 mb-4 mt-5 pb-2 mx-auto" style="width: 160px; <?php echo ($result->DomainColor != '' ? 'color  :'. $result->DomainColor.'!important' : ''); ?> ">
                                 www.domain.com
                                    <a href="javascript:void(0);" class="itsColorButtonW top-0 left-0"></a>
                                    <input type="color" name="DomainColor" value="<?php echo $result->DomainColor ?>" class="clr-pickW left-0 top-0 position-absolute" style="opacity:0;width: 32px;height: 32px;z-index: 11;">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 d-flex justify-content-center">
                           <div class="align-self-center">
                              <h2 class="text-danger">Splash</h2>
                              <p class="mb-5">Intro Screen</p>
                              <div class="form-group">
                                 <input type="text" placeholder="Splash Background Hex" name="SplashBackgroundHex" class="form-control" value="<?php echo $result->SplashBackgroundHex;?>">
                              </div>
                              <div class="form-group">
                                 <input type="text" placeholder="2nd Color Hex (add to set as Gradient)" name="Splash2ndHex" class="form-control" value="<?php echo $result->Splash2ndHex;?>">
                              </div>
                              <p>or</p>
                              <div class="form-group">
                                 <div class="input-group">
                                    <div class="custom-file position-relative d-flex align-items-end">
                                       <label class="input-group-text btn btn-danger px-4 w-100 d-inline-block" for="splashBgImage">Upload a Background Instead</label>
                                       <input type="file" name="SplashBackgroundImage[]" class="custom-file-input" id="splashBgImage" aria-describedby="splashBgImage">
                                       <button type="button" class="border-0 p-0 shadow-none h-auto bg-transparent position-absolute right-0 top-o" style="margin-right: -27px;">
                                          <i class="fa fa-times-circle fa-2x text-secondary rounded-circle"></i>
                                       </button>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <input type="text" placeholder="Splash Duration" name="SplashDuration" class="form-control" value="<?php echo $result->SplashDuration;?>" >
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="mbl-screen scnd-scrn d-flex align-items-center  bg-success" style="min-width:auto;background-image:url('<?php echo $login_background_image; ?>');">
                              <div class="koiBe w-100">
                                 <div class="form-group m-0 w-100 ">
                                    <div class="dropify-wrapper border-0 bg-transparent h-auto pt-0 pb-2 mb-3">
                                       <div class="dropify-message" style="background-image:url('<?php echo $login_logo; ?>');">
                                          <i class="fa fa-image position-relative fa-2x mb-2 text-danger"></i>
                                          <p class=" text-danger lineHeight1-3">Upload your Logo</p>
                                          <p class="dropify-error">Ooops, something wrong appended.</p>
                                       </div>
                                       <div class="dropify-loader"></div>
                                       <div class="dropify-errors-container">
                                          <ul>
                                          </ul>
                                       </div>
                                       <input type="file" name="LoginLogo[]" class="dropify" data-height="300">
                                       <button type="button" class="dropify-clear">Remove</button>
                                       <div class="dropify-preview">
                                          <span class="dropify-render"></span>
                                          <div class="dropify-infos">
                                             <div class="dropify-infos-inner">
                                                <p class="dropify-filename"><i class="fa fa-image position-relative fa-2x mb-2 text-danger"></i> <span class="dropify-filename-inner"></span></p>
                                                <p class="dropify-infos-message text-danger">Upload your Logo</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="">
                                    <div class="haveColorButton">
                                       <input type="email" placeholder="Email" class="form-control mb-3"  disabled="true">
                                       <a href="javascript:void(0);" class="itsColorButton"></a>
                                       <input type="color" value="#ff0000" class="clr-pick" style="opacity:0; position:absolute; left:8px;bottom:17px; width:15%; z-index: 11;">
                                    </div>
                                    <input type="password" placeholder="Password" class="form-control mb-3"  disabled="true">
                                    <div class="haveColorButton">
                                       <button type="button" class="btn btn-icon btn-sbt btn-success m-b-5">Sign In</button>
                                       <a href="javascript:void(0);" class="itsColorButton"></a>
                                       <input type="color" value="#ff0000" class="clr-pick" style="opacity:0; position:absolute; left:8px;bottom:19px; width:15%; z-index: 11;" >
                                    </div>
                                 </div>
                                 <div class="haveColorButton text-white text-center position-absolute left-0 right-0 bottom-0 mb-4 mt-5 pb-2 mx-auto" style="width: 160px;">
                                    Register
                                    <a href="javascript:void(0);" class="itsColorButtonW left-0 top-0"></a>
                                    <input type="color" value="#ff0000" class="clr-pickW left-0 top-0 position-absolute" style="opacity:0;width: 32px;height: 32px;z-index: 11;">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 d-flex justify-content-center">
                           <div class="align-self-center">
                              <h2 class="text-danger">Login</h2>
                              <p class="mb-5">Screen</p>
                              <div class="form-group">
                                 <input type="text" placeholder="Background Hex" name="LoginBackgroundHex" value="<?php echo $result->LoginBackgroundHex ;?>" class="form-control">
                              </div>
                              <div class="form-group">
                                 <input type="text" placeholder="2nd Color Hex (add to set as Gradient)" name="Login2ndHex" value="<?php echo $result->Login2ndHex ;?>" class="form-control">
                              </div>
                              <p>or</p>
                              <div class="form-group">
                                 <div class="input-group">
                                    <div class="custom-file position-relative d-flex align-items-end">
                                       <label class="input-group-text btn btn-danger px-4 w-100 d-inline-block" for="login_background_image">Upload a Background Instead</label>
                                       <input type="file" name="LoginBackgroundImage[]" class="custom-file-input" id="login_background_image" aria-describedby="login_background_image">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row mobileScreensEd my-5 py-6">
                  <div class="col-md-6">
                     <div class="row">
                        <div class="col-md-6 pr-0">
                           <div class="mbl-screen d-flex align-items-start bg-success p-0  position-relative" style="min-width:auto; background-image:url('<?php echo $main_elements_background_image; ?>');">
                              <div class="form-group m-0 w-100 ">
                                 <div class="bg-white pt-3 pb-2 px-2 d-flex justify-content-between rounded-top" <?php echo ($result->TopTabBarColor != '' ? 'style="background-color: '.$result->TopTabBarColor.' !important;"' : ''); ?>>
                                    <div class="haveColorButton">
                                       <a href="javascript:void(0);" class="itsColorButton"></a>
                                       <input type="color" name="TopTabBarColor" value="<?php echo $result->TopTabBarColor; ?>" class="clr-pick p-0 border-0 opacity-0 position-absolute top-0 left-0" style="opacity: 0;margin: -15px 0 0 13px;width: 32px;z-index: 11;height: 32px;cursor: pointer;">
                                    </div>
                                    <div class="dropify-wrapper border-0 bg-transparent h-auto p-0">
                                       <div class="dropify-message p-0 shadow-none">
                                          <p class=" text-danger lineHeight1-3 m-0 d-flex align-items-center smallFontSize pl-2"><i class="fa fa-image position-relative fa-2x mr-1 text-danger"></i> Upload your Logo</p>
                                       </div>
                                       <div class="dropify-loader"></div>
                                       <input type="file" class="dropify" data-height="300">
                                    </div>
                                    <div class="icons d-flex pl-2">
                                       <a href="javscript:void(0);" class="text-danger"><i class="fa fa-cart-plus"></i></a>
                                       <a href="javscript:void(0);" class="text-danger ml-3"><i class="fa fa-search"></i></a>
                                    </div>
                                 </div>
                                
                                 <div class="card align-middle border-0 shadow mt-5 p-3 mx-3">
                                    <div class="haveColorButton">
                                      <p class="smallFontSize text-dark" <?php echo ($result->AlertTextColor != '' ? 'style="color: '.$result->AlertTextColor.' !important;"' : ''); ?>>Not sure what you're going to do with love this icon now you've found it? Check out our guide on getting started. Or deep dive into how to use a specific styling trick or method in our docs.</p>

                                      <a href="javascript:void(0);" class="itsColorButton"></a>
                                      <input type="color" name="AlertTextColor" value="<?php echo $result->AlertTextColor; ?>" class="clr-pick p-0 border-0 opacity-0 position-absolute top-0 left-0" style="opacity: 0;margin: -15px 0 0 13px;width: 32px;z-index: 11;height: 32px;cursor: pointer;">
                                    </div>
                                    <div class="d-flex justify-content-around w-100 rounded">
                                       <div class="haveColorButton">
                                          <button type="button" <?php echo ($result->AlertPositiveButtonColor != '' ? 'style="background-color: '.$result->AlertPositiveButtonColor.' !important;"' : ''); ?> class="mw-auto btn btn-primary waves-effect w-md waves-light m-0">Accept</button>
                                          <a href="javascript:void(0);" class="itsColorButton"></a>
                                          <input type="color" name="AlertPositiveButtonColor" value="<?php echo $result->AlertPositiveButtonColor; ?>" class="clr-pick p-0 border-0 opacity-0 position-absolute top-0 left-0" style="opacity: 0;margin: -15px 0 0 13px;width: 32px;z-index: 11;height: 32px;cursor: pointer;">
                                       </div>
                                       <div class="haveColorButton">
                                          <button type="button" <?php echo ($result->AlertNegativeButtonColor != '' ? 'style="background-color: '.$result->AlertNegativeButtonColor.' !important;"' : ''); ?> class="mw-auto btn btn-secondary waves-effect w-md waves-light m-0">Reject</button>
                                          <a href="javascript:void(0);" class="itsColorButton"></a>
                                          <input type="color" name="AlertNegativeButtonColor" value="<?php echo $result->AlertNegativeButtonColor; ?>" class="clr-pick p-0 border-0 opacity-0 position-absolute top-0 left-0" style="opacity: 0;margin: -15px 0 0 13px;width: 32px;z-index: 11;height: 32px;cursor: pointer;">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="bg-white pt-2 pb-3 px-2 d-flex justify-content-between rounded-bottom fourIconsFooter position-absolute left-0 right-0 bottom-0" <?php echo ($result->TabBarColor != '' ? 'style="background-color: '.$result->TabBarColor.' !important;"' : ''); ?>>
                                    <div class="haveColorButton">
                                       <a href="javascript:void(0);" class="itsColorButton"></a>
                                       <input type="color" name="TabBarColor" value="<?php echo $result->TabBarColor; ?>" class="clr-pick p-0 border-0 opacity-0 position-absolute top-0 left-0" style="opacity: 0;margin: -15px 0 0 13px;width: 32px;z-index: 11;height: 32px;cursor: pointer;">
                                    </div>
                                    <div class="d-flex justify-content-around w-100">
                                       <a href="javascript:void(0);" class="d-inline-block rounded bg-light text-danger text-center"><i class="fa fa-home"></i></a>
                                       <a href="javascript:void(0);" class="d-inline-block rounded bg-light text-danger text-center"><i class="fa fa-shopping-basket"></i></i></a>
                                       <a href="javascript:void(0);" class="d-inline-block rounded bg-light text-danger text-center"><i class="fa fa-tags"></i></a>
                                       <a href="javascript:void(0);" class="d-inline-block rounded bg-light text-danger text-center"><i class="fa fa-cog"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 d-flex justify-content-center">
                           <div class="align-self-center">
                              <h2 class="text-danger">Colors</h2>
                              <p class="mb-5">of main elements</p>
                              <div class="form-group">
                                 <input type="text" placeholder="Main Background Hex" name="MainElementsBackgroundColor" class="form-control" value="<?php echo $result->MainElementsBackgroundColor; ?>">
                              </div>
                              <div class="form-group">
                                 <input type="text" placeholder="2nd Color Hex (add to set as Gradient)" name="MainElementsBackground2ndColor" value="<?php echo $result->MainElementsBackground2ndColor; ?>" class="form-control">
                              </div>
                              <p>or</p>
                              <div class="form-group">
                                 <div class="input-group">
                                    <div class="custom-file position-relative d-flex align-items-end">
                                       <label class="input-group-text btn btn-danger px-4 w-100 d-inline-block" for="splashBgImage1">Upload a Background Instead</label>
                                       <input type="file" name="MainElementsBackgroundImage[]" class="custom-file-input" id="splashBgImage1" aria-describedby="splashBgImage">
                                    </div>
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="row">
                        <div class="col-md-6 pr-0">
                           <div class="mbl-screen d-flex align-items-start bg-success p-0  position-relative" style="min-width:auto; background-image:url('');">
                              <div class="form-group m-0 w-100 ">
                                 <div class="bg-white pt-3 pb-2 px-2 d-flex justify-content-between rounded-top" <?php echo ($result->TopTabBarColor != '' ? 'style="background-color: '.$result->TopTabBarColor.' !important;"' : ''); ?>>
                                    <div class="dropify-wrapper border-0 bg-transparent h-auto p-0">
                                       <div class="dropify-message p-0 shadow-none">
                                          <p class=" text-danger lineHeight1-3 m-0 d-flex align-items-center smallFontSize pl-2"><i class="fa fa-image position-relative fa-2x mr-1 text-danger"></i> Upload your Logo</p>
                                       </div>
                                       <div class="dropify-loader"></div>
                                       <input type="file" class="dropify" data-height="300">
                                    </div>
                                    <div class="icons d-flex pl-2">
                                       <a href="javscript:void(0);" class="text-danger"><i class="fa fa-cart-plus"></i></a>
                                       <a href="javscript:void(0);" class="text-danger ml-3"><i class="fa fa-search"></i></a>
                                    </div>
                                 </div>
                                 <div class="prodsNewStyle d-flex align-items-start flex-wrap">
                                       <div id="" class="pb-5 noBefore d-flex justify-content-between align-items-stretch px-3 flex-wrap">

                                           <?php if($products && $result->ShowActualProducts == 1){
                                             foreach($products as $product){ 
                                                 $image_name = get_images($product['ProductID'],'product',false);
                                                 $image = base_url()."uploads/product_images/286468046620200805104107targus_0002_Layer_4.jpg";   
                                                  
                                                 if(file_exists($image_name)){
                                                                 
                                                     $image = base_url().$image_name;
                                                 }
                                         ?>
                                          
                                          <div class="singleProduct position-relative text-center search_product">
                                             <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="<?php echo $image; ?>" alt="product" height="83" width="83"></div>
                                             <p class="m-0 text-danger bigger text-truncate" title="Protection"><?php echo $product['BrandTitle']; ?></p>
                                             <p class="text-dark my-1"><?php echo $product['Title']; ?></p>
                                             <p class="m-0 text-success bigger"><?php echo $product['Price']; ?> SAR</p>
                                             <div class="position-absolute bottom-0 left-0 right-0 px-2">
                                                <button class="btn btn-primary d-flex justify-content-between align-items-center w-100  mb-2 px-2 w-100%"><i class="fa fa-cart-plus"></i> <span class="text-white">00.00</span></button>
                                             </div>
                                          </div>
                                       <?php } }else{ ?>

                                          <div class="singleProduct position-relative text-center search_product">
                                             <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="<?php echo base_url('uploads/product_images/286468046620200805104107targus_0002_Layer_4.jpg'); ?>" alt="product" height="83" width="83"></div>
                                             <p class="m-0 text-danger bigger text-truncate" title="Protection">Brand</p>
                                             <p class="text-dark my-1">Product Title</p>
                                             <p class="m-0 text-success bigger">10.00 SAR</p>
                                             <div class="position-absolute bottom-0 left-0 right-0 px-2">
                                                <button class="btn btn-primary d-flex justify-content-between align-items-center w-100  mb-2 px-2 w-100%"><i class="fa fa-cart-plus"></i> <span class="text-white">00.00</span></button>
                                             </div>
                                          </div>
                                          <div class="singleProduct position-relative text-center search_product">
                                             <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="<?php echo base_url('uploads/product_images/286468046620200805104107targus_0002_Layer_4.jpg'); ?>" alt="product" height="83" width="83"></div>
                                             <p class="m-0 text-danger bigger text-truncate" title="Protection">Brand</p>
                                             <p class="text-dark my-1">Product Title</p>
                                             <p class="m-0 text-success bigger">10.00 SAR</p>
                                             <div class="position-absolute bottom-0 left-0 right-0 px-2">
                                                <button class="btn btn-primary d-flex justify-content-between align-items-center w-100  mb-2 px-2 w-100%"><i class="fa fa-cart-plus"></i> <span class="text-white">00.00</span></button>
                                             </div>
                                          </div>
                                          <div class="singleProduct position-relative text-center search_product">
                                             <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="<?php echo base_url('uploads/product_images/286468046620200805104107targus_0002_Layer_4.jpg'); ?>" alt="product" height="83" width="83"></div>
                                             <p class="m-0 text-danger bigger text-truncate" title="Protection">Brand</p>
                                             <p class="text-dark my-1">Product Title</p>
                                             <p class="m-0 text-success bigger">10.00 SAR</p>
                                             <div class="position-absolute bottom-0 left-0 right-0 px-2">
                                                <button class="btn btn-primary d-flex justify-content-between align-items-center w-100  mb-2 px-2 w-100%"><i class="fa fa-cart-plus"></i> <span class="text-white">00.00</span></button>
                                             </div>
                                          </div>
                                          <div class="singleProduct position-relative text-center search_product">
                                             <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="<?php echo base_url('uploads/product_images/286468046620200805104107targus_0002_Layer_4.jpg'); ?>" alt="product" height="83" width="83"></div>
                                             <p class="m-0 text-danger bigger text-truncate" title="Protection">Brand</p>
                                             <p class="text-dark my-1">Product Title</p>
                                             <p class="m-0 text-success bigger">10.00 SAR</p>
                                             <div class="position-absolute bottom-0 left-0 right-0 px-2">
                                                <button class="btn btn-primary d-flex justify-content-between align-items-center w-100  mb-2 px-2 w-100%"><i class="fa fa-cart-plus"></i> <span class="text-white">00.00</span></button>
                                             </div>
                                          </div>


                                       <?php } ?>
                                          
                                    </div>
                                 </div>
                                 
                                 <div class="bg-white pt-2 pb-3 px-2 d-flex justify-content-between rounded-bottom fourIconsFooter position-absolute left-0 right-0 bottom-0" <?php echo ($result->TabBarColor != '' ? 'style="background-color: '.$result->TabBarColor.' !important;"' : ''); ?>>
                                    <div class="d-flex justify-content-around w-100">
                                       <a href="javascript:void(0);" class="d-inline-block rounded bg-light text-danger text-center"><i class="fa fa-home"></i></a>
                                       <a href="javascript:void(0);" class="d-inline-block rounded bg-light text-danger text-center"><i class="fa fa-shopping-basket"></i></i></a>
                                       <a href="javascript:void(0);" class="d-inline-block rounded bg-light text-danger text-center"><i class="fa fa-tags"></i></a>
                                       <a href="javascript:void(0);" class="d-inline-block rounded bg-light text-danger text-center"><i class="fa fa-cog"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <?php

                        $tab_image_home_icon = base_url().'assets/images/icon_home.png';
                        if($result->TabImageHomeIcon != ''){
                          if(file_exists($result->TabImageHomeIcon)){
                                $tab_image_home_icon = base_url().$result->TabImageHomeIcon;
                          }
                        
                        }

                        $tab_image_catalouge_icon = base_url().'assets/images/icon_catalogue.png';
                        if($result->TabImageCatalougeIcon != ''){
                          if(file_exists($result->TabImageCatalougeIcon)){
                                $tab_image_catalouge_icon = base_url().$result->TabImageCatalougeIcon;
                          }
                        
                        }

                        $tab_image_orders_icon = base_url().'assets/images/icon_Orders.png';
                        if($result->TabImageOrdersIcon != ''){
                          if(file_exists($result->TabImageOrdersIcon)){
                                $tab_image_orders_icon = base_url().$result->TabImageOrdersIcon;
                          }
                        
                        }

                        $tab_image_setting_icon = base_url().'assets/images/icon_Settings.png';
                        if($result->TabImageSettingIcon != ''){
                          if(file_exists($result->TabImageSettingIcon)){
                                $tab_image_setting_icon = base_url().$result->TabImageSettingIcon;
                          }
                        
                        }




                        $tab_image_home_hover_icon = base_url().'assets/images/icon_Green_Home.png';
                        if($result->TabImageHomeHoverIcon != ''){
                          if(file_exists($result->TabImageHomeHoverIcon)){
                                $tab_image_home_hover_icon = base_url().$result->TabImageHomeHoverIcon;
                          }
                        
                        }

                        $tab_image_catalouge_hover_icon = base_url().'assets/images/icon_Green_Catalogue.png';
                        if($result->TabImageCatalougeHoverIcon != ''){
                          if(file_exists($result->TabImageCatalougeHoverIcon)){
                                $tab_image_catalouge_hover_icon = base_url().$result->TabImageCatalougeHoverIcon;
                          }
                        
                        }

                        $tab_image_orders_hover_icon = base_url().'assets/images/icon_GreenOrders.png';
                        if($result->TabImageOrdersHoverIcon != ''){
                          if(file_exists($result->TabImageOrdersHoverIcon)){
                                $tab_image_orders_hover_icon = base_url().$result->TabImageOrdersHoverIcon;
                          }
                        
                        }

                        $tab_image_setting_hover_icon = base_url().'assets/images/icon_Green_settings.png';
                        if($result->TabImageSettingHoverIcon != ''){
                          if(file_exists($result->TabImageSettingHoverIcon)){
                                $tab_image_setting_hover_icon = base_url().$result->TabImageSettingHoverIcon;
                          }
                        
                        }

                        $tab_image_home_selected_icon = base_url().'assets/images/icon_Order.png';
                        if($result->TabImageHomeSelectedIcon != ''){
                          if(file_exists($result->TabImageHomeSelectedIcon)){
                                $tab_image_home_selected_icon = base_url().$result->TabImageHomeSelectedIcon;
                          }
                        
                        }

                        $tab_image_catalouge_selected_icon = base_url().'assets/images/icon_cart.png';
                        if($result->TabImageCatalougeSelectedIcon != ''){
                          if(file_exists($result->TabImageCatalougeSelectedIcon)){
                                $tab_image_catalouge_selected_icon = base_url().$result->TabImageCatalougeSelectedIcon;
                          }
                        
                        }

                        $tab_image_orders_selected_icon = base_url().'assets/images/icon_search.png';
                        if($result->TabImageOrdersSelectedIcon != ''){
                          if(file_exists($result->TabImageOrdersSelectedIcon)){
                                $tab_image_orders_selected_icon = base_url().$result->TabImageOrdersSelectedIcon;
                          }
                        
                        }

                        $tab_image_setting_selected_icon = base_url().'assets/images/icon_whiteCart.png';
                        if($result->TabImageSettingSelectedIcon != ''){
                          if(file_exists($result->TabImageSettingSelectedIcon)){
                                $tab_image_setting_selected_icon = base_url().$result->TabImageSettingSelectedIcon;
                          }
                        
                        }
                                          
                                          
                                          
                        ?>
                        <div class="col-md-6 d-flex justify-content-start">
                           <div class="align-self-center w-100">
                              <h2 class="text-danger">Icons</h2>
                              <p class="mb-5">shown in Application</p>
                             <div class="switchCase d-flex align-items-center mb-3">
                                <p class="mediumFontSize text-dark m-0 pr-2">Show actual products</p>
                                 <label class="switch m-0">
                                    <input type="checkbox" name="ShowActualProducts" value="1" <?php echo ($result->ShowActualProducts == 1 ? 'checked' : ''); ?>>
                                    <span class="slider round"></span>
                                 </label>
                             </div>
                              <div class="fourBicIcons d-flex justify-content-between mb-4">
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_home" style="background-image:url('<?php echo $tab_image_home_icon; ?>')"><input id="icon_home" name="TabImageHomeIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Home</span></div>
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_catalogue" style="background-image:url('<?php echo $tab_image_catalouge_icon; ?>')"><input id="icon_catalogue" name="TabImageCatalougeIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Catalogue</span></div>
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_Orders" style="background-image:url('<?php echo $tab_image_orders_icon; ?>')"><input id="icon_Orders" name="TabImageOrdersIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Orders</span></div>
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_Settings" style="background-image:url('<?php echo $tab_image_setting_icon; ?>')"><input id="icon_Settings" name="TabImageSettingIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Settings</span></div>
                             </div>
                              
                             <div class="fourBicIcons d-flex justify-content-between mb-4">
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_Green_Home" style="background-image:url('<?php echo $tab_image_home_hover_icon; ?>')"><input id="icon_Green_Home" name="TabImageHomeHoverIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Home</span></div>
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_Green_Catalogue" style="background-image:url('<?php echo $tab_image_catalouge_hover_icon; ?>')"><input id="icon_Green_Catalogue" name="TabImageCatalougeHoverIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Catalogue</span></div>
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_GreenOrders" style="background-image:url('<?php echo $tab_image_orders_hover_icon; ?>')"><input id="icon_GreenOrders" name="TabImageOrdersHoverIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Orders</span></div>
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_Green_settings" style="background-image:url('<?php echo $tab_image_setting_hover_icon; ?>')"><input id="icon_Green_settings" name="TabImageSettingHoverIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Settings</span></div>
                             </div>
                              
                             <div class="fourBicIcons d-flex justify-content-between mb-4">
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_Order" style="background-image:url('<?php echo $tab_image_home_selected_icon; ?>')"><input id="icon_Order" name="TabImageHomeSelectedIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Home</span></div>
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_cart" style="background-image:url('<?php echo $tab_image_catalouge_selected_icon; ?>')"><input id="icon_cart" name="TabImageCatalougeSelectedIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Catalogue</span></div>
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_search" style="background-image:url('<?php echo $tab_image_orders_selected_icon; ?>')"><input id="icon_search" name="TabImageOrdersSelectedIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Orders</span></div>
                                 <div class="block position-relative"><span class="d-block rounded thisIsColored text-center mx-auto haveFileTypeEd"><label for="icon_whiteCart" style="background-image:url('<?php echo $tab_image_setting_selected_icon; ?>')"><input id="icon_whiteCart" name="TabImageSettingSelectedIcon[]" type="file" class="buttonIcon d-none position-absolute top-0 left-0" value="" ></span><span class="d-block w-100 smallFontSize text-dark mt-1 text-center">Settings</span></div>
                             </div>
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="my-4 d-flex justify-content-between align-items-center">
                  <button type="button" class="btn btn-light px-4 w-auto text-left text-white border-0">Proceed to Next Step</button>
                  <button type="submit" class="btn btn-primary waves-effect w-md waves-light m-0">Save</button>
               </div>
               
            </div>
            <div class="tab-pane" id="step_3" role="tabpanel" aria-labelledby="step_3-tab">
               <div class="row">
                  <div class="col-md-4 my-3  d-flex align-items-center">
                     <div class="lightBgControlForm w-100">
                        <div class="form-group">
                           <div class="input-group position-relative">
                              <input type="text" class="form-control" name="AppStoreTitle" value="<?php echo $result->AppStoreTitle; ?>" id="" placeholder="Application Title on Store">
                              <div class="input-group-append">
                                 <div class="input-group-text position-absolute right-0 top-0 border-0 pl-0 pb-0 pt-1 pr-2 bg-transparent"><i class="fa fa-exclamation-circle fa-2x text-secondary opacity-05"></i></div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="input-group position-relative">
                              <input type="text" class="form-control" name="AppName" value="<?php echo $result->AppName; ?>" id="" placeholder="Application Name on mobile">
                              <div class="input-group-append">
                                 <div class="input-group-text position-absolute right-0 top-0 border-0 pl-0 pb-0 pt-1 pr-2 bg-transparent"><i class="fa fa-exclamation-circle fa-2x text-secondary opacity-05"></i></div>
                              </div>
                           </div>
                        </div>
                        <div class="d-flex justify-content-between">
                           <div class="dropify-wrapper border-danger border-dashed mb-3 pt-3" style="height: 127px;width: 127px;">
                              <div class="dropify-message">
                                 <i class="fa fa-image position-relative fa-2x mb-2 text-danger"></i>
                                 <p class=" text-danger lineHeight1-3">Upload your Logo</p>
                                 <p class="dropify-error">Ooops, something wrong appended.</p>
                              </div>
                              <div class="dropify-loader"></div>
                              <div class="dropify-errors-container">
                                 <ul>
                                 </ul>
                              </div>
                              <input type="file" class="dropify" data-height="300">
                              <button type="button" class="dropify-clear">Remove</button>
                              <div class="dropify-preview">
                                 <span class="dropify-render"></span>
                                 <div class="dropify-infos">
                                    <div class="dropify-infos-inner">
                                       <p class="dropify-filename"><i class="fa fa-image position-relative fa-2x mb-2 text-danger"></i> <span class="dropify-filename-inner"></span></p>
                                       <p class="dropify-infos-message text-danger">Upload your Logo</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="text" style="width: calc(100% - 140px );">
                              <p class="text-dark mediumFontSize font-weight-bold">Application Icon</p>
                              <p class="text-dark mediumFontSize">Upload the application icon without rounded corners with minimum 1280 x 1280 px dimensions.</p>
                              <p class="text-dark mediumFontSize">The icon should be *.jpg (without transparency) and shouldn't include rounded corners.</p>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="input-group position-relative">
                              <textarea name="AppDescription" id="" cols="30" rows="10" class="form-control" id="" placeholder="Application Description"><?php echo $result->AppDescription; ?></textarea>
                              <div class="input-group-append">
                                 <div class="input-group-text position-absolute right-0 top-0 border-0 pl-0 pb-0 pt-1 pr-2 bg-transparent"><i class="fa fa-exclamation-circle fa-2x text-secondary opacity-05"></i></div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="input-group position-relative">
                              <textarea name="AppKeywords" id="" cols="30" rows="10" class="form-control" id="" placeholder="Keywords"><?php echo $result->AppKeywords; ?></textarea>
                              <div class="input-group-append">
                                 <div class="input-group-text position-absolute right-0 top-0 border-0 pl-0 pb-0 pt-1 pr-2 bg-transparent"><i class="fa fa-exclamation-circle fa-2x text-secondary opacity-05"></i></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-8 d-flex justify-content-around">
                     <div class="">
                        <div class="mbl-screen d-flex align-items-center mr-2 my-3">
                           <div class="wrapper">
                              <div class="text-primary my-3">
                                 <a href="#." class="smallFontSize"><i class="fa fa-chevron-left"></i> Back</a>
                              </div>
                              <div class="d-flex justify-content-start align-items-start">
                                 <div class="imgBox mr-3"><img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_175e02bea8a%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_175e02bea8a%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.421875%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" height="73" width="73" class="rounded" alt=""></div>
                                 <div class="text w-100">
                                    <p class="text-dark mediumFontSize font-weight-bold mb-0">Application Title</p>
                                    <p class="text-dark mediumFontSize mb-0">Niehz</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                       <button type="button" class="btn btn-primary waves-effect w-auto waves-light m-0">Get</button>
                                       <i class="fa fa-upload text-dark fa-2x"></i>
                                    </div>
                                 </div>
                              </div>
                              <div class="text-muted d-flex justify-content-between align-items-center my-3">
                                 <div class="left">
                                    <p class="m-0 largeFontSize">
                                       3.5
                                       <i class="fa fa-star"></i>
                                       <i class="fa fa-star"></i>
                                       <i class="fa fa-star-half-alt"></i>
                                       <i class="fa fa-star"></i>
                                       <i class="fa fa-star"></i>
                                    </p>
                                    <p class="m-0 smallerFontSize">2500 Ratings</p>
                                 </div>
                                 <div class="right">
                                    <p class="m-0 largeFontSize">13+</p>
                                    <p class="m-0 smallerFontSize">Age</p>
                                 </div>
                              </div>
                              <p class="text-dark mediumFontSize">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                           </div>
                        </div>
                        <p class="text-danger largeFontSize font-weight-bold text-center my-3">IOS Store Preview</p>
                     </div>
                     <div class="">
                        <div class="mbl-screen d-flex align-items-center mr-2 my-3"></div>
                        <p class="text-danger largeFontSize font-weight-bold text-center my-3">Android Preview</p>
                     </div>
                  </div>
               </div>
               <div class="my-4 d-flex justify-content-between align-items-center">
                  <button type="button" class="btn btn-light px-4 w-auto text-left text-white border-0">Proceed to Next Step</button>
                  <button type="submit" class="btn btn-primary waves-effect w-md waves-light m-0">Save</button>
               </div>
            </div>
         </div>
      </form>
      </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
   $('#uploadSlashLogo').change(function(){ 
     getFileName();
   });
   function getFileName(elm) {
     var filePath=$('#uploadSlashLogo').val(); 
      var fn = $(elm).val();
      var filename = fn.match(/[^\\/]*$/)[0]; // remove C:\fakename
      alert(filePath);
   }
   
</script>