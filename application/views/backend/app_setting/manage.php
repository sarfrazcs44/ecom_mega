<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">App Setting</h4>
                        
                        <div class="material-datatables">
                            <table id="" class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
 
                                    <th>Company</th>

                                    <th>Created At</th>
                                  
                                    <th>Action</th>

                                    

                                </tr>
                                </thead>
                                <tbody>
                                     <?php if($companies){
                                                foreach($companies as $value){ ?>
                                                    <tr>
                                                   
                                                   
                                                    <th><?php echo CompanyName($value->CompanyID,$language); ?></th>
                                                     
                                                    <th><?php echo $value->CreatedAt; ?></th>
                                                        
                                                    <th>
                                                       
                                                        <a href="<?php echo base_url('cms/' . $this->data['ControllerName'] . '/edit/' . $value->CompanyID) ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons" title="Edit">dvr</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
