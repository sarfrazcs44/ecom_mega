<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Packages">Package</label>
                                            <select class="form-control" name="PackageID">
              
                                               <?php
                                               if(!empty($packages)){

                                                foreach($packages as $package){ ?>
                                                <option value="<?php echo $package->PackageID; ?>"><?php echo $package->Title; ?></option>
                                               <?php } } ?>
                                           </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DomainName">Domain</label>
                                        <input type="text" name="DomainName" required  class="form-control" id="DomainName">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">Description</label>
                                        <textarea class="form-control summernote" name="Description"
                                                  id="Description" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <h4 class="col-xs-12">Template Setting</h4>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Homepage">Homepage</label>
                                        <select name="Homepage" class="selectpicker" data-style="select-with-transition" id="Homepage" required >
                                                <option value="1">Homepage 1</option>
                                                <option value="2">Homepage 2</option>
                                                <option value="3">Homepage 3</option>
                                                <option value="4">Homepage 4</option>
                                                <option value="5">Homepage 5</option>
                                                <option value="6">Homepage 6</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Product">Product</label>
                                        <select name="Product" class="selectpicker" data-style="select-with-transition" id="Product" required >
                                                <option value="1">Product 1</option>
                                                <option value="2">Product 2</option>
                                                <option value="3">Product 3</option>
                                                <option value="4">Product 4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <h4 class="col-xs-12">Email SMTP Setting</h4>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Protocol">Protocol</label>
                                        <input type="text" name="Protocol" required  class="form-control" id="Protocol">
                                       
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Host">Host</label>
                                        <input type="text" name="Host" required  class="form-control" id="Host">
                                        
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Port">Port</label>
                                        <input type="text" name="Port" required  class="form-control" id="Port">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="User">User Name</label>
                                        <input type="text" name="User" required  class="form-control" id="User">
                                       
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Pass">Password</label>
                                        <input type="text" name="Pass" required  class="form-control" id="Pass">
                                        
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FromEmail">From Email</label>
                                        <input type="text" name="FromEmail" required  class="form-control" id="FromEmail">
                                        
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>