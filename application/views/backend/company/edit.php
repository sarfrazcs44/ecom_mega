<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        $common_fields5 = '';
        if($key == 0){
        
        $common_fields3 .= '<div class="row">
                                <h4 class="col-xs-12">Template Setting</h4>
                                <div class="col-md-6 col-xs-12"><div class="form-group label-floating">
                                <label class="control-label" for="Homepage">Homepage</label> 
                                    <select name="Homepage" class="selectpicker" data-style="select-with-transition" id="Homepage" required>';


                               
                                
                                $common_fields3 .= '<option '.((isset($result[$key]->Homepage) && $result[$key]->Homepage ==                    1) ? 'selected' : '').' value="1">Homepage 1</option>
                                                    <option '.((isset($result[$key]->Homepage) && $result[$key]->Homepage == 2) ? 'selected' : '').' value="2">Homepage 2</option>
                                                    <option '.((isset($result[$key]->Homepage) && $result[$key]->Homepage == 3) ? 'selected' : '').' value="3">Homepage 3</option>
                                                    <option '.((isset($result[$key]->Homepage) && $result[$key]->Homepage == 4) ? 'selected' : '').' value="4">Homepage 4</option>
                                                    <option '.((isset($result[$key]->Homepage) && $result[$key]->Homepage == 5) ? 'selected' : '').' value="5">Homepage 5</option>
                                                    <option '.((isset($result[$key]->Homepage) && $result[$key]->Homepage == 6) ? 'selected' : '').' value="6">Homepage 6</option>';
                                 
        $common_fields3 .= '</select>';
        $common_fields3 .= '</div></div>';

        $common_fields4 .= '<div class="col-md-6 col-xs-12">
                            <div class="form-group label-floating">
                                <label class="control-label" for="Product">Product</label> 
                                    <select name="Product" class="selectpicker" data-style="select-with-transition" id="Product" required>';


                               
                                
                                $common_fields4 .= '<option '.((isset($result[$key]->Product) && $result[$key]->Product == 1)                     ? 'selected' : '').' value="1">Product 1</option>
                                                    <option '.((isset($result[$key]->Product) && $result[$key]->Product == 2) ? 'selected' : '').' value="2">Product 2</option>
                                                    <option '.((isset($result[$key]->Product) && $result[$key]->Product == 3) ? 'selected' : '').' value="3">Product 3</option>
                                                    <option '.((isset($result[$key]->Product) && $result[$key]->Product == 4) ? 'selected' : '').' value="4">Product 4</option>';
                                 
        $common_fields4 .= '</select>';
        $common_fields4 .= '</div></div></div>';


        $common_fields .=   '<div class="col-md-6">
                            <div class="form-group label-floating">
                            <label class="control-label" for="Packages">Package</label>
                            <select class="form-control" name="PackageID">';


                               if(!empty($packages)){

                                foreach($packages as $package){ 
                                $common_fields .= '<option value="'.$package->PackageID.'" '.($result[$key]->PackageID == $package->PackageID ? 'selected' : '').'>'.$package->Title.'</option>';
                               } }  
        $common_fields .= '</select>';
        $common_fields .= '</div></div>';
                                                     

        $common_fields2 = '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>';
        $common_fields5 = '<hr/>
                            <div class="row">
                                <h4 class="col-xs-12">Email SMTP Setting</h4>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Protocol">Protocol</label>
                                        <input type="text" name="Protocol" required  class="form-control" id="Protocol" value="'.$result[0]->Protocol.'">
                                       
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Host">Host</label>
                                        <input type="text" name="Host" required  class="form-control" id="Host" value="'.$result[0]->Host.'">
                                        
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Port">Port</label>
                                        <input type="text" name="Port" required  class="form-control" id="Port" value="'.$result[0]->Port.'">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="User">User Name</label>
                                        <input type="text" name="User" required  class="form-control" id="User" value="'.$result[0]->User.'">
                                       
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Pass">Password</label>
                                        <input type="text" name="Pass" required  class="form-control" id="Pass" value="'.$result[0]->Pass.'">
                                        
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FromEmail">From Email</label>
                                        <input type="text" name="FromEmail" required  class="form-control" id="FromEmail" value="'.$result[0]->FromEmail.'">
                                        
                                    </div>
                                </div>
                            </div>';                        

        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').'</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                         '.$common_fields.'
                                                    </div>
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="DomainName'.$key.'">Domain</label>
                                                                <input type="text" name="DomainName" parsley-trigger="change" required  class="form-control" id="DomainName'.$key.'" value="'.((isset($result[$key]->DomainName)) ? $result[$key]->DomainName : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                     <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description' . $key . '">Description</label>
                                                                <textarea class="form-control summernote" name="Description"
                                                                          id="Description' . $key . '" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    '.$common_fields3.'
                                                    '.$common_fields4.'
                                                    '.$common_fields5.'
                                                    <hr/>
                                                    '.$common_fields2.'


                                                    
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>