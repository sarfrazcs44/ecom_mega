<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Feedback & CV Collection</h4>
                        <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="active nav-pills-warning">
                                    <a href="#feedback" data-toggle="tab" aria-expanded="false">Feedback</a>
                                </li>
                                <li class="">
                                    <a href="#career" data-toggle="tab" aria-expanded="false">CV Collection</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="feedback">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Company</th>
                                        <th>Department</th>
                                        <th>Message</th>
                                        <th>Created At</th>
                                        <?php if (checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($feedbacks) {
                                        foreach ($feedbacks as $value) { ?>
                                            <tr id="<?php echo $value->ContactRequestID; ?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->Company; ?></td>
                                                <td><?php echo $value->Department; ?></td>
                                                <td><?php echo $value->Message; ?></td>
                                                <td><?php echo getFormattedDateTime($value->CreatedAt, 'd M Y h:i A'); ?></td>
                                                <?php if (checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td>
                                                        <?php if (checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                            <a href="javascript:void(0);"
                                                               onclick="deleteRecord('<?php echo $value->ContactRequestID; ?>','cms/contactRequest/action','')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="career">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>CV</th>
                                        <th>Created At</th>
                                        <?php if (checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($careers) {
                                        foreach ($careers as $value) { ?>
                                            <tr id="<?php echo $value->ContactRequestID; ?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><a href="<?php echo base_url($value->CV); ?>" target="_blank"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxTWInx2VRcMJrxM8wJog0QnJa3O70mmT_GBW5gDgQ4kmjMardhg" style="width:40px !important;height:40px !important;"></a></td>
                                                <td><?php echo getFormattedDateTime($value->CreatedAt, 'd M Y h:i A'); ?></td>
                                                <?php if (checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td>
                                                        <?php if (checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                            <a href="javascript:void(0);"
                                                               onclick="deleteRecord('<?php echo $value->ContactRequestID; ?>','cms/contactRequest/action','')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>