<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Edit Customer Group</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/CustomerGroup/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="CustomerGroupID" value="<?php echo $group_detail->CustomerGroupID; ?>">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="GroupType">Group Type</label>
                                        <select id="GroupType" class="selectpicker" data-style="select-with-transition"
                                                name="CustomerGroupType">
                                            <option value="Orders" <?php echo ($group_detail->CustomerGroupType == 'Orders' ? 'selected' : ''); ?>>Orders</option>
                                            <option value="Purchases" <?php echo ($group_detail->CustomerGroupType == 'Purchases' ? 'selected' : ''); ?>>Purchases</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="MinimumValue">Minimum Value</label>
                                        <input type="text" name="MinimumValue" required class="form-control" id="MinimumValue" value="<?php echo $group_detail->MinimumValue; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="MaximumValue">Maximum Value</label>
                                        <input type="text" name="MaximumValue" required class="form-control" id="MaximumValue" value="<?php echo $group_detail->MaximumValue; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-default waves-effect m-l-5" onclick="showRecords();">
                                        Show Records
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CustomerGroupTitle">Customer Group Title</label>
                                        <input type="text" name="CustomerGroupTitle" required class="form-control" id="CustomerGroupTitle" value="<?php echo $group_detail->CustomerGroupTitle; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit'); ?>
                                </button>
                                <a href="<?php echo base_url(); ?>cms/CustomerGroup">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back'); ?>
                                    </button>
                                </a>
                            </div>

                        </form>
                        <div id="showRecords"></div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>