<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName); ?></h4>
                            <ul class="nav nav-pills">
                                <li class="<?php echo($url_status == 'all_customers' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/customers?status=all_customers'); ?>">All Customers</a>
                                </li>
                                <li class="<?php echo($url_status == 'online_customers' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/customers?status=online_customers'); ?>">Now Online</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="all_customers">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Mobile No.</th>
                                    <th>Active</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                            
                            
                        </div>
                        </div>
                           
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    var columns = <?php echo $columns; ?>;
    ParentID = '<?php echo $url_status; ?>';
    var CallUrl = '<?php echo $ControllerName;?>';
</script>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
