<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-content">
                        <p class="category">Total Orders</p>
                        <h3 class="card-title"><?php echo $TotalOrders; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-content">
                        <p class="category">New Order</p>
                        <h3 class="card-title"><?php echo $TotalNewOrders; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-content">
                        <p class="category">Completed Orders</p>
                        <h3 class="card-title"><?php echo $CompletedOrdersCount; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-content">
                        <p class="category">Cancelled Orders</p>
                        <h3 class="card-title"><?php echo $CancelOrdersCount; ?></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group label-floating">
                    <label class="control-label" for="Year"><?php echo 'Choose Year'; ?></label>
                    <select id="Year" class="selectpicker" data-style="select-with-transition" required="">
                        <?php for ($i = 0; $i < 11; $i++) { ?>
                            <option value="<?php echo date('Y', strtotime('-' . $i . ' year')); ?>" <?php echo(date('Y', strtotime('-' . $i . ' year')) == $year ? 'selected' : ''); ?>><?php echo date('Y', strtotime('-' . $i . ' year')); ?></option>

                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        <i class="material-icons">timeline</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Total Sales
                            <small> - Rounded</small>
                        </h4>
                    </div>
                    <div id="colouredRoundedLineChart1" class="ct-chart"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="rose">
                        <i class="material-icons">insert_chart</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Total Orders
                            <small></small>
                        </h4>
                    </div>
                    <div id="multipleBarsChart1" class="ct-chart"></div>
                </div>
            </div>
        </div>


    </div>
</div>


<script type="text/javascript">


    $(document).ready(function () {

        // Javascript method's body can be found in assets/js/demos.js
        //demo.initDashboardPageCharts();
        //  demo.initCharts();

        //demo.initVectorMap();
        $('#Year').on('change', function () {

            window.location.replace("<?php echo base_url('cms/dashboard/index/');?>" + $("#Year option:selected").val());

        });
        var month_array = <?php echo json_encode(getMonths($language));?>;


        dataColouredRoundedLineChart = {
            labels: month_array,
            series: [
                <?php echo json_encode($TotalSales);?>
            ]
        };

        optionsColouredRoundedLineChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 10
            }),
            axisY: {
                showGrid: true,
                offset: 40
            },
            axisX: {
                showGrid: false,
            },
            low: 0,
            // high: 10000,
            showPoint: true,
            showArea: true,
            height: '300px'
        };


        var colouredRoundedLineChart = new Chartist.Line('#colouredRoundedLineChart1', dataColouredRoundedLineChart, optionsColouredRoundedLineChart);

        md.startAnimationForLineChart(colouredRoundedLineChart);


        var dataMultipleBarsChart = {
            labels: month_array,
            series: [
                <?php echo json_encode($TotalOrdersInMonths); ?>
            ]
        };

        var optionsMultipleBarsChart = {
            seriesBarDistance: 10,
            axisX: {
                showGrid: false
            },
            height: '300px',
            plugins: [
                Chartist.plugins.tooltip()
            ]
        };

        var responsiveOptionsMultipleBarsChart = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        var multipleBarsChart = Chartist.Bar('#multipleBarsChart1', dataMultipleBarsChart, optionsMultipleBarsChart, responsiveOptionsMultipleBarsChart);

        //start animation for the Emails Subscription Chart
        md.startAnimationForBarChart(multipleBarsChart);
    });
</script>