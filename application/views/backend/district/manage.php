
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('title');?></th>
                                    <th><?php echo lang('city');?></th>

                                    <th><?php echo lang('is_active');?></th>

                                    <?php if(checkUserRightAccess(35,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(35,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                        <th><?php echo lang('actions');?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    var columns = <?php echo $columns; ?>;
    city_id = '<?php echo $city_id; ?>'
    var CallUrl = '<?php echo $ControllerName;?>';
</script>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>