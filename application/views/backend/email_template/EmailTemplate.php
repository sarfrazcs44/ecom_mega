<?php $site_setting = site_settings(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $site_setting->SiteName; ?></title>
    <style>
        p {
            margin-bottom: 5px !important;
            margin-top: 5px !important;
        }

        h4 {

            margin-bottom: 20px;
        }

        #wrap {
            float: left;
            position: relative;
            left: 50%;
        }

        #content {
            float: left;
            position: relative;
            left: -50%;
        }
    </style>
</head>

<body>
<table width="40%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">
            <?php
            $Description = '';
            $DefaultLang = getDefaultLanguage();
            if ($DefaultLang) {
                $lang_id = $DefaultLang->SystemLanguageID;
            } else {
                $lang_id = '1';
            }
            if ($result) {
                foreach ($result as $value) {
                    if ($value->SystemLanguageID == $lang_id) {
                        $Description = $value->Description;
                    }
                }
            }
            echo $Description; ?>
            <div><!--[if mso]>
                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word"
                             href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%"
                             stroke="f" fillcolor="#10b26a">
                    <w:anchorlock/>
                    <center>
                <![endif]-->
                <!--[if mso]>
                </center>
                </v:roundrect>
                <![endif]--></div>
            <br/>
            <hr width="100%" align="left">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img src="<?php echo base_url() . $site_setting->SiteImage; ?>" width="60" height="60"
                             alt="Site Logo">
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;"><?php echo $site_setting->SiteName; ?></h4>
                        <span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
                        <span style="font-family:sans-serif;">
<a href="<?php echo site_url(); ?>"
   style="color:grey;font-size:10px;text-decoration: none;"><?php echo site_url(); ?></a>
</span>
                    </td>
                </tr>
            </table>


        </td>
    </tr>
</table>

</body>
</html>