<footer class="footer">
    <div class="container-fluid">
        <p class="copyright pull-right">
            <a href="<?php echo base_url('cms/dashboard'); ?>"><?php echo $site_setting->SiteName; ?></a> | &copy;
            <?php echo date('Y'); ?>
        </p>
    </div>
</footer>
</div>
</div>
</body>


<!-- Forms Validations Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo base_url(); ?>assets/backend/js/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.21/moment-timezone-with-data.js"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/chartist.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="http://localhost:8888/both/assets/backend/js/bootstrap-notify.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<!--   Sharrre Library    -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!-- Select Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.datatables.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>

<!--<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?php echo base_url(); ?>assets/backend/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/summernote/summernote.min.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url(); ?>assets/backend/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url(); ?>assets/backend/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/script.js?v=<?php echo rand(); ?>"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.blockUI.js"></script>
<!--
<script src="<?php echo base_url(); ?>assets/backend/plugins/jquery.filer/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.fileuploads.init.js"></script>-->

<?php if ($this->session->flashdata('message')) { ?>
    <script>
        $(document).ready(function () {
            showError('<?php echo $this->session->flashdata('message'); ?>');
        });

    </script>
<?php } ?>

<script>

        
$(document).ready(function(){
        console.log("i am working");
        // $(".edEditBtn input[type=checkbox]").click(function() { 
        //     if ($(".edEditBtn input[type=checkbox]").prop(":checked")) { 
        //         console.log("Check box in Checked"); 
        //     } else { 
        //         console.log("Check box is Unchecked"); 
        //     } 
        // }); 
    });
        
        // $('input[type=checkbox]#toggleEditPermissions').click(function(){
        //     if($(this).prop("checked") == true){
        //         $('#chkEditMode').removeClass('edEditModeDisable');
        //         console.log("Checkbox is checked.");
        //     }
        //     else if($(this).prop("checked") == false){
        //         $('#chkEditMode').addClass('edEditModeDisable');
        //     }
        // });
</script>
</html>
