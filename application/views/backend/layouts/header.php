<!doctype html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/favicon.png" type="image/x-icon">
    <!-- App title -->
    <title><?php echo site_title(); ?> Admin</title>
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>assets/backend/plugins/summernote/summernote.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url(); ?>assets/backend/css/material-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/demo.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link href="<?php echo base_url(); ?>assets/backend/plugins/chartist-plugin-tooltip/chartist-plugin-tooltip.css" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet"/>
    <!--     Fonts and icons     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/font-awesome.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/css/google-roboto-300-700.css" rel="stylesheet"/>
    <!--   Core JS Files   -->


    <!-- filer -->
   <!-- <link href="<?php echo base_url();?>assets/backend/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/backend/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />-->

    <!--end filer -->
    <script src="<?php echo base_url();?>assets/backend/js/jquery-3.1.1.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url();?>assets/backend/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/material.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/perfect-scrollbar.jquery.min.js"
            type="text/javascript"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>

    <script src="https://js.pusher.com/4.4/pusher.min.js"></script>

</head>
<script>
    
    var base_url = '<?php echo base_url(); ?>';
    var delete_msg = '<?php echo lang('are_you_sure');?>';
    var ParentID = 0;
</script>
<style>
    .error-border {
        border-bottom: 1px solid red !important;
    }

    #validation-msg {
        visibility: hidden;
        min-width: 250px;
        margin-left: -125px;
        text-align: center;
        border-radius: 2px;
        padding: 16px;
        position: fixed;
        z-index: 9999;
        left: 50%;
        top: 80px;
        font-size: 17px;
    }

    #validation-msg.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 4.5s;
        animation: fadein 0.5s, fadeout 0.5s 4.5s;
    }

    .panel-default{
        border: none !important;
    }

    .custom_style_select {
        width: 100%;
        border: none;
        border-bottom: 1.2px solid #ddd;
        padding: 15px 0 5px 0;
        font-size: 12px;
        letter-spacing: 0;
        font-weight: 400;
        text-transform: uppercase;
        font-family: inherit;
        display: inline-block;
        text-align: left;
        text-overflow: ellipsis;
        -webkit-appearance: none;
        -moz-appearance: none;
    }

</style>
<body>
<div class="wrapper">
        
  
  