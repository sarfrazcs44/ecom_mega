<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div>
                                </div>
                                 <div class="col-sm-6 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="Discount"><?php echo lang('Discount'); ?></label>
                                    <input type="text" name="Discount" class="form-control number-with-decimals"
                                           id="Discount">
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label"><?php echo 'Choose '.lang('DiscountType');?></label>
                                        <select name="DiscountType" class="form-control" id="DiscountType" required>
                                            
                                                <option value="percentage"><?php echo lang('Percentage');?></option>
                                                <option value="per item"><?php echo lang('PerItem');?></option>
                                           
                                        </select>
                                    </div>
                                </div>
                        </div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="FromDate"><?php echo lang('FromDate'); ?></label>
                                    <input type="text" name="ValidFrom"
                                           class="form-control custom_datepicker" id="FromDate">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="ToDate"><?php echo lang('ToDate'); ?></label>
                                    <input type="text" name="ValidTo" class="form-control custom_datepicker"
                                           id="ToDate">
                                </div>
                            </div>
                        </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description"><?php echo lang('description'); ?></label>
                                        <textarea class="form-control" name="Description" id="Description" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            

                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label"><?php echo 'Choose '.lang('products');?></label>
                                        <select name="ProductID[]" class="form-control" id="ProductID" required multiple>
                                            <?php foreach ($products as $key => $value) { ?>
                                                <option value="<?php echo $value->ProductID; ?>"><?php echo $value->Title; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label"><?php echo 'Choose '.lang('customer_groups');?></label>
                                        <select name="CustomerGroupID[]" class="form-control" id="CustomerGroupID" required multiple>
                                            <option value="0"><?php echo lang('ForAll'); ?></option>
                                            <?php foreach ($groups as $key => $value) { ?>
                                                <option value="<?php echo $value->CustomerGroupID; ?>"><?php echo $value->CustomerGroupTitle; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                </div>

                           



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
                   
       $('#CategoryID').on('change',function(){
           
            var CategoryID = $(this).val();

            if(CategoryID == '')
            {
                $('#SubCategoryID').html('<option value=""><?php echo lang("choose_sub_category");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/category/getSubCategory',
                    data: {
                        'CategoryID': CategoryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#SubCategoryID').html(result.html);

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
      

       

       
    });

    
</script>