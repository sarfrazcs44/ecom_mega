<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Assign Driver</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&nbsp;</span>
        </button>
    </div>
    <div class="modal-body">
        <input type="hidden" id="OrderID" value="<?php echo $OrderID; ?>">
        <input type="hidden" id="form_type" value="assign_driver">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group label-floating">
                    <label class="control-label" for="UserID">Drivers*</label>
                    <select class="custom_style_select" id="UserID" data-style="select-with-transition" required>
                        <?php if (!empty($users)) {
                            foreach ($users as $user) { ?>
                                <option value="<?php echo $user->UserID; ?>"><?php echo $user->FullName; ?> (City: <?php echo $user->CityTitle; ?>) </option>
                            <?php }
                        } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary odom-submit" id="assignOrderToDriver">Assign</button>
    </div>

</div>

<script>
    $("#assignOrderToDriver").click(function () {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        var form_type = $('#form_type').val();
        var OrderID = $('#OrderID').val();
        var UserID = $('#UserID').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>cms/orders/action",
            data: {"form_type": form_type, "OrderID": OrderID, "UserID": UserID},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                }
                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
    });
</script>