<style>
    .orderHeader .w-130 {width:130px;}
    .orderDtlRow td {
        display: table-cell !important;
        background: #f3ebd5;
        border: 0 !important;
    }
    .orderDtlRow {
        display:none;
    }
       
    .click4_OrderDtl {transition-duration:0.5s;}
    .click4_OrderDtl.open,
    .click4_OrderDtl:hover td {
        background: #f3ebd5;
    }
    .card-body {
        padding: 15px;
    }
    .card-body .wbox {
        margin-bottom: 25px;
        background-color: #ffffff;
        padding: 25px;
        border-radius: 6px;
        box-shadow: 3px 3px 13px -2px rgba(0, 0, 0, 0.59);
    }
</style>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Search</h4>
                        
                        <div class="material-datatables">
                            <form action="" method="post" style="padding-bottom: 105px;">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group is-focused">
                                            <label class="control-label" for="Email ">User Email</label>
                                            <input type="text" name="Email" class="form-control" value="<?php echo (isset($post_data['Email']) ? $post_data['Email'] : '' ); ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group is-focused">
                                            <label class="control-label" for="OrderTrackID ">Order Track ID</label>
                                            <input type="text" name="OrderTrackID" class="form-control" value="<?php echo (isset($post_data['OrderTrackID']) ? $post_data['OrderTrackID'] : '' ); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group  is-focused">
                                            <label class="control-label" for="DepositDate ">From</label>
                                            <input type="text" name="From" class="form-control custom_datepicker" value="<?php echo (isset($post_data['From']) ? $post_data['From'] : '' ); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group is-focused">
                                            <label class="control-label" for="DepositDate ">To</label>
                                            <input type="text" name="To" class="form-control custom_datepicker" value="<?php echo (isset($post_data['To']) ? $post_data['To'] : '' ); ?>">
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                               Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Orders</h4>
                        <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="<?php echo($url_status == 'pos' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=pos'); ?>">Pos</a>
                                </li>
                                <li class="<?php echo($url_status == 'pending' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=pending'); ?>">Pending</a>
                                </li>
                                <li class="<?php echo($url_status == 'packed' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=packed'); ?>">Packed</a>
                                </li>
                                <li class="<?php echo($url_status == 'dispatched' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=dispatched'); ?>">Dispatched</a>
                                </li>
                                <li class="<?php echo($url_status == 'delivered' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=delivered'); ?>">Delivered</a>
                                </li>
                                <li class="<?php echo($url_status == 'cancelled' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=cancelled'); ?>">Cancelled</a>
                                </li>
                                <li class="<?php echo($url_status == 'cancelled_not_collect' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=cancelled_not_collect'); ?>">Cancelled Not Collected</a>
                                </li>
                                <li class="<?php echo($url_status == 'returned' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=returned'); ?>">Returned</a>
                                </li>
                                <li class="<?php echo($url_status == 'all' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=all'); ?>">All</a>
                                </li>
                                <?php if ($this->session->userdata['admin']['RoleID'] == 1) { ?>
                                    <li class="<?php echo($url_status == 'unopened' ? 'active nav-pills-warning' : ''); ?>">
                                        <a href="<?php echo base_url('cms/orders?status=unopened'); ?>">Un-Opened</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr class="orderHeader">
                                    <th>Full Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Order #</th> 
                                    <th>Transaction ID </th> 
                                    <th>Order Amount</th>
                                    <?php
                                    if (true || (isset($_GET['status']) && ($_GET['status'] == 'all' || $_GET['status'] == 'unopened'))) { ?>
                                        <th>Order Status</th>
                                    <?php }
                                    ?>
                                    <th>Branch</th>
                                    <th>User City</th>
                                    <th>Assigned To</th>
                                    <th>Received At</th>
                                    <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th class="w-130"><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                               

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
    <audio id="audiotag" src="<?php echo base_url('assets/bell.mp3'); ?>" preload="auto"></audio>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" id="response_data">

    </div>
</div>
<script type="text/javascript">
    var columns = <?php echo $columns; ?>;
     ParentID = '<?php echo $customID; ?>';
    var CallUrl = '<?php echo $ControllerName;?>'; 
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {
        
        $(document).on('click','.assign_order', function () {

            var OrderID = $(this).attr('data-order-id');


            $('#response_data').html('');

            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });


            $.ajax({
                type: "POST",
                url: base_url + 'cms/orders/get_drivers',
                data: {
                    'OrderID': OrderID
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {

                    $('#response_data').html(result.html);
                    $('#exampleModal').modal('show');

                },
                complete: function () {
                    $.unblockUI();
                }
            });

        });
    });
</script>
<script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('a796cb54d7c4b4ae4893', {
        cluster: 'ap2',
        forceTLS: true
    });

    var channel = pusher.subscribe('Ecommerce_Order_Channel');
    channel.bind('Ecommerce_Order_Event', function (data) {
        console.log(JSON.stringify(data));
        playAudio();
        setTimeout(function () {
            window.location.reload();
        }, 2000);
    });

    function playAudio() {
        var audiotag = document.getElementById('audiotag');
        audiotag.play();
    }

    // dt-buttons
    $(document).ready(function () {
      /*  $(".dt-buttons").children('button').removeClass();
        $(".dt-buttons").children('button').addClass('btn btn-primary btn-sm');
        $(".dt-buttons").children('button').children('span').text('Show / Hide Columns');*/
    });


//  table Rows Event
$('.click4_OrderDtl').click(function(e){
    //e.preventDefault();
    var value=$(this).attr('id');
    $('.click4_OrderDtl').removeClass('open');
    $(this).addClass('open');
    $('.orderDtlRow').slideUp();
    $('.orderDtlRow#orderDtl_'+value).slideDown();
    //  orderDtl_
    // alert(value);
});
</script>