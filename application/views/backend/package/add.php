<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">

 
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div> 
                                </div>
                                 <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="PackagePrice">Price</label>
                                            <input type="text" name="PackagePrice" required class="form-control number-with-decimals" id="PackagePrice">
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">Description</label>
                                        <textarea class="form-control summernote" name="Description"
                                                  id="Description" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                            <a  data-toggle="collapse" href="#packagesList" role="button" aria-expanded="false" aria-controls="packagesList">Select Module </a>
                        </div>
                        <div class="collapse in" id="packagesList">
                            <div class="card card-body mb-4">
                                <div class="row">
                                    <?php $menu_items = getActiveModules(); 
                                             foreach ($menu_items as $key => $main_menu) { ?>
                                                <?php if($main_menu['ParentID'] == 0){ ?>
                                                 <div class="col-md-4 mb-3 py-2 position-relative haveBefore">
                                                    <label class="w-100 m-0 px-2 pb-0 text-danger" for="<?php echo $main_menu['ModuleID']; ?>"><input type="checkbox" id="<?php echo $main_menu['ModuleID']; ?>"  data-id = "<?php echo $main_menu['ModuleID']; ?>" class="w-auto mr-2 parent parent-<?php echo $main_menu['ModuleID']; ?>" name="ModuleID[]" value="<?php echo $main_menu['ModuleID']; ?>"><?php echo $main_menu['Title']; ?></label>
                                                    <?php 
                                                    foreach ($menu_items as $sub_menu) { 
                                                                 if ($sub_menu['ParentID'] == $main_menu['ModuleID']) {
                                                        ?>
                                                    <label class="w-100 m-0 px-4 pb-0" for="firstChkBox_<?php echo $sub_menu['ModuleID']; ?>"><input type="checkbox" name="ModuleID[]" value="<?php echo $sub_menu['ModuleID']; ?>" data-parent-id="<?php echo $main_menu['ModuleID']; ?>" id="firstChkBox_<?php echo $sub_menu['ModuleID']; ?>" class="w-auto mr-2 child child-<?php echo $main_menu['ModuleID']; ?>"><?php echo $sub_menu['Title']; ?></label>
                                                <?php }  } ?>
                                                </div>
                                                <?php } ?>
                                             
                                             <?php }

                                    ?>
                                   
                                    
                                </div>
                            </div>
                        </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script language="JavaScript">
   $(document).ready(function(){
        $('.parent').on('click',function(){
            var parent_id  = $(this).attr('data-id');

            if($(this).is(":checked")){
                $('.child-'+parent_id).prop('checked', true);

            }else{
                 $('.child-'+parent_id).prop('checked', false);

            }
        });

        $('.child').on('click',function(){
                
                var parent_id  = $(this).attr('data-parent-id');
                var $boxes = $('.child-'+parent_id+':checked');

                
                if($boxes.length > 0){

                    $('.parent-'+parent_id).prop('checked', true);

                }else{
                    $('.parent-'+parent_id).prop('checked', false);
                }

        });

   });
</script>
<style>
.collapse.in {
    display: block !important;
}
</style>