<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        if($key == 0){


        $common_fields3 .= '<div class="form-group">
                                        <a  data-toggle="collapse" href="#packagesList" role="button" aria-expanded="false" aria-controls="packagesList">Select Modules </a>
                                    </div>
                                    <div class="collapse in" id="packagesList">
                                        <div class="card card-body mb-4">
                                            <div class="row">';  
        



            $menu_items = getActiveModules(); 

            $select_module = explode(',',$result[0]->ModuleID);
            foreach ($menu_items as $main_menu) { 
               if($main_menu['ParentID'] == 0){
                                                 $common_fields3 .= '<div class="col-md-4 mb-3 py-2 position-relative haveBefore">
                                                    <label class="w-100 m-0 px-2 pb-0 text-danger" for="'.$main_menu['ModuleID'].'"><input type="checkbox" id="'.$main_menu['ModuleID'].'"  data-id = "'.$main_menu['ModuleID'].'" class="w-auto mr-2 parent parent-'.$main_menu['ModuleID'].'" name="ModuleID[]" value="'.$main_menu['ModuleID'].'" 
                                                     '.(in_array($main_menu['ModuleID'], $select_module) ? 'checked' : '').'>'.$main_menu['Title'].'</label>';
                                                    
                                                   
                                                    foreach ($menu_items as $sub_menu) { 
                                                                 if ($sub_menu['ParentID'] == $main_menu['ModuleID']) {
                                                        
                                                    $common_fields3 .= '<label class="w-100 m-0 px-4 pb-0" for="firstChkBox_'.$sub_menu['ModuleID'].'"><input type="checkbox" name="ModuleID[]" value="'.$sub_menu['ModuleID'].'" data-parent-id="'.$main_menu['ModuleID'].'" id="firstChkBox_'.$sub_menu['ModuleID'].'" class="w-auto mr-2 child child-'.$main_menu['ModuleID'].'" '.(in_array($sub_menu['ModuleID'], $select_module) ? 'checked' : '').'>'.$sub_menu['Title'].'</label>';

                                                 }  } 
                                                    
                                               $common_fields3 .= ' </div>';
                                               } 
                                             

                                             }




                                    
                                   
                                    
        $common_fields3 .= '</div></div></div>';




         
        $common_fields2 = '<div class="row"><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div></div>';

        $common_fields = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxPrice">Box Price</label>
                                        <input type="text" name="BoxPrice" required  class="form-control number-with-decimals" id="BoxPrice" value="' . $result[$key]->PackagePrice . '">
                                    </div>
                                </div>';
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').'</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                        ' . $common_fields . '
                                                       
                                                    </div>
                                                    <div class="row">
                                                     <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description' . $key . '">Description</label>
                                                                <textarea class="form-control summernote" name="Description"
                                                                          id="Description' . $key . '" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    '.$common_fields2.' 
                                                    '.$common_fields3.'



                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


        
        
        
        
        
    }
}


?> 
                                      
                                    

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script language="JavaScript">
   $(document).ready(function(){
        $('.parent').on('click',function(){
            var parent_id  = $(this).attr('data-id');

            if($(this).is(":checked")){
                $('.child-'+parent_id).prop('checked', true);

            }else{
                 $('.child-'+parent_id).prop('checked', false);

            }
        });

        $('.child').on('click',function(){
                
                var parent_id  = $(this).attr('data-parent-id');
                var $boxes = $('.child-'+parent_id+':checked');

                
                if($boxes.length > 0){

                    $('.parent-'+parent_id).prop('checked', true);

                }else{
                    $('.parent-'+parent_id).prop('checked', false);
                }

        });

   });
</script>
<style>
.collapse.in {
    display: block !important;
}
</style>