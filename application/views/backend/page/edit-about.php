<?php
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if ($result[0]->Image == '') {
    $image = base_url("assets/backend/img/no_img.png");
} else {
    $image = base_url($result[0]->Image);
}
if ($result[0]->BannerImage == '') {
    $banner_image = base_url("assets/backend/img/no_img.png");
} else {
    $banner_image = base_url($result[0]->BannerImage);
}
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields4 = '';
        $is_active_field = '';
        
        $is_sec_one_active = '';
        $is_sec_two_active = '';
        $is_sec_three_active = '';
        $is_sec_four_active = '';
        $is_sec_five_active = '';
        $is_sec_six_active = '';
        $is_sec_seven_active = '';
        $is_sec_eight_active = '';

        $image_field = '';
        $banner_image_field = '';
        if ($key == 0) {

            $is_sec_one_active = createCheckbox('SectionOneActive','Is Active',$result[0]->SectionOneActive);
            $is_sec_two_active = createCheckbox('SectionTwoActive','Is Active',$result[0]->SectionTwoActive);
            $is_sec_three_active = createCheckbox('SectionThreeActive','Is Active',$result[0]->SectionThreeActive);
            $is_sec_four_active = createCheckbox('SectionFourActive','Is Active',$result[0]->SectionFourActive);
            $is_sec_five_active = createCheckbox('SectionFiveActive','Is Active',$result[0]->SectionFiveActive);
            $is_sec_six_active = createCheckbox('SectionSixActive','Is Active',$result[0]->SectionSixActive);
            $is_sec_seven_active = createCheckbox('SectionSevenActive','Is Active',$result[0]->SectionSevenActive);
            $is_sec_eight_active = createCheckbox('SectionEightActive','Is Active',$result[0]->SectionEightActive);


             $common_fields4 = '<div class="row">';
            $page_images = get_images($result[$key]->PageID, 'page');
            // print_rm($product_images);
            if ($page_images) {
                foreach ($page_images as $page_image) {
                    $common_fields4 .= '<div class="col-md-3 col-sm-3 col-xs-3"><i class="fa fa-trash delete_image" data-image-id="' . $page_image->SiteImageID . '" aria-hidden="true"></i><a href="'.base_url($page_image->ImageName).'" target="_blank"><img src="' . base_url() . $page_image->ImageName . '" style="height:200px;width:200px;"></a></div>';
                }
            }

           
            $common_fields4 .= '<div class="col-md-12 col-sm-12 col-xs-12">

                                                <div class="form-group">

                                                    <label>Image</label>
                                                    <input type="file" name="Image[]" multiple="multiple">
                                                    <p>Upload images for slider</p>
                                                </div>
                                            </div>
                                        </div>';

            $is_active_field = '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="togglebutton">
                                                <label for="IsActive">
                                                 <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[0]->IsActive) && $result[0]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                                </label>
                                            </div>
                                    </div>
                                </div>';
            
                $image_field = '<div class="row">
                                <div class="col-md-12">
                                    
                                    <img src="' . $image . '" style="width:100px;height:100px;">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                    <label>Image</label>
                                         <input type="file" name="Image[]"  required accept="image/*">
                                    </div>
                                </div>
                            </div>';
            
            
                $banner_image_field = '<div class="row">
                                <div class="col-md-12">
                                    
                                    <img src="' . $banner_image . '" style="width:100px;height:100px;">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                    <label>Banner Image</label>
                                         <input type="file" name="BannerImage[]"  required accept="image/*">
                                    </div>
                                </div>
                            </div>';
            
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                        ' . $is_active_field . '
                                                    </div>
                                                   

                                                    <div class="row">
                                                    '.createInputField('HeadingEight',6,'Success Count Section','Success Count Section','readonly').'
                                                    '.$is_sec_eight_active.'
                                                    </div>


                                                        <div class="row">
                                                        '.createInputField('SubHeadingOne',4,'Sub Heading One',$result[$key]->SubHeadingOne).'
                                                        '.createInputField('CountOne',2,'Count',$result[$key]->CountOne).'
                                                        </div>
                                                        <div class="row">
                                                        '.createInputField('SubHeadingTwo',4,'Sub Heading Two',$result[$key]->SubHeadingTwo).'
                                                        '.createInputField('CountTwo',2,'Count',$result[$key]->CountTwo).'
                                                        </div>
                                                        <div class="row">
                                                        '.createInputField('SubHeadingThree',4,'Sub Heading Three',$result[$key]->SubHeadingThree).'
                                                        '.createInputField('CountThree',2,'Count',$result[$key]->CountThree).'
                                                        </div>
                                                        <div class="row">
                                                        '.createInputField('SubHeadingFour',4,'Sub Heading Four',$result[$key]->SubHeadingFour).'
                                                        '.createInputField('CountFour',2,'Count',$result[$key]->CountFour).'
                                                        </div>
                                                        <div class="row">
                                                        '.createInputField('SubHeadingFive',4,'Sub Heading Five',$result[$key]->SubHeadingFive).'
                                                        '.createInputField('CountFive',2,'Count',$result[$key]->CountFive).'
                                                        </div>


                                                    <div class="row">
                                                    '.createInputField('HeadingTwo',6,'Services Heading',$result[$key]->HeadingTwo).'
                                                    '.$is_sec_two_active.'
                                                    </div>


                                                    <div class="row">
                                                    '.createInputField('HeadingOne',6,'Main Heading',$result[$key]->HeadingOne).'
                                                    '.$is_sec_one_active.'
                                                    </div>
                                                    '.createDescriptionField('Description',12,'Main Description',$result[$key]->Description).'
                                                    
                                                    
                                                    
                                                    
                                                    <div class="row">
                                                    '.createInputField('HeadingThree',6,'Our Performance Heading',$result[$key]->HeadingThree).'
                                                    '.$is_sec_three_active.'
                                                    </div>
                                                    '.createDescriptionField('DescriptionThree',12,'Our Performance Description',$result[$key]->DescriptionThree).'
                                                    
                                                    <div class="row">
                                                    '.createInputField('HeadingFour',6,'Who we are',$result[$key]->HeadingFour).'
                                                    '.$is_sec_four_active.'
                                                    </div>
                                                    '.createDescriptionField('DescriptionFour',12,'Who we are Description',$result[$key]->DescriptionFour).'
                                                    
                                                    <div class="row">
                                                    '.createInputField('HeadingFive',6,'Our history',$result[$key]->HeadingFive).'
                                                    '.$is_sec_five_active.'
                                                    </div>
                                                    '.createDescriptionField('DescriptionFive',12,'Our history Description',$result[$key]->DescriptionFive).'
                                                    
                                                    <div class="row">
                                                    '.createInputField('HeadingSix',6,'Our mission',$result[$key]->HeadingSix).'
                                                    '.$is_sec_six_active.'
                                                    </div>
                                                    '.createDescriptionField('DescriptionSix',12,'Our mission Description',$result[$key]->DescriptionSix).'

                                                    <div class="row">
                                                    '.createInputField('HeadingSeven',6,'Our Team',$result[$key]->HeadingSeven).'
                                                    '.$is_sec_seven_active.'
                                                    </div>
                                                    '.createDescriptionField('DescriptionSeven',12,'Our Team Description',$result[$key]->DescriptionSeven).'

                                                    '.$common_fields4.'

                                                    ';


        

        $lang_data .= '<div class="row">
                                                            
                                                            
                                                        </div>
                           
                            
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/tinymce_custom.js"></script>
<script>
    
    $(document).ready(function () {
        $(".delete_image").on('click',function(){
            var id=$(this).attr('data-image-id');

            url = "cms/page/deleteImage2";
            reload = "<?php echo base_url();?>cms/page/edit/<?php echo $result[0]->PageID; ?>";
            deleteRecord(id,url,reload);

        });
    });
</script>