<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Cash Flow</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/pos/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label  class="control-label" for="type">Trasaction Type :</label>
                                        <select id="type" name="Type" class="form-control " required="">
                                            <option value="Credit">Credit</option>
                                            <option value="Debit">Debit</option>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>

                                    
                                </div>
                                <div class="col-md-6">
                                     <div class="form-group label-floating">
                                        <label class="control-label" for="TransactionUser">Trasaction By</label>
                                        <input type="text" name="TransactionUser" required  class="form-control" id="TransactionUser">
                                    </div>
                                </div>
                              
                                <div class="col-md-12">

                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">Description / Purpose</label>
                                        <textarea class="form-control" name="Description" id="Description" style="height: 100px;"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                     <div class="form-group">
                                       <label class="control-label" for="TotalAmount">Total Amount * :</label>
                                       <input type="number" class="form-control" name="TotalAmount" id="TotalAmount" required value="">
                                     </div>
                                    
                                </div>


                               
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Invoice</label>
                                                                <input type="file" name="image[]" multiple="multiple">
                                                            </div>
                                                        </div>
                            </div>
                           



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/pos">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
