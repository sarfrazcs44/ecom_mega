<?php 
$parent_category = '';
$child_category = '';
if($categories){
   foreach ($categories as $key => $category) {
      if($category->ParentID == 0){
         $parent_category .= '<option value="'.$category->CategoryID.'">'.$category->Title.'</option>';
      }else{
         $child_category .= '<option style="display:none;" class="child_category child_category'.$category->ParentID.'" value="'.$category->CategoryID.'">'.$category->Title.'</option>';
      }
     
   }
   } ?>
<div class="content-page" style="margin-top:10px;">
<!-- Start content -->
<div class="content">
   <div class="container-fluid">
      
       <!-- end row -->
       <div class="row fixedRightColumn">
         <div class="col-md-9">
            <div class="page-title-box mb-0">
               <h4 class="page-title">Point of Sales</h4>
               <ol class="breadcrumb p-0 m-0">
               </ol>
               <div class="clearfix"></div>
            </div>

            <div class="card-box">
               <div class="d-flex justify-content-between align-items-center">
                     <div class="flex-grow-1 pr-4">
                        <div class="row">
                           <div class="col-md-4">
                                 <div class="form-group">
                                    <label for="gender">Category</label>
                                    <div class="position-relative">
                                       <select name="category" class="form-control get_products" id="category">
                                          <option value="">Category</option>
                                       <?php echo $parent_category; ?>
                                       </select>
                                       <i class="fa fa-chevron-down position-absolute right-0 top-0 bottom-0 left-auto my-auto height-12px mr-3"></i>
                                    </div>
                                    
                                 </div>
                           </div>
                           <div class="col-md-4 px-0">
                                 <div class="form-group">
                                    <label for="gender">Sub Category</label>
                                    <div class="position-relative">
                                       <select name="subcategory" class="form-control get_products" id="sub_category">
                                          <option value="">Sub Category</option>
                                       <?php echo $child_category; ?>
                                       </select>
                                       <i class="fa fa-chevron-down position-absolute right-0 top-0 bottom-0 left-auto my-auto height-12px mr-3"></i>
                                    </div>
                                 </div>
                           </div>
                           <div class="col-md-4">
                                 <div class="form-group">
                                    <label for="gender">Search Products</label>   
                                    <input type="text" class="form-control " id="search" placeholder="Search Products">
                                 </div>
                           </div>
                        </div>
                     </div>
                     
               </div>
               <div class="prodsNewStyle d-flex align-items-start flex-wrap">
                  <div id="products_div" class="row noBefore d-flex align-items-stretch flex-wrap">
                  <?php if($products){ 
                           foreach($products as $product){ 
                              $product_image = get_images($product['ProductID'], 'product', false);
                               $image = base_url()."uploads/product_images/dummy-img.png";    
                               if($product_image)
                               {
                                    if(file_exists($product_image)){
                                                        
                                       $image = base_url().$product_image;
                                    }
                               }
                     ?>
                     <div class="col-md-2">
                        <div class="singleProduct position-relative text-center search_product">
                                        <div class="dropdown">
                                            <a class="px-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v text-secondary"></i>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a class="dropdown-item w-100" href="<?php echo base_url('cms/product/edit/'.$product['ProductID']);?>">View</a>
                                                <a class="dropdown-item w-100" href="javascript:void(0);" onclick="deleteRecord('<?php echo $product['ProductID'];?>','cms/product/action','')">Delete</a>
                                            </div>
                                        </div>
                                        <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="<?php echo $image;?>" alt="product" height="83" width="83" ></div>
                                        <p class="m-0 text-danger bigger text-truncate" title="<?php echo $product['CategoryTitle']; ?>"><?php echo $product['CategoryTitle']; ?></p>
                                        <p class="text-dark my-1"><?php echo $product['Title']; ?></p>
                                        <p class="m-0 text-success bigger"><?php echo $product['Price']; ?> SAR</p>
                                        <div class="text-center"><input type="button" class="add_to_cart btn btn-primary" data-product-id="<?php echo $product['ProductID']; ?>" value="Add to cart"></div>
                        </div>
                        </div>
                    
                        <?php }  } ?>

                  </div>

               </div>
               <div class="text-center py-2 mt-2">
                        <?php if($total_products > 40){ ?>
                                <a href="javascript:void(0);" class="text-secondary load_more" data-page-no="1">Load More</a>
                        <?php } ?>
               </div>
            </div>
         </div>
         <div class="col-md-3">
            <div class="page-title-box mb-0">
               <h4 class="page-title">Order</h4>
               <ol class="breadcrumb p-0 m-0">
               </ol>
               <div class="clearfix"></div>
            </div>
            <div class="card-box pb-0 <?php if(!$cart_products){ echo 'noProductInCart'; } ?>" id="cart_items">
               <div class="haveScroll slimscrollleft " style="max-height:520px;">
                  <ul class="m-0 p-0">
                     <?php 
                              $total = 0;
                              if($cart_products){
                                       foreach ($cart_products as $key => $value) { 
                                          $total += $value['Price'] * $value['Quantity'];
                                          $product_image = get_images($value['ProductID'],'product',false);
                                           $image = base_url()."uploads/product_images/dummy-img.png";    
                                           if($product_image)
                                           {
                                                if(file_exists($product_image)){
                                                                    
                                                   $image = base_url().$product_image;
                                                }
                                           }
                                           ?>
                     <li class="d-flex py-2">
                        <div class="imgBox mr-3"><img class="rounded" src="<?php echo $image; ?>" alt="product" height="45" width="45" ></div>
                        <div class="text">
                           <p class="m-0 smallFontSize lineHeight1-3">
                              <span class="text-danger d-block w-100 strong bold"><?php echo $value['Title']; ?></span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold"></span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">Price per unit <?php echo $value['Price']; ?> SAR</span>
                           </p>
                        </div>
                        <div class="dropdown  dropleft">
                           <a class="px-2" href="#" role="button" id="deleteCart_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-ellipsis-v text-secondary"></i>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="deleteCart_1" style="width:auto;">
                              <a class="dropdown-item w-100 delete_cart" href="javascript:void(0);" data-temp-order-id = "<?php echo $value['TempOrderID']; ?>">Delete</a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="d-flex justify-content-between align-items-center mt-2 mb-4 px-1 pb-2 plusMinusPrice">
                           <div class="form-group mb-0 mr-3">
                              <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected position-relative">



                                 <span class="input-group-btn input-group-prepend">
                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto left-0 largeFontSize bootstrap-touchspin-down btn-minus" data-temp-order-id="<?php echo $value['TempOrderID']; ?>" data-product-id="<?php echo $value['ProductID']; ?>" type="button"><i class="typcn typcn-minus"></i></button>
                                 </span>


                                 <input id="demo0" type="text"  name="quantity" value="<?php echo $value['Quantity']; ?>"  data-bts-min="0" data-bts-max="100" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-primary" data-bts-button-up-class="btn btn-primary" class="form-control text-center strong bold largeFontSize text-dark bg-light px-4 quantity<?php echo $value['ProductID']; ?>">
                                 <span class="input-group-btn input-group-append">



                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto right-0 largeFontSize bootstrap-touchspin-up btn-plus" data-temp-order-id="<?php echo $value['TempOrderID']; ?>" data-product-id="<?php echo $value['ProductID']; ?>" type="button"><i class="typcn typcn-plus"></i></button>
                                 </span>
                              </div>
                           </div>
                           <div class="shadow bg-white rounded-pill text-center text-success">
                              <p class="m-0 ">
                                    <span class="d-block largeFontSize w-100 strong bold"><?php echo $value['Price'] * $value['Quantity']; ?></span>
                                    <span class="d-block smallerFontSize  w-100 ">SAR</span>
                              </p>
                           </div>
                        </div>
                        
                     </li>
                     <?php } } ?>
                  </ul>
               </div>
               <div class="row show_footer" <?php if(!$cart_products){ echo "style='display:none;'";} ?>>
                  <div class="tbody card-box shadow w-100 m-0 px-3">
                     <div class="px-3">
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong><?php echo $total; ?> SAR</strong></p></div>
                        </div>
                        <div class="row text-center border-bottom border-success">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Tax (VAT 15%)</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong><?php echo $vat_setting->Vat; ?> SAR</strong></p></div>
                        </div>
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>Grand Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong><?php echo $vat_setting->Vat + $total; ?> SAR</strong></p></div>
                        </div>
                     </div>
                     <button type="button" class="btn btn-primary waves-effect w-md waves-light mb-0 mt-2 w-100 get_popup" >Check Out</button>
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      
   <!-- container -->
</div>
<!-- content -->

 <!-- Modal -->
<div class="modal" id="checkOutModal"    tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered modal-lg">
      <div class="mdWrapperEd w-100">
          <div class="d-flex justify-content-between align-items-center">
             <img src="<?php echo base_url(); ?>assets/images/msjwhite.png" width="82">
             <button type="button" class="text-white text-white bg-transparent border-0 fa-2x lineHeight1-1" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-content p-3 rounded">
               <div class="modal-body py-2">
                  <div class="row justify-content-center">
                  <div class="border-0 ">
                     <div class="panel-body card-box ">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="alert" id="validatio-msg" style="display: none;"></div>
                            </div>
                            <div class="col-md-6 mt-3 ">
                               <div id="popup"></div>
                            </div>
                           <div class="col-md-6 border-left mt-3">
                              <span class="text-danger smallFontSize d-block w-100"><b>Customer Details</b></span>
                              <div class="form-group">
                                 <div class="input-group position-relative">
                                    <input type="text"  name="txtCustMobNo" id="customer_mobile_no" class="form-control pr-2" placeholder="Search by Mobile">
                                    <span class="input-group-append position-absolute right-0 top-0 bottom-0  my-auto">
                                       
                                    </span>
                                 </div>
                              </div>
                              <div class="smallFontSize mt-3 text-black-50">
                                 <p>If Customer is not selected, the order will be placed for a guest customer</p>
                              </div>
                              <div class="text mt-3">
                                 <p class="m-0 smallFontSize py-2 lineHeight1-3 ">
                                    <span class="text-danger  d-block w-100"><b>Additional Notes</b></span>
                                 </p>
                                 <textarea class="form-control"  id="additional_note"></textarea>
                              </div>
                              <div class="text mt-3">
                                 <p class="m-0 smallFontSize py-2 lineHeight1-3 ">
                                    <button type="button" class="submit  btn btn-primary w-100 my-0" id="place_order" href="#">Place Order</button>
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
  </div>
</div> 
<script type="text/javascript">
   
   $(document).ready(function(){
      $("#search").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $(".search_product").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
      });
      
      $('#category').on('change',function(){
         $('.child_category').hide();
         var category = $( "#category option:selected" ).val();
         $('.child_category'+category).show();
      });
       
      $('.get_products').on('change',function(){
         var page = parseInt(0);
         var category_id = $( "#category option:selected" ).val();
         var sub_category_id = $( "#sub_category option:selected" ).val();
         $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/pos/fetch_products');?>',
            data: {'category_id' : category_id,'sub_category_id' : sub_category_id,'page' : page},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
              
             
                
                $('#products_div').html(result.html);
                if(result.html == ''){
                    $('.load_more').attr('data-page-no', 0);
                }else{
                    if(result.show_load_more){
                        $('.load_more').attr('data-page-no', page + 1);
                    }
                    
                }
                if(!result.show_load_more){
                    $('.load_more').hide();

                }
                
            }, complete: function () {
                
            }
        });
         
      });



      $(document).on('click','.add_to_cart',function(){
         $('.show_footer').show();
        
         var product_id = $(this).attr('data-product-id');
        
         $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/pos/add_to_cart');?>',
            data: {'ProductID' : product_id},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
              
             
                $('#cart_items').html(result.html);
                //$('#products_div').html(result.html);
                
            }, complete: function () {
                
            }
        });
         
      }); 


      $(document).on('click','.get_popup',function(){

        
         
        
         $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/pos/get_popup_detail');?>',
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
              
             
                $('#popup').html(result.pop_up_html);
                $('#checkOutModal').modal('show');
                //$('#products_div').html(result.html);
                
            }, complete: function () {
                
            }
        });

      });
      $(document).on('click','.btn-plus',function(){
         var product_id = $(this).attr('data-product-id');
         var temp_order_id = $(this).attr('data-temp-order-id');
         var quantity = $('.quantity'+product_id).val();
         quantity = parseInt(quantity) + 1;
         //$('.quantity'+product_id).val(quantity);
          $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/pos/update_cart');?>',
            data: {'product_quantity' : quantity,'temp_order_id':temp_order_id},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
              
             
                $('#cart_items').html(result.html);
               
                
            }, complete: function () {
                
            }
        });
      });
      $(document).on('click','.btn-minus',function(){
         var product_id = $(this).attr('data-product-id');
         var temp_order_id = $(this).attr('data-temp-order-id');
         var quantity = $('.quantity'+product_id).val();
         quantity = parseInt(quantity) - 1;
         if(quantity <= 0){
            quantity = 1;
         }
         //$('.quantity'+product_id).val(quantity);
          $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/pos/update_cart');?>',
            data: {'product_quantity' : quantity,'temp_order_id':temp_order_id},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
              
             
                $('#cart_items').html(result.html);
                
                
            }, complete: function () {
                
            }
        });
      });
      $(document).on('click','.delete_cart',function(){
         
         var temp_order_id = $(this).attr('data-temp-order-id');
        
         
          $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/pos/delete_cart_item');?>',
            data: {'temp_order_id':temp_order_id},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
              
              if(result.reload){
               window.location.reload();
              }else{
               $('#cart_items').html(result.html);
              }
             
                
               
                
            }, complete: function () {
                
            }
        });
      });


      $('.load_more').on('click',function(){
        var page = parseInt($(this).attr('data-page-no'));
         var category_id = $( "#category option:selected" ).val();
         var sub_category_id = $( "#sub_category option:selected" ).val();
         $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/pos/fetch_products');?>',
            data: {'category_id' : category_id,'sub_category_id' : sub_category_id,'page' : page},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
              
             
                
                $('#products_div').append(result.html);

                if(result.html == ''){
                    $('.load_more').attr('data-page-no', 0);
                }else{
                   if(result.show_load_more){
                        $('.load_more').attr('data-page-no', page + 1);
                    }
                }
                


                if(!result.show_load_more){
                    $('.load_more').hide();

                }
                
            }, complete: function () {
                
            }
        });
         
      });

      $('#place_order').on('click',function(){
         var mobile = $('#customer_mobile_no').val();
         var additional_note = $('#additional_note').val();
         if(mobile == ''){
            if ($('#validatio-msg').hasClass('alert-success')) {
                        $('#validatio-msg').removeClass('alert-success');
            }
            if (!$('#validatio-msg').hasClass('alert-danger')) {
                        $('#validatio-msg').addClass('alert-danger');
            }
            $("#validatio-msg").html('<p>Please enter customer mobile no.</p>');
            $('#validatio-msg').show();
            return false;
         }else{
            $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/pos/place_order');?>',
            data: {'mobile':mobile,'additional_note' : additional_note},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
               if (result.error != 'false') {
                    if ($('#validatio-msg').hasClass('alert-success')) {
                        $('#validatio-msg').removeClass('alert-success');
                    }
                    if (!$('#validatio-msg').hasClass('alert-danger')) {
                        $('#validatio-msg').addClass('alert-danger');
                    }
                    $("#validatio-msg").html(result.error);
                    $('#validatio-msg').show();
                } else {
                    if ($('#validatio-msg').hasClass('alert-danger')) {
                        $('#validatio-msg').removeClass('alert-danger');
                    }
                    if (!$('#validatio-msg').hasClass('alert-success')) {
                        $('#validatio-msg').addClass('alert-success');
                    }
                    $("#validatio-msg").html(result.success);
                    $('#validatio-msg').show();
                    if (result.reset)
                        $form[0].reset();
                    if (result.reload)
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    if (result.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + result.url;
                        }, 1000);
                    }
                    // document.getElementById("show_success_messge").click();
                }
              
              
             
                
               
                
            }, complete: function () {
                
            }
        });
         }
      })
   });
</script>
<style>
   .border-right {
      border-width:0 1px 0 0;
      border-style:none solid none none;
   }
   .border-gray {
      border-color:#444;
   }
 
   .bill-invoice h4.page-title {
    margin: 0 0 10px;
}
   
   .productImg {
   display: block;
   width: 150px;
   margin: auto;
   }
   .ord-no {
   float: right;
   font-size: 16px;
   margin-top: 15px;
   }
   table.tbl-cart {
   /*border: 1px solid;*/
   }
   table.tbl-cart thead tr th {
   padding: 12px;
   font-size: 20px;
   }
   table.tbl-cart tbody tr td {
      padding: 6px 8px;
    font-size: 15px;
    font-weight: 500;
    vertical-align: middle;
   }
   table.tbl-cart tfoot tr:first-child {
      border-top:20px solid transparent;
   }
   table.tbl-cart tfoot tr th {
      padding: 3px 8px;
   }
   .cls-icon, .qty-btn {
      width: 30px;
    height: 30px;
    text-align: center;
    border: none;
    line-height: 1;
    border-radius: 20px;
    background: transparent;
    margin: auto;
    font-weight: bold;
    font-size: 20px;
   }
   .qty-pdt {
   background: transparent;
   border: none;
   text-align: center;
   font-size: 15px;
   width:70px;
   }
   .pTitle {
    padding-top: 10px;
    font-weight: 500;
    line-height: 18px;
    margin: 0 0 15px;
    min-height: 100px;
}

   .hedTitle {
   margin-top:15px;
   }
   .card-boxe {
   border: 1px solid;
   padding: 20px;
   border-radius: 15px;
   }
   body {
   color: gray;
   }
   a.dropdown-item {
   display: block;
   padding: 10px;
   width: 300px;
   color: gray;
   }
   .hedTitle {
   color: gray;
   }
   .qty-btns {
   display: flex;
   }
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
   -webkit-appearance: none;
   }
   .prdt-r {
   margin: 25px 0;
   }
   table.tbl-cart tbody tr:last-child {
   border: 0;
   }
   select {
   /* for Firefox */
   -moz-appearance: none;
   /* for Chrome */
   -webkit-appearance: none;
   }
   /* For IE10 */
   select::-ms-expand {
   display: none;
   }
   .add-to-cart {
   display: block;
   margin-right: 20px;
   margin-left: auto;
   }
   button.cart-Btn {
   background: transparent;
   width: 100px;
   padding: 10px;
   border-radius: 30px;
   border: none;
   font-weight: 600;
   color: #fff;
   }
   .fa-shopping-cart:hover::before {
   color: #f5707a;
   }
   .fa-shopping-cart:before {
   content: "\f07a";
   font-size: 24px;
   color: #4bd396;
   }
   button.submit.btn.btn-primary {
    display: block;
    width: 100%;
    margin: 20px 0;
}

.bill-invoice {
    position: sticky;
    top: 0;
}
::-webkit-scrollbar {
    width: 0px;
    background: transparent; /* make scrollbar transparent */
}
   @media (max-width:1280px) {
   .pdct {
   width: 100%;
   }
   .bill-invoice {
   width: 100%;
   float:none;
   }
   }
   @media (max-width: 425px) {
   table.tbl-cart thead tr th {
   padding: 7px;
   font-size: 18px;
   }
   table.tbl-cart tbody tr td {
   padding: 5px;
   font-size: 15px;
   font-weight: 500;
   }
   .cls-icon, .qty-btn {
   width: 20px;
   height: 20px;
   text-align: center;
   border: 1px solid;
   line-height: 0.8;
   border-radius: 20px;
   background: transparent;
   margin: auto;
   }
   .qty-pdt {
   width:30px;
   }
   .cls-icon {
   line-height: 1.1;
   }
   button.btn-plus {
   padding-left: 5px;
   }
   .card-boxe {
   padding: 10px;
   }
   button.cart-Btn {
   display: block;
   margin: 10px 0px 0px auto;
   }
   }
   .products-ctalog {
    padding: 15px;
    box-shadow: 4px 5px 10px -2px rgba(0,0,0,0.5);
    border-radius: 6px;
    margin: 0 auto 15px;
}
</style>