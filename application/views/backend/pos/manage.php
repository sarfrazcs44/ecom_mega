<div class="content">
    <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Search</h4>
                        
                        <div class="material-datatables">
                            <form action="" method="post" style="padding-bottom: 105px;">
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group  is-focused">
                                            <label class="control-label" for="DepositDate ">From</label>
                                            <input type="text" name="From" class="form-control custom_datepicker" value="<?php echo (isset($post_data['From']) ? $post_data['From'] : '' ); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group is-focused">
                                            <label class="control-label" for="DepositDate ">To</label>
                                            <input type="text" name="To" class="form-control custom_datepicker" value="<?php echo (isset($post_data['To']) ? $post_data['To'] : '' ); ?>">
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                               Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                   
                    <div class="card-content">
                        <h4 class="card-title">Cashflow</h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/pos/add');?>">
                                <button type="button"
                                        class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Entry</button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                        <tr>
                                            <th>Transaction</th>
                                            <th>Done By</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                            <th>Invoice</th>
                                            <th>Grand Total</th>
                                            
                                            
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    var columns = <?php echo $columns; ?>;
    var CallUrl = '<?php echo $ControllerName;?>'; 
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>

