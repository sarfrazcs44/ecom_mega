<form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/saveCrossSellingProductAvailability" method="post" onsubmit="return false;" class="" id="availability_form" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="ProductID" value="<?php echo $ProductID; ?>">

<?php if($products){ 
    foreach($products as $key => $product){
        //print_rm($products);
        $checkbox = '';
       
        

        if(in_array($product['ProductID'],$crossSellingProduct)){
            $checkbox = 'checked';
        }

     ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <input name="CrossSellingProductID[]" value="<?php echo $product['ProductID'];?>" type="checkbox" id="Product<?php echo $product['ProductID'];?>" <?php echo $checkbox; ?>/>
                                        <label>
                                            <a href="<?php echo base_url(get_images($product['ProductID'], 'product', false)); ?>"
                                                   target="_blank">
                                                    <img src="<?php echo base_url(get_images($product['ProductID'], 'product', false)); ?>"
                                                         style="height:70px;width:70px;">
                                                </a>
                                        </label>
                                        <label for="Product<?php echo $product['ProductID'];?>">
                                            <?php echo $product['Title']; ?>
                                        </label>
                                        <label>
                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/edit/' . $product['ProductID']); ?>"
                                                                   class="btn btn-simple btn-warning btn-icon edit"><i
                                                                            class="material-icons" title="Edit">edit</i>
                                                                </a>
                                        </label>

                                        </div>


                                </div>

                                
                                

                            </div>
                        <?php } } ?>
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit" >
                                    <?php echo lang('submit');?>
                                </button>
                                
                                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                               
                            </div>


<script type="text/javascript">

$(document).ready(function () {
    $("#availability_form").submit(function (e) {
        e.preventDefault();


        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                }
                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
    });


    if ($('.jFiler-input-text').length > 0) {

        $('.jFiler-input-text').hide();
    }


});

</script>