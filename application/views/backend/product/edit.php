<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
.form-group.label-floating label.control-label, .form-group.label-placeholder label.control-label {
    color: #bd9371;
    position: static;
    float: left;
    min-width: 160px;
}
.form-group.label-floating .form-control,
.select2-container {
    float: right;
    width: calc(100% - 160px) !important;
    border-color: #D2D2D2;
    border-style: solid;
    border-width: 1px 1px 1px;
    background-position: center bottom, center calc(100% + 1px);
    padding-left: 10px;
    padding-right: 10px;
}
.select2-container--default.select2-container--focus .select2-selection--multiple {
    border: none !important;
    outline: 0;
}
.select2-container--default .select2-selection--multiple {
    background: transparent;
    border: 0;
    border-radius: 0;
}
.select2-container--default .select2-selection--multiple ul {
    padding:0;
}
.add_whats_inside + .row .form-group.label-floating .form-control {
    width: 100%;
    margin-top: 5px;
}
.form-group.label-floating .btn-group.bootstrap-select,
.form-group.label-floating .note-editor.note-frame {
    float: right;
    width: calc(100% - 165px);
}
.note-editor.note-frame {
    border: 1px solid #D2D2D2 !important;
}
.chocoCMSAccordion .card {
    margin: 15px 0;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.2);
    /* overflow: hidden; */
}
.chocoCMSAccordion .card .card-header {
    padding: 0 !important;
}
.chocoCMSAccordion .card .card-header h2 {
    margin: 0;
}
.chocoCMSAccordion .card .card-header button.btn.btn-link {
    color: #50456d;
    background: transparent;
    border: 0;
    border-radius: 0;
    margin: 0;
    box-shadow: none;
    padding: 14px 20px;
    line-height: 1.5;
    min-height: inherit;
    font-size: 22px;
    font-weight: normal;
    display: block;
    width: 100%;
    text-align: inherit;
    text-decoration: none;
    transition-duration: 0.5s;
}
.chocoCMSAccordion .card .card-header button.btn.btn-link:hover {
    background-color: #f7f4eb;
}
.chocoCMSAccordion .card .card-body {
    padding: 50px 35px;
    /* border-top: 2px solid #513c7e; */
}
.chocoCMSAccordion .card .card-header i.fa {
    display: inline-block;
    vertical-align: top;
    font-size: 25px;
    line-height: 12px;
    margin-top: 8px;
    margin-right: 4px;
    transition-duration: 0.5s;
}
.chocoCMSAccordion .card .card-header button.btn[aria-expanded="true"] i.fa {
    transform: rotateX(180deg);
}
</style>
<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields3_a = '';
        $common_fields4 = '';
        $common_fields5 = '';
        $common_fields6 = '';
        $common_fields7 = '';
        $category_dropdown = '';
        $sub_category_dropdown = '';
        $nutrition_dropdown = '';
        $boxes_dropdown = '';
        $inside_product = '';
        $tags_dropdown = '';
        $common_tags = '';
        $category_array = array();
        $brand_dropdown = '';

        if ($key == 0) {
            foreach ($brands as $brand){
                $brand_dropdown .= '<option value="'.$brand->BrandID.'" '.((isset($result[$key]->BrandID) && $result[$key]->BrandID == $brand->BrandID) ? 'selected' : '').'>'.$brand->Title.' </option>';
            }
            foreach ($categories as $category) {
                $category_dropdown .= '<option ' . (in_array($category->CategoryID,explode(',',$result[$key]->CategoryID )) ? 'selected="selected"' : '') . ' value="' . $category->CategoryID . '">' . $category->Title . '</option>';
            }

            foreach($tags as $tag){
                $tags_dropdown .= '<option ' . (in_array($tag->TagID,explode(',',$result[$key]->TagIDs )) ? 'selected="selected"' : '') . ' value="' . $tag->TagID . '">' . $tag->Title . '</option>';
            }

            foreach ($subcategories as $k => $SubCategory) {
                if(!in_array($SubCategory->ParentID,$category_array)){
                    if($k != 0){
                        
                        $sub_category_dropdown .= '</optgroup>';
                       
                    }
                     $title = categoryName($SubCategory->ParentID,'EN');
                     $sub_category_dropdown .= '<optgroup label="'.$title.'">';
                }
                $sub_category_dropdown .= '<option ' . (in_array($SubCategory->CategoryID,explode(',',$result[$key]->SubCategoryID)) ? 'selected="selected"' : '') . ' value="' . $SubCategory->CategoryID . '">' . $SubCategory->Title . '</option>';
                $category_array[] = $SubCategory->ParentID;
            }

            foreach ($nutritions as $nutrition) {
                $nutrition_dropdown .= '<option ' . ((!empty($product_nutritions_result) && in_array($nutrition->NutritionID, $product_nutritions_result)) ? 'selected' : '') . ' value="' . $nutrition->NutritionID . '" class="nutrition_' . $nutrition->NutritionID . '" data-quantity="' . (isset($product_nutritions_quanity[$nutrition->NutritionID]) ? $product_nutritions_quanity[$nutrition->NutritionID] : 0) . '">' . $nutrition->Title . '</option>';
            }

            foreach ($boxes as $box) {
                $boxes_dropdown .= '<option ' . (in_array($box->BoxID, explode(',', $result[$key]->BoxIDs)) ? 'selected' : '') . ' value="' . $box->BoxID . '">' . $box->Title . '</option>';
            }


            $common_fields4 = '<div class="row">';
            $product_images = get_images($result[$key]->ProductID, 'product');
            // print_rm($product_images);
            if ($product_images) {
                foreach ($product_images as $product_image) {
                    $common_fields4 .= '<div class="col-md-3 col-sm-3 col-xs-3"><i class="fa fa-trash delete_image" data-image-id="' . $product_image->SiteImageID . '" aria-hidden="true"></i><img src="' . base_url() . $product_image->ImageName . '" style="height:200px;width:200px;"></div>';
                }
            }

            $common_tags .= '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Tags *</label>
                                        <select name="TagIDs[]" class="selectpicker select2" data-style="select-with-transition" id="TagID" required multiple>
                                            <option value="">'.lang('tags').'</option>
                                            '.$tags_dropdown.'
                                        </select>
                                    </div>
                                </div>';
            $common_fields4 .= '<div class="col-md-12 col-sm-12 col-xs-12">

                                                <div class="form-group">

                                                    <label>Image</label>
                                                    <input type="file" name="Image[]" multiple="multiple">
                                                    <p>' . lang('cannot_upload_more_then') . '</p>
                                                </div>
                                            </div>
                                        </div>';


            $common_fields = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="SKU">SKU *</label>
                                        <input type="text" name="SKU" required  class="form-control" id="SKU" value="' . ((isset($result[$key]->SKU)) ? $result[$key]->SKU : '') . '">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="Price' . $key . '">' . lang('price') . ' *</label>
                                     <input type="text" name="Price" parsley-trigger="change" required  class="form-control number-with-decimals" id="Price" value="' . ((isset($result[$key]->Price)) ? $result[$key]->Price : '') . '">
                                                               
                                </div>
                          </div>';
            $common_fields7 = '<div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label" for="Type">Type *</label>
                                                                <select name="Type" id="Type" class="form-control">
                                                                  <option value="Product" ' . (($result[$key]->Type == "Product") ? 'selected' : '') . '/>Product</option>
                                                                  <option value="Product" ' . (($result[$key]->Type == "AddOn") ? 'selected' : '') . '/>Add-On</option>
                                                                </select>
                                                        </div>
                                                    </div>';



            if (isset($result[$key]->IsCustomizedProduct) && $result[$key]->IsCustomizedProduct == 1) {
                $IsCustomizedProductStyle = "display:block;";
            } else {
                $IsCustomizedProductStyle = "display:none;";
            }
            if (isset($result[$key]->IsCorporateProduct) && $result[$key]->IsCorporateProduct == 1) {
                $IsCorporateProductStyle = "display:block;";
            } else {
                $IsCorporateProductStyle = "display:none;";
            }
            $common_fields2 = '<div class="row"><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsFeatured">
                                                <input name="IsFeatured" value="1" type="checkbox" id="IsFeatured" ' . ((isset($result[$key]->IsFeatured) && $result[$key]->IsFeatured == 1) ? 'checked' : '') . '/> ' . lang('is_featured') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="OutOfStock">
                                                <input name="OutOfStock" value="1" type="checkbox" id="OutOfStock" ' . ((isset($result[$key]->OutOfStock) && $result[$key]->OutOfStock == 1) ? 'checked' : '') . '/> ' . lang('out_of_stock') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsCorporateProduct">
                                                <input name="IsCorporateProduct" value="1" type="checkbox" id="IsCorporateProduct" ' . ((isset($result[$key]->IsCorporateProduct) && $result[$key]->IsCorporateProduct == 1) ? 'checked' : '') . '/> ' . lang('is_corporate') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsCustomizedProduct">
                                                <input name="IsCustomizedProduct" value="1" type="checkbox" id="IsCustomizedProduct" ' . ((isset($result[$key]->IsCustomizedProduct) && $result[$key]->IsCustomizedProduct == 1) ? 'checked' : '') . '/> ' . lang('is_customize') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="row" id="ForIsCorporateProduct" style="' . $IsCorporateProductStyle . '">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CorporateMinQuantity">Minimum Quantity</label>
                                        <input type="text" name="CorporateMinQuantity" class="form-control" id="CorporateMinQuantity" value="' . ((isset($result[$key]->CorporateMinQuantity)) ? $result[$key]->CorporateMinQuantity : '') . '">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CorporatePrice">Corporate Price</label>
                                        <input type="text" name="CorporatePrice" class="form-control number-with-decimals" id="CorporatePrice" value="' . ((isset($result[$key]->CorporatePrice)) ? $result[$key]->CorporatePrice : '') . '">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                                <div class="row" id="ForIsCustomizedProduct" style="' . $IsCustomizedProductStyle . '">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxIDs">Boxes</label>
                                        <select name="BoxIDs[]" class="form-control" id="BoxIDs" multiple required>
                                            ' . $boxes_dropdown . '
                                        </select>
                                    </div>
                                </div>
                            </div>';

            $common_fields3 = '<div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="PriceType" class="form-control" id="PriceType" required>
                                            <option value="">' . lang('price_type') . '</option>
                                            <option value="item" ' . ($result[$key]->PriceType == "item" ? "selected" : "") . '>' . lang('price_type_item') . '</option>
                                            <option value="kg" ' . ($result[$key]->PriceType == "kg" ? "selected" : "") . '>' . lang('price_type_kg') . '</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6 minimum_kg_quantity">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="MinimumPerKgQuantity">Minimum Purchasable</label>
                                                <input type="number" name="MinimumOrderQuantity" required  class="form-control number-with-decimals" id="MinimumOrderQuantity" value="' . ((isset($result[$key]->MinimumOrderQuantity)) ? $result[$key]->MinimumOrderQuantity : '') . '">
                                            </div>
                                        </div>
                                        <div class="col-md-2"><label class="control-label is-empty" style="margin-top:36px!important;" id="min_order_label">' . ($result[$key]->PriceType == "item" ? "Pcs" : "Grams") . '</label></div>
                                        </div>
                                    </div>
                                </div>';

            $common_fields3_a = '<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Brand *</label>
                                        <select name="BrandID" class="selectpicker" data-style="select-with-transition" id="BrandID" required>
                                            <option value="">Choose Brand</option>
                                            ' . $brand_dropdown . '
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Main Category *</label>
                                        <select name="CategoryID[]" class="selectpicker" data-style="select-with-transition" id="CategoryID" required>
                                            <option value="">' . lang('choose_category') . '</option>
                                            ' . $category_dropdown . '
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Sub Category *</label>
                                        <select name="SubCategoryID[]" class="selectpicker" data-style="select-with-transition" id="SubCategoryID" required>
                                            <option value="">' . lang('choose_sub_category') . '</option>
                                            ' . $sub_category_dropdown . '
                                        </select>
                                    </div>
                                </div>
                            </div>';

            $common_fields6 = '<div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Serving Size</label>
                                                        <input type="text" name="ServingSize" required  class="form-control" id="ServingSize" value="'.$result[$key]->ServingSize.'">
                                                    </div>
                                                    
                                                </div>
                                            </div><div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . lang('select_nutritions') . '</label>
                                        <select  class="form-control" id="Sl_Chocolate" multiple required>
                                            
                                            ' . $nutrition_dropdown . '
                                        </select>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row eddie" id="multiple_fields">
                            ' . $product_nutrition_field . '
                            </div>';

            $inside_product = '<div class="row">
                                 <div class="col-md-6" id="add_whats_inside_fields" style="display:' . ($result[$key]->PriceType == "item" ? "block" : "none") . '">
                                     <a href="#" class="add_whats_inside">
                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                            Add What\'s Inside Field
                                        </button>
                                    </a>
                                     '.$ProductHtml.'
                                </div>
                                <div class="clearfix"></div>

                            </div>';               
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                    <div class="accordion chocoCMSAccordion" id="addProdAccordion">
                                                        <div class="card">
                                                            <div class="card-header" id="headingOne'.$key.'">
                                                                <h2 class="mb-0">
                                                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne'.$key.'" aria-expanded="false" aria-controls="collapseOne"><span><i class="fa fa-angle-down" aria-hidden="true"></i></span>Product Information</button>
                                                                </h2>
                                                            </div>
                                                            <div id="collapseOne'.$key.'" class="collapse" aria-labelledby="headingOne" data-parent="#addProdAccordion" aria-expanded="true" style="">
                                                                <div class="card-body">
                                                                <div class="row">

                                                                ' . $common_fields7 . '

                                                                <div class="clearfix"></div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label" for="Title' . $key . '">' . lang('title') . ' *</label>
                                                                        <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                ' . $common_fields . '
                                                                 
                                                            </div>
                                                            ' . $common_fields3 . '
                                                            
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card">
                                                            <div class="card-header" id="headingTwo'.$key.'">
                                                                <h2 class="mb-0">
                                                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo'.$key.'" aria-expanded="false" aria-controls="collapseTwo"><span><i class="fa fa-angle-down" aria-hidden="true"></i></span>Category</button>
                                                                </h2>
                                                            </div>
                                                            <div id="collapseTwo'.$key.'" class="collapse" aria-labelledby="headingTwo" data-parent="#addProdAccordion" aria-expanded="false" style="height: 0px;">
                                                                <div class="card-body">
                                                                    ' . $common_fields3_a . '
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card">
                                                            <div class="card-header" id="headingThree'.$key.'">
                                                                <h2 class="mb-0">
                                                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree'.$key.'" aria-expanded="false" aria-controls="collapseThree"><span><i class="fa fa-angle-down" aria-hidden="true"></i></span>Product Description</button>
                                                                </h2>
                                                            </div>
                                                            <div id="collapseThree'.$key.'" class="collapse" aria-labelledby="headingThree" data-parent="#addProdAccordion">
                                                                <div class="card-body">
                                                                ' . $common_fields6 . '
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description">' . lang('description') . '</label>
                                                                <textarea class="form-control" name="Description" id="Description" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Ingredients">' . lang('ingredients') . '</label>
                                                                <textarea class="form-control" name="Ingredients" id="Ingredients" style="height: 100px;">' . ((isset($result[$key]->Ingredients)) ? $result[$key]->Ingredients : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Specifications">Specifications</label>
                                                                <textarea class="form-control summernote" name="Specifications" id="Specifications" style="height: 100px;">' . ((isset($result[$key]->Specifications)) ? $result[$key]->Specifications : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Packaging">Packaging and Delivery</label>
                                                                <textarea class="form-control" name="Packaging" id="Packaging" style="height: 100px;">' . ((isset($result[$key]->Packaging)) ? $result[$key]->Packaging : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="SuggestedUse">Suggested Use</label>
                                                                <textarea class="form-control" name="SuggestedUse" id="SuggestedUse" style="height: 100px;">' . ((isset($result[$key]->SuggestedUse)) ? $result[$key]->SuggestedUse : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Warnings">Warnings</label>
                                                                <textarea class="form-control" name="Warnings" id="Warnings" style="height: 100px;">' . ((isset($result[$key]->Warnings)) ? $result[$key]->Warnings : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    '.$inside_product.'
                                                    <hr>
                                                    <div class="row">

                                                        '.$common_tags.'
                                                        <div class="clearfix"></div>
                                                         <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Keywords">' . lang('keywords') . '</label>
                                                                <input type="text" name="Keywords" required  class="form-control" id="Keywords"  value="' . ((isset($result[$key]->Keywords)) ? $result[$key]->Keywords : '') . '">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="MetaTages">' . lang('meta_tages') . '</label>
                                                                    <input type="text" name="MetaTags" required  class="form-control" id="MetaTages" value="' . ((isset($result[$key]->MetaTags)) ? $result[$key]->MetaTags : '') . '">
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="MetaKeywords">' . lang('meta_keywords') . '</label>
                                                                    <input type="text" name="MetaKeywords" required  class="form-control" id="MetaKeywords" value="' . ((isset($result[$key]->MetaKeywords)) ? $result[$key]->MetaKeywords : '') . '">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="MetaDescription">' . lang('meta_description') . '</label>
                                                                    <textarea class="form-control" name="MetaDescription" id="MetaDescription" style="height: 100px;">' . ((isset($result[$key]->MetaDescription)) ? $result[$key]->MetaDescription : '') . '</textarea>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    ' . $common_fields2 . '
                                                    ' . $common_fields4 . '
                                                   

                                                    
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                   
                                                    
                                                    

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                        <button type="button" class="btn btn-primary store_availability" data-product-id= "<?php echo $result[0]->$TableKey;?>" data-product-title="<?php echo $result[0]->Title; ?>"  id="">Add Quantity</button>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Product Availability</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="model_body">
        
      </div>
      
  </div>
</div>
<script>
    $('.select2').select2();
</script>
<script type="text/javascript">
    var inside_count = '<?php echo $InsideCount; ?>';
    $(document).ready(function () {

        $('#CategoryID').on('change', function () {

            var CategoryID = $(this).val();

            if (CategoryID == '') {
                $('#SubCategoryID').html('<option value=""><?php echo lang("choose_sub_category");?></option>');
            } else {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/category/getSubCategory',
                    data: {
                        'CategoryID': CategoryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function (result) {

                        $('#SubCategoryID').html(result.html);
                        $('#SubCategoryID').selectpicker('refresh');

                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }

        });

        $(".delete_image").on('click', function () {
            var id = $(this).attr('data-image-id');

            url = "cms/product/deleteImage2";
            reload = "<?php echo base_url();?>cms/product/edit/<?php echo $result[0]->ProductID; ?>";
            deleteRecord(id, url, reload);

        });

        $(".delete_inside").on('click', function () {
            var id = $(this).attr('data-inside-id');

            url = "cms/product/deleteProductInside";
            reload = "<?php echo base_url();?>cms/product/edit/<?php echo $result[0]->ProductID; ?>";
            deleteRecord(id, url, reload);

        });


        $('#PriceType').on('change',function(){

        var value = $(this).val();
        if(value == 'kg'){
            //$('.minimum_kg_quantity').show();
            $('#min_order_label').html('Grams');
             $('#add_whats_inside_fields').hide();
        }else{
            //$('.minimum_kg_quantity').hide();
            $('#min_order_label').html('Pcs');
             $('#add_whats_inside_fields').show();
        }

       });



        


    });


</script>