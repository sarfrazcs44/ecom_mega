<div class="content">
    <div class="container-fluid">
        <?php if($this->session->userdata['admin']['RoleID'] == 1){ ?>
        <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Import Products List</h4>
                            <div class="material-datatables" style="margin-left: 450px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="<?php echo base_url(); ?>cms/product/importCsv" method="post"
                                                      onsubmit="return false;" class="form_data"
                                                      enctype="multipart/form-data"
                                                      data-parsley-validate novalidate>
                                        
                                            
                                                
                                                       
                                            <div class="row">
                                                        <div class="col-md-12">

                                                    <label>Upload Products CSV</label>
                                                    <input type="file" name="Csv" id="Csv">
                                                </div>
                                                </div>

                                            <div class="row">
                                                <div class="col-md-12">

                                                    <label>Upload Products Images as Zip File</label>
                                                    <input type="file" name="file" id="file">
                                                </div>
                                                </div>


                                            
                                            <div class="">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                    Import
                                                </button>
                                            </div>
                                        
                                    </form>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="<?php echo base_url('assets/product_sample.csv');?>" target="_blank">Download Sample Format</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
        </div>
    <?php } ?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName . 's'); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/' . $ControllerName . '/add'); ?>">
                                <button type="button"
                                        class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_' . $ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                   
                                    <th><?php echo lang('title'); ?></th>
                                    <th>SKU</th>
                                    <th>Purchases</th>
                                    <th>Active</th>
                                    <th>Featured</th>
                                    <th>Customized</th>
                                    <th>Out of stock</th>
                                    <th>Corporate</th>
                                    <th>Rating</th>
                                    <th>Image</th>


                                    <?php if (checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Product Availability</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <form action="" method="post" onsubmit="return false;" class="" id="availability_form" enctype="multipart/form-data" data-parsley-validate novalidate>
        <input type="hidden" name="ProductID" id="ProductID" value="">
      <div class="modal-body" id="model_body">
      </div>
      <div id="attribute_body"></div>
      <div class="text-center py-2 mt-2">
                            
                                <a href="javascript:void(0);" class="text-secondary load_more" data-page-no="0" data-product-id= "" data-product-title="" data-action="">Load More</a>
                           
                        </div>
        <div class="form-group text-right m-b-0">
            <button class="btn btn-primary waves-effect waves-light"  type="submit" >Submit</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
        </div>
      </form>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">
    var columns = <?php echo $columns; ?>;
    ParentID = '<?php echo $url_status; ?>';
    var CallUrl = '<?php echo $ControllerName;?>';
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script type="text/javascript">
      
</script>
<script type="text/javascript">

$(document).ready(function () {
    $("#availability_form").submit(function (e) {
        e.preventDefault();


        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                }
                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
    });


    if ($('.jFiler-input-text').length > 0) {

        $('.jFiler-input-text').hide();
    }


});


</script>