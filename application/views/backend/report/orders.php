<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"> Orders</h4>
                        <form action="" method="post" style="padding-bottom: 105px;">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label" for="DepositDate ">From</label>
                                            <input type="text" name="from" class="form-control datepicker" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label" for="DepositDate ">To</label>
                                            <input type="text" name="to" class="form-control datepicker" value="">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Show By</label>
                                            <select name="ShowBy" class="form-control">
                                                
                                                <option value="Year" <?php echo ($Interval == 'Year' ? 'selected' : ''); ?>>Year</option>
                                                <option value="Month" <?php echo ($Interval == 'Month' ? 'selected' : ''); ?>>Month</option>
                                                <option value="Day" <?php echo ($Interval == 'Day' ? 'selected' : ''); ?>>Day</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                               Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <?php if($from != '' && $to != ''){ ?>
                            <p><b> From : </b> <?php echo $from; ?><b> To : </b> <?php echo $to; ?></p>
                        <?php } ?>
                        <div class="material-datatables">
                            <table id="" class="datatables_csv table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    

                                    <th>Interval</th>
                                    <th>Orders</th>
                                    
                                    <th>Sales Total</th>
                                    <th>Sales TAX</th>
                                    <th>Sales Shipping</th>
                                    <th>Sales Discount</th>
                                    <th>Cancelled Orders</th>
                                    
                                    


                                    

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    $date_y = array();
                                    $date_m = array();
                                    $date_d = array();
                                    $TotalOrders = 0;
                                    $TotalSum = 0;
                                    foreach($results as $key => $value){
                                        $TotalOrders = $TotalOrders + $value->TotalOrders;
                                        $TotalSum = $TotalSum + $value->TotalSum;
                                        $date_f = '';
                                        if($Interval == 'Year'){
                                            if(!in_array($value->Year,$date_y)){
                                                $date_f = $value->Year;
                                                $date_y[] = $value->Year;
                                            }
                                        }
                                        if($Interval == 'Month'){
                                            if(!in_array($value->Month,$date_m) || !in_array($value->Year, $date_y)){
                                                 $date_f = $value->Month.'/'.$value->Year;
                                                 $date_m[] = $value->Month;
                                                 $date_y[] = $value->Year;
                                            }
                                        }

                                        if($Interval == 'Day'){
                                            if(!in_array($value->Day,$date_d) || !in_array($value->Month,$date_m) || !in_array($value->Year, $date_y)){
                                                 $date_f = $value->Day.'/'.$value->Month.'/'.$value->Year;
                                                  $date_d[] = $value->Day;
                                                  $date_m[] = $value->Month;
                                                  $date_y[] = $value->Year;
                                            }
                                        }
                                        
                                     ?>
                                        <tr>
                                            <td><?php echo $date_f; ?></td>
                                            <td><?php echo $value->TotalOrders; ?></td>
                                            
                                            <td><?php echo $value->TotalSum; ?></td>
                                            <td><?php echo $value->TotalTaxAmount; ?></td>
                                            <td><?php echo $value->TotalShippingCharges; ?></td>
                                            <td><?php echo $value->Discount; ?></td>
                                            <td><?php echo $value->Cancelled; ?></td>
                                                                      
                                                                     
                                        </tr>
                                        <?php

                                       
                                    } ?>

                                     


                                    <?php

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function() {
     $('.datepicker').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove',
                inline: true
            }
         });
     });
</script>