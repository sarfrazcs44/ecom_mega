<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Products Reviews</h4>
                        <div class="toolbar">
                            
                        </div>
                        <div class="material-datatables">
                            <table id="" class="search_fields datatables_csv table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>ID</th>

                                    <th><?php echo lang('title');?></th>

                                    <th>Reviews</th>
                                    <th>Average</th>
                                    <th>Action</th>


                                    

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){
                                        $avg_rating = productAverageRating($value->ProductID);
                                     ?>
                                        <tr>
                                            <td><?php echo $value->ProductID; ?></td>
                                            <td><?php echo $value->Title; ?></td>
                                            <td><?php echo $value->TotalReviews; ?></td>
                                            <td><?php echo $avg_rating; ?></td>                           
                                            <td><a href="<?php echo base_url('cms/report/product_reviews/'.$value->ProductID);?>">Show Review</a></td>                           
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>