<style>
    .box_shadow {
        box-shadow: 0 1px 5px 0 rgb(0, 0, 0);
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="background: lightgrey;">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assessment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Reports</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card box_shadow">
                                    <div class="card-content">
                                        <h4 class="card-title" style="font-weight: bold;">Marketing</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/products_in_cart'); ?>">Products in cart</a></h5>
                                            </div>
                                            <div class="col-md-12">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/searchTag'); ?>">Search
                                                        Terms</a></h5>
                                            </div>
                                            <div class="col-md-12">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/abandonedCart'); ?>">Abandoned
                                                        Carts</a></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card box_shadow">
                                    <div class="card-content">
                                        <h4 class="card-title" style="font-weight: bold;">Sales</h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/orders'); ?>">Orders</a></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/tax'); ?>">TAX</a>
                                                </h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/shipping'); ?>">Shipping</a>
                                                </h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/coupons'); ?>">Coupons</a></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/salesByItems'); ?>">By Items</a></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/salesByBrand'); ?>">By Brand</a></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/salesByItems'); ?>?order=desc">Most Selling</a></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/salesByItems'); ?>?order=asc">Least Selling</a></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card box_shadow">
                                    <div class="card-content">
                                        <h4 class="card-title" style="font-weight: bold;">Customers</h4>
                                        <div class="row">
                                            
                                            <div class="col-md-12">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/customer_orders'); ?>">Total Order</a>
                                                </h5>
                                            </div>
                                            <div class="col-md-12">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/new_account_orders'); ?>">Order from New Account</a></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card box_shadow">
                                    <div class="card-content">
                                        <h4 class="card-title" style="font-weight: bold;">Reviews</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/customers_reviews_count'); ?>">By Customers</a></h5>
                                            </div>
                                            <div class="col-md-12">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/products_reviews'); ?>">By Product</a></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card box_shadow">
                                    <div class="card-content">
                                        <h4 class="card-title" style="font-weight: bold;">Products</h4>
                                        <div class="row">
                                            <!--<div class="col-md-12">
                                                <h5 style="font-size: 15px;">Views</h5>
                                            </div>-->
                                            <div class="col-md-12">
                                                <h5 style="font-size: 15px;"><a
                                                            href="<?php echo base_url('cms/report/order_products'); ?>">Ordered Products</a>
                                                </h5>
                                            </div>
                                           <!-- <div class="col-md-12">
                                                <h5 style="font-size: 15px;"><a href="<?php echo base_url('cms/report/out_of_stock'); ?>">Out of Stock</a></h5>
                                            </div>
                                            <div class="col-md-12">
                                                <h5 style="font-size: 15px;">Ordered</h5>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 