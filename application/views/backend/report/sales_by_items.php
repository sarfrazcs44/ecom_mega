<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"> Sales by Items</h4>
                        
                        <div class="material-datatables">
                            <table id="datatables" class="datatables_csv table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    

                                    <th><?php echo lang('title'); ?></th>
                                    <th>SKU</th>
                                    <th>Purchases</th>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    


                                    

                                </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    var columns = <?php echo $columns; ?>;
    ParentID = '<?php echo $customID; ?>';
    var get_data = 'getSalesByItems';
</script>
<script src="<?php echo base_url();?>assets/backend/js/datatable-report.js"></script>
