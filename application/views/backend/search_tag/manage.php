<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Searched Terms Report</h4>
                        <div class="toolbar">
                        </div>
                        <div class="material-datatables">
                            <table id="" class="table table-striped table-no-bordered table-hover search_fields datatables_csv"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Search Query</th>
                                    <th>Hits</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($results) {
                                    foreach ($results as $value) { ?>
                                        <tr>
                                            <td><?php echo $value->SearchTag; ?></td>
                                            <td><?php echo $value->SearchCount; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>