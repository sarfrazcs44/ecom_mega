<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Shipments</h4>
                        <div class="toolbar">
                            <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="<?php echo($url_status == 'driver_shipments' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/shipment?status=driver_shipments'); ?>">To Driver</a>
                                </li>
                                <li class="<?php echo($url_status == 'api_shipments' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/shipment?status=api_shipments'); ?>">To Shipment API</a>
                                </li>
                            </ul>
                        </div>
                           
                        </div>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="driver_shipments">
                                <table id="datatables"  class=" table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                    <?php if($url_status == 'driver_shipments'){ ?>
                                         <th>Driver Name</th>
                                        <th>Driver Mobile</th>

                                    <?php } ?>
                                       
                                        <th>Customer Name</th>
                                        <th>Customer Mobile</th>
                                        <th>Order #</th>
                                        <th>Order Status</th>
                                        <th>Order Amount</th>
                                        <th>Received At</th>
                                        <th><?php echo lang('actions'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    var columns = <?php echo $columns; ?>;
    ParentID = '<?php echo $url_status; ?>';
    var CallUrl = '<?php echo $ControllerName;?>';
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    
    setTimeout(function () {
        window.location.reload();
    }, 60000);
</script>