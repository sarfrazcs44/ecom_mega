<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('EditSiteSettings'); ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/site_setting/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="SiteSettingID" value="<?php echo $SiteSettingID; ?>">


                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="SiteName"><?php echo lang('SiteName'); ?>  :</label>
                                        <input type="text" class="form-control" name="SiteName" id="SiteName" required value="<?php echo $result->SiteName; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="PhoneNumber"><?php echo lang('PhoneNumber'); ?> :</label>
                                        <input type="text" class="form-control" name="PhoneNumber" id="PhoneNumber" value="<?php echo $result->PhoneNumber; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Email"><?php echo lang('email'); ?> :</label>
                                        <input type="email" class="form-control" name="Email" id="Email" value="<?php echo $result->Email; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Whatsapp"><?php echo lang('Whatsapp'); ?> :</label>
                                        <input type="text" class="form-control" name="Whatsapp" id="Whatsapp" value="<?php echo $result->Whatsapp; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Skype"><?php echo lang('Skype'); ?> :</label>
                                        <input type="text" class="form-control" name="Skype" id="Skype" value="<?php echo $result->Skype; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Fax"><?php echo lang('Fax'); ?> :</label>
                                        <input type="text" class="form-control" name="Fax" id="Fax" value="<?php echo $result->Fax; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Image"><?php echo lang('SiteLogo'); ?> :</label>
                                        <input type="file" class="filestyle" id="Image" name="Image[]" data-placeholder="No Image" accept="image/*" style="position: relative !important;">
                                    </div>
                                    <br>
                                    <?php if ($result->SiteImage != '') { ?>
                                        <img src="<?php echo base_url($result->SiteImage); ?>" alt="image" class="img-responsive img-thumbnail" style="height:200px; width:200px;"/>
                                    <?php } ?>
                                </div>
                            </div>

                            <hr/>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DaysToDeliver">Days to deliver :</label>
                                        <input type="text" class="form-control" name="DaysToDeliver" id="DaysToDeliver" value="<?php echo $result->DaysToDeliver; ?>">
                                    </div>
                                </div>
                            </div>

                            <hr/>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FacebookUrl"><?php echo lang('FacebookUrl'); ?> :</label>
                                        <input type="text" class="form-control" name="FacebookUrl" id="FacebookUrl" value="<?php echo $result->FacebookUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="GoogleUrl"><?php echo lang('GoogleUrl'); ?> :</label>
                                        <input type="text" class="form-control" name="GoogleUrl" id="GoogleUrl" value="<?php echo $result->GoogleUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="LinkedInUrl"><?php echo lang('LinkedInUrl'); ?> :</label>
                                        <input type="text" class="form-control" name="LinkedInUrl" id="LinkedInUrl" value="<?php echo $result->LinkedInUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TwitterUrl"><?php echo lang('TwitterUrl'); ?> :</label>
                                        <input type="text" class="form-control" name="TwitterUrl" id="TwitterUrl" value="<?php echo $result->TwitterUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="InstagramUrl">Instagram Url :</label>
                                        <input type="text" class="form-control" name="InstagramUrl" id="InstagramUrl" value="<?php echo $result->InstagramUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="YoutubeUrl">Youtube Url :</label>
                                        <input type="text" class="form-control" name="YoutubeUrl" id="YoutubeUrl" value="<?php echo $result->YoutubeUrl; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group label-floating">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        <?php echo lang('submit'); ?>
                                    </button>



                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>