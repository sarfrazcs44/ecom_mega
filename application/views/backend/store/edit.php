<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4&libraries=places"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style>
   
    #map-canvas {
        height: 100%;
        width: 100%;
    }
   
</style>
<?php
$option = '';
if(!empty($cities)){
    foreach($cities as $city){
        $option .= '<option value="'.$city->CityID.'" '.((isset($result[0]->CityID) && $result[0]->CityID == $city->CityID) ? 'selected' : '').'>'.$city->Title.' </option>';
    } }


$option2 = '';
$store_district = explode(',',$result[0]->DistrictID);
if (!empty($districts)) {
    foreach ($districts as $district) {
        $option2 .= '<option value="' . $district->DistrictID . '" '.(in_array($district->DistrictID, $store_district) ? 'selected' : '').'>' . $district->Title . ' </option>';
    }
}


?>
<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $map_fields = '';
        $vat_field = '';
        if($key == 0){
        $common_fields = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="CityID">'.lang('choose_city').'</label>
                                        <select id="CityID" class="form-control" required="" name="CityID">
                                               '.$option.'
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="DistrictID">'.lang('district').'</label>
                                        <select id="DistrictID" class="form-control" required="" name="DistrictID[]" multiple>
                                            '.$option2.'
                                        </select>
                                    </div>
                                </div>';
        
        $map_fields = '<div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="Latitude" class="latitude" id="lat" value="'.$result[$key]->Latitude.'">
                                    <input type="hidden" name="Longitude" class="longitude" id="lng" value="'.$result[$key]->Longitude.'">
                                    <input type="hidden"  class="reg-input-city">
                                    
                                    
                                     <div id="map-canvas" style="height: 400px;"></div>
                                   
                                </div>
                            </div>';

        $common_fields2 = '<div class="col-sm-6 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>';
        $vat_field = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="VatNo">Vat no</label>
                                        <textarea id="VatNo" name="VatNo" class="form-control"  required>'.$result[$key]->VatNo.'</textarea>
                                    </div>
                                </div>'; 


        $common_fields3 = '<div class="row">
                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursSaturdayToThursdayFrom">Working Hours Saturday to Thursday From</label>
                                        <input type="text" name="WorkingHoursSaturdayToThursdayFrom" required class="form-control timepicker" id="WorkingHoursSaturdayToThursdayFrom" value="'.$result[$key]->WorkingHoursSaturdayToThursdayFrom.'">
                                        
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursSaturdayToThursdayTo">Working Hours Saturday to Thursday From</label>
                                        
                                        <input type="text" name="WorkingHoursSaturdayToThursdayTo" required class="form-control timepicker" id="WorkingHoursSaturdayToThursdayTo" value="'.$result[$key]->WorkingHoursSaturdayToThursdayTo.'">
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursFridayFrom">Working Hours Friday From</label>
                                        <input type="text" name="WorkingHoursFridayFrom" required class="form-control timepicker" id="WorkingHoursFridayFrom" value="'.$result[$key]->WorkingHoursFridayFrom.'">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursFridayTo">Working Hours Friday To</label>
                                        <input type="text" name="WorkingHoursFridayTo" required class="form-control timepicker" id="WorkingHoursFridayTo" value="'.$result[$key]->WorkingHoursFridayTo.'">
                                    </div>
                                </div>
                            </div> ';                                               
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').'</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                             </div>  
                                                            </div>
                                                             '.$common_fields2.'
                                                        </div>
                                                        '.$common_fields3.'
                                <div class="row">
                                '.$vat_field.'                        
                                
                                </div>
                                 <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DeliveryTime">Delivery Time</label>
                                        <textarea id="DeliveryTime" name="DeliveryTime" class="form-control" required>'.((isset($result[$key]->DeliveryTime)) ? $result[$key]->DeliveryTime : '').'</textarea>
                                    </div>
                                </div>
                                                       
                                                         
                                                         
                                                    </div>
                                                    <div class="row">
                                                   
                                                    '.$common_fields.'

                                                    </div>

                                                    <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label" for="Address">Address</label>
                                                            <textarea id="'.($key == 0 ? 'map-search' : '').'" name="Address" class="form-control" required>'.((isset($result[$key]->Address)) ? $result[$key]->Address : '').'</textarea>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    '.$map_fields.'
                                                    
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';





    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- notice modal -->
<div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i>
                </button>
                <h5 class="modal-title" id="myModalLabel">Select location on map by dragging marker</h5>
            </div>
            <div class="modal-body">
                <div id="googleMap" style="height: 400px;"></div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        $('input.timepicker').timepicker({
            minTime: '9:00:00' // 11:45:00 AM,
        });
    });
</script>
<script>
    function initialize() {
        var mapOptions, map, marker, searchBox, city,
        infoWindow = '',
        addressEl = document.querySelector( '#map-search' ),
        latEl = document.querySelector( '.latitude' ),
        longEl = document.querySelector( '.longitude' ),
        element = document.getElementById( 'map-canvas' );
        city = document.querySelector( '.reg-input-city' );
        mapOptions = {
            // How far the maps zooms in.
            zoom: 8,
            // Current Lat and Long position of the pin/
            center: new google.maps.LatLng( <?php echo $result[0]->Latitude; ?>, <?php echo $result[0]->Longitude; ?> ),
            // center : {
            //  lat: -34.397,
            //  lng: 150.644
            // },
            disableDefaultUI: false, // Disables the controls like zoom control on the map if set to true
            scrollWheel: true, // If set to false disables the scrolling on the map.
            draggable: true, // If set to false , you cannot move the map around.
            // mapTypeId: google.maps.MapTypeId.HYBRID, // If set to HYBRID its between sat and ROADMAP, Can be set to SATELLITE as well.
            // maxZoom: 11, // Wont allow you to zoom more than this
            // minZoom: 9  // Wont allow you to go more up.
            styles:[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "color": "#50456d"
            },
            {
                "saturation": "29"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#b31f1f"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dfd1ae"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#f00000"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text",
        "stylers": [
            {
                "color": "#1a1526"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ff0f0f"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ebe3cd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "color": "#892020"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-84"
            },
            {
                "lightness": "85"
            },
            {
                "gamma": "0.00"
            },
            {
                "weight": "1"
            },
            {
                "color": "#50456d"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#f6c866"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e98d58"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
]
        };
    /**
     * Creates the map using google function google.maps.Map() by passing the id of canvas and
     * mapOptions object that we just created above as its parameters.
     *
     */
    // Create an object map with the constructor function Map()
        map = new google.maps.Map( element, mapOptions ); // Till this like of code it loads up the map.
    /**
     * Creates the marker on the map
     *
     */
        marker = new google.maps.Marker({
            position: mapOptions.center,
             icon: '<?php echo base_url(); ?>assets/frontend/images/pin.svg',
            label: {text: '1' , color: 'white'},
            map: map,
            draggable: true
        });
    /**
     * Creates a search box
     */
         searchBox = new google.maps.places.SearchBox( addressEl );
    /**
     * When the place is changed on search box, it takes the marker to the searched location.
     */
    google.maps.event.addListener( searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces(),
            bounds = new google.maps.LatLngBounds(),
            i, place, lat, long, resultArray,
            addresss = places[0].formatted_address;
        for( i = 0; place = places[i]; i++ ) {
            bounds.extend( place.geometry.location );
            marker.setPosition( place.geometry.location );  // Set marker position new.
        }
        map.fitBounds( bounds );  // Fit to the bound
        map.setZoom( 15 ); // This function sets the zoom to 15, meaning zooms to level 15.
        // console.log( map.getZoom() );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        latEl.value = lat;
        longEl.value = long;
        resultArray =  places[0].address_components;
        // Get the city and set the city input value to the one selected
        for( var i = 0; i < resultArray.length; i++ ) {
            if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                citi = resultArray[ i ].long_name;
                city.value = citi;
            }
        }
        // Closes the previous info window if it already exists
        if ( infoWindow ) {
            infoWindow.close();
        }
        /**
         * Creates the info Window at the top of the marker
         */
        infoWindow = new google.maps.InfoWindow({
            content: addresss
        });
        infoWindow.open( map, marker );
    } );
    /**
     * Finds the new position of the marker when the marker is dragged.
     */
    google.maps.event.addListener( marker, "dragend", function ( event ) {
        var lat, long, address, resultArray, citi;
        console.log( 'i am dragged' );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { latLng: marker.getPosition() }, function ( result, status ) {
            if ( 'OK' === status ) {  // This line can also be written like if ( status == google.maps.GeocoderStatus.OK ) {
                address = result[0].formatted_address;
                resultArray =  result[0].address_components;
                // Get the city and set the city input value to the one selected
                for( var i = 0; i < resultArray.length; i++ ) {
                    if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                        citi = resultArray[ i ].long_name;
                        console.log( citi );
                        city.value = citi;
                    }
                }
                addressEl.value = address;
                latEl.value = lat;
                longEl.value = long;
            } else {
                console.log( 'Geocode was not successful for the following reason: ' + status );
            }
            // Closes the previous info window if it already exists
            if ( infoWindow ) {
                infoWindow.close();
            }
            /**
             * Creates the info Window at the top of the marker
             */
            infoWindow = new google.maps.InfoWindow({
                content: address
            });
            infoWindow.open( map, marker );
        } );
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<!-- end notice modal -->
<script>
   /* var my_lat = <?php echo $result[0]->Latitude; ?>;
    var my_lng = <?php echo $result[0]->Longitude; ?>;

    function initialize() {
        var myLatlng = new google.maps.LatLng(my_lat, my_lng);
        var mapProp = {
            center: myLatlng,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP

        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Drag me!',
            draggable: true
        });
        document.getElementById('lat').value = my_lat;
        document.getElementById('lng').value = my_lng;
        // marker drag event
        google.maps.event.addListener(marker, 'drag', function (event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lng').value = event.latLng.lng();
        });

        //marker drag event end
        google.maps.event.addListener(marker, 'dragend', function (event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lng').value = event.latLng.lng();
            // alert("lat=>" + event.latLng.lat());
            // alert("long=>" + event.latLng.lng());
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);*/
    $(document).ready(function () {
        

         $('#CityID').on('change',function(){
          
            var CityID = $(this).val();
            $('#DistrictID').html('');

            if(CityID == '')
            {
                $('#DistrictID').html('<option value=""><?php echo lang("district");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "GET",
                    url: base_url + 'cms/district/getDistrictsForCity',
                    data: {
                        'CityID': CityID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#DistrictID').html(result.html);

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
   });

</script>