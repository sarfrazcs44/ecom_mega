<?php
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        if ($key == 0) {
            if ($result[$key]->ChargesType == "Tax") {
                $tax = "selected";
                $shipment = "";
            } elseif ($result[$key]->ChargesType == "Shipment") {
                $tax = "";
                $shipment = "selected";
            }
            if ($result[$key]->Type == "Fixed") {
                $fixed = "selected";
                $percentage = "";
            } elseif ($result[$key]->Type == "Percentage") {
                $fixed = "";
                $percentage = "selected";
            }
            if ($result[$key]->Image == '') {
                $image = base_url("assets/backend/img/no_img.png");
            } else {
                $image = base_url($result[$key]->Image);
            }
            $common_fields = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Amount">Amount</label>
                                        <input type="text" name="Amount" required class="form-control number-with-decimals" id="Amount" value="' . $result[$key]->Amount . '">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ChargesType">Charges Type</label>
                                        <select id="ChargesType" class="selectpicker"
                                                data-style="select-with-transition"
                                                name="ChargesType">
                                            <option value="Tax" ' . $tax . '>Tax</option>
                                            <option value="Shipment" ' . $shipment . '>Shipment</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Type">Type</label>
                                        <select id="Type" class="selectpicker" data-style="select-with-transition"
                                                name="Type">
                                            <option value="Fixed" ' . $fixed . '>Fixed</option>
                                            <option value="Percentage" ' . $percentage . '>Percentage</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-12">
                                    <img src="' . $image . '" style="width:100px;height:100px;">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type="file" name="Image[]" multiple="multiple">
                                    </div>
                                </div>
                            </div>';
            $common_fields2 = '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>';
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                        ' . $common_fields . '
                                                        <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">Description</label>
                                        <textarea class="form-control textarea" name="Description" id="Description"
                                                  style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                    </div>
                                </div>
                                                         ' . $common_fields2 . '
                                                    </div>
                                                    
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>