<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/' . $ControllerName . '/add'); ?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add
                                    Charges
                                </button>
                            </a>
                        </div>
                        <br>
                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#pill1" data-toggle="tab" aria-expanded="true">Taxes</a>
                            </li>
                            <li class="">
                                <a href="#pill2" data-toggle="tab" aria-expanded="false">Shipment Charges</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="pill1">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th><?php echo lang('title');?></th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Image</th>
                                        <th>Description</th>
                                        <th><?php echo lang('is_active');?></th>
                                        <?php if(checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                            <th><?php echo lang('actions');?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($taxes){
                                        foreach($taxes as $value){ ?>
                                            <tr id="<?php echo $value->TaxShipmentChargesID;?>">
                                                <td><?php echo $value->Title; ?></td>
                                                <td><?php echo $value->Type; ?></td>
                                                <td><?php echo $value->Amount; ?></td>
                                                <td><?php echo $value->Image; ?></td>
                                                <td><?php echo $value->Description; ?></td>
                                                <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                                <?php if(checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <td>
                                                        <?php if(checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                            <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->TaxShipmentChargesID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                        <?php } ?>

                                                        <?php if(checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                            <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->TaxShipmentChargesID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="pill2">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th><?php echo lang('title');?></th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Image</th>
                                        <th>Description</th>
                                        <th><?php echo lang('is_active');?></th>
                                        <?php if(checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                            <th><?php echo lang('actions');?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($shipment_charges){
                                        foreach($shipment_charges as $value){ ?>
                                            <tr id="<?php echo $value->TaxShipmentChargesID;?>">
                                                <td><?php echo $value->Title; ?></td>
                                                <td><?php echo $value->Type; ?></td>
                                                <td><?php echo $value->Amount; ?></td>
                                                <td><?php echo $value->Image; ?></td>
                                                <td><?php echo $value->Description; ?></td>
                                                <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                                <?php if(checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <td>
                                                        <?php if(checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                            <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->TaxShipmentChargesID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                        <?php } ?>

                                                        <?php if(checkUserRightAccess(80,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                            <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->TaxShipmentChargesID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $(document).ready(function () {
        $('table.datatable').DataTable({
            "ordering": false
        });
    });
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>