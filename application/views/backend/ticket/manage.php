<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Tickets</h4>
                        <div class="toolbar">
                            <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="<?php echo($url_status == 'ongoing_tickets' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/ticket?status=ongoing_tickets'); ?>">Ongoing</a>
                                </li>
                                <li class="<?php echo($url_status == 'closed_tickets' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/ticket?status=closed_tickets'); ?>">Closed</a>
                                </li>
                                <?php if ($this->session->userdata['admin']['RoleID'] == 1) { ?>
                                    <li class="<?php echo($url_status == 'reopened_tickets' ? 'active nav-pills-warning' : ''); ?>">
                                       <a href="<?php echo base_url('cms/ticket?status=reopened_tickets'); ?>">Re-Opened</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        </div>
                        <div class="tab-content">
                            <div class="" id="ongoing_tickets">
                                <table id="datatables" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>User Email</th>
                                    <th>Ticket #</th>
                                    <th>Ticket submitted on</th>
                                    <th>Order #</th>
                                    <th>Order Status</th>
                                    <th>Order Amount</th>
                                    <?php if (checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    var columns = <?php echo $columns; ?>;
    ParentID = '<?php echo $url_status; ?>';
    var CallUrl = '<?php echo $ControllerName;?>';
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    
    setTimeout(function () {
        window.location.reload();
    }, 60000);
</script>