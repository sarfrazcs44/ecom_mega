<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<style>
    /* .msgbox {
        overflow-y: scroll;
        height: 500px;
    } */
    .panel-group .card {
        margin-top: 0;
    }
</style>
<?php
if ($ticket[0]->IsClosed == 0) {
    $IsClosed = "Ongoing";
    $class = "btn btn-success btn-sm";
} else if ($ticket[0]->IsClosed == 1) {
    $IsClosed = "Closed";
    $class = "btn btn-danger btn-sm";
} else if ($ticket[0]->IsClosed == 2) {
    $IsClosed = "Re-Opened";
    $class = "btn btn-warning btn-sm";
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Ticket comments</h5>
                    </div>
                    <div class="card-content">
                        <div class="msgbox"><!--  OLD CLASS ==>   msgbox    -->
                            <?php
                            $html = '';
                            if ($messages) {
                                foreach ($messages as $message) {
                                    $WhoSaid = ($message->UserID == 0) ? "Support" : "Customer";
                                    $class = $message->UserID == 0 ? "label-danger" : "label-success";
                                    $right_left = $message->UserID == 0 ? "pull-right" : "pull-left";
                                    $SentReceived = ($message->UserID == 0) ? "msgsent" : "msgreceive"; ?>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="timeline-panel <?php echo $right_left; ?>">
                                                <div class="timeline-body">
                                                    <p>
                                                        <span class="label <?php echo $class; ?>"><?php echo $WhoSaid; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $message->Message; ?>
                                                    </p>
                                                </div>
                                                <small>
                                                    <i class="ti-time"></i> <?php echo date('d.m.Y h:i:s A', strtotime($message->CreatedAt)); ?>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <form action="<?php echo base_url('cms/ticket/saveMessage'); ?>" method="post"
                              class="ticketMessage"
                              id="saveTicketMessage">
                            <input type="text" name="Message" class="form-control required">
                            <input type="hidden" name="TicketID" id="TicketIDForMessage"
                                   value="<?php echo $ticket[0]->TicketID; ?>">
                            <button type="submit"
                                    class="btn btn btn-primary pull-right" <?php echo($ticket[0]->IsClosed == 1 ? 'disabled' : ''); ?>>
                                Send
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#collapse1">Order Details</a></h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    Details For ticket # <b><?php echo $ticket[0]->TicketNumber; ?></b>
                                                    &nbsp;&nbsp;<button class="<?php echo $class; ?>">
                                                        <?php echo $IsClosed; ?>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                    <div class="pull-right">
                                                        <?php
                                                        if ($ticket[0]->IsClosed == 0) { ?>
                                                            <button class="btn btn-danger btn-sm"
                                                                    onclick="closeTicket(<?php echo $ticket[0]->TicketID; ?>, '1');">
                                                                Close Ticket
                                                                <div class="ripple-container"></div>
                                                            </button>
                                                        <?php } elseif ($ticket[0]->IsClosed == 1 && $this->session->userdata['admin']['RoleID'] == 1) { ?>
                                                            <button class="btn btn-warning btn-sm"
                                                                    onclick="closeTicket(<?php echo $ticket[0]->TicketID; ?>, '2');">
                                                                Re-Open Ticket
                                                                <div class="ripple-container"></div>
                                                            </button>
                                                        <?php } elseif ($ticket[0]->IsClosed == 2 && $this->session->userdata['admin']['RoleID'] == 1) { ?>
                                                            <button class="btn btn-danger btn-sm"
                                                                    onclick="closeTicket(<?php echo $ticket[0]->TicketID; ?>, '1');">
                                                                Close Ticket
                                                                <div class="ripple-container"></div>
                                                            </button>
                                                        <?php }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-content">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Order #</label>
                                                                <h5><?php echo $ticket[0]->OrderNumber; ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Order Status</label>
                                                                <h5><?php echo $ticket[0]->OrderStatusEn; ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Order Placed At</label>
                                                                <h5><?php echo date('d-m-Y h:i:s A', strtotime($ticket[0]->OrderCreatedAt)); ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Est Order Delivery</label>
                                                                <?php
                                                                $site_setting = site_settings();
                                                                $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                ?>
                                                                <h5><?php echo date('d-m-Y', strtotime($days_to_deliver, strtotime($ticket[0]->OrderCreatedAt))); ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Customer Name</label>
                                                                <h5>
                                                                    <a href="<?php echo base_url('cms/customer/edit') . '/' . $ticket[0]->UserID; ?>"
                                                                    target="_blank"><?php echo $ticket[0]->FullName; ?></a></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Mobile No.</label>
                                                                <h5><?php echo($ticket[0]->Mobile != '' ? '<a href="tel:' . $ticket[0]->Mobile . '">' . $ticket[0]->Mobile . '</a>' : 'N/A'); ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Email</label>
                                                                <h5><?php echo($ticket[0]->Email != '' ? '<a href="mailto:' . $ticket[0]->Email . '">' . $ticket[0]->Email . '</a>' : 'N/A'); ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">User City</label>
                                                                <h5><?php echo($ticket[0]->UserCity != '' ? ucfirst($ticket[0]->UserCity) : 'N/A'); ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Coupon Code Used</label>
                                                                <h5><?php echo($ticket[0]->CouponCodeUsed != '' ? $ticket[0]->CouponCodeUsed : 'N/A'); ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Coupon Discount %</label>
                                                                <h5><?php echo($ticket[0]->CouponCodeDiscountPercentage > 0 ? $ticket[0]->CouponCodeDiscountPercentage : 'N/A'); ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Discount Availed</label>
                                                                <h5><?php echo($ticket[0]->DiscountAvailed > 0 ? $ticket[0]->DiscountAvailed . ' SAR' : 'N/A') ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Total Shipping Charges</label>
                                                                <h5><?php echo number_format($ticket[0]->TotalShippingCharges, 2); ?> SAR</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Total Tax Amount</label>
                                                                <h5><?php echo number_format($ticket[0]->TotalTaxAmount, 2); ?> SAR</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Total Amount</label>
                                                                <h5><?php echo number_format($ticket[0]->TotalAmount, 2); ?> SAR</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Payment Method</label>
                                                                <h5><?php echo $ticket[0]->PaymentMethod; ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Shipment Method</label>
                                                                <h5><?php echo $ticket[0]->ShipmentMethodTitle; ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label">Ticket Created At</label>
                                                                <h5><?php echo date('d-m-Y h:i:s A', strtotime($ticket[0]->TicketCreatedAt)); ?></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="row">
                                                    <?php
                                                    if (isset($ticket[0]->CollectFromStore) && $ticket[0]->CollectFromStore == 1) { ?>
                                                        <h4>Collect From Store</h4>
                                                    <?php } else { ?>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group label-floating">
                                                                <div class="form-line">
                                                                    <label class="control-label">Delivery Address</label>
                                                                    <h5>
                                                                        <span>Recipient Name: <?php echo $ticket[0]->RecipientName; ?></span><br>
                                                                        <span>Recipient Mobile No: <?php echo $ticket[0]->MobileNo; ?></span><br>
                                                                        <span>Email: <?php echo $ticket[0]->Email; ?></span><br>
                                                                        <span>City: <?php echo $ticket[0]->AddressCity; ?></span><br>
                                                                        <span>District: <?php echo $ticket[0]->AddressDistrict; ?></span><br>
                                                                        <span>Building No: <?php echo $ticket[0]->BuildingNo; ?></span><br>
                                                                        <span>Street: <?php echo $ticket[0]->Street; ?></span><br>
                                                                        <span>P.O Box: <?php echo $ticket[0]->POBox; ?></span><br>
                                                                        <span>Zip Code: <?php echo $ticket[0]->ZipCode; ?></span><br>
                                                                        <span>See location on map: <a
                                                                                    href="https://www.google.com/maps/search/?api=1&amp;query=<?php echo $ticket[0]->Latitude; ?>,<?php echo $ticket[0]->Longitude; ?>"
                                                                                    target="_blank">
                                                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHbVVHxTASKeTMj0H-JFtHN138p-i6Rx-UdC0VQ2l17yJcaRFVCQ"
                                                                        height="25" width="25"
                                                                        style="height: 40px !important;width: 40px !important;">
                                                                </a></span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#collapse2">Extra Charges</a></h4></div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-title">Extra Charges</h5>
                                            </div>
                                            <div class="card-content">
                                                <div class="material-datatables">
                                                    <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                                        style="width:100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Title</th>
                                                            <th>Factor</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php if ($order_extra_charges) {
                                                            foreach ($order_extra_charges as $extra_charge) { ?>
                                                                <tr>
                                                                    <td><?php echo $extra_charge->Title; ?></td>
                                                                    <td><?php echo $extra_charge->Factor; ?></td>
                                                                    <td><?php echo number_format($extra_charge->Amount, 2); ?> SAR</td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-title">Assigned To</h5>
                                            </div>
                                            <div class="card-content">
                                                <div class="row clearfix">
                                                    <div class="col-sm-12">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label" for="AssignedDriverName">Assigned Driver
                                                                    Name</label>
                                                                <?php echo($ticket[0]->AssignedDriverName != '' ? ucfirst($ticket[0]->AssignedDriverName) : 'N/A'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label" for="AssignedDriverEmail">Assigned Driver
                                                                    Email</label>
                                                                <?php echo($ticket[0]->AssignedDriverEmail != '' ? '<a href="mailto:' . $ticket[0]->AssignedDriverEmail . '">' . $ticket[0]->AssignedDriverEmail . '</a>' : 'N/A'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label" for="AssignedDriverMobile">Assigned Driver
                                                                    Mobile</label>
                                                                <?php echo($ticket[0]->AssignedDriverMobile != '' ? '<a href="tel:' . $ticket[0]->AssignedDriverMobile . '">' . $ticket[0]->AssignedDriverMobile . '</a>' : 'N/A'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-title">Change order status</h5>
                                            </div>
                                            <div class="card-content">
                                                <div class="row clearfix">
                                                    <div class="col-sm-12">
                                                        <div class="form-group label-floating">
                                                            <div class="form-line">
                                                                <label class="control-label" for="Status">Status</label>
                                                                <select class="selectpicker" id="Status" data-style="select-with-transition">
                                                                    <?php foreach ($order_statuses as $order_status) { ?>
                                                                        <option value="<?php echo $order_status->OrderStatusID; ?>" <?php echo($order_status->OrderStatusID == $ticket[0]->Status ? 'selected disabled' : ''); ?>><?php echo $order_status->OrderStatusEn; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="OrderID" value="<?php echo $ticket[0]->OrderID; ?>">
                                                    <div class="col-sm-12">
                                                        <div class="form-group label-floating">
                                                            <button class="btn btn-primary waves-effect waves-light pull-right"
                                                                    type="button"
                                                                    onclick="changeOrderStatus();">
                                                                <?php echo lang('submit'); ?>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#collapse3">Ordered Items</a></h4></div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-title">Ordered Items</h5>
                                            </div>
                                            <div class="card-content">
                                                <div class="material-datatables">
                                                    <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                                        style="width:100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Product Image</th>
                                                            <th>Product Title</th>
                                                            <th>Quantity</th>
                                                            <th>Price</th>
                                                            <th>Total</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php if ($order_items) {
                                                            foreach ($order_items as $order_item) { ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php
                                                                        if ($order_item->ItemType == 'Product') { ?>
                                                                            <a href="<?php echo base_url() . '/' . get_images($order_item->ProductID, 'product', false); ?>" target="_blank">
                                                                                <img src="<?php echo base_url() . '/' . get_images($order_item->ProductID, 'product', false); ?>" style="width: 70px;height: 70px;">
                                                                            </a>
                                                                        <?php } elseif ($order_item->ItemType == 'Customized Shape') { ?>
                                                                            <a href="<?php echo base_url($order_item->CustomizedShapeImage); ?>" target="_blank">
                                                                                <img src="<?php echo base_url($order_item->CustomizedShapeImage); ?>" style="width: 70px;height: 70px;">
                                                                            </a>
                                                                        <?php } else { ?>
                                                                            <a href="javascript:void(0);" class="chocobox_detail" title="Click to view whats inside" data-box_id="<?php echo $order_item->CustomizedBoxID; ?>" data-pids="<?php echo $order_item->CustomizedOrderProductIDs; ?>" data-box_type="<?php echo $order_item->ItemType; ?>">
                                                                                <img src="<?php echo front_assets("images/".$order_item->ItemType.".png"); ?>" style="width: 70px;height: 70px;">
                                                                            </a>
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php
                                                                        if ($order_item->ItemType == 'Product') { ?>
                                                                            <a href="<?php echo base_url('cms/product/edit') . '/' . $order_item->ProductID; ?>"
                                                                            target="_blank"><?php echo $order_item->Title; ?></a>
                                                                        <?php } elseif ($order_item->ItemType == 'Customized Shape') { ?>
                                                                            Customized Shape
                                                                        <?php } else { ?>
                                                                            <a href="javascript:void(0);" class="chocobox_detail" title="Click to view whats inside" data-box_id="<?php echo $order_item->CustomizedBoxID; ?>" data-pids="<?php echo $order_item->CustomizedOrderProductIDs; ?>" data-box_type="<?php echo $order_item->ItemType; ?>">
                                                                                <?php echo $order_item->ItemType; ?>
                                                                            </a>
                                                                        <?php }
                                                                        ?>
                                                                    </td>
                                                                    <td><?php echo $order_item->Quantity; ?><?php echo ($order_item->IsCorporateItem ? ' kg' : 'pcs'); ?></td>
                                                                    <td><?php echo number_format($order_item->Amount, 2); ?> SAR</td>
                                                                    <td><?php echo number_format($order_item->Quantity * $order_item->Amount, 2); ?>
                                                                        SAR
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Offer Detail Message Modal -->
<div id="ChocoboxDetailModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="ChocoboxDetailModalDescription">
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('a796cb54d7c4b4ae4893', {
        cluster: 'ap2',
        forceTLS: true
    });

    var channel = pusher.subscribe("Ecommerce_Ticket_Channel_<?php echo $ticket[0]->TicketID; ?>");
    channel.bind("Ecommerce_Ticket_Event_<?php echo $ticket[0]->TicketID; ?>", function (data) {
        var my_html = data.my_html;
        $('.msgbox').html(my_html);
        $('.msgbox').animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });

    $(document).ready(function () {
        $('.msgbox').animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });
</script>
<script>
    $(document).on('click', '.chocobox_detail', function () {
        var pids = $(this).data('pids');
        var box_type = $(this).data('box_type');
        var box_id = $(this).data('box_id');
        showCustomLoader();
        $.ajax({
            type: "POST",
            url: base_url + 'customize/getChocoboxDetail',
            data: {'ProductIDs': pids, 'box_type': box_type, 'box_id': box_id},
            success: function (result) {
                hideCustomLoader();
                $('#ChocoboxDetailModalDescription').html(result);
                $('#ChocoboxDetailModal').modal('show');
            }
        });
    });
</script>