<style type="text/css">
    .user_field, .user_field_delivery{
        display: none;
    }
</style>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Import Backend User</h4>
                        
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form method="post" id="import_form" enctype="multipart/form-data">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="RoleID"><?php /*echo lang('choose_user_role'); */?></label>
                                        <select id="RoleID" class="selectpicker" data-style="select-with-transition" required name="RoleID">

                                            <?php if(!empty($roles)){
                                                foreach($roles as $role){ ?>
                                                    <option value="<?php echo $role->RoleID; ?>"><?php echo $role->Title; ?> </option>
                                                <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <p><label>Select Excel File</label>
                            <input type="file" name="file" id="file" required accept=".xls, .xlsx" /></p>
                            <br />
                            <input type="submit" name="form_type" value="Import" class="btn btn-info" />
                        </form>
                        <div class="table-responsive" id="customer_data">
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
$(document).ready(function(){


 $('#import_form').on('submit', function(event){
  event.preventDefault();
  $.ajax({
   url:base_url + 'cms/user/import',
   method:"POST",
   data:new FormData(this),
   contentType:false,
   cache:false,
   processData:false,
   success:function(data){
    $('#file').val('');
    alert(data);
   }
  })
 });

});
</script>
