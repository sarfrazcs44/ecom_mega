<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/' . $ControllerName . '/add'); ?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add
                                    Backend User
                                </button>
                            </a>
                            <a href="<?php echo base_url('cms/' . $ControllerName . '/importUser'); ?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Import Backend User
                                </button>
                            </a>
                        </div><br>
                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#pill1" data-toggle="tab" aria-expanded="true">Admins</a>
                            </li>
                            <li class="">
                                <a href="#pill2" data-toggle="tab" aria-expanded="false">Warehouse Users</a>
                            </li>
                            <li class="">
                                <a href="#pill3" data-toggle="tab" aria-expanded="false">Delivery Users</a>
                            </li>
                            <li class="">
                                <a href="#pill4" data-toggle="tab" aria-expanded="false">Store Admin Users</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="pill1">
                                <table id="" class="datatable  table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Is Active?</th>

                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($admins) {
                                        foreach ($admins as $value) { ?>
                                            <tr id="<?php echo $value->UserID; ?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                                <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                                    <td>
                                                        <!-- <a href="<?php echo base_url('cms/' . $ControllerName . '/changePassword/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>-->
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/edit/' . $value->UserID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="Edit">dvr</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                            <a href="javascript:void(0);"
                                                               onclick="deleteRecord('<?php echo $value->UserID; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/user/rights/' . $value->UserID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="User Rights">vertical_split</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="pill2">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Store Name</th>
                                        <th>Is Active?</th>

                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($warehouse_users) {
                                        foreach ($warehouse_users as $value) { ?>
                                            <tr id="<?php echo $value->UserID; ?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->StoreTitle; ?></td>
                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                                <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                                    <td>
                                                        <!-- <a href="<?php echo base_url('cms/' . $ControllerName . '/changePassword/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>-->
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/edit/' . $value->UserID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="Edit">dvr</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                            <a href="javascript:void(0);"
                                                               onclick="deleteRecord('<?php echo $value->UserID; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/user/rights/' . $value->UserID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="User Rights">vertical_split</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="pill3">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Store Name</th>
                                        <th>Is Active?</th>

                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($delivery_users) {
                                        foreach ($delivery_users as $value) { ?>
                                            <tr id="<?php echo $value->UserID; ?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->StoreTitle; ?></td>
                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                                <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                                    <td>
                                                        <!-- <a href="<?php echo base_url('cms/' . $ControllerName . '/changePassword/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>-->
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/edit/' . $value->UserID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="Edit">dvr</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                            <a href="javascript:void(0);"
                                                               onclick="deleteRecord('<?php echo $value->UserID; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/user/rights/' . $value->UserID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="User Rights">vertical_split</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="pill4">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Store Name</th>
                                        <th>Is Active?</th>

                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($store_admin_users) {
                                        foreach ($store_admin_users as $value) { ?>
                                            <tr id="<?php echo $value->UserID; ?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->StoreTitle; ?></td>
                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                                <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete') || checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                                    <td>
                                                        <!-- <a href="<?php echo base_url('cms/' . $ControllerName . '/changePassword/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>-->
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/edit/' . $value->UserID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="Edit">dvr</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                            <a href="javascript:void(0);"
                                                               onclick="deleteRecord('<?php echo $value->UserID; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/user/rights/' . $value->UserID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="User Rights">vertical_split</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $(document).ready(function () {
        $('table.datatable').DataTable({
            "ordering": false
        });
    });
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>