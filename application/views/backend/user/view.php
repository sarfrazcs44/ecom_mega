<?php
$user = convertEmptyToNA($result[0]);
if ($user->IsActive == 0) {
    $is_active_msg = 'Activate';
    $is_active_cls = "btn btn-success btn-sm";
    $is_active_val = 1;
} elseif ($user->IsActive == 1) {
    $is_active_msg = 'Deactivate';
    $is_active_cls = "btn btn-danger btn-sm";
    $is_active_val = 0;
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            User Details
                            <div class="pull-right">
                                <button class="<?php echo $is_active_cls; ?>"
                                        onclick="UserActive('<?php echo $user->UserID; ?>', '<?php echo $is_active_val; ?>');">
                                    <?php echo $is_active_msg; ?>
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-danger btn-sm"
                                        onclick="LogoutUser('<?php echo $user->UserID; ?>');">
                                    Logout From All Devices
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                            <?php if($user->IsEmailVerified == 0){ ?>
                            <div class="pull-right">
                                <button class="btn btn-success btn-sm"
                                        onclick="VerifyUser('<?php echo $user->UserID; ?>', 'email');">
                                    Verify Email
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                        <?php } ?>
                         <?php if($user->IsMobileVerified == 0){ ?>
                            <div class="pull-right">
                                <button class="btn btn-success btn-sm"
                                        onclick="VerifyUser('<?php echo $user->UserID; ?>', 'mobile');">
                                    Verify Mobile
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Full Name</label>
                                        <h5><?php echo $user->FullName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">City</label>
                                        <h5><?php echo $user->CityTitle; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Gender</label>
                                        <h5><?php echo $user->Gender; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Email</label>
                                        <h5><?php echo $user->Email; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Mobile No.</label>
                                        <h5><?php echo $user->Mobile; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Email Verified?</label>
                                        <h5><?php echo $user->IsEmailVerified == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Mobile Verified?</label>
                                        <h5><?php echo $user->IsMobileVerified == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Device Type</label>
                                        <h5><?php echo $user->DeviceType; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Device OS</label>
                                        <h5><?php echo $user->OS; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Active?</label>
                                        <h5><?php echo $user->IsActive == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <?php if($user->RoleID == 3){ ?>
                <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            Driver Details
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Iqama Number</label>
                                        <h5><?php echo $user->IqamaNumber; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Iqama Expiry</label>
                                        <h5><?php echo $user->IqamaExpiry; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Driving License No</label>
                                        <h5><?php echo $user->DrivingLicenseNo; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Driving License Expiry</label>
                                        <h5><?php echo $user->DrivingLicenseExpiry; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Vehicle Make</label>
                                        <h5><?php echo $user->VehicleMake; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">VehicleModel</label>
                                        <h5><?php echo $user->VehicleModel; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Registration No</label>
                                        <h5><?php echo $user->RegistrationNo; ?></h5>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Emergency Contact</label>
                                        <h5><?php echo $user->EmergencyContact; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Driving License Img</label>
                                        <?php if($user->DrivingLicenseImg != 'N/A'){ ?>
                                             <h5><a href="<?php base_url($user->DrivingLicenseImg);?>" target="_blank" style="height:200px;width:200px;cursor: pointer;"><img src></a></h5>
                                    <?php }else{ ?>
                                        <h5><?php echo $user->DrivingLicenseImg; ?></h5>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Registration License Img</label>
                                        <?php if($user->RegistrationLicenseImg != 'N/A'){ ?>
                                        <h5><a href="<?php base_url($user->RegistrationLicenseImg);?>" target="_blank" style="height:200px;width:200px;cursor: pointer;"><img src></a></h5>
                                    <?php }else{ ?>
                                        <h5><?php echo $user->RegistrationLicenseImg; ?></h5>

                                    <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">IBAN</label>
                                        <h5><?php echo $user->IBAN; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">BankName</label>
                                        <h5><?php echo $user->BankName; ?></h5>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



            <?php } ?>
            
        </div>
    </div>
</div>


<script>
    function UserActive(user_id, val) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/updateIsActive',
                        data: {
                            'UserID': user_id,
                            'IsActive': val
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }


    function VerifyUser(user_id, verification_of) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/verifyUser',
                        data: {
                            'UserID': user_id,
                            'Verification': verification_of
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }


    function LogoutUser(user_id) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/logout_from_all_devices',
                        data: {
                            'UserID': user_id
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }

    $(document).ready(function () {
        $('.fancybox').fancybox({
            beforeShow: function () {
                this.title = $(this.element).data("caption");
            }
        });
    }); // ready
</script>