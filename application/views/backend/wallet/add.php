<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Add Credit</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Amount">Add the requied amount</label>
                                        <input type="number" name="Amount" required  class="form-control" id="Amount">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <input type="radio" id="CreditCard" name="Method" value="Credit Card" checked="checked">
                                            <label for="CreditCard">Visa Card/Master Card</label><br>
                                            <div class="CreditCard div">
                                                    <div class="form-group owner">
                                                        <label for="CardHolder">Owner</label>
                                                        <input type="text" name="CardHolder" class="form-control" id="CardHolder">
                                                    </div>
                                                    <div class="form-group CVV">
                                                        <label for="CVV">CVV</label>
                                                        <input type="number" name="CVV" placeholder="xxx" class="form-control" id="cvv" max="999">
                                                    </div>
                                                    <div class="form-group" id="card-number-field">
                                                        <label for="CardNumber">Card Number</label>
                                                        <input type="tel" inputmode="numeric" pattern="[0-9\s]{13,19}" autocomplete="cc-number" maxlength="19" placeholder="xxxx xxxx xxxx xxxx" name="CardNumber" class="form-control" id="cardNumber">
                                                    </div>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="ExpiryDate">Expiry Date</label>
                                                        <input type="text" name="ExpiryDate" required
                                                               class="form-control" id="ExpiryDate" placeholder="MM/YY">
                                                    </div>
                                            </div>
                                            <input type="radio" id="BankTransfer" name="Method" value="Bank Transfer">
                                            <label for="BankTransfer">Bank transfer</label><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetDiv = $("." + inputValue);
        $(".div").not(targetDiv).hide();
        $(targetDiv).show();
    });
});
</script>