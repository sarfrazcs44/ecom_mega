<section class="banner">
    <img src="<?php echo base_url($result->BannerImage); ?>">
</section>
<section class="content about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo base_url($result->Image); ?>" class="featured">
                <?php echo $result->Description; ?>
            </div>
        </div>
    </div>
</section>
