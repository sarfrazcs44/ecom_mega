<!--<script  type="text/javascript"
        src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false&key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4"></script>-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4&libraries=places"></script>
<style>
    #addAddress .form-control {
        background-color: transparent !important;
        border: 1px solid #e5cec8;
    }
    #addAddress label {
        font-size: 15px;
        margin: 0 0 8px;
    }
    #addAddress .form-field {
        padding-bottom: 15px !important;
        margin-top: 25px;
    }
    a {
        cursor: pointer;
    }
   
    #map-canvas {
        height: 100%;
        width: 100%;
    }
   
</style>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo lang('add_new_address'); ?></span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <form action="<?php echo base_url('address/save'); ?>" method="post" class="ajaxForm"
                          id="addAddress">
                        <div class="col-md-4">
                            <div class="form-field">
                                <label><?php echo lang('recipient_name'); ?></label>
                                <input type="text" name="RecipientName"
                                       class="form-control required" value="<?php echo $this->session->userdata['user']->FullName; ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-field">
                                <label><?php echo lang('mobile_no'); ?></label>
                                <input type="text" name="MobileNo" value="<?php echo $this->session->userdata['user']->Mobile; ?>" class="form-control phone number-only required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-field">
                                <label><?php echo lang('email'); ?></label>
                                <input type="email" name="Email" value="<?php echo $this->session->userdata['user']->Email; ?>" class="form-control required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="CityID"><?php echo lang('city'); ?></label>
                                <select class="form-control required" id="CityID" name="CityID"
                                        onchange="getDistrictsForCity(this.value, '<?php echo $language; ?>');">
                                    <option value=""><?php echo lang('please_select_city'); ?></option>
                                    <?php
                                    $cities = getCities();
                                    foreach ($cities as $city) { ?>
                                        <option value="<?php echo $city->CityID; ?>"><?php echo $city->Title; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="DistrictID"><?php echo lang('district'); ?></label>
                                <select class="form-control required DistrictDD" id="DistrictID" name="DistrictID">
                                    <option value=""><?php echo lang('please_select_city_first'); ?></option>
                                </select>
                            </div>
                        </div>
                        <!--<div class="col-md-4">
                            <div class="form-field">
                                <label><?php echo lang('building_no'); ?></label>
                                <input type="text" name="BuildingNo" class="form-control required">
                            </div>
                        </div>-->
                        
                        <!--<div class="col-md-4">
                            <div class="form-field">
                                <label><?php echo lang('POBox'); ?></label>
                                <input type="text" name="POBox" class="form-control required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-field">
                                <label><?php echo lang('zip_code'); ?></label>
                                <input type="text" name="ZipCode" class="form-control required">
                            </div>
                        </div>-->
                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="AddressType"><?php echo lang('address_type'); ?></label>
                                <select class="form-control" id="AddressType" name="AddressType" >
                                    <option value="Work">Work</option>
                                    <option value="Home"><?php echo lang('home'); ?></option>
                                    
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <div class="form-field">
                                <label><?php echo lang('street'); ?></label>
                                <input type="text" id="map-search" name="Street" class="form-control required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-field">
                                <label> &nbsp; <?php // echo lang('is_default'); ?></label><br>
                                <label class="addChkBoxEd"><?php echo lang('is_default'); ?>
                                    <input type="checkbox" name="IsDefault" >
                                    <span class="checkmark"></span>
                                </label>
                                <!-- <label for="IsDefault"><?php echo lang('is_default'); ?></label>
                                 <input type="checkbox" name="IsDefault" class="form-control"> -->
                                <!--<select class="form-control" id="IsDefault" name="IsDefault" >
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                    
                                </select>-->
                            </div>
                        </div>
                       
                
                        
                       <div class="col-md-12">
                            <input type='hidden' name='Latitude' id='lat' class="latitude">
                            <input type='hidden' name='Longitude' id='lng' class="longitude">
                            <input type='hidden'  class="reg-input-city">
                            
                            
                             <div id="map-canvas" style="height: 400px;"></div>
                           <!-- <div class="form-field">
                                
                                
                                <h5>
                                    <a data-toggle="modal" data-target="#mapModal">
                                        <?php echo lang('google_pin_link'); ?>
                                    </a>
                                    <p><?php echo lang('jeddah_default_pin'); ?></p>
                                </h5>
                            </div>-->
                        </div>
                        <div class="col-md-12" style="margin-top: 10px;">
                            <div class="btn-groups">
                                <button type="submit" class="btn btn-primary"><?php echo lang('add'); ?></button>
                                <!--<button type="button" class="btn btn-primary cancel">Cancel</button>-->
                                <a class="btn btn-primary cancel" href="javascript:void(0);"
                                   onclick="window.history.back();"><?php echo lang('cancel'); ?></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function initialize() {
        var mapOptions, map, marker, searchBox, city,
        infoWindow = '',
        addressEl = document.querySelector( '#map-search' ),
        latEl = document.querySelector( '.latitude' ),
        longEl = document.querySelector( '.longitude' ),
        element = document.getElementById( 'map-canvas' );
        city = document.querySelector( '.reg-input-city' );
        mapOptions = {
            // How far the maps zooms in.
            zoom: 8,
            // Current Lat and Long position of the pin/
            center: new google.maps.LatLng( 21.484716, 39.189606 ),
            // center : {
            //  lat: -34.397,
            //  lng: 150.644
            // },
            disableDefaultUI: false, // Disables the controls like zoom control on the map if set to true
            scrollWheel: true, // If set to false disables the scrolling on the map.
            draggable: true, // If set to false , you cannot move the map around.
            // mapTypeId: google.maps.MapTypeId.HYBRID, // If set to HYBRID its between sat and ROADMAP, Can be set to SATELLITE as well.
            // maxZoom: 11, // Wont allow you to zoom more than this
            // minZoom: 9  // Wont allow you to go more up.
            styles:[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "color": "#50456d"
            },
            {
                "saturation": "29"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#b31f1f"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dfd1ae"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#f00000"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text",
        "stylers": [
            {
                "color": "#1a1526"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ff0f0f"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ebe3cd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "color": "#892020"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-84"
            },
            {
                "lightness": "85"
            },
            {
                "gamma": "0.00"
            },
            {
                "weight": "1"
            },
            {
                "color": "#50456d"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#f6c866"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e98d58"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
]
        };
    /**
     * Creates the map using google function google.maps.Map() by passing the id of canvas and
     * mapOptions object that we just created above as its parameters.
     *
     */
    // Create an object map with the constructor function Map()
        map = new google.maps.Map( element, mapOptions ); // Till this like of code it loads up the map.
    /**
     * Creates the marker on the map
     *
     */
        marker = new google.maps.Marker({
            position: mapOptions.center,
             icon: '<?php echo base_url(); ?>assets/frontend/images/pin.svg',
            label: {text: '1' , color: 'white'},
            map: map,
            draggable: true
        });
    /**
     * Creates a search box
     */
         searchBox = new google.maps.places.SearchBox( addressEl );
    /**
     * When the place is changed on search box, it takes the marker to the searched location.
     */
    google.maps.event.addListener( searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces(),
            bounds = new google.maps.LatLngBounds(),
            i, place, lat, long, resultArray,
            addresss = places[0].formatted_address;
        for( i = 0; place = places[i]; i++ ) {
            bounds.extend( place.geometry.location );
            marker.setPosition( place.geometry.location );  // Set marker position new.
        }
        map.fitBounds( bounds );  // Fit to the bound
        map.setZoom( 15 ); // This function sets the zoom to 15, meaning zooms to level 15.
        // console.log( map.getZoom() );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        latEl.value = lat;
        longEl.value = long;
        resultArray =  places[0].address_components;
        // Get the city and set the city input value to the one selected
        for( var i = 0; i < resultArray.length; i++ ) {
            if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                citi = resultArray[ i ].long_name;
                city.value = citi;
            }
        }
        // Closes the previous info window if it already exists
        if ( infoWindow ) {
            infoWindow.close();
        }
        /**
         * Creates the info Window at the top of the marker
         */
        infoWindow = new google.maps.InfoWindow({
            content: addresss
        });
        infoWindow.open( map, marker );
    } );
    /**
     * Finds the new position of the marker when the marker is dragged.
     */
    google.maps.event.addListener( marker, "dragend", function ( event ) {
        var lat, long, address, resultArray, citi;
        console.log( 'i am dragged' );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { latLng: marker.getPosition() }, function ( result, status ) {
            if ( 'OK' === status ) {  // This line can also be written like if ( status == google.maps.GeocoderStatus.OK ) {
                address = result[0].formatted_address;
                resultArray =  result[0].address_components;
                // Get the city and set the city input value to the one selected
                for( var i = 0; i < resultArray.length; i++ ) {
                    if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                        citi = resultArray[ i ].long_name;
                        console.log( citi );
                        city.value = citi;
                    }
                }
                addressEl.value = address;
                latEl.value = lat;
                longEl.value = long;
            } else {
                console.log( 'Geocode was not successful for the following reason: ' + status );
            }
            // Closes the previous info window if it already exists
            if ( infoWindow ) {
                infoWindow.close();
            }
            /**
             * Creates the info Window at the top of the marker
             */
            infoWindow = new google.maps.InfoWindow({
                content: address
            });
            infoWindow.open( map, marker );
        } );
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<!--<script>
    var my_lat = 21.484716;
    var my_lng = 39.189606;
    function initialize() {
        var myLatlng = new google.maps.LatLng(my_lat, my_lng);
        var mapProp = {
            center: myLatlng,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Drag me!',
            draggable: true
        });
        document.getElementById('lat').value = my_lat;
        document.getElementById('lng').value = my_lng;
        // marker drag event
        google.maps.event.addListener(marker, 'drag', function (event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lng').value = event.latLng.lng();
        });
        //marker drag event end
        google.maps.event.addListener(marker, 'dragend', function (event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lng').value = event.latLng.lng();
            // alert("lat=>" + event.latLng.lat());
            // alert("long=>" + event.latLng.lng());
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>-->