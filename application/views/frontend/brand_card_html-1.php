<?php foreach ($brands as $key => $brand){ 
    $bg_color = array(9,10,11,12,13,14);
    $keys = array_rand($bg_color);
    ?>
<div class="card-2 bg-<?php echo $bg_color[$keys] ?> wow animate__animated animate__fadeInUp" data-wow-delay=".1s">
    <figure class="img-hover-scale overflow-hidden">
        <a href="shop-grid-right.html">
            <?php if(file_exists($brand->Image) &&  $brand->Image != ''){

                            $brand_image = base_url($brand->Image);

                    }else{
                        $brand_image =    base_url().'assets/frontend/imgs/shop/product-1-1.jpg';

                    } ?>
        <img src="<?php echo $brand_image; ?>" alt="" /></a>
    </figure>
    <h6><a href="#"><?php echo $brand->Title; ?></a></h6>
    <span><?php echo $brand->TotalProducts; ?></span>
</div>
<?php } ?>