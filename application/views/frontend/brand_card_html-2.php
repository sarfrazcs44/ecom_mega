<?php foreach ($brands as $key => $brand){ ?>
<div class="card-1">
    <figure class="img-hover-scale overflow-hidden">
        <a href="shop-grid-right.html">
            <?php if(file_exists($brand->Image) &&  $brand->Image != ''){

                            $brand_image = base_url($brand->Image);

                    }else{
                        $brand_image =    base_url().'assets/frontend/imgs/theme/icons/category-1.svg';

                    } ?>
            <img src="<?php echo $brand_image; ?>" alt="" /></a>
    </figure>
    <h6>
        <a href="shop-grid-right.html"><?php echo $brand->Title; ?></a>
    </h6>
</div>
<?php } ?>