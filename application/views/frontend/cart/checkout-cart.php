 <main class="main">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="<?php echo base_url(); ?>" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                    <span></span> Shop
                    <span></span> Cart
                </div>
            </div>
        </div>
        <div class="container mb-80 mt-50">
            <div class="row">
                <div class="col-lg-8 mb-40">
                    <h1 class="heading-2 mb-10">Your Cart</h1>
                    <div class="d-flex justify-content-between">
                        <h6 class="text-body">There are <span class="text-brand"><?php echo $user_total_product ?></span> products in your cart</h6>
                        <h6 class="text-body"><a href="javascript:void(0);" onclick="clearCart();" class="text-muted"><i class="fi-rs-trash mr-5"></i>Clear Cart</a></h6>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="table-responsive shopping-summery">
                        <table class="table table-wishlist">
                            <thead>
                                <tr class="main-heading">
                                    
                                    <th class="start pl-30" scope="col" colspan="2">Product</th>
                                    <th scope="col">Unit Price</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Subtotal</th>
                                    <th scope="col" class="end">Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                   
                                    $total = 0;
                                    foreach ($cart_items as $cart_item) { 
                                        $total += $cart_item->TempItemPrice * $cart_item->Quantity;

                                        if(file_exists($cart_item->ImageName) &&  $cart_item->ImageName != ''){

                                            $product_image = base_url($cart_item->ImageName);

                                    }else{
                                        $product_image =    base_url().'assets/frontend/imgs/shop/product-1-1.jpg';

                                    } 
                                    ?>
                                <tr class="pt-30" id="TempOrderID<?php echo $cart_item->TempOrderID ?>">
                                    
                                    <td class="image product-thumbnail pt-40"><img src="<?php echo $product_image ?>" alt="#"></td>
                                    <td class="product-des product-name">
                                        <h6 class="mb-5"><a class="product-name mb-10 text-heading" href="shop-product-right.html"><?php echo $cart_item->Title; ?></a></h6>
                                        <div class="product-rate-cover">
                                            <div class="product-rate d-inline-block">
                                                <div class="product-rating" style="width:<?php echo $cart_item->Rating* 100/5;?>%">
                                                </div>
                                            </div>
                                            <span class="font-small ml-5 text-muted"> (<?php echo number_format($cart_item->Rating,1); ?>)</span>
                                        </div>
                                    </td>
                                    <td class="price" data-title="Price">
                                        <h4 class="text-body"><?php echo number_format($cart_item->Price,2); ?> </h4>
                                    </td>
                                    <td class="text-center detail-info" data-title="Stock">
                                        <div class="detail-extralink mr-15">
                                            <div class="detail-qty border radius">
                                                <a href="javascript:void();" class="qty-down cart_quantity" data-temp_order_id="<?php echo $cart_item->TempOrderID; ?>" data-item_price="<?php echo $cart_item->Price; ?>"><i class="fi-rs-angle-small-down"></i></a>
                                                <span class="qty-val qty-val-<?php echo $cart_item->TempOrderID; ?>"><?php echo $cart_item->Quantity; ?></span>
                                                <a href="javascript:void();" class="qty-up cart_quantity" data-temp_order_id="<?php echo $cart_item->TempOrderID; ?>" data-item_price="<?php echo $cart_item->Price; ?>"><i class="fi-rs-angle-small-up"></i></a>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="price" data-title="Price">
                                        <h4 class="text-brand" id="TotalPrice_<?php echo $cart_item->TempOrderID ?>"><?php echo number_format(($cart_item->Price * $cart_item->Quantity),2); ?> </h4>
                                    </td>
                                    <td class="action text-center" data-title="Remove"><a href="javascript:void(0);" class="text-body" onclick="removeIt('cart/removeFromCart', 'TempOrderID', <?php echo $cart_item->TempOrderID ?>);"><i class="fi-rs-trash"></i></a></td>
                                </tr>
                            <?php } ?>
                                
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="divider-2 mb-30"></div>
                    <div class="cart-action d-flex justify-content-between">
                        <a href="<?php echo base_url(); ?>" class="btn "><i class="fi-rs-arrow-left mr-10"></i>Continue Shopping</a>
                        <!--<a class="btn  mr-10 mb-sm-15"><i class="fi-rs-refresh mr-10"></i>Update Cart</a>-->
                    </div>
                    <div class="row mt-50">
                        
                        <div class="col-lg-5">
                            <?php
                                    if ($this->session->userdata('order_coupon')) {
                                        $order_coupon = $this->session->userdata('order_coupon');
                                        $coupon_code = $order_coupon['CouponCode'];
                                        $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                                        $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total;
                                        $total = $total - $coupon_discount_availed;
                                        ?>
                                        <h4 class="mb-10">Promo Applied:</h4>
                                        <p class="mb-30"><span class="font-lg text-muted"><?php echo $coupon_code; ?> <a class="btn" href="javascript:void(0);" onclick="removeCoupon();">
                                                <button class="btn">Clear <i class="fa fa-remove"
                                                                             style="color: red !important;"></i>
                                                </button>
                                            </a></p>
                                        
                                            <a class="btn" href="javascript:void(0);" onclick="removeCoupon();">
                                                <button class="btn">Clear <i class="fa fa-remove"
                                                                             style="color: red !important;"></i>
                                                </button>
                                            </a>
                                       
                                        <p><span><?php echo lang('promo_discount');?> %:</span>
                                            <strong><?php echo $coupon_discount_percentage; ?></strong></p>
                                        <p><span><?php echo lang('promo_discount_availed'); ?>:</span>
                                            <strong><?php echo $coupon_discount_availed; ?> <?php echo lang('sar'); ?></strong></p>
                                    <?php } else { ?>
                            <div class="p-40">
                                <h4 class="mb-10">Apply Coupon</h4>
                                <p class="mb-30"><span class="font-lg text-muted">Using A Promo Code?</p>
                                <form action="<?php echo base_url('cart/applyCoupon'); ?>" method="post"
                                              class="couponApplyForm gift" id="couponForm">
                                    <div class="d-flex justify-content-between">
                                        <input class="font-medium mr-15 coupon" name="CouponCode" placeholder="Enter Your Coupon">
                                        <button class="btn" type="submit"><i class="fi-rs-label mr-10"></i>Apply</button>
                                    </div>
                                </form>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="border p-md-4 cart-totals ml-30">
                        <div class="table-responsive">
                            <table class="table no-border">
                                <tbody>
                                    <tr>
                                        <td class="cart_total_label">
                                            <h6 class="text-muted">Subtotal</h6>
                                        </td>
                                        <td class="cart_total_amount">
                                            <h4 class="text-brand text-end"><?php echo $total; ?></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="col" colspan="2">
                                            <div class="divider-2 mt-10 mb-10"></div>
                                        </td>
                                    </tr>
                                    <?php
                                        $shipping_amount = 0;
                                        $shipment_method = getTaxShipmentCharges('Shipment', true);
                                        if ($shipment_method) {
                                            $shipping_title = $shipment_method->Title;
                                            $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                                            if ($shipment_method->Type == 'Fixed') {
                                                $shipping_amount = $shipment_method->Amount;
                                            } elseif ($shipment_method->Type == 'Percentage') {
                                                $shipping_amount = ($shipment_method->Amount / 100) * ($total);
                                            }
                                            ?>
                                    <tr>
                                        <td class="cart_total_label">
                                            <h6 class="text-muted">Shipping</h6>
                                        </td>
                                        <td class="cart_total_amount">
                                            <h5 class="text-heading text-end"><?php echo number_format($shipping_amount, 2); ?></h5>
                                        </td>
                                        <td scope="col" colspan="2">
                                            <div class="divider-2 mt-10 mb-10"></div>
                                        </td>
                                    </tr>
                                    <?php 

                                    $total = $total + $shipping_amount;

                                }
                                        ?>
                                        <?php
                                        $total_tax = 0;
                                        $taxes = getTaxShipmentCharges('Tax');
                                        foreach ($taxes as $tax) {
                                            $tax_title = $tax->Title;
                                            $tax_factor = $tax->Type == 'Fixed' ? '' : $tax->Amount . '%';
                                            if ($tax->Type == 'Fixed') {
                                                $tax_amount = $tax->Amount;
                                            } elseif ($tax->Type == 'Percentage') {
                                               // $tax_amount = ($tax->Amount / 100) * ($total + $shipping_amount);
                                                 $tax_amount = ($tax->Amount / 100) * ($total);
                                            }
                                            ?>

                                            <tr>
                                                <td class="cart_total_label">
                                                    <h6 class="text-muted"><?php echo $tax_title; ?> <?php echo $tax_factor; ?></h6>
                                                </td>
                                                <td class="cart_total_amount">
                                                    <h5 class="text-heading text-end"><?php echo number_format($tax_amount, 2); ?></h5>
                                                </td>
                                                 <td scope="col" colspan="2">
                                                    <div class="divider-2 mt-10 mb-10"></div>
                                                </td>
                                                
                                            </tr>
                                            
                                            <?php
                                            $total_tax += $tax_amount;
                                        }

                                       // $total = $total + $shipping_amount + $total_tax;
                                        $total = $total + $total_tax;
                                        ?>
                                    <tr>
                                        <td class="cart_total_label">
                                            <h6 class="text-muted">Total</h6>
                                        </td>
                                        <td class="cart_total_amount">
                                            <h4 class="text-brand text-end"><?php echo number_format($total, 2); ?></h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <a href="javascript:void(0);" class="btn mb-20 w-100"  onclick="proceedToCheckout();">Proceed To CheckOut<i class="fi-rs-sign-out ml-15"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </main>