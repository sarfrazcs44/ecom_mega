<div class="cart-dropdown-wrap cart-dropdown-hm2">
    <ul>
        <?php 
        $total = 0;
        foreach ($cart_items as $key => $value) {

            $total += $value->Quantity * $value->Price;
            if(file_exists($value->ImageName) &&  $value->ImageName != ''){

                            $product_image = base_url($value->ImageName);

                    }else{
                        $product_image =    base_url().'assets/frontend/imgs/shop/thumbnail-3.jpg';

                    } 

         ?>
            <li>
                <div class="shopping-cart-img">
                    <a href="shop-product-right.html"><img alt="Nest" src="<?php echo $product_image; ?>" /></a>
                </div>
                <div class="shopping-cart-title">
                    <h4><a href="shop-product-right.html"><?php echo $value->Title ?></a></h4>
                    <h4><span><?php echo $value->Quantity ?> × </span><?php echo $value->Price ?></h4>
                </div>
                <div class="shopping-cart-delete">
                    <a href="#"><i class="fi-rs-cross-small"></i></a>
                </div>
            </li>
           
        <?php } ?>
        
       
    </ul>
    <div class="shopping-cart-footer">
        <div class="shopping-cart-total">
            <h4>Total <span><?php echo $total ?></span></h4>
        </div>
        <div class="shopping-cart-button">
            <a href="<?php echo base_url('cart'); ?>" class="outline">View cart</a>
            <a href="<?php echo base_url('address'); ?>">Checkout</a>
        </div>
    </div>
</div>