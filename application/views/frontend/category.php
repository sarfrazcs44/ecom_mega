<style>

</style>
<div class="container">
    	<div class="content">
    		<div class="row">
    		  <div class="col-md-12">	
    			<h2 class="ramadan-head"><?php echo categoryName($CategoryID,$language); ?></h2>
    		  </div>	
    		</div>
    		<div class="row">
                <?php if($categories){ 
                    foreach($categories as $key => $value){
                        if($key % 2 == 0 && $key != 0){
                            echo '</div>';
                            echo '<div class="row">';

                        }
                     ?>
					 <a href="<?php echo base_url('product');?>?q=<?php echo strtolower(str_replace(' ','-',$value->Title)); ?><?php echo '-s'.$value->CategoryID;?>">
						<div class="col-md-6">
							<div class="row first d-flex applyBgImage" style="background-image:url('<?php echo base_url($value->Image); ?>')">
								<div class="col-md-6 txt-cl align-self-end">
									<div class="choco-txt">
									<h3><?php echo $value->Title; ?></h3>
									<p><?php echo $value->Description; ?></p>
									</div>
								</div>
								<div class="col-md-6 img-cl">
									<img src="<?php echo base_url($value->Image); ?>" alt="ecommerce" class="img-fluid choc-img">
								</div>
							</div>
						</div>
					</a>
            <?php } } ?>
    		</div>
    	</div>
	</div>