
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4"></script>
        <style>
.content.checkout.address .wbox {margin-bottom:30px;}
.mb-0 {margin-bottom:0px !important;}
.h-100 {height:100%;}
.editbox.alwaysBeThere .bbox {background-color: #D3E6CF !important;}
.content.checkout.address .wbox .bbox {border-radius: 5px;}/** margin-bottom:34px !important; **/
.content.checkout .wbox .row.no-border {border:0px; margin-bottom:0px;padding-bottom:0px}
.content.checkout .wbox.side ol.chkOutAdd li {margin: 0;color: #bd9371;text-align: center;}
.content.checkout .wbox.side ol.chkOutAdd li span {
    float: none;
    color: inherit;
    display: inline-block;
    vertical-align: middle;
    font-size: 18px;
    margin: 0;
    font-weight:normal;
}
.content.checkout .wbox.side ol.chkOutAdd li strong {
    font-weight: normal;
    font-size: 18px;
}
.content.checkout.address .wbox .btn.ccheckout {margin-top:0px;}
label.customradio.default {
    font-weight: 900;
    padding: <?php echo($lang == 'AR' ? ' 0px 35px 0px 0px ' : ' 0px 0px 0px 35px'); ?>;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    line-height: 25px; */
}
label.customradio.default .checkmark {
    border:1px solid rgb(153,153,153);
}
.shipMethodTxt h5 strong {
    display: block;
    font-size: 18px;
    font-weight: bold;
}
.shipMethodTxt h5 {
    line-height: 1.1 !important;
}
.plusNewBox a {
    display: BLOCK;
    width: 100%;
    height: 100%;
    border: 1px dashed #86A57F;
    border-radius: 5px;
    background-color: #fff;
    position: relative;
    font-size: 0;
}
.plusNewBox a:before {
    content: '';
    width: 100px;
    height: 100px;
    background: #CE8D8D 0% 0% no-repeat padding-box;
    box-shadow: 0px 3px 6px #00000029;
    display: block;
    border-radius: 50%;
    margin: auto;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
}
.plusNewBox a:after {
    background: transparent;
    content: '+';
    width: 40px;
    height: 40px;
    display: block;
    margin: auto;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    font-size: 65px;
    color: #fff;
    text-shadow: 0px 3px 6px #00000029;
    text-align: center;
    line-height: 36px;
}
.customradio .checkmark:after {
    top: 7.5px;
    left: 8px;
}
.content.checkout.address .wbox .bbox h5 {
    font-size: 18px;
    margin: 0;
    line-height: 1.5;
    font-weight: normal;
    margin: 10px 0 8px;
    word-break: break-word;
}
.content.checkout.address .wbox .edImgBox {
    display: block;
    width: 100%;
    padding: 15px 0;
}
.content.checkout.address .wbox .edImgBox img {
    max-width: 100%;
    height: auto;
}
.content.checkout.address .wbox .bbox h5 a {
    color: #0000004E;
    margin-top: 4px;
    display: inline-block;
}
.collOptRow_f_Wrap > div {
    margin-bottom: 25px;
}
.collOptRow_f_Wrap {
    flex-wrap: wrap;
    margin-bottom: -25px !important;
}
.col-md-4.editbox.plusNewBox:nth-child(n+4) {
    display: none;
}
.mod-cnt {
    padding: 2% 6%;
    border-radius: 5px;
  }
  .modW2 {
    max-width: 1104px;
    max-height: 1176px;
}
  .mod_hdd {
    border: none;
  }
  .mod_ftt {
    border: none;
  }
  
.chcimg {
  width: 60px;
  height: 60px;
  border-radius: 17px;
}
.title {
  margin-bottom: 5%;
  margin-top: 3%;
}
h3.scnd-title {
    color: #50456D !important;
    font-size: 30px !important;
    position: relative;
    bottom: 14px;
}
h1.hed-title {
    color: #CE8D8D;
    font-size: 36px;
}  
.txt-detail p.chc-nam {
    color: #000000 !important;
    font-size: 17px !important;
}
.crs-btn.btn-dark {
    background-color: #CE8D8D;
    width: 35px;
    height: 35px;
    border-radius: 50px;
    border: none;
}
button.btn-light.crt-btn {
    width: 100%;
    height: 46px;
    background-color: #F4EBD3 !important;
    color: #000;
    border: none;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.161);
    border-radius: 7px;
    font-size: 16px;
    margin-bottom: 30px;
    margin-top: 7%;
}
.cat-choc .txt-price {
    /* position: relative; */
    display: block;
    width: 100%;
    padding-right: 40px;
}
.cat-choc .txt-price p,
.cat-choc .txt-detail p.chc-nam {
    color: #000 !important;
}
.cat-choc .txt-price p span.green,
.cat-choc .txt-detail p.chc-nam span.green {color: #21AD00 !important;}
.cat-choc .txt-price p span.red,
.cat-choc .txt-detail p.chc-nam span.red {color: #C30000 !important;-webkit-text-decoration-line: line-through; /* Safari */text-decoration-line: line-through; }
.cat-choc .txt-price button.crs-btn.btn-dark {
    position: absolute;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.161);
    color: #fff;
    text-shadow: 0px 3px 6px #00000029;
}
.btn-info.pay-btn {
    background-color: #CE8D8D;
    color: #fff;
    width: 100%;
    height: 46px;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.161);
    border-radius: 7px;
    font-size: 16px;
    border: none;
    display: inline-block;
    line-height: 46px;
}
.cat-choc {
    border-bottom: 1px solid #E2E2E2;
    padding: 14px 0;
    margin: 0 15px;
}
.chc-nam {
  font-size: 18px;
}
.txt-price {
  font-size: 18px;
}
.clk-con {
    font-size: 16px;
    color: #1A1526;
    font-weight: normal;
    margin-top: 14px;
    margin-bottom: 15px;
    padding: 0;
}
.edt-crt {
  width: 100%;
}
.edt-pay {
  width: 100%;
}
</style>
<section class="content checkout address">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Shopping <span>Check Out</span></h2>
            </div>
            <div class="col-md-8">
                <div class="wbox mb-3">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Shipping Address</h6>
                            <div class="dropdown barownButton">
                                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li><a href="<?php echo base_url(); ?>/address/add">Add an Address</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row no-border d-flex align-items-stretch collOptRow_f_Wrap">
                        <div class="col-md-4  editbox alwaysBeThere">
                            <div class="bbox dropdown mb-0 h-100">
                                <label class="customradio default"><?php echo lang('Collect_From_Store'); ?>
                                    <input type="radio" name="collectionOption"
                                            class="collectFromStore" data-district-id="0" <?php echo(isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1 ? 'checked' : ''); ?>>
                                    <span class="checkmark"></span>
                                </label>
                                <div class="edImgBox">
                                    <img src="<?php echo front_assets(); ?>imgs/hand.svg" height="" width="" />
                                </div>
                                <h5>No Shipping Charges <br>Required for Pickup</h5>
                            </div>
                        </div>
                        <?php
                       
                        foreach ($addresses as $address) { ?>
                            <div class="col-md-4 editbox">
                                <div class="bbox dropdown mb-0 h-100">
                                    <label class="customradio default">Deliver here
                                        <input type="radio" name="collectionOption" class="addressCheckbox chk"
                                               value="<?php echo $address->AddressID; ?>" data-district-id="<?php echo $address->DistrictID; ?>" <?php echo !isset($_COOKIE['CollectFromStore']) && $address->IsDefault == 1 ? 'checked' : '' ?>>
                                        <span class="checkmark"></span>
                                    </label>
                                    <!--<label class="customcheck default">Collect payment here
                                        <input type="checkbox" class="CollectPaymentChk"
                                               value="<?php echo $address->AddressID; ?>" <?php echo !isset($_COOKIE['CollectFromStore']) && $address->UseForPaymentCollection == 1 ? 'checked' : '' ?>>
                                        <span class="checkmark"></span>
                                    </label>-->
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="javascript:void(0);"
                                               onclick="removeIt('address/delete', 'AddressID', <?php echo $address->AddressID ?>);">
                                                Remove this address
                                            </a>
                                        </li>
                                    </ul>
                                    <h5>
                                        <?php echo $address->RecipientName; ?>
                                        <span><?php echo $address->MobileNo; ?></span>
                                    </h5>
                                    <h5>
                                        <span><?php echo lang('email'); ?>: <?php echo $address->Email; ?></span>
                                        <span><?php echo lang('city'); ?>: <?php echo $address->CityTitle; ?></span>
                                        <span><?php echo lang('district'); ?>: <?php echo $address->DistrictTitle; ?></span>
                                       
                                        <span><?php echo lang('street'); ?>: <?php echo $address->Street; ?></span>
                                        <a href="https://maps.google.com/?q=<?php echo $address->Latitude; ?>,<?php echo $address->Longitude; ?>" target="_blank"><?php echo lang('google_pin_link'); ?></a>
                                    </h5>
                                </div>
                            </div>
                        <?php }
                        ?>
                         <div class="col-md-4 editbox plusNewBox">
                            <a href="<?php echo base_url(); ?>/address/add">+</a>
                        </div>
                    </div>
                </div>
                <div class="wbox mb-3">
                    <div class="row">
                        <div class="col-md-12">
                            <h6><span id="delivery" <?php echo((isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1) ? 'style="display: none;"' : 'style="display: inline;"'); ?>>Deliver</span><span id="collect" <?php echo((isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1) ? 'style="display: inline;"' : 'style="display: none;"'); ?>>Collect</span> From Branch</h6>
                        </div>
                    </div>
                    <div class="row no-border">
                        <div class="col-md-12">
                            <div class="form-group edSelectStyle white">
                                <label for="cities">Select City</label>
                                <select class="form-control border" name="citiess" onchange="selectBrach(this.value);">
                                    
                                    <?php 
                                    if(isset($_GET['city']) && $_GET['city'] !=  ''){
                                        echo '<option value="">Show All City</option>';
                                    }else{
                                        echo '<option value="">Select</option>';
                                    }
                                    if($available_cities){
                                        $check_array = array();
                                        foreach ($available_cities as $store){
                                            if(!in_array($store->CityID,$check_array)){
                                                echo '<option value="'.$store->CityID.'" '.((isset($_GET['city']) && $_GET['city'] == $store->CityID) ? 'selected' : '').'>'.$store->CityTitle.'</option>';
                                            }
                                            $check_array[] = $store->CityID;
                                            
                                        }
                                    }
                                    ?>
                                    
                                    
                                </select>
                            </div>
                            <div class="googleMap">
                                <div id="map" style="height:400px; width:100%;"></div>
                            </div>
                            <br>
                            <p id="selected_branch_html" style="<?php echo ($this->session->userdata('DeliveryStoreID') ? 'display: block;' : 'display: none;' );?>"><?php echo ($this->session->userdata('DeliveryStoreID') ? $this->session->userdata('DeliveryStoreTitle').' Selected' : '' );?> </p>
                        </div>
                    </div>
                </div>
                <div class="wbox shipment_method" id="shipment_method" <?php echo(isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1 ? 'style="display:none;"' : 'style="display:block;"'); ?>>
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Shipping Method</h6>
                        </div>
                    </div>
                    <div class="row no-border shipMethodTxt">
                        <?php
                                $shipment_methods = getTaxShipmentCharges('Shipment');
                                foreach ($shipment_methods as $shipment_method) { ?>
                            <div class="col-md-4 editbox">
                                <div class="bbox dropdown">
                                    <label class="customradio default"><?php echo $shipment_method->Title; ?>
                                       <input type="radio" name="shipment_method"
                                                   value="<?php echo $shipment_method->TaxShipmentChargesID; ?>" <?php echo($shipment_method->TaxShipmentChargesID == $this->session->userdata('ShipmentMethodIDForBooking') ? 'checked' : ''); ?>
                                                   onclick="changeShipmentMethod(this.value);">
                                        <span class="checkmark"></span>
                                    </label>
                                    <h5>
                                        <strong><?php echo $shipment_method->Description; ?></strong>
                                        
                                        <span class="light"><?php echo $shipment_method->Amount; ?> <?php echo lang('sar'); ?></span>
                                    </h5>
                                </div>
                            </div>
                             <?php }
                                    ?>
                            
                        
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="wbox side">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Basket</h6>
                        </div>
                    </div>
                    <h5 class="text-center" style="margin-bottom: 20px;">Order Details</h5>
                    <?php
                    $total = 0;
                    $product_name = '';
                    //print_rm($cart_items);
                    foreach ($cart_items as $itm => $cart_item) {
                        if($itm != 0){
                            $product_name .= ','.$cart_item->Title;
                        }else{
                            $product_name .= $cart_item->Title;
                        }
                        
                     ?>
                        <div class="row">
                            <div class="col-sm-12" style="padding-<?php echo($lang == 'AR' ? 'left' : 'right'); ?>: 90px;">
                                <?php
                                if ($cart_item->ItemType == 'Product') { ?>
                                    <img src="<?php echo base_url(get_images($cart_item->ProductID, 'product', false)); ?>">
                                    <h5><?php echo $cart_item->Title; ?>
                                        <?php if($cart_item->PriceType == 'kg'){ ?>
                                             <span><?php echo $cart_item->Quantity * 1000; ?> <?php echo ($language == 'AR' ? 'غرام' : 'Grams'); ?></span>
                                         <?php }else{ ?>
                                             <span><?php echo $cart_item->Quantity; ?>pc(s)</span>
                                        <?php  } ?>
                                    </h5>
                                <?php } elseif ($cart_item->ItemType == 'Customized Shape') { ?>
                                    <img src="<?php echo base_url($cart_item->CustomizedShapeImage); ?>">
                                    <h5>Customized Shape
                                        <?php if($cart_item->PriceType == 'kg'){ ?>
                                             <span><?php echo $cart_item->Quantity * 1000; ?> <?php echo ($language == 'AR' ? 'غرام' : 'Grams'); ?></span>
                                         <?php }else{ ?>
                                             <span><?php echo $cart_item->Quantity; ?>pc(s)</span>
                                        <?php  } ?>
                                       
                                    </h5>
                                <?php } else { ?>
                                <a href="javascript:void(0);" class="chocobox_detail" title="Click to view whats inside" data-box_id="<?php echo $cart_item->CustomizedBoxID; ?>" data-pids="<?php echo $cart_item->CustomizedOrderProductIDs; ?>" data-box_type="<?php echo $cart_item->ItemType; ?>">
                                    <img src="<?php echo front_assets("images/".$cart_item->ItemType.".png"); ?>">
                                    <h5><?php echo $cart_item->ItemType; ?>
                                     <?php if($cart_item->PriceType == 'kg'){ ?>
                                             <span><?php echo $cart_item->Quantity * 1000; ?> <?php echo ($language == 'AR' ? 'غرام' : 'Grams'); ?></span>
                                         <?php }else{ ?>
                                             <span><?php echo $cart_item->Quantity; ?>pcs</span>
                                        <?php  } ?>
                                    </h5>
                                </a>
                                <?php } ?>
                                <div class="price"><?php echo number_format($cart_item->TempItemPrice, 2); ?> <?php echo lang('sar'); ?></div>
                            </div>
                        </div>
                        <?php $total += $cart_item->TempItemPrice * $cart_item->Quantity;
                    }
                    ?>
                    <div class="row last">
                        <div class="col-md-12">
                            <?php
                            if ($this->session->userdata('order_coupon')) {
                                $order_coupon = $this->session->userdata('order_coupon');
                                $coupon_code = $order_coupon['CouponCode'];
                                $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                                $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total;
                                $total = $total - $coupon_discount_availed;
                                ?>
                                <p><span>Promo Applied:</span> <strong><?php echo $coupon_code; ?></strong></p>
                                <p><span><?php echo lang('promo_discount'); ?> %:</span>
                                    <strong><?php echo $coupon_discount_percentage; ?></strong></p>
                                <p><span><?php echo lang('promo_discount_availed'); ?>:</span> <strong><?php echo $coupon_discount_availed; ?>
                                        <?php echo lang('sar'); ?></strong></p>
                            <?php } ?>
                            <!--<h6>
                                <span>Shipping VAT 5%</span>
                                <strong>20.00 sar 7.00 sar</strong>
                            </h6>-->
                            <ol class="chkOutAdd">
                                <?php
                                $shipping_amount = 0;
                                if(!isset($_COOKIE['CollectFromStore'])){
                                
                                if($this->session->userdata('ShipmentMethodIDForBooking')){
                                     $shipment_method = getTaxShipmentChargesByID('Shipment', $this->session->userdata('ShipmentMethodIDForBooking'));
                                 }else{
                                     $shipment_method = getTaxShipmentCharges('Shipment', true);
                                 }
                                //$shipment_method = getTaxShipmentCharges('Shipment', true);
                                if ($shipment_method) {
                                    $shipping_title = $shipment_method->Title;
                                    $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                                    if ($shipment_method->Type == 'Fixed') {
                                        $shipping_amount = $shipment_method->Amount;
                                    } elseif ($shipment_method->Type == 'Percentage') {
                                        $shipping_amount = ($shipment_method->Amount / 100) * ($total);
                                    }
                                    ?>
                                    <li>
                                                <span><!-- <i class="fa fa-truck" aria-hidden="true"></i> --> <?php echo $shipping_title; ?></span>
                                        <strong id="ShippingAmount"><?php echo number_format($shipping_amount, 2); ?>
                                            <?php echo lang('sar'); ?></strong>
                                    </li>
                                <?php }
                            }
                                ?>
                                <?php
                                $total_tax = 0;
                                $taxes = getTaxShipmentCharges('Tax');
                                foreach ($taxes as $tax) {
                                    $tax_title = $tax->Title;
                                    $tax_factor = $tax->Type == 'Fixed' ? number_format($tax->Amount, 2) . ' SAR' : $tax->Amount . '%';
                                    if ($tax->Type == 'Fixed') {
                                        $tax_amount = $tax->Amount;
                                    } elseif ($tax->Type == 'Percentage') {
                                        //$tax_amount = ($tax->Amount / 100) * ($total + $shipping_amount);
                                        $tax_amount = ($tax->Amount / 100) * ($total);
                                    }
                                    ?>
                                    <li>
                                                <span><!-- <i class="fa fa-file-text-o" aria-hidden="true"></i> --> <?php echo $tax_title; ?> <?php echo $tax_factor; ?></span>
                                        <strong id="TaxAmount"><?php echo number_format($tax_amount, 2); ?> <?php echo lang('sar'); ?></strong>
                                    </li>
                                    <?php
                                    $total_tax += $tax_amount;
                                }
                                $total = $total + $shipping_amount + $total_tax;
                                ?>
                            </ol>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-secondary">
                                <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($total, 2); ?> <?php echo lang('sar'); ?></strong>
                            </button>
                        </div>
                    </div>
                    <div class="row no-border shipMethodTxt">
                        
                        <label class="customcheck" style="border: none !important;">
                                        I accept the <a href="javascript:void(0)" data-toggle="modal" data-target="#TermsConditions">Terms and conditions</a>
                                        <input type="checkbox" class="acceptTerms">
                                        <span class="checkmark"></span>
                                    </label> 
                        <div class="col-md-12">
                            <a href="javascript:void(0);" class="btn ccheckout btn-primary"  onclick="placeOrder();" >Continue to
                                Payment</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="chkContShipping" tabindex="1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">
  <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
  <div class="modal-dialog modal-dialog-centered modWl" role="document">
    <div class="modal-content mod-cont <?php echo($lang == 'AR' ? 'text-right' : 'text-left'); ?>">
        <div class="modal-header mod_hdd">
            
            <div class="title">
            <h1 class="hed-title">Attention!</h1>
            <h3 class="scnd-title">Some of the Items are not available in this Branch</h3>
            </div>
        </div>
        <div class="modal-body" id="chkContShippingBody">
            
        </div>
    <div class="modal-footer mod_ftt">
        
        <div class="edt-crt text-center">
            <a href="<?php echo base_url('cart');?>"> <button  class="btn-light crt-btn">Edit Cart</button></a>
        </div>
        
        <div class="edt-pay text-center">  
            <a href="javascript:void();" class="btn-info pay-btn" onclick="placeOrder();">Continue to Payment</a>
            <p class="clk-con text-center">By Clicking continue, you will receive the proposed quantity and sold-out items will be discarded</p>
        </div>
    </div>
     
    </div>
  </div>
</div>
<input type="hidden"  id="StoreID" value="<?php echo ($this->session->userdata('DeliveryStoreID') ? $this->session->userdata('DeliveryStoreID') : ''); ?>">
<script>
    $(document).ready(function(){
            $('.collectFromStore').on('click',function(){
                $('#collect').show();
                //$('.shipment_method').hide();
                $("input:radio[name='shipment_method']").each(function(i) {
                       this.checked = false;
                });
               // $("#shipment_method").css("pointer-events","none");
                //$('#delivery').hide();
                $('#shipment_method').hide();
                
                
            });
            $('.addressCheckbox').on('click',function(){
                
                $('#collect').hide();
               // $('.shipment_method').show();
               //$("#shipment_method").css("pointer-events","auto");
                 $('#delivery').show();
                $('#shipment_method').show();
            });
            <?php if(isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1 ){ ?>
                    $("#shipment_method").css("pointer-events","none");
                    $("input:radio[name='shipment_method']").each(function(i) {
                       this.checked = false;
                    });
            <?php } ?>
    });
    function selectBrach(city_id){
        if(city_id == ''){
             window.location.replace("<?php echo base_url();?>address");
        }else{
             window.location.replace("<?php echo base_url();?>address?city="+city_id);
        }
       // window.location.replace("<?php echo base_url();?>address?city="+city_id);
    }
    var markers = [];
    var center_lat = <?php echo(isset($available_cities[0]) ? $available_cities[0]->Latitude : 21.484716); ?>;
    var center_lng = <?php echo(isset($available_cities[0]) ? $available_cities[0]->Longitude : 39.189606); ?>;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: new google.maps.LatLng(center_lat, center_lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles:[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "color": "#50456d"
            },
            {
                "saturation": "29"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#b31f1f"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dfd1ae"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#f00000"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text",
        "stylers": [
            {
                "color": "#1a1526"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ff0f0f"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ebe3cd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "color": "#892020"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-84"
            },
            {
                "lightness": "85"
            },
            {
                "gamma": "0.00"
            },
            {
                "weight": "1"
            },
            {
                "color": "#50456d"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#f6c866"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e98d58"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
]
    });
    <?php if ($available_cities && count($available_cities) > 0)
    { ?>
    var infowindow = new google.maps.InfoWindow();
    var marker;
    <?php
    $i = 0;
    foreach ($available_cities as $store)
    { ?>
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(<?php echo $store->Latitude; ?>, <?php echo $store->Longitude; ?>),
         icon: '<?php echo base_url(); ?>assets/frontend/imgs/pin.svg',
         label: {text: '<?php echo $i+1; ?>' , color: 'white'},
        map: map
    });
    google.maps.event.addListener(marker, 'click', (function (marker) {
        return function () {
            infowindow.setContent('<h5 style="color:black;"><?php echo $store->StoreTitle; ?></h5>');
            infowindow.open(map, marker);
            var district = '<?php echo $store->DistrictID; ?>';
            var district_list = district.split(',');
            var address_district = $('input[name="collectionOption"]:checked').attr('data-district-id');
            if(address_district > 0){
                if(jQuery.inArray(address_district, district_list) != -1) {
                    //console.log(district_list);
                    //console.log(address_district);
                   $('#selected_branch_html').html('<?php echo $store->StoreTitle; ?>'+' Selected');
                   $('#selected_branch_html').show();
                   $('#StoreID').val(<?php echo $store->StoreID; ?>);
                   changeDeliveryStoreID(<?php echo $store->StoreID; ?>,'<?php echo $store->StoreTitle; ?>');
                } else {
                    showMessage('This branch is not delivered in your district please select other branch.','danger');
                    $('#selected_branch_html').html('This branch is not delivered in your district please select other branch.');
                    $('#selected_branch_html').show();
                    $('#StoreID').val('');
                    unsetDeliveryStoreID();
                    //return false;
                }
            }else{
                $('#selected_branch_html').html('<?php echo $store->StoreTitle; ?>'+' Selected');
                $('#selected_branch_html').show();
                $('#StoreID').val(<?php echo $store->StoreID; ?>);
                changeDeliveryStoreID(<?php echo $store->StoreID; ?>,'<?php echo $store->StoreTitle; ?>');
            }
            
             
           // $('#selected_branch_html').html('<?php echo $store->StoreTitle; ?>'+' Selected');
            //$('#selected_branch_html').show();
            //$('#StoreID').val(<?php echo $store->StoreID; ?>);
        }
    })(marker));
    markers.push(marker);
    <?php $i++; } ?>
    <?php } ?>
    function myClick(index) {
        google.maps.event.trigger(markers[index], 'click');
    }
    // Code to disable browser back button
   /* $(document).ready(function () {
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            window.history.pushState(null, "", window.location.href);
        };
    });
// In the following example, markers appear when the user clicks on the map.
      // Each marker is labeled with a single alphabetical character.
      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var labelIndex = 0;
      function initialize(){
        var bangalore = { lat: 12.97, lng: 77.59 };
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: bangalore
        });
        // This event listener calls addMarker() when the map is clicked.
        google.maps.event.addListener(map, 'click', function(event) {
          addMarker(event.latLng, map);
        });
        // Add a marker at the center of the map.
        addMarker(bangalore, map);
      }
      // Adds a marker to the map.
      function addMarker(location, map) {
        // Add the marker at the clicked location, and add the next-available label
        // from the array of alphabetical characters.
        var marker = new google.maps.Marker({
            icon: '<?php echo base_url(); ?>assets/frontend/imgs/pin.svg',
          position: location,
          label: '1',
          map: map
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);*/
</script>