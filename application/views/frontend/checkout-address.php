<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4"></script>
<main class="main">
    <div class="page-header breadcrumb-wrap">
        <div class="container">
            <div class="breadcrumb">
                <a href="index.html" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                <span></span> Shop
                <span></span> Checkout
            </div>
        </div>
    </div>
    <div class="container mb-80 mt-50">
        <div class="row">
            <div class="col-lg-8 mb-40">
                <h1 class="heading-2 mb-10">Checkout</h1>
                <div class="d-flex justify-content-between">
                    <h6 class="text-body">There are <span class="text-brand">3</span> products in your cart</h6>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7">
                <div class="row mb-50">
                   <div class="col-md-4 col-12 col-sm-6">
                            <div class="product-cart-wrap mb-30">
                                <div class="product-content-wrap mt-20">
                                
                                    <input id="collectionOption0" type="radio" name="collectionOption"
                                            class="collectFromStore form-check-input" data-district-id="0" <?php echo(isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1 ? 'checked' : ''); ?>>
                                    <span class="checkmark"></span>
                                    <label class="form-check-label" for="collectionOption0"><?php echo lang('Collect_From_Store'); ?></label>
                               
                                <div class="edImgBox">
                                    <img src="<?php echo base_url(); ?>assets/frontend/imgs/hand.svg" height="" width="" />
                                </div>
                                <p>No Shipping Charges <br>Required for Pickup</p>
                            </div>
                        </div>
                    </div>
                        <?php
                       
                        foreach ($addresses as $address) { ?>
                            <div class="col-md-4 col-12 col-sm-6">
                                <div class="product-cart-wrap mb-30">
                                    <div class="product-content-wrap mt-20">
                                        
                                            <input id="collectionOption1" type="radio" name="collectionOption" class="addressCheckbox chk form-check-input"
                                                   value="<?php echo $address->AddressID; ?>" data-district-id="<?php echo $address->DistrictID; ?>" <?php echo !isset($_COOKIE['CollectFromStore']) && $address->IsDefault == 1 ? 'checked' : '' ?>>
                                            <span class="checkmark"></span>
                                            <label class="form-check-label" for="collectionOption1">Deliver here</label>
                                        
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="javascript:void(0);"
                                                   onclick="removeIt('address/delete', 'AddressID', <?php echo $address->AddressID ?>);">
                                                    Remove this address
                                                </a>
                                            </li>
                                        </ul>
                                        <p>
                                            <?php echo $address->RecipientName; ?>
                                            <span><?php echo $address->MobileNo; ?></span>
                                        </p>
                                        <p>
                                            <span><?php echo lang('email'); ?>: <?php echo $address->Email; ?></span>
                                            <span><?php echo lang('city'); ?>: <?php echo $address->CityTitle; ?></span>
                                            <span><?php echo lang('district'); ?>: <?php echo $address->DistrictTitle; ?></span>
                                           
                                            <span><?php echo lang('street'); ?>: <?php echo $address->Street; ?></span>
                                            <a href="https://maps.google.com/?q=<?php echo $address->Latitude; ?>,<?php echo $address->Longitude; ?>" target="_blank"><?php echo lang('google_pin_link'); ?></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        ?>
                        
                </div>
                <div class="row">
                    <div class="row no-border">
                        <div class="col-md-12">
                            <div class="form-group edSelectStyle white">
                                <label for="cities">Select City</label>
                                <select class="form-control border" name="citiess" onchange="selectBrach(this.value);">
                                    
                                    <?php 
                                    if(isset($_GET['city']) && $_GET['city'] !=  ''){
                                        echo '<option value="">Show All City</option>';
                                    }else{
                                        echo '<option value="">Select</option>';
                                    }
                                    if($available_cities){
                                        $check_array = array();
                                        foreach ($available_cities as $store){
                                            if(!in_array($store->CityID,$check_array)){
                                                echo '<option value="'.$store->CityID.'" '.((isset($_GET['city']) && $_GET['city'] == $store->CityID) ? 'selected' : '').'>'.$store->CityTitle.'</option>';
                                            }
                                            $check_array[] = $store->CityID;
                                            
                                        }
                                    }
                                    ?>
                                    
                                    
                                </select>
                            </div>
                            <div class="googleMap">
                                <div id="map" style="height:400px; width:100%;"></div>
                            </div>
                            <br>
                            <p id="selected_branch_html" style="<?php echo ($this->session->userdata('DeliveryStoreID') ? 'display: block;' : 'display: none;' );?>"><?php echo ($this->session->userdata('DeliveryStoreID') ? $this->session->userdata('DeliveryStoreTitle').' Selected' : '' );?> </p>
                        </div>
                    </div>
                    <div class="wbox shipment_method border-radius-15 border" id="shipment_method" <?php echo(isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1 ? 'style="display:none;"' : 'style="display:block;"'); ?>>
                            <div class="payment ml-30">
                                <h4 class="mb-30 mt-20">Shipping Method</h4>
                                <div class="row no-border shipMethodTxt">
                                <?php
                                        $shipment_methods = getTaxShipmentCharges('Shipment');
                                        foreach ($shipment_methods as $shipment_method) { ?>
                                    <div class="col-md-4 product-cart-wrap mb-30 mr-20">

                                        <div class="bbox dropdown mb-10 mt-10">
                                               
                                               <input class="form-check-input" type="radio" name="shipment_method"
                                                           value="<?php echo $shipment_method->TaxShipmentChargesID; ?>" <?php echo($shipment_method->TaxShipmentChargesID == $this->session->userdata('ShipmentMethodIDForBooking') ? 'checked' : ''); ?>
                                                           onclick="changeShipmentMethod(this.value);">
                                                
                                               
                                                <label class="form-check-label"><h5><?php echo $shipment_method->Title; ?></h5></label>
                                            
                                            <p>
                                                <strong><?php echo $shipment_method->Description; ?></strong><br>
                                                
                                                <span class="light"><?php echo $shipment_method->Amount; ?> <?php echo lang('sar'); ?></span>
                                            </p>
                                        </div>
                                    </div>
                                     <?php }
                                            ?>
                                    
                                
                                </div>
                            </div>
                        </div>
                </div>

                
            </div>
            <div class="col-lg-5">
                <div class="border p-40 cart-totals ml-30 mb-50">
                    <div class="d-flex align-items-end justify-content-between mb-30">
                        <h4>Your Order</h4>
                        <h6 class="text-muted">Subtotal</h6>
                    </div>
                    <div class="divider-2 mb-30"></div>
                    <div class="table-responsive order_table checkout">
                        <table class="table no-border">
                            <tbody>
                                <?php
                                $total = 0;
                                $product_name = '';
                                //print_rm($cart_items);
                                foreach ($cart_items as $itm => $cart_item) {
                                    if($itm != 0){
                                        $product_name .= ','.$cart_item->Title;
                                    }else{
                                        $product_name .= $cart_item->Title;
                                    }
                                    
                                 ?>
                                <tr>
                                    <?php
                                    if ($cart_item->ItemType == 'Product') { 
                                        if(file_exists($cart_item->ImageName) &&  $cart_item->ImageName != '') { 
                                            $product_image = base_url($cart_item->ImageName);
                                        } else {
                                            $product_image = base_url().'assets/frontend/imgs/shop/product-1-1.jpg';
                                        }
                                            ?>
                                        
                                    <td class="image product-thumbnail"><img src="<?php echo $product_image; ?>" alt="#"></td>
                                    <td>
                                        <h6 class="w-160 mb-5"><a href="shop-product-full.html" class="text-heading"><?php echo $cart_item->Title; ?></a>
                                        
                                    </td>
                                    <td>
                                        <h6 class="text-muted pl-20 pr-20">
                                             <span>x<?php echo $cart_item->Quantity; ?></span>
                                        
                                        </h6></span></h6>
                                    </td>
                                    <td>
                                        <h4 class="text-brand"><?php echo number_format($cart_item->TempItemPrice, 2); ?> <?php echo lang('sar'); ?></h4>
                                    </td>
                                </tr>
                                
                                
                            <?php $total += $cart_item->TempItemPrice * $cart_item->Quantity;
                            } 
                        } ?>
                            </tbody>
                        </table>

                    </div>
                    <div class="row last">
                        <div class="col-md-12">
                            <?php
                            if ($this->session->userdata('order_coupon')) {
                                $order_coupon = $this->session->userdata('order_coupon');
                                $coupon_code = $order_coupon['CouponCode'];
                                $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                                $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total;
                                $total = $total - $coupon_discount_availed;
                                ?>
                                <p><span>Promo Applied:</span> <strong><?php echo $coupon_code; ?></strong></p>
                                <p><span><?php echo lang('promo_discount'); ?> %:</span>
                                    <strong><?php echo $coupon_discount_percentage; ?></strong></p>
                                <p><span><?php echo lang('promo_discount_availed'); ?>:</span> <strong><?php echo $coupon_discount_availed; ?>
                                        <?php echo lang('sar'); ?></strong></p>
                            <?php } ?>
                            <!--<h6>
                                <span>Shipping VAT 5%</span>
                                <strong>20.00 sar 7.00 sar</strong>
                            </h6>-->
                            <ol class="chkOutAdd">
                                <?php
                                $shipping_amount = 0;
                                if(!isset($_COOKIE['CollectFromStore'])){
                                
                                if($this->session->userdata('ShipmentMethodIDForBooking')){
                                     $shipment_method = getTaxShipmentChargesByID('Shipment', $this->session->userdata('ShipmentMethodIDForBooking'));
                                 }else{
                                     $shipment_method = getTaxShipmentCharges('Shipment', true);
                                 }
                                //$shipment_method = getTaxShipmentCharges('Shipment', true);
                                if ($shipment_method) {
                                    $shipping_title = $shipment_method->Title;
                                    $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                                    if ($shipment_method->Type == 'Fixed') {
                                        $shipping_amount = $shipment_method->Amount;
                                    } elseif ($shipment_method->Type == 'Percentage') {
                                        $shipping_amount = ($shipment_method->Amount / 100) * ($total);
                                    }
                                    ?>
                                    <li>
                                                <span><!-- <i class="fa fa-truck" aria-hidden="true"></i> --> <?php echo $shipping_title; ?></span>
                                        <strong id="ShippingAmount"><?php echo number_format($shipping_amount, 2); ?>
                                            <?php echo lang('sar'); ?></strong>
                                    </li>
                                <?php }
                            }
                                ?>
                                <?php
                                $total_tax = 0;
                                $taxes = getTaxShipmentCharges('Tax');
                                foreach ($taxes as $tax) {
                                    $tax_title = $tax->Title;
                                    $tax_factor = $tax->Type == 'Fixed' ? number_format($tax->Amount, 2) . ' SAR' : $tax->Amount . '%';
                                    if ($tax->Type == 'Fixed') {
                                        $tax_amount = $tax->Amount;
                                    } elseif ($tax->Type == 'Percentage') {
                                        //$tax_amount = ($tax->Amount / 100) * ($total + $shipping_amount);
                                        $tax_amount = ($tax->Amount / 100) * ($total);
                                    }
                                    ?>
                                    <li>
                                                <span><!-- <i class="fa fa-file-text-o" aria-hidden="true"></i> --> <?php echo $tax_title; ?> <?php echo $tax_factor; ?></span>
                                        <strong id="TaxAmount">&emsp;<?php echo number_format($tax_amount, 2); ?> <?php echo lang('sar'); ?></strong>
                                    </li>
                                    <?php
                                    $total_tax += $tax_amount;
                                }
                                $total = $total + $shipping_amount + $total_tax;
                                ?>
                            </ol>
                        </div>
                        <div class="col-md-12">
                            
                                <span><?php echo lang('Grand_Total'); ?></span>&emsp;<strong><?php echo number_format($total, 2); ?> <?php echo lang('sar'); ?></strong>
                            
                        </div>
                    </div>
                    <div class="row no-border shipMethodTxt">
                         <div class="form-group">
                            <div class="chek-form">
                                <div class="custome-checkbox">
                                    <input class="form-check-input acceptTerms" type="checkbox" name="checkbox" id="TermsConditions">
                                    <label class="form-check-label label_info" data-bs-toggle="collapse" data-target="#collapseAddress" for="TermsConditions"><span>I accept the Terms and Conditons</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <a href="javascript:void(0);" class="btn ccheckout btn-primary"  onclick="placeOrder();" >Continue to
                                Payment</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</main>
<!-- Modal -->
<div class="modal fade" id="chkContShipping" tabindex="1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">
  <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
  <div class="modal-dialog modal-dialog-centered modWl" role="document">
    <div class="modal-content mod-cont <?php echo($lang == 'AR' ? 'text-right' : 'text-left'); ?>">
        <div class="modal-header mod_hdd">
            
            <div class="title">
            <h1 class="hed-title">Attention!</h1>
            <h3 class="scnd-title">Some of the Items are not available in this Branch</h3>
            </div>
        </div>
        <div class="modal-body" id="chkContShippingBody">
            
        </div>
    <div class="modal-footer mod_ftt">
        
        <div class="edt-crt text-center">
            <a href="<?php echo base_url('cart');?>"> <button  class="btn-light crt-btn">Edit Cart</button></a>
        </div>
        
        <div class="edt-pay text-center">  
            <a href="javascript:void();" class="btn-info pay-btn" onclick="placeOrder();">Continue to Payment</a>
            <p class="clk-con text-center">By Clicking continue, you will receive the proposed quantity and sold-out items will be discarded</p>
        </div>
    </div>
     
    </div>
  </div>
</div>
<input type="hidden"  id="StoreID" value="<?php echo ($this->session->userdata('DeliveryStoreID') ? $this->session->userdata('DeliveryStoreID') : ''); ?>">
<script>
    $(document).ready(function(){
            $('.collectFromStore').on('click',function(){
                $('#collect').show();
                //$('.shipment_method').hide();
                $("input:radio[name='shipment_method']").each(function(i) {
                       this.checked = false;
                });
               // $("#shipment_method").css("pointer-events","none");
                //$('#delivery').hide();
                $('#shipment_method').hide();
                
                
            });
            $('.addressCheckbox').on('click',function(){
                
                $('#collect').hide();
               // $('.shipment_method').show();
               //$("#shipment_method").css("pointer-events","auto");
                 $('#delivery').show();
                $('#shipment_method').show();
            });
            <?php if(isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1 ){ ?>
                    $("#shipment_method").css("pointer-events","none");
                    $("input:radio[name='shipment_method']").each(function(i) {
                       this.checked = false;
                    });
            <?php } ?>
    });
    function selectBrach(city_id){
        if(city_id == ''){
             window.location.replace("<?php echo base_url();?>address");
        }else{
             window.location.replace("<?php echo base_url();?>address?city="+city_id);
        }
       // window.location.replace("<?php echo base_url();?>address?city="+city_id);
    }
    var markers = [];
    var center_lat = <?php echo(isset($available_cities[0]) ? $available_cities[0]->Latitude : 21.484716); ?>;
    var center_lng = <?php echo(isset($available_cities[0]) ? $available_cities[0]->Longitude : 39.189606); ?>;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: new google.maps.LatLng(center_lat, center_lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles:[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "color": "#50456d"
            },
            {
                "saturation": "29"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#b31f1f"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dfd1ae"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#f00000"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text",
        "stylers": [
            {
                "color": "#1a1526"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ff0f0f"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ebe3cd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "color": "#892020"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-84"
            },
            {
                "lightness": "85"
            },
            {
                "gamma": "0.00"
            },
            {
                "weight": "1"
            },
            {
                "color": "#50456d"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#f6c866"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e98d58"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
]
    });
    <?php if ($available_cities && count($available_cities) > 0)
    { ?>
    var infowindow = new google.maps.InfoWindow();
    var marker;
    <?php
    $i = 0;
    foreach ($available_cities as $store)
    { ?>
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(<?php echo $store->Latitude; ?>, <?php echo $store->Longitude; ?>),
         icon: '<?php echo base_url(); ?>assets/frontend/imgs/pin.svg',
         label: {text: '<?php echo $i+1; ?>' , color: 'white'},
        map: map
    });
    google.maps.event.addListener(marker, 'click', (function (marker) {
        return function () {
            infowindow.setContent('<h5 style="color:black;"><?php echo $store->StoreTitle; ?></h5>');
            infowindow.open(map, marker);
            var district = '<?php echo $store->DistrictID; ?>';
            var district_list = district.split(',');
            var address_district = $('input[name="collectionOption"]:checked').attr('data-district-id');
            if(address_district > 0){
                if(jQuery.inArray(address_district, district_list) != -1) {
                    //console.log(district_list);
                    //console.log(address_district);
                   $('#selected_branch_html').html('<?php echo $store->StoreTitle; ?>'+' Selected');
                   $('#selected_branch_html').show();
                   $('#StoreID').val(<?php echo $store->StoreID; ?>);
                   changeDeliveryStoreID(<?php echo $store->StoreID; ?>,'<?php echo $store->StoreTitle; ?>');
                } else {
                    showMessage('This branch is not delivered in your district please select other branch.','danger');
                    $('#selected_branch_html').html('This branch is not delivered in your district please select other branch.');
                    $('#selected_branch_html').show();
                    $('#StoreID').val('');
                    unsetDeliveryStoreID();
                    //return false;
                }
            }else{
                $('#selected_branch_html').html('<?php echo $store->StoreTitle; ?>'+' Selected');
                $('#selected_branch_html').show();
                $('#StoreID').val(<?php echo $store->StoreID; ?>);
                changeDeliveryStoreID(<?php echo $store->StoreID; ?>,'<?php echo $store->StoreTitle; ?>');
            }
            
             
           // $('#selected_branch_html').html('<?php echo $store->StoreTitle; ?>'+' Selected');
            //$('#selected_branch_html').show();
            //$('#StoreID').val(<?php echo $store->StoreID; ?>);
        }
    })(marker));
    markers.push(marker);
    <?php $i++; } ?>
    <?php } ?>
    function myClick(index) {
        google.maps.event.trigger(markers[index], 'click');
    }
    // Code to disable browser back button
   /* $(document).ready(function () {
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            window.history.pushState(null, "", window.location.href);
        };
    });
// In the following example, markers appear when the user clicks on the map.
      // Each marker is labeled with a single alphabetical character.
      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var labelIndex = 0;
      function initialize(){
        var bangalore = { lat: 12.97, lng: 77.59 };
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: bangalore
        });
        // This event listener calls addMarker() when the map is clicked.
        google.maps.event.addListener(map, 'click', function(event) {
          addMarker(event.latLng, map);
        });
        // Add a marker at the center of the map.
        addMarker(bangalore, map);
      }
      // Adds a marker to the map.
      function addMarker(location, map) {
        // Add the marker at the clicked location, and add the next-available label
        // from the array of alphabetical characters.
        var marker = new google.maps.Marker({
            icon: '<?php echo base_url(); ?>assets/frontend/imgs/pin.svg',
          position: location,
          label: '1',
          map: map
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);*/
</script>