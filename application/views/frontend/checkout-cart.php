<style>
small, .small {
    font-size: 13px;
    font-weight: normal;
    color: #000;
}
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
    .content.checkout .navarea li:nth-child(1) img.gold {display:none;}
    .content.checkout .navarea li:nth-child(1) img.gray {display:inline-block;}
    .content.checkout .navarea li:nth-child(1).active img.gold {display:inline-block;}
    .content.checkout .navarea li:nth-child(1).active img.gray {display:none;}
    @media (max-width:767px) {
        .mb-xs-small {
            margin-bottom:10px !important;
        }
        .mb-xs-small h6.visible-xs {
            margin: 0 0 4px !important;
        }
        .mb-xs-small h5 {
            margin: 0 !important;
        }
    }
</style>
<section class="content products checkout pt10">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="navarea">
                    <li class="active">
                        <a data-toggle="tab" href="#shopping">
                        <img class="gray" src="<?php echo front_assets() ?>images/shopping_basket_gray_small.png">
                        <img class="gold" src="<?php echo front_assets() ?>images/shopping_basket_gold_small.png">
                            <span><?php echo lang('shopping_basket'); ?></span>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#wishlist">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <span><?php echo lang('wishlist'); ?></span>
                        </a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div id="shopping" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="wbox">
                                   <div class="minWidthScroll">
                                   <div class="row">
                                        <div class="col-sm-4">
                                            <h6 class="hidden-xs"><?php echo lang('products'); ?></h6>
                                        </div>
                                        <div class="col-sm-3">
                                            <h6 class="hidden-xs"><?php echo lang('quantity'); ?></h6>
                                        </div>
                                        <div class="col-sm-2">
                                            <h6 class="hidden-xs"><?php echo lang('price'); ?></h6>
                                        </div>
                                        <div class="col-sm-3">
                                            <h6 class="hidden-xs"><?php echo lang('total'); ?></h6>
                                        </div>
                                    </div>
                                    <?php
                                    $i = 1;
                                    $total = 0;
                                    foreach ($cart_items as $cart_item) {
                                        if ($cart_item->CorporateMinQuantity > 0) {
                                            $MinQuantity = $cart_item->CorporateMinQuantity;
                                        } else {
                                            $MinQuantity = 1;
                                        }
                                        ?>
                                        <div class="row" id="TempOrderID<?php echo $cart_item->TempOrderID; ?>">
                                            <div class="col-sm-4 mb-xs-small">
                                                <h6 class="visible-xs"><?php echo lang('products'); ?></h6>
                                                <?php
                                                if ($cart_item->ItemType == 'Product') { ?>
                                                    <a href="<?php echo base_url() . 'product/detail/' . productTitle($cart_item->ProductID); ?>">
                                                        <img src="<?php echo base_url(get_images($cart_item->ProductID, 'product', false)); ?>">
                                                        <h5><?php echo $cart_item->Title; ?></h5>
                                                    </a>
                                                <?php } elseif ($cart_item->ItemType == 'Customized Shape') { ?>
                                                    <img src="<?php echo base_url($cart_item->CustomizedShapeImage); ?>">
                                                    <h5>Customized Shape</h5>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0);" class="chocobox_detail"
                                                       title="Click to view whats inside"
                                                       data-box_id="<?php echo $cart_item->CustomizedBoxID; ?>"
                                                       data-pids="<?php echo $cart_item->CustomizedOrderProductIDs; ?>"
                                                       data-box_type="<?php echo $cart_item->ItemType; ?>"
                                                       data-ribbon="<?php echo $cart_item->Ribbon; ?>">
                                                        <img src="<?php echo front_assets("images/" . $cart_item->ItemType . ".png"); ?>">
                                                        <h5><?php echo $cart_item->ItemType; ?></h5>
                                                    </a>
                                                <?php }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-3 mb-xs-small inputnumber <?php echo ($cart_item->PriceType == 'kg' ? 'hide_inc_btn' : ''); ?>">
                                                <h6 class="visible-xs"><?php echo lang('quantity'); ?></h6>
                                                <input id="after<?php echo $i; ?>"
                                                       class="form-control cart_quantity update_cart "
                                                       type="number"
                                                       value="<?php echo $cart_item->Quantity; ?>"
                                                       min="<?php echo $MinQuantity; ?>"
                                                       data-temp_order_id="<?php echo $cart_item->TempOrderID; ?>"
                                                       data-item_price="<?php echo $cart_item->TempItemPrice; ?>" <?php echo ($cart_item->PriceType == 'kg' ? 'readonly' : ''); ?> <?php echo ($cart_item->PriceType == 'kg' ? 'style="display:none"' : ''); ?>/>
                                                       <?php if($cart_item->PriceType == 'kg'){ ?>
                                                        <span><?php echo $cart_item->Quantity * 1000; ?> <?php echo ($language == 'AR' ? 'غرام' : 'Grams'); ?><br><a class="small" href="<?php echo base_url() . 'product/detail/' . productTitle($cart_item->ProductID); ?>"><?php echo lang('Add more'); ?></a></span>
                                                       <?php } ?>
                                            </div>
                                            <div class="col-sm-2 mb-xs-small">
                                                <h6 class="visible-xs"><?php echo lang('price'); ?></h6>
                                                <h5><?php echo number_format($cart_item->TempItemPrice, 2); ?> <?php echo lang('sar'); ?></h5>
                                            </div>
                                            <div class="col-sm-3 mb-xs-small dropdown editbox">
                                                <h6 class="visible-xs"><?php echo lang('total'); ?></h6>
                                                <h5>
                                                    <span id="TotalPrice_<?php echo $cart_item->TempOrderID; ?>"><?php echo number_format($cart_item->TempItemPrice * $cart_item->Quantity, 2); ?></span>
                                                    <?php echo lang('sar'); ?></h5>
                                                <!-- <a href="javascript:void(0);"
                                                   onclick="removeIt('cart/removeFromCart', 'TempOrderID', <?php echo $cart_item->TempOrderID ?>);">
                                                    <button class="btn dropdown-toggle" type="button">
                                                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                    </button>
                                                </a> -->
                                                <div class="dropdown">
                                                    <button class="btn dropdown-toggle" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                        <?php if(isLiked($cart_item->ProductID,'Product') == 'p_liked'){ ?>
                                                                <li><a href="javascript:void(0);" onclick="addToWishlist(<?php echo $cart_item->ProductID; ?>,'Product');"><?php echo lang('removed_from_wishlist'); ?></a></li>
                                                        <?php }else{ ?>
                                                            <li><a href="javascript:void(0);" onclick="addToWishlist(<?php echo $cart_item->ProductID; ?>,'Product');"><?php echo lang('click_to_add_to_your_wishlist'); ?></a></li>
                                                      <?php  } ?>
                                                        <li><a href="javascript:void(0);" onclick="removeIt('cart/removeFromCart', 'TempOrderID', <?php echo $cart_item->TempOrderID ?>);"><?php echo lang('delete'); ?></a></li>
                                                    </ul>
                                                </div>


                                            </div>
                                        </div>
                                        <?php
                                        $total += $cart_item->TempItemPrice * $cart_item->Quantity;
                                        $i++;
                                    }
                                    ?>
                                   </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="wbox side">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6 class="mb-0" style="margin-bottom: 0;"><?php echo lang('Total_bill'); ?></h6>
                                        </div>
                                    </div>
                                    <?php
                                    if ($this->session->userdata('order_coupon')) {
                                        $order_coupon = $this->session->userdata('order_coupon');
                                        $coupon_code = $order_coupon['CouponCode'];
                                        $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                                        $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total;
                                        $total = $total - $coupon_discount_availed;
                                        ?>
                                        <p><span>Promo Applied:</span> <strong><?php echo $coupon_code; ?></strong>
                                            <a class="btn" href="javascript:void(0);" onclick="removeCoupon();">
                                                <button class="btn">Clear <i class="fa fa-remove"
                                                                             style="color: red !important;"></i>
                                                </button>
                                            </a>
                                        </p>
                                        <p><span><?php echo lang('promo_discount');?> %:</span>
                                            <strong><?php echo $coupon_discount_percentage; ?></strong></p>
                                        <p><span><?php echo lang('promo_discount_availed'); ?>:</span>
                                            <strong><?php echo $coupon_discount_availed; ?> <?php echo lang('sar'); ?></strong></p>
                                    <?php } else { ?>
                                        <form action="<?php echo base_url('cart/applyCoupon'); ?>" method="post"
                                              class="couponApplyForm gift" id="couponForm">
                                            <i class="fa fa-gift" aria-hidden="true"></i>
                                            <span class="borderBox"><input type="text" name="CouponCode" placeholder="Gift voucher code."
                                                   class="form-control required"></span>
                                            <input type="submit" name="" value="Redeem" class="btn btn-primary">
                                        </form>
                                    <?php }
                                    ?>
                                    <ol>
                                        <?php
                                        $shipping_amount = 0;
                                        $shipment_method = getTaxShipmentCharges('Shipment', true);
                                        if ($shipment_method) {
                                            $shipping_title = $shipment_method->Title;
                                            $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                                            if ($shipment_method->Type == 'Fixed') {
                                                $shipping_amount = $shipment_method->Amount;
                                            } elseif ($shipment_method->Type == 'Percentage') {
                                                $shipping_amount = ($shipment_method->Amount / 100) * ($total);
                                            }
                                            ?>
                                            <li>
                                                <!--<span><i class="fa fa-truck"
                                                         aria-hidden="true"></i> <?php echo $shipping_title; ?></span>
                                                <strong id="ShippingAmount"><?php echo number_format($shipping_amount, 2); ?>
                                                    SAR</strong>-->
                                                    <span><i class="fa fa-file-text-o"
                                                         aria-hidden="true"></i> <?php echo lang('total'); ?></span>
                                                    <strong id="ShippingAmount"><?php echo number_format($total, 2); ?>
                                                    <?php echo lang('sar'); ?></strong>
                                            </li>
                                        <?php }
                                        ?>
                                        <?php
                                        $total_tax = 0;
                                        $taxes = getTaxShipmentCharges('Tax');
                                        foreach ($taxes as $tax) {
                                            $tax_title = $tax->Title;
                                            $tax_factor = $tax->Type == 'Fixed' ? '' : $tax->Amount . '%';
                                            if ($tax->Type == 'Fixed') {
                                                $tax_amount = $tax->Amount;
                                            } elseif ($tax->Type == 'Percentage') {
                                               // $tax_amount = ($tax->Amount / 100) * ($total + $shipping_amount);
                                                 $tax_amount = ($tax->Amount / 100) * ($total);
                                            }
                                            ?>
                                            <li>
                                                <span><i class="fa fa-file-text-o"
                                                         aria-hidden="true"></i> <?php echo $tax_title; ?> <?php echo $tax_factor; ?></span>
                                                <strong id="TaxAmount"><?php echo number_format($tax_amount, 2); ?>
                                                <?php echo lang('sar'); ?></strong>
                                            </li>
                                            <?php
                                            $total_tax += $tax_amount;
                                        }

                                       // $total = $total + $shipping_amount + $total_tax;
                                        $total = $total + $total_tax;
                                        ?>
                                        <li><h5><span><?php echo lang('Grand_Total'); ?></span>
                                                <?php echo number_format($total, 2); ?><strong><?php echo lang('sar'); ?></strong></h5>
                                        </li>
                                    </ol>
                                    <a href="javascript:void(0);" onclick="proceedToCheckout();">
                                        <button type="button" class="btn btn-primary checkoutbtn"><?php echo lang('Procceed_to_Checkout'); ?>
                                        </button>
                                    </a>
                                    <p class="text-center"><a href="<?php echo base_url('product'); ?>"><?php echo lang('Continue_Shopping'); ?></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="wishlist" class="tab-pane fade">
                        <div class="row">
                            <?php
                            foreach ($wishlist_items as $wishlist_item) {
                                if ($wishlist_item->ItemType == 'Product') {
                                    $url = base_url() . 'product/detail/' . productTitle($wishlist_item->ItemID);
                                    $image = base_url(get_images($wishlist_item->ItemID, 'product', false));
                                } elseif ($wishlist_item->ItemType == 'Collection') {
                                    $url = base_url() . 'collection/detail/' . collectionTitle($wishlist_item->ItemID);
                                    $image = base_url(get_images($wishlist_item->ItemID, 'collection', false));
                                }
                                ?>
                                <div class="col-md-2">
                                    <div class="inbox">
                                        <div class="imgbox">
                                            <img src="<?php echo $image; ?>">
                                        </div>
                                        <a href="<?php echo $url; ?>">
                                            <h4><?php echo $wishlist_item->Title; ?></h4>
                                            <h5><strong><?php echo $wishlist_item->Price; ?></strong> <?php echo lang('sar'); ?></h5>
                                        </a>
                                        <a title="<?php echo lang('click_to_add_to_your_wishlist'); ?>" href="javascript:void(0);"
                                           onclick="addToWishlist(<?php echo $wishlist_item->ItemID; ?>, '<?php echo $wishlist_item->ItemType; ?>');"><i
                                                    class="fa fa-heart <?php echo isLiked($wishlist_item->ItemID, $wishlist_item->ItemType); ?>"
                                                    id="item<?php echo $wishlist_item->ItemID; ?>"
                                                    aria-hidden="true"></i></a>
                                        <a href="javascript:void(0);" title="<?php echo lang('click_to_add_this_to_your_cart'); ?>"
                                           onclick="addWishlistToCart(<?php echo $wishlist_item->ItemID; ?>, '<?php echo ucfirst($wishlist_item->ItemType); ?>', '<?php echo $wishlist_item->Price; ?>','<?php echo $wishlist_item->IsCorporateProduct; ?>');">
                                            <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

