<section class="main">
    <div class="page-header">
        <div class="row mt-20">
            <div class="col-xl-10 col-lg-12 mt-20">
                <div class="alertbox container mb-30">
                    <h5>
                        <i class="fa fa-check-circle ok" aria-hidden="true"></i>
                        <h5 class="heading-2 mb-10">Your Order has been placed</h5>
                        <span><strong><?php echo lang('Order_No'); ?>. <?php echo $order->OrderNumber; ?></strong></span>
                        <!--<a href="javascript:void(0);" onclick="window.print();" style="text-decoration: none;">-->
                        <a href="<?php echo base_url(); ?>page/invoice/<?php echo base64_encode($order->OrderID); ?>" target="_blank" style="text-decoration: none;">
                        <b>
                            <i class="fa fa-print" aria-hidden="true"></i>
                            print
                        </b>
                        </a>
                    </h5>

                </div>
            </div>
        </div>
        <div class="container mb-30">
            <div class="row">
                <div class="col-xl-10 col-lg-12">
                                 
                            <div class="col-md-12">
                                
                                <a href="<?php echo base_url(); ?>">
                                    <button class="btn">Continue</button>
                                </a>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    // Code to disable browser back button
    $(document).ready(function () {
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            window.history.pushState(null, "", window.location.href);
        };
    });

    eraseCookie('CollectFromStore');
    function eraseCookie(name) {
        document.cookie = name + '=; Max-Age=-99999999;';
    }
</script>