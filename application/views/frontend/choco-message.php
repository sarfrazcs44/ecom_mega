<style type="text/css">
    .draggable {
        filter: alpha(opacity=60);
        opacity: 0.6;
    }

    .dropped {
        position: static !important;
        list-style-type: none;
        float: left;
        margin: 0 1% 0 0;
        width: 19%;
    }

    #dvSource, #dvDest {
        border: 5px solid #ccc;
        padding: 5px;
        min-height: 100px;
        width: 430px;
    }

    img {
        -webkit-transition-duration: 0s;
        -o-transition-duration: 0s;
        transition-duration: 0s;
    }
</style>
<?php $choco_msg = getPageContent(10, $lang) ?>
<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $choco_msg->Title; ?></h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li><?php echo $choco_msg->Title; ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="content single-products productskg customizebox">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="list-group">
                    <a href="#menu1" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span>Compose your message<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse in" id="menu1">
                        <p>Click Chocolate letters to add them to your box.</p>
                        <ul class="customletters custommsg">
                            <li data-product_id="C" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/c.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="H" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/h.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="O" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/o.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="R" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/r.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="D" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/d.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="C" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/c.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="H" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/h.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="O" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/o.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="R" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/r.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="D" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/d.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="C" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/c.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="H" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/h.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="O" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/o.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="R" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/r.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="D" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/d.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="C" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/c.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="H" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/h.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="O" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/o.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="R" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/r.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="D" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/d.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="C" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/c.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="H" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/h.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="O" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/o.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="R" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/r.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="D" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/d.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="list-group">
                    <a href="#menu2" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span>Numbers & Symbols<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu2">
                        <p>Click Symbols and Shapes to add them to your box.</p>
                        <ul class="customletters custommsg">
                            <li data-product_id="HEART" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/heart.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="HEART" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/heart.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="HEART" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/heart.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="HEART" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/heart.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="HEART" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/heart.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                            <li data-product_id="HEART" data-product_price="5">
                                <div class="img-overlay">
                                    <img src="<?php echo front_assets(); ?>images/heart.png">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li>
                        </ul>
                    </div>
                </div>
                <!--<div class="list-group">
                    <a href="#menu3" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span>Ribbon Color<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu3">
                        <p>Click Symbols and Shapes to add them to your box.</p>
                        <ul class="customletters custommsg">
                            <li>
                                <div class="img-overlay">
                                    <img src="<?php /*echo front_assets(); */?>images/red.png">
                                </div>
                                <img src="<?php /*echo front_assets(); */?>images/closes.png" class="close">
                            </li>
                            <li>
                                <div class="img-overlay">
                                    <img src="<?php /*echo front_assets(); */?>images/green.png">
                                </div>
                                <img src="<?php /*echo front_assets(); */?>images/closes.png" class="close">
                            </li>
                            <li>
                                <div class="img-overlay">
                                    <img src="<?php /*echo front_assets(); */?>images/pink.png">
                                </div>
                                <img src="<?php /*echo front_assets(); */?>images/closes.png" class="close">
                            </li>
                        </ul>
                    </div>
                </div>-->
            </div>
            <div class="col-md-5 sidebox">
                <div class="product-info">
                    <div id="customsgbox" data-box_size="<?php echo $box->BoxSpace; ?>" data-box_min_price="<?php echo $box->BoxPrice; ?>" data-box_id="<?php echo $box->BoxID; ?>" style="background-image: url('<?php echo base_url($box->BoxImage); ?>');">
                        <ul>

                        </ul>
                    </div>
                    <h2><?php echo $box->Title; ?></h2>
                    <h5>
                        <?php echo lang('box_price'); ?>
                        <strong><?php echo number_format($box->BoxPrice, 2); ?></strong>
                        <?php echo lang('sar'); ?>
                    </h5>
                    <h5><?php echo lang('box_capacity'); ?>
                        <strong><?php echo $box->BoxSpace; ?></strong> <?php echo lang('pieces'); ?></h5>
                    <?php echo $box->Description; ?>
                    <h5 class="bordered"><?php echo lang('price'); ?> <span><b class="ProductP"><?php echo number_format($box->BoxPrice, 2); ?></b> <?php echo lang('sar'); ?></span></h5>
                    <div class="row">
                        <div class="col-md-6">
                            <h6><?php echo lang('quantity'); ?></h6>
                        </div>
                        <div class="col-md-6">
                            <input id="after" name="Quantity" class="form-control Quantity" type="number" value="1"
                                   min="1"/>
                            <input type="hidden" class="ProductIDs" name="ProductIDs">
                            <input type="hidden" class="ProductPrice" name="ProductPrice" value="<?php echo $box->BoxPrice; ?>">
                            <input type="hidden" class="BoxID" name="BoxID" value="<?php echo $box->BoxID; ?>">
                            <input type="hidden" class="RibbonColor" name="RibbonColor" value="<?php echo $RibbonColor; ?>">
                            <input type="hidden" class="ItemType" value="Choco Message">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary" onclick="addChocoBoxToCart();"><?php echo lang('add_to_basket'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>