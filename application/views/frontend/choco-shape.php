<style type="text/css">
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .btn {
        border: 2px solid gray;
        color: gray;
        background-color: white;
        padding: 8px 20px;
        border-radius: 8px;
        font-size: 20px;
        font-weight: bold;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .draggable {
        filter: alpha(opacity=60);
        opacity: 0.6;
    }

    .dropped {
        position: static !important;
        list-style-type: none;
        float: left;
        margin: 0 1% 0 0;
        width: 19%;
    }

    #dvSource, #dvDest {
        border: 5px solid #ccc;
        padding: 5px;
        min-height: 100px;
        width: 430px;
    }

    img {
        -webkit-transition-duration: 0s;
        -o-transition-duration: 0s;
        transition-duration: 0s;
    }
</style>
<?php $choco_shape = getPageContent(12, $lang) ?>
<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $choco_shape->Title; ?></h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li><?php echo $choco_shape->Title; ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="content single-products productskg customizebox">
    <div class="container">
        <div class="row">
            <form action="<?php echo base_url('cart/addCustomizedShapeToCart'); ?>" method="post"
                  id="addCustomizedShapeToCart" enctype="multipart/form-data" onsubmit="return false;">
                <!--<div class="col-md-5">
                    <img id="uploadPreview" style="width: 300px;height: 300px;margin-bottom: 20px;display: none;"/>
                    <input id="uploadImage" type="file" name="CustomizedShapeImage" accept="image/*"
                           onchange="PreviewImage();"/>
                </div>-->

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <img id="uploadPreview"
                                 style="width: 300px;height: 300px;margin-bottom: 20px;display: none;"/>
                        </div>
                        <div class="col-md-12">
                            <div class="upload-btn-wrapper">
                                <button class="btn">Upload shape file</button>
                                <input type="file" name="CustomizedShapeImage" id="uploadImage"
                                       accept="image/jpeg, image/png"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <small style="margin-left: 26px;"><?php echo lang('only_shape_formats'); ?></small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 sidebox">
                    <div class="product-info" data-box_min_price="2.00">
                        <h2>30 Chocolate PCS Box</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie
                            molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac.
                            Suspendisse
                            potenti.</p>
                        <h5 class="bordered"><?php echo lang('price'); ?> <span><b
                                        class="ProductP">0.00</b> <?php echo lang('sar'); ?></span></h5>
                        <div class="row">
                            <div class="col-md-6">
                                <h6><?php echo lang('quantity'); ?></h6>
                            </div>
                            <div class="col-md-6">
                                <input id="after" name="Quantity" class="form-control Quantity" type="number" value="1"
                                       min="1"/>
                                <input type="hidden" class="ProductPrice" name="TempItemPrice" value="0">
                                <input type="hidden" name="ItemType" value="Customized Shape">
                            </div>
                            <div class="col-md-12">
                                <button type="submit"
                                        class="btn btn-primary addToCartBTN"><?php echo lang('add_to_basket'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $('.ProductP').html($('.product-info').data('box_min_price'));
    $('.ProductPrice').val($('.product-info').data('box_min_price'));

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
            $('#uploadPreview').show();
        };
    };

    $(document).ready(function () {
        $('input[type="file"]').change(function (e) {
            // var fileName = e.target.files[0].name;
            var fileType = e.target.files[0].type;
            if (fileType == 'image/png' || fileType == 'image/jpeg') {
                PreviewImage();
                $('.addToCartBTN').attr('disabled', false);
            } else {
                showMessage('<?php echo lang('only_shape_formats'); ?>', 'danger');
                $('.addToCartBTN').attr('disabled', true);
                $('#uploadPreview').hide();
            }
        });
    });
</script>