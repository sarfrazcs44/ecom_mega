

<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Corporate Chocolates <span>Chocolates Per KG</span></h2>
            </div>
        </div>
    </div>
</section>

<section class="content productskg">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp1.png">
                            </div>
                            <input id="after1" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp2.png">
                            </div>
                            <input id="after2" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp3.png">
                            </div>
                            <input id="after3" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp4.png">
                            </div>
                            <input id="after4" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp5.png">
                            </div>
                            <input id="after5" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp6.png">
                            </div>
                            <input id="after6" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp7.png">
                            </div>
                            <input id="after7" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp8.png">
                            </div>
                            <input id="after8" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp9.png">
                            </div>
                            <input id="after9" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp10.png">
                            </div>
                            <input id="after10" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp11.png">
                            </div>
                            <input id="after11" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pbox inputnumber">
                            <div class="imgbox">
                                <img src="<?php echo front_assets(); ?>images/cp12.png">
                            </div>
                            <input id="after12" class="form-control" type="number" value="3" min="1" max="15" />
                            <h3>Walnut & Chocolate <span>20.00 SAR</span></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 sidebox">
                <h2>Mix Chocolates Min 2 KG</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti.</p>
                <h5>Box Dimensions</h5>
                <ol>
                    <li>
                        <span>Length:</span>   
                        <strong>244 mm</strong>
                    </li>
                    <li>
                        <span>Width:</span>   
                        <strong>244 mm</strong>
                    </li>
                    <li>
                        <span>Height:</span>   
                        <strong>244 mm</strong>
                    </li>
                </ol>
                <h5>Order Notes</h5>
                <ol>
                    <li>
                        <span>Minimum Order:</span>   
                        <strong>1 Pieces</strong>
                    </li>
                    <li>
                        <span>Processing Time:</span>   
                        <strong>3 Days</strong>
                    </li>
                    <li>
                        <span>Height:</span>   
                        <strong>244 mm</strong>
                    </li>
                </ol>
                <h5>Minimum Qty <span>2 KG</span></h5>
                <h5>Price<span>270.00 SAR</span></h5>
                <button class="btn btn-primary">Add to Cart</button>
            </div>
        </div>
    </div>
</section>

