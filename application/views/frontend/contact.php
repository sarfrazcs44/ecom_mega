<?php $site_settings = site_settings(); ?>
<section class="content contact">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2><?php echo $result->Title; ?></h2>
                <?php echo $result->Description; ?>
            </div>
            <div class="col-md-9">
                <div class="sideinner">
                    <div class="row">
                        <div class="col-md-6">
                            <h6><?php echo lang('feel_free_to'); ?></h6>
                            <h2><?php echo lang('write_us'); ?></h2>
                        </div>
                        <div class="col-md-6">
                            <h6><?php echo lang('toll_free'); ?></h6>
                            <h2>
                                <a href="tel:<?php echo $site_settings->PhoneNumber; ?>"><?php echo $site_settings->PhoneNumber; ?></a>
                            </h2>
                        </div>
                    </div>
                    <div class="row">
                        <form action="<?php echo base_url('page/saveContactForm'); ?>" method="post"
                              onsubmit="return false;" class="ajaxForm" id="contactForm">
                            <div class="col-md-6">
                                <label><?php echo lang('full_name'); ?></label>
                                <input type="text" name="FullName" class="form-control required">
                            </div>
                            <div class="col-md-6">
                                <label><?php echo lang('email'); ?></label>
                                <input type="email" name="Email" class="form-control required">
                            </div>
                            <div class="col-md-6">
                                <label><?php echo lang('subject'); ?></label>
                                <select class="form-control" name="Subject" onchange="contactUsSubject(this.value, 'contactForm');">
                                    <option value="Feedback" selected><?php echo lang('feedback'); ?></option>
                                    <option value="Career"><?php echo lang('career'); ?></option>
                                </select>
                            </div>
                            <div class="col-md-6 FeedbackField">
                                <label><?php echo lang('company'); ?></label>
                                <input type="text" name="Company" class="form-control required">
                            </div>
                            <div class="col-md-6 FeedbackField">
                                <label><?php echo lang('department'); ?></label>
                                <input type="text" name="Department" class="form-control required">
                            </div>
                            <div class="col-md-12 FeedbackField">
                                <label><?php echo lang('your_message'); ?></label>
                                <textarea name="Message" class="form-control required"></textarea>
                            </div>
                            <div class="col-md-6 CareerField" style="display: none;">
                                <label><?php echo lang('upload_cv'); ?></label>
                                <input type="file" name="CV" class="form-control" accept="application/msword, application/pdf">
                            </div>
                            <div class="col-md-6">
                                <div class="g-recaptcha" data-sitekey="6LeqI5EUAAAAABiG0Jaj_0WJgXehYfF5ICFrnWeO"></div>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-secondary"><?php echo lang('submit'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>