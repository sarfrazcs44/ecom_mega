<div class="slider home">
    <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Carousel items -->
        <div class="carousel-inner">
            <div class="active item">
                <img src="<?php echo base_url($result->Image); ?>">
                <div class="carousel-caption">
                    <?php echo $result->ShortDescription; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner text-center">
                    <?php echo $result->Description; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content announcements">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h3><?php echo lang('bulk'); ?> <span><?php echo lang('announcements'); ?></span></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ol>
                    <?php
                    if ($corporate_products) {
                        foreach ($corporate_products as $corporate_product) { ?>
                            <li>
                                <a href="<?php echo base_url() . 'product/detail/' . productTitle($corporate_product->ProductID); ?>">
                                    <div class="item">
                                        <img src="<?php echo base_url(get_images($corporate_product->ProductID, 'product', false)); ?>"
                                             alt="<?php echo $corporate_product->Title; ?>">
                                        <h4><?php echo $corporate_product->Title; ?></h4>
                                    </div>
                                </a>
                            </li>
                        <?php }
                    }
                    ?>
                </ol>
                <div class="text-center btn-row">
                    <a href="<?php echo base_url('corporate/products'); ?>" class="btn btn-primary"><?php echo lang('view_all'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>

