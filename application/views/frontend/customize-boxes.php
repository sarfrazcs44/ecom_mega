<?php
$ribbons = array('red', 'green', 'pink');
?>
<section class="content products titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo lang('box_packaging_detail'); ?>
                    <span><?php echo lang('select_box_to_continue'); ?></span></h2>
            </div>
        </div>
    </div>
</section>

<section class="content products">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3><?php echo lang('boxes'); ?></h3>
                <?php
                if ($boxes) {
                    foreach ($boxes as $box) {
                        ?>
                        <div class="col-md-4">
                            <div class="inbox">
                                <a class="selectBox" title="<?php echo lang('click_to_proceed_with_box'); ?>"
                                   href="javascript:void(0);" data-box_id="<?php echo $box->BoxID; ?>">
                                    <div class="imgbox">
                                        <img src="<?php echo base_url($box->BoxImage); ?>"
                                             alt="<?php echo $box->Title; ?>">
                                    </div>
                                </a>
                                <h4><?php echo $box->Title; ?></h4>
                                <h5>
                                    <?php echo lang('box_price'); ?>
                                    <strong><?php echo number_format($box->BoxPrice, 2); ?></strong>
                                    <?php echo lang('sar'); ?>
                                </h5>
                                <h5><?php echo lang('box_capacity'); ?>
                                    <strong><?php echo $box->BoxSpace; ?></strong> <?php echo lang('pieces'); ?></h5>
                            </div>
                        </div>
                    <?php }
                }
                ?>
            </div>
            <div class="col-md-4">
                <h3>Your selected box</h3>
                <div class="box_detail">
                    <img src="<?php echo base_url($boxes[0]->BoxImage); ?>">
                </div>
                <br>
                <h3>Your selected ribbon</h3>
                <div class="ribbon_detail">
                    <img src="<?php echo front_assets(); ?>images/<?php echo $ribbons[0]; ?>.png">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <h3><?php echo lang('ribbons'); ?></h3>
                <?php
                foreach ($ribbons as $ribbon) { ?>
                    <a href="javascript:void(0);" data-ribbon_color="<?php echo $ribbon; ?>" class="selectRibbon">
                        <img src="<?php echo front_assets(); ?>images/<?php echo $ribbon; ?>.png">
                    </a>
                <?php }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 pull-right">
                <input type="hidden" id="BoxID" value="<?php echo $boxes[0]->BoxID; ?>">
                <input type="hidden" id="BoxType" value="<?php echo $type; ?>">
                <input type="hidden" id="RibbonColour" value="<?php echo $ribbons[0]; ?>">
                <button class="btn btn-secondary continueBTN" disabled onclick="continueToNext();">Continue</button>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $('.continueBTN').attr('disabled', false);
    });

    $(document).on('click', '.selectBox', function () {
        showCustomLoader();
        var box_id = $(this).data('box_id');
        $('#BoxID').val(box_id);
        var box_image = $(this).children('.imgbox').children('img').attr('src');
        setTimeout(function () {
            $('.box_detail').children('img').attr('src', box_image);
            hideCustomLoader()
        }, 500);
    });
    $(document).on('click', '.selectRibbon', function () {
        showCustomLoader();
        var ribbon_color = $(this).data('ribbon_color');
        $('#RibbonColour').val(ribbon_color);
        var ribbon = $(this).children('img').attr('src');
        setTimeout(function () {
            $('.ribbon_detail').children('img').attr('src', ribbon);
            hideCustomLoader()
        }, 500);
    });

    function continueToNext() {
        var box_id = $('#BoxID').val();
        var ribbon_color = $('#RibbonColour').val();
        var box_type = $('#BoxType').val();
        if (box_id !== '' && ribbon_color !== '') {
            showCustomLoader();
            box_id = btoa(box_id);
            ribbon_color = btoa(ribbon_color);
            window.location.href = base_url + "customize/" + box_type + "/" + box_id + "/" + ribbon_color;
        } else {
            showMessage('something missing', 'danger');
        }
    }
</script>

