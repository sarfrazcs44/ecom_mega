<style>
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
    .content.checkout .navarea li:nth-child(1) img.gold {display:none;}
    .content.checkout .navarea li:nth-child(1) img.gray {display:inline-block;}
    .content.checkout .navarea li:nth-child(1).active img.gold {display:inline-block;}
    .content.checkout .navarea li:nth-child(1).active img.gray {display:none;}
</style>
<section class="content products checkout pt10">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="navarea">
                    <li class="active">
                        <a data-toggle="tab" href="#shopping">
                        <img class="gray" src="<?php echo front_assets() ?>images/shopping_basket_gray_small.png">
                        <img class="gold" src="<?php echo front_assets() ?>images/shopping_basket_gold_small.png">
                            <span><?php echo lang('shopping_basket'); ?></span>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#wishlist">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <span><?php echo lang('wishlist'); ?></span>
                        </a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div id="shopping" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="text-center">
                                        <img class="gray" src="<?php echo front_assets() ?>images/noProductInCart.png">
                                        <p class="blueNoteText"><span>Looks like there are no items in your basket yet!</span></p>
                                    </div>
                                    <br>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div id="wishlist" class="tab-pane fade">
                        <div class="row">
                            <?php
                            foreach ($wishlist_items as $wishlist_item) {
                                if ($wishlist_item->ItemType == 'Product') {
                                    $url = base_url() . 'product/detail/' . productTitle($wishlist_item->ItemID);
                                    $image = base_url(get_images($wishlist_item->ItemID, 'product', false));
                                } elseif ($wishlist_item->ItemType == 'Collection') {
                                    $url = base_url() . 'collection/detail/' . collectionTitle($wishlist_item->ItemID);
                                    $image = base_url(get_images($wishlist_item->ItemID, 'collection', false));
                                }
                                ?>
                                <div class="col-md-2">
                                    <div class="inbox">
                                        <div class="imgbox">
                                            <img src="<?php echo $image; ?>">
                                        </div>
                                        <a href="<?php echo $url; ?>">
                                            <h4><?php echo $wishlist_item->Title; ?></h4>
                                            <h5><strong><?php echo $wishlist_item->Price; ?></strong> SAR</h5>
                                        </a>
                                        <a title="Click to add to your wishlist" href="javascript:void(0);"
                                           onclick="addToWishlist(<?php echo $wishlist_item->ItemID; ?>, '<?php echo $wishlist_item->ItemType; ?>');"><i
                                                    class="fa fa-heart <?php echo isLiked($wishlist_item->ItemID, $wishlist_item->ItemType); ?>"
                                                    id="item<?php echo $wishlist_item->ItemID; ?>"
                                                    aria-hidden="true"></i></a>
                                        <a href="javascript:void(0);" title="Click to add this to your cart"
                                           onclick="addWishlistToCart(<?php echo $wishlist_item->ItemID; ?>, '<?php echo ucfirst($wishlist_item->ItemType); ?>', '<?php echo $wishlist_item->Price; ?>','<?php echo $wishlist_item->IsCorporateProduct; ?>');">
                                            <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

