<div class="row">
    <?php shuffle($featured_categories);
    foreach($featured_categories as $key => $category){
        $bg_image = array(1,2,3);
        $keys = array_rand($bg_image);
        if($key > 2){
            break;
         }
        ?>      
    <div class="col-lg-4 d-md-none d-lg-flex mt-20">
        <div class="banner-img mb-sm-0 wow animate__animated animate__fadeInUp" data-wow-delay=".2s">
            <img src="<?php echo base_url(); ?>assets/frontend/imgs/banner/banner-<?php echo $bg_image[$keys] ?>.png" alt="" />
            <div class="banner-text">
                <h4><?php echo $category->Title; ?></h4>
                <a href="<?php echo base_url('product/index');?>/<?php echo strtolower(str_replace(' ','-',$category->Title)); ?><?php echo '-'.$category->CategoryID;?>" class="btn btn-xs">Shop Now <i class="fi-rs-arrow-small-right"></i></a>
            </div>
        </div>
    </div>
<?php } ?>
</div>