<?php $home = getPageContent(7); ?>
<style>
nav.navbar {
    height:125px;
}
.carousel {
    position: relative;
    display: block;
    width: 100%;
    height: calc(100vh - 130px);
}
.carousel-inner {
    height: 100%;
}
.slider .carousel-inner .item {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 100%;
    background-size: cover;
    background-position: center center;
    background-repeat:no-repeat;
}
.slider .carousel-inner .item > img {display:none;}
.owl-carousel.owl-drag .owl-item img {
    width: 192px;
    height: 192px;
}
</style>
<div class="slider homeEdSlider">
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <!-- Carousel items -->
        <div class="carousel-inner">
            <?php
            $i = 0;
            foreach ($slider_images as $slider_image) { ?>
                <div style="background-image:url('<?php echo base_url($slider_image->Image); ?>');" class="item <?php echo($i == 0 ? 'active' : ''); ?>">
                    <img src="<?php echo base_url($slider_image->Image); ?>">
                    <div class="carousel-caption">
                        <div class="wow fadeInRightBig" data-wow-animation="7s">
                        <br>
                            <h2>
                            <?php echo $slider_image->Title; ?><br>
                            <span><?php echo $slider_image->Description; ?></span>
                            </h2>
                            <?php
                            if ($slider_image->UrlLink !== '') { ?>
                                <a href="<?php echo $slider_image->UrlLink; ?>">
                                    <button type="button" class="btn btn-secondary"><?php echo lang('view_detail'); ?></button>
                                </a>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
                <?php $i++;
            }
            ?>
        </div>
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </div>
</div>
<section class="content one">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner">
                    <img src="<?php echo front_assets(); ?>images/logo2.png">
                    <?php echo $home->Description; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content two imgan">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <?php
                    $total_featured = 1;
                    foreach ($collections_featured as $val) {
                        $total_featured++;

                        $image = get_images($val->CollectionID, 'collection', false);

                        ?>
                        <li>
                            <a href="<?php echo $val->Link; ?>">
                                <div class="img-overlay"
                                     style="background-image: url(<?php echo base_url($val->HomeImage); ?>);">
                                </div>
                                <div class="titlebox">
                                    <h4><?php echo $val->Title; ?> <span></span></h4>
                                </div>
                            </a>
                        </li>
                    <?php }

                   /* if ((Count($collections_featured) < 4) && $collections) {

                        for ($i = $total_featured; $i < 5; $i++) {

                            if (isset($collections[$i]->IsFeatured) && $collections[$i]->IsFeatured == 0) {
                                $image = get_images($collections[$i]->CollectionID, 'collection', false)
                                ?>

                                <li>
                                    <a href="<?php echo $collections->Link; ?>">
                                        <div class="img-overlay"
                                             style="background-image: url(<?php echo base_url($collections[$i]->HomeImage); ?>);">
                                        </div>
                                        <div class="titlebox">
                                            <h4><?php echo(isset($collections[$i]->Title) ? $collections[$i]->Title : ''); ?>
                                                <span></span></h4>
                                        </div>
                                    </a>
                                </li>


                                <?php
                            }

                        }

                    }*/


                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php
if ($featured_products) { ?>
    <section class="content three">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h2><?php echo lang('featured'); ?> <span><?php echo lang('products'); ?></span></h2>
                    </div>
                    <div class="procarousel">
                        <div class="owl-carousel owl-theme brands-slider">
                            <?php
                            foreach ($featured_products as $featured_product) { ?>
                                <div class="item">
                                    <a href="<?php echo base_url() . 'product/detail/' . productTitle($featured_product->ProductID); ?>">
                                        <img src="<?php echo base_url(get_images($featured_product->ProductID, 'product', false)); ?>">
                                        <h4><?php echo $featured_product->Title; ?></h4>
                                    </a>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div class="text-center btn-row">
                            <a href="<?php echo base_url('product'); ?>"
                               class="btn btn-primary"><?php echo lang('view_all'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }
?>

<section class="content four imgan hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h2><?php echo lang('customize'); ?> <span><?php echo lang('your_order'); ?></span></h2>
                </div>
                <ul>

                    <?php if($customizations){
                        foreach($customizations as $customize){ ?>
                    
                    <li>
                        <!--<a href="<?php echo base_url('customize/boxes/choco_box'); ?>">-->
                            <a href="javascript:void(0);">

                            <div class="img-overlay"
                                 style="background-image: url('<?php echo base_url($customize->Image); ?>');">
                            </div>
                            <div class="titlebox">
                                <h4><?php echo $customize->Title; ?>
                                </h4>
                            </div>
                        </a>
                    </li>
                <?php } } ?>
                   
                    
                </ul>
            </div>
        </div>
    </div>
</section>
