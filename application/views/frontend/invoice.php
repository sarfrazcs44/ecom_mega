<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <title>Invoice</title>
  </head>
  <body>

<div class="container-fluid ">
	<div class="row mt-4 mx-3">
		<div class="col-md-6 bill-info">
			<div class="row">
				<div class="col-md-5 logo">
					<img src="<?php echo base_url('/assets/frontend/imgs/theme/logo.svg'); ?>">	
				</div>
				<div class="col-md-7 address">
					<h5>
						BILL FROM:
					</h5>
					<h5>
						Hato
					</h5>
					<p><?php echo $order->StoreTitle; ?><br>
					
					<?php echo $order->StoreCityTitle; ?><br>
					Kingdom of Saudi Arabia</p>
				</div>
			</div>
		</div>
		<div class="col-md-6 shopping-info">
			<div class="row receipt-no">
				<div class="col-md-6">
					<h4 class="reg-no">VAT REG No.</h4>
					<h3><?php echo $order->VatNo; ?></h3>	
				</div>
				<div class="col-md-6">
					<!--<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/barCode.jpg" class="img-responsive">-->
					<p style="text-align:right;"> 
						<a href="javascript:void(0);" onclick="window.print();">
							<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/print.jpg" height="28" width="28">
						</a></p>
				</div>	
			</div>
		</div>
	</div>
<div class="row  mt-2 mx-3">
    <div class="col-md-6 bill-info">
		<div class="row">
			<div class="col-md-12">
				<div class="edHeading">
					<h3>BILL TO:</h3>
				</div>
				<div class="row">
					<div class="col-md-5 client-info">
						<strong class="haveIcon"><img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/userIcon.jpg"> <?php echo $order->FullName; ?></strong>
						<p><?php echo $order->MobileNo; ?></p>
					</div>
					<div class="col-md-7 client-info">
						
						<strong class="haveIcon"><img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/MapIcon.jpg"> Address</strong>
						<p><?php echo $order->Street; ?><br>
						<?php echo $order->BuildingNo; ?><br>
						<?php echo $order->AddressCity; ?><br>
						Kingdom of Saudi Arabia</p>
					</div>	
				</div>
			</div>
		</div>
	</div>
	<!--2nd Column-->
	<div class="col-md-6 shopping-info">
		<div class="edHeading">
			<div class="row">
				<div class="col-md-6"><h3>ORDER DETAILS:</h3></div>
				<div class="col-md-6">
					<h3><img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/userIcon.jpg">CUSTOMER ID: <?php echo $order->UserID; ?></h3>
				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-6 order-info">
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/invoiceNo.jpg" height="28" width="28" class="icon">
		<strong>Invoice No. :</strong>
		<p><?php echo $order->OrderNumber; ?></p>	
		</div>
		<div class="col-md-6 order-info">
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/calender.jpg" height="28" width="28" class="icon">
		<strong>Date of issue:</strong>		
		<p><?php echo date('d/m/Y', strtotime($order->OrderCreateDate)); ?></p>	
		</div>

		<div class="col-md-6 order-info">
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/timeIcon.jpg" height="28" width="28" class="icon">
		<strong>Time of issue:</strong>
		<p><?php echo date('H:i a', strtotime($order->OrderCreateDate)); ?></p>	
		</div>
		<div class="col-md-6 order-info">
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/dropBox.jpg" height="28" width="28" class="icon">
		<strong>Estimated Delivery:</strong>
		<?php
$site_setting = site_settings();
$days_to_deliver = "+$site_setting->DaysToDeliver days";
?>		
		<p><?php echo date('d/m/Y', strtotime($days_to_deliver, strtotime($order->OrderCreateDate))); ?></p>	
		</div>

		<div class="col-md-6 order-info">
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/cardIcon.jpg" height="28" width="28" class="icon">
		<strong>Payment Menthod.:</strong>
		<p><?php echo $order->PaymentMethod; ?></p>	
		</div>
		<?php if ($order->CollectFromStore == 1)
{ ?>
		<div class="col-md-6 order-info">
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/pStore.jpg" height="28" width="28" class="icon">
		<strong>Pick Up Store:</strong>
		<p><?php echo $order->StoreTitle; ?> / <?php echo $order->StoreAddress; ?></p>	
		</div>
		<?php
} ?>
		<?php if ($order->CouponCodeUsed != '')
{ ?>
		<div class="col-md-6 order-info">
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/codeUSed.jpg" height="28" width="28" class="icon">
		<strong>Promo Code Used:</strong>
		<p><?php echo $order->CouponCodeUsed; ?></p>	
		</div>
		<?php
} ?>
		
		<?php if ($order->CollectFromStore == 0)
{ ?>

		<div class="col-md-6 order-info">
			<?php if ($order->ShipmentMethodID == 4)
    { ?>
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/typeShipment.jpg" height="28" width="28" class="icon">
	<?php
    }
    else
    { ?>
<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/shipping.jpg" height="28" width="28" class="icon">
	<?php
    } ?>
		<strong>Type of Shipment:</strong>
		<p><?php echo $order->ShipmentMethodTitle; ?></p>	
		</div>
		<?php
} ?>

		<div class="col-md-6 order-info">
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/trackOrder.jpg" height="28" width="28" class="icon">
		<strong>Tracking Order No.</strong>
		<p><?php echo $order->OrderNumber; ?></p>	
		</div>
		
		<div class="col-md-6 order-info">
		<img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/status.jpg" height="28" width="28" class="icon">
		<strong>Status:</strong>
		<p><?php echo $order->OrderStatusEn; ?></p>	
		</div>

	</div>
	</div>
</div>

<div class="row mt-4 mx-3 pb-4">
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<div class="edHeading">
					<h3>RECIPIENT INFORMATION:</h3>
				</div>
				<div class="row">
					<div class="col-md-5 client-info">
						<strong class="haveIcon"><img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/userIcon.jpg"> <?php echo $order->FullName; ?></strong> 
						<p><?php echo $order->MobileNo; ?></p>
					</div>
					<div class="col-md-7 client-info">
						<strong class="haveIcon"><img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/MapIcon.jpg"> Address</strong>
						<p><?php echo $order->Street; ?><br>
						<?php echo $order->BuildingNo; ?><br>
						<?php echo $order->AddressCity; ?><br>
						Kingdom of Saudi Arabia</p>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row mt-4 mx-3 mb-5">
	<div class="col-md-12">
		<div class="edHeading">
			<h3>Invoice:</h3>
		</div>
		<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center"><H6>SN</H6></th>   
                                <th class="text-center"><h6>SKU</h6></th>
                                <th class="text-center"><h6>Discription</h6></th>
                                <th class="text-center"><h6>Unit</h6></th>
                                <th class="text-center"><h6>QTY</h6></th>
                                <th class="text-center"><h6>Price</h6></th>
                                <th class="text-center"><h6>Sub Total</h6></th>
                                <!--<th class="text-center"><h6>VAT</h6></th>
                                <th class="text-center"><h6>VAT Rate</h6></th>
                                <th class="text-center"><h6>TOTAL</h6></th>-->
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
$i = 1;
$total_cost = 0;
$order_items = getOrderItems($order->OrderID);
foreach ($order_items as $item)
{
    $total_cost = $total_cost + ($item->Amount * $item->Quantity);
?>
                                    <tr>
                                        <td class="text-center"><?php echo $i; ?></td>
                                        <td class="text-center">
                                            <?php echo $item->SKU; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php
    echo $item->Description;
?>
                                        </td>
                                        <td class="text-center">
                                            <?php echo (($item->PriceType == 'kg') ? ' kg' : 'pcs'); ?>
                                        </td>
                                         <td class="text-center">
                                            <?php echo $item->Quantity; ?>
                                        </td>
                                        <td class="text-center"><?php echo number_format($item->Amount, 2); ?>
                                            SAR
                                        </td>
                                       
                                        <td class="text-center">
                                            <?php echo number_format($item->Amount * $item->Quantity, 2); ?>
                                            SAR
                                        </td>
                                    </tr>
                                    <?php $i++;
}
?>
                            
                            <tr>
                                <td class="text-right" colspan="6"><strong>Total</strong></td>
                                <td class="text-center clr"><?php echo number_format($total_cost, 2); ?> SAR</td>
                                
                                
                            </tr>
                        </tbody>
                        <tfoot>
                        	<?php if ($order->CollectFromStore == 0)
{ ?>
                        	<tr>
                        		<td class="text-left no-borders" colspan="3"><p>Transactions made based on gross total</p></td>
                        		<td class="text-right no-borders" colspan="3"><strong>Shipment Charges:</strong> <img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/shipping.jpg" class="td-icon"></td>
                        		<td class="text-center clr"><?php echo $order->TotalShippingCharges; ?> SAR</td>
                        	</tr>
                        <?php
} ?>
                        	<tr>
                        		<td class="text-right no-borders" colspan="6"><strong>Tax Paid:</strong><img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/cardDiscount.jpg" class="td-icon"></td>
                        		<td class="text-center clr"><?php echo number_format($order->TotalTaxAmount, 2); ?> SAR</td>
                        	</tr>
                        	<?php
if ($order->DiscountAvailed > 0)
{ ?>
                        	<tr>
                        		<td class="text-right no-borders" colspan="6"><strong>Discount:</strong><img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/cardDiscount.jpg" class="td-icon"></td>
                        		<td class="text-center clr"><?php echo number_format($order->DiscountAvailed, 2); ?> SAR</td>
                        	</tr>
                        <?php
} ?>
                        	<tr>
                        		<td class="text-right no-borders" colspan="6"><strong>Grand Total:</strong> <img src="<?php echo base_url(); ?>assets/frontend/imgs/invoice/grandTotal.jpg" class="td-icon"></td>
                        		<td class="text-center clr"><?php echo $order->TotalAmount; ?> SAR</td>
                        	</tr>
                        </tfoot>
                    </table>                      
                  </div>  
                </div>
            </div>  
        </div>
    </div>
	</div>
	
</div>

	<div class="row note">
		<div class="col-md-12">
		<h2 class="text-center">THANK YOU FOR YOUR BUSINESS!</h2>
		<!--<p><b>PLEASE NOTE:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
 </p>-->			
		</div>
	</div>		
</div>

<style type="text/css">
.edHeading {
	border-bottom: 3px solid black;
	margin: 0 0 10px;
    /* padding: 0 10px; */
}
.edHeading h3 {
    margin: 0 0 5px;
    font-size: 20px;
    font-weight: bolder;
    line-height: 24px;
}
.edHeading h3 img {
    height: 24px;
    display: inline-block;
    vertical-align: top;
    margin-right: 3px;
}
	.address p {
		font-size: 1rem;
	}
	.address h5 {
		font-size: 20px;
	}
	.bill-text {
		border-bottom: 3px solid black;
		width: 90%;
		
	}

	.recipient-info {
		border-bottom: 3px solid black;
		width: 90%;
	}
	
	.text {
		font-size: 20px;
	}
	.reg-no {
		margin-top: 20px;
	}
	.receipt-no {
		
		width: 90%;
		margin-bottom: 20px;
	}
	.details {
		border-bottom: 3px solid black;
	}
	.user-info {
		margin-bottom: 10px;
		border-bottom: 3px solid black;

	}
	.note {
		margin-top: 50px;
		margin-bottom: 30px;
		margin-left: 50px;
		margin-right: 50px;
		text-align: center; 
	}
	.note h2 {
		margin-bottom: 25px;
	}
	td.no-borders {
		border: none;
	}
	td.clr {
		background-color: #E5F6AA;
	} 
	.client-info p {
	    margin-left: 28px;
    font-size: 1rem;
	}
	.client-info strong {
		font-size: 20px;
	}
	strong.haveIcon {
    display: block;
    width: 100%;
    font-size: 1rem;
    line-height: 1.5;
}
strong.haveIcon img {
    display: inline-block;
    vertical-align: top;
    width: 24px;
    height: auto;
}
	.recipient-info p {
		margin-left: 40px;
		font-size: 1rem;
	}
	.recipient-info strong {
		font-size: 20px;
	}
	.order-info p {
		margin-left: 55px;
	}
	.icon {
		float: left;
		margin-right: 10px;
		margin-bottom: 15px;
		width: 48px;
		height: 48px;
	}
	.img-responsive {
		max-width: 100%;
	}
	.id-icon {
	padding: 0 5px 5px 5px;
    height: 40px;
	}
	.td-icon {
	padding: 0 5px 5px 5px;
    height: 40px;
	}
</style>
</body>
</html>