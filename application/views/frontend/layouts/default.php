<?php
if ($this->session->userdata('lang')) {
    $language = $this->session->userdata('lang');
} else {
    $result = getDefaultLanguage();
    if ($result) {
        $language = $result->ShortCode;
    } else {
        $language = 'EN';
    }
}
$data['lang'] = $language;
$data['language'] = $language;
$this->load->view('frontend/layouts/header', $data);
$this->load->view($view, $data);
$this->load->view('frontend/layouts/modals', $data);
$this->load->view('frontend/layouts/footer', $data);
?>