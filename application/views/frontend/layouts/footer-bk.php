<?php $site_settings = site_settings(); ?>
<div class="footer-widgets">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <h5><?php echo lang('newsletter_subscribe'); ?></h5>
                <form action="<?php echo base_url('page/newsletter_subscribe'); ?>" method="post" class="ajaxForm"
                      id="newsletterForm">
                    <div class="row">
                        <div class="col-md-8">
                            <input type="email" name="Email" class="form-control required"
                                   placeholder="<?php lang('email_placeholder') ?>">
                        </div>
                        <div class="col-md-4 p0">
                            <button type="submit" class="btn btn-secondary"><?php echo lang('subscribe'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-md-6 p0 text-center">
                        <h5><?php echo lang('payments_supported_by'); ?></h5>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="wbox">
                            <ul>
                                <li><img src="<?php echo front_assets(); ?>images/c1.png"></li>
                                <li><img src="<?php echo front_assets(); ?>images/c2.png"></li>
                                <li><img src="<?php echo front_assets(); ?>images/c3.png"></li>
                                <li><img src="<?php echo front_assets(); ?>images/c4.png"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<style>
.mx-3 {
    margin-right: 1rem;
    margin-left: 1rem;
}
footer.site-footer a {
    color: #fff;
    transition-duration:0.5s;
}
footer.site-footer a:hover {
    color:#f4ebd3
}
</style>
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <p><?php echo lang('copyright'); ?> @ <?php echo date('Y'); ?> <?php echo lang('ecommerce'); ?>
                    . <?php echo lang('all_rights_reserved'); ?>
                    <span class="mx-3">|</span>
                    <a href="javascript:void(0);" data-toggle="modal"
                       data-target="#TermsConditions">
                        <?php
                        $terms_content = getPageContent(9, $lang);
                        echo $terms_content->Title;
                        ?>
                    </a>
                    <span class="mx-3">|</span>
                    <a href="<?php echo base_url('privacy-policy'); ?>"><?php echo lang('privacy_policy'); ?></a>
                    <span class="mx-3">|</span>
                    <a href="<?php echo base_url('return-policy'); ?>"><?php echo lang('return_policy'); ?></a>
                    <span class="mx-3">|</span>
                    <!-- <a href="<?php echo base_url('site-map'); ?>"><?php echo lang('site_map'); ?></a>
                    <span class="mx-3">|</span> -->
                    <a href="<?php echo base_url('faq'); ?>"><?php echo lang('faq'); ?></a>
                </p>
            </div>
            <div class="col-md-4 col-sm-4 text-right">
                <ul>
                    <?php
                    if ($site_settings->FacebookUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->FacebookUrl; ?>" target="_blank">
                                <i class="fa fa-facebook-official" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->TwitterUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->TwitterUrl; ?>" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->GoogleUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->GoogleUrl; ?>" target="_blank">
                                <i class="fa fa-google" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->LinkedInUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->LinkedInUrl; ?>" target="_blank">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->InstagramUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->InstagramUrl; ?>" target="_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->YoutubeUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->YoutubeUrl; ?>" target="_blank">
                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="alert alert-dismissible text-center cookiealert" role="alert">
        <div class="cookiealert-container">
            <?php echo lang('cookies_usage_alert'); ?>
            <a href="http://cookiesandyou.com/" target="_blank"><?php echo lang('learn_more'); ?></a>
            <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
                <?php echo lang('got_it'); ?>
            </button>
        </div>
    </div>
</footer>
<script src="<?php echo front_assets(); ?>js/wow.js"></script>
<script src="<?php echo front_assets(); ?>js/owl.carousel.min.js"></script>
<script src="<?php echo front_assets(); ?>js/bootstrap-number-input.js"></script>
<script src="<?php echo front_assets(); ?>js/jquery.rateyo.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>
<script src="<?php echo front_assets(); ?>cookie_alert/cookiealert.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="<?php echo front_assets(); ?>js/sort_plugin/tinysort.js"></script>
<script src="<?php echo front_assets(); ?>js/sort_plugin/jquery.tinysort.js"></script>
<script src="<?php echo front_assets(); ?>js/sort_plugin/tinysort.charorder.js"></script>
<script src="<?php echo front_assets(); ?>int_tel_input/js/intlTelInput.js"></script>
<script src="<?php echo front_assets(); ?>js/gallery.js"></script>
<script src="<?php echo front_assets(); ?>js/custom.js?v=<?php echo rand(); ?>"></script>
<script src="<?php echo front_assets(); ?>js/functions.js?v=<?php echo rand(); ?>"></script>
<link href="<?php echo front_assets(); ?>css/magiczoomplus.css?v=<?php echo rand(); ?>" rel="stylesheet" type="text/css" media="screen"/>
<script src="<?php echo front_assets(); ?>js/magiczoomplus.js?v=<?php echo rand(); ?>"></script>
<?php if(isset($_GET['verify_mobile']) && $this->session->userdata('user')){ ?>
<script>
    $(document).ready(function () {
setTimeout(function(){ showVerifyMobile('Write mobile number for OTP.'); }, 1000);
    });
   
</script>
<?php    

}
?>
<script>
// $(function () {
 
//  $("#showRating").rateYo({

//    "rating" : 3.2,
//    "starSvg": "<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">"+
//                 "<path d="M12 6.76l1.379 4.246h4.465l-3.612 2.625 1.379"+
//                           " 4.246-3.611-2.625-3.612 2.625"+
//                           " 1.379-4.246-3.612-2.625h4.465l1.38-4.246zm0-6.472l-2.833"+
//                           " 8.718h-9.167l7.416 5.389-2.833 8.718 7.417-5.388"+
//                           " 7.416 5.388-2.833-8.718"+
//                           " 7.417-5.389h-9.167l-2.833-8.718z"></path>"+
//                "</svg>"
//  });
// });

// $(function () {
 
//  $("#showRating").rateYo({
//    ratedFill: "#E74C3C"
//  });

// });
    var mzOptions = {
        cssClass: "edImgZoomCus"
    };
    // $("#prodCatImg a.dropdown-item").hover( function() {
    //     var value=$(this).attr('data-image');
    //     $("#replaceImgHere").attr("src", value);
    // });
    $("#prodCatImg a.dropdown-item").hover( function() {
        var hoverId=$(this).attr('data-imgId');
        // $(".imbBoxRight .imagesURL").removeClass('active');
        // $(".imbBoxRight .imagesURL#"+hoverId).addClass('active');
        $(".imbBoxRight .imagesURL").fadeOut('');
        $(".imbBoxRight .imagesURL#"+hoverId).fadeIn('');
    });
</script>
<script>
    $(document).ready(function () {
        hideCustomLoader();
        $('.offered_product').tooltip();
        <?php
        if ($this->session->flashdata('message') && $this->session->flashdata('message') != '')
        { 
            if($this->session->flashdata('message_type')){
                $message_type = $this->session->flashdata('message_type');
            }else{
                $message_type = 'warning';
            }

            ?>
            showMessage('<?php echo $this->session->flashdata('message'); ?>', '<?php echo $message_type; ?>');
        <?php }
        ?>
        <?php if (!TermsAcceptedByUser())
        { ?>
            $('#TermsUpdatedModal').modal('show');
        <?php } ?>
    });
</script>
</body>
</html>