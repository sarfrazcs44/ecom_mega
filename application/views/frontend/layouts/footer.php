<footer class="main">
            <section class="newsletter mb-15 wow animate__animated animate__fadeIn">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="position-relative newsletter-inner">
                                <div class="newsletter-content">
                                    <h2 class="mb-20">
                                        Stay home & get your daily <br />
                                        needs from our shop
                                    </h2>
                                    <p class="mb-45">Start You'r Daily Shopping with <span class="text-brand">Nest Mart</span></p>
                                    <form class="form-subcriber d-flex">
                                        <input type="email" placeholder="Your emaill address" />
                                        <button class="btn" type="submit">Subscribe</button>
                                    </form>
                                </div>
                                <img src="<?php echo base_url(); ?>assets/frontend/imgs/banner/banner-9.png" alt="newsletter" />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section-padding footer-mid">
                <div class="container pt-15 pb-20">
                    <div class="footer-link-widget widget-install-app col wow animate__animated animate__fadeInUp d-flex justify-content-between" data-wow-delay=".5s">
                        <div>
                            <p class="mb-20">Install App</p>
                            <div class="download-app">
                                <a href="#" class="hover-up mb-sm-2 mb-lg-0"><img class="active" src="<?php echo base_url(); ?>assets/frontend/imgs/theme/app-store.jpg" alt="" /></a>
                                <a href="#" class="hover-up mb-sm-2"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/google-play.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div>
                            <p class="mb-20">Secured Payment Gateways</p>
                            <img class="" src="<?php echo base_url(); ?>assets/frontend/imgs/theme/payment-method.png" alt="" />
                        </div>
                    </div>
                </div>        
            </section>
            <div class="container pb-30 wow animate__animated animate__fadeInUp" data-wow-delay="0">
                <div class="row align-items-center">
                    <div class="col-12 mb-30">
                        <div class="footer-bottom"></div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <p class="font-sm mb-0">&copy; 2021, <strong class="text-brand">Hato</strong> Powered by Schöpfen for Sinditech<br />All rights reserved</p>
                    </div>
                    <div class="col-xl-4 col-lg-6 text-center d-none d-xl-block">
                        <div class="hotline d-lg-inline-flex mr-30">
                            <img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/phone-call.svg" alt="hotline" />
                            <p>1900 - 6666<span>Working 8:00 - 22:00</span></p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 text-end d-none d-md-block">
                        <div class="mobile-social-icon">
                            <h6>Follow Us</h6>
                            <a href="#"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-facebook-white.svg" alt="" /></a>
                            <a href="#"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-twitter-white.svg" alt="" /></a>
                            <a href="#"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-instagram-white.svg" alt="" /></a>
                            <a href="#"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-pinterest-white.svg" alt="" /></a>
                            <a href="#"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-youtube-white.svg" alt="" /></a>
                        </div>
                        <p class="font-sm">Up to 15% discount on your first subscribe</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Preloader Start -->
        <div id="preloader-active">
            <div class="preloader d-flex align-items-center justify-content-center">
                <div class="preloader-inner position-relative">
                    <div class="text-center">
                        <img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/loading.gif" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <!-- Vendor JS-->
        
        <script src="<?php echo base_url(); ?>assets/frontend/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/slick.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/jquery.syotimer.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/waypoints.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/wow.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/slider-range.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/perfect-scrollbar.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/magnific-popup.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/select2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/counterup.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/jquery.countdown.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/images-loaded.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/isotope.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/scrollup.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/jquery.vticker-min.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/jquery.theia.sticky.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/jquery.elevatezoom.js"></script>
        <!-- Template  JS -->
        <script src="<?php echo base_url(); ?>assets/frontend/js/plugins/leaflet.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/main.js?v=4.0"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/shop.js?v=4.0"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/bootstrap_notify/bootstrap-notify.js"></script>
        <script src="<?php echo front_assets(); ?>js/jquery.rateyo.min.js"></script>
        <script src="<?php echo front_assets(); ?>js/functions.js?v=<?php echo rand(); ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    </body>
</html>