<!-- Forgot Password Modal -->
<div id="ForgotPassword" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo lang('forgot_password'); ?></h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('account/resetPassword'); ?>" method="post" class="ajaxForm"
                      id="resetPasswordForm">
                    <input type="email" name="Email" placeholder="<?php echo lang('enter_registered_email_here'); ?>"
                           class="form-control required">
                    <button type="submit" class="btn btn-primary"><?php echo lang('request_password'); ?></button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Contact Success Message Modal -->
<div id="SuccessMsg" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p id="formMsg"><?php echo lang('Your Feedback has been submitted successfully!'); ?></p>
                <button type="button" class="btn btn-primary"
                        data-dismiss="modal"><?php echo lang('dismiss'); ?></button>
            </div>
        </div>
    </div>
</div>

<!-- Added To Cart Modal -->
<div id="addToCartMsgModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
                <h4 id="addToCartMsg"><?php echo lang('Your Feedback has been submitted successfully!'); ?></h4><!--  style="color: #BD9371;" -->
                <div class="d-flex justify-content-between">
                    <a href="javascript:void(0);" onclick="window.location.reload();">
                        <button type="button" class="btn btn-primary"><?php echo lang('continue_shopping'); ?></button>
                    </a>
                    <a href="<?php echo base_url('cart'); ?>">
                        <button type="button" class="btn btn-success"><?php echo lang('proceed_to_checkout'); ?></button>
                    </a>
                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Verify OTP Modal -->
<div id="verifyOTPModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p id="verifyOTPMsg" style="color: #BD9371;"><?php echo lang('Your Feedback has been submitted successfully!'); ?></p>
                <br>
                <input type="text" class="form-control" maxlength="4" id="OTP"
                       style="border: 1px solid #ddd !important;"
                       placeholder="<?php echo lang('write_otp_here_and_verify'); ?>">
                <a href="javascript:void(0);" onclick="verifyOTP();">
                    <button type="button"  class="btn btn-success"><?php echo lang('verify_otp_here'); ?></button>
                </a>
                <a href="javascript:void(0);" onclick="sendOTP();">
                    <button type="button" id="" class="btn btn-success regenerateOTP"><?php echo lang('resend_otp'); ?></button>
                </a>
                <div>Time left = <span id="timer"></span></div>
            </div>
        </div>
    </div>
</div>


<div id="verifyMobileOTPModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p id="verifyobileOTPMsg" style="color: #BD9371;"><?php echo lang('Your Feedback has been submitted successfully!'); ?></p>
                <br>
                <input type="text" class="form-control" maxlength="4" id="MobileOTP"
                       style="border: 1px solid #ddd !important;"
                       placeholder="<?php echo lang('write_otp_here_and_verify'); ?>">
                <a href="javascript:void(0);" onclick="verifyMobileOTP();">
                    <button type="button" class="btn btn-success"><?php echo lang('verify_otp_here'); ?></button>
                </a>
                <a href="javascript:void(0);" onclick="sendMobileOTP();">
                    <button type="button"  class="btn btn-success regenerateOTP"><?php echo lang('resend_otp'); ?></button>
                </a>
                <div>Time left = <span id="timer1"></span></div>
            </div>
        </div>
    </div>
</div>

<!-- Verify OTP Modal -->
<div id="verifyOTPMobileNumberModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p id="verifyMobileMsg" style="color: #BD9371;"><?php echo lang('Your Feedback has been submitted successfully!'); ?></p>
                <br>
                <input type="text" class="form-control"  name="Mobile" id="MobileNoForOTP"
                       style="border: 1px solid #ddd !important;"
                       placeholder="<?php echo lang('write_mobile_number'); ?> :  +96654xxxxxxx">
                <a href="javascript:void(0);" onclick="sendMobileOTP();">
                    <button type="button" class="btn btn-success"><?php echo lang('send_otp'); ?></button>
                </a>
            </div>
        </div>
    </div>
</div>


<!-- Forgot Password Modal -->
<div id="CMessage" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="removeMessageBoxClass();">&times;
                </button>
            </div>
            <div class="modal-body">
                <div class="titlebox">
                    <h6>Order ID <span id="OrderNumber">1192357862</span></h6>
                    <h6>Order total <span id="TotalAmount">250 SAR</span></h6>
                    <h6>Ticket Status <span id="TicketStatus">Ongoing</span></h6>
                </div>
                <div class="msgbox">
                    <div class="msgsent">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos veritatis quis aut, et
                            consequuntur, alias optio dolorum, ullam sequi nulla eum voluptas, tempore quisquam impedit
                            modi commodi atque. Doloremque, illum.</p>
                    </div>
                    <div class="msgreceive">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos veritatis quis aut, et
                            consequuntur, alias optio dolorum, ullam sequi nulla eum voluptas, tempore quisquam impedit
                            modi commodi atque. Doloremque, illum.</p>
                    </div>
                    <div class="msgsent">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos veritatis quis aut, et
                            consequuntur, alias optio dolorum, ullam sequi nulla eum voluptas, tempore quisquam impedit
                            modi commodi atque. Doloremque, illum.</p>
                    </div>
                </div>
                <div class="footerarea">
                    <form action="<?php echo base_url('ticket/saveMessage'); ?>" method="post" class="ticketMessage"
                          id="saveTicketMessage">
                        <input type="text" name="Message" class="form-control required">
                        <input type="hidden" name="TicketID" id="TicketIDForMessage">
                        <button type="submit" class="btn ticket_form_btn"><i
                                    class="fa fa-arrow-<?php echo($lang == 'rtl' ? 'left' : 'right'); ?>"
                                    aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add new address Modal -->
<!--<div id="NewAddress" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Address</h4>
            </div>
            <div class="modal-body">
                <form action="<?php /*echo base_url('address/save'); */ ?>" method="post" class="ajaxFormBk" id="addAddressBk">
                    <div class="form-field">
                        <label>Recipient Name</label>
                        <input type="text" name="RecipientName" placeholder="Write here" class="form-control required">
                    </div>
                    <div class="form-field">
                        <label>Mobile</label>
                        <input type="text" name="MobileNo" placeholder="Write here" class="form-control required">
                    </div>
                    <div class="form-field">
                        <label>Email Address</label>
                        <input type="email" name="Email" placeholder="Write here" class="form-control required">
                    </div>
                    <div class="form-field">
                        <label for="CityID">City</label>
                        <select class="form-control required" id="CityID" name="CityID"
                                onchange="getDistrictsForCity(this.value);">
                            <option value="">Please select city</option>
                            <?php
/*                            $cities = getCities();
                            foreach ($cities as $city) { */ ?>
                                <option value="<?php /*echo $city->CityID; */ ?>"><?php /*echo $city->Title; */ ?></option>
                            <?php /*}
                            */ ?>
                        </select>
                    </div>
                    <div class="form-field">
                        <label for="DistrictID">District</label>
                        <select class="form-control required DistrictDD" id="DistrictID" name="DistrictID">
                            <option value="">Please select city first</option>
                        </select>
                    </div>
                    <div class="form-field">
                        <label>Building No.</label>
                        <input type="text" name="BuildingNo" placeholder="Write here" class="form-control required">
                    </div>
                    <div class="form-field">
                        <label>Street</label>
                        <input type="text" name="Street" placeholder="Write here" class="form-control required">
                    </div>
                    <div class="form-field">
                        <label>P.O Box</label>
                        <input type="text" name="POBox" placeholder="Write here" class="form-control required">
                    </div>
                    <div class="form-field">
                        <label>Zip Code</label>
                        <input type="text" name="ZipCode" placeholder="Write here" class="form-control required">
                    </div>
                    <input type='hidden' name='Latitude' id='lat'>
                    <input type='hidden' name='Longitude' id='lng'>
                    <div class="form-field">
                        <h5>
                            <a data-toggle="modal"
                               data-target="#mapModal">
                                Google Pin Link
                            </a>
                        </h5>
                    </div>
                    <div class="btn-groups">
                        <button type="submit" class="btn btn-primary">Add</button>
                        <button type="button" data-dismiss="modal" class="btn btn-primary cancel">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>-->

<!-- Terms and conditions Modal -->
<?php
$terms_content = getPageContent(9, $lang);
?>
<div id="TermsConditions" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title"
                    style="margin: 0 0 10px;color: #BD9371;"><?php echo $terms_content->Title; ?></span></h5>
            </div>
            <div class="modal-body">
                <?php echo $terms_content->Description; ?>
            </div>
        </div>
    </div>
</div>

<!-- Map Modal -->
<div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                </button>
                <h5 class="modal-title" id="myModalLabel"><?php echo lang('select_location_on_map'); ?></h5>
            </div>
            <div class="modal-body">
                
                
                <div id="googleMap" style="height: 400px;"></div>
               
                
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary waves-effect waves-light"
                        data-dismiss="modal"><?php echo lang('done'); ?>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Offer Detail Message Modal -->
<div id="OfferModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h4 id="OfferTitleModal"></h4>
                <p id="OfferDescriptionModal"></p>
            </div>
        </div>
    </div>
</div>

<!-- Offer Detail Message Modal -->
<div id="ChocoboxDetailModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="ChocoboxDetailModalDescription">
            </div>
        </div>
    </div>
</div>

<!-- Chocoboxes Modal -->
<div id="ChocoboxesModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="ChocoboxesDetails">
            </div>
        </div>
    </div>
</div>

<!-- Our Terms Are Updated Modal -->
<div id="TermsUpdatedModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">
                <p><?php echo lang('Out terms and conditions are updated and you should have a look at those'); ?></p>
                <button type="button" class="btn btn-primary" onclick="showTerms();"><?php echo lang('View updated terms and conditions'); ?>
                </button>
            </div>
        </div>
    </div>
</div>