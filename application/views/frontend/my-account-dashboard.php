<style>
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
    .msgbox {
        overflow: scroll;
        height: 600px;
    }
    .msgreceive {
        padding-left: 40px !important;
    }

</style>
<section class="content products checkout address myaccount">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo lang('my_account'); ?> <span><?php echo $user->FullName; ?></span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="wbox accountBoxes">
                    
                    <h2><?php echo lang('Manage your Account'); ?><span class="small"><?php echo lang('Make is easier to check-out your cart. Always keep your information updated!'); ?></span></h2>
                    <ul class="nav nav-tabs ">
                        <li>
                            <a  href="<?php echo base_url('account/profile');?>">
                                <div class="imageBox"><img src="<?php echo front_assets(); ?>images/mAccountMyProfile.png" height="" width="" alt="My Profile" /></div>
                                <?php echo lang('my_profile'); ?>
                                <p class="text-truncate"><?php echo lang('View/Edit your Personal Information'); ?></p>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/orders');?>">
                                <div class="imageBox"><img src="<?php echo front_assets(); ?>images/mAccountOrders.png" height="" width="" alt="My Orders" /></div>
                                 <?php echo lang('my_orders'); ?>
                               
                                <p class="text-truncate"><?php echo lang('View all orders placed by you'); ?></p>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/addresses');?>">
                                <div class="imageBox"><img src="<?php echo front_assets(); ?>images/mAccountAddress.png" height="" width="" alt="My Address" /></div>
                                <?php echo lang('my_addresses'); ?>
                                <p class="text-truncate"><?php echo lang('Manage your shipping Addresses'); ?></p>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/wishlist');?>">
                            <div class="imageBox"><img src="<?php echo front_assets(); ?>images/mAccountWishList.png" height="" width="" alt="My Wishlist" /></div>
                                <?php echo lang('my_wishlist'); ?>
                                <p class="text-truncate"><?php echo lang('Manage all items you wish to buy later'); ?></p>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                    </ul>
                                <div class="clearfix"></div>
                    <div class="tab-content">
                        <div id="info" class="tab-pane fade in active">
                            <form action="javascript:void(0);" method="post" class="ajaxForm" id="updateProfileForm">
                                <ul>
                                 
                                    <li>
                                        <!-- <button type="submit" class="btn btn-success"><?php echo lang('save_changes'); ?></button> -->
                                        <?php if ($user->LastUnsuccessfulLogin !== '0000-00-00 00:00:00') { ?>
                                            <h6 class="changes"><?php echo lang('last_unsuccessful_login'); ?>
                                                <span><?php echo date('d.m.Y \| h:i A', strtotime($user->LastUnsuccessfulLogin)); ?></span>
                                            </h6>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (isset($_GET['p'])) { ?>
    <script>
        $('a[href="#<?php echo $_GET['p']; ?>"]').tab('show');
    </script>
<?php } ?>
<script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
    var pusher = new Pusher('a796cb54d7c4b4ae4893', {
        cluster: 'ap2',
        forceTLS: true
    });
    var channel = pusher.subscribe('Ecommerce_Ticket_Channel');
    channel.bind('Ecommerce_Ticket_Event', function (data) {
        var my_html = data.my_html;
        var TicketID = data.TicketID;
        $('.TicketID' + TicketID).html(my_html);
        $('.TicketID' + TicketID).animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });
    $(document).ready(function () {
        $('.msgbox').animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });
</script>
