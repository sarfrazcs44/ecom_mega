<style>
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
    .msgbox {
        overflow: scroll;
        height: 600px;
    }
    .msgreceive {
        padding-left: 40px !important;
    }
</style>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<section class="content products checkout address myaccount">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo lang('my_account'); ?> <span><?php echo $user->FullName; ?></span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="wbox">
                    <ul class="nav nav-tabs tabsInBoxes">
                        <li class="active">
                            <a  href="<?php echo base_url('account/profile');?>">
                                <?php echo lang('my_information'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-user"></i></span>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/orders');?>">
                                <?php echo lang('orders'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-shopping-cart"></i></span>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/addresses');?>">
                                <?php echo lang('my_addresses'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-address-card"></i></span>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/wishlist');?>">
                                <?php echo lang('wishlist_items'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-heart"></i></span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane fade in active">
                            <form action="<?php echo base_url('account/updateProfile'); ?>" method="post"
                                  class="ajaxForm" id="updateProfileForm">
                                <ul>
                                    <li>
                                        <label><?php echo lang('full_name'); ?></label>
                                        <input type="text" name="FullName" value="<?php echo $user->FullName; ?>"
                                               class="form-control required">
                                    </li>
                                    <li>
                                        <label><?php echo lang('mobile_no'); ?></label>
                                        <input type="tel" name="Mobile" value="<?php echo $user->Mobile; ?>"
                                               class="form-control phone number-only required">
                                    </li>
                                    <li>
                                        <label><?php echo lang('email'); ?></label>
                                        <input type="email" name="Email" value="<?php echo $user->Email; ?>"
                                               class="form-control required">
                                    </li>
                                    <li>
                                        <label><?php echo lang('password'); ?></label>
                                        <strong id="pfields"><?php echo lang('password_protected'); ?></strong>
                                        <div class="phiddenfields">
                                            <input type="password" name="OldPassword" placeholder="<?php echo lang('old_password'); ?>"
                                                   class="form-control password" disabled>
                                            <input type="password" name="NewPassword"
                                                   placeholder="<?php echo lang('new_password'); ?>" minlength="6"
                                                   class="form-control password" disabled>
                                            <input type="password" name="ConfirmPassword"
                                                   placeholder="<?php echo lang('confirm_new_password'); ?>" minlength="6"
                                                   class="form-control password" disabled>
                                        </div>
                                    </li>
                                    <li>
                                        <button type="submit" class="btn btn-success"><?php echo lang('save_changes'); ?></button>
                                       
                                        <a class="btn btn-danger btn-changes" href="javascript:void(0);"
                                           onclick="window.history.back();"><?php echo lang('discard'); ?></a>
                                           <br>
                                           <?php if ($user->LastUnsuccessfulLogin !== '0000-00-00 00:00:00') { ?>
                                            <h6 class="changes"><?php echo lang('last_unsuccessful_login'); ?>
                                                <span><?php echo date('d.m.Y \| h:i A', strtotime($user->LastUnsuccessfulLogin)); ?></span>
                                            </h6>
                                            <?php } ?>
                                    </li>
                                </ul>
                            </form>
                        </div>
                        
                        
                    </div>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>
</section>
<?php if (isset($_GET['p'])) { ?>
    <script>
        $('a[href="#<?php echo $_GET['p']; ?>"]').tab('show');
    </script>
<?php } ?>
<script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
    var pusher = new Pusher('a796cb54d7c4b4ae4893', {
        cluster: 'ap2',
        forceTLS: true
    });
    var channel = pusher.subscribe('Ecommerce_Ticket_Channel');
    channel.bind('Ecommerce_Ticket_Event', function (data) {
        var my_html = data.my_html;
        var TicketID = data.TicketID;
        $('.TicketID' + TicketID).html(my_html);
        $('.TicketID' + TicketID).animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });
    $(document).ready(function () {
        $('.msgbox').animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });
</script>
