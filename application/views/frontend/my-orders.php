<style>
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
    
    .msgbox {
        overflow: scroll;
        height: 600px;
    }
    
    .msgreceive {
        padding-left: 40px !important;
    }
</style>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<section class="content products checkout address myaccount">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo lang('my_account'); ?> <span><?php echo $user->FullName; ?></span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="wbox">
                    <ul class="nav nav-tabs tabsInBoxes">
                        <li>
                            <a href="<?php echo base_url('account/profile');?>">
                                <?php echo lang('my_information'); ?>
                                    <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                    <span class="iconEd"><i class="fa fa-user"></i></span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="<?php echo base_url('account/orders');?>">
                                <?php echo lang('orders'); ?>
                                    <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                    <span class="iconEd"><i class="fa fa-shopping-cart"></i></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('account/addresses');?>">
                                <?php echo lang('my_addresses'); ?>
                                    <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                    <span class="iconEd"><i class="fa fa-address-card"></i></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('account/wishlist');?>">
                                <?php echo lang('wishlist_items'); ?>
                                    <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                    <span class="iconEd"><i class="fa fa-heart"></i></span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="orders" class="tab-pane fade in active">
                            <style>
                                .edOrderListStyle > a.list-group-item {
                                    margin-bottom: 30px !important;
                                }
                                
                                .edOrderListStyle .ostatus.odetails {
                                    padding: 0 !important;
                                    background-color: #f7f7f7 !important;
                                    border: 0 !important;
                                    border-radius: 0 !important;
                                }
                                
                                .orderHeader .infos {
                                    display: -ms-flexbox!important;
                                    display: flex!important;
                                    -ms-flex-pack: justify!important;
                                    justify-content: space-between!important;
                                    -ms-flex-align: stretch!important;
                                    align-items: stretch!important;
                                }
                                
                                .edOrderListStyle .ostatus.odetails .row {
                                    border-bottom: 0 !important;
                                    padding-bottom: 0 !important;
                                    margin-bottom: 0 !important;
                                }
                                
                                .edOrderListStyle .ostatus.odetails .row {
                                    border-bottom: 0 !important;
                                    padding-bottom: 0 !important;
                                    margin-bottom: 0 !important;
                                }
                                
                                .edOrderListStyle .ostatus.odetails .orderHeader .infos li {
                                    margin: 0;
                                }
                                
                                .edOrderListStyle .orderHeader .infos li.dropdown {
                                    position: relative;
                                    z-index: 10;
                                }
                                
                                .edOrderListStyle .ostatus.odetails .orderHeader li .btn {
                                    top: 10px !important;
                                    z-index: 2;
                                }
                                
                                .edOrderListStyle .ostatus.odetails .orderHeader {
                                    margin: 0;
                                    border-radius: 5px;
                                    padding: 20px 10px !important;
                                }
                                
                                .content.myaccount .tab-content .ostatus ul ul.dropdown-menu {
                                    top: 27px;
                                    z-index: 0;
                                }
                                
                                .edOrderListStyle .card-body {
                                    background: #f3ebd5;
                                    padding: 0 20px 20px;
                                }
                                
                                .edOrderListStyle .ostatus.odetails .accordion .card {
                                    margin: 0 0 15px;
                                    border: 1px solid #ddd;
                                    border-radius: 10px;
                                    /* background: #f3ebd5; */
                                }
                                
                                .edOrderListStyle .ostatus.odetails .orderHeader {
                                    transition-duration: 0.35s;
                                }
                                
                                .edOrderListStyle .ostatus.odetails .orderHeader[aria-expanded="true"] {
                                    background: #f3ebd5;
                                }
                                
                                .edOrderListStyle .ostatus .wbox {
                                    margin-bottom: 25px;
                                    background-color: #ffffff;
                                    padding: 25px;
                                    border-radius: 6px;
                                    box-shadow: 3px 3px 13px -2px rgba(0, 0, 0, 0.59);
                                }
                                
                                .edOrderListStyle .ostatus a.btn.btn-success.btn-red {
                                    margin-top: 25px;
                                }
                                
                                .edOrderListStyle .ostatus .shippingVat ol {
                                    border-top: 1px solid #dddddd;
                                    padding: 11px 0 2px !important;
                                    float: left;
                                    width: 100%;
                                    text-align: center;
                                }
                                
                                .edOrderListStyle .ostatus .shippingVat ol li {
                                    color: #bd9371 !important;
                                    font-size: 18px;
                                    font-weight: bold;
                                    line-height: 1.5;
                                    margin: 0 !important;
                                }
                            </style>
                            <div class="list-group">
                                <div class="edOrderListStyle">
                                    <?php 
                                                if ($pending_orders && count($pending_orders) > 0) { ?>
                                    <a href="#OrderCat_1" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('pending_orders');?>
                                    </a>
                                    <div class="collapse" id="OrderCat_1" aria-expanded="false" style="">
                                        <div class="ostatus odetails">
                                            <div class="accordion" id="allOrderAccordion">
                                               
                                                 <?php
                                                    foreach ($pending_orders as $key => $order) { ?>
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <div class="row orderHeader collapsed" data-toggle="collapse" data-target="#pending<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo">
                                                            <div class="col-md-5">
                                                                <p>
                                                                    <?php echo lang('Order_No'); ?>:
                                                                    <span>   <?php echo $order->OrderNumber; ?>
                                                                       </span>, 
                                                                       <?php echo lang('Transaction_ID'); ?>: <span><?php echo $order->TransactionID; ?></span>
                                                                </p>
                                                               
                                                                <div class="header-progress-container">
                                                                   <img src="<?php echo front_assets(); ?>images/orderPending.svg" alt="Pending Order" height="" width=" ">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <ul class="infos">
                                                                    <li>
                                                                        <p><?php echo lang('Est_Delivery'); ?>
                                                                            <span> <?php
                                                                        $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Total_Cost'); ?>
                                                                            <span><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Paid_By'); ?>
                                                                            <span>
                                                                                    <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                                                </span>
                                                                        </p>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                                        <ul class="dropdown-menu">
                                                                            <li>
                                                                                <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" target="_blank" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                                            </li>
                                                                            
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="pending<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="wbox side leftbox">
                                                                        <?php
                                                                    $order_items = getOrderItems($order->OrderID);
                                                                    foreach ($order_items as $item) { ?>
                                                                        
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <?php
                                                                                if ($item->ItemType == 'Product') { ?>
                                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                                            <p><?php echo $item->Title; ?></p>
                                                                               
                                                                                <div class="price"><?php echo number_format($item->Amount, 2); ?> <?php echo lang('SAR'); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                       

                                                                    <?php } ?>
                                                                        <div class="row last">
                                                                            <div class="col-md-12 shippingVat text-center">
                                                                                <ol>
                                                                                    <li>
                                                                                                            <?php
                                                                                    $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                                    ?>
                                                                                                     <?php echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); ?>
                                                                                                <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                                        <?php echo lang('SAR'); ?></strong>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                             Total Tax Paid
                                                                                                            <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                                        <?php echo lang('SAR'); ?></strong>
                                                                                                        </li>
                                                                                   
                                                                                    
                                                                                </ol>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="col-md-12 grandTotal">
                                                                                <button class="btn btn-secondary">
                                                                                    <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    <?php echo lang('SAR'); ?></strong></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="wbox">
                                                                        
                                                                       
                                                                           <?php
                                                                    if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                                        <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                                
                                                                <?php } else { ?>
                                                                    <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                                    <ul>
                                                                        <li>
                                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('StreetName'); ?></span>
                                                                            <strong><?php echo $order->Street; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('CityName'); ?></span>
                                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('POBox'); ?></span>
                                                                            <strong><?php echo $order->POBox; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                                        </li>
                                                                    </ul>
                                                                    <?php } ?>
                                                                       
                                                                    </div>
                                                                    <div class="wbox">
                                                                        <h6><?php echo lang('Complain'); ?></h6>
                                                                        <?php
                                                                    if ($order->HasTicket == 1) {
                                                                        $HasTicket1Style = "display:block;";
                                                                        $HasTicket0Style = "display:none;";
                                                                    } else {
                                                                        $HasTicket1Style = "display:none;";
                                                                        $HasTicket0Style = "display:block;";
                                                                    }
                                                                    if ($order->IsClosed == 0) {
                                                                        $IsClosed = "Ongoing";
                                                                    } else if ($order->IsClosed == 1) {
                                                                        $IsClosed = "Closed";
                                                                    } else if ($order->IsClosed == 2) {
                                                                        $IsClosed = "Re-Opened";
                                                                    }
                                                                    ?>

                                                                    <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                                    <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                                        <?php echo lang('ComplainNo'); ?>.
                                                                        <span><?php echo $order->TicketNumber; ?></span>
                                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo $IsClosed; ?></span>
                                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                                    </p>
                                                                    <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                                        <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                                        </button>
                                                                    </a>
                                                                </div>
                                                                <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                                    <p><?php echo lang('if_you_did'); ?></p>
                                                                    <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?>
                                                                        </a>
                                                                </div>


                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php }  ?>
                                                
                                            </div>

                                        </div>
                                    </div>
                                <?php } ?>
                                <?php 
                                                if ($packed_orders && count($packed_orders) > 0) { ?>
                                    <a href="#OrderCat_2" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('packed_orders');?>
                                    </a>
                                    <div class="collapse" id="OrderCat_2" aria-expanded="false" style="">
                                        <div class="ostatus odetails">
                                            <div class="accordion" id="allOrderAccordion">
                                                
                                                <?php
                                                    foreach ($packed_orders as $key => $order) { ?>
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <div class="row orderHeader collapsed" data-toggle="collapse" data-target="#packed_orders<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo">
                                                            <div class="col-md-5">
                                                                <p>
                                                                    <?php echo lang('Order_No'); ?> :
                                                                    <span>   <?php echo $order->OrderNumber; ?>
                                                                       </span>, 
                                                                       <?php echo lang('Transaction_ID'); ?> : <span><?php echo $order->TransactionID; ?></span>
                                                                </p>
                                                               
                                                                <div class="header-progress-container">
                                                                    <img src="<?php echo front_assets(); ?>images/orderPacked.svg" alt="Pending Order" height="" width=" ">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <ul class="infos">
                                                                    <li>
                                                                        <p><?php echo lang('Est_Delivery'); ?>
                                                                            <span> <?php
                                                                        $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Total_Cost'); ?>
                                                                            <span><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Paid_By'); ?>
                                                                            <span>
                                                                                    <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                                                </span>
                                                                        </p>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                                        <ul class="dropdown-menu">
                                                                            <li>
                                                                                <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" target="_blank" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                                            </li>
                                                                           
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="packed_orders<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="wbox side leftbox">
                                                                        <?php
                                                                    $order_items = getOrderItems($order->OrderID);
                                                                    foreach ($order_items as $item) { ?>
                                                                        
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <?php
                                                                                if ($item->ItemType == 'Product') { ?>
                                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                                             <p><?php echo $item->Title; ?></p>
                                                                               
                                                                                <div class="price"><?php echo number_format($item->Amount, 2); ?> <?php echo lang('SAR'); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                       

                                                                    <?php } ?>
                                                                        <div class="row last">
                                                                            <div class="col-md-12 shippingVat text-center">
                                                                                <ol>
                                                                                    <li>
                                                                                                            <?php
                                                                                    $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                                    ?>
                                                                                                     <?php echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); ?>
                                                                                                <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                                        SAR</strong>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                             Total Tax Paid
                                                                                                            <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                                        <?php echo lang('SAR'); ?></strong>
                                                                                                        </li>
                                                                                   
                                                                                    
                                                                                </ol>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="col-md-12 grandTotal">
                                                                                <button class="btn btn-secondary">
                                                                                    <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    SAR</strong></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="wbox">
                                                                        <?php
                                                                    if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                                        <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                                
                                                                <?php } else { ?>
                                                                    <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                                    <ul>
                                                                        <li>
                                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('StreetName'); ?></span>
                                                                            <strong><?php echo $order->Street; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('CityName'); ?></span>
                                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('POBox'); ?></span>
                                                                            <strong><?php echo $order->POBox; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                                        </li>
                                                                    </ul>
                                                                    <?php } ?>
                                                                       
                                                                    </div>
                                                                    <div class="wbox">
                                                                        <h6><?php echo lang('Complain'); ?></h6>
                                                                        <?php
                                                                    if ($order->HasTicket == 1) {
                                                                        $HasTicket1Style = "display:block;";
                                                                        $HasTicket0Style = "display:none;";
                                                                    } else {
                                                                        $HasTicket1Style = "display:none;";
                                                                        $HasTicket0Style = "display:block;";
                                                                    }
                                                                    if ($order->IsClosed == 0) {
                                                                        $IsClosed = "Ongoing";
                                                                    } else if ($order->IsClosed == 1) {
                                                                        $IsClosed = "Closed";
                                                                    } else if ($order->IsClosed == 2) {
                                                                        $IsClosed = "Re-Opened";
                                                                    }
                                                                    ?>

                                                                    <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                                    <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                                        <?php echo lang('ComplainNo'); ?>.
                                                                        <span><?php echo $order->TicketNumber; ?></span>
                                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo $IsClosed; ?></span>
                                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                                    </p>
                                                                    <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                                        <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                                        </button>
                                                                    </a>
                                                                </div>
                                                                <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                                    <p><?php echo lang('if_you_did'); ?></p>
                                                                    <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?> a Complaint
                                                                        </a>
                                                                </div>


                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php }  ?>
                                            </div>

                                        </div>
                                    </div>
                                <?php }  ?>
                                <?php 
                                                if ($dispatch_orders && count($dispatch_orders) > 0) { ?>
                                    <a href="#OrderCat_3" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('dispatch_orders');?>
                                    </a>
                                    <div class="collapse" id="OrderCat_3" aria-expanded="false" style="">
                                        <div class="ostatus odetails">
                                            <div class="accordion" id="allOrderAccordion">
                                                
                                                <?php
                                                    foreach ($dispatch_orders as $key => $order) { ?>
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <div class="row orderHeader collapsed" data-toggle="collapse" data-target="#dispatch_orders<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo">
                                                            <div class="col-md-5">
                                                                <p>
                                                                    Order No:
                                                                    <span>   <?php echo $order->OrderNumber; ?>
                                                                       </span>, 
                                                                       Transaction ID : <span><?php echo $order->TransactionID; ?></span>
                                                                </p>
                                                               
                                                                <div class="header-progress-container">
                                                                    <img src="<?php echo front_assets(); ?>images/orderDispatched.svg" alt="Pending Order" height="" width=" ">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <ul class="infos">
                                                                    <li>
                                                                        <p><?php echo lang('Est_Delivery'); ?>
                                                                            <span> <?php
                                                                        $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Total_Cost'); ?>
                                                                            <span><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Paid_By'); ?>
                                                                            <span>
                                                                                    <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                                                </span>
                                                                        </p>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                                        <ul class="dropdown-menu">
                                                                            <li>
                                                                                <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" target="_blank" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                                            </li>
                                                                           
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="dispatch_orders<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="wbox side leftbox">
                                                                        <?php
                                                                    $order_items = getOrderItems($order->OrderID);
                                                                    foreach ($order_items as $item) { ?>
                                                                        
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <?php
                                                                                if ($item->ItemType == 'Product') { ?>
                                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                                             <p><?php echo $item->Title; ?></p>
                                                                               
                                                                                <div class="price"><?php echo number_format($item->Amount, 2); ?> <?php echo lang('SAR'); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                       

                                                                    <?php } ?>
                                                                        <div class="row last">
                                                                            <div class="col-md-12 shippingVat text-center">
                                                                                <ol>
                                                                                    <li>
                                                                                                            <?php
                                                                                    $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                                    ?>
                                                                                                     <?php echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); ?>
                                                                                                <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                                        <?php echo lang('SAR'); ?></strong>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                             Total Tax Paid
                                                                                                            <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                                        <?php echo lang('SAR'); ?></strong>
                                                                                                        </li>
                                                                                   
                                                                                    
                                                                                </ol>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="col-md-12 grandTotal">
                                                                                <button class="btn btn-secondary">
                                                                                    <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    <?php echo lang('SAR'); ?></strong></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="wbox">
                                                                        <?php
                                                                    if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                                        <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                                
                                                                <?php } else { ?>
                                                                    <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                                    <ul>
                                                                        <li>
                                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('StreetName'); ?></span>
                                                                            <strong><?php echo $order->Street; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('CityName'); ?></span>
                                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('POBox'); ?></span>
                                                                            <strong><?php echo $order->POBox; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                                        </li>
                                                                    </ul>
                                                                    <?php } ?>
                                                                       
                                                                    </div>
                                                                    <div class="wbox">
                                                                        <h6><?php echo lang('Complain'); ?></h6>
                                                                        <?php
                                                                    if ($order->HasTicket == 1) {
                                                                        $HasTicket1Style = "display:block;";
                                                                        $HasTicket0Style = "display:none;";
                                                                    } else {
                                                                        $HasTicket1Style = "display:none;";
                                                                        $HasTicket0Style = "display:block;";
                                                                    }
                                                                    if ($order->IsClosed == 0) {
                                                                        $IsClosed = "Ongoing";
                                                                    } else if ($order->IsClosed == 1) {
                                                                        $IsClosed = "Closed";
                                                                    } else if ($order->IsClosed == 2) {
                                                                        $IsClosed = "Re-Opened";
                                                                    }
                                                                    ?>

                                                                    <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                                    <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                                        <?php echo lang('ComplainNo'); ?>.
                                                                        <span><?php echo $order->TicketNumber; ?></span>
                                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo $IsClosed; ?></span>
                                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                                    </p>
                                                                    <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                                        <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                                        </button>
                                                                    </a>
                                                                </div>
                                                                <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                                    <p><?php echo lang('if_you_did'); ?></p>
                                                                    <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?>
                                                                        </a>
                                                                </div>


                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php }  ?>
                                            </div>

                                        </div>
                                    </div>
                                <?php } ?>
                                 <?php 
                                                if ($deliver_orders && count($deliver_orders) > 0) { ?>
                                    <a href="#OrderCat_4" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('delivered_orders');?>
                                    </a>
                                    <div class="collapse" id="OrderCat_4" aria-expanded="false" style="">
                                        <div class="ostatus odetails">
                                            <div class="accordion" id="allOrderAccordion">
                                                
                                                <?php
                                                    foreach ($deliver_orders as $key => $order) { ?>
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <div class="row orderHeader collapsed" data-toggle="collapse" data-target="#deliver_orders<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo">
                                                            <div class="col-md-5">
                                                                <p>
                                                                    Order No:
                                                                    <span>   <?php echo $order->OrderNumber; ?>
                                                                       </span>, 
                                                                       Transaction ID : <span><?php echo $order->TransactionID; ?></span>
                                                                </p>
                                                               
                                                                <div class="header-progress-container">
                                                                    <img src="<?php echo front_assets(); ?>images/orderDelivered.svg" alt="Pending Order" height="" width=" ">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <ul class="infos">
                                                                    <li>
                                                                        <p><?php echo lang('Est_Delivery'); ?>
                                                                            <span> <?php
                                                                        $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Total_Cost'); ?>
                                                                            <span><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Paid_By'); ?>
                                                                            <span>
                                                                                    <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                                                </span>
                                                                        </p>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                                        <ul class="dropdown-menu">
                                                                            <li>
                                                                                <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" target="_blank" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                                            </li>
                                                                            
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="deliver_orders<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="wbox side leftbox">
                                                                        <?php
                                                                    $order_items = getOrderItems($order->OrderID);
                                                                    foreach ($order_items as $item) { ?>
                                                                        
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <?php
                                                                                if ($item->ItemType == 'Product') { ?>
                                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                                             <p><?php echo $item->Title; ?></p>
                                                                               
                                                                                <div class="price"><?php echo number_format($item->Amount, 2); ?> <?php echo lang('SAR'); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                       

                                                                    <?php } ?>
                                                                        <div class="row last">
                                                                            <div class="col-md-12 shippingVat text-center">
                                                                                <ol>
                                                                                    <li>
                                                                                                            <?php
                                                                                    $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                                    ?>
                                                                                                     <?php echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); ?>
                                                                                                <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                                        SAR</strong>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                             Total Tax Paid
                                                                                                            <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                                        <?php echo lang('SAR'); ?></strong>
                                                                                                        </li>
                                                                                   
                                                                                    
                                                                                </ol>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="col-md-12 grandTotal">
                                                                                <button class="btn btn-secondary">
                                                                                    <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    SAR</strong></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="wbox">
                                                                        <?php
                                                                    if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                                        <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                                
                                                                <?php } else { ?>
                                                                    <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                                    <ul>
                                                                        <li>
                                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('StreetName'); ?></span>
                                                                            <strong><?php echo $order->Street; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('CityName'); ?></span>
                                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('POBox'); ?></span>
                                                                            <strong><?php echo $order->POBox; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                                        </li>
                                                                    </ul>
                                                                    <?php } ?>
                                                                       
                                                                    </div>
                                                                    <div class="wbox">
                                                                        <h6><?php echo lang('Complain'); ?></h6>
                                                                        <?php
                                                                    if ($order->HasTicket == 1) {
                                                                        $HasTicket1Style = "display:block;";
                                                                        $HasTicket0Style = "display:none;";
                                                                    } else {
                                                                        $HasTicket1Style = "display:none;";
                                                                        $HasTicket0Style = "display:block;";
                                                                    }
                                                                    if ($order->IsClosed == 0) {
                                                                        $IsClosed = "Ongoing";
                                                                    } else if ($order->IsClosed == 1) {
                                                                        $IsClosed = "Closed";
                                                                    } else if ($order->IsClosed == 2) {
                                                                        $IsClosed = "Re-Opened";
                                                                    }
                                                                    ?>

                                                                    <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                                    <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                                        <?php echo lang('ComplainNo'); ?>.
                                                                        <span><?php echo $order->TicketNumber; ?></span>
                                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo $IsClosed; ?></span>
                                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                                    </p>
                                                                    <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                                        <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                                        </button>
                                                                    </a>
                                                                </div>
                                                                <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                                    <p><?php echo lang('if_you_did'); ?></p>
                                                                    <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?>
                                                                        </a>
                                                                </div>


                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php }  ?>
                                            </div>

                                        </div>
                                    </div>
                                    <?php }  ?>
                                     <?php 
                                                if ($cancel_orders && count($cancel_orders) > 0) { ?>
                                    <a href="#OrderCat_5" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('cancelled_orders');?>
                                    </a>
                                    <div class="collapse" id="OrderCat_5" aria-expanded="false" style="">
                                        <div class="ostatus odetails">
                                            <div class="accordion" id="allOrderAccordion">
                                                
                                                
                                                <?php
                                                    foreach ($cancel_orders as $key => $order) { ?>
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <div class="row orderHeader collapsed" data-toggle="collapse" data-target="#cancel_orders<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo">
                                                            <div class="col-md-5">
                                                                <p>
                                                                    Order No:
                                                                    <span>   <?php echo $order->OrderNumber; ?>
                                                                       </span>, 
                                                                       Transaction ID : <span><?php echo $order->TransactionID; ?></span>
                                                                </p>
                                                               
                                                                <div class="header-progress-container">
                                                                    <img src="<?php echo front_assets(); ?>images/orderCancelled.svg" alt="Pending Order" height="" width=" ">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <ul class="infos">
                                                                    <li>
                                                                        <p><?php echo lang('Est_Delivery'); ?>
                                                                            <span> <?php
                                                                        $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Total_Cost'); ?>
                                                                            <span><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                                        </p>
                                                                    </li>
                                                                    <li>
                                                                        <p><?php echo lang('Paid_By'); ?>
                                                                            <span>
                                                                                    <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                                                </span>
                                                                        </p>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                                        <ul class="dropdown-menu">
                                                                            <li>
                                                                                <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" target="_blank" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                                            </li>
                                                                            
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="cancel_orders<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="wbox side leftbox">
                                                                        <?php
                                                                    $order_items = getOrderItems($order->OrderID);
                                                                    foreach ($order_items as $item) { ?>
                                                                        
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <?php
                                                                                if ($item->ItemType == 'Product') { ?>
                                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                                             <p><?php echo $item->Title; ?></p>
                                                                               
                                                                                <div class="price"><?php echo number_format($item->Amount, 2); ?> <?php echo lang('SAR'); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                       

                                                                    <?php } ?>
                                                                        <div class="row last">
                                                                            <div class="col-md-12 shippingVat text-center">
                                                                                <ol>
                                                                                    <li>
                                                                                                            <?php
                                                                                    $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                                    ?>
                                                                                                     <?php echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); ?>
                                                                                                <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                                        SAR</strong>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                             Total Tax Paid
                                                                                                            <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                                        <?php echo lang('SAR'); ?></strong>
                                                                                                        </li>
                                                                                   
                                                                                    
                                                                                </ol>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="col-md-12 grandTotal">
                                                                                <button class="btn btn-secondary">
                                                                                    <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    SAR</strong></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="wbox">
                                                                        <?php
                                                                    if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                                        <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                                
                                                                <?php } else { ?>
                                                                    <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                                    <ul>
                                                                        <li>
                                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('StreetName'); ?></span>
                                                                            <strong><?php echo $order->Street; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('CityName'); ?></span>
                                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('POBox'); ?></span>
                                                                            <strong><?php echo $order->POBox; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                                        </li>
                                                                        <li>
                                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                                        </li>
                                                                    </ul>
                                                                    <?php } ?>
                                                                       
                                                                    </div>
                                                                    <div class="wbox">
                                                                        <h6><?php echo lang('Complain'); ?></h6>
                                                                        <?php
                                                                    if ($order->HasTicket == 1) {
                                                                        $HasTicket1Style = "display:block;";
                                                                        $HasTicket0Style = "display:none;";
                                                                    } else {
                                                                        $HasTicket1Style = "display:none;";
                                                                        $HasTicket0Style = "display:block;";
                                                                    }
                                                                    if ($order->IsClosed == 0) {
                                                                        $IsClosed = "Ongoing";
                                                                    } else if ($order->IsClosed == 1) {
                                                                        $IsClosed = "Closed";
                                                                    } else if ($order->IsClosed == 2) {
                                                                        $IsClosed = "Re-Opened";
                                                                    }
                                                                    ?>

                                                                    <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                                    <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                                        <?php echo lang('ComplainNo'); ?>.
                                                                        <span><?php echo $order->TicketNumber; ?></span>
                                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo $IsClosed; ?></span>
                                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                                    </p>
                                                                    <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                                        <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                                        </button>
                                                                    </a>
                                                                </div>
                                                                <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                                    <p><?php echo lang('if_you_did'); ?></p>
                                                                    <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?>
                                                                        </a>
                                                                </div>


                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php }  ?>
                                            </div>

                                        </div>
                                    </div>
                                    <?php }  ?>
                                </div>

                                

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<?php if (isset($_GET['p'])) { ?>
    <script>
        $('a[href="#<?php echo $_GET['
            p ']; ?>"]').tab('show');
    </script>
    <?php } ?>
        <script>
            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;
            var pusher = new Pusher('a796cb54d7c4b4ae4893', {
                cluster: 'ap2',
                forceTLS: true
            });
            var channel = pusher.subscribe('Ecommerce_Ticket_Channel');
            channel.bind('Ecommerce_Ticket_Event', function(data) {
                var my_html = data.my_html;
                var TicketID = data.TicketID;
                $('.TicketID' + TicketID).html(my_html);
                $('.TicketID' + TicketID).animate({
                    scrollTop: $('.msgbox').prop("scrollHeight")
                }, 1000);
            });
            $(document).ready(function() {
                $('.msgbox').animate({
                    scrollTop: $('.msgbox').prop("scrollHeight")
                }, 1000);
            });
        </script>