<style>
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
    .msgbox {
        overflow: scroll;
        height: 600px;
    }
    .msgreceive {
        padding-left: 40px !important;
    }
</style>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<section class="content products checkout address myaccount">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo lang('my_account'); ?> <span><?php echo $user->FullName; ?></span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="wbox">
                    <ul class="nav nav-tabs tabsInBoxes">
                        <li>
                            <a  href="<?php echo base_url('account/profile');?>">
                                <?php echo lang('my_information'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-user"></i></span>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/orders');?>">
                                <?php echo lang('orders'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-shopping-cart"></i></span>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/addresses');?>">
                                <?php echo lang('my_addresses'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-address-card"></i></span>
                            </a>
                        </li>
                        <li class="active">
                            <a  href="<?php echo base_url('account/wishlist');?>">
                                <?php echo lang('wishlist_items'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-heart"></i></span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                       
                    <div id="favourite" class="tab-pane fade in active">
                            <div class="row nmp">
                                <?php
                                if ($wishlist_items && count($wishlist_items) > 0) {
                                    foreach ($wishlist_items as $wishlist_item) {
                                        ?>
                                        <div class="col-md-3">
                                            <div class="inbox">
                                                <div class="imgbox" style="background-image:url('<?php echo base_url(get_images($wishlist_item->ItemID, 'product', false)); ?>');">
                                                    <img src="<?php echo base_url(get_images($wishlist_item->ItemID, 'product', false)); ?>">
                                                </div>
                                                <a href="<?php echo base_url() . 'product/detail/' . productTitle($wishlist_item->ItemID); ?>">
                                                    <h4><?php echo $wishlist_item->Title; ?></h4>
                                                    <h5><strong><?php echo $wishlist_item->Price; ?></strong> <?php echo lang('SAR'); ?></h5>
                                                </a>
                                                <a title="<?php echo lang('click_to_add_to_your_wishlist'); ?>" href="javascript:void(0);"
                                                   onclick="addToWishlist(<?php echo $wishlist_item->ItemID; ?>, '<?php echo $wishlist_item->ItemType; ?>');"><i
                                                            class="fa fa-heart <?php echo isLiked($wishlist_item->ItemID, $wishlist_item->ItemType); ?>"
                                                            id="item<?php echo $wishlist_item->ItemID; ?>"
                                                            aria-hidden="true"></i></a>
                                                <a href="javascript:void(0);" title="<?php echo lang('click_to_add_this_to_your_cart'); ?>"
                                                   onclick="addWishlistToCart(<?php echo $wishlist_item->ItemID; ?>, '<?php echo ucfirst($wishlist_item->ItemType); ?>', '<?php echo $wishlist_item->Price; ?>');">
                                                    <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                                </a>
                                            </div>
                                        </div>
                                    <?php }
                                } else { ?>
                                    <div class="col-md-12">
                                        <p><?php echo lang('no_item_in_wishlist'); ?></p>
                                    </div>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (isset($_GET['p'])) { ?>
    <script>
        $('a[href="#<?php echo $_GET['p']; ?>"]').tab('show');
    </script>
<?php } ?>
<script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
    var pusher = new Pusher('a796cb54d7c4b4ae4893', {
        cluster: 'ap2',
        forceTLS: true
    });
    var channel = pusher.subscribe('Ecommerce_Ticket_Channel');
    channel.bind('Ecommerce_Ticket_Event', function (data) {
        var my_html = data.my_html;
        var TicketID = data.TicketID;
        $('.TicketID' + TicketID).html(my_html);
        $('.TicketID' + TicketID).animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });
    $(document).ready(function () {
        $('.msgbox').animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });
</script>
