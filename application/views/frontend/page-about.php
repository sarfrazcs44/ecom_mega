﻿<main class="main pages">
    <div class="page-header breadcrumb-wrap">
        <div class="container">
            <div class="breadcrumb">
                <a href="index.html" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                <span></span> Pages <span></span> About us
            </div>
        </div>
    </div>
    <div class="page-content pt-50">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-12 m-auto">
                    <?php if($about_page[0]->SectionOneActive == 1){ ?>
                    <section class="row align-items-center mb-50">
                        <div class="col-lg-6">
                            <?php if(file_exists($page_images[0]->ImageName) &&  $page_images[0]->ImageName != ''){

                                            $page_image = base_url($page_images[0]->ImageName);

                                        }else{
                                            $page_image =    base_url().'assets/frontend/imgs/page/about-1.png';

                                        } ?>
                            <img src="<?php echo $page_image; ?>" alt="" class="border-radius-15 mb-md-3 mb-lg-0 mb-sm-4" />
                        </div>
                        <div class="col-lg-6">
                            <div class="pl-25">
                                <h2 class="mb-30"><?php echo $about_us[0]->HeadingOne; ?></h2>
                                <p class="mb-25"><?php echo $about_us[0]->Description; ?></p>
                                <div class="carausel-3-columns-cover position-relative">
                                    <div id="carausel-3-columns-arrows"></div>
                                    <div class="carausel-3-columns" id="carausel-3-columns">
                                        <?php foreach($page_images as $image){ ?>
                                        <img src="<?php echo base_url($image->ImageName); ?>" alt="" />
                                        <?php } ?> 
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php } ?>
                    <?php if($about_page[0]->SectionTwoActive == 1){ ?>
                    <section class="text-center mb-50">
                        <h2 class="title style-3 mb-40"><?php echo $about_us[0]->HeadingTwo; ?></h2>
                        <div class="row">
                            <?php foreach($services as $service){ ?>
                            <div class="col-lg-4 col-md-6 mb-24">
                                <div class="featured-card">
                                    <?php if(file_exists($service->Image) &&  $service->Image != ''){
                                        $image = base_url($service->Image);
                                    } else {
                                        $image = base_url().'assets/frontend/imgs/theme/icons/icon-1.svg';
                                    } ?>
                                    <img src="<?php echo $image; ?>" alt="" />
                                    <h4><?php echo $service->Title; ?></h4>
                                    <p><?php echo $service->Description; ?></p>
                                    
                                </div>
                            </div>
                            <?php } ?>
                            
                        </div>
                    </section>
                    <?php } ?>
                    
                    <section class="row align-items-center mb-50">
                        <?php if($about_page[0]->SectionThreeActive == 1){ ?>
                        <div class="row mb-50 align-items-center">
                            <div class="col-lg-7 pr-30">
                                <?php if(file_exists($page_images[0]->ImageName) &&  $page_images[0]->ImageName != ''){

                                            $page_image = base_url($page_images[0]->ImageName);

                                        }else{
                                            $page_image =    base_url().'assets/frontend/imgs/page/about-5.png';

                                        } ?>
                                <img src="<?php echo $page_image; ?>" alt="" class="mb-md-3 mb-lg-0 mb-sm-4" />
                            </div>
                            <div class="col-lg-5">
                                <h4 class="mb-20 text-muted"><?php echo $about_us[0]->HeadingThree; ?></h4>
                                <h1 class="heading-1 mb-40"><?php echo $about_us[0]->DescriptionThree; ?></p>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="row">
                            <?php if($about_page[0]->SectionFourActive == 1){ ?>
                            <div class="col-lg-4 pr-30 mb-md-5 mb-lg-0 mb-sm-5">
                                <h3 class="mb-30"><?php echo $about_us[0]->HeadingFour; ?></h3>
                                <p><?php echo $about_us[0]->DescriptionFour; ?></p>
                            </div>
                            <?php } if($about_page[0]->SectionFiveActive == 1){ ?>
                            <div class="col-lg-4 pr-30 mb-md-5 mb-lg-0 mb-sm-5">
                                <h3 class="mb-30"><?php echo $about_us[0]->HeadingFive; ?></h3>
                                <p><?php echo $about_us[0]->DescriptionFive; ?></p>
                            </div>
                            <?php } if($about_page[0]->SectionSixActive == 1){ ?>
                            <div class="col-lg-4">
                                <h3 class="mb-30"><?php echo $about_us[0]->HeadingSix; ?></h3>
                                <p><?php echo $about_us[0]->DescriptionSix; ?></p>
                            </div>
                            <?php } ?>
                        </div>
                    </section>
                    
                </div>
            </div>
        </div>
        <?php if($about_page[0]->SectionEightActive == 1){ ?>
        <section class="container mb-50 d-none d-md-block">
            <div class="row about-count">
                <div class="col-lg-1-5 col-md-6 text-center mb-lg-0 mb-md-5">
                    <h1 class="heading-1"><span class="count"><?php echo $about_us[0]->CountOne; ?></span>+</h1>
                    <h4><?php echo $about_us[0]->SubHeadingOne; ?></h4>
                </div>
                <div class="col-lg-1-5 col-md-6 text-center">
                    <h1 class="heading-1"><span class="count"><?php echo $about_us[0]->CountTwo; ?></span>+</h1>
                    <h4><?php echo $about_us[0]->SubHeadingTwo; ?></h4>
                </div>
                <div class="col-lg-1-5 col-md-6 text-center">
                    <h1 class="heading-1"><span class="count"><?php echo $about_us[0]->CountThree; ?></span>+</h1>
                    <h4><?php echo $about_us[0]->SubHeadingThree; ?></h4>
                </div>
                <div class="col-lg-1-5 col-md-6 text-center">
                    <h1 class="heading-1"><span class="count"><?php echo $about_us[0]->CountFour; ?></span>+</h1>
                    <h4><?php echo $about_us[0]->SubHeadingFour; ?></h4>
                </div>
                <div class="col-lg-1-5 text-center d-none d-lg-block">
                    <h1 class="heading-1"><span class="count"><?php echo $about_us[0]->CountFive; ?></span>+</h1>
                    <h4><?php echo $about_us[0]->SubHeadingFive; ?></h4>
                </div>
            </div>
        </section>
        <?php } ?>
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-12 m-auto">
                    <section class="mb-50">
                        <h2 class="title style-3 mb-40 text-center"><?php echo $about_us[0]->HeadingSeven; ?></h2>
                        <div class="row">
                            <div class="col-lg-4 mb-lg-0 mb-md-5 mb-sm-5">
                                <h6 class="mb-5 text-brand"><?php echo $about_us[0]->HeadingSeven; ?></h6>
                                <h1 class="mb-30">Meet Our Expert Team</h1>
                                <p class="mb-30"><?php echo $about_us[0]->DescriptionSeven; ?></p>
                                
                                <a href="#" class="btn">View All Members</a>
                            </div>
                            <div class="col-lg-8">
                                <div class="row">
                                    <?php foreach($our_teams as $team){ ?>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="team-card">
                                            <?php if(file_exists($team->Image) &&  $team->Image != ''){

                                                $image = base_url($team->Image);

                                            }else{
                                                $image =    base_url().'assets/frontend/imgs/page/about-6.png';

                                            } ?>
                                            <img src="<?php echo $image; ?>" alt="" />
                                            <div class="content text-center">
                                                <h4 class="mb-5"><?php echo $team->FullName; ?></h4>
                                                <span><?php echo $team->Designation; ?></span>
                                                <div class="social-network mt-20">
                                                    <a href="<?php echo $team->FacebookUrl; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-facebook-brand.svg" alt="" /></a>
                                                    <a href="<?php echo $team->TwitterUrl; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-twitter-brand.svg" alt="" /></a>
                                                    <a href="<?php echo $team->InstagramUrl; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-instagram-brand.svg" alt="" /></a>
                                                    <a href="<?php echo $team->YoutubeUrl; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-youtube-brand.svg" alt="" /></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>