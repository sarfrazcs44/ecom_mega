﻿<main class="main pages">
    <div class="page-header breadcrumb-wrap">
        <div class="container">
            <div class="breadcrumb">
                <a href="<?php echo base_url(); ?>" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                <span></span> Pages <span></span> My Account
            </div>
        </div>
    </div>
    <div class="page-content pt-150 pb-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 m-auto">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="dashboard-menu">
                                <ul class="nav flex-column" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="dashboard-tab" data-bs-toggle="tab" href="#dashboard" role="tab" aria-controls="dashboard" aria-selected="false"><i class="fi-rs-settings-sliders mr-10"></i>Dashboard</a>
                                    </li>
                                    <li class="nav-item" id="orders-tab-li">
                                        <a class="nav-link" id="orders-tab" data-bs-toggle="tab" href="#orders" role="tab" aria-controls="orders" aria-selected="false"><i class="fi-rs-shopping-bag mr-10"></i>Orders</a>
                                    </li>
                                    
                                    <li class="nav-item">
                                        <a class="nav-link" id="address-tab" data-bs-toggle="tab" href="#address" role="tab" aria-controls="address" aria-selected="true"><i class="fi-rs-marker mr-10"></i>My Address</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="account-detail-tab" data-bs-toggle="tab" href="#account-detail" role="tab" aria-controls="account-detail" aria-selected="true"><i class="fi-rs-user mr-10"></i>Account details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('account/logout'); ?>"><i class="fi-rs-sign-out mr-10"></i>Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content account dashboard-content pl-50">
                                <div class="tab-pane fade active show" id="dashboard" role="tabpanel" aria-labelledby="dashboard-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="mb-0">Hello <?php echo $this->session->userdata['user']->FullName; ?>!</h3>
                                        </div>
                                        <div class="card-body">
                                            <p>
                                                From your account dashboard. you can easily check &amp; view your <a href="javascript:void(0);" id="recent_orders">recent orders</a>,<br />
                                                manage your <a href="javascript:void(0)" id="shipping_address">shipping and billing addresses</a> and <a href="javascript:void(0)" id="account_detail">edit your password and account details.</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="orders" role="tabpanel" aria-labelledby="orders-tab">
                                    <div class="card">
                                         <?php 
                                            if ($pending_orders && count($pending_orders) > 0) { ?>
                                        <div class="card-header">
                                           
                                            <h3 class="mb-0">Pending Orders</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Order No</th>
                                                            <th>Transaction ID</th>
                                                            <th>Deliver Date</th>
                                                            <th>Total</th>
                                                            <th>Paid by</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach ($pending_orders as $key => $order) { ?>
                                                        <tr>
                                                            <td><?php echo $order->OrderNumber; ?></td>
                                                            <td><?php echo $order->TransactionID; ?></td>
                                                            <td><?php $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></td>
                                                            <td><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></td>
                                                            <td><img src="<?php echo base_url(); ?>assets/frontend/imgs/payment/<?php echo $order->PaymentMethod; ?>.png" style="width: 70px !important;" alt="Payment Method"></td>
                                                            <td><a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" class="btn-small d-block" target="_blank">View</a></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php 
                                    if ($packed_orders && count($packed_orders) > 0) { ?>
                                        <div class="card-header">
                                           
                                            <h3 class="mb-0">Packed Orders</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Order NO</th>
                                                            <th>Transaction ID</th>
                                                            <th>Deliver Date</th>
                                                            <th>Total</th>
                                                            <th>Paid by</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach ($packed_orders as $key => $order) { ?>
                                                        <tr>
                                                            <td><?php echo $order->OrderNumber; ?></td>
                                                            <td><?php echo $order->TransactionID; ?></td>
                                                            <td><?php $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></td>
                                                            <td><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></td>
                                                            <td><img src="<?php echo base_url(); ?>assets/frontend/imgs/payment/<?php echo $order->PaymentMethod; ?>.png" style="width: 70px !important;" alt="Payment Method"></td>
                                                            <td><a href="#" class="btn-small d-block">View</a></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php 
                                    if ($dispatch_orders && count($dispatch_orders) > 0) { ?>
                                        <div class="card-header">
                                           
                                            <h3 class="mb-0">Dispatched Orders</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Order No</th>
                                                            <th>Transaction ID</th>
                                                            <th>Deliver Date</th>                                                            
                                                            <th>Total</th>
                                                            <th>Paid by</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach ($dispatch_orders as $key => $order) { ?>
                                                        <tr>
                                                            <td><?php echo $order->OrderNumber; ?></td>
                                                            <td><?php echo $order->TransactionID; ?></td>
                                                            <td><?php $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></td>
                                                            <td><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></td>
                                                            <td><img src="<?php echo base_url(); ?>assets/frontend/imgs/payment/<?php echo $order->PaymentMethod; ?>.png" style="width: 70px !important;" alt="Payment Method"></td>
                                                            <td><a href="#" class="btn-small d-block">View</a></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php 
                                    if ($deliver_orders && count($deliver_orders) > 0) { ?>
                                        <div class="card-header">
                                           
                                            <h3 class="mb-0">Dispatched Orders</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Order No</th>
                                                            <th>Transaction ID</th>
                                                            <th>Deliver Date</th>
                                                            <th>Total</th>
                                                            <th>Paid by</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach ($deliver_orders as $key => $order) { ?>
                                                        <tr>
                                                            <td><?php echo $order->OrderNumber; ?></td>
                                                            <td><?php echo $order->TransactionID; ?></td>
                                                            <td><?php $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></td>
                                                            <td><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></td>
                                                            <td><img src="<?php echo base_url(); ?>assets/frontend/imgs/payment/<?php echo $order->PaymentMethod; ?>.png" style="width: 70px !important;" alt="Payment Method"></td>
                                                            <td><a href="#" class="btn-small d-block">View</a></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php 
                                    if ($cancel_orders && count($cancel_orders) > 0) { ?>
                                        <div class="card-header">
                                           
                                            <h3 class="mb-0">Canceled Orders</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Order No</th>
                                                            <th>Transaction ID</th>
                                                            <th>Deliver Date</th>
                                                            <th>Total</th>
                                                            <th>Paid by</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach ($cancel_orders as $key => $order) { ?>
                                                        <tr>
                                                            <td><?php echo $order->OrderNumber; ?></td>
                                                            <td><?php echo $order->TransactionID; ?></td>
                                                            <td><?php $site_setting = site_settings();
                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                        ?>
                                                                        <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></td>
                                                            <td><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></td>
                                                            <td><img src="<?php echo base_url(); ?>assets/frontend/imgs/payment/<?php echo $order->PaymentMethod; ?>.png" style="width: 70px !important;" alt="Payment Method"></td>
                                                            <td><a href="#" class="btn-small d-block">View</a></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="track-orders" role="tabpanel" aria-labelledby="track-orders-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="mb-0">Orders tracking</h3>
                                        </div>
                                        <div class="card-body contact-from-area">
                                            <p>To track your order please enter your OrderID in the box below and press "Track" button. This was given to you on your receipt and in the confirmation email you should have received.</p>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <form class="contact-form-style mt-30 mb-50" action="#" method="post">
                                                        <div class="input-style mb-20">
                                                            <label>Order ID</label>
                                                            <input name="order-id" placeholder="Found in your order confirmation email" type="text" />
                                                        </div>
                                                        <div class="input-style mb-20">
                                                            <label>Billing email</label>
                                                            <input name="billing-email" placeholder="Email you used during checkout" type="email" />
                                                        </div>
                                                        <button class="submit submit-auto-width" type="submit">Track</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="address" role="tabpanel" aria-labelledby="address-tab">
                                    <a href="<?php echo base_url('address/add'); ?>"><button class="btn btn-primary"><?php echo lang('AddNewAddress'); ?></button></a>
                                    <?php if ($addresses && count($addresses) > 0){ 
                                        foreach ($addresses as $key => $address){ ?>
                                         
                                    <div class="row">

                                        <div class="col-lg-6">
                                            <div class="card mb-3 mb-lg-0">
                                                <div class="card-header">
                                                    <h3 class="mb-0"><?php echo ($key == 0 ? lang('primary') : lang('secondary')); ?></h3>
                                                </div>
                                                 
                                                <div class="card-body">
                                                    <address>
                                                        <?php echo $address->RecipientName; ?>,<br />
                                                        <?php echo $address->Street; ?>,<br />
                                                        <?php echo $address->DistrictTitle; ?>,<br />
                                                        <?php echo $address->CityTitle; ?> <br />
                                                    </address>
                                                    <p><?php echo $address->MobileNo; ?></p>
                                                    <a href="<?php echo base_url('address/edit/'.$address->AddressID);?>" class="btn-small">Edit</a>&emsp; | &emsp;
                                                    <a onclick="removeIt('address/delete', 'AddressID', <?php echo $address->AddressID ?>);" href="javascript:void(0);">Delete</a>
                                                </div>
                                            
                                            </div>
                                        </div>
                                    </div>
                                <?php } 
                                } ?>
                                
                                </div>
                                <div class="tab-pane fade" id="account-detail" role="tabpanel" aria-labelledby="account-detail-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Account Details</h5>
                                        </div>
                                        <div class="card-body">
                                            
                                            <form action="<?php echo base_url('account/updateProfile'); ?>" method="post"
                                                class="ajaxForm" id="updateProfileForm">
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label>Full Name *</label>
                                                        <input required="" class="form-control required" name="FullName" type="text" value="<?php echo $user->FullName; ?>" />
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>Mobile No *</label>
                                                        <input class="form-control phone number-only required" name="Mobile" type="tel" value="<?php echo $user->Mobile; ?>" />
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>Email Address</label>
                                                        <input value="<?php echo $user->Email; ?>" class="form-control" name="Email" type="email" readonly />
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>Current Password</label>
                                                        <input class="form-control" name="OldPassword" type="password" />
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>New Password </label>
                                                        <input type="password" class="form-control" name="NewPassword" minlength="6"  />
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>Confirm Password</label>
                                                        <input class="form-control" name="ConfirmPassword" type="password" minlength="6" />
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-fill-out submit font-weight-bold" name="submit" value="Submit">Save Change</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    $(document).ready(function(){
        $('#recent_orders').on('click',function(){
            $('#orders-tab').tab('show');
        });
        $('#shipping_address').on('click',function(){
            $('#address-tab').tab('show');
        });
        $('#account_detail').on('click',function(){
            $('#account-detail-tab').tab('show');
        });
    });
</script>