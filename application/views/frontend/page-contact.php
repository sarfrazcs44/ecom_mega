﻿<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4&libraries=places"></script>
<style>
   
    #map-canvas {
        height: 100%;
        width: 100%;
    }
   
</style>
<main class="main pages">
    <div class="page-header breadcrumb-wrap">
        <div class="container">
            <div class="breadcrumb">
                <a href="index.html" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                <span></span> Pages <span></span> Contact
            </div>
        </div>
    </div>
    <div class="page-content pt-50">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-12 m-auto">
                    <section class="row align-items-end mb-50">
                        <div class="col-lg-12 mb-lg-0 mb-md-5 mb-sm-5">
                            <h4 class="mb-20 text-brand"><?php echo $result[0]->HeadingOne; ?></h4>
                            <h1 class="mb-30"><?php echo $result[0]->HeadingTwo; ?></h1>
                            <p class="mb-20"><?php echo $result[0]->Description; ?></p>
                            <p class="mb-20"><?php echo $result[0]->DescriptionTwo; ?></p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <section class="container mb-50 d-none d-md-block">
            <div class="row">
                <div class="col-md-12 border-radius-15 overflow-hidden">
                    <input type="hidden" name="Latitude" class="latitude" id="lat" value="<?php $result[0]->Latitude; ?>">
                    <input type="hidden" name="Longitude" class="longitude" id="lng" value="<?php $result[0]->Longitude; ?>">
                    <input type="hidden"  class="reg-input-city">
                    
                    
                     <div id="map-canvas" style="height: 400px;"></div>
                   
                </div>
            </div>
        </section>
        
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-12 m-auto">
                    <section class="mb-50">
                        <div class="row mb-60">
                            <div class="col-md-4 mb-4 mb-md-0">
                                <h4 class="mb-15 text-brand">Office</h4>
                                <?php echo $result[0]->Address; ?><br />
                                
                                <abbr title="Phone">Phone:</abbr> <?php echo $result[0]->Mobile; ?><br />
                                <abbr title="Email">Email: </abbr><?php echo $result[0]->Email; ?><br />
                                <a class="btn btn-sm font-weight-bold text-white mt-20 border-radius-5 btn-shadow-brand hover-up"><i class="fi-rs-marker mr-5"></i>View map</a>
                            </div>
                            <div class="col-md-4 mb-4 mb-md-0">
                                <h4 class="mb-15 text-brand">Studio</h4>
                                <?php echo $result[0]->AddressTwo; ?><br />
                                
                                <abbr title="Phone">Phone:</abbr><?php echo $result[0]->MobileTwo; ?><br />
                                <abbr title="Email">Email: </abbr><?php echo $result[0]->EmailTwo; ?><br />
                                <a class="btn btn-sm font-weight-bold text-white mt-20 border-radius-5 btn-shadow-brand hover-up"><i class="fi-rs-marker mr-5"></i>View map</a>
                            </div>
                            <div class="col-md-4">
                                <h4 class="mb-15 text-brand">Shop</h4>
                                <?php echo $result[0]->AddressThree; ?><br />
                                
                                <abbr title="Phone">Phone:</abbr><?php echo $result[0]->MobileThree; ?><br />
                                <abbr title="Email">Email: </abbr><?php echo $result[0]->EmailThree; ?><br />
                                <a class="btn btn-sm font-weight-bold text-white mt-20 border-radius-5 btn-shadow-brand hover-up"><i class="fi-rs-marker mr-5"></i>View map</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-8">
                                <div class="contact-from-area padding-20-row-col">
                                    <h5 class="text-brand mb-10">Contact form</h5>
                                    <h2 class="mb-10">Drop Us a Line</h2>
                                    <p class="text-muted mb-30 font-sm">Your email address will not be published. Required fields are marked *</p>
                                    <form action="<?php echo base_url('page/saveContactForm'); ?>" class="contact-form-style mt-30 ajaxForm" id="contactForm" method="post" onsubmit="return false;">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="input-style mb-20">
                                                    <input name="FullName" placeholder="Full Name" type="text" class="form-control required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="input-style mb-20">
                                                    <input name="Email" placeholder="Your Email" type="email" class="form-control required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="input-style mb-20">
                                                    <input name="Mobile" placeholder="Your Phone" type="tel" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="input-style mb-20">
                                                    <input name="Subject" placeholder="Subject" type="text" onchange="contactUsSubject(this.value, 'contactForm');" />
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12">
                                                <div class="textarea-style mb-30">
                                                    <textarea name="Message" placeholder="Message" class="form-control required"></textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6 text-right">
                                                <button type="submit" class="submit submit-auto-width"><?php echo lang('submit'); ?></button>
                                            </div>

                                        </div>

                                    </form>
                                    <p class="form-messege"></p>
                                </div>
                            </div>
                            <div class="col-lg-4 pl-50 d-lg-block d-none">
                                <img class="border-radius-15 mt-50" src="assets/imgs/page/contact-2.png" alt="" />
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    function initialize() {
        var mapOptions, map, marker, searchBox, city,
        infoWindow = '',
        addressEl = document.querySelector( '#map-search' ),
        latEl = document.querySelector( '.latitude' ),
        longEl = document.querySelector( '.longitude' ),
        element = document.getElementById( 'map-canvas' );
        city = document.querySelector( '.reg-input-city' );
        mapOptions = {
            // How far the maps zooms in.
            zoom: 8,
            // Current Lat and Long position of the pin/
            center: new google.maps.LatLng( <?php echo $result[0]->Latitude; ?>, <?php echo $result[0]->Longitude; ?> ),
            // center : {
            //  lat: -34.397,
            //  lng: 150.644
            // },
            disableDefaultUI: false, // Disables the controls like zoom control on the map if set to true
            scrollWheel: true, // If set to false disables the scrolling on the map.
            draggable: true, // If set to false , you cannot move the map around.
            // mapTypeId: google.maps.MapTypeId.HYBRID, // If set to HYBRID its between sat and ROADMAP, Can be set to SATELLITE as well.
            // maxZoom: 11, // Wont allow you to zoom more than this
            // minZoom: 9  // Wont allow you to go more up.
            styles:[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "color": "#50456d"
            },
            {
                "saturation": "29"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#b31f1f"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dfd1ae"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#f00000"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text",
        "stylers": [
            {
                "color": "#1a1526"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ff0f0f"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ebe3cd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "color": "#892020"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-84"
            },
            {
                "lightness": "85"
            },
            {
                "gamma": "0.00"
            },
            {
                "weight": "1"
            },
            {
                "color": "#50456d"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#f6c866"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e98d58"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
]
        };
    /**
     * Creates the map using google function google.maps.Map() by passing the id of canvas and
     * mapOptions object that we just created above as its parameters.
     *
     */
    // Create an object map with the constructor function Map()
        map = new google.maps.Map( element, mapOptions ); // Till this like of code it loads up the map.
    /**
     * Creates the marker on the map
     *
     */
        marker = new google.maps.Marker({
            position: mapOptions.center,
             icon: '<?php echo base_url(); ?>assets/frontend/imgs/pin.svg',
            label: {text: '1' , color: 'white'},
            map: map,
            draggable: true
        });
    /**
     * Creates a search box
     */
         searchBox = new google.maps.places.SearchBox( addressEl );
    /**
     * When the place is changed on search box, it takes the marker to the searched location.
     */
    google.maps.event.addListener( searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces(),
            bounds = new google.maps.LatLngBounds(),
            i, place, lat, long, resultArray,
            addresss = places[0].formatted_address;
        for( i = 0; place = places[i]; i++ ) {
            bounds.extend( place.geometry.location );
            marker.setPosition( place.geometry.location );  // Set marker position new.
        }
        map.fitBounds( bounds );  // Fit to the bound
        map.setZoom( 15 ); // This function sets the zoom to 15, meaning zooms to level 15.
        // console.log( map.getZoom() );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        latEl.value = lat;
        longEl.value = long;
        resultArray =  places[0].address_components;
        // Get the city and set the city input value to the one selected
        for( var i = 0; i < resultArray.length; i++ ) {
            if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                citi = resultArray[ i ].long_name;
                city.value = citi;
            }
        }
        // Closes the previous info window if it already exists
        if ( infoWindow ) {
            infoWindow.close();
        }
        /**
         * Creates the info Window at the top of the marker
         */
        infoWindow = new google.maps.InfoWindow({
            content: addresss
        });
        infoWindow.open( map, marker );
    } );
    /**
     * Finds the new position of the marker when the marker is dragged.
     */
    google.maps.event.addListener( marker, "dragend", function ( event ) {
        var lat, long, address, resultArray, citi;
        console.log( 'i am dragged' );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { latLng: marker.getPosition() }, function ( result, status ) {
            if ( 'OK' === status ) {  // This line can also be written like if ( status == google.maps.GeocoderStatus.OK ) {
                address = result[0].formatted_address;
                resultArray =  result[0].address_components;
                // Get the city and set the city input value to the one selected
                for( var i = 0; i < resultArray.length; i++ ) {
                    if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                        citi = resultArray[ i ].long_name;
                        console.log( citi );
                        city.value = citi;
                    }
                }
                addressEl.value = address;
                latEl.value = lat;
                longEl.value = long;
            } else {
                console.log( 'Geocode was not successful for the following reason: ' + status );
            }
            // Closes the previous info window if it already exists
            if ( infoWindow ) {
                infoWindow.close();
            }
            /**
             * Creates the info Window at the top of the marker
             */
            infoWindow = new google.maps.InfoWindow({
                content: address
            });
            infoWindow.open( map, marker );
        } );
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>