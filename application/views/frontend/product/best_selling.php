<?php foreach ($bestSellingProducts as $key => $product){ ?>
<div class="product-cart-wrap">
    <div class="product-img-action-wrap">
        <div class="product-img product-img-zoom">
            <a href="<?php echo base_url('product/detail');?>/<?php echo strtolower(str_replace(' ','-',$product->Title)); ?><?php echo '-'.$product->ProductID;?>">
                <img class="default-img" src="<?php echo base_url(); ?>assets/frontend/imgs/shop/product-5-1.jpg" alt="" />
                <img class="hover-img" src="<?php echo base_url(); ?>assets/frontend/imgs/shop/product-5-2.jpg" alt="" />
            </a>
        </div>
        <div class="product-action-1">
            <a aria-label="Quick view" class="action-btn small hover-up" data-bs-toggle="modal" data-bs-target="#quickViewModal"> <i class="fi-rs-eye"></i></a>
            <a aria-label="Add To Wishlist" class="action-btn small hover-up" href="shop-wishlist.html"><i class="fi-rs-heart"></i></a>
            <a aria-label="Compare" class="action-btn small hover-up" href="shop-compare.html"><i class="fi-rs-shuffle"></i></a>
        </div>
        <div class="product-badges product-badges-position product-badges-mrg">
            <span class="sale">Sale</span>
        </div>
    </div>
    <div class="product-content-wrap">
        <div class="product-category">
            <a href="shop-grid-right.html"><?php echo $product->BrandTitle; ?></a>
        </div>
        <h2><a href="<?php echo base_url('product/detail');?>/<?php echo strtolower(str_replace(' ','-',$product->Title)); ?><?php echo '-'.$product->ProductID;?>"><?php echo $product->Title; ?></a></h2>
        <div class="product-rate d-inline-block">

            <div class="product-rating" style="width: <?php echo $product->Rating * 100/5;?>%"></div>
        </div>
        <div class="product-price mt-10">
            <span><?php echo $product->Price; ?></span>
            <span class="old-price"><?php echo $product->Price; ?></span>
        </div>
        <div class="sold mt-15 mb-15">
            <div class="progress mb-5">
                <div class="progress-bar" role="progressbar" style="width: <?php echo $product->OrderCounts * 100/120;?>%" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <span class="font-xs text-heading"> Sold: <?php echo $product->OrderCounts; ?>/120</span>
        </div>
        <a href="javascript:void();" class="btn w-100 hover-up" onclick="addToCart(<?php echo $product->ProductID; ?>,1,'<?php echo ($product->Type == '' ? 'Product' : $product->Type); ?>',<?php echo $product->Price; ?>);"><i class="fi-rs-shopping-cart mr-5"></i>Add To Cart</a>
    </div>
</div>
<?php } ?>