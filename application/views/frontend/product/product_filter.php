<div class="sidebar-widget price_range range mb-30">
    <h5 class="section-title style-1 mb-30">Fill by price</h5>

    <div class="price-filter">
        <div class="price-filter-inner">
            <div id="slider-range" class="mb-20"></div>
            <div class="d-flex justify-content-between">
                <div class="caption">From: <strong id="slider-range-value1" class="text-brand"></strong></div>
                <div class="caption">To: <strong id="slider-range-value2" class="text-brand"></strong></div>
            </div>
        </div>
    </div>
    <div class="list-group">
        <div class="list-group-item mb-10 mt-10">
            <?php if($variants){
                 foreach($variants as $variant){
                    $attributes = getAttribute($variant->VariantID,$lang);
                    if(!empty($attributes)){
                        
             ?>
            <label class="fw-900"><?php echo $variant->Title; ?></label>
            <?php foreach($attributes as $attribute){ ?>
            <div class="custome-checkbox">
                <input class="form-check-input attribute-fill" type="checkbox" name="checkbox" id="<?php echo $attribute->VariantID; ?>" value="<?php echo $attribute->VariantID; ?>" />
                <label class="form-check-label" for="<?php echo $attribute->VariantID; ?>"><span><?php echo $attribute->Title; ?></span></label>
                <br />
               
                
            </div>
             <?php } } }?>
        <?php } ?>
            
    </div>
    <a href="javascript:void(0);" class="btn btn-sm btn-default btn-fillter"><i class="fi-rs-filter mr-5"></i> Fillter</a>
</div>
<script type="text/javascript">

$(document).ready(function () {
    $(".btn-fillter").click(function(){
    
        getProductsList(<?php echo $category_id; ?>, 1,<?php echo $sub_category; ?>);
        
    
    });

    
});

</script>