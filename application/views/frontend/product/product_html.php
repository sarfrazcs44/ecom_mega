<?php foreach ($products as $key => $product) { ?>
<div class="col-lg-1-5 col-md-4 col-12 col-sm-6">
    <div class="product-cart-wrap mb-30 wow animate__animated animate__fadeIn" data-wow-delay=".1s">
        <div class="product-img-action-wrap">
            <div class="product-img product-img-zoom">
                <a href="<?php echo base_url('product/detail');?>/<?php echo strtolower(str_replace(' ','-',$product->Title)); ?><?php echo '-'.$product->ProductID;?>">
                    <?php if(file_exists($product->ImageName) &&  $product->ImageName != ''){

                            $product_image = base_url($product->ImageName);

                    }else{
                        $product_image =    base_url().'assets/frontend/imgs/shop/product-1-1.jpg';

                    } ?>
                    <img class="default-img" src="<?php echo $product_image; ?>" alt="" />
                    <img class="hover-img" src="<?php echo $product_image; ?>" alt="" />
                </a>
            </div>
            <div class="product-action-1">
                <a aria-label="Add To Wishlist" class="action-btn" href="shop-wishlist.html"><i class="fi-rs-heart"></i></a>
                <a aria-label="Compare" class="action-btn" href="shop-compare.html"><i class="fi-rs-shuffle"></i></a>
                <a aria-label="Quick view" class="action-btn" data-bs-toggle="modal" data-bs-target="#quickViewModal"><i class="fi-rs-eye"></i></a>
            </div>
            <div class="product-badges product-badges-position product-badges-mrg">
                <span class="sale">Sale</span>
            </div>
        </div>
        
        <div class="product-content-wrap">
            <div class="product-category">
                <a href="shop-grid-right.html"><?php echo $product->CategoryTitle; ?></a>
            </div>
            <h2><a href="<?php echo base_url('product/detail');?>/<?php echo strtolower(str_replace(' ','-',$product->Title)); ?><?php echo '-'.$product->ProductID;?>"><?php echo $product->Title; ?></a></h2>
            <div class="product-rate-cover">
                <div class="product-rate d-inline-block">
                    <div class="product-rating" style="width: <?php echo $product->Rating* 100/5;?>%"></div>
                </div>
                <span class="font-small ml-5 text-muted"> (4.0)</span>
            </div>
            <div>
                <span class="font-small text-muted">By <a href="vendor-details-1.html"><?php echo $product->BrandTitle; ?></a></span>
            </div>
            <div class="product-card-bottom">
                <div class="product-price">
                    <span><?php echo $product->Price; ?></span>
                    <span class="old-price"><?php echo $product->Price; ?></span>
                </div>
                <div class="add-cart">
                    <a class="add" href="javascript:void(0);" onclick="addToCart(<?php echo $product->ProductID; ?>,1,'<?php echo ($product->Type == '' ? 'Product' : $product->Type); ?>',<?php echo $product->Price; ?>);"><i class="fi-rs-shopping-cart mr-5"></i>Add </a>
                </div>
            </div>
        </div>
    
    </div>

</div>
<?php } ?>