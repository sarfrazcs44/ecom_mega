<?php
$ProductDiscountedPrice = $product->Price;
$offer_product = checkProductIsInAnyOffer($product->ProductID);
if(!empty($offer_product)){
    
    $discountSign = '';

    $DiscountType = $offer_product['DiscountType'];
    $DiscountFactor = $offer_product['Discount'];
    if ($DiscountType == 'percentage') {
        $discountSign = '%';
        $Discount = ($DiscountFactor / 100) * $Price;
        if ($Discount > $Price) {
            $ProductDiscountedPrice = 0;
        } else {
            $ProductDiscountedPrice = $Price - $Discount;
        }
    } elseif ($DiscountType == 'per item') {
        $Discount = $DiscountFactor;
        if ($Discount > $Price) {
            $ProductDiscountedPrice = 0;
        } else {
            $ProductDiscountedPrice = $Price - $DiscountFactor;
        }
    } else {
        $Discount = 0;
        if ($Discount > $Price) {
            $ProductDiscountedPrice = 0;
        } else {
            $ProductDiscountedPrice = $Price;
        }
    }

}





$ratings = productRatings($product->ProductID);
if ($this->session->userdata('user')) {
    $IsProductPurchased = IsProductPurchased($this->session->userdata['user']->UserID, $product->ProductID);
    if ($IsProductPurchased) {
        $UserCanReviewRate = true;
    } else {
        $UserCanReviewRate = false;
    }
} else {
    $UserCanReviewRate = false;
}
                ?>
<main class="main">
    <div class="page-header breadcrumb-wrap">
        <div class="container">
            <div class="breadcrumb">
                <a href="index.html" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                <span></span> <a href="shop-grid-right.html"><?php echo $product->CategoryTitle; ?></a> <span></span> <?php echo $product->Title; ?>
            </div>
        </div>
    </div>
    <div class="container mb-30">
        <div class="row">
            <div class="col-xl-10 col-lg-12 m-auto">
                <div class="product-detail accordion-detail">
                    <div class="row mb-50 mt-30">
                        <div class="col-md-6 col-sm-12 col-xs-12 mb-md-0 mb-sm-5">
                            <div class="detail-gallery">
                                <span class="zoom-icon"><i class="fi-rs-search"></i></span>
                                <!-- MAIN SLIDES -->
                                <div class="product-image-slider">
                                    <figure class="border-radius-10">
                                        <?php if(file_exists($product->ImageName) &&  $product->ImageName != ''){

                                            $product_image = base_url($product->ImageName);

                                        }else{
                                            $product_image =    base_url().'assets/frontend/imgs/shop/product-1-1.jpg';

                                        } ?>
                                        <img src="<?php echo $product_image; ?>" alt="product image" />
                                    </figure>
                                    <?php foreach($product_images as $image){ 
                                            if(file_exists($image->ImageName) &&  $image->ImageName != ''){
                                        ?>
                                    <figure class="border-radius-10">
                                        <img src="<?php echo base_url($image->ImageName); ?>" alt="product image" />
                                    </figure>
                                   <?php } }  ?>
                                   
                                </div>
                                <!-- THUMBNAILS -->
                                <div class="slider-nav-thumbnails">
                                     <?php foreach($product_images as $image){ 
                                            if(file_exists($image->ImageName) &&  $image->ImageName != ''){
                                        ?>
                                    <div><img src="<?php echo base_url($image->ImageName); ?>" alt="product image" /></div>
                                     <?php } }  ?>
                                   
                                </div>
                            </div>
                            <!-- End Gallery -->
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="detail-info pr-30 pl-30">
                                <span class="stock-status out-stock"> Sale Off </span>
                                <h2 class="title-detail"><?php echo $product->Title; ?></h2>
                                <div class="product-detail-rating">
                                    <div class="product-rate-cover text-end">
                                        <div class="product-rate d-inline-block">
                                            <div class="product-rating" style="width: <?php echo $product->Rating* 100/5;?>%"></div>
                                        </div>
                                        <span class="font-small ml-5 text-muted"> (<?php echo $ratings['total_ratings_count']; ?>)</span>
                                    </div>
                                </div>
                                <div class="clearfix product-price-cover">
                                    <div class="product-price primary-color float-left">
                                        <span class="current-price text-brand"><?php echo $ProductDiscountedPrice; ?></span>
                                        <?php if(!empty($offer_product)){ ?>

                                        <span>
                                            <span class="save-price font-md color3 ml-15"><?php echo $DiscountFactor;?><?php echo $discountSign;?> Off</span>
                                            <span class="old-price font-md ml-15"><?php echo $product->Price; ?></span>
                                        </span>
                                    <?php } ?>
                                    </div>
                                </div>
                                <div class="short-desc mb-30">
                                    <p class="font-lg"><?php echo $product->Description; ?></p>
                                </div>

                                <?php foreach($variants as $value){ ?>
                                <div class="attr-detail attr-size mb-30">
                                    <strong class="mr-10"><?php echo $value['Title']; ?>: </strong>
                                    <ul class="list-filter size-filter font-small">
                                         <?php foreach($value['attributes'] as $attribute){ ?>
                                            <li><a href="#"><?php echo $attribute['Title']; ?></a></li>
                                         <?php } ?>
                                        
                                    </ul>
                                </div>
                            <?php } ?>
                                <div class="detail-extralink mb-50">
                                    <div class="detail-qty border radius">
                                        <a href="#" class="qty-down"><i class="fi-rs-angle-small-down"></i></a>
                                        <span class="qty-val">1</span>
                                        <a href="#" class="qty-up"><i class="fi-rs-angle-small-up"></i></a>
                                    </div>
                                    <div class="product-extra-link2">
                                        <button type="submit" class="button button-add-to-cart" onclick="addToCart(<?php echo $product->ProductID; ?>,1,'<?php echo ($product->Type == '' ? 'Product' : $product->Type); ?>',<?php echo $ProductDiscountedPrice; ?>);"><i class="fi-rs-shopping-cart"></i>Add to cart</button>
                                        <!--<a aria-label="Add To Wishlist" class="action-btn hover-up" href="shop-wishlist.html"><i class="fi-rs-heart"></i></a>
                                        <a aria-label="Compare" class="action-btn hover-up" href="shop-compare.html"><i class="fi-rs-shuffle"></i></a>-->
                                    </div>
                                </div>
                                <div class="font-xs">
                                    <ul class="mr-50 float-start">
                                        <li class="mb-5">Brand: <span class="text-brand"><?php echo $product->BrandTitle; ?></span></li>
                                        <li class="mb-5">Category: <span class="text-brand"><?php echo $product->CategoryTitle; ?></span></li>
                                        <li>Sub Category: <span class="text-brand"><?php echo $product->SubCategoryTitle; ?></span></li>
                                    </ul>
                                    <ul class="float-start">
                                        <li class="mb-5">SKU: <a href="#"><?php echo $product->SKU; ?></a></li>
                                        <?php if($tags) { ?>
                                        <li class="mb-5">Tags: <a href="#" rel="tag"></a><?php 

                                        foreach($tags as $tag){ ?><a><?php echo $tag->Title; ?></a> <?php } ?></li>
                                        <?php } ?>
                                        <li>Stock:<span class="in-stock text-brand ml-5">8 Items In Stock</span></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Detail Info -->
                        </div>
                    </div>
                    <div class="product-info">
                        <div class="tab-style3">
                            <ul class="nav nav-tabs text-uppercase">
                                <li class="nav-item">
                                    <a class="nav-link active" id="Description-tab" data-bs-toggle="tab" href="#Description">Description</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="Additional-info-tab" data-bs-toggle="tab" href="#Additional-info">Specifications</a>
                                </li>
                                <!--<li class="nav-item">
                                    <a class="nav-link" id="Vendor-info-tab" data-bs-toggle="tab" href="#Vendor-info">Vendor</a>
                                </li>-->
                                <li class="nav-item">
                                    <a class="nav-link" id="Reviews-tab" data-bs-toggle="tab" href="#Reviews">Reviews (<?php echo $ratings['total_ratings_count']; ?>)</a>
                                </li>
                            </ul>
                            <div class="tab-content shop_info_tab entry-main-content">
                                <div class="tab-pane fade show active" id="Description">
                                    <div class="">
                                        <p><?php echo $product->Description; ?></p>
                                        
                                        <h4 class="mt-30">Packaging & Delivery</h4>
                                        
                                        <p class="product-more-infor mt-30"><?php echo $product->Packaging; ?></p>

                                        <h4 class="mt-30">Suggested Use</h4>
                                        <p class="product-more-infor mt-30"><?php echo $product->SuggestedUse; ?></p>

                                        <h4 class="mt-30">Other Ingredients</h4>
                                        <p class="product-more-infor mt-30"><?php echo $product->Ingredients; ?></p>

                                        <h4 class="mt-30">Warnings</h4>
                                        <p class="product-more-infor mt-30"><?php echo $product->Warnings; ?></p>

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Additional-info">
                                    <table class="font-md">
                                        <tbody>
                                            <tr class="stand-up">
                                                
                                                <td>
                                                    <p></p><?php echo $product->Specifications; ?></p>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="Vendor-info">
                                    <div class="vendor-logo d-flex mb-30">
                                        <img src="<?php echo base_url(); ?>assets/frontend/imgs/vendor/vendor-18.svg" alt="" />
                                        <div class="vendor-name ml-15">
                                            <h6>
                                                <a href="vendor-details-2.html">Noodles Co.</a>
                                            </h6>
                                            <div class="product-rate-cover text-end">
                                                <div class="product-rate d-inline-block">
                                                    <div class="product-rating" style="width: 90%"></div>
                                                </div>
                                                <span class="font-small ml-5 text-muted"> (<?php echo $ratings['total_ratings_count']; ?> reviews)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="contact-infor mb-50">
                                        <li><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-location.svg" alt="" /><strong>Address: </strong> <span>5171 W Campbell Ave undefined Kent, Utah 53127 United States</span></li>
                                        <li><img src="<?php echo base_url(); ?>assets/frontend/imgs/theme/icons/icon-contact.svg" alt="" /><strong>Contact Seller:</strong><span>(+91) - 540-025-553</span></li>
                                    </ul>
                                    <div class="d-flex mb-55">
                                        <div class="mr-30">
                                            <p class="text-brand font-xs">Rating</p>
                                            <h4 class="mb-0">92%</h4>
                                        </div>
                                        <div class="mr-30">
                                            <p class="text-brand font-xs">Ship on time</p>
                                            <h4 class="mb-0">100%</h4>
                                        </div>
                                        <div>
                                            <p class="text-brand font-xs">Chat response</p>
                                            <h4 class="mb-0">89%</h4>
                                        </div>
                                    </div>
                                    <p>Noodles & Company is an American fast-casual restaurant that offers international and American noodle dishes and pasta in addition to soups and salads. Noodles & Company was founded in 1995 by Aaron Kennedy and is headquartered in Broomfield, Colorado. The company went public in 2013 and recorded a $457 million revenue in 2017.In late 2018, there were 460 Noodles & Company locations across 29 states and Washington, D.C.</p>
                                </div>
                                <div class="tab-pane fade" id="Reviews">
                                    <!--Comments-->
                                    <div class="comments-area">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <h4 class="mb-30">Customer Reviews</h4>
                                                <div class="comment-list">
                                                    <?php
                                                    if ($all_reviews) {
                                                        foreach ($all_reviews as $item) {
                                                            ?>
                                                    <div class="single-comment justify-content-between d-flex mb-30">
                                                        <div class="user justify-content-between d-flex">
                                                            <div class="thumb text-center">
                                                                <?php if($item->Image != ''){
                                                                    $user_image = base_url($item->Image);
                                                                }else{
                                                                    $user_image = base_url(). 'assets/frontend/imgs/blog/author-2.png';
                                                                } ?>
                                                                <img src="<?php echo $user_image; ?>" alt="" />
                                                                <a href="#" class="font-heading text-brand"></a><?php echo $item->FullName; ?></a>
                                                            </div>
                                                            <div class="desc">
                                                                <div class="d-flex justify-content-between mb-10" style="width: 500px;">
                                                                    <div class="d-flex align-items-center">
                                                                        <span class="font-xs text-muted"><?php echo date('d.m.Y h:i A', strtotime($item->CreatedAt)) ?></span>
                                                                    </div>
                                                                    <div class="product-rate d-inline-block">
                                                                        <div class="product-rating" style="width: <?php echo $product->Rating* 100/5;?>%"></div>
                                                                    </div>
                                                                </div>
                                                                <p class="mb-10"><?php echo $item->Comment; ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }
                                                 }else { ?>
                                                    
                                                    <div class="single-comment justify-content-between d-flex">
                                                        <div class="user justify-content-between d-flex">
                                                            
                                                            <div class="desc">
                                                                <div class="d-flex justify-content-between mb-10">
                                                                    <div class="d-flex align-items-center">
                                                                        <span class="font-xs text-muted"> </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <p class="mb-10">No reviews yet.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                </div>
                                            </div>

                                             <?php if ($UserCanReviewRate && $review) { ?>
                                            <div class="col-lg-8">
                                                <h4 class="mb-30">Your Review</h4>
                                                <div class="comment-list">
                                                    
                                                    <div class="single-comment justify-content-between d-flex mb-30">
                                                        <div class="user justify-content-between d-flex">
                                                            <div class="thumb text-center">
                                                                <?php if(file_exists($product->ImageName) &&  $review->Image != ''){

                                                                            $product_image = base_url($review->Image);

                                                                    }else{
                                                                        $product_image =  base_url(). 'assets/frontend/imgs/blog/author-2.png';

                                                                    } ?>
                                                                <img src="<?php echo $product_image; ?>" alt="" />
                                                            </div>
                                                            <div class="desc">
                                                                <div class="d-flex justify-content-between mb-10" style="width: 500px;">
                                                                    <div class="d-flex align-items-center">
                                                                        <span class="font-xs text-muted"><?php echo date('d.m.Y h:i A', strtotime($review->CreatedAt)) ?></span>
                                                                    </div>
                                                                    <div class="product-rate d-inline-block">
                                                                        <div class="product-rating" style="width: <?php echo $product->Rating* 100/5;?>%"></div>
                                                                    </div>
                                                                </div>
                                                                <p class="mb-10"><?php echo $review->Comment; ?> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                            <?php } ?>


                                            <div class="col-lg-4">
                                                <h4 class="mb-30">Customer reviews</h4>
                                                <div class="d-flex mb-30">
                                                    <div class="product-rate d-inline-block mr-15">
                                                        <div class="product-rating" style="width: <?php echo $product->Rating * 100/5;?>%"></div>
                                                    </div>
                                                    <h6><?php echo floatval($product->Rating);?> out of 5</h6>
                                                </div>
                                                <div class="progress">
                                                    <span>5 star</span>
                                                    <div class="progress-bar" role="progressbar" style="width: <?php echo $ratings['rating_5']; ?>%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"><?php echo $ratings['rating_5']; ?>%</div>
                                                </div>
                                                <div class="progress">
                                                    <span>4 star</span>
                                                    <div class="progress-bar" role="progressbar" style="width: <?php echo $ratings['rating_4']; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?php echo $ratings['rating_4']; ?>%</div>
                                                </div>
                                                <div class="progress">
                                                    <span>3 star</span>
                                                    <div class="progress-bar" role="progressbar" style="width: <?php echo $ratings['rating_3']; ?>%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"><?php echo $ratings['rating_3']; ?>%</div>
                                                </div>
                                                <div class="progress">
                                                    <span>2 star</span>
                                                    <div class="progress-bar" role="progressbar" style="width: <?php echo $ratings['rating_2']; ?>%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"><?php echo $ratings['rating_2']; ?>%</div>
                                                </div>
                                                <div class="progress mb-30">
                                                    <span>1 star</span>
                                                    <div class="progress-bar" role="progressbar" style="width: <?php echo $ratings['rating_1']; ?>%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"><?php echo $ratings['rating_1']; ?>%</div>
                                                </div>
                                                <!--<a href="#" class="font-xs text-muted">How are ratings calculated?</a>-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--comment form-->
                                     <?php if ($UserCanReviewRate && !$review) { ?>
                                    <div class="comment-form">
                                        <h4 class="mb-15">Add a review</h4>
                                        <div class="d-inline-block mb-30 giveRating"></div>
                                        <div class="row">
                                            <div class="col-lg-8 col-md-12">
                                                <form class="form-contact comment_form" action="<?php echo base_url('product/saveReview'); ?>" method="post" class="ajaxForm" id="productReviewForm">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <input type="hidden" name="ProductID" value="<?php echo $product->ProductID; ?>">
                                                                <textarea class="form-control w-100" name="comment" id="comment" cols="30" rows="9" placeholder="Write Review" required></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="button button-contactForm" value="Submit">Submit Review</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(!empty($products)){ ?>
                    <div class="row mt-60">
                        <div class="col-12">
                            <h2 class="section-title style-1 mb-30">Related products</h2>
                        </div>
                        <div class="col-12">
                            <div class="row related-products">
                                <?php $this->load->view('frontend/product/product_html'); ?>
                                
                                
                                
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</main>

    <script>
        $(function () {
            $("#showRating").rateYo({
                normalFill: "#fff4d1",
                ratedFill: "#f9c834",
                starWidth: "24px",
                rating: <?php echo productAverageRating($product->ProductID); ?>,
                readOnly: true,
            });
        });

        $( document ).ready(function(){

            $('.check_availability').on('change',function(){
                    var city_id = $(this).find(':selected').val();
                    $('.check_availability_city').hide();

                    $('.city-'+city_id).show();
            });

        });

        
    </script>

<?php 
if ($UserCanReviewRate) { ?>
    <script>
        $(function () {
            // $(".alreadyRated").rateYo({
                // rating: <?php // echo($rating ? $rating->Rating : 0); ?>,
                // readOnly: true
            // });
            $(".alreadyRated").rateYo({
                rating: <?php echo($rating ? $rating->Rating : 0); ?>,
                // readOnly: true
                fullStar: true,
                normalFill: "#fff4d1",
                ratedFill: "#f9c834",
                starWidth: "24px",
                onSet: function (rating, rateYoInstance) {
                    // alert("Rating is set to: " + rating);
                    showCustomLoader();
                    $.ajax({
                        type: "POST",
                        url: base_url + 'product/giveRating',
                        data: {
                            'ProductID': <?php echo $product->ProductID; ?>,
                            'UserID': <?php echo $this->session->userdata['user']->UserID; ?>,
                            'Rating': rating
                        },
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            hideCustomLoader();
                            showMessage(result.message);
                        }
                    });
                }
            });
        });
        $(function () {
            $(".giveRating").rateYo({
                normalFill: "#7E7E7E",
                ratedFill: "#f9c834",
                starWidth: "12px",
                fullStar: true,
                onSet: function (rating, rateYoInstance) {
                    // alert("Rating is set to: " + rating);
                    showCustomLoader();
                    $.ajax({
                        type: "POST",
                        url: base_url + 'product/giveRating',
                        data: {
                            'ProductID': <?php echo $product->ProductID; ?>,
                            'UserID': <?php echo $this->session->userdata['user']->UserID; ?>,
                            'Rating': rating
                        },
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            hideCustomLoader();
                            showMessage(result.message);
                        }
                    });
                }
            });
        });
    </script>
<?php } ?>