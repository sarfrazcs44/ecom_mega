<div class="sidebar-widget product-sidebar mb-30 p-30 bg-grey border-radius-10">
    <h5 class="section-title style-1 mb-30">New products</h5>
    
    
    <div class="single-post clearfix">
        <?php foreach($newProducts as $product){ ?>
        <div class="image">
            <?php if(file_exists($product->ImageName) &&  $product->ImageName != ''){

                $product_image = base_url($product->ImageName);

            }else{
                $product_image =    base_url().'assets/frontend/imgs/shop/thumbnail-1.jpg';

            } ?>
            <img src="<?php echo $product_image;?>" alt="#" />
        </div>
        <div class="content pt-10">
            <h6><a href="<?php echo base_url('product/detail');?>/<?php echo strtolower(str_replace(' ','-',$product->Title)); ?><?php echo '-'.$product->ProductID;?>"><?php echo $product->Title; ?></a></h6>
            <p class="price mb-0 mt-5"><?php echo $product->Price; ?></p>
            <div class="product-rate">
                <div class="product-rating" style="width: <?php echo $product->Rating* 100/5;?>%"></div>
            </div>
        </div>
    <?php } ?>
    </div>
</div>