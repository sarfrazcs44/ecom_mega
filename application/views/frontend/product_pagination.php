<?php 
$p = (ceil($total_count / 50)); 
if ($p != 0 ){
?>
<div>
    <div class="pagination-area mt-20 mb-20">
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-start">
                <li class="page-item">
                    <a class="page-link pagination_link" href="javascript:void(0);" data-page-no="<?php echo ($page > 1 ? $page - 1 : 1);?>"><i class="fi-rs-arrow-small-left"></i></a>
                </li>
                <?php 
                
                for ($i=1; $i <= $p ; $i++) { ?>
                
                <li class="page-item <?php echo ($page == $i ? 'active' : ''); ?>"><a class="page-link active_page_no pagination_link" data-page-no="<?php echo $i;?>" href="javascript:void(0);"><?php echo $i;?></a></li>
                <?php } ?>
                <li class="page-item">
                    <a class="page-link pagination_link" href="javascript:void(0);" data-page-no="<?php echo ($page == $p ? 1 : $page + 1);?>"><i class="fi-rs-arrow-small-right"></i></a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<?php } ?>