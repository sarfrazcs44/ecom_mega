<style>
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
</style>
<section class="content products titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo lang('product_catalogue'); ?></h2>
                <ul>
                    <li><a href="<?php echo base_url();?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('product');?>"><?php echo lang('products'); ?></a></li>
                    
                </ul>
            </div>
            <div class="col-md-12 filter">
                <h6>
                    <i class="fa fa-filter" aria-hidden="true"></i>
                    Filter
                    <span>
                        <b>Sort</b>
                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </span>
                </h6>
            </div>
        </div>
    </div>
</section>
<section class="content products">
    <div class="container">
        <div class="row">
            <?php if(!isset($search)){?>
            <div class="col-md-3" id="sidebar">
                <h5><?php echo lang('collections');?></h5>
                <?php if($collections){
                    foreach ($collections as $key => $value) { 

                        $collection_products  = getCollectionProductsSubCategories(json_encode($value),$language);

                        ?>
                        <div class="list-group">
                            <a href="#menu<?php echo $key; ?>" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo $value->Title; ?><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse<?php echo ($key == 0 ? ' in' : ''); ?>" id="menu<?php echo $key; ?>">
                                <?php if($collection_products){
                                    foreach ($collection_products as $key => $category) { ?>
                                       <label class="customcheck"><?php echo $category->Title; ?>
                                            <input type="checkbox" data-collection-id ="<?php echo $value->CollectionID; ?>" name="SubCategoryID[]" value="<?php echo $category->CategoryID;?>">
                                            <span class="checkmark"></span>
                                        </label>
                                   
                                <?php        
                                    }
                                }
                                ?>
                                
                               
                            </div>
                        </div>


                        <?php
                    }
                }
                ?>

                <h5><?php echo lang('categorys');?></h5>
                <?php if($categories){
                    foreach ($categories as $key => $category) { 

                        $sub_categories = subCategories($category->CategoryID,$language);
                        if($sub_categories){
                        ?>
                        <div class="list-group">
                            <a href="#menu-cate<?php echo $key; ?>" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo $category->Title; ?><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate<?php echo $key; ?>">
                                <?php if($sub_categories){
                                    foreach ($sub_categories as $key => $subcategory) { ?>
                                       <label class="customcheck"><?php echo $subcategory->Title; ?>
                                            <input type="checkbox get_products" data-collection-id = "0" name="SubCategoryID[]" value="<?php echo $subcategory->CategoryID;?>">
                                            <span class="checkmark"></span>
                                        </label>
                                   
                                <?php        
                                    }
                                }
                                ?>
                                
                               
                            </div>
                        </div>


                        <?php
                    }
                    }
                }
                ?>

                
            </div>
        <?php } ?>
            <div  id="Product-Listing" class="col-md-<?php echo(!isset($search) ? '9' : '12'); ?>">
                <div class="row">
                    <?php
                    foreach ($products as $key => $product) { 
                        if($key != 0 && $key % 4 == 0){
                            echo '</div><div class="row">';
                        }

                        ?>
                        <div class="col-md-3">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="<?php echo base_url(get_images($product->ProductID, 'product', false)); ?>">
                                </div>
                                <a href="<?php echo base_url() . 'product/detail/' . productTitle($product->ProductID); ?>">
                                    <h4><?php echo $product->Title; ?></h4>
                                    <h5><strong><?php echo number_format($product->Price, 2); ?></strong> SAR</h5>
                                    <?php
                                        if ($product->OutOfStock == 1)
                                        { ?>
                                            <small style="font-weight: bold;color: red;">(Out Of Stock)</small>
                                        <?php }
                                    ?>
                                </a>
                                <a title="Click to add to your wishlist" href="javascript:void(0);"
                                   onclick="addToWishlist(<?php echo $product->ProductID; ?>, 'product');"><i
                                            class="fa fa-heart <?php echo isLiked($product->ProductID, 'product'); ?>" id="item<?php echo $product->ProductID; ?>" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" title="Click to add this to your cart"
                                   onclick="addWishlistToCart(<?php echo $product->ProductID; ?>, 'Product');">
                                    <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <button><?php echo lang('load_more'); ?></button>
            </div>
        </div>
    </div>
</section>
<script>
    
    $(document).ready(function () {
        $(".get_products").on('click',function(e){
            alert();
            $(this).prop("checked", true);

        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });



        $.ajax({
            type: "POST",
            url: base_url + 'product/getMoreProducts',
            data: {
                'id': ''
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                $("#Product-Listing").html(result.html);

            },
            complete: function () {
                $.unblockUI();
            }
        });
        

        });
    
    });
</script>
