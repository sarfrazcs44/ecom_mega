<?php
$URL_Ids = getURLIds();
$category_ids_in_url = $URL_Ids['category_ids'];
$collection_ids_in_url = $URL_Ids['collection_ids'];
?>
<link rel="stylesheet" href="<?php echo front_assets(); ?>jPages/css/jPages.css">
<link rel="stylesheet" href="<?php echo front_assets(); ?>jPages/css/animate.css">
<script src="<?php echo front_assets(); ?>jPages/js/highlight.pack.js"></script>
<script src="<?php echo front_assets(); ?>jPages/js/tabifier.js"></script>
<script src="<?php echo front_assets(); ?>jPages/js/js.js"></script>
<script src="<?php echo front_assets(); ?>jPages/js/jPages.js"></script>
<style>
    .holder {
        margin: 15px 0;
    }

    .holder a {
        font-size: 12px;
        cursor: pointer;
        margin: 0 5px;
        color: #333;
    }

    .holder a:hover {
        background-color: #222;
        color: #fff;
    }

    .holder a.jp-previous {
        margin-right: 15px;
    }

    .holder a.jp-next {
        margin-left: 15px;
    }

    .holder a.jp-current, a.jp-current:hover {
        color: #FF4242;
        font-weight: bold;
    }

    .holder a.jp-disabled, a.jp-disabled:hover {
        color: #bbb;
    }

    .holder a.jp-current, a.jp-current:hover,
    .holder a.jp-disabled, a.jp-disabled:hover {
        cursor: default;
        background: none;
    }

    .holder span {
        margin: 0 5px;
    }


    .content.products .inbox .add_wishlist_to_cart {
        position: absolute;
        top: 7px;
        <?php echo($lang == 'AR' ? 'left' : 'right'); ?>: 55px;
        bottom: auto;
        <?php echo($lang == 'AR' ? 'right' : 'left'); ?>: auto;
        color: #fb7176;
        font-size: 20px;
    }
    .btnrow {
        margin-top: 25px;
    }
</style>

<section class="content products titlarea pt-0">
    <div class="edProdBanner" style="background-image:url('<?php echo base_url($category_data['Image']);?>')">
        <div class="container">
            <h2><?php echo categoryName($sub_category_data['CategoryID'],$language); ?></h2>
            <ul>
                <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                <li><a href="<?php echo base_url('product/category/'); ?><?php echo str_replace(' ','-',strtolower(categoryName($sub_category_data['ParentID'],'EN'))); ?>-c<?php echo $sub_category_data['ParentID']; ?> "><?php echo categoryName($sub_category_data['ParentID'],$language); ?></a></li>
                
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="filter">
                    <div class="row">
                        <div class="col-md-6 col-sm-5">
                            <h6>
                                <i class="fa fa-filter" aria-hidden="true"></i>
                                <?php echo lang('filter'); ?>
                                <span>
                                    <b><?php echo lang('count'); ?></b>
                                    <i aria-hidden="true" id="now"><?php echo count($products); ?> </i>/
                                    <i aria-hidden="true" id="total"> <?php echo $countproducts; ?></i>
                                </span>
                            </h6>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <?php
                            $price_highest_to_lowest = "";
                            $price_lowest_to_higher = "";
                            $product_older_to_newer = "";
                            $product_newer_to_older = "";
                            $purchased_highest_to_lowest = "";
                            $purchased_lowest_to_higher = "";
                            if (isset($_GET['sort']) && $_GET['sort'] != '') {
                                if ($_GET['sort'] == 'price_highest_to_lowest') {
                                    $price_highest_to_lowest = 'selected';
                                }

                                if ($_GET['sort'] == 'price_lowest_to_higher') {
                                    $price_lowest_to_higher = 'selected';
                                }

                                if ($_GET['sort'] == 'product_older_to_newer') {
                                    $product_older_to_newer = 'selected';
                                }

                                if ($_GET['sort'] == 'product_newer_to_older') {
                                    $product_newer_to_older = 'selected';
                                }

                                if ($_GET['sort'] == 'purchased_highest_to_lowest') {
                                    $purchased_highest_to_lowest = 'selected';
                                }

                                if ($_GET['sort'] == 'purchased_lowest_to_higher') {
                                    $purchased_lowest_to_higher = 'selected';
                                }
                            }
                            ?>
                            <select class="form-control" onchange="sortProducts(this.value);">
                                <option selected disabled><?php echo lang('Select_filter_to_sort_by'); ?></option>
                                <option value="price_highest_to_lowest" <?php echo $price_highest_to_lowest ?>><?php echo lang('price'); ?>:
                                    <?php echo lang('Highest_to_lowest'); ?>
                                </option>
                                <option value="price_lowest_to_higher" <?php echo $price_lowest_to_higher ?>><?php echo lang('price'); ?>:
                                    <?php echo lang('Lowest_to_Highest'); ?>
                                    
                                </option>
                                <!-- <option value="product_older_to_newer" <?php echo $product_older_to_newer ?>><?php echo lang('products'); ?>:
                                   <?php echo lang('Older_to_Newer'); ?>
                                </option>
                                <option value="product_newer_to_older" <?php echo $product_newer_to_older ?>><?php echo lang('products'); ?>:
                                    <?php echo lang('Newer_to_Older'); ?>
                                </option> -->
                                <option value="purchased_highest_to_lowest" <?php echo $purchased_highest_to_lowest ?>>
                                    <?php echo lang('Purchased'); ?>:
                                   <?php echo lang('Highest_to_lowest'); ?>
                                </option>
                                <option value="purchased_lowest_to_higher" <?php echo $purchased_lowest_to_higher ?>>
                                    <?php echo lang('Purchased'); ?>:
                                    <?php echo lang('Lowest_to_Highest'); ?>
                                </option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <select class="form-control ProductsPerPage">
                                <option selected disabled><?php echo lang('product_per_page'); ?></option>
                                <option>9</option>
                                <option>15</option>
                                <option>20</option>
                            </select>
                        </div>
                        <div class="col-md-1 iconsrow text-right col-sm-2">
                            <a onclick="changeGridLayout('items_list');"><i class="fa fa-list"></i></a>
                            <a onclick="changeGridLayout('items_grid');"><i class="fa fa-th"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content products">
    <div class="container">
        <div class="row">
            <?php if (!isset($search)) { ?>
                <div class="col-md-3" id="sidebar">
               
                    <?php /*if ($collections) {  ?>
                        <h5><?php echo lang('seasons'); ?></h5>
                        <?php
                        foreach ($collections as $key => $value) {
                            $collection_products = getCollectionProductsSubCategories(json_encode($value), $language);
                            ?>
                            <div class="list-group">
                                <a href="#menu<?php echo $key; ?>" class="list-group-item" data-toggle="collapse"
                                   data-parent="#sidebar">
                                    <span><?php echo $value->Title; ?><i class="fa fa-angle-down"
                                                                         aria-hidden="true"></i></span>
                                </a>
                                <div class="collapse<?php echo(($key == 0 && !isset($CollectionID)) ? ' in' : ''); ?><?php echo((isset($CollectionID) && $CollectionID == $value->CollectionID) ? ' in' : ''); ?>"
                                     id="menu<?php echo $key; ?>">
                                    <?php if ($collection_products) {
                                        $collection_category = array();
                                        foreach ($collection_products as $key => $category) {
                                            if (!in_array($category->CategoryID, $collection_category)) {
                                                ?>
                                                <label class="customcheck"><?php echo $category->Title; ?>
                                                    <input class="get_products <?php echo((isset($CollectionID) && $value->CollectionID == $CollectionID) ? 'checked_this' : ''); ?>"
                                                           type="checkbox"
                                                           data-collection-id="<?php echo $value->CollectionID; ?>"
                                                           data-url-title="<?php echo strtolower(str_replace(' ', '-', $value->Title)); ?>-c<?php echo $value->CollectionID; ?>"
                                                           name="SubCategoryID[]"
                                                           value="<?php echo $category->CategoryID; ?>">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <?php
                                                $collection_category[] = $category->CategoryID;
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                    }*/
                    ?>
                    <div class="list-group onlyChkBox">
                        <label class="customcheck">
                            <input class="get_products featured" type="checkbox" data-url-title="" name="chkFeature" value="">
                            <span class="checkmark"></span><?php echo lang('featured'); ?>
                        </label>
                    </div>
  <?php if ($offers || $offers_for_you) { ?>
                    <div class="list-group">
                                    <a href="#menu-cate6" class="list-group-item"
                                       data-toggle="collapse" data-parent="#sidebar">
                                        <span><?php echo lang('offers'); ?><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    </a>

                                    <div class="collapse" id="menu-cate6">
                                     <?php if ($offers) { 

                                            foreach ($offers as $key => $value) {
                                        ?>   
                                    <label class="customcheck" style="color:#a94442;">
                                        <input class="offers get_products" type="checkbox" 
                                                name="Offer[]"
                                                value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                        <span class="checkmark"></span>
                                    </label>

                                    <?php  
                                     }
                                        }


                                        ?>

                                         <?php if ($offers_for_you) {
                                                 foreach ($offers_for_you as $key => $value) {
                                          ?>
                                    <label class="customcheck" style="color:#a94442;">
                                        <input class="offers get_products" type="checkbox" 
                                                name="Offer[]"
                                                value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                        <span class="checkmark"></span>
                                    </label>

                                <?php } } ?>
                                    
                                    </div>
                    </div>
                <?php } ?>
                      
                    <div class="list-group">
                    
 
                                    <a href="#menu-cate2" class="list-group-item"
                                       data-toggle="collapse" data-parent="#sidebar">
                                        <span><?php echo lang('price'); ?><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="menu-cate2">
                                    <div class="priceRangeEd">
                                        <div class="form-group">
                                            <input type="text" id="amount" readonly class="form-control">
                                            <div id="slider-range"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!--<label class="customradio">
                                        <input class="price radio get_products" type="radio" 
                                                name="Price"
                                                value="1-50">1 - 50
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customradio">
                                        <input class="price radio get_products" type="radio" 
                                                name="Price"
                                                value="51-100">51 - 100
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customradio">
                                        <input class="price radio get_products" type="radio" name="Price"
                                                value="101-150">101 - 150
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customradio">
                                        <input class="price radio get_products" type="radio" name="Price"
                                                value="151-200">151 - 200
                                        <span class="checkmark"></span>
                                    </label>-->
                                    </div>
                    </div>
                    <div class="list-group">
                                    <a href="#menu-cate4" class="list-group-item"
                                       data-toggle="collapse" data-parent="#sidebar">
                                        <span><?php echo lang('rating'); ?><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="menu-cate4">
                                        
                                    <label class="customcheck">
                                                    <input class="rating get_products" type="checkbox" 
                                                           data-url-title=""
                                                           name="Rating[]"
                                                           value="1"><i class="filled_star"></i>
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="customcheck">
                                                    <input class="rating get_products" type="checkbox" 
                                                           data-url-title=""
                                                           name="Rating[]"
                                                           value="2"><i class="filled_star"></i><i class="filled_star"></i>
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="customcheck">
                                                    <input class="rating get_products" type="checkbox" 
                                                           data-url-title=""
                                                           name="Rating[]"
                                                           value="3"><i class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i>
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="customcheck">
                                                    <input class="rating get_products" type="checkbox"
                                                           data-url-title=""
                                                           name="Rating[]"
                                                           value="4"><i class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i>
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="customcheck">
                                                    <input class="rating get_products" type="checkbox" 
                                                           data-url-title=""
                                                           name="Rating[]"
                                                           value="5"><i class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i>
                                                    <span class="checkmark"></span>
                                                </label>
                                    </div>
                    </div>
                    <div class="list-group">
                                    <a href="#menu-cate5" class="list-group-item"
                                       data-toggle="collapse" data-parent="#sidebar">
                                        <span><?php echo lang('tags'); ?><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="menu-cate5">
                                        <div class="priceRangeEd">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="search_tags" placeholder="<?php echo lang('search_tags'); ?>" style="margin: 0;">
                                            </div>
                                        </div>
                                      <?php if($tags){
                                                foreach ($tags as $key => $value) { ?>
                                                    
                                                    <label class="customcheck search-labels" id="search-label-<?php echo $value->TagID;?>" data-text="<?php echo $value->Title; ?>">
                                                        <input class="get_products tags" type="checkbox" 
                                                               data-url-title=""
                                                               name="TagID[]"
                                                               value="<?php echo $value->TagID; ?>"><?php echo $value->Title; ?>
                                                        <span class="checkmark"></span>
                                                    </label>
                                              
                                        <?php
                                                }
                                      }

                                      ?>  
                                    
                                               
                                    </div>
                    </div>

                    <?php /*if ($categories) { ?>
                        <h5><?php echo lang('categorys'); ?></h5>
                        <?php
                        foreach ($categories as $key => $category) {
                            $sub_categories = subCategories($category->CategoryID, $language);
                            if ($sub_categories) {
                                ?>
                                <div class="list-group">
                                    <a href="#menu-cate<?php echo $key; ?>" class="list-group-item"
                                       data-toggle="collapse" data-parent="#sidebar">
                                        <span><?php echo $category->Title; ?><i class="fa fa-angle-down"
                                                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="menu-cate<?php echo $key; ?>">
                                        <?php if ($sub_categories) {
                                            foreach ($sub_categories as $key => $subcategory) { ?>
                                                <label class="customcheck"><?php echo $subcategory->Title; ?>
                                                    <input class="get_products" type="checkbox" data-collection-id="0"
                                                           data-url-title="<?php echo strtolower(str_replace(' ', '-', $subcategory->Title)); ?>-s<?php echo $subcategory->CategoryID; ?>"
                                                           name="SubCategoryID[]"
                                                           value="<?php echo $subcategory->CategoryID; ?>">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    } */
                    ?>
                </div>
            <?php } ?>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12" id="Product-Listing">
                        <?php echo product_html($products); ?>
                    </div>
                    <div class="col-md-12 holder" <?php echo (empty($products) ? 'style="display:none;"' : 'style="display:block;"'); ?>></div>
                    <div class="col-md-12 btnrow text-center" style="display: none;">
                        <input type="hidden" value="0" id="Page">
                        <button id="loadmore"
                                class="get_products loadmore btn btn-primary" <?php echo((count($products) <= $countproducts) ? 'style="display:none;"' : ''); ?>><?php echo lang('load_more'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<button class="get_products" id="hidden_btn" style="display: none;">&nbsp;</button>
<input type="hidden" value="0" id="hidden_price" >
<script>
$( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 1,
      max: 1000,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.values[ 0 ] + "-" + ui.values[ 1 ] );

      },
      stop: function( event, ui ) {
        $('#hidden_price').val(1);
        $('#hidden_btn').click();


      }
    });
    $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) +
      "-" + $( "#slider-range" ).slider( "values", 1 ) );


  } );
  </script>
<script>
    $(document).ready(function () {
        <?php if(isset($CollectionID)){ ?>
        setTimeout(function () {
            $('.checked_this').click();
        }, 500);
        <?php
        }
        ?>



    $(document).on("input", "#search_tags", function(){
            var v = $(this).val();
                v = v.toLowerCase();
            $('.search-labels').each(function() {
                var str = $(this).attr('data-text');
                var res = str.toLowerCase();

                if (res.indexOf(v) >= 0){
                   $(this).show();

                }else{
                    $(this).hide();
                }
               
            });
            
    });

        
        $(".get_products").on('click', function (e) {
            // clearing url titles
           // removeTitleToUrl();
           
            var loadmore = true;
            $(".overlaybg").show();
            $("#loadmore").show();
            if (!$(this).hasClass("loadmore")) {
                loadmore = false;
                if ($(this).hasClass("checked")) {
                    $(this).removeClass("checked");
                } else {
                    $(this).addClass("checked");
                }

                if($(this).hasClass('radio')){
                    $('.radio').removeClass("checked");
                    $(this).addClass("checked");
                }
                $("#Page").val(0);
            }
           
            var TagID = [];
            var OfferID = [];
            var SubCategoryID = <?php echo $sub_category_data['CategoryID']; ?>;
            var featured = false;
            if($('#hidden_price').val() == 1){
                var price = $('#amount').val();
            }else{
                var price = 0;
            }
            
            var Rating = [];
            var page;
            page = $("#Page").val();
            var h = 0;
            var i = 0;
            var j = 0;
            $(".get_products").each(function () {
                if ($(this).hasClass("checked")) {
                    
                    if($(this).hasClass("featured")){
                        featured = true;
                    }

                    if($(this).hasClass("tags")){
                       
                        TagID[h] = $(this).val();
                        h = h + 1;
                    }

                    /*if($(this).hasClass("price")){
                       
                        price = $(this).val();
                        
                    }*/

                    if($(this).hasClass("rating")){
                       
                        Rating[i] = $(this).val();
                        i = i + 1;
                    }

                    if($(this).hasClass("offers")){
                       
                        OfferID[j] = $(this).val();
                        j = j + 1;
                    }

                }
            });
            // add category titles to URL logic here
            var favorite = [];
            $.each($(".get_products:checked"), function () {
                if (typeof $(this).data('url-title') !== 'undefined' && $(this).data('url-title') != '') {
                    favorite.push($(this).data('url-title'));
                }
            });
            if (favorite.length > 0) {
                var titles = favorite.join("+");
                addTitleToUrl(titles);
            }

            var ul_cls = $('#Product-Listing > .row > .col-md-12 > ul').attr('class');
            var show_no_of_items = $('.ProductsPerPage').val();

            $.ajax({
                type: "POST",
                url: base_url + 'product/getMoreProducts',
                data: {
                    'Featured': featured,
                    'SubCategoryID': SubCategoryID,
                    'TagID': TagID,
                    'OfferID': OfferID,
                    'Rating': Rating,
                    'Price' : price,
                    'Page': $("#Page").val(),
                    'ul_cls': ul_cls
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {
                    if (loadmore) {
                        $("#Product-Listing").append(result.html);
                    } else {
                        $("#Product-Listing").html(result.html);
                    }
                    $("#total").html(result.count_product);
                    $("#now").html(result.total_now);
                    if (result.total_now == 0) {
                        $("#now").html(result.count_product);
                    }
                    if (result.page == page) {
                        $("#loadmore").hide();
                    }
                    $("#Page").val(result.page);
                },
                complete: function () {
                    $(".overlaybg").hide();
                    $('.offered_product').tooltip();
                    $('.holder').show();
                    $("div.holder").jPages("destroy").jPages({
                        containerID: "productsContainer",
                        perPage: show_no_of_items > 0 ? show_no_of_items : 6,
                        animation: "fadeInLeft",
                        keyBrowse: true
                    });
                    // $.unblockUI();
                }
            });
        });

        function addTitleToUrl(titles) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + "?q=" + titles;
            window.history.pushState({path: newurl}, '', newurl);
        }

        function removeTitleToUrl() {
            window.history.replaceState(null, null, "/product");
        }
    });

    $(".sorting").on('change', function (e) {
        $('.overlaybg').fadeIn();
        var sort_val = $(this).val();
        if (sort_val == 'price_highest_to_lowest') ;
        {
            tinysort('.single_product', {selector: 'strong', data: 'price', order: 'desc'});
        }
        if (sort_val == 'price_lowest_to_higher') {
            tinysort('.single_product', {selector: 'strong', data: 'price'});
        }
        if (sort_val == 'product_newer_to_older') {
            tinysort('.single_product', {selector: 'strong', data: 'pid', order: 'desc'});
        }
        if (sort_val == 'product_older_to_newer') {
            tinysort('.single_product', {selector: 'strong', data: 'pid'});
        }
        setTimeout(function () {
            $('.overlaybg').fadeOut();
        }, 500);
    });

    function sortProducts(sort_val) {
        var current_url = $(location).attr('href');
        url = new URL(window.location.href);
        if (url.searchParams.get('q')) {
            // append sort value at the end
            if (url.searchParams.get('sort')) {
                current_url = removeParam('sort', current_url);
                current_url += "&sort=" + sort_val;
            } else {
                current_url += "&sort=" + sort_val;
            }
        } else {
            // append sort value at the start
            if (url.searchParams.get('sort')) {
                current_url = removeParam('sort', current_url);
                current_url += "sort=" + sort_val;
            } else {
                current_url += "?sort=" + sort_val;
            }
        }
        window.location.href = current_url;
    }

    function removeParam(key, sourceURL) {
        var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    }

</script>
<script>
    /* when document is ready */
    $(function () {

        /* initiate plugin */
        $("div.holder").jPages({
            containerID: "productsContainer",
            perPage: 6,
            animation: "fadeInLeft",
            keyBrowse: true
        });

        /* on select change */
        $(".ProductsPerPage").change(function () {
            /* get new nº of items per page */
            var newPerPage = parseInt($(this).val());

            /* destroy jPages and initiate plugin again */
            $("div.holder").jPages("destroy").jPages({
                containerID: "productsContainer",
                perPage: newPerPage,
                animation: "fadeInLeft",
                keyBrowse: true
            });
        });

    });

    function changeGridLayout(cls) {

        $('.overlaybg').fadeIn();
        $('#Product-Listing > .row > .col-md-12 > ul').removeClass('items_grid');
        $('#Product-Listing > .row > .col-md-12 > ul').removeClass('items_list');
        setTimeout(function () {
            $('.overlaybg').fadeOut();
            $('#Product-Listing > .row > .col-md-12 > ul').addClass(cls);
        }, 500);
    }
</script>