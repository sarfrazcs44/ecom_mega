<style>
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
</style>
<section class="content products titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Product Catalogue</h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Wedding</a></li>
                    <li><a href="#">Design</a></li>
                </ul>
            </div>
            <div class="col-md-12 filter">
                <h6>
                    <i class="fa fa-filter" aria-hidden="true"></i>
                    Filter
                    <span>
                        <b>Sort</b>
                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </span>
                </h6>
            </div>
        </div>
    </div>
</section>
<section class="content products">
    <div class="container">
        <div class="row">
            <?php if(!isset($search)){?>
            <div class="col-md-3" id="sidebar">
                <div class="list-group">
                    <a href="#menu1" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span>Favorite<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse in" id="menu1">
                        <label class="customcheck">Walnut
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">Chocolate
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">Walnut
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">Chocolate
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="list-group">
                    <a href="#menu2" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span>Ocassion<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu2">
                        <label class="customcheck">One
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">One
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">One
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">One
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
        <?php } ?>
            <div class="col-md-<?php echo(!isset($search) ? '9' : '12'); ?>">
                <div class="row">
                    <?php
                    foreach ($products as $product) { ?>
                        <div class="col-md-3">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="<?php echo base_url(get_images($product->ProductID, 'product', false)); ?>">
                                </div>
                                <a href="<?php echo base_url() . 'product/detail/' . productTitle($product->ProductID); ?>">
                                    <h4><?php echo $product->Title; ?></h4>
                                    <h5><strong><?php echo number_format($product->Price, 2); ?></strong> SAR</h5>
                                    <?php
                                        if ($product->OutOfStock == 1)
                                        { ?>
                                            <small style="font-weight: bold;color: red;">(Out Of Stock)</small>
                                        <?php }
                                    ?>
                                </a>
                                <a title="Click to add to your wishlist" href="javascript:void(0);"
                                   onclick="addToWishlist(<?php echo $product->ProductID; ?>, 'product');"><i
                                            class="fa fa-heart <?php echo isLiked($product->ProductID, 'product'); ?>" id="item<?php echo $product->ProductID; ?>" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" title="Click to add this to your cart"
                                   onclick="addWishlistToCart(<?php echo $product->ProductID; ?>, 'Product');">
                                    <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                </a>
                            </div>
                        </div>
                    <?php }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
