<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Product Catalogue</h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Wedding</a></li>
                    <li><a href="#">Design</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="content single-products">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="imggallery">
                    <div id="slideshow">
                        <!-- Below are the images in the gallery -->
                        <?php
                        $i = 1;
                        foreach ($collection_images as $collection_image) { ?>
                            <div id="img-<?php echo $i; ?>" data-img-id="<?php echo $i; ?>"
                                 class="img-wrapper <?php echo($i == 1 ? 'active' : ''); ?>"
                                 style="background-image: url('<?php echo base_url($collection_image->ImageName); ?>')"></div>
                            <?php $i++;
                        }
                        ?>

                        <!-- Below are the thumbnails of above images -->
                        <div class="thumbs-container bottom">
                            <div id="prev-btn" class="prev">
                                <i class="fa fa-angle-left fa-3x"></i>
                            </div>

                            <ul class="thumbs">
                                <?php
                                $i = 1;
                                foreach ($collection_images as $collection_image) { ?>
                                    <li data-thumb-id="<?php echo $i; ?>"
                                        class="thumb <?php echo($i == 1 ? 'active' : ''); ?>"
                                        style="background-image: url('<?php echo base_url($collection_image->ImageName); ?>')"></li>
                                    <?php $i++;
                                }
                                ?>
                            </ul>

                            <div id="next-btn" class="next">
                                <i class="fa fa-angle-right fa-3x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="product-info">
                    <h2><?php echo $collection->Title; ?></h2>
                    <p><?php echo $collection->Description; ?></p>
                    <div class="ratings">
                        <div id="showRating"></div>
                    </div>
                    <h4>Specifications</h4>
                    <?php echo $collection->Specifications; ?>
                    <!--<h5 class="bordered">Price <span><?php /*echo number_format($collection->Price); */?> SAR</span></h5>-->
                    <?php
/*                    if ($collection->OutOfStock == 1)
                    { */?><!--
                        <small style="font-weight: bold;color: red;">(Out Of Stock)</small>
                    --><?php /*}
                    */?>
                    <!--<div class="row">
                        <div class="col-md-6">
                            <h6>Quantity</h6>
                        </div>
                        <div class="col-md-6">
                            <input id="after" name="Quantity" class="form-control Quantity" type="number" value="1"
                                   min="1" max="15"/>
                            <input type="hidden" class="ProductID" name="ProductID"
                                   value="<?php /*echo $collection->CollectionID; */?>">
                            <input type="hidden" class="ItemType" value="Collection">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary" onclick="addToCart();">Add to Cart</button>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</section>
<?php $collection_products = whats_inside_collection($collection->CollectionID); ?>
<section class="content three">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h3><strong><span>what's</span> Inside</strong></h3>
                </div>
                <div class="procarousel">
                    <div class="owl-carousel owl-theme brands-slider">
                        <?php
                        if ($collection_products) {
                            foreach ($collection_products as $collection_product) { ?>
                                <div class="item">
                                    <a href="<?php echo base_url() . 'product/detail/' . productTitle($collection_product->ProductID); ?>">
                                        <img src="<?php echo base_url(get_images($collection_product->ProductID, 'product', false)); ?>">
                                        <h4><?php echo $collection_product->Title; ?></h4>
                                    </a>
                                </div>
                            <?php }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
if ($this->session->userdata('user')) { ?>
    <section class="content rateproduct">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h3><strong><span>rate the</span> Product</strong></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <?php
                    if ($review) { ?>
                        <h6 class="heading">Your review</h6>
                        <div class="row">
                            <div class="col-md-7">
                                <h6><span><?php echo date('d.m.Y h:i A', strtotime($review->CreatedAt)) ?></span></h6>
                                <p class="cname"><strong><?php echo $review->Title; ?></strong></p>
                            </div>
                            <div class="col-md-5">
                                <div class="ratings">
                                    <?php
                                    if ($rating) { ?>
                                        <div id="alreadyRated"></div>
                                    <?php } else { ?>
                                        <div id="giveRating"></div>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p><?php echo $review->Comment; ?></p>
                            </div>
                        </div>
                    <?php } else { ?>
                        <h6 class="heading">Comments</h6>
                        <form action="<?php echo base_url('collection/saveReview'); ?>" method="post" class="ajaxForm"
                              id="collectionReviewForm">
                            <div class="row">
                                <div class="col-md-7">
                                    <input type="text" name="Title" placeholder="Title" class="form-control required">
                                    <input type="hidden" name="CollectionID" value="<?php echo $collection->CollectionID; ?>">
                                </div>
                                <div class="col-md-5">
                                    <div class="ratings">
                                        <?php
                                        if ($rating) { ?>
                                            <div id="alreadyRated"></div>
                                        <?php } else { ?>
                                            <div id="giveRating"></div>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <textarea placeholder="Review" name="Comment"
                                              class="form-control required"></textarea>
                                    <input type="submit" name="" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>
                    <?php }
                    ?>
                    <div class="row">
                        <?php
                        if ($all_reviews) {
                            foreach ($all_reviews as $item) {
                                ?>
                                <div class="col-md-12">
                                    <h6><?php echo $item->FullName; ?> <span><?php echo date('d.m.Y h:i A', strtotime($item->CreatedAt)) ?></span></h6>
                                    <p class="cname"><strong><?php echo $item->Title; ?></strong></p>
                                    <p><?php echo $item->Comment; ?></p>
                                </div>
                            <?php }
                        } else { ?>
                            <div class="col-md-12">
                                <?php
                                if ($review)
                                { ?>
                                    <h6>No other reviews yet.</h6>
                                <?php } else { ?>
                                    <h6>No reviews yet.</h6>
                                <?php }
                                ?>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
                <?php
                $ratings = collectionRatings($collection->CollectionID);
                ?>
                <div class="col-md-4">
                    <div class="ratingbox">
                        <h6 class="heading">Ratings
                            <small>(<?php echo $ratings['total_ratings_count']; ?>)</small>
                        </h6>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_1']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_1']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_1']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_2']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_2']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_2']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_3']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_3']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_3']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_4']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_4']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_4']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                        <div class="progress progress-bar-vertical last">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_5']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_5']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_5']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function () {
            $("#showRating").rateYo({
                rating: <?php echo collectionAverageRating($collection->CollectionID); ?>,
                readOnly: true
            });
        });

        $(function () {
            $("#alreadyRated").rateYo({
                rating: <?php echo ($rating ? $rating->Rating : 0); ?>,
                readOnly: true
            });
        });

        $(function () {
            $("#giveRating").rateYo({
                fullStar: true,
                onSet: function (rating, rateYoInstance) {
                    // alert("Rating is set to: " + rating);
                    showCustomLoader();
                    $.ajax({
                        type: "POST",
                        url: base_url + 'collection/giveRating',
                        data: {'CollectionID': <?php echo $collection->CollectionID; ?>, 'UserID': <?php echo $this->session->userdata['user']->UserID; ?>, 'Rating': rating},
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            hideCustomLoader();
                            showMessage(result.message);
                        }
                    });
                }
            });
        });
    </script>
<?php }
?>
