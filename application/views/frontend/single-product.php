<?php
                $ratings = productRatings($product->ProductID);

                ?>
<div class="modal fade" id="chk-avail" tabindex="1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">

  <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
  <div class="modal-dialog modal-dialog-centered modWl" role="document">


    <div class="modal-content mod-cont">
      <div class="modal-header mod_hed">
        <h5 class="modal-title hdTxt" id="exampleModalLongTitle"><?php echo lang('check');?> <span><?php echo lang('availability'); ?></span> </h5>
      </div>
      <div class="modal-body mod-bod">
        <div class="frmcity">
           <p><strong><?php echo lang('please_select_city'); ?></strong></p>
              <div class="form-group edSelectstyle  white" >
                   <select class="fmod form-control check_availability" style="max-width: 387px;">
                      <option><?php echo lang('check');?> <?php echo lang('availability'); ?></option>
                       <?php 
                       $branches_html = '';
                       $available = false;

                       if($available_cities){
                        $ontime_array = array();
                                  foreach ($available_cities as $key => $value) {
                                            if($value->Quantity > 0){
                                                $available = true;
                                            }

                                            if($value->Quantity == '' || $value->Quantity == 0){
                                                $quantity = '<span class="redTxt">Sold Out</span>';
                                            }elseif($value->Quantity >= 6 && $value->Quantity <= 10){
                                                $quantity = '<span class="gTxt">'.$value->Quantity.' Pcs Available</span>';
                                            }elseif($value->Quantity <= 5 && $value->Quantity > 0){
                                                $quantity = '<span class="orangeTxt">Last '.$value->Quantity.' Pcs</span>';
                                            }else{
                                                $quantity = '<span class="gTxt">Available</span>';
                                            }




                                                if(!in_array($value->CityID,$ontime_array)){?>
                                        
                                        <option value="<?php echo $value->CityID; ?>"><?php echo $value->CityTitle; ?></option> 

                                    <?php
                                }

                                $branches_html .= '<div class="branchTXT check_availability_city city-'.$value->CityID.'" style="display:none;">
                                                 <p class="hTxt"><strong>'.$value->StoreTitle.'</strong><br>
                                                 <span>'.$value->Address.'</span><br>
                                                '.$quantity.'</p>
                                             </div>';
                                $ontime_array[] = $value->CityID;
                                 }
                       }

                       ?>
                                                                                        
                      </select>
                      <p id="show_availability" style="display: none;"></p>
                  </div>
              </div>
        <div class="rsltTxt">
                         <h6><?php echo lang('results'); ?></h6>
                     </div>

                     <?php echo $branches_html; ?>
      </div>
      <div class="modal-footer mod_fot text-center">
        <div class="text-center cls-bt">
        <button type="button" class="btn-success close-icon" data-dismiss="modal"><?php echo lang('Close'); ?></button>
      </div>
    </div>
  </div>
</div>
</div>
<?php
if ($this->session->userdata('user')) {
    $IsProductPurchased = IsProductPurchased($this->session->userdata['user']->UserID, $product->ProductID);
    if ($IsProductPurchased) {
        $UserCanReviewRate = true;
    } else {
        $UserCanReviewRate = false;
    }
} else {
    $UserCanReviewRate = false;
}
if ($product->IsCorporateProduct == 1 && $product->CorporateMinQuantity > 0) {
    $MinQuantity = $product->CorporateMinQuantity;
    $Price = $product->CorporatePrice;
    $MinQClass = "bordered";
    $PriceClass = "";
    $PriceType = lang('price_type_kg');
    $IsCorporateItem = 1;
} else {
    $MinQuantity = 1;
    $MinQClass = "";
    $PriceClass = "bordered";
    $Price = $product->Price;
    if ($product->PriceType == 'kg') {
        $PriceType = lang('price_type_kg');
    } else {
        $PriceType = lang('price_type_item');
    }
    $IsCorporateItem = 0;
}
?>
<?php
// offer logic here
$IsOnOffer = false;
$DiscountDescription = '';
$offer_product = checkProductIsInAnyOffer($product->ProductID);

$ProductDiscountedPrice = $product->Price;
if(!empty($offer_product)){
    $Price = $product->Price;
    $DiscountDescription = $offer_product['Description'];
    $IsOnOffer = true;
    $DiscountType = $offer_product['DiscountType'];
    $DiscountFactor = $offer_product['Discount'];
    if ($DiscountType == 'percentage') {
        $Discount = ($DiscountFactor / 100) * $Price;
        if ($Discount > $Price) {
            $ProductDiscountedPrice = 0;
        } else {
            $ProductDiscountedPrice = $Price - $Discount;
        }
    } elseif ($DiscountType == 'per item') {
        $Discount = $DiscountFactor;
        if ($Discount > $Price) {
            $ProductDiscountedPrice = 0;
        } else {
            $ProductDiscountedPrice = $Price - $DiscountFactor;
        }
    } else {
        $Discount = 0;
        if ($Discount > $Price) {
            $ProductDiscountedPrice = 0;
        } else {
            $ProductDiscountedPrice = $Price;
        }
    }

}
/*$IsOnOffer = false;
$DiscountDescription = '';
if ($product->IsCorporateProduct == 0 && $offer_id != "") {
    $CurrentDate = date('Y-m-d');
    $offer_id = base64_decode($offer_id);
    $offer = array($offer_id);
    $offer_products = $this->Offer_model->getOfferProducts($offer);
    if ($offer_products) {
        foreach ($offer_products as $offer_product) {
            $OfferID = $offer_product->OfferID;
            $ProductIDsOffer = explode(',', $offer_product->ProductID);
            if (in_array($product->ProductID, $ProductIDsOffer)) {
                $OfferValidFrom = $offer_product->ValidFrom;
                $OfferValidTo = $offer_product->ValidTo;
                if ($CurrentDate >= $OfferValidFrom && $CurrentDate <= $OfferValidTo) {
                    $IsOnOffer = true;
                    $DiscountDescription = $offer_product->Title . ": " . $offer_product->Description;
                    $DiscountType = $offer_product->DiscountType;
                    $DiscountFactor = $offer_product->Discount;
                    if ($DiscountType == 'percentage') {
                        $Discount = ($DiscountFactor / 100) * $Price;
                        if ($Discount > $Price) {
                            $ProductDiscountedPrice = 0;
                        } else {
                            $ProductDiscountedPrice = $Price - $Discount;
                        }
                    } elseif ($DiscountType == 'per item') {
                        $Discount = $DiscountFactor;
                        if ($Discount > $Price) {
                            $ProductDiscountedPrice = 0;
                        } else {
                            $ProductDiscountedPrice = $Price - $DiscountFactor;
                        }
                    } else {
                        $Discount = 0;
                        if ($Discount > $Price) {
                            $ProductDiscountedPrice = 0;
                        } else {
                            $ProductDiscountedPrice = $Price;
                        }
                    }
                }
                break;
            }
        }
    }
}*/
?>
<style>
    #PriceT {
        padding: 0 !important;
    }
    div#slideshow {
        overflow: visible;
    }
    .mz-expand .mz-figure > img {
        right: 0;
        margin: 0 auto !important;
        display: inline-block;
        width: auto !important;
    }
</style>
    <section class="content titlarea">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- <h2><?php echo $product->Title; ?></h2> -->
                    <ul>
                        <?php
                        if ($product->IsCorporateProduct == 1) { ?>
                            <li><a href="<?php echo base_url('corporate'); ?>"><?php echo lang('corporate'); ?></a></li>
                            <li><a href="<?php echo base_url('corporate/products'); ?>"><?php echo lang('products'); ?></a></li>
                        <?php } else { ?>
                            <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                            <li><a href="<?php echo base_url('product'); ?>"><?php echo lang('products'); ?></a></li>
                            <li>
                                <?php $first_cate = explode(',',$product->CategoryID);

                                    

                                    $first_child_cate = explode(',',$product->SubCategoryID);

                                    

                                 ?>
                                <a href="<?php echo base_url('product/category/'.strtolower(str_replace(' ','-',categoryName($first_cate[0], $language)).'-c'.$first_cate[0])); ?>"><?php echo categoryName($first_cate[0], $language); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('product?q='.strtolower(str_replace(' ','-',categoryName($first_child_cate[0], $language)).'-s'.$first_child_cate[0])); ?>"><?php echo categoryName($first_child_cate[0], $language); ?></a>
                            </li>
                            <li><?php echo $product->Title; ?></li>
                        <?php }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="content single-products">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="imggallery">
                        <div class="app-figure" id="zoom-fig">
                            <?php
                                if(isset($product_images[0]->ImageName)) {
                                    ?>
                                        <a id="Zoom-1" class="MagicZoom" title="Ecommerce"
                                            href="<?php echo base_url($product_images[0]->ImageName); ?>?h=1400"
                                            data-zoom-image-2x="<?php echo base_url($product_images[0]->ImageName); ?>?h=2800"
                                            data-image-2x="<?php echo base_url($product_images[0]->ImageName); ?>?h=800" >
                                            <img src="<?php echo base_url($product_images[0]->ImageName); ?>?h=400" srcset="<?php echo base_url($product_images[0]->ImageName); ?>?h=800 2x"
                                                alt=""/>
                                        </a>
                                    <?php
                                }
                            ?>
                            <div class="selectors">
                                <?php
                                    $ed = 1;
                                    if($product_images){
                                    foreach ($product_images as $product_image) { ?>
                                    <a
                                        data-zoom-id="Zoom-1"
                                        href="<?php echo base_url($product_image->ImageName); ?>?h=1400"
                                        data-image="<?php echo base_url($product_image->ImageName); ?>?h=400"
                                        data-zoom-image-2x="<?php echo base_url($product_image->ImageName); ?>?h=2800"
                                        data-image-2x="<?php echo base_url($product_image->ImageName); ?>?h=800"
                                    >
                                        <img srcset="<?php echo base_url($product_image->ImageName); ?>?h=120 2x" src="<?php echo base_url($product_image->ImageName); ?>?h=60"/>
                                    </a>
                                        <?php $ed++;
                                    } }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-info">
                        <h2><?php echo $product->Title; ?></h2>
                        <p><?php echo $product->Description; ?></p>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="ratings">
                                    <div id="showRating"></div>
                                    <p><?php echo productAverageRating($product->ProductID); ?> <?php echo lang('avg_rated_by'); ?> <?php echo $ratings['total_ratings_count']; ?> <?php echo lang('People'); ?></p>
                                </div>
                            </div>
                            <div class="col-xs-6 nutritionBtn">
                                <a class="pointer" data-toggle="modal" data-target="#nutritionInfo"><?php echo lang('nutrition_info'); ?></a>
                            </div>
                        </div>
                        <h4><?php echo lang('specifications'); ?></h4>
                        <?php echo $product->Specifications; ?>
                        <?php if ($product->IsCorporateProduct == 1) { ?>
                            <h5 class="MinimumQty <?php echo $MinQClass; ?>"><?php echo lang('Minimum_Qty'); ?>
                                <span><?php echo $MinQuantity; ?> <?php echo lang('kg'); ?></span></h5>
                        <?php } ?>
                        <h5 class="<?php echo $PriceClass; ?>"><?php echo lang('price'); ?>
                            <?php
                            if ($IsOnOffer) { ?>
                                <span id="PriceP"
                                      style="text-decoration: line-through;"><?php echo number_format($Price, 2); ?> <?php echo lang('SAR'); ?></span>
                                <span style="text-decoration: line-through;"
                                      id="PriceT"><?php echo $PriceType; ?></span>
                                <span style="font-weight: bold;" class="offered_product"
                                      title="<?php echo $DiscountDescription; ?>"><?php echo number_format($ProductDiscountedPrice, 2); ?> <?php echo lang('SAR'); ?></span>
                                <span style="font-weight: bold;" class="offered_product"
                                      id="PriceT"><?php echo $PriceType; ?></span>
                                <?php
                                $ProductPriceForCart = $ProductDiscountedPrice;
                            } else { ?>
                                <span id="PriceP"><?php echo number_format($Price, 2); ?> <?php echo lang('SAR'); ?></span>
                                <span id="PriceT"><?php echo $PriceType; ?></span>
                                <?php
                                $ProductPriceForCart = $Price;
                            }
                            ?>
                        </h5>
                        <?php
                        if ($product->OutOfStock == 1) { ?>
                            <small style="font-weight: bold;color: red;">(<?php echo lang('Out_Of_Stock'); ?>)</small>
                        <?php }

                        if(!empty($available_cities)){
                        ?>

                        
                         
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row" style="margin-bottom:10px">
                                    <div class="col-xs-5">
                                        <h6><?php echo lang('quantity'); ?></h6>
                                    </div>
                                    <div class="col-xs-7">

                                        <?php if($product->PriceType != 'kg'){ ?>

                                            <input id="after" name="Quantity" class="form-control Quantity" type="number"
                                            value="<?php echo $MinQuantity; ?>" min="1"/>

                                        <?php }else{ ?>
                                            <div class="form-group edSelectStyle ">
                                                <select class="form-control Quantity">
                                                    <option value = "0.250"><?php echo ($language == 'AR' ? 'ربع ٢٥٠ غرام' : '250 Grams'); ?></option>
                                                    <option value = "0.500" ><?php echo ($language == 'AR' ? ' نصف ٥٠٠ غرام' : '500 Grams'); ?></option>
                                                  
                                                    <option value = "1"><?php echo ($language == 'AR' ? 'كيلو ١٠٠٠ غرام' : '1 KG'); ?></option>
                                                   
                                                  
                                                </select>
                                            </div>
                                        <?php } ?>
                                        
                                        


                                        <input type="hidden" class="ProductID" name="ProductID"
                                            value="<?php echo $product->ProductID; ?>">
                                        <input type="hidden" class="ProductPrice" name="ProductPrice"
                                            value="<?php echo $ProductPriceForCart; ?>">
                                        <input type="hidden" class="IsCorporateItem" value="<?php echo $IsCorporateItem; ?>">
                                        <input type="hidden" class="ItemType" value="Product">

                                        <input id="IsCorporateProductOriginal" type="hidden"
                                            value="<?php echo $product->IsCorporateProduct; ?>">
                                        <input id="CorporateMinQuantityOriginal" type="hidden"
                                            value="<?php echo $product->CorporateMinQuantity; ?>">
                                        <input id="ProductPriceOriginal" type="hidden"
                                            value="<?php echo number_format($product->Price, 2); ?>">
                                        <input id="CorporateProductPriceOriginal" type="hidden"
                                            value="<?php echo number_format($product->CorporatePrice, 2); ?>">
                                        <input id="PriceTypeOriginal" type="hidden"
                                            value="<?php echo($product->PriceType == 'item' ? lang('price_type_item') : lang('price_type_kg')); ?>">
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-lg-6 changeOneMobile <?php echo($lang == 'AR' ? 'text-left' : 'text-right'); ?>">
                            <?php if($available){ ?>
                            <button type="button" class="ck btn-secondary " data-toggle="modal" data-target="#chk-avail"><?php echo lang('check');?> <?php echo lang('availability'); ?></button>
                        <?php }else{ ?>
                            <button type="button" class="ck btn-secondary noStocl"><?php echo lang('Out_Of_Stock'); ?></button>
                        <?php } ?>
                            
                     <?php } ?>


                            </div>
                            <?php if($available){ ?>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary"
                                        onclick="addToCart();"><?php echo lang('add_to_basket'); ?></button>
                            </div>
                             <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php if($whats_inside){ ?>
    <section class="content rateproduct">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h3><strong><span><?php echo lang('what'); ?></span><?php echo lang('Inside'); ?></strong></h3>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme brands-slider">
                <?php foreach ($whats_inside as $key => $value) { ?>
                        
                        <div class="item">
                            <a href="javascript:void(0);">
                                <img src="<?php echo base_url($value->InsideImage); ?>">
                                <h4><?php echo ($language == 'EN' ? $value->InsideTitle : $value->InsideTitleAr); ?></h4>
                            </a>
                        </div>
                


                    <?php
                }

                ?>
                
            </div>
        </div>
    </section>

<?php } ?>
    <section class="content rateproduct">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h3><strong><span><?php echo lang('rate_the'); ?></span> <?php echo lang('product'); ?></strong></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <?php if ($UserCanReviewRate && $review) { ?>
                        <h6 class="heading"><?php echo lang('Your_review'); ?></h6>
                        <div class="row" style="margin-bottom:20px;">
                            <div class="col-md-7">
                                <h6 class="cusNameEd"><?php echo lang('Customer_Name'); ?></h6><!-- hard coded -->
                                <h6><span><?php echo date('d.m.Y h:i A', strtotime($review->CreatedAt)) ?></span>
                                </h6>
                                <p class="cname"><strong><?php echo $review->Title; ?></strong></p>
                            </div>
                            <div class="col-md-5">
                                <div class="ratings">
                                <span>Rate it</span>
                                    <?php
                                    if ($rating) { ?>
                                        <div class="alreadyRated"></div>
                                    <?php } else { ?>
                                        <div class="giveRating"></div>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p><?php echo $review->Comment; ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($UserCanReviewRate && !$review) { ?>
                        <form action="<?php echo base_url('product/saveReview'); ?>" method="post" class="ajaxForm"
                              id="productReviewForm">
                            <div class="row">
                                <div class="col-md-12"><h6 class="heading"><?php echo lang('Give_Your_Review'); ?></h6></div>
                                <div class="col-md-7">
                                    <input type="text" name="Title" placeholder="Title" class="form-control required">
                                    <input type="hidden" name="ProductID" value="<?php echo $product->ProductID; ?>">
                                </div>
                                <div class="col-md-5">
                                    <div class="ratings">
                                        <span><?php echo lang('Rate_it'); ?></span>
                                        <?php
                                        if ($rating) { ?>
                                            <div class="alreadyRated"></div>
                                        <?php } else { ?>
                                            <div class="giveRating"></div>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <textarea placeholder="Review" name="Comment"
                                              class="form-control required"></textarea>
                                    <input type="submit" name="" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h6 class="heading"><?php echo lang('Other_Reviews'); ?></h6>
                        </div>
                        <?php
                        if ($all_reviews) {
                            foreach ($all_reviews as $item) {
                                ?>
                                <div class="col-md-12">
                                    <h6><?php echo $item->FullName; ?>
                                        <span><?php echo date('d.m.Y h:i A', strtotime($item->CreatedAt)) ?></span></h6>
                                    <p class="cname"><strong><?php echo $item->Title; ?></strong></p>
                                    <p><?php echo $item->Comment; ?></p>
                                </div>
                            <?php }
                        } else { ?>
                            <div class="col-md-12">
                                <?php
                                if ($review) { ?>
                                    <h6><?php echo lang('No_other_reviews_yet'); ?></h6>
                                <?php } else { ?>
                                    <h6><?php echo lang('No_reviews_yet'); ?></h6>
                                <?php }
                                ?>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="ratingbox">
                        <h6 class="heading"><?php echo lang('ratings'); ?>
                            
                        </h6>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_1']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_1']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_1']; ?>% <?php echo lang('Complete'); ?></span>
                                <span class="totalRecords"><?php echo $ratings['rating_1']; ?>%</span><!-- HardCode Text -->
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                            <small>(<?php echo $ratings['rating_1_count']; ?>)</small>
                        </div>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_2']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_2']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_2']; ?>% <?php echo lang('Complete'); ?></span>
                                <span class="totalRecords"><?php echo $ratings['rating_2']; ?>%</span><!-- HardCode Text -->
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                            <small>(<?php echo $ratings['rating_2_count']; ?>)</small>
                        </div>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_3']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_3']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_3']; ?>% <?php echo lang('Complete'); ?></span>
                                <span class="totalRecords"><?php echo $ratings['rating_3']; ?>%</span><!-- HardCode Text -->
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                            <small>(<?php echo $ratings['rating_3_count']; ?>)</small>
                        </div>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_4']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_4']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_4']; ?>% <?php echo lang('Complete'); ?></span>
                                <span class="totalRecords"><?php echo $ratings['rating_4']; ?>%</span><!-- HardCode Text -->
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                            <small>(<?php echo $ratings['rating_4_count']; ?>)</small>
                        </div>
                        <div class="progress progress-bar-vertical last">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_5']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_5']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_5']; ?>% <?php echo lang('Complete'); ?></span>
                                <span class="totalRecords"><?php echo $ratings['rating_5']; ?>%</span><!-- HardCode Text -->
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                            <small>(<?php echo $ratings['rating_5_count']; ?>)</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php if ($product->IsCorporateProduct == 1) { ?>
    <script>
        $(document).on('keyup', '.Quantity', function () {
            setTimeout(function () {
                updateFields();
            }, 500);
        });
        $(document).on('click', '.input-group-btn', function () {
            var operator = $(this).find('button').text();
            if (operator == '+' || operator == '-') {
                setTimeout(function () {
                    updateFields();
                }, 100);
            }
        });
        function updateFields() {
            var IsCorporateItem = $('.IsCorporateItem').val();
            var IsCorporateProductOriginal = $('#IsCorporateProductOriginal').val();
            var CorporateMinQuantityOriginal = $('#CorporateMinQuantityOriginal').val();
            var ProductPriceOriginal = $('#ProductPriceOriginal').val();
            var CorporateProductPriceOriginal = $('#CorporateProductPriceOriginal').val();
            var PriceTypeOriginal = $('#PriceTypeOriginal').val();
            var Quantity = $('.Quantity').val();
            if (IsCorporateItem == 1) {
                if (Quantity < CorporateMinQuantityOriginal) {
                    showCustomLoader();
                    $('.ProductPrice').val(ProductPriceOriginal);
                    $('.IsCorporateItem').val(0);
                    $('.MinimumQty').hide();
                    $('#PriceT').html(PriceTypeOriginal);
                    $('#PriceP').html(PriceTypeOriginal);
                    $('#PriceP').html(ProductPriceOriginal);
                    $('#PriceP').parent('h5').addClass('bordered');
                    hideCustomLoader();
                    showMessage('<?php echo lang('This_item_will_not'); ?>', 'danger');
                }
            } else if (IsCorporateItem == 0) {
                if (Quantity >= CorporateMinQuantityOriginal) {
                    showCustomLoader();
                    $('.ProductPrice').val(CorporateProductPriceOriginal);
                    $('.IsCorporateItem').val(1);
                    $('.MinimumQty').show();
                    $('#PriceT').html('<?php echo lang('price_type_kg'); ?>');
                    $('#PriceP').html(CorporateProductPriceOriginal);
                    $('#PriceP').parent('h5').removeClass('bordered');
                    hideCustomLoader();
                    showMessage('<?php echo lang('This_item_will_be'); ?>');
                }
            }
        }
    </script>
<?php } ?>
    <script>
        $(function () {
            $("#showRating").rateYo({
                normalFill: "#fff4d1",
                ratedFill: "#f9c834",
                starWidth: "24px",
                rating: <?php echo productAverageRating($product->ProductID); ?>,
                readOnly: true,
            });
        });

        $( document ).ready(function(){

            $('.check_availability').on('change',function(){
                    var city_id = $(this).find(':selected').val();
                    $('.check_availability_city').hide();

                    $('.city-'+city_id).show();
            });

        });

        
    </script>
<?php
if ($UserCanReviewRate) { ?>
    <script>
        $(function () {
            // $(".alreadyRated").rateYo({
                // rating: <?php // echo($rating ? $rating->Rating : 0); ?>,
                // readOnly: true
            // });
            $(".alreadyRated").rateYo({
                rating: <?php echo($rating ? $rating->Rating : 0); ?>,
                // readOnly: true
                fullStar: true,
                normalFill: "#fff4d1",
                ratedFill: "#f9c834",
                starWidth: "24px",
                onSet: function (rating, rateYoInstance) {
                    // alert("Rating is set to: " + rating);
                    showCustomLoader();
                    $.ajax({
                        type: "POST",
                        url: base_url + 'product/giveRating',
                        data: {
                            'ProductID': <?php echo $product->ProductID; ?>,
                            'UserID': <?php echo $this->session->userdata['user']->UserID; ?>,
                            'Rating': rating
                        },
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            hideCustomLoader();
                            showMessage(result.message);
                        }
                    });
                }
            });
        });
        $(function () {
            $(".giveRating").rateYo({
                normalFill: "#fff4d1",
                ratedFill: "#f9c834",
                starWidth: "24px",
                fullStar: true,
                onSet: function (rating, rateYoInstance) {
                    // alert("Rating is set to: " + rating);
                    showCustomLoader();
                    $.ajax({
                        type: "POST",
                        url: base_url + 'product/giveRating',
                        data: {
                            'ProductID': <?php echo $product->ProductID; ?>,
                            'UserID': <?php echo $this->session->userdata['user']->UserID; ?>,
                            'Rating': rating
                        },
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            hideCustomLoader();
                            showMessage(result.message);
                        }
                    });
                }
            });
        });
    </script>
<?php } ?>

<style type="text/css">
    /*Modal Css*/
.modWl {
    max-width: 510px;
    max-height: 875px;
}

div#chk-avail {
    box-shadow: 0px 8px 16px #00000029;
    border-radius: 5px;
    opacity: 1;
}

.mod-cont {
    margin: 10% ;
}

div.mod_hed {
    border: none;
    padding: 0px !important;
  }
  .mod-bod {
    padding: 0px !important; 
  }
  div.mod_fot {
    border: none;
    padding: 0px !important;
  }
h5.hdTxt {
    font-size: 31px !important;
    color: #50456D;
    width: 87px;
    height: 42px;
    padding: 7% 10% 9% 10%;
}

h5.hdTxt span {
    font-size: 41px;
    color: #CE8D8D;
    position: relative;
    bottom: 20px;
}

.edSelectstyle select.fmod.form-control {
    background-color: #FFFFFF;
    box-shadow: 0px 3px 11px #0000000D;
    border: 1px solid #E1E1E1 !important;
    border-radius: 4px;
    opacity: 1;
    height: 37px;
    color: #B28C67;
    padding: 8px 0 5px 22px;
    font-size: 15px !important;
}

.frmcity p strong{
    font-size: 15px;
    color: #555555;
    letter-spacing: 0;
    opacity: 1;
    width: 90px;
    height:25px;
}

.frmcity {
    padding: 13% 10% 5% 10%;
    text-align: left;
}

.rsltTxt h6 {
    font-size: 18px;
    color: #CE8D8D;
    border-top: 1px solid #E2E2E2;
    border-bottom: 1px solid #E2E2E2;
    opacity: 1; 
    font-weight: bolder;
    text-align: left;
    padding: 2% 10% 2% 10%;
}
.branchTXT {
    border-bottom: 1px solid #E2E2E2;  
    line-height: 0.8;
    padding: 3% 10% 5% 10%;
}
.gTxt {
    color: #21AD00;
    letter-spacing: 0;
}

.redTxt {
    color: #C30000;
    letter-spacing: 0;
}
.orangeTxt {
    color: #FF7600;
    letter-spacing: 0;
}
p.hTxt {
    font-size: 15px !important;
    color: #000 !important;
    text-align: left !important;
}

button.btn-success.close-icon {
    background-color: #50456D;
    border-radius: 6px;
    opacity: 1;
    width: 166px;
    height: 37px;
    color: #fff;
    border: none;
    font-size: 15px;
}
button.ck.btn-secondary {
    background-color: #f4ebd3 !important;
    font-size: 20px !important;
    font-weight: normal !important;
    max-width: 250px;
    height: 42px;
    border-radius: 8px;
    color: #BD9371;
    border: none;
    width: 250px;
}

.cls-bt {
    padding: 5%; 
}
.content.single-products .product-info .input-group {
    width: 160px !important;
}
.content.single-products .product-info .input-group-btn > .btn {
    border-radius: 8px;
}
button.ck.btn-secondary.noStocl {
    color: red;
}
.product-info h2 {
  font-size: 41px;
  font-weight: 400;
  text-transform: none;
  margin: 0 0 10px;
  color: #CE8D8D;
}
</style>
<!-- Modal -->
<div id="nutritionInfo" class="modal fade nutritionInfoModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class=""><?php echo lang('Nutrition_Facts'); ?></h2>
      </div>
      <div class="modal-body">
      <table class="table">
<colgroup>
<col style="width: 156pt" width="208">
<col style="width: 56pt" width="75">
<col style="width: 67pt" width="89">
</colgroup>
<tbody>
<tr>
<td colspan="4" width="372"><p><b><?php echo $result->Title; ?></b></p></td>
</tr>
<tr>
<td colspan="4" width="372"><p><b><?php echo lang('Serving_Size'); ?> </b> <b class="black"> <?php echo $result->ServingSize; ?></b><p></td>
</tr>
<!--<tr>
<td colspan="3" width="372"><span>Servings Per Container about 8</span></td>
</tr>
<tr>-->
<td class="text-center" colspan="3"><b class="gold"><?php echo lang('Nutrition'); ?> &nbsp;</b></td>
<td class="text-center" width="89"><b class="gold"> Value</b></td>
</tr>
<?php if(!empty($product_nutritions)){
    foreach ($product_nutritions as $key => $value) { ?>
    <tr>
        <td class="text-center" colspan="3"><b><?php echo $value['Title']; ?></b></td>
        <td  width="89"><span><?php echo $value['Quantity']; ?></span></td>
       
    </tr>

    <?php   
    }
} ?>

<!--<tr>
<td colspan="3"  width="372"><span>Vitamin D 4%• Potassium 2% • Calcium 2% • Iron 6%&nbsp;</span></td>
</tr>-->
<tr height="33" style="height: 24.75pt">
<td colspan="4" width="372">
<p class="goldBlack"><b>Ingredients:</b> <?php echo $result->Ingredients; ?></p>
</td>
</tr>
</tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Close'); ?></button>
      </div>
    </div>

  </div>
</div>