<style>

.col-left .form-group.edSelectStyle.white {
    margin: -25px 0 -6px;
}

</style>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false&key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4"></script>
<section class="content contact stores">
    <div class="container-fluid p-0">
        <div class="customRowEd">
            <div class="col-left">
                <h2><?php echo lang('stores'); ?></h2>
                <div class="form-group edSelectStyle white">
                    <label for="cities"><?php echo lang('please_select_city'); ?></label>
                    <select class="form-control store_cities" name="citiess">
                        <option value="0"><?php echo lang('select'); ?></option>
                        <?php
                        $check_city = array();
                        $branches_html = '';

                        if($stores){
                         
                         foreach($stores as $key => $store){ 

                            if(!in_array($store->CityID,$check_city)){

                                echo '<option value="'.$store->CityID.'">'.$store->CityTitle.'</option>';
                            }

                            $check_city[] = $store->CityID;
                            $working_hours = '';
                            if($store->WorkingHoursSaturdayToThursdayFrom != '' && $store->WorkingHoursSaturdayToThursdayTo){
                                 $working_hours .= 'Saturday-Thursday : '.$store->WorkingHoursSaturdayToThursdayFrom .' - '.$store->WorkingHoursSaturdayToThursdayTo .'<br>';
                            }
                            if($store->WorkingHoursFridayFrom != '' && $store->WorkingHoursFridayTo){
                                 $working_hours .= 'Friday : '.$store->WorkingHoursFridayFrom .' - '.$store->WorkingHoursFridayTo;
                            }
                            


                            $branches_html .= '<li class="city city-'.$store->CityID.'">
                                <a href="javascript:void(0);" class="city-href-'.$key.'" data-city-'.$key.' = '.$store->CityTitle.'  data-title-'.$key.' = '.str_replace(' ','@',$store->Title).' data-working-hour-'.$key.'="'.$working_hours.'"  data-address-'.$key.'="'.$store->Address.'" onclick="myClick('.$key.');">
                                    <i class="fa fa-map-marker" aria-hidden="true"><b>'.($key + 1).'</b></i>
                                    <h5>'.$store->Title.', '.$store->CityTitle.'</h5>
                                    <p>'.$store->Address.'</p>
                                </a>
                            </li>';


                         } } ?>

                        
                    </select>
                </div>
                <ul class="storeLocs">
                    <?php echo $branches_html; ?>
                </ul>
            </div>
            <div class="col-right">
                <div class="sideinners">
                    <div id="map"><br/>
                    </div>
                    <div class="col-left hide">
                        <div class="d-block">
                            <span class="hideBoxEd"><i class="fa fa-angle-left"></i></span>
                            <h2 id="store_city"></h2>
                            <ul>
                                <li>
                                    <p id="store_title">Othman Ibn Afaan St .</p>
                                    <p>
                                        <strong><?php echo lang('opening_hour'); ?></strong>
                                       <span id="store_working_hours"> Saturday-Thursday: 09:00-23:00
                                        <br>
                                        Friday: 14:00-23:00</span>
                                    </p>
                                    <p>
                                        <strong><?php echo lang('address'); ?></strong>
                                        <span id="store_address">
                                        Ar Rayyan, Othman Ibn Afaan St, Dammam</span>
                                    </p>
                                    <!--<p>
                                        <strong>Contacts</strong>
                                        Telephone:0508288853
                                        <br>    
                                        Mobile: 0508288853
                                    </p>-->
                                    <!--<button type="button" class="btn btn-primary">Go Now</button>-->
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
</section>
<script type="text/javascript">
    $( document ).ready(function() {
        $('.store_cities').on('change',function(){
            

            $('.city').hide();
            var city_id = $(this).children("option:selected").val();

            if(city_id == 0){
                $('.city').show();
            }else{
                $('.city-'+city_id).show();
            }
            
        });

    });


    var markers = [];
    var center_lat = <?php echo(isset($stores[0]) ? $stores[0]->Latitude : 21.484716); ?>;
    var center_lng = <?php echo(isset($stores[0]) ? $stores[0]->Longitude : 39.189606); ?>;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: new google.maps.LatLng(center_lat, center_lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles:[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "color": "#50456d"
            },
            {
                "saturation": "29"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#b31f1f"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dfd1ae"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#f00000"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text",
        "stylers": [
            {
                "color": "#1a1526"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ff0f0f"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ebe3cd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "color": "#892020"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-84"
            },
            {
                "lightness": "85"
            },
            {
                "gamma": "0.00"
            },
            {
                "weight": "1"
            },
            {
                "color": "#50456d"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#f6c866"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e98d58"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
]
    });
    <?php if ($stores && count($stores) > 0)
    { ?>
    var infowindow = new google.maps.InfoWindow();
    var marker;
    <?php
    $i = 0;
    foreach ($stores as $store)
    { ?>
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(<?php echo $store->Latitude; ?>, <?php echo $store->Longitude; ?>),
         icon: '<?php echo base_url(); ?>assets/frontend/images/pin.svg',
         label: {text: '<?php echo $i+1; ?>' , color: 'white'},
        map: map
    });
    google.maps.event.addListener(marker, 'click', (function (marker) {
        return function () {
            infowindow.setContent('<h5 style="color:black;"><?php echo $store->Title . ', ' . $store->CityTitle; ?></h5>');
            infowindow.open(map, marker);
        }
    })(marker));
    markers.push(marker);
    <?php $i++; } ?>
    <?php } ?>
    function myClick(index) {

        var store_title = $( ".city-href-"+index  ).attr('data-title-'+index);
            store_title = store_title.replace(/@/g,' ');
            //alert(store_title);
        $('#store_city').html($( ".city-href-"+index ).attr('data-city-'+index));
        $('#store_title').html(store_title);
        $('#store_working_hours').html($( ".city-href-"+index  ).attr('data-working-hour-'+index));
        $('#store_address').html($( ".city-href-"+index  ).attr('data-address-'+index));
        google.maps.event.trigger(markers[index], 'click');

        


    }
$('.storeLocs > li').click(function(){
    $('.storeLocs > li').removeClass('active');
    $(this).addClass('active');
    $('.customRowEd .sideinners .col-left').removeClass('hide');
});
$('span.hideBoxEd').click(function(){
    $('.customRowEd .sideinners .col-left').addClass('hide');
});
</script>