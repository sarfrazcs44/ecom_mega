$(document).ready(function() {

    drawDatatableReport('datatables',columns,ParentID,get_data);
        /*$('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            },
            "aaSorting": []

        });


        var table = $('#datatables').DataTable();*/

        

        $('.card .material-datatables label').addClass('form-group');
    });
function drawDatatableReport(table_id,columns,customID,get_data){
    $('#'+table_id).DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + '/cms/report/'+get_data,
        columns: columns,
        fnServerParams: function (aoData) {
            aoData.customID = customID;
        },
        columnDefs: [ {
            'targets': [0], /* column index [0,1,2,3]*/
            'orderable': false, /* true or false */
        }],
        dom: 'Bflrtip',
        "lengthMenu": [[10, 25, 50, 9999999999999999], [10, 25, 50, "All"]],
        buttons: [
            {
                extend: 'csv',
                text: 'Export Results As Csv',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel',
                text: 'Export Results As Excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                text: 'Export Results As Pdf',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ],
        
    });
}