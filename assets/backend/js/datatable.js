 $(document).ready(function() {

    drawDatatable('datatables',columns,ParentID);
        /*$('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            },
            "aaSorting": []

        });


        var table = $('#datatables').DataTable();*/

        

        $('.card .material-datatables label').addClass('form-group');
    });

 $(document).ready(function() {
     $('.datatables').DataTable({
         "pagingType": "full_numbers",
         "lengthMenu": [
             [10, 25, 50, -1],
             [10, 25, 50, "All"]
         ],
         responsive: true,
         language: {
             search: "_INPUT_",
             searchPlaceholder: "Search records",
         },
         "aaSorting": []

     });


     var table = $('.datatables').DataTable();

    


     $('.datatables_csv').DataTable({
         dom: 'Bfrtip',
         "pagingType": "full_numbers",
         "lengthMenu": [
             [10, 25, 50, -1],
             [10, 25, 50, "All"]
         ],
         responsive: true,
         language: {
             search: "_INPUT_",
             searchPlaceholder: "Search records",
         },
         "aaSorting": [],
         buttons: [
            {
                extend: 'csv',
                text: 'Export Results As Csv',
                exportOptions: {
                    columns: ':visible:not(.notexport)'
                }
            },
            {
                extend: 'excel',
                text: 'Export Results As Excel',
                exportOptions: {
                    columns: ':visible:not(.notexport)'
                }
            },
            {
                extend: 'pdf',
                text: 'Export Results As Pdf',
                exportOptions: {
                    columns: ':visible:not(.notexport)'
                }
            }
        ],
        orderCellsTop: true,
        fixedHeader: true

     });

    var table = $('.datatables_csv').DataTable();
    if($('.datatables_csv').hasClass('search_fields')){
        $('.datatables_csv thead tr').clone(true).appendTo( '.datatables_csv thead' );
        $('.datatables_csv thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title != 'Action'){
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
     
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                });

            }
            
        });
    }

    $('.datatables_csv2').DataTable({
         dom: 'Bfrtip',
         "pagingType": "full_numbers",
         "lengthMenu": [
             [10, 25, 50, -1],
             [10, 25, 50, "All"]
         ],
         responsive: true,
         language: {
             search: "_INPUT_",
             searchPlaceholder: "Search records",
         },
         "aaSorting": [],
         buttons: [
            {
                extend: 'csv',
                text: 'Export Results As Csv',
                exportOptions: {
                    columns: ':visible:not(.notexport)'
                }
            },
            {
                extend: 'excel',
                text: 'Export Results As Excel',
                exportOptions: {
                    columns: ':visible:not(.notexport)'
                }
            },
            {
                extend: 'pdf',
                text: 'Export Results As Pdf',
                exportOptions: {
                    columns: ':visible:not(.notexport)'
                }
            }
        ],
        orderCellsTop: true,
        fixedHeader: true

     });

    var table2 = $('.datatables_csv2').DataTable();
    if($('.datatables_csv2').hasClass('search_fields')){
        $('.datatables_csv2 thead tr').clone(true).appendTo( '.datatables_csv2 thead' );

        $('.datatables_csv2 thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title != 'Action'){
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
     
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table2.column(i).search() !== this.value ) {
                        table2
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                });

            }
            
        });
    }
    


   
 
    



     $('.card .material-datatables label').addClass('form-group');
 });
 function drawDatatable(table_id,columns,customID){
    $('#'+table_id).DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + '/cms/'+CallUrl+'/getEntries',
        columns: columns,
        fnServerParams: function (aoData) {
            aoData.customID = customID;
        },
        columnDefs: [ {
            'targets': [0], /* column index [0,1,2,3]*/
            'orderable': false, /* true or false */
        }],
        dom: 'Bflrtip',
        "lengthMenu": [[10, 25, 50, 9999999999999999], [10, 25, 50, "All"]],
        buttons: [
            {
                extend: 'csv',
                text: 'Export Results As Csv',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel',
                text: 'Export Results As Excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                text: 'Export Results As Pdf',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ],
        
    });
}
