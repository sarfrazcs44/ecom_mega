$(document).ready(function () {
    $(".form_data").submit(function (e) {
        e.preventDefault();


        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                }
                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
    });


    if ($('.jFiler-input-text').length > 0) {

        $('.jFiler-input-text').hide();
    }


});
var page = -1;
$(document).ready(function () {
    //$('.dataTables_length').children('label').children('select').addClass('selectpicker').data('style','select-with-transition');

    
   
    $(document).on('click','.store_btn_click,.load_more', function () {
                var product_id = $(this).attr('data-product-id');
                $('#ProductID').val(product_id);
                $('#exampleModalLabel').html($(this).attr('data-product-title'));
                var form_action_type = $(this).attr('data-action');
                $('.load_more').attr('data-product-id',product_id);
                $('.load_more').attr('data-product-title',$(this).attr('data-product-title')); 
                $('.load_more').attr('data-action',form_action_type); 

                if($(this).hasClass('store_btn_click')){
                     $('#model_body').html('');
                     page = -1;
                 
                }

                page = page + 1;



                if(form_action_type == 'addon'){
                    $('#availability_form').attr('action',base_url+'cms/product/saveAddonProducts');
                   
                    var get_data_action =  base_url + 'cms/product/addonProducts/'+product_id;
                }else if(form_action_type == 'add_quantity'){
                    $('#availability_form').attr('action',base_url+'cms/product/saveProductAvailability');
                   
                    var get_data_action =  base_url + 'cms/product/productAvailability/'+product_id;
                }else if(form_action_type == 'cross_sell'){
                    $('#availability_form').attr('action',base_url+'cms/product/saveCrossSellingProduct');
                   
                    var get_data_action =  base_url + 'cms/product/crossSellingProduct/'+product_id;
                }else if(form_action_type == 'add_variant'){
                    $('#availability_form').attr('action',base_url+'cms/product/saveproductVariant');
                   
                    var get_data_action =  base_url + 'cms/product/productVariant/'+product_id;
                }

                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: get_data_action,
                    dataType: "json",
                    cache: false,
                    data:{'page_no': page},
                    //async:false,
                    success: function (result) {
                        if(form_action_type == 'add_variant' || result.html == '')
                        {
                            $('.load_more').hide();
                        }else{
                            $('.load_more').show();
                        }

                        $('#model_body').append(result.html);

                        if(form_action_type == 'add_variant'){
                            $('#attribute_body').html(result.html_attribute);
                        }else{
                            $('#attribute_body').html('');
                        }


                        $('#exampleModal').modal('show');

                        if($('#VariantID').length > 0){
                             $('#VariantID').selectpicker('refresh');
                        }
                        if($('#AttributeID').length > 0){
                             $('#AttributeID').selectpicker('refresh');
                        }



                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });


        });
});

$(document).ready(function () {
                 
       $(document).on('change','#VariantID',function(){

            var VariantID = $(this).val();
            if(VariantID == '')
            {
                $('#AttributeID').html('<option value="">Choose Attribute</option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/variant/getAttribute',
                    data: {
                        'VariantID': VariantID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {
                        $('#AttributeID').html(result.html);
                        $('#AttributeID').selectpicker('refresh');
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
});


$(document).ready(function () {
                 
       $(document).on('change','#AttributeID',function(){

           var html = '';
            var inputs = $(".price_attribute");

            var input_array = [];

                for(var i = 0; i < inputs.length; i++){

                    input_array[$(inputs[i]).attr('data-id')] = $(inputs[i]).val();
                    console.log(input_array);

                }

            $("#AttributeID :selected").map(function(i, el) {

               if(typeof input_array[$(el).val()] === 'undefined') {
                    // does not exist
                    html += '<label for="price" > '+$(el).text()+' Price :</label><input name="price['+$(el).val()+']" class="price_attribute" data-id="'+$(el).val()+'" value="" /><br><br>'
           
                }
                else {
                   html += '<label for="price" > '+$(el).text()+' Price :</label><input name="price['+$(el).val()+']" class="price_attribute" data-id="'+$(el).val()+'" value="'+input_array[$(el).val()]+'" /><br><br>'
           
                }


                
               
            }).get();

            $('#attribute_body').html(html);
           
       });
});


// .dataTables_length

function showSuccess(message_text) {
    type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">' + message_text + '</span>'

    }, {
        type: type[2], // 2nd index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function showError(message_text) {
    type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">' + message_text + '</span>'

    }, {
        type: type[4],// 4th index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function deleteRecord(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    if (confirm(delete_msg)) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != false) {
                    showError(result.error);
                } else {
                    $('#' + id).remove();
                    showSuccess(result.success);
                }


                if (reloadUrl != "") setTimeout(function () {
                    document.location.href = reloadUrl;
                }, 1000);

            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}

function suspendUser(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to suspend this?")) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {'id': id, 'form_type': 'suspend'},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != 'false') {
                    if ($('#validatio-msg').hasClass('alert-success')) {
                        $('#validatio-msg').removeClass('alert-success');
                    }
                    if (!$('#validatio-msg').hasClass('alert-danger')) {
                        $('#validatio-msg').addClass('alert-danger');
                    }

                    $("#validatio-msg").html(result.error);
                    $('#validatio-msg').show();

                } else {
                    if ($('#validatio-msg').hasClass('alert-danger')) {
                        $('#validatio-msg').removeClass('alert-danger');
                    }
                    if (!$('#validatio-msg').hasClass('alert-success')) {
                        $('#validatio-msg').addClass('alert-success');
                    }

                    $("#validatio-msg").html(result.success);
                    $('#validatio-msg').show();
                    if (reloadUrl != "") setTimeout(function () {
                        document.location.href = base_url + '' + reloadUrl;
                    }, 1000);
                }

            }, complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}

function activateUser(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to activate this?")) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {'id': id, 'form_type': 'activate'},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != 'false') {
                    if ($('#validatio-msg').hasClass('alert-success')) {
                        $('#validatio-msg').removeClass('alert-success');
                    }
                    if (!$('#validatio-msg').hasClass('alert-danger')) {
                        $('#validatio-msg').addClass('alert-danger');
                    }

                    $("#validatio-msg").html(result.error);
                    $('#validatio-msg').show();

                } else {
                    if ($('#validatio-msg').hasClass('alert-danger')) {
                        $('#validatio-msg').removeClass('alert-danger');
                    }
                    if (!$('#validatio-msg').hasClass('alert-success')) {
                        $('#validatio-msg').addClass('alert-success');
                    }

                    $("#validatio-msg").html(result.success);
                    $('#validatio-msg').show();
                    if (reloadUrl != "") setTimeout(function () {
                        document.location.href = base_url + '' + reloadUrl;
                    }, 1000);
                }

            }, complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}


function deleteImage(id, actionUrl) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to delete?")) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete_image'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {


                if (result.error != 'false') {
                    alert('There is something went wrong');

                } else {
                    $('#img-' + id).remove();
                    alert('Deleted Successfully');
                }


            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}


function redirect(redirect_to) {
    redirect_to = base_url + redirect_to;
    window.location.href = redirect_to;
}

$(".validate_ksa_number").blur(function () {
    var phone = $(this).val();
    if (phone.charAt(0) == '+' && phone.charAt(1) == '9' && phone.charAt(2) == '6' && phone.charAt(3) == '6' && phone.charAt(4) == '5' && phone.length == 13) {
        return true;
    } else {
        var name = $(this).attr('name');
        showError("You have entered an invalid phone number in " + name + " field !");
        return false;
    }
});

$(".number-with-decimals").keydown(function (event) {
    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 110 ||
        event.keyCode == 190) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
    //if a decimal has been added, disable the "."-button

});

$(".number-only").keydown(function (event) {
    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if (
        (event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 107 || event.keyCode == 187
    ) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
    //if a decimal has been added, disable the "."-button

});

function changeOrderStatus() {
    var Status = $('#Status').val();
    var OrderID = $('#OrderID').val();
    if (confirm('Are you sure to change status of this order?')) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        if(Status == 4){
            $.ajax({
                type: "POST",
                url: base_url + 'cms/orders/get_otp',
                data: {'OrderID': OrderID, 'Status': Status},
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {


                   
                    
                        $('#response_data').html(result.html);
                        $('#exampleModal').modal('show');

                }, complete: function () {
                    $.unblockUI();
                }
            });
        }else{
            $.ajax({
                type: "POST",
                url: base_url + '' + 'cms/orders/update',
                data: {
                    'OrderID': OrderID,
                    'Status': Status,
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {

                    if (result.error != false) {
                        showError(result.error);
                    } else {
                        showSuccess(result.success);
                        setTimeout(function () {
                            document.location.reload();
                        }, 2000);
                    }

                },
                complete: function () {
                    $.unblockUI();
                }
            });
        }

        
        return true;
    }
}

function cancelOrder(OrderID, UserID) {
    if (confirm('Are you sure to cancel this booking?')) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + 'cms/booking/update',
            data: {
                'OrderID': OrderID,
                'UserID': UserID,
                'Status': 6,
                'SendCancelBookingNotification': 'yes'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess('Booking cancelled successfully');
                    setTimeout(function () {
                        document.location.reload();
                    }, 2000);
                }

            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    }
}

function getStoresForCity(CityID) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

    $.ajax({
        type: "POST",
        url: base_url + '' + 'cms/store/getStoresForCity',
        data: {
            'CityID': CityID
        },
        dataType: "json",
        cache: false,
        //async:false,
        success: function (result) {
            /*var technicians_array = result.technicians_array;
            var vehicle_array = result.vehicle_array;
            var technicians_string = result.technicians_string;
            var vehicles_string = result.vehicles_string;
            console.log(technicians_string);
            // $.parseJSON(technicians_string);
            $('.selectpicker').selectpicker();

            for (var i = 0; i < 4; i++) {
                var o = new Option("option text"+i, "value"+i);
                /// jquerify the DOM object 'o' so we can use the html method
                $(o).html("option text"+i);
                $(".selectpicker").append(o);
            }


            $(".selectpicker").selectpicker('refresh');*/

            $('.storeDD').html(result.html);
        },
        complete: function () {
            $.unblockUI();
        }
    });
    return true;
}

function getUsersForStore(StoreID) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

    $.ajax({
        type: "POST",
        url: base_url + '' + 'cms/user/getUsersForStore',
        data: {
            'StoreID': StoreID
        },
        dataType: "json",
        cache: false,
        //async:false,
        success: function (result) {
            /*var technicians_array = result.technicians_array;
            var vehicle_array = result.vehicle_array;
            var technicians_string = result.technicians_string;
            var vehicles_string = result.vehicles_string;
            console.log(technicians_string);
            // $.parseJSON(technicians_string);
            $('.selectpicker').selectpicker();

            for (var i = 0; i < 4; i++) {
                var o = new Option("option text"+i, "value"+i);
                /// jquerify the DOM object 'o' so we can use the html method
                $(o).html("option text"+i);
                $(".selectpicker").append(o);
            }


            $(".selectpicker").selectpicker('refresh');*/

            $('.storeDD').html(result.html);
        },
        complete: function () {
            $.unblockUI();
        }
    });
    return true;
}

function parseIt(jsonString) {
    "use strict";
    var result = [], parsedObject, key;

    parsedObject = JSON.parse(jsonString);
    for (key in parsedObject) {
        if (parsedObject.hasOwnProperty(key)) {
            result.push({LocationsName: key, LocationCount: parsedObject[key]});
        }
    }
    return result;
}


/*
* Asif
* Product create form multiselect function
* */
$(document).ready(function () {
    $("#Sl_Chocolate").on('change', function () {
        $("#multiple_fields").html('');

        $('#Sl_Chocolate :selected').each(function () {
            $selected_value = $(this).val();
            $selected_text = $(this).text();
            $data_value = $('.nutrition_' + $selected_value).attr('data-quantity');
            ;
            $fields = `
                        <div class="col-md-6" id="sl_chocolate_text">
                            <div class="form-group label-floating">
                                <label class="control-label">&nbsp;</label>
                                <input type="text" class="form-control" name="sl_text_${$selected_value}" value="${$selected_text}">
                            </div>
                        </div>
                        <div class="col-md-6" id="sl_chocolate_qty">
                            <div class="form-group label-floating">
                                <label class="control-label">Write Quantity</label>
                                <input type="text" class="form-control" name="NutritionQuantity[${$selected_value}]" value="${$data_value}">
                            </div>
                        </div>
                        
           `;

            $("#multiple_fields").append($fields);

        });

    });

 

    $(".add_whats_inside").on('click', function () {
       
        $fields = `<div class="row" id="whats_inside`+inside_count+`">
        <div class="col-md-1">
                            <i class="fa fa-times delete_inside_added_row" onclick="removeRow(`+inside_count+`)" style="margin-top:30px;cursor:pointer;" aria-hidden="true"></i>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Title Eng</label>
                                <input type="text" class="form-control" name="WhatsInsideTitle[]" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Title Arb</label>
                                <input type="text" class="form-control" name="WhatsInsideTitleAr[]" value="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Image</label>
                                <input type="file" name="WhatsInsideImage`+inside_count+`[]">
                            </div>
                        </div>
                    </div>
                        
           `;


        $("#add_whats_inside_fields").append($fields);
        inside_count++;
    

    });

    



});

function removeRow(id){
    //alert();
    $('#whats_inside'+id).remove();
}

$(document).ready(function () {
    /*$('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false                 // set focus to editable area after initializing summernote
    });*/

    $(".summernote").summernote({
        styleWithSpan: true,
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['view', ['fullscreen', 'codeview']],
        ]
    });

    $('.inline-editor').summernote({
        airMode: true
    });

    $('.custom_datepicker').datetimepicker({format: 'YYYY-MM-DD'});
});

setTimeout(function () {
    $(".summernote-arb").css("direction", "rtl");
}, 1000);

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
}

//var base_url = "<?php echo base_url() ?>";
var system_timezone = moment.tz.guess();
eraseCookie('system_timezone');
setCookie('system_timezone', system_timezone, 7);
console.log("Local system timezone is: " + system_timezone);


// editable mode
$(document).ready(function () {
    /*if (localStorage.getItem('edit_state') == 'YES') {
        $("#toggleEditPermissions").attr('checked', 'checked');

        // enable datatable edit fields
        if ($("#datatables thead tr th:last-child").text() == 'Action') {
            $('#datatables thead tr th:last-child, #datatables tbody tr td:last-child').show();
        }


        var str = $(".datatable thead tr th:last-child").text();
        var check = str.includes("Action"); 
        if (check) {
            $('.datatable thead tr th:last-child, .datatable tbody tr td:last-child').show();
        }



        $('.toolbar > a').show();
    } else if (localStorage.getItem('edit_state') == 'NO') {
        $("#toggleEditPermissions").attr('checked', false);

        // disable datatable edit fields
        if ($("#datatables thead tr th:last-child").text() == 'Action') {
            $('#datatables thead tr th:last-child, #datatables tbody tr td:last-child').hide();
            $('table#dataTable > tbody > tr > td:first-child').attr('style', 'padding: 16px !important');
        }


        var str = $(".datatable thead tr th:last-child").text();
        var check = str.includes("Action"); 
        


        if (check) {
            $('.datatable thead tr th:last-child, .datatable tbody tr td:last-child').hide();
            $('table.datatable > tbody > tr > td:first-child').attr('style', 'padding: 16px !important');
        }

        $('.toolbar > a').hide();
    } else {
        $("#toggleEditPermissions").attr('checked', false);

        // disable datatable edit fields
        if ($("#datatables thead tr th:last-child").text() == 'Action') {
            $('#datatables thead tr th:last-child, #datatables tbody tr td:last-child').hide();
            $('table#dataTable > tbody > tr > td:first-child').attr('style', 'padding: 16px !important');
        }


        var str = $(".datatable thead tr th:last-child").text();
        var check = str.includes("Action"); 

        if (check) {
            $('.datatable thead tr th:last-child, .datatable tbody tr td:last-child').hide();
            $('table.datatable > tbody > tr > td:first-child').attr('style', 'padding: 16px !important');
        }

        $('.toolbar > a').hide();
    }
    $("#toggleEditPermissions").on('click', function () {

        $editing_state = 'NO';
        if ($(this).prop('checked')) {
            $editing_state = 'YES';

            // enable datatable edit fields
            if ($("#datatables thead tr th:last-child").text() == 'Action') {
                $('#datatables thead tr th:last-child, #datatables tbody tr td:last-child').show();
            }

            var str = $(".datatable thead tr th:last-child").text();
            var check = str.includes("Action"); 

            if (check) {
                $('.datatable thead tr th:last-child, .datatable tbody tr td:last-child').show();
            }

            $('.toolbar > a').show();
        } else {
            // disable datatable edit fields
            if ($("#datatables thead tr th:last-child").text() == 'Action') {
                $('#datatables thead tr th:last-child, #datatables tbody tr td:last-child').hide();
                $('table#datatables > tbody > tr > td:first-child').attr('style', 'padding: 16px !important');
            }

            var str = $(".datatable thead tr th:last-child").text();
            var check = str.includes("Action"); 

             if (check) {
                $('.datatable thead tr th:last-child, .datatable tbody tr td:last-child').hide();
                $('table.datatable  > tbody > tr > td:first-child').attr('style', 'padding: 16px !important');
            }

            $('.toolbar > a').hide();
        }
        $local_state = localStorage.setItem('edit_state', $editing_state);
    });*/
});

function showCustomLoader() {
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
}

function hideCustomLoader() {
    $.unblockUI();
}

$(".ticketMessage").submit(function (e) {
    e.preventDefault();
    var form_id = $(this).attr('id');
    var formValidated = validateForm(form_id);
    if (formValidated) {
        showCustomLoader();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                hideCustomLoader();
                console.log(result.message);
                $('.msgbox').html(result.html);
                $form[0].reset();
                $('.msgbox').animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
            }
        });
    }
});

function validateForm(specific_form_id) {
    var isValid = true;
    $('.required').siblings('label').removeClass('error');
    $('.required').removeClass('error-border');
    $('form#' + specific_form_id + ' .required').each(function () {
        if ($(this).val() === '') {
            $(this).siblings('label').addClass('error');
            $(this).addClass('error-border');
            isValid = false;
        }
        if ($(this).attr('type') == 'email') {
            var validEmail = validateEmail($(this).val());
            if (!validEmail) {
                $(this).siblings('label').addClass('error');
                $(this).addClass('error-border');
                isValid = false;
            }
        }
        if ($(this).is("select")) {
            if ($(this).val() > 0) {
                // all ok
            } else {
                $(this).siblings('label').addClass('error');
                $(this).addClass('error-border');
                isValid = false;
            }
        }
    });
    return isValid;
}

function showRecords() {
    var GroupType = $('#GroupType').val();
    var MinimumValue = $('#MinimumValue').val();
    var MaximumValue = $('#MaximumValue').val();
    var FromDate = $('#FromDate').val();
    var ToDate = $('#ToDate').val();
    if (MinimumValue != '' && MaximumValue != '' || GroupType == 'AllUsers') {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + 'cms/CustomerGroup/showRecords',
            data: {
                'CustomerGroupType': GroupType,
                'MinimumValue': MinimumValue,
                'MaximumValue': MaximumValue,
                'FromDate': FromDate,
                'ToDate': ToDate
            },
            dataType: "json",
            cache: false,
            success: function (result) {
                if (result.status == true) {
                    $('#showRecords').html(result.html);
                    if (result.show_group_create_button == true) {
                        $('.create_customer_group_button').show();
                    } else {
                        $('.create_customer_group_button').hide();
                    }
                } else {
                    showError(result.message)
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
    } else {
        showError('All fields marked with * are required');
    }
}

function openCustomerGroupTitlePopup() {
    var selected = new Array();
    $("input:checkbox[name=custom_group_users]:checked").each(function () {
        selected.push($(this).val());
    });

    if (selected.length > 0) {
        var customer_ids = selected.join(',');
        $('#CustomerIds').val(customer_ids);
        $('#CreateCustomerGroupTitleModal').modal('show');
    } else {
        showError('Atleast one user to be selected to create group')
    }
}

function createCustomerGroup() {
    var GroupType = $('#GroupType').val();
    var MinimumValue = $('#MinimumValue').val();
    var MaximumValue = $('#MaximumValue').val();
    var FromDate = $('#FromDate').val();
    var ToDate = $('#ToDate').val();
    var CustomerGroupTitle = $('#CustomerGroupTitle').val();
    var CustomerIds = $('#CustomerIds').val();
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

    $.ajax({
        type: "POST",
        url: base_url + 'cms/CustomerGroup/action',
        data: {
            'form_type': 'save',
            'CustomerGroupType': GroupType,
            'MinimumValue': MinimumValue,
            'MaximumValue': MaximumValue,
            'FromDate': FromDate,
            'ToDate': ToDate,
            'CustomerGroupTitle': CustomerGroupTitle,
            'CustomerIds': CustomerIds
        },
        dataType: "json",
        cache: false,
        success: function (result) {
            if (result.error != false) {
                showError(result.error);
            } else {
                showSuccess(result.success);
            }
            if (result.redirect) {
                setTimeout(function () {
                    window.location.href = base_url + result.url;
                }, 1000);
            }
        },
        complete: function () {
            $.unblockUI();
        }
    });
}

$(document).on('click','.changeOrderStatus', function () {
    var StatusTitle = $(this).data('status_title');
    if (confirm("Are you sure to change order status to " + StatusTitle + "?")) {
        var OrderID = $(this).data('order_id');
        var Status = $(this).data('order_status');
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        if(StatusTitle == 'Delivered'){
            $.ajax({
                type: "POST",
                url: base_url + 'cms/orders/get_otp',
                data: {'OrderID': OrderID, 'Status': Status},
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {


                   
                    
                        $('#response_data').html(result.html);
                        $('#exampleModal').modal('show');

                }, complete: function () {
                    $.unblockUI();
                }
            });
        }else{


            $.ajax({
                type: "POST",
                url: base_url + 'cms/orders/update',
                data: {
                    'OrderID': OrderID,
                    'Status': Status
                },
                dataType: "json",
                cache: false,
                success: function (result) {
                    if (result.error != false) {
                        showError(result.error);
                    } else {
                        showSuccess(result.success);
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                },
                complete: function () {
                    $.unblockUI();
                }
            });
        }
        
    }
});

function closeTicket(TicketID, IsClosed) {
    if (IsClosed == 1) {
        var t_msg = "Are you sure you want to close this ticket?";
    } else if (IsClosed == 2) {
        var t_msg = "Are you sure you want to reopen this ticket?";
    }
    if (confirm(t_msg)) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + 'cms/ticket/update',
            data: {
                'TicketID': TicketID,
                'IsClosed': IsClosed
            },
            dataType: "json",
            cache: false,
            success: function (result) {
                showSuccess(result.message);
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            },
            complete: function () {
                $.unblockUI();
            }
        });
    }

}

$("#IsCustomizedProduct").on('click', function () {
    if ($(this).prop('checked')) {
        $('#ForIsCustomizedProduct').show();
    } else {
        $('#ForIsCustomizedProduct').hide();
        $('#BoxIDs').prop('selectedIndex',0);
    }
});

$("#IsCorporateProduct").on('click', function () {
    if ($(this).prop('checked')) {
        $('#ForIsCorporateProduct').show();
    } else {
        $('#ForIsCorporateProduct').hide();
    }
});