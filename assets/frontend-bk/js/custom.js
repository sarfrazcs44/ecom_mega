var ProductIDs = [];
new WOW().init();
/*$(window).on('load', function () {
    $(".loader-wrapper").fadeOut();
});*/

$(document).ready(function () {
    $('#after').bootstrapNumber();
    $('#after1').bootstrapNumber();
    $('#after2').bootstrapNumber();
    $('#after3').bootstrapNumber();
    $('#after4').bootstrapNumber();
    $('#after5').bootstrapNumber();
    $('#after6').bootstrapNumber();
    $('#after7').bootstrapNumber();
    $('#after8').bootstrapNumber();
    $('#after9').bootstrapNumber();
    $('#after10').bootstrapNumber();
    $('#after11').bootstrapNumber();
    $('#after12').bootstrapNumber();
    $('#after13').bootstrapNumber();
    $('.btn-changes').show();
    $('#pfields').click(function () {
        $(this).hide();
        $('.changes').hide();
        $('.phiddenfields').show();
        $('.phiddenfields').children('.form-control').attr('disabled', false);
        $('.phiddenfields').children('.form-control').addClass('required');
        $('.btn-changes').show();
    });
    $('#customice li').click(function () {
        var chocobox_size = $('#custombox').data('box_size');
        if (ProductIDs.length < chocobox_size) {
            $(this).clone().appendTo("#custombox ul");
            var index = ProductIDs.length;
            var product_id = $(this).data('product_id');
            var product_price = $(this).data('product_price');
            addItemToArray(index, product_id, product_price);
        } else {
            notifyMessageCustom("You can not add more than " + chocobox_size + " items in the box", "danger");
        }
    });
    //$("ul li").each(function(){
    $(document).on('click', '#custombox ul li', function () {
        var clicked_li_index = $(this).index();
        var product_id = $(this).data('product_id');
        var product_price = $(this).data('product_price');
        removeItemFromArray(clicked_li_index, product_id, product_price);
        // $(this).fadeOut();
        $(this).remove();
    });
    $('.customletters li').click(function () {
        var chocobox_size = $('#customsgbox').data('box_size');
        if (ProductIDs.length < chocobox_size) {
            $(this).clone().appendTo("#customsgbox ul");
            var index = ProductIDs.length;
            var product_id = $(this).data('product_id');
            var product_price = $(this).data('product_price');
            addItemToArray(index, product_id, product_price);
        } else {
            notifyMessageCustom("You can not add more than " + chocobox_size + " items in the box", "danger");
        }
    });
    $(document).on('click', '#customsgbox ul li', function () {
        var clicked_li_index = $(this).index();
        var product_id = $(this).data('product_id');
        var product_price = $(this).data('product_price');
        removeItemFromArray(clicked_li_index, product_id, product_price);
        // $(this).fadeOut();
        $(this).remove();
    });
    //});
    $('#nav-icon2').click(function () {
        $(this).toggleClass('open');
        $('#mobilenav').toggleClass('active');
        $('body').toggleClass('navBarOpended');
    });

    $('#searchtrigger').click(function () {
        $(this).toggleClass('open');
        $('#searcharea').toggleClass('active');
        $('body').addClass('overflowHidden');
    });

    $('#back').click(function () {
        $('#searcharea').toggleClass('active');
        $('body').removeClass('overflowHidden');

    });
});
if ($('body').hasClass('ar')) {
    $('.brands-slider').owlCarousel({
        loop: true,
        margin: 70,
        nav: true,
        dots: false,
        rtl: true,
        autoplay: true,
        autoplayTimeout: 20000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            700: {
                items: 3
            },
            1000: {
                items: 4
            },
            1300: {
                items: 5
            }
        }
    });
} else {
    $('.brands-slider').owlCarousel({
        loop: true,
        margin: 70,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            700: {
                items: 3
            },
            1000: {
                items: 4
            },
            1300: {
                items: 5
            }
        }
    });
}

function addItemToArray(index, product_id, product_price) {
    ProductIDs.insert(index, product_id);
    ProductIDsJoined = ProductIDs.join();
    $('.ProductIDs').val(ProductIDsJoined);
    var old_price = $('.ProductPrice').val();
    var new_price = Number(old_price) + Number(product_price);
    $('.ProductP').html(formatNumber(new_price));
    $('.ProductPrice').val(new_price);
}

function removeItemFromArray(index, product_id, product_price) {
    ProductIDs.splice(index, 1);
    ProductIDsJoined = ProductIDs.join();
    $('.ProductIDs').val(ProductIDsJoined);
    var old_price = $('.ProductPrice').val();
    var new_price = old_price - product_price;
    $('.ProductP').html(formatNumber(new_price));
    $('.ProductPrice').val(new_price);
}

Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
};

function formatNumber(number) {
    number = number.toFixed(2) + '';
    x = number.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function notifyMessageCustom(message, type) {
    $.notify({
        // options
        message: message
    }, {
        // settings
        type: type,
        placement: {
            from: "top",
            align: align_notify_message
        },
        delay: 7000,
        offset: 20,
        spacing: 10,
        z_index: 1031,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    });
}