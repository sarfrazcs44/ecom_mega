$(document).ready(function (e) {
    if($('.phone').length > 0){

        var input = document.querySelector(".phone");
        var iti = window.intlTelInput(input, {
            allowDropdown: true,
            nationalMode: false,
            separateDialCode:true,
            autoPlaceholder:"off",
            initialCountry: "sa",
            onlyCountries: ["sa"],
            utilsScript: base_url + "assets/frontend/int_tel_input/js/utils.js?1549804213570" // just for formatting/placeholders etc
        });
        /*input.addEventListener("countrychange", function() {
            var countryData = window.intlTelInputGlobals.getCountryData();
            $(".phone").val('');
        });*/
        input.addEventListener('countrychange', function (e) {

             var dialCode = iti.getSelectedCountryData().dialCode;
            
             $("#MobileCode").val('+'+dialCode);
        });

        $(".phone").inputmask({"mask": "999999999"});

    }
    
});

$(".ajaxForm").submit(function (e) {
    e.preventDefault();
    var form_id = $(this).attr('id');
    var formValidated = validateForm(form_id);
    if (formValidated) {
        showCustomLoader();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {

                hideCustomLoader();
                if (typeof result.message !== 'undefined' && result.message != '') {
                    if(typeof result.error !== 'undefined' && result.error){
                        
                        showMessage(result.message, 'danger');
                    }else{

                        showMessage(result.message);
                    }
                    
                }
                if (typeof result.reset !== 'undefined' && result.reset) {
                    $form[0].reset();
                    if (form_id == 'contactForm') {
                        contactUsSubject('Feedback', form_id);
                        grecaptcha.reset()
                    }
                }
                if (typeof result.reload !== 'undefined' && result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
                if (typeof result.redirect !== 'undefined' && result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 2000);
                }
            }
        });
    }
});

$("#signUpForm").submit(function (e) {
    e.preventDefault();
    var form_id = $(this).attr('id');
    var formValidated = validateForm(form_id);
    if (formValidated) {
        showCustomLoader();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                hideCustomLoader();
                if (typeof result.message !== 'undefined' && result.message != '') {
                    showMessage(result.message);
                }
                if (typeof result.reset !== 'undefined' && result.reset) {
                    $form[0].reset();
                    if (form_id == 'contactForm') {
                        contactUsSubject('Feedback', form_id);
                    }
                }
                if (typeof result.reload !== 'undefined' && result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
                if (typeof result.redirect !== 'undefined' && result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 2000);
                }
            }
        });
    }
});

function sendOTP() {
    var Mobile = $('#MobileCode').val() + $('#Mobile').val();
    var Email = $('#Email').val();
    var formValidated = validateForm('signUpForm');
    if (formValidated) {
        if ($('.acceptSignupTerms').prop("checked") == true) {
            showCustomLoader();
            disableResend('timer');//index0 for inner html
            //timer(120,'timer');
            $.ajax({
                type: "POST",
                url: base_url + 'account/sendOTP',
                data: {'Mobile': Mobile, 'Email': Email},
                dataType: "json",
                success: function (result) {
                    hideCustomLoader();
                    if (result.status == true) {
                        showVerifyOTP(result.message);
                    } else {
                        showMessage(result.message);
                    }
                }
            });
        } else {
            showMessage('You must accept terms and conditions to proceed with registration.', 'danger');
        }
    }
}

function sendMobileOTP() {
    var Mobile = $('#MobileNoForOTP').val();

    
    if (Mobile != '') {
            disableResend('timer1');//index 1 for inner html
            //timer(120,'timer1');
            showCustomLoader();
            $.ajax({
                type: "POST",
                url: base_url + 'account/sendOTPUsingMobile',
                data: {'Mobile': Mobile},
                dataType: "json",
                success: function (result) {
                    hideCustomLoader();
                    if (result.status == true) {
                        hideVerifyMobile();
                        showVerifyMobileOTP(result.message);
                        $('#verifyOTPMobileNumberModal').model('show');

                    } else {
                        showMessage(result.message);
                    }
                }
            });
        } else {
            showMessage('Mobile field required.', 'danger');
        }
}

function verifyMobileOTP() {
    var OTP = $('#MobileOTP').val();
    $('.required').removeClass('error-border');
    if (OTP == '') {
        $('#MobileOTP').attr('title', 'This field is required');
        $('#MobileOTP').addClass('error-border');
    } else {
        showCustomLoader();
        $.ajax({
            type: "POST",
            url: base_url + 'account/verifyOTPForSocialLogin',
            data: {'OTP': OTP},
            dataType: "json",
            success: function (verify_result) {
                if (verify_result.status == true) {
                    hideCustomLoader();
                    if (typeof verify_result.message !== 'undefined' && verify_result.message != '') {
                                    showMessage(verify_result.message);
                        }

                     setTimeout(function () {
                                        window.location.href = base_url;
                                    }, 2000);   
                } else {
                    hideCustomLoader();
                    showMessage(verify_result.message, 'danger');
                }


            }
        });
    }
}
function verifyOTP() {
    var OTP = $('#OTP').val();
    $('.required').removeClass('error-border');
    if (OTP == '') {
        $('#OTP').attr('title', 'This field is required');
        $('#OTP').addClass('error-border');
    } else {
        showCustomLoader();
        $.ajax({
            type: "POST",
            url: base_url + 'account/verifyOTP',
            data: {'OTP': OTP},
            dataType: "json",
            success: function (verify_result) {
                if (verify_result.status == true) {
                    var formValidated = validateForm('signUpForm');
                    if (formValidated) {
                        var signUpForm = document.getElementById("signUpForm");
                        $form = $('#signUpForm');
                        $.ajax({
                            type: "POST",
                            url: $form.attr('action'),
                            data: new FormData(signUpForm),
                            dataType: "json",
                            cache: false,
                            contentType: false,
                            processData: false,
                            //async:false,
                            success: function (result) {
                                hideCustomLoader();
                                if (typeof result.message !== 'undefined' && result.message != '') {
                                    showMessage(result.message);
                                }
                                if (typeof result.reset !== 'undefined' && result.reset) {
                                    $form[0].reset();
                                    if (form_id == 'contactForm') {
                                        contactUsSubject('Feedback', form_id);
                                    }
                                }
                                if (typeof result.reload !== 'undefined' && result.reload) {
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 2000);
                                }
                                if (typeof result.redirect !== 'undefined' && result.redirect) {
                                    setTimeout(function () {
                                        window.location.href = base_url + result.url;
                                    }, 2000);
                                }
                            }
                        });
                    }
                } else {
                    hideCustomLoader();
                    showMessage(verify_result.message, 'danger');
                }
            }
        });
    }
}


function disableResend(id)
{
     $(".regenerateOTP").attr("disabled", true);
     timer(60,id);
     
      //$('.regenerateOTP').prop('disabled', true);
      setTimeout(function() {
        // enable click after 1 second
     $('.regenerateOTP').removeAttr("disabled");
        //$('.disable-btn').prop('disabled', false);
      }, 60000); // 1 second delay
}

let timerOn = true;

function timer(remaining,id) {
   // alert(id);
  var m = Math.floor(remaining / 60);
  var s = remaining % 60;
  
  m = m < 10 ? '0' + m : m;
  s = s < 10 ? '0' + s : s;

  document.getElementById(id).innerHTML = m + ':' + s;
  remaining -= 1;
  
  if(remaining >= 0 && timerOn) {
    setTimeout(function() {
        timer(remaining,id);
    }, 1000);
    return;
  }

  if(!timerOn) {
    // Do validate stuff here
    $(".regenerateOTP").attr("disabled", false);
    return;
  }
  
  // Do timeout stuff here
  //alert('Timeout for otp');
}


function showMessage(message, msg_type = 'info') {
    notifyMessage(message, msg_type);
}

function showPopupMessage(message) {
    $('#formMsg').html(message);
    $('#SuccessMsg').modal('show');
}

function hidePopupMessage() {
    $('#SuccessMsg').modal('hide');
}

function showAddedToCart(message) {
    $('#addToCartMsg').html(message);
    $('#addToCartMsgModal').modal('show');
}

function showVerifyOTP(message) {
    $('#verifyOTPMsg').html(message);
    $('#verifyOTPModal').modal('show');
}

function showVerifyMobile(message) {

    $('#verifyMobileMsg').html(message);
    $('#verifyOTPMobileNumberModal').modal('show');
}

function hideVerifyMobile() {
    $('#verifyOTPMobileNumberModal').modal('hide');
}


function showVerifyMobileOTP(message) {

    $('#verifyobileOTPMsg').html(message);
    $('#verifyMobileOTPModal').modal('show');
}

function hideVerifyMobileOTP() {
    $('#verifyMobileOTPModal').modal('hide');
}


function hideVerifyOTP() {
    $('#verifyOTPModal').modal('hide');
}

function validateForm(specific_form_id) {
    var isValid = true;
    $('form#' + specific_form_id + ' .required').siblings('label').removeClass('error');
    $('form#' + specific_form_id + ' .required').removeClass('error-border');
    // $('.required').tooltip('dispose');
    $('form#' + specific_form_id + ' .required').each(function () {
        if ($(this).val() == '') {
            $(this).attr('title', 'This field is required');
            $(this).siblings('label').addClass('error');
            $(this).addClass('error-border');
            isValid = false;
        }
        if ($(this).attr('type') == 'email') {
            var validEmail = validateEmail($(this).val());
            if (!validEmail) {
                $(this).attr('title', 'Please enter a valid email address');
                $(this).siblings('label').addClass('error');
                $(this).addClass('error-border');
                isValid = false;
            }
        }
        if ($(this).attr('type') == 'password') {
            var signup_password = $(this).val();
            var len = signup_password.length;
            if (len < 6) {
                showMessage('Password field must contain atleast 6 characters', 'danger');
                $(this).attr('title', 'Password must contain atleast 6 characters');
                $(this).siblings('label').addClass('error');
                $(this).addClass('error-border');
                isValid = false;
            }
        }
        if ($(this).is("select")) {
            if ($(this).val() > 0) {
                // all ok
            } else {
                $(this).attr('title', 'Please select a value from dropdown');
                $(this).siblings('label').addClass('error');
                $(this).addClass('error-border');
                isValid = false;
            }
        }
    });
    return isValid;
}

$(document).on('keydown', '.password', function (e) {
    if (e.keyCode == 32) return false;
});

function showLoader() {
    $(".loader-wrapper").fadeIn();
}

function hideLoader() {
    $(".loader-wrapper").fadeOut();
}

function showCustomLoader() {
    $('.overlaybg').fadeIn();
}

function hideCustomLoader() {
    $('.overlaybg').fadeOut();
}

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}

function changeLanguage(lang) {
    window.location = base_url + "page/changeLanguage/" + lang;
}

$(".number-with-decimals").keydown(function (event) {
    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 110 ||
        event.keyCode == 190) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
    //if a decimal has been added, disable the "."-button

});

$(".number-only").keydown(function (event) {
    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if (
        (event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 107 || event.keyCode == 187
    ) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
    //if a decimal has been added, disable the "."-button

});

function addToCart(ProductID,Quantity,ItemType,TempItemPrice) {

    if(ProductID == 'undefined' || ProductID == ''){
         ProductID = $('.ProductID').val();
    }

    if(Quantity == 'undefined' || Quantity == ''){
          Quantity = $('.Quantity').val();
    }

    if(ItemType == 'undefined' || ItemType == ''){
          ItemType = $('.ItemType').val();
    }

    if(TempItemPrice == 'undefined' || TempItemPrice == ''){
          TempItemPrice = $('.ProductPrice').val();
    }

    
    
   
    
   // var IsCorporateItem = $('.IsCorporateItem').val();
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'cart/addToCart',
        data: {
            'ProductID': ProductID,
            'Quantity': Quantity,
            'ItemType': ItemType,
            'TempItemPrice': TempItemPrice
        },
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            if (result.status == true) {
                $('.CartItemsCount').html(result.total_cart_items);
                showAddedToCart(result.message);
            } else {
                showMessage(result.message, 'danger');
            }
        }
    });
}

function update_cart(temp_order_id, quantity, item_price) {
    $.ajax({
        type: "POST",
        url: base_url + 'cart/updateCart',
        data: {'TempOrderID': temp_order_id, 'Quantity': quantity, 'Price': item_price},
        dataType: "json",
        success: function (result) {
            // hideCustomLoader();
            $('.CartItemsCount').html(result.total_cart_items);
            $("#TotalPrice_" + temp_order_id).text(result.total);
            setTimeout(function () {
                window.location.reload();
            }, 500);
        }
    });
}

$(document).on('keyup', '.cart_quantity', function () {
    showCustomLoader();
    var quantity = $(this).val();
    var temp_order_id = $(this).data('temp_order_id');
    var item_price = $(this).data('item_price');
    setTimeout(function () {
        update_cart(temp_order_id, quantity, item_price);
    }, 1000);
});

$(document).on('click', '.cart_quantity', function () {
    showCustomLoader();
   
    var temp_order_id = $(this).data('temp_order_id');
     var quantity = parseInt($('.qty-val-'+temp_order_id).html());
   
    var item_price = $(this).data('item_price');

    setTimeout(function () {
        update_cart(temp_order_id, quantity, item_price);
    }, 1000);
});

$(document).on('click', '.input-group-btn', function () {
    var operator = $(this).find('button').text();
    if (operator == '+' || operator == '-') {
        if ($(this).siblings('.cart_quantity').hasClass('update_cart')) {
            showCustomLoader();
            var quantity = $(this).siblings('.cart_quantity').val();
            var temp_order_id = $(this).siblings('.cart_quantity').data('temp_order_id');
            var item_price = $(this).siblings('.cart_quantity').data('item_price');
            setTimeout(function () {
                update_cart(temp_order_id, quantity, item_price);
            }, 1000);
        }
    }
});

function clearCart() {
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure you want to remove this?',
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirm: function () {
                showCustomLoader();
                var obj = {};
                obj['TempOrderID'] = -1;
                $.ajax({
                    type: "POST",
                    url: base_url + 'cart/removeFromCart',
                    data: obj,
                    dataType: "json",
                    success: function (result) {
                        hideCustomLoader();
                        
                        showMessage(result.message);
                        if (typeof result.reload !== 'undefined' && result.reload) {
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                });
            },
            cancel: function () {

            }
        }
    });
}

function removeIt(url, key, value) {
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure you want to remove this?',
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirm: function () {
                showCustomLoader();
                var obj = {};
                obj[key] = value;
                $.ajax({
                    type: "POST",
                    url: base_url + url,
                    data: obj,
                    dataType: "json",
                    success: function (result) {
                        hideCustomLoader();
                        if (typeof result.status !== 'undefined' && result.status == true) {
                            $("#" + key + value).remove();
                        }
                        showMessage(result.message);
                        if (typeof result.reload !== 'undefined' && result.reload) {
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                });
            },
            cancel: function () {

            }
        }
    });

    /*if (confirm("Are you sure you want to remove this?")) {
        showCustomLoader();
        var obj = {};
        obj[key] = value;
        $.ajax({
            type: "POST",
            url: base_url + url,
            data: obj,
            dataType: "json",
            success: function (result) {
                hideCustomLoader();
                if (typeof result.status !== 'undefined' && result.status == true) {
                    $("#" + key + value).remove();
                }
                showMessage(result.message);
                if (typeof result.reload !== 'undefined' && result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            }
        });
    }*/
}

function proceedToCheckout() {
    showCustomLoader();
    $.ajax({
        type: "GET",
        url: base_url + 'account/checkIfUserLoggedin',
        success: function (result) {
            hideCustomLoader();
            if (result == 0) {
                // not logged in
                showMessage("You need to be logged in to proceed to checkout", 'danger');
                $(".login_li").addClass("open");
                $(".open_login_dd").attr("aria-expanded", "true");
            } else {
                // logged in
                window.location.href = base_url + 'address';
            }
        }
    });
}

function placeOrder() {
    if ($('.acceptTerms').prop("checked") == true) {
        showCustomLoader();
        var store_id = $("#StoreID").val();
        if(store_id == ''){
            hideCustomLoader();
            showMessage('Please select brach from map', 'danger');
            return false;
        }
        //var district_id = $("#BranchDeliveryDistrictID option:selected").val();
        $.ajax({
            type: "GET",
            url: base_url + 'order/placeOrder?StoreID=' + store_id,
            dataType: "json",
            success: function (result) {



                console.log(result);
                hideCustomLoader();

                if(result.show_model == true){
                    $('#chkContShippingBody').html(result.model_html);
                    $('#chkContShipping').modal('show');

                }else{
                    showMessage(result.message);
                    if (result.redirect == true) {
                        setTimeout(function () {
                            window.location.href = base_url + result.url;
                        }, 1500);
                    }

                }



                
            }
        });
    } else {
        showMessage('You must accept terms and conditions to proceed with checkout.', 'danger');
    }
}

function cancelOrder(OrderID) {
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure you want to cancel this order?',
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirm: function () {
                showCustomLoader();
                $.ajax({
                    type: "POST",
                    url: base_url + 'order/cancelOrder',
                    dataType: "json",
                    data: {'OrderID': OrderID},
                    success: function (result) {
                        console.log(result);
                        hideCustomLoader();
                        showMessage(result.message);
                        if (result.redirect == true) {
                            setTimeout(function () {
                                window.location.href = base_url + result.url;
                            }, 1500);
                        }
                    }
                });
            },
            cancel: function () {

            }
        }
    });


    /*if (confirm("Are you sure you want to cancel this order?")) {
        showCustomLoader();
        $.ajax({
            type: "POST",
            url: base_url + 'order/cancelOrder',
            dataType: "json",
            data: {'OrderID': OrderID},
            success: function (result) {
                console.log(result);
                hideCustomLoader();
                showMessage(result.message);
                if (result.redirect == true) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1500);
                }
            }
        });
    }*/
}

function getProductsList(CategoryID, page,sub_category) {
      
      
        var sorting = $('#CreatedAt').val();
        var limit = $('#Limit').val();

        

        var attribute ="";
        $('.attribute-fill:checkbox:checked').each(function() {
        attribute += $(this).val()+',';
        });

        //SubCategory it is 0 or 1 we are getting value against CategoryID
            showCustomLoader();
            $.ajax({
                type: "GET",
                url: base_url + 'product/product_listing?attribute='+attribute+'&SubCategory='+sub_category+'&CategoryID='+ CategoryID + '&page=' + page + '&sorting=' + sorting + '&limit=' + limit,
                dataType: "json",
                success: function (result) {
                    hideCustomLoader();
                    $("#product-listing").html(result.html);
                    $("#product-paggination").html(result.paggination);
                    //alert(result.total_product);
                    $("#total-count").html(result.total_product);
                    
                }
            });
           
}

function getDistrictsForCity(CityID, language) {
    showCustomLoader();
    $.ajax({
        type: "GET",
        url: base_url + 'cms/district/getDistrictsForCity?CityID=' + CityID + '&Language=' + language,
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            $(".DistrictDD").html(result.html);
        }
    });
}

$(".register_now").click(function () {
    if ($(".login_li").hasClass("open")) {
        $(".login_li").removeClass("open");
        $(".open_login_dd").attr("aria-expanded", "false");
    }
    if (!$(".register_li").hasClass("open")) {
        $(".register_li").addClass("open");
        $(".open_register_dd").attr("aria-expanded", "true");
    }
});

$('.chk').on('click', function () {
    $('.chk').prop('checked', false);
    $('.collectFromStore').prop('checked', false);
    $(this).prop('checked', true);
    markAddressAsDefault($(this).val());
    eraseCookie('CollectFromStore');
});

function markAddressAsDefault(address_id) {
    showCustomLoader();
    $.ajax({
        type: "GET",
        url: base_url + 'address/markAddressAsDefault?AddressID=' + address_id,
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            showMessage(result.message);
        }
    });
}

$(document).ready(function () {
    $('.collectFromStore').click(function () {
        showCustomLoader();
        if ($(this).prop("checked") == true) {
            $('.chk').prop('checked', false);
            $('.CollectPaymentChk').prop('checked', false);
            $(this).prop('checked', true);
            setCookie('CollectFromStore', 1, 1);

            $.ajax({
                type: "POST",
                url: base_url + 'checkout/unsetShipmentMethod',
                dataType: "json",
                success: function (result) {
                    //hideCustomLoader();
                    //showMessage(result.message);
                   
                }
            });
            hideCustomLoader();
            showMessage("Order set to be collected from store");
            setTimeout(function () {
                        window.location.reload();
                    }, 1000);
        } else if ($(this).prop("checked") == false) {
            eraseCookie('CollectFromStore');
            window.location.reload();
        }
    });
});

function setCookie(name, value, days) {

    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function eraseCookie(name) {

    document.cookie = name + '=; Max-Age=-99999999;path=/';
}

function addToWishlist(ID, Type) {
    showCustomLoader();
    $.ajax({
        type: "GET",
        url: base_url + 'cart/addToWishlist?ID=' + ID + '&Type=' + Type,
        dataType: "json",
        success: function (result) {
            $("#item" + ID).removeClass("p_liked");
            $("#item" + ID).removeClass("p_unliked");
            $("#item" + ID).addClass(result.class);
            hideCustomLoader();
            showMessage(result.message);
            if (result.class == "p_unliked" || true) {
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        }
    });
}

function createTicket(OrderID) {
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'ticket/createTicket',
        data: {'OrderID': OrderID},
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            if (result.status == true) // if ticket created successfully
            {
                $("#Order" + OrderID + "HasTicket1").show();
                $("#Order" + OrderID + "HasTicket0").hide();
                $("#ComplaintDetail" + OrderID).html(result.ComplaintDetail);
                $("#Order" + OrderID + "HasTicket1").children('.showMessagesForTicket').data('ticket_id', result.TicketID);
                showCustomLoader();
                $.ajax({
                    type: "POST",
                    url: base_url + 'ticket/getMessagesForTicket',
                    data: {'TicketID': result.TicketID},
                    dataType: "json",
                    success: function (result_messages) {
                        hideCustomLoader();
                        $('#OrderNumber').html(result_messages.OrderNumber);
                        $('#TotalAmount').html(result_messages.TotalAmount);
                        $('.msgbox').html(result_messages.html);
                        $('.msgbox').addClass("TicketID" + result.TicketID);
                        $('#CMessage').modal('show');
                        $('#TicketIDForMessage').val(result.TicketID);
                    }
                });
            } else {
                showMessage(result.message, 'danger');
            }
        }
    });
}


$(document).on('click', '.showMessagesForTicket', function () {
    var TicketID = $(this).data("ticket_id");
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'ticket/getMessagesForTicket',
        data: {'TicketID': TicketID},
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            $('#OrderNumber').html(result.OrderNumber);
            $('#TotalAmount').html(result.TotalAmount);
            $('.msgbox').html(result.html);
            $('.msgbox').addClass("TicketID" + TicketID);
            $('#CMessage').modal('show');
            if (result.IsClosed == 0) {
                $('.ticket_form_btn').prop('disabled', false);
                $('#TicketStatus').html('Ongoing');
            } else if (result.IsClosed == 1) {
                $('.ticket_form_btn').prop('disabled', true);
                $('#TicketStatus').html('Closed');
            } else if (result.IsClosed == 2) {
                $('.ticket_form_btn').prop('disabled', false);
                $('#TicketStatus').html('Re-Opened');
            }
            $('#TicketIDForMessage').val(TicketID);
            setTimeout(function () {
                $('.msgbox').animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
            }, 1000);
        }
    });
});

$(".ticketMessage").submit(function (e) {
    e.preventDefault();
    var form_id = $(this).attr('id');
    var formValidated = validateForm(form_id);
    if (formValidated) {
        showCustomLoader();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                hideCustomLoader();
                console.log(result.message);
                $('.msgbox').html(result.html);
                $('.msgbox').addClass("TicketID" + result.TicketID);
                $form[0].reset();
                $('.msgbox').animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
            }
        });
    }
});

function removeMessageBoxClass() {
    var TicketID = $('#TicketIDForMessage').val();
    $('.msgbox').removeClass("TicketID" + TicketID);
}

$(".couponApplyForm").submit(function (e) {
    e.preventDefault();
    var form_id = $(this).attr('id');
    var formValidated = validateForm(form_id);
    if (formValidated) {
        showCustomLoader();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                hideCustomLoader();
                
                if (result.status == true) {
                    showMessage(result.message);
                    $('.couponApplyForm').hide();
                    $form[0].reset();
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }else{
                    showMessage(result.message,'danger');
                }
            }
        });
    }
});

function removeCoupon() {
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to clear this coupon?',
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirm: function () {
                showCustomLoader();
                $.ajax({
                    type: "GET",
                    url: base_url + 'cart/removeCoupon',
                    dataType: "json",
                    success: function (result) {
                        hideCustomLoader();
                        showMessage(result.message);
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                });
            },
            cancel: function () {

            }
        }
    });

    /*if (confirm("Are you sure to clear this coupon?")) {
        showCustomLoader();
        $.ajax({
            type: "GET",
            url: base_url + 'cart/removeCoupon',
            dataType: "json",
            success: function (result) {
                hideCustomLoader();
                showMessage(result.message);
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            }
        });
    }*/
}

function addWishlistToCart(ProductID, ItemType, TempItemPrice, IsCorporateItem) {
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'cart/addToCart',
        data: {
            'ProductID': ProductID,
            'Quantity': 1,
            'ItemType': ItemType,
            'TempItemPrice': TempItemPrice,
            'IsCorporateItem': IsCorporateItem
        },
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            if (result.status == true) {
                $('.CartItemsCount').html(result.total_cart_items);
                showAddedToCart(result.message);
            } else {
                showMessage(result.message);
            }
        }
    });
}

$(document).ready(function () {
    $('.required').tooltip({
        title: 'This field is required',
        animation: "fade",
        delay: "200",
        placement: "top"
    });
});

function contactUsSubject(type, form_id) {
    if (type == 'Feedback') {
        $('.FeedbackField').show();
        $('.FeedbackField > :input').addClass('required');
        $('.CareerField').hide();
        $('.CareerField > :input').removeClass('required');
        $('.CareerField > :input').val('');
    } else if (type == 'Career') {
        $('.CareerField').show();
        $('.CareerField > :input').addClass('required');
        $('.FeedbackField').hide();
        $('.FeedbackField > :input').removeClass('required');
        $('.FeedbackField > :input').val('');
    }
    $('form#' + form_id + ' .required').siblings('label').removeClass('error');
    $('form#' + form_id + ' .required').removeClass('error-border');
}

function notifyMessage(message, type) {
    $.notify({
        // options
        message: message
    }, {
        // settings
        type: type,
        placement: {
            from: "top",
            align: align_notify_message
        },
        delay: 7000,
        mouse_over: "pause",
        offset: 20,
        spacing: 10,
        z_index: 1031,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    });
}

function changeShipmentMethod(ShipmentMethodID) {
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'checkout/changeShipmentMethod',
        data: {'ShipmentMethodID': ShipmentMethodID},
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            showMessage(result.message);
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        }
    });
}

function changePaymentMethod(PaymentMethod) {
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'checkout/changePaymentMethod',
        data: {'PaymentMethod': PaymentMethod},
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            // showMessage(result.message);
        }
    });
}

function changeDeliveryStoreID(StoreID,StoreTitle) {
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'checkout/changeDeliveryStoreID',
        data: {'StoreID': StoreID,'StoreTitle' : StoreTitle},
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            // showMessage(result.message);
        }
    });
}

function unsetDeliveryStoreID() {
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'checkout/unsetDeliveryStoreID',
        data: {},
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            // showMessage(result.message);
        }
    });
}

$('.CollectPaymentChk').on('click', function () {
    $('.CollectPaymentChk').prop('checked', false);
    $('.collectFromStore').prop('checked', false);
    $(this).prop('checked', true);
    setAddressForPaymentCollection($(this).val());
    eraseCookie('CollectFromStore');
});

function setAddressForPaymentCollection(address_id) {
    showCustomLoader();
    $.ajax({
        type: "GET",
        url: base_url + 'address/setAddressForPaymentCollection?AddressID=' + address_id,
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            showMessage(result.message);
        }
    });
}

$(document).on('click', '#openOfferModal', function () {
    var offer_id = $(this).data('offer_id');
    var offer_notification_id = $(this).data('offer_notification_id');
    var offer_title = $(this).data('offer_title');
    var offer_description = $(this).data('offer_description');
    showCustomLoader();
    $.ajax({
        type: "GET",
        url: base_url + 'page/markOfferAsRead?OfferID=' + offer_id,
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            $('#OfferTitleModal').html(offer_title);
            $('#OfferDescriptionModal').html(offer_description);
            $('#OfferModal').modal('show');
        }
    });
});

function addChocoBoxToCart() {
    var ProductIDs = $('.ProductIDs').val();
    var Quantity = $('.Quantity').val();
    var ItemType = $('.ItemType').val();
    var TempItemPrice = $('.ProductPrice').val();
    var CustomizedBoxID = $('.BoxID').val();
    var RibbonColor = $('.RibbonColor').val();
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'cart/addChocoBoxToCart',
        data: {
            'ProductIDs': ProductIDs,
            'Quantity': Quantity,
            'ItemType': ItemType,
            'TempItemPrice': TempItemPrice,
            'CustomizedBoxID': CustomizedBoxID,
            'RibbonColor': RibbonColor
        },
        dataType: "json",
        success: function (result) {
            hideCustomLoader();
            if (result.status == true) {
                $('.CartItemsCount').html(result.total_cart_items);
                showAddedToCart(result.message);
            } else {
                showMessage(result.message, 'danger');
            }
        }
    });
}

$("#addCustomizedShapeToCart").submit(function (e) {
    e.preventDefault();
    showCustomLoader();
    $form = $(this);
    $.ajax({
        type: "POST",
        url: $form.attr('action'),
        data: new FormData(this),
        dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            hideCustomLoader();
            if (result.status == true) {
                $('.CartItemsCount').html(result.total_cart_items);
                showAddedToCart(result.message);
            } else {
                showMessage(result.message, 'danger');
            }
        }
    });
});

$(document).on('click', '.chocobox_detail', function () {
    var pids = $(this).data('pids');
    var box_type = $(this).data('box_type');
    var box_id = $(this).data('box_id');
    var ribbon = $(this).data('ribbon');
    showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'customize/getChocoboxDetail',
        data: {'ProductIDs': pids, 'box_type': box_type, 'box_id': box_id, 'ribbon': ribbon},
        success: function (result) {
            hideCustomLoader();
            $('#ChocoboxDetailModalDescription').html(result);
            $('#ChocoboxDetailModal').modal('show');
        }
    });
});

function openCustomizeBoxPopup(type) {
    window.location.href = base_url + 'customize/boxes/' + type
    /*showCustomLoader();
    $.ajax({
        type: "POST",
        url: base_url + 'customize/getChocoboxes',
        data: {'type': type},
        success: function (result) {
            hideCustomLoader();
            $('#ChocoboxDetailModalDescription').html(result);
            $('#ChocoboxDetailModal').modal('show');
        }
    });*/
}

function showTerms() {
    $('#TermsUpdatedModal').modal('hide');
    showCustomLoader();
    $.ajax({
        type: "GET",
        url: base_url + 'account/updateTermsAccepted',
        success: function (result) {
            hideCustomLoader();
            $('#TermsConditions').modal('show');
        }
    });
}