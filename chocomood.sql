-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 05, 2020 at 04:49 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `chocomood`
--

-- --------------------------------------------------------

--
-- Table structure for table `boxes`
--

CREATE TABLE `boxes` (
  `BoxID` int(11) NOT NULL,
  `BoxPrice` decimal(10,2) NOT NULL,
  `BoxSpace` int(11) NOT NULL,
  `BoxImage` text NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boxes`
--

INSERT INTO `boxes` (`BoxID`, `BoxPrice`, `BoxSpace`, `BoxImage`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, '30.00', 18, 'uploads/images/2682775138520190410025019box-1.png', 0, 0, 1, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1),
(2, '20.00', 10, 'uploads/images/2682775138520190410025019box-1.png', 0, 0, 1, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `boxes_text`
--

CREATE TABLE `boxes_text` (
  `BoxTextID` int(11) NOT NULL,
  `BoxID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boxes_text`
--

INSERT INTO `boxes_text` (`BoxTextID`, `BoxID`, `Title`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, '30 Chocolate PCS Box', '30 Chocolate PCS Box', 1, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1),
(2, 1, '30 Chocolate PCS Box', '<p>arabic</p>', 2, '2019-04-10 14:19:50', '2019-04-11 15:29:50', 1, 1),
(3, 2, '20 Chocolate PCS Box', '', 1, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1),
(4, 2, '20 Chocolate PCS Box', '', 2, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CategoryID` int(11) NOT NULL,
  `ParentID` int(11) NOT NULL,
  `CategoryPrice` float NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CategoryID`, `ParentID`, `CategoryPrice`, `SortOrder`, `Image`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(32, 31, 0, 1, '', 0, 1, '2019-11-27 11:05:54', '2019-11-27 11:05:54', 1, 1),
(34, 0, 0, 2, '', 0, 1, '2019-11-30 11:15:49', '2019-11-30 11:18:17', 1, 1),
(35, 0, 0, 3, '', 0, 1, '2019-11-30 11:16:21', '2019-11-30 11:16:21', 1, 1),
(36, 0, 0, 4, '', 0, 1, '2019-11-30 11:16:38', '2019-11-30 11:16:38', 1, 1),
(37, 0, 0, 5, '', 0, 1, '2019-11-30 11:17:06', '2019-11-30 11:17:06', 1, 1),
(38, 0, 0, 6, '', 0, 1, '2019-11-30 11:18:33', '2019-11-30 11:18:33', 1, 1),
(39, 0, 0, 7, '', 0, 1, '2019-11-30 11:18:56', '2019-11-30 11:19:09', 1, 1),
(40, 34, 0, 8, '', 0, 1, '2019-11-30 11:20:07', '2019-11-30 11:20:07', 1, 1),
(41, 34, 0, 9, '', 0, 1, '2019-11-30 11:21:07', '2019-11-30 11:21:07', 1, 1),
(42, 34, 0, 10, '', 0, 1, '2019-11-30 11:28:40', '2019-11-30 11:28:40', 1, 1),
(43, 34, 0, 11, '', 0, 1, '2019-11-30 11:29:22', '2019-11-30 11:29:22', 1, 1),
(44, 34, 0, 12, '', 0, 1, '2019-11-30 11:29:52', '2019-11-30 11:29:52', 1, 1),
(45, 35, 0, 13, '', 0, 1, '2019-11-30 11:30:10', '2019-11-30 11:30:10', 1, 1),
(46, 35, 0, 14, '', 0, 1, '2019-11-30 11:30:33', '2019-11-30 11:30:33', 1, 1),
(47, 35, 0, 15, '', 0, 1, '2019-11-30 11:30:48', '2019-11-30 11:30:48', 1, 1),
(48, 35, 0, 16, '', 0, 1, '2019-11-30 11:31:07', '2019-11-30 11:31:07', 1, 1),
(49, 36, 0, 17, '', 0, 1, '2019-11-30 11:31:30', '2019-11-30 11:31:30', 1, 1),
(50, 36, 0, 18, '', 0, 1, '2019-11-30 11:31:53', '2019-11-30 11:31:53', 1, 1),
(51, 37, 0, 19, '', 0, 1, '2019-11-30 11:33:10', '2019-11-30 11:33:10', 1, 1),
(52, 37, 0, 20, '', 0, 1, '2019-11-30 11:33:40', '2019-11-30 11:33:40', 1, 1),
(53, 39, 0, 21, '', 0, 1, '2019-11-30 11:34:09', '2019-11-30 11:34:09', 1, 1),
(54, 39, 0, 22, '', 0, 1, '2019-11-30 11:34:28', '2019-11-30 11:34:28', 1, 1),
(55, 39, 0, 23, '', 0, 1, '2019-11-30 11:34:53', '2019-11-30 11:34:53', 1, 1),
(56, 38, 0, 24, '', 0, 1, '2019-11-30 11:35:15', '2019-11-30 11:35:15', 1, 1),
(57, 37, 0, 25, '', 0, 1, '2019-11-30 11:40:04', '2019-11-30 11:40:04', 1, 1),
(58, 38, 0, 26, '', 0, 1, '2019-11-30 11:41:57', '2019-11-30 11:41:57', 1, 1),
(59, 38, 0, 27, '', 0, 1, '2019-11-30 11:42:37', '2019-11-30 11:42:37', 1, 1),
(60, 37, 0, 28, '', 0, 1, '2019-11-30 11:43:12', '2019-11-30 11:43:12', 1, 1),
(64, 34, 0, 29, '', 0, 1, '2019-12-07 16:55:38', '2019-12-07 16:55:38', 1, 1),
(65, 0, 0, 30, 'uploads/images/993692098752020021011112572124360_2558790247717603_7384555194358431744_n.jpg', 0, 1, '2020-02-10 11:25:11', '2020-02-10 11:25:11', 1, 1),
(66, 34, 0, 31, 'uploads/images/169588961192020021011272672124360_2558790247717603_7384555194358431744_n.jpg', 0, 1, '2020-02-10 11:26:27', '2020-02-10 11:26:27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories_text`
--

CREATE TABLE `categories_text` (
  `CategoryTextID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories_text`
--

INSERT INTO `categories_text` (`CategoryTextID`, `CategoryID`, `Title`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(61, 32, 'Caramel ', '', 1, '2019-11-27 11:05:54', '2019-11-27 11:05:54', 1, 1),
(62, 32, 'Caramel ', '', 2, '2019-11-27 11:05:54', '2019-11-27 11:05:54', 1, 1),
(65, 34, 'Pralines', '', 1, '2019-11-30 11:15:49', '2019-11-30 11:18:17', 1, 1),
(66, 34, 'Praline', '', 2, '2019-11-30 11:15:49', '2019-11-30 11:15:49', 1, 1),
(67, 35, 'Ocassion', '', 1, '2019-11-30 11:16:21', '2019-11-30 11:16:21', 1, 1),
(68, 35, 'Ocassion', '', 2, '2019-11-30 11:16:21', '2019-11-30 11:16:21', 1, 1),
(69, 36, 'Boxes', '', 1, '2019-11-30 11:16:38', '2019-11-30 11:16:38', 1, 1),
(70, 36, 'Boxes', '', 2, '2019-11-30 11:16:38', '2019-11-30 11:16:38', 1, 1),
(71, 37, 'Confectionery', '', 1, '2019-11-30 11:17:06', '2019-11-30 11:17:06', 1, 1),
(72, 37, 'Confectionery', '', 2, '2019-11-30 11:17:06', '2019-11-30 11:17:06', 1, 1),
(73, 38, 'Pastry', '', 1, '2019-11-30 11:18:33', '2019-11-30 11:18:33', 1, 1),
(74, 38, 'Pastry', '', 2, '2019-11-30 11:18:33', '2019-11-30 11:18:33', 1, 1),
(75, 39, 'Variety', '', 1, '2019-11-30 11:18:56', '2019-11-30 11:19:09', 1, 1),
(76, 39, 'variety', '', 2, '2019-11-30 11:18:56', '2019-11-30 11:18:56', 1, 1),
(77, 40, 'Elegant', '', 1, '2019-11-30 11:20:07', '2019-11-30 11:20:07', 1, 1),
(78, 40, 'Elegant', '', 2, '2019-11-30 11:20:07', '2019-11-30 11:20:07', 1, 1),
(79, 41, 'Style', '', 1, '2019-11-30 11:21:07', '2019-11-30 11:21:07', 1, 1),
(80, 41, 'Style', '', 2, '2019-11-30 11:21:07', '2019-11-30 11:21:07', 1, 1),
(81, 42, 'Carmola', '', 1, '2019-11-30 11:28:40', '2019-11-30 11:28:40', 1, 1),
(82, 42, 'Carmola', '', 2, '2019-11-30 11:28:40', '2019-11-30 11:28:40', 1, 1),
(83, 43, 'Prestige', '', 1, '2019-11-30 11:29:22', '2019-11-30 11:29:22', 1, 1),
(84, 43, 'Prestige', '', 2, '2019-11-30 11:29:22', '2019-11-30 11:29:22', 1, 1),
(85, 44, 'Sugar Free', '', 1, '2019-11-30 11:29:52', '2019-11-30 11:29:52', 1, 1),
(86, 44, 'Sugar Free', '', 2, '2019-11-30 11:29:52', '2019-11-30 11:29:52', 1, 1),
(87, 45, 'Baby', '', 1, '2019-11-30 11:30:10', '2019-11-30 11:30:10', 1, 1),
(88, 45, 'Baby', '', 2, '2019-11-30 11:30:10', '2019-11-30 11:30:10', 1, 1),
(89, 46, 'Wedding', '', 1, '2019-11-30 11:30:33', '2019-11-30 11:30:33', 1, 1),
(90, 46, 'Wedding', '', 2, '2019-11-30 11:30:33', '2019-11-30 11:30:33', 1, 1),
(91, 47, 'Graduation', '', 1, '2019-11-30 11:30:48', '2019-11-30 11:30:48', 1, 1),
(92, 47, 'Graduation', '', 2, '2019-11-30 11:30:48', '2019-11-30 11:30:48', 1, 1),
(93, 48, 'Special Day', '', 1, '2019-11-30 11:31:07', '2019-11-30 11:31:07', 1, 1),
(94, 48, 'Special Day', '', 2, '2019-11-30 11:31:07', '2019-11-30 11:31:07', 1, 1),
(95, 49, 'Luxury', '', 1, '2019-11-30 11:31:30', '2019-11-30 11:31:30', 1, 1),
(96, 49, 'Luxury', '', 2, '2019-11-30 11:31:30', '2019-11-30 11:31:30', 1, 1),
(97, 50, 'Everyday', '', 1, '2019-11-30 11:31:53', '2019-11-30 11:31:53', 1, 1),
(98, 50, 'Everyday', '', 2, '2019-11-30 11:31:53', '2019-11-30 11:31:53', 1, 1),
(99, 51, 'Halkoum and Nougat ', '', 1, '2019-11-30 11:33:10', '2019-11-30 11:33:10', 1, 1),
(100, 51, 'Halkoum and Nougat ', '', 2, '2019-11-30 11:33:10', '2019-11-30 11:33:10', 1, 1),
(101, 52, 'Candy', '', 1, '2019-11-30 11:33:40', '2019-11-30 11:33:40', 1, 1),
(102, 52, 'Candy', '', 2, '2019-11-30 11:33:40', '2019-11-30 11:33:40', 1, 1),
(103, 53, 'Lollipop', '', 1, '2019-11-30 11:34:09', '2019-11-30 11:34:09', 1, 1),
(104, 53, 'Lollipop', '', 2, '2019-11-30 11:34:09', '2019-11-30 11:34:09', 1, 1),
(105, 54, 'Bars', '', 1, '2019-11-30 11:34:28', '2019-11-30 11:34:28', 1, 1),
(106, 54, 'Bars', '', 2, '2019-11-30 11:34:28', '2019-11-30 11:34:28', 1, 1),
(107, 55, 'Fancy Shape', '', 1, '2019-11-30 11:34:53', '2019-11-30 11:34:53', 1, 1),
(108, 55, 'Fancy Shape', '', 2, '2019-11-30 11:34:53', '2019-11-30 11:34:53', 1, 1),
(109, 56, 'Salty Biscuit', '', 1, '2019-11-30 11:35:15', '2019-11-30 11:35:15', 1, 1),
(110, 56, 'Salty Biscuit', '', 2, '2019-11-30 11:35:15', '2019-11-30 11:35:15', 1, 1),
(111, 57, 'Dates', '', 1, '2019-11-30 11:40:04', '2019-11-30 11:40:04', 1, 1),
(112, 57, 'Dates', '', 2, '2019-11-30 11:40:04', '2019-11-30 11:40:04', 1, 1),
(113, 58, 'Petit Four', '', 1, '2019-11-30 11:41:57', '2019-11-30 11:41:57', 1, 1),
(114, 58, 'Petit Four', '', 2, '2019-11-30 11:41:57', '2019-11-30 11:41:57', 1, 1),
(115, 59, 'Canapé', '', 1, '2019-11-30 11:42:37', '2019-11-30 11:42:37', 1, 1),
(116, 59, 'Canapé', '', 2, '2019-11-30 11:42:37', '2019-11-30 11:42:37', 1, 1),
(117, 60, 'Dragée', '', 1, '2019-11-30 11:43:12', '2019-11-30 11:43:12', 1, 1),
(118, 60, 'Dragée', '', 2, '2019-11-30 11:43:12', '2019-11-30 11:43:12', 1, 1),
(125, 64, 'Ruby', '', 1, '2019-12-07 16:55:38', '2019-12-07 16:55:38', 1, 1),
(126, 64, 'Ruby', '', 2, '2019-12-07 16:55:38', '2019-12-07 16:55:38', 1, 1),
(127, 65, 'test', '', 1, '2020-02-10 11:25:11', '2020-02-10 11:25:11', 1, 1),
(128, 65, 'test', '', 2, '2020-02-10 11:25:11', '2020-02-10 11:25:11', 1, 1),
(129, 66, 'dfsdfds', 'this is tesitng', 1, '2020-02-10 11:26:27', '2020-02-10 11:26:27', 1, 1),
(130, 66, 'dfsdfds', 'this is tesitng', 2, '2020-02-10 11:26:27', '2020-02-10 11:26:27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `CityID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`CityID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(77, 76, 0, 1, '2018-11-29 10:14:37', '2018-11-29 10:14:37', 1, 1),
(113, 110, 0, 1, '2019-05-04 12:50:41', '2019-05-04 12:50:41', 1, 1),
(114, 111, 0, 1, '2019-11-27 11:32:53', '2019-11-27 11:32:53', 1, 1),
(115, 112, 0, 1, '2019-11-27 11:35:43', '2019-11-27 11:35:43', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities_text`
--

CREATE TABLE `cities_text` (
  `CityTextID` int(11) NOT NULL,
  `CityID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities_text`
--

INSERT INTO `cities_text` (`CityTextID`, `CityID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(84, 77, 'Riyadh', 1, '2018-11-29 10:14:37', '2018-11-29 10:14:37', 1, 1),
(196, 77, 'Riyadh', 2, '2018-11-29 10:14:37', '2018-11-29 10:14:37', 1, 1),
(234, 113, 'Jeddah', 1, '2019-05-04 12:50:41', '2019-05-04 12:50:41', 1, 1),
(235, 113, 'Jeddah', 2, '2019-05-04 12:50:41', '2019-05-04 12:50:41', 1, 1),
(236, 114, 'Makkah', 1, '2019-11-27 11:32:53', '2019-11-27 11:32:53', 1, 1),
(237, 114, 'Makkah', 2, '2019-11-27 11:32:53', '2019-11-27 11:32:53', 1, 1),
(238, 115, 'Madinah', 1, '2019-11-27 11:35:43', '2019-11-27 11:35:43', 1, 1),
(239, 115, 'Madinah', 2, '2019-11-27 11:35:43', '2019-11-27 11:35:43', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `CollectionID` int(11) NOT NULL,
  `ProductID` varchar(255) NOT NULL,
  `IsFeatured` tinyint(4) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `HomeImage` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`CollectionID`, `ProductID`, `IsFeatured`, `SortOrder`, `Hide`, `IsActive`, `HomeImage`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(22, '35', 1, 3, 0, 1, 'uploads/images/9035747175520191202111832y.jpg', '2019-12-02 11:32:18', '2019-12-02 11:32:51', 1, 1),
(19, '34,35', 1, 0, 0, 1, 'uploads/images/607892687982019121511015001_choco.jpg', '2019-11-30 12:32:23', '2019-12-15 11:50:01', 1, 1),
(24, '34', 0, 4, 0, 1, 'uploads/images/612656759172019121511224801_choco.jpg', '2019-12-15 11:48:22', '2019-12-15 11:48:22', 1, 1),
(21, '34', 1, 2, 0, 1, 'uploads/images/4995846680620191202114531h.jpg', '2019-12-02 11:31:45', '2019-12-02 11:31:45', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `collections_text`
--

CREATE TABLE `collections_text` (
  `CollectionTextID` int(11) NOT NULL,
  `CollectionID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `Ingredients` varchar(2000) NOT NULL,
  `Specifications` text NOT NULL,
  `Tags` varchar(500) NOT NULL,
  `Keywords` varchar(500) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `MetaTags` varchar(255) NOT NULL,
  `MetaKeywords` varchar(255) NOT NULL,
  `MetaDescription` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `collections_text`
--

INSERT INTO `collections_text` (`CollectionTextID`, `CollectionID`, `Title`, `Description`, `Ingredients`, `Specifications`, `Tags`, `Keywords`, `SystemLanguageID`, `MetaTags`, `MetaKeywords`, `MetaDescription`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(24, 19, 'Ramadhan Collection', 'xhshcas', 'dchshdf', '', 'Ramadhan', '', 2, '', '', '', '2019-11-30 12:32:23', '2019-11-30 12:32:23', 1, 1),
(33, 24, 'Wedding', 'non', 'non', '', '', 'chocolate', 1, 'choco collection', 'jeddah', '', '2019-12-15 11:48:22', '2019-12-15 11:48:22', 1, 1),
(27, 21, 'collection3', '', '', '', '', '', 1, '', '', '', '2019-12-02 11:31:45', '2019-12-02 11:31:45', 1, 1),
(28, 21, 'collection3', '', '', '', '', '', 2, '', '', '', '2019-12-02 11:31:45', '2019-12-02 11:31:45', 1, 1),
(29, 22, '4', 'hhh', 'hh', '<p>hh</p>', '', '', 1, '', '', '', '2019-12-02 11:32:18', '2019-12-02 11:32:51', 1, 1),
(30, 22, '4', 'hhh', 'hh', '<p>hh</p>', '', '', 2, '', '', '', '2019-12-02 11:32:18', '2019-12-02 11:32:18', 1, 1),
(34, 24, 'Wedding', 'non', 'non', '', '', 'chocolate', 2, 'choco collection', 'jeddah', '', '2019-12-15 11:48:22', '2019-12-15 11:48:22', 1, 1),
(23, 19, 'Ramadhan Collection', 'xhshcas', 'dchshdf', '', 'Ramadhan', '', 1, '', '', '', '2019-11-30 12:32:23', '2019-12-15 11:50:01', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `collection_ratings`
--

CREATE TABLE `collection_ratings` (
  `CollectionRatingID` int(11) NOT NULL,
  `CollectionID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Rating` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collection_ratings`
--

INSERT INTO `collection_ratings` (`CollectionRatingID`, `CollectionID`, `UserID`, `Rating`) VALUES
(1, 2, 54, 4);

-- --------------------------------------------------------

--
-- Table structure for table `collection_reviews`
--

CREATE TABLE `collection_reviews` (
  `CollectionReviewID` int(11) NOT NULL,
  `CollectionID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Comment` text CHARACTER SET utf8 NOT NULL,
  `CreatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_requests`
--

CREATE TABLE `contact_requests` (
  `ContactRequestID` int(11) NOT NULL,
  `FullName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `Company` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Department` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Message` text CHARACTER SET utf8 NOT NULL,
  `CV` varchar(255) NOT NULL,
  `CreatedAt` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_requests`
--

INSERT INTO `contact_requests` (`ContactRequestID`, `FullName`, `Email`, `Subject`, `Company`, `Department`, `Message`, `CV`, `CreatedAt`) VALUES
(1, 'Dev Testing', 'dev@gmail.com', 'Feedback', 'Schopfen', 'IT', 'Hi this is a testing message.', '', '1549026151'),
(6, 'asdasd', 'Test@test.com', 'Career', '', '', '', 'uploads/582247656820190411044456dummy-pdf_2.pdf', '1554991004');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `CountryID` int(11) NOT NULL,
  `CountryCode` varchar(50) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`CountryID`, `CountryCode`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'PK', 0, 0, 1, '2018-10-03 22:17:19', '2018-10-03 22:17:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries_text`
--

CREATE TABLE `countries_text` (
  `CountryTextID` int(11) NOT NULL,
  `CountryID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries_text`
--

INSERT INTO `countries_text` (`CountryTextID`, `CountryID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Pakistan', 1, '2018-10-03 22:17:19', '2018-10-03 22:17:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `CouponID` int(11) NOT NULL,
  `CouponCode` varchar(255) NOT NULL,
  `UsageCount` int(11) NOT NULL,
  `DiscountPercentage` decimal(10,2) NOT NULL,
  `ExpiryDate` date NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`CouponID`, `CouponCode`, `UsageCount`, `DiscountPercentage`, `ExpiryDate`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 'BLK001', 99, '20.00', '2020-11-26', 0, 0, 1, '2019-11-27 11:18:48', '2019-11-27 11:19:16', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupons_text`
--

CREATE TABLE `coupons_text` (
  `CouponTextID` int(11) NOT NULL,
  `CouponID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupons_text`
--

INSERT INTO `coupons_text` (`CouponTextID`, `CouponID`, `Title`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 3, 'Black Friday', 'This offer will start date 20 for total of 100 coupons and with total discount amount of 20000 Saudi Rial offer ends based on what ends first total count of 100 or exp. day pass', 1, '2019-11-27 11:18:48', '2019-11-27 11:19:16', 1, 1),
(4, 3, 'الجمعه السوداء', 'This offer will start date 20 for total of 100 coupons and with total discount amount of 20000 Saudi Rial offer ends based on what ends first total count of 100 or exp. day pass', 2, '2019-11-27 11:18:48', '2019-11-27 11:19:36', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer_groups`
--

CREATE TABLE `customer_groups` (
  `CustomerGroupID` int(11) NOT NULL,
  `CustomerGroupTitle` varchar(255) NOT NULL,
  `CustomerGroupType` enum('Orders','Purchases') NOT NULL DEFAULT 'Orders',
  `MinimumValue` varchar(255) NOT NULL,
  `MaximumValue` varchar(255) NOT NULL,
  `FromDate` date NOT NULL,
  `ToDate` date NOT NULL,
  `GroupMembersCount` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_groups`
--

INSERT INTO `customer_groups` (`CustomerGroupID`, `CustomerGroupTitle`, `CustomerGroupType`, `MinimumValue`, `MaximumValue`, `FromDate`, `ToDate`, `GroupMembersCount`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 'New ', 'Purchases', '50', '999', '2019-11-01', '2019-12-08', 1, 0, 0, 0, '2019-12-08 13:06:19', '2019-12-08 13:06:19', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_group_members`
--

CREATE TABLE `customer_group_members` (
  `CustomerGroupMemberID` int(11) NOT NULL,
  `CustomerGroupID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_group_members`
--

INSERT INTO `customer_group_members` (`CustomerGroupMemberID`, `CustomerGroupID`, `UserID`) VALUES
(3, 3, 64);

-- --------------------------------------------------------

--
-- Table structure for table `customizes`
--

CREATE TABLE `customizes` (
  `CustomizeID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customizes`
--

INSERT INTO `customizes` (`CustomizeID`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'uploads/images/831963780792020021907415081898777_2460926880890961_7466254659263397888_n.jpg', 0, 0, 1, '2020-02-19 07:50:32', '2020-02-19 07:50:41', 1, 1),
(2, 'uploads/images/324346328082020021907565472124360_2558790247717603_7384555194358431744_n.jpg', 1, 0, 1, '2020-02-19 07:54:56', '2020-02-19 07:54:56', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customizes_text`
--

CREATE TABLE `customizes_text` (
  `CustomizeTextID` int(11) NOT NULL,
  `CustomizeID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customizes_text`
--

INSERT INTO `customizes_text` (`CustomizeTextID`, `CustomizeID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'test', 1, '2020-02-19 07:50:32', '2020-02-19 07:50:41', 1, 1),
(2, 1, 'test', 2, '2020-02-19 07:50:32', '2020-02-19 07:50:32', 1, 1),
(3, 2, 'second customization', 1, '2020-02-19 07:54:56', '2020-02-19 07:54:56', 1, 1),
(4, 2, 'second customization', 2, '2020-02-19 07:54:56', '2020-02-19 07:54:56', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `DistrictID` int(11) NOT NULL,
  `CityID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`DistrictID`, `CityID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 77, 0, 0, 1, '2018-11-20 08:44:43', '2018-11-20 08:44:43', 1, 1),
(2, 77, 1, 0, 1, '2018-11-20 08:45:36', '2018-11-20 08:45:36', 1, 1),
(3, 77, 2, 0, 1, '2018-11-20 08:45:51', '2018-11-20 08:45:51', 1, 1),
(4, 77, 3, 0, 1, '2018-11-20 08:46:04', '2018-11-20 08:46:04', 1, 1),
(5, 77, 4, 0, 1, '2018-11-20 08:46:34', '2018-11-20 08:46:34', 1, 1),
(11, 77, 5, 0, 1, '2019-05-01 08:34:03', '2019-05-01 08:34:03', 1, 1),
(12, 113, 6, 0, 1, '2019-05-07 13:21:41', '2019-05-07 13:21:41', 1, 1),
(13, 113, 7, 0, 1, '2019-11-27 00:11:29', '2019-11-27 00:11:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `districts_text`
--

CREATE TABLE `districts_text` (
  `DistrictTextID` int(11) NOT NULL,
  `DistrictID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts_text`
--

INSERT INTO `districts_text` (`DistrictTextID`, `DistrictID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Tabuk', 1, '2018-11-20 08:44:43', '2018-11-20 08:44:43', 1, 1),
(2, 2, 'Bahah', 1, '2018-11-20 08:45:36', '2018-11-20 08:45:36', 1, 1),
(3, 3, 'Jawf', 1, '2018-11-20 08:45:51', '2018-11-20 08:45:51', 1, 1),
(4, 4, 'Madinah', 1, '2018-11-20 08:46:04', '2018-11-20 08:46:04', 1, 1),
(5, 5, 'Makkah', 1, '2018-11-20 08:46:34', '2018-11-20 08:46:34', 1, 1),
(13, 11, 'Ruwais', 1, '2019-05-01 08:34:03', '2019-05-01 08:34:03', 1, 1),
(14, 11, 'Ruwais', 2, '2019-05-01 08:34:03', '2019-05-01 08:34:03', 1, 1),
(15, 1, 'Tabuk', 2, '2018-11-20 08:44:43', '2018-11-20 08:44:43', 1, 1),
(16, 2, 'Bahah', 2, '2018-11-20 08:45:36', '2018-11-20 08:45:36', 1, 1),
(17, 3, 'Jawf', 2, '2018-11-20 08:45:51', '2018-11-20 08:45:51', 1, 1),
(18, 4, 'Madinah', 2, '2018-11-20 08:46:04', '2018-11-20 08:46:04', 1, 1),
(19, 5, 'Makkah', 2, '2018-11-20 08:46:34', '2018-11-20 08:46:34', 1, 1),
(20, 12, 'Rabwah', 1, '2019-05-07 13:21:41', '2019-05-07 13:21:41', 1, 1),
(21, 12, 'Rabwah', 2, '2019-05-07 13:21:41', '2019-05-07 13:21:41', 1, 1),
(22, 13, 'AL Hamra', 1, '2019-11-27 00:11:29', '2019-11-27 00:11:29', 1, 1),
(23, 13, 'AL Hamra', 2, '2019-11-27 00:11:29', '2019-11-27 00:11:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `Email_templateID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`Email_templateID`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'uploads/email_templates/1210446676201807020635331.jpg', 0, 0, 1, '2018-07-02 17:08:45', '2019-03-13 14:04:02', 1, 1),
(4, 'uploads/email_templates/379150302201807020631102.jpg', 1, 0, 1, '2018-07-02 18:10:31', '2019-04-05 09:16:16', 1, 1),
(5, 'uploads/email_templates/414693546201807020612353.jpg', 2, 0, 1, '2018-07-02 18:35:12', '2019-04-09 11:37:19', 1, 1),
(6, '', 3, 0, 1, '2019-04-05 09:22:13', '2019-04-09 11:38:19', 1, 1),
(7, '', 4, 0, 1, '2019-11-05 09:02:29', '2019-11-07 12:11:41', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates_text`
--

CREATE TABLE `email_templates_text` (
  `Email_templateTextID` int(11) NOT NULL,
  `Email_templateID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Heading` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates_text`
--

INSERT INTO `email_templates_text` (`Email_templateTextID`, `Email_templateID`, `Title`, `Heading`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Signup', 'Signup', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>Thank you for registering with Chocomood. We hope to provide you the best of our services.</p>\r\n<p>Your login details are:</p>\r\n<p>Email: {{email}}</p>\r\n<p>Password: {{password}}</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 1, '2018-07-02 17:08:45', '2019-03-13 14:04:02', 1, 1),
(4, 4, 'Reset Password', 'Reset Password', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>We received your request to reset your password at chocomood. We have generated a new password for you. Please login using your new password at chocomood and change your password from profile section.</p>\r\n<p>Here are your login details at chocomood.</p>\r\n<p>&nbsp;</p>\r\n<p>Email: {{email}}</p>\r\n<p>Password: {{password}}</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 1, '2018-07-02 18:10:31', '2019-04-05 09:16:16', 1, 1),
(5, 5, 'Order Status Changed', 'Order Status Changed', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for your order no. {{order_no}} is changed to {{order_status}}.</p>', 1, '2018-07-02 18:35:12', '2019-04-09 11:37:19', 1, 1),
(6, 5, 'Order Status Changed', 'Order Status Changed', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for your order no. {{order_no}} is changed to {{order_status}}.</p>', 2, '2018-07-02 18:36:55', '2019-04-09 11:37:33', 1, 1),
(7, 1, 'Signup At Chocomood', 'Signup at Chocomood', '<p><strong>Hi {{fullname}} ,</strong></p>\r\n<p>Thank you for registering with Chocomood. We hope to provide you the best of our services.</p>\r\n<p>Your login details are:</p>\r\n<p>Email: {{email}}</p>\r\n<p>Password: {{password}}</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 2, '2019-03-01 16:04:09', '2019-03-01 16:04:09', 1, 1),
(8, 4, 'Reset Password', 'Reset Password', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>We received your request to reset your password at chocomood. We have generated a new password for you. Please login using your new password at chocomood and change your password from profile section.</p>\r\n<p>Here are your login details at chocomood.</p>\r\n<p>&nbsp;</p>\r\n<p>Email: {{email}}</p>\r\n<p>Password: {{password}}</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 2, '2018-07-02 18:10:31', '2019-04-05 09:16:39', 1, 1),
(9, 6, 'Order Status Changed For Assigned Order Driver', 'Order Status Changed For Assigend Order', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for your assigned order no. {{order_no}} is changed to {{order_status}}.</p>', 1, '2019-04-05 09:22:13', '2019-04-09 11:38:19', 1, 1),
(10, 6, 'Order Status Changed For Assigned Order Driver Ar', 'Order Status Changed For Assigend Order', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for your assigned order no. {{order_no}} is changed to {{order_status}}.</p>', 2, '2019-04-05 09:23:02', '2019-04-09 11:38:34', 1, 1),
(11, 7, 'Delivery OTP', 'Delivery OTP', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>Your order # {{order_number}} is assigned to the delivery boy with delivery OTP {{OTP}}. Please give this OTP to the delivery boy who gives you parcel. You can also ask him to scan QR code sent in email.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 1, '2019-11-05 09:02:29', '2019-11-07 12:11:41', 1, 1),
(12, 7, 'Delivery OTP', 'Delivery OTP', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>Your order is assigned to the delivery boy with delivery OTP {{OTP}}. Please give this OTP to the delivery boy who gives you parcel.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 2, '2019-11-05 09:02:29', '2019-11-05 09:02:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_slider_images`
--

CREATE TABLE `home_slider_images` (
  `HomeSliderImageID` int(11) NOT NULL,
  `UrlLink` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_slider_images`
--

INSERT INTO `home_slider_images` (`HomeSliderImageID`, `UrlLink`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'https://chocomood.henka.tech', 'uploads/images/2731452030420190503021557img1.png', 0, 0, 0, '2019-05-03 14:57:15', '2019-11-27 00:30:25', 1, 1),
(4, 'https://chocomood.henka.tech', 'uploads/images/3434726870420191214055120Dates.jpg', 1, 0, 0, '2019-12-14 17:18:22', '2019-12-14 17:24:01', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_slider_images_text`
--

CREATE TABLE `home_slider_images_text` (
  `HomeSliderImageTextID` int(11) NOT NULL,
  `HomeSliderImageID` int(11) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_slider_images_text`
--

INSERT INTO `home_slider_images_text` (`HomeSliderImageTextID`, `HomeSliderImageID`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, '<h2>Chocolate <span>is the happiness that you can..</span></h2>\r\n<h4>eat!</h4>', 1, '2019-05-22 00:00:00', '2019-11-27 00:30:25', 1, 1),
(2, 1, '<h2>Chocolate <span>is the happiness that you can..</span></h2>\r\n<h4>eat!</h4>\r\narabic', 2, '2019-05-22 00:00:00', '2019-05-03 15:26:09', 1, 1),
(7, 4, '<h2><span><strong>Ramadhan</strong></span></h2>\r\n<br />\r\n<h4><span>Collection</span></h4>', 1, '2019-12-14 17:18:22', '2019-12-14 17:24:01', 1, 1),
(8, 4, '<h2><span><strong>Ramadhan</strong></span></h2>\r\n<br />\r\n<h2>Collection</h2>', 2, '2019-12-14 17:18:22', '2019-12-14 17:18:22', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `LanguageID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `ModuleID` int(11) NOT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `IconClass` varchar(255) NOT NULL,
  `Slug` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`ModuleID`, `ParentID`, `IconClass`, `Slug`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 'settings', '#', 32, 0, 1, '2018-05-09 00:00:00', '2018-11-30 10:32:35', 1, 1),
(18, 1, ' mdi mdi-view-module', 'module', 0, 0, 1, '2018-04-19 15:22:06', '2018-04-19 15:22:06', 1, 1),
(22, 1, 'mdi mdi-yeast', 'role', 3, 0, 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(23, 0, 'account_circle', 'user', 8, 0, 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(32, 1, 'mdi mdi-city', 'city', 7, 0, 1, '2018-05-03 14:01:59', '2018-05-03 14:01:59', 1, 1),
(35, 1, 'mdi mdi-city', 'district', 6, 0, 1, '2018-05-03 15:49:43', '2018-05-03 15:49:43', 1, 1),
(36, 1, 'settings', 'Site_setting', 8, 0, 1, '2018-06-21 20:24:33', '2018-06-21 20:24:33', 1, 1),
(39, 72, ' mdi mdi-email', 'email_template', 9, 0, 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(40, 1, 'mdi mdi-format-list-bulleted', 'language', 10, 1, 1, '2018-07-04 14:12:11', '2018-07-04 14:12:23', 1, 1),
(43, 64, 'view_quilt', 'category', 3, 0, 1, '2018-10-05 00:00:00', '2018-11-30 09:30:09', 1, 1),
(44, 0, 'bookmarks', 'package', 3, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(45, 77, 'file_copy', 'page', 5, 0, 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(46, 0, 'shopping_cart', 'booking', 2, 1, 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(47, 46, '', 'booking/received', 6, 0, 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(48, 46, '', 'booking/assigned', 7, 0, 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(50, 46, '', 'booking/OnTheWay', 8, 0, 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(51, 46, '', 'booking/reached', 9, 0, 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(52, 46, '', 'booking/completed', 10, 0, 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(53, 46, '', 'booking/cancelled', 11, 0, 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(54, 46, '', 'booking', 5, 0, 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(55, 0, 'directions_car', 'vehicle', 6, 1, 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(56, 0, 'build', 'technician', 7, 1, 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(57, 72, 'card_giftcard', 'coupon', 8, 0, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(58, 0, 'shop_two', 'requests', 9, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(59, 46, 'apps', 'booking/overdue', 10, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(60, 72, 'feedback', 'notification', 11, 0, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(61, 0, 'store', 'store', 7, 0, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(62, 64, 'bookmark', 'product', 6, 0, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(63, 0, 'local_atm', '#Sales', 1, 0, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(64, 0, 'shop_two', '#Catalogs', 2, 0, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(65, 63, 'apps', 'orders', 9, 0, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(66, 63, 'apps', 'invoice', 10, 0, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(67, 63, 'apps', 'shipment', 11, 0, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(68, 63, 'apps', 'ticket', 12, 0, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(69, 63, 'apps', 'refund', 13, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(70, 0, 'face', '#customers', 3, 0, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(71, 70, 'apps', 'customerss', 15, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(72, 0, 'insert_chart_outlined', '#Marketing', 4, 0, 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(73, 72, 'apps', 'contactRequest', 17, 0, 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(74, 0, 'assignment', 'report', 6, 0, 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(75, 64, 'assignment', 'collection', 7, 0, 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(76, 1, '', 'nutrition', 8, 0, 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(77, 0, 'file_copy', '#content', 5, 0, 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(78, 77, '', 'home_slider_image', 6, 0, 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(79, 72, '', 'newsletter', 7, 0, 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(80, 1, '', 'taxShipmentCharges', 8, 0, 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(81, 70, '', 'customers/online', 9, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(82, 70, '', 'customers', 10, 0, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(83, 70, '', 'CustomerGroup', 11, 0, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(84, 64, 'local_offer', 'offer', 12, 0, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(85, 72, '', 'searchTag', 13, 0, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(86, 64, '', 'box', 14, 0, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(87, 72, '', 'abandonedCart', 15, 0, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(88, 0, 'bookmark', 'customize', 16, 0, 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(89, 1, 'feedback', 'tag', 17, 0, 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_rights`
--

CREATE TABLE `modules_rights` (
  `ModuleRightID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `CanView` tinyint(4) NOT NULL,
  `CanAdd` tinyint(4) NOT NULL,
  `CanEdit` tinyint(4) NOT NULL,
  `CanDelete` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_rights`
--

INSERT INTO `modules_rights` (`ModuleRightID`, `ModuleID`, `RoleID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(13, 18, 1, 1, 1, 1, 1, '2018-04-19 15:22:06', '2018-06-29 17:44:17', 1, 1),
(17, 18, 2, 1, 1, 1, 1, '2018-05-02 11:37:19', '2018-05-02 11:37:19', 1, 1),
(20, 22, 1, 1, 1, 1, 1, '2018-05-02 12:10:33', '2018-06-29 17:44:17', 1, 1),
(21, 22, 2, 1, 1, 1, 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(22, 23, 1, 1, 1, 1, 1, '2018-05-02 12:44:19', '2018-06-29 17:44:17', 1, 1),
(23, 23, 2, 1, 1, 1, 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(40, 32, 1, 1, 1, 1, 1, '2018-05-03 14:01:59', '2018-06-29 17:44:17', 1, 1),
(41, 32, 2, 1, 1, 1, 1, '2018-05-03 14:01:59', '2018-05-03 14:01:59', 1, 1),
(46, 35, 1, 1, 1, 1, 1, '2018-05-03 15:49:43', '2018-06-29 17:44:17', 1, 1),
(47, 35, 2, 1, 1, 1, 1, '2018-05-03 15:49:43', '2018-05-03 15:49:43', 1, 1),
(52, 18, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(54, 22, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(55, 23, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(56, 32, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(57, 35, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(74, 1, 3, 1, 1, 1, 1, '2018-05-09 12:20:50', '2019-03-06 07:52:27', 1, 1),
(75, 1, 2, 1, 1, 1, 1, '2018-05-09 12:20:50', '2018-05-09 12:20:50', 1, 1),
(76, 1, 1, 1, 1, 1, 1, '2018-05-09 12:20:50', '2018-06-29 17:44:17', 1, 1),
(77, 36, 1, 1, 1, 1, 1, '2018-02-12 16:32:10', '2018-06-29 17:44:17', 1, 1),
(78, 36, 2, 1, 1, 1, 1, '2018-06-29 13:51:40', '2018-06-29 13:51:40', 1, 1),
(79, 36, 3, 0, 0, 0, 0, '2018-06-29 09:34:16', '2019-03-06 07:52:27', 1, 1),
(86, 39, 1, 1, 1, 1, 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(87, 39, 2, 1, 1, 1, 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(88, 39, 3, 0, 0, 0, 0, '2018-07-02 16:26:28', '2019-03-06 07:52:27', 1, 1),
(89, 40, 1, 1, 1, 1, 1, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(90, 40, 2, 1, 1, 1, 1, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(91, 40, 3, 0, 0, 0, 0, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(98, 43, 1, 1, 1, 1, 1, '2018-10-03 19:48:40', '2018-10-04 21:47:28', 1, 1),
(99, 43, 2, 1, 1, 1, 1, '2018-10-03 19:48:40', '2018-10-03 19:48:40', 1, 1),
(100, 43, 3, 0, 0, 0, 0, '2018-10-03 19:48:40', '2019-03-06 07:52:27', 1, 1),
(111, 43, 4, 1, 1, 1, 1, '2018-10-04 20:15:57', '2018-10-04 20:15:57', 1, 1),
(112, 44, 1, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(113, 44, 2, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(114, 44, 3, 0, 0, 0, 0, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(115, 45, 1, 1, 1, 1, 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(116, 45, 2, 1, 1, 1, 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(117, 45, 3, 0, 0, 0, 0, '2018-12-19 05:54:48', '2019-03-06 07:52:27', 1, 1),
(118, 46, 1, 1, 1, 1, 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(119, 46, 2, 1, 1, 1, 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(120, 46, 3, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(121, 47, 1, 1, 1, 1, 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(122, 47, 2, 1, 1, 1, 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(123, 47, 3, 0, 0, 0, 0, '2018-12-21 09:19:50', '2019-03-06 07:52:27', 1, 1),
(124, 48, 1, 1, 1, 1, 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(125, 48, 2, 1, 1, 1, 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(126, 48, 3, 0, 0, 0, 0, '2018-12-21 09:43:33', '2019-03-06 07:52:27', 1, 1),
(130, 50, 1, 1, 1, 1, 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(131, 50, 2, 1, 1, 1, 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(132, 50, 3, 0, 0, 0, 0, '2018-12-21 09:50:43', '2019-03-06 07:52:27', 1, 1),
(133, 51, 1, 1, 1, 1, 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(134, 51, 2, 1, 1, 1, 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(135, 51, 3, 0, 0, 0, 0, '2018-12-21 09:51:27', '2019-03-06 07:52:27', 1, 1),
(136, 52, 1, 1, 1, 1, 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(137, 52, 2, 1, 1, 1, 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(138, 52, 3, 0, 0, 0, 0, '2018-12-21 09:52:06', '2019-03-06 07:52:27', 1, 1),
(139, 53, 1, 1, 1, 1, 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(140, 53, 2, 1, 1, 1, 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(141, 53, 3, 0, 0, 0, 0, '2018-12-21 09:52:39', '2019-03-06 07:52:27', 1, 1),
(142, 54, 1, 1, 1, 1, 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(143, 54, 2, 1, 1, 1, 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(144, 54, 3, 0, 0, 0, 0, '2018-12-21 11:00:54', '2019-03-06 07:52:27', 1, 1),
(145, 55, 1, 1, 1, 1, 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(146, 55, 2, 1, 1, 1, 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(147, 55, 3, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(148, 56, 1, 1, 1, 1, 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(149, 56, 2, 1, 1, 1, 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(150, 56, 3, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(151, 57, 1, 1, 1, 1, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(152, 57, 2, 1, 1, 1, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(153, 57, 3, 0, 0, 0, 0, '2018-12-26 07:06:18', '2019-03-06 07:52:27', 1, 1),
(154, 58, 1, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(155, 58, 2, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(156, 58, 3, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(157, 59, 1, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(158, 59, 2, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(159, 59, 3, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(160, 60, 1, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(161, 60, 2, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(162, 60, 3, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-03-06 07:52:27', 1, 1),
(163, 61, 1, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(164, 61, 2, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(165, 61, 3, 0, 0, 0, 0, '2019-01-29 06:18:23', '2019-03-06 07:52:27', 1, 1),
(166, 62, 1, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(167, 62, 2, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(168, 62, 3, 0, 0, 0, 0, '2019-02-01 05:47:53', '2019-03-06 07:52:27', 1, 1),
(169, 1, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(170, 18, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(171, 22, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(172, 23, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(173, 32, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(174, 35, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(175, 36, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(176, 39, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(177, 40, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(178, 43, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(179, 44, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(180, 45, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(181, 46, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(182, 47, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(183, 48, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(184, 50, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(185, 51, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(186, 52, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(187, 53, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(188, 54, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(189, 55, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(190, 56, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(191, 57, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(192, 58, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(193, 59, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(194, 60, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(195, 61, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(196, 62, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(197, 63, 1, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(198, 63, 2, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(199, 63, 3, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-03-06 07:52:27', 1, 1),
(200, 63, 4, 0, 0, 0, 0, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(201, 64, 1, 1, 1, 1, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(202, 64, 2, 1, 1, 1, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(203, 64, 3, 0, 0, 0, 0, '2019-02-01 11:11:30', '2019-03-06 07:52:27', 1, 1),
(204, 64, 4, 0, 0, 0, 0, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(205, 65, 1, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(206, 65, 2, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(207, 65, 3, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-03-06 07:52:27', 1, 1),
(208, 65, 4, 0, 0, 0, 0, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(209, 66, 1, 1, 1, 1, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(210, 66, 2, 1, 1, 1, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(211, 66, 3, 0, 0, 0, 0, '2019-02-01 11:16:07', '2019-03-06 07:52:27', 1, 1),
(212, 66, 4, 0, 0, 0, 0, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(213, 67, 1, 1, 1, 1, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(214, 67, 2, 1, 1, 1, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(215, 67, 3, 0, 0, 0, 0, '2019-02-01 11:17:01', '2019-03-06 07:52:27', 1, 1),
(216, 67, 4, 0, 0, 0, 0, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(217, 68, 1, 1, 1, 1, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(218, 68, 2, 1, 1, 1, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(219, 68, 3, 0, 0, 0, 0, '2019-02-01 11:17:56', '2019-03-06 07:52:27', 1, 1),
(220, 68, 4, 0, 0, 0, 0, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(221, 69, 1, 1, 1, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(222, 69, 2, 1, 1, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(223, 69, 3, 0, 0, 0, 0, '2019-02-01 11:19:27', '2019-03-06 07:52:27', 1, 1),
(224, 69, 4, 0, 0, 0, 0, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(225, 70, 1, 1, 1, 1, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(226, 70, 2, 1, 1, 1, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(227, 70, 3, 0, 0, 0, 0, '2019-02-01 11:20:46', '2019-03-06 07:52:27', 1, 1),
(228, 70, 4, 0, 0, 0, 0, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(229, 71, 1, 1, 1, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(230, 71, 2, 1, 1, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(231, 71, 3, 0, 0, 0, 0, '2019-02-01 11:22:25', '2019-03-06 07:52:27', 1, 1),
(232, 71, 4, 0, 0, 0, 0, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(233, 72, 1, 1, 1, 1, 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(234, 72, 2, 1, 1, 1, 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(235, 72, 3, 0, 0, 0, 0, '2019-02-01 11:23:19', '2019-03-06 07:52:27', 1, 1),
(236, 72, 4, 0, 0, 0, 0, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(237, 73, 1, 1, 1, 1, 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(238, 73, 2, 1, 1, 1, 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(239, 73, 3, 0, 0, 0, 0, '2019-02-01 11:24:43', '2019-03-06 07:52:27', 1, 1),
(240, 73, 4, 0, 0, 0, 0, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(241, 74, 1, 1, 1, 1, 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(242, 74, 2, 1, 1, 1, 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(243, 74, 3, 0, 0, 0, 0, '2019-02-01 11:29:13', '2019-03-06 07:52:27', 1, 1),
(244, 74, 4, 0, 0, 0, 0, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(245, 75, 1, 1, 1, 1, 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(246, 75, 2, 1, 1, 1, 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(247, 75, 3, 0, 0, 0, 0, '2019-02-01 13:02:53', '2019-03-06 07:52:27', 1, 1),
(248, 75, 4, 0, 0, 0, 0, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(249, 76, 1, 1, 1, 1, 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(250, 76, 2, 1, 1, 1, 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(251, 76, 3, 0, 0, 0, 0, '2019-02-06 07:02:37', '2019-03-06 07:52:27', 1, 1),
(252, 76, 4, 0, 0, 0, 0, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(253, 77, 1, 1, 1, 1, 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(254, 77, 2, 1, 1, 1, 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(255, 77, 3, 0, 0, 0, 0, '2019-02-12 11:27:55', '2019-03-06 07:52:27', 1, 1),
(256, 77, 4, 0, 0, 0, 0, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(257, 78, 1, 1, 1, 1, 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(258, 78, 2, 1, 1, 1, 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(259, 78, 3, 0, 0, 0, 0, '2019-02-12 11:43:22', '2019-03-06 07:52:27', 1, 1),
(260, 78, 4, 0, 0, 0, 0, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(261, 79, 1, 1, 1, 1, 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(262, 79, 2, 1, 1, 1, 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(263, 79, 3, 0, 0, 0, 0, '2019-02-19 12:11:34', '2019-03-06 07:52:27', 1, 1),
(264, 79, 4, 0, 0, 0, 0, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(265, 1, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(266, 18, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(267, 22, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(268, 23, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(269, 32, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(270, 35, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(271, 36, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(272, 39, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(273, 40, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(274, 43, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(275, 44, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(276, 45, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(277, 46, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(278, 47, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(279, 48, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(280, 50, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(281, 51, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(282, 52, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(283, 53, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(284, 54, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(285, 55, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(286, 56, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(287, 57, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(288, 58, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(289, 59, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(290, 60, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(291, 61, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(292, 62, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(293, 63, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(294, 64, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(295, 65, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(296, 66, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(297, 67, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(298, 68, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(299, 69, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(300, 70, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(301, 71, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(302, 72, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(303, 73, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(304, 74, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(305, 75, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(306, 76, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(307, 77, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(308, 78, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(309, 79, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(310, 80, 1, 1, 1, 1, 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(311, 80, 2, 1, 1, 1, 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(312, 80, 3, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-03-06 07:52:27', 1, 1),
(313, 80, 4, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(314, 80, 5, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(315, 81, 1, 1, 1, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(316, 81, 2, 1, 1, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(317, 81, 3, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(318, 81, 4, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(319, 81, 5, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(320, 82, 1, 1, 1, 1, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(321, 82, 2, 1, 1, 1, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(322, 82, 3, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(323, 82, 4, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(324, 82, 5, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(325, 83, 1, 1, 1, 1, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(326, 83, 2, 1, 1, 1, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(327, 83, 3, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(328, 83, 4, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(329, 83, 5, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(330, 84, 1, 1, 1, 1, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(331, 84, 2, 1, 1, 1, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(332, 84, 3, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(333, 84, 4, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(334, 84, 5, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(335, 85, 1, 1, 1, 1, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(336, 85, 2, 1, 1, 1, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(337, 85, 3, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(338, 85, 4, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(339, 85, 5, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(340, 86, 1, 1, 1, 1, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(341, 86, 2, 1, 1, 1, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(342, 86, 3, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(343, 86, 4, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(344, 86, 5, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(345, 87, 1, 1, 1, 1, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(346, 87, 2, 1, 1, 1, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(347, 87, 3, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(348, 87, 4, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(349, 87, 5, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(350, 1, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(351, 18, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(352, 22, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(353, 23, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(354, 32, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(355, 35, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(356, 36, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(357, 39, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(358, 40, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(359, 43, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(360, 44, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(361, 45, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(362, 46, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(363, 47, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(364, 48, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(365, 50, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(366, 51, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(367, 52, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(368, 53, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(369, 54, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(370, 55, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(371, 56, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(372, 57, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(373, 58, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(374, 59, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(375, 60, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(376, 61, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(377, 62, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(378, 63, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(379, 64, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(380, 65, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(381, 66, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(382, 67, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(383, 68, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(384, 69, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(385, 70, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(386, 71, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(387, 72, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(388, 73, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(389, 74, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(390, 75, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(391, 76, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(392, 77, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(393, 78, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(394, 79, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(395, 80, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(396, 81, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(397, 82, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(398, 83, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(399, 84, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(400, 85, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(401, 86, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(402, 87, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(403, 1, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(404, 18, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(405, 22, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(406, 23, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(407, 32, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(408, 35, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(409, 36, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(410, 39, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(411, 40, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(412, 43, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(413, 44, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(414, 45, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(415, 46, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(416, 47, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(417, 48, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(418, 50, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(419, 51, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(420, 52, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(421, 53, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(422, 54, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(423, 55, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(424, 56, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(425, 57, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(426, 58, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(427, 59, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(428, 60, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(429, 61, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(430, 62, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(431, 63, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(432, 64, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(433, 65, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(434, 66, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(435, 67, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(436, 68, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(437, 69, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(438, 70, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(439, 71, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(440, 72, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(441, 73, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(442, 74, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(443, 75, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(444, 76, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(445, 77, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(446, 78, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(447, 79, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(448, 80, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(449, 81, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(450, 82, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(451, 83, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(452, 84, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(453, 85, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(454, 86, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(455, 87, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(456, 1, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(457, 18, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(458, 22, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(459, 23, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(460, 32, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(461, 35, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(462, 36, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(463, 39, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(464, 40, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(465, 43, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(466, 44, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(467, 45, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(468, 46, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(469, 47, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(470, 48, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(471, 50, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(472, 51, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(473, 52, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(474, 53, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(475, 54, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(476, 55, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(477, 56, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(478, 57, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(479, 58, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(480, 59, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(481, 60, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(482, 61, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(483, 62, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(484, 63, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(485, 64, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(486, 65, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(487, 66, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(488, 67, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(489, 68, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(490, 69, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(491, 70, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(492, 71, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(493, 72, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(494, 73, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(495, 74, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(496, 75, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(497, 76, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(498, 77, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(499, 78, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(500, 79, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(501, 80, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(502, 81, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(503, 82, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(504, 83, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(505, 84, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(506, 85, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(507, 86, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(508, 87, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(509, 88, 1, 1, 1, 1, 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(510, 88, 2, 1, 1, 1, 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(511, 88, 3, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(512, 88, 4, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(513, 88, 5, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(514, 88, 6, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(515, 88, 7, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(516, 88, 8, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(517, 89, 1, 1, 1, 1, 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(518, 89, 2, 1, 1, 1, 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(519, 89, 3, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(520, 89, 4, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(521, 89, 5, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(522, 89, 6, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(523, 89, 7, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(524, 89, 8, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_text`
--

CREATE TABLE `modules_text` (
  `ModuleTextID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_text`
--

INSERT INTO `modules_text` (`ModuleTextID`, `ModuleID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 18, 'Module', 1, '2018-04-19 15:22:06', '2018-04-19 15:22:06', 1, 1),
(5, 22, 'Roles', 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(6, 23, 'Backend Users', 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(15, 32, 'City', 1, '2018-05-03 14:01:59', '2018-05-03 14:01:59', 1, 1),
(18, 35, 'District', 1, '2018-05-03 15:49:43', '2018-05-03 15:49:43', 1, 1),
(26, 1, 'Miscellaneous', 1, '2018-05-09 00:00:00', '2018-11-30 10:32:35', 1, 1),
(27, 36, 'Site Setting', 1, '2018-06-29 07:34:18', '2018-06-29 07:34:18', 1, 1),
(30, 39, 'Email Templates', 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(31, 40, 'Languages ', 1, '2018-07-04 14:12:11', '2018-07-04 14:12:23', 1, 1),
(32, 43, 'Categories', 1, '2018-10-05 00:00:00', '2018-11-30 09:30:09', 1, 1),
(33, 44, 'Packages', 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(34, 45, 'Content Pages', 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(35, 46, 'Orders', 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(36, 47, 'Newly Received', 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(37, 48, 'Assigned', 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(39, 50, 'On The Way', 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(40, 51, 'Reached', 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(41, 52, 'Completed', 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(42, 53, 'Cancelled', 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(43, 54, 'All Bookings', 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(44, 55, 'Vehicles', 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(45, 56, 'Technicians', 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(46, 57, 'Coupons', 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(47, 58, 'Package Requests', 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(48, 59, 'Overdue', 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(49, 60, 'Send Notifications', 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(50, 61, 'Stores', 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(51, 62, 'Products', 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(52, 63, 'Sales', 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(53, 64, 'Catalogs', 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(54, 65, 'Orders', 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(55, 66, 'Invoices', 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(56, 67, 'Shipments', 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(57, 68, 'Tickets', 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(58, 69, 'Refunds', 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(59, 70, 'Customers', 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(60, 71, 'All Customerss', 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(61, 72, 'Marketing', 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(62, 73, 'Feedback & CV Collection', 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(63, 74, 'Reports', 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(64, 75, 'Collections', 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(65, 76, 'Nutrition Facts', 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(66, 77, 'Site Content', 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(67, 78, 'Home Slider Images', 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(68, 79, 'Newsletters', 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(69, 80, 'Taxes and Shipment Methods', 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(70, 81, 'Online Customers', 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(71, 82, 'All Customers', 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(72, 83, 'Customer Groups', 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(73, 84, 'Offers', 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(74, 85, 'Searched Tags', 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(75, 86, 'Boxes', 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(76, 87, 'Abandoned Carts', 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(77, 88, 'Customize', 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(78, 89, 'Tags', 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_users_rights`
--

CREATE TABLE `modules_users_rights` (
  `ModuleRightID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `CanView` tinyint(4) NOT NULL,
  `CanAdd` tinyint(4) NOT NULL,
  `CanEdit` tinyint(4) NOT NULL,
  `CanDelete` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_users_rights`
--

INSERT INTO `modules_users_rights` (`ModuleRightID`, `ModuleID`, `UserID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(10, 1, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(11, 18, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(12, 22, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(13, 23, 7, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(14, 32, 7, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(15, 35, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(16, 36, 7, 1, 1, 1, 1, '2018-06-29 12:42:13', '2018-06-29 12:42:13', 1, 1),
(17, 1, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(18, 18, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(19, 22, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(20, 23, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(21, 32, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(22, 35, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(23, 36, 1, 1, 1, 1, 1, '2018-06-29 12:42:13', '2019-03-18 10:37:10', 1, 1),
(28, 39, 1, 1, 1, 1, 1, '2018-07-02 16:26:28', '2019-03-18 10:37:10', 1, 1),
(29, 39, 7, 0, 0, 0, 0, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(30, 40, 1, 1, 1, 1, 1, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(31, 40, 7, 0, 0, 0, 0, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(48, 43, 1, 1, 1, 1, 1, '2018-10-03 19:48:40', '2019-03-18 10:37:10', 1, 1),
(115, 43, 32, 0, 0, 0, 0, '2018-10-04 21:51:22', '2018-10-04 21:52:17', 1, 1),
(123, 45, 1, 1, 1, 1, 1, '2018-10-09 07:12:05', '2019-03-18 10:37:10', 1, 1),
(124, 45, 39, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(125, 45, 40, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(126, 45, 41, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(127, 45, 42, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(128, 45, 43, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(129, 45, 44, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(130, 47, 1, 1, 1, 1, 1, '2018-11-01 06:32:12', '2019-03-18 10:37:10', 1, 1),
(131, 47, 51, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(132, 47, 52, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(133, 47, 53, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(134, 47, 54, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(135, 47, 55, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(136, 47, 56, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(137, 47, 57, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(138, 47, 58, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(139, 47, 59, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(140, 47, 60, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(141, 47, 61, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(142, 47, 62, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(143, 46, 1, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-12-20 08:27:27', 1, 1),
(144, 48, 1, 1, 1, 1, 1, '2018-11-01 07:29:59', '2019-03-18 10:37:10', 1, 1),
(145, 48, 51, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(146, 48, 52, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(147, 48, 53, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(148, 48, 54, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(149, 48, 55, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(150, 48, 56, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(151, 48, 57, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(152, 48, 58, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(153, 48, 59, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(154, 48, 60, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(155, 48, 61, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(156, 48, 62, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(170, 50, 1, 1, 1, 1, 1, '2018-11-15 08:35:05', '2019-03-18 10:37:10', 1, 1),
(171, 50, 51, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(172, 50, 52, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(173, 50, 53, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(174, 50, 54, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(175, 50, 55, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(176, 50, 56, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(177, 50, 57, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(178, 50, 58, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(179, 50, 59, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(180, 50, 60, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(181, 50, 61, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(182, 50, 62, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(183, 50, 63, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(184, 50, 64, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(185, 50, 65, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(186, 50, 66, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(187, 50, 67, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(188, 50, 68, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(189, 51, 1, 1, 1, 1, 1, '2018-11-15 10:21:49', '2019-03-18 10:37:10', 1, 1),
(190, 51, 51, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(191, 51, 52, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(192, 51, 53, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(193, 51, 54, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(194, 51, 55, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(195, 51, 56, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(196, 51, 57, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(197, 51, 58, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(198, 51, 59, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(199, 51, 60, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(200, 51, 61, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(201, 51, 62, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(202, 51, 63, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(203, 51, 64, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(204, 51, 65, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(205, 51, 66, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(206, 51, 67, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(207, 51, 68, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(208, 52, 1, 1, 1, 1, 1, '2018-11-16 11:22:54', '2019-03-18 10:37:10', 1, 1),
(209, 52, 51, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(210, 52, 52, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(211, 52, 53, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(212, 52, 54, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(213, 52, 55, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(214, 52, 56, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(215, 52, 57, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(216, 52, 58, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(217, 52, 59, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(218, 52, 60, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(219, 52, 61, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(220, 52, 62, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(221, 52, 63, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(222, 52, 64, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(223, 52, 65, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(224, 52, 66, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(225, 52, 67, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(226, 52, 68, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(227, 53, 1, 1, 1, 1, 1, '2018-11-19 07:30:34', '2019-03-18 10:37:10', 1, 1),
(228, 53, 51, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(229, 53, 52, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(230, 53, 53, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(231, 53, 54, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(232, 53, 55, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(233, 53, 56, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(234, 53, 57, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(235, 53, 58, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(236, 53, 59, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(237, 53, 60, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(238, 53, 61, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(239, 53, 62, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(240, 53, 63, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(241, 53, 64, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(242, 53, 65, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(243, 53, 66, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(244, 53, 67, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(245, 53, 68, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(246, 53, 69, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(247, 54, 1, 1, 1, 1, 1, '2018-11-22 07:40:38', '2019-03-18 10:37:10', 1, 1),
(248, 54, 51, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(249, 54, 52, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(250, 54, 53, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(251, 54, 54, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(252, 54, 55, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(253, 54, 56, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(254, 54, 57, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(255, 54, 58, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(256, 54, 59, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(257, 54, 60, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(258, 54, 61, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(259, 54, 62, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(260, 54, 63, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(261, 54, 64, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(262, 54, 65, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(263, 54, 66, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(264, 54, 67, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(265, 54, 68, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(266, 54, 69, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(267, 55, 1, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(268, 55, 51, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(269, 55, 52, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(270, 55, 53, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(271, 55, 54, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(272, 55, 55, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(273, 55, 56, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(274, 55, 57, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(275, 55, 58, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(276, 55, 59, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(277, 55, 60, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(278, 55, 61, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(279, 55, 62, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(280, 55, 63, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(281, 55, 64, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(282, 55, 65, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(283, 55, 66, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(284, 55, 67, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(285, 55, 68, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(286, 55, 69, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(287, 56, 1, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(288, 56, 51, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(289, 56, 52, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(290, 56, 53, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(291, 56, 54, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(292, 56, 55, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(293, 56, 56, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(294, 56, 57, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(295, 56, 58, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(296, 56, 59, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(297, 56, 60, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(298, 56, 61, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(299, 56, 62, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(300, 56, 63, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(301, 56, 64, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(302, 56, 65, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(303, 56, 66, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(304, 56, 67, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(305, 56, 68, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(306, 56, 69, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(307, 58, 1, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(308, 58, 51, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(309, 58, 52, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(310, 58, 53, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(311, 58, 54, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(312, 58, 55, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(313, 58, 56, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(314, 58, 57, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(315, 58, 58, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(316, 58, 59, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(317, 58, 60, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(318, 58, 61, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(319, 58, 62, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(320, 58, 63, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(321, 58, 64, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(322, 58, 65, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(323, 58, 66, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(324, 58, 67, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(325, 58, 68, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(326, 58, 69, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(327, 59, 1, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(328, 59, 51, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(329, 59, 52, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(330, 59, 53, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(331, 59, 54, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(332, 59, 55, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(333, 59, 56, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(334, 59, 57, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(335, 59, 58, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(336, 59, 59, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(337, 59, 60, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(338, 59, 61, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(339, 59, 62, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(340, 59, 63, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(341, 59, 64, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(342, 59, 65, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(343, 59, 66, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(344, 59, 67, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(345, 59, 68, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(346, 59, 69, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(347, 60, 1, 1, 1, 1, 1, '2018-11-28 09:31:07', '2019-03-18 10:37:10', 1, 1),
(348, 60, 51, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(349, 60, 52, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(350, 60, 53, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(351, 60, 54, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(352, 60, 55, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(353, 60, 56, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(354, 60, 57, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(355, 60, 58, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(356, 60, 59, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(357, 60, 60, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(358, 60, 61, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(359, 60, 62, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(360, 60, 63, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(361, 60, 64, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(362, 60, 65, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(363, 60, 66, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(364, 60, 67, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(365, 60, 68, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(366, 60, 69, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(367, 61, 1, 1, 1, 1, 1, '2018-11-28 09:32:02', '2019-03-18 10:37:10', 1, 1),
(368, 61, 51, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(369, 61, 52, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(370, 61, 53, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(371, 61, 54, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(372, 61, 55, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(373, 61, 56, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(374, 61, 57, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(375, 61, 58, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(376, 61, 59, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(377, 61, 60, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(378, 61, 61, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(379, 61, 62, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(380, 61, 63, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(381, 61, 64, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(382, 61, 65, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(383, 61, 66, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(384, 61, 67, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(385, 61, 68, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(386, 61, 69, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(387, 62, 1, 1, 1, 1, 1, '2018-11-30 08:20:18', '2019-03-18 10:37:10', 1, 1),
(388, 62, 77, 1, 1, 1, 1, '2018-11-30 08:20:18', '2018-11-30 08:20:18', 1, 1),
(389, 62, 78, 1, 1, 1, 1, '2018-11-30 08:20:18', '2018-11-30 08:20:18', 1, 1),
(390, 62, 79, 1, 1, 1, 1, '2018-11-30 08:20:18', '2018-11-30 08:20:18', 1, 1),
(391, 62, 80, 1, 1, 1, 1, '2018-11-30 08:20:18', '2018-11-30 08:20:18', 1, 1),
(392, 63, 1, 1, 1, 1, 1, '2018-11-30 08:24:03', '2019-03-18 10:37:10', 1, 1),
(393, 63, 77, 1, 1, 1, 1, '2018-11-30 08:24:03', '2018-11-30 08:24:03', 1, 1),
(394, 63, 78, 1, 1, 1, 1, '2018-11-30 08:24:03', '2018-11-30 08:24:03', 1, 1),
(395, 63, 79, 1, 1, 1, 1, '2018-11-30 08:24:03', '2018-11-30 08:24:03', 1, 1),
(396, 63, 80, 1, 1, 1, 1, '2018-11-30 08:24:03', '2018-11-30 08:24:03', 1, 1),
(397, 64, 1, 1, 1, 1, 1, '2018-11-30 08:28:16', '2019-03-18 10:37:10', 1, 1),
(398, 64, 77, 1, 1, 1, 1, '2018-11-30 08:28:16', '2018-11-30 08:28:16', 1, 1),
(399, 64, 78, 1, 1, 1, 1, '2018-11-30 08:28:16', '2018-11-30 08:28:16', 1, 1),
(400, 64, 79, 1, 1, 1, 1, '2018-11-30 08:28:16', '2018-11-30 08:28:16', 1, 1),
(401, 64, 80, 1, 1, 1, 1, '2018-11-30 08:28:16', '2018-11-30 08:28:16', 1, 1),
(402, 65, 1, 1, 1, 1, 1, '2018-11-30 09:34:59', '2019-03-18 10:37:10', 1, 1),
(403, 65, 77, 1, 1, 1, 1, '2018-11-30 09:34:59', '2018-11-30 09:34:59', 1, 1),
(404, 65, 78, 1, 1, 1, 1, '2018-11-30 09:34:59', '2018-11-30 09:34:59', 1, 1),
(405, 65, 79, 1, 1, 1, 1, '2018-11-30 09:34:59', '2018-11-30 09:34:59', 1, 1),
(406, 65, 80, 1, 1, 1, 1, '2018-11-30 09:34:59', '2018-11-30 09:34:59', 1, 1),
(407, 66, 1, 1, 1, 1, 1, '2018-11-30 09:37:50', '2019-03-18 10:37:10', 1, 1),
(408, 66, 77, 1, 1, 1, 1, '2018-11-30 09:37:50', '2018-11-30 09:37:50', 1, 1),
(409, 66, 78, 1, 1, 1, 1, '2018-11-30 09:37:50', '2018-11-30 09:37:50', 1, 1),
(410, 66, 79, 1, 1, 1, 1, '2018-11-30 09:37:50', '2018-11-30 09:37:50', 1, 1),
(411, 66, 80, 1, 1, 1, 1, '2018-11-30 09:37:50', '2018-11-30 09:37:50', 1, 1),
(412, 67, 1, 1, 1, 1, 1, '2018-12-11 06:50:04', '2019-03-18 10:37:10', 1, 1),
(413, 67, 77, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(414, 67, 78, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(415, 67, 79, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(416, 67, 80, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(417, 67, 81, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(418, 67, 82, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(419, 67, 83, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(420, 68, 1, 1, 1, 1, 1, '2018-12-11 06:51:20', '2019-03-18 10:37:10', 1, 1),
(421, 68, 77, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(422, 68, 78, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(423, 68, 79, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(424, 68, 80, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(425, 68, 81, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(426, 68, 82, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(427, 68, 83, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(428, 69, 1, 1, 1, 1, 1, '2018-12-11 06:53:12', '2019-03-18 10:37:10', 1, 1),
(429, 69, 77, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(430, 69, 78, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(431, 69, 79, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(432, 69, 80, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(433, 69, 81, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(434, 69, 82, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(435, 69, 83, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(436, 70, 1, 1, 1, 1, 1, '2018-12-11 06:59:04', '2019-03-18 10:37:10', 1, 1),
(437, 70, 77, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(438, 70, 78, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(439, 70, 79, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(440, 70, 80, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(441, 70, 81, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(442, 70, 82, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(443, 70, 83, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(444, 44, 1, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-20 08:27:27', 1, 1),
(445, 44, 77, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(446, 44, 78, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(447, 44, 79, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(448, 44, 80, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(449, 44, 81, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(450, 44, 82, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(451, 44, 83, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(452, 44, 85, 0, 0, 0, 0, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(453, 44, 86, 0, 0, 0, 0, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(454, 44, 87, 0, 0, 0, 0, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(455, 45, 1, 1, 1, 1, 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(456, 45, 90, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(457, 45, 91, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(458, 45, 92, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(459, 45, 93, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(460, 45, 94, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(461, 45, 95, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(462, 45, 96, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(463, 45, 97, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(464, 45, 98, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(465, 45, 99, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(466, 45, 100, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(467, 45, 101, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(468, 45, 102, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(469, 45, 103, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(470, 45, 104, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(471, 45, 105, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(472, 45, 106, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(473, 45, 107, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(474, 45, 108, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(475, 45, 109, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(476, 46, 1, 1, 1, 1, 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(477, 46, 90, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(478, 46, 91, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(479, 46, 92, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(480, 46, 93, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(481, 46, 94, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(482, 46, 95, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(483, 46, 96, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(484, 46, 97, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(485, 46, 98, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(486, 46, 99, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(487, 46, 100, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(488, 46, 101, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(489, 46, 102, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(490, 46, 103, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(491, 46, 104, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(492, 46, 105, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(493, 46, 106, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(494, 46, 107, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(495, 46, 108, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(496, 46, 109, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(497, 1, 114, 1, 1, 1, 1, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(498, 18, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(499, 22, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(500, 23, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(501, 32, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(502, 35, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(503, 36, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(504, 39, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(505, 40, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(506, 43, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(507, 44, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(508, 45, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(509, 46, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(510, 47, 1, 1, 1, 1, 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(511, 47, 77, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(512, 47, 90, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(513, 47, 91, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(514, 47, 92, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(515, 47, 93, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(516, 47, 94, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(517, 47, 95, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(518, 47, 96, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(519, 47, 97, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(520, 47, 98, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(521, 47, 99, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(522, 47, 100, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(523, 47, 101, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(524, 47, 102, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(525, 47, 103, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(526, 47, 104, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(527, 47, 105, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(528, 47, 106, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(529, 47, 107, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(530, 47, 108, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(531, 47, 109, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(532, 47, 110, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(533, 47, 111, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(534, 47, 112, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(535, 47, 113, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(536, 47, 114, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(537, 48, 1, 1, 1, 1, 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(538, 48, 77, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(539, 48, 90, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(540, 48, 91, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(541, 48, 92, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(542, 48, 93, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(543, 48, 94, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(544, 48, 95, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(545, 48, 96, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(546, 48, 97, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(547, 48, 98, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(548, 48, 99, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(549, 48, 100, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(550, 48, 101, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(551, 48, 102, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(552, 48, 103, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(553, 48, 104, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(554, 48, 105, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(555, 48, 106, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(556, 48, 107, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(557, 48, 108, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(558, 48, 109, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(559, 48, 110, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(560, 48, 111, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(561, 48, 112, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(562, 48, 113, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(563, 48, 114, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(591, 50, 1, 1, 1, 1, 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(592, 50, 77, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(593, 50, 90, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(594, 50, 91, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(595, 50, 92, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(596, 50, 93, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(597, 50, 94, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(598, 50, 95, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(599, 50, 96, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(600, 50, 97, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(601, 50, 98, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(602, 50, 99, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(603, 50, 100, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(604, 50, 101, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(605, 50, 102, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(606, 50, 103, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(607, 50, 104, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(608, 50, 105, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(609, 50, 106, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(610, 50, 107, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(611, 50, 108, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(612, 50, 109, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(613, 50, 110, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(614, 50, 111, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(615, 50, 112, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(616, 50, 113, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(617, 50, 114, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(618, 51, 1, 1, 1, 1, 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(619, 51, 77, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(620, 51, 90, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(621, 51, 91, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(622, 51, 92, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(623, 51, 93, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(624, 51, 94, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(625, 51, 95, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(626, 51, 96, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(627, 51, 97, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(628, 51, 98, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(629, 51, 99, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(630, 51, 100, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(631, 51, 101, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(632, 51, 102, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(633, 51, 103, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(634, 51, 104, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(635, 51, 105, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(636, 51, 106, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(637, 51, 107, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(638, 51, 108, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(639, 51, 109, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(640, 51, 110, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(641, 51, 111, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(642, 51, 112, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(643, 51, 113, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(644, 51, 114, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(645, 52, 1, 1, 1, 1, 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(646, 52, 77, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(647, 52, 90, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(648, 52, 91, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(649, 52, 92, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(650, 52, 93, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(651, 52, 94, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(652, 52, 95, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(653, 52, 96, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(654, 52, 97, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(655, 52, 98, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(656, 52, 99, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(657, 52, 100, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(658, 52, 101, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(659, 52, 102, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(660, 52, 103, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(661, 52, 104, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(662, 52, 105, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(663, 52, 106, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(664, 52, 107, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(665, 52, 108, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(666, 52, 109, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(667, 52, 110, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(668, 52, 111, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(669, 52, 112, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(670, 52, 113, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(671, 52, 114, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(672, 53, 1, 1, 1, 1, 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(673, 53, 77, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(674, 53, 90, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(675, 53, 91, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(676, 53, 92, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(677, 53, 93, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(678, 53, 94, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(679, 53, 95, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(680, 53, 96, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(681, 53, 97, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(682, 53, 98, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(683, 53, 99, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(684, 53, 100, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(685, 53, 101, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(686, 53, 102, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(687, 53, 103, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(688, 53, 104, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(689, 53, 105, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(690, 53, 106, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(691, 53, 107, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(692, 53, 108, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(693, 53, 109, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(694, 53, 110, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(695, 53, 111, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(696, 53, 112, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(697, 53, 113, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(698, 53, 114, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(699, 54, 1, 1, 1, 1, 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(700, 54, 77, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(701, 54, 90, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(702, 54, 91, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(703, 54, 92, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(704, 54, 93, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(705, 54, 94, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(706, 54, 95, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(707, 54, 96, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(708, 54, 97, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(709, 54, 98, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(710, 54, 99, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(711, 54, 100, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(712, 54, 101, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(713, 54, 102, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(714, 54, 103, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(715, 54, 104, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(716, 54, 105, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(717, 54, 106, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(718, 54, 107, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(719, 54, 108, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(720, 54, 109, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(721, 54, 110, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(722, 54, 111, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(723, 54, 112, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(724, 54, 113, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(725, 54, 114, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(726, 55, 1, 1, 1, 1, 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(727, 55, 77, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(728, 55, 90, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(729, 55, 91, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(730, 55, 92, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(731, 55, 93, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(732, 55, 94, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(733, 55, 95, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(734, 55, 96, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(735, 55, 97, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(736, 55, 98, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(737, 55, 99, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(738, 55, 100, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(739, 55, 101, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(740, 55, 102, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(741, 55, 103, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(742, 55, 104, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(743, 55, 105, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(744, 55, 106, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(745, 55, 107, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(746, 55, 108, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(747, 55, 109, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(748, 55, 110, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(749, 55, 111, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(750, 55, 112, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(751, 55, 113, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(752, 55, 114, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(753, 55, 115, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(754, 56, 1, 1, 1, 1, 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(755, 56, 77, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(756, 56, 90, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(757, 56, 91, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(758, 56, 92, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(759, 56, 93, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(760, 56, 94, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(761, 56, 95, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(762, 56, 96, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(763, 56, 97, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(764, 56, 98, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(765, 56, 99, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(766, 56, 100, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(767, 56, 101, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(768, 56, 102, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(769, 56, 103, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(770, 56, 104, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(771, 56, 105, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(772, 56, 106, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(773, 56, 107, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(774, 56, 108, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(775, 56, 109, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(776, 56, 110, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(777, 56, 111, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(778, 56, 112, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(779, 56, 113, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(780, 56, 114, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(781, 56, 115, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(782, 57, 1, 1, 1, 1, 1, '2018-12-26 07:06:18', '2019-03-18 10:37:10', 1, 1),
(783, 57, 77, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(784, 57, 90, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(785, 57, 91, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(786, 57, 92, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(787, 57, 93, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1);
INSERT INTO `modules_users_rights` (`ModuleRightID`, `ModuleID`, `UserID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(788, 57, 94, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(789, 57, 95, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(790, 57, 96, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(791, 57, 97, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(792, 57, 98, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(793, 57, 99, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(794, 57, 100, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(795, 57, 101, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(796, 57, 102, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(797, 57, 103, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(798, 57, 104, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(799, 57, 105, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(800, 57, 106, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(801, 57, 107, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(802, 57, 108, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(803, 57, 109, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(804, 57, 110, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(805, 57, 111, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(806, 57, 112, 1, 1, 1, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(807, 57, 113, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(808, 57, 114, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(809, 57, 115, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(810, 57, 116, 1, 1, 1, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(811, 58, 1, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(812, 58, 77, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(813, 58, 90, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(814, 58, 91, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(815, 58, 92, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(816, 58, 93, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(817, 58, 94, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(818, 58, 95, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(819, 58, 96, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(820, 58, 97, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(821, 58, 98, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(822, 58, 99, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(823, 58, 100, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(824, 58, 101, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(825, 58, 102, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(826, 58, 103, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(827, 58, 104, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(828, 58, 105, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(829, 58, 106, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(830, 58, 107, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(831, 58, 108, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(832, 58, 109, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(833, 58, 110, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(834, 58, 111, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(835, 58, 112, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(836, 58, 113, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(837, 58, 114, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(838, 58, 115, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(839, 58, 116, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(840, 58, 117, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(841, 59, 1, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(842, 59, 77, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(843, 59, 90, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(844, 59, 91, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(845, 59, 92, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(846, 59, 93, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(847, 59, 94, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(848, 59, 95, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(849, 59, 96, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(850, 59, 97, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(851, 59, 98, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(852, 59, 99, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(853, 59, 100, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(854, 59, 101, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(855, 59, 102, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(856, 59, 103, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(857, 59, 104, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(858, 59, 105, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(859, 59, 106, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(860, 59, 107, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(861, 59, 108, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(862, 59, 109, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(863, 59, 110, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(864, 59, 111, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(865, 59, 112, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(866, 59, 113, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(867, 59, 114, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(868, 59, 115, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(869, 59, 116, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(870, 59, 117, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(871, 59, 118, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(872, 60, 1, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(873, 60, 77, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(874, 60, 90, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(875, 60, 91, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(876, 60, 92, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(877, 60, 93, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(878, 60, 94, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(879, 60, 95, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(880, 60, 96, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(881, 60, 97, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(882, 60, 98, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(883, 60, 99, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(884, 60, 100, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(885, 60, 101, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(886, 60, 102, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(887, 60, 103, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(888, 60, 104, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(889, 60, 105, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(890, 60, 106, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(891, 60, 107, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(892, 60, 108, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(893, 60, 109, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(894, 60, 110, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(895, 60, 111, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(896, 60, 112, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(897, 60, 113, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(898, 60, 114, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(899, 60, 115, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(900, 60, 116, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(901, 60, 117, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(902, 60, 118, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(903, 61, 1, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(904, 61, 42, 0, 0, 0, 0, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(905, 61, 44, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(906, 61, 45, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(907, 61, 46, 0, 0, 0, 0, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(908, 1, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(909, 18, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(910, 22, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(911, 23, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(912, 32, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(913, 35, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(914, 36, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(915, 39, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(916, 40, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(917, 43, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(918, 44, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(919, 45, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(920, 46, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(921, 47, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(922, 48, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(923, 50, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(924, 51, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(925, 52, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(926, 53, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(927, 54, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(928, 55, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(929, 56, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(930, 57, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(931, 58, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(932, 59, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(933, 60, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(934, 61, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(935, 62, 1, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(936, 62, 42, 0, 0, 0, 0, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(937, 62, 44, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(938, 62, 45, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(939, 62, 46, 0, 0, 0, 0, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(940, 62, 47, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(941, 1, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(942, 18, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(943, 22, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(944, 23, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(945, 32, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(946, 35, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(947, 36, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(948, 39, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(949, 40, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(950, 43, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(951, 44, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(952, 45, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(953, 46, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(954, 47, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(955, 48, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(956, 50, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(957, 51, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(958, 52, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(959, 53, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(960, 54, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(961, 55, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(962, 56, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(963, 57, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(964, 58, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(965, 59, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(966, 60, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(967, 61, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(968, 62, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(969, 1, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(970, 18, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(971, 22, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(972, 23, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(973, 32, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(974, 35, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(975, 36, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(976, 39, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(977, 40, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(978, 43, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(979, 44, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(980, 45, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(981, 46, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(982, 47, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(983, 48, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(984, 50, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(985, 51, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(986, 52, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(987, 53, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(988, 54, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(989, 55, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(990, 56, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(991, 57, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(992, 58, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(993, 59, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(994, 60, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(995, 61, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(996, 62, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(997, 1, 50, 1, 1, 1, 1, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(998, 18, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(999, 22, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1000, 23, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1001, 32, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1002, 35, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1003, 36, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1004, 39, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1005, 40, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1006, 43, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1007, 44, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1008, 45, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1009, 46, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1010, 47, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1011, 48, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1012, 50, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1013, 51, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1014, 52, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1015, 53, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1016, 54, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1017, 55, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1018, 56, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1019, 57, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1020, 58, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1021, 59, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1022, 60, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1023, 61, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1024, 62, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1025, 63, 1, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(1026, 63, 49, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(1027, 63, 50, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-03-06 07:53:21', 1, 1),
(1028, 64, 1, 1, 1, 1, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(1029, 64, 49, 1, 1, 1, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(1030, 64, 50, 0, 0, 0, 0, '2019-02-01 11:11:30', '2019-03-06 07:53:21', 1, 1),
(1031, 65, 1, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(1032, 65, 49, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(1033, 65, 50, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-03-06 07:53:21', 1, 1),
(1034, 66, 1, 1, 1, 1, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(1035, 66, 49, 1, 1, 1, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(1036, 66, 50, 0, 0, 0, 0, '2019-02-01 11:16:07', '2019-03-06 07:53:21', 1, 1),
(1037, 67, 1, 1, 1, 1, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(1038, 67, 49, 1, 1, 1, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(1039, 67, 50, 0, 0, 0, 0, '2019-02-01 11:17:01', '2019-03-06 07:53:21', 1, 1),
(1040, 68, 1, 1, 1, 1, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(1041, 68, 49, 1, 1, 1, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(1042, 68, 50, 0, 0, 0, 0, '2019-02-01 11:17:56', '2019-03-06 07:53:21', 1, 1),
(1043, 69, 1, 1, 1, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(1044, 69, 49, 1, 1, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(1045, 69, 50, 0, 0, 0, 0, '2019-02-01 11:19:27', '2019-03-06 07:53:21', 1, 1),
(1046, 70, 1, 1, 1, 1, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(1047, 70, 49, 1, 1, 1, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(1048, 70, 50, 0, 0, 0, 0, '2019-02-01 11:20:46', '2019-03-06 07:53:21', 1, 1),
(1049, 71, 1, 1, 1, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(1050, 71, 49, 1, 1, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(1051, 71, 50, 0, 0, 0, 0, '2019-02-01 11:22:25', '2019-03-06 07:53:21', 1, 1),
(1052, 72, 1, 1, 1, 1, 1, '2019-02-01 11:23:19', '2019-03-18 10:37:10', 1, 1),
(1053, 72, 49, 1, 1, 1, 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(1054, 72, 50, 0, 0, 0, 0, '2019-02-01 11:23:19', '2019-03-06 07:53:21', 1, 1),
(1055, 73, 1, 1, 1, 1, 1, '2019-02-01 11:24:43', '2019-03-18 10:37:10', 1, 1),
(1056, 73, 49, 1, 1, 1, 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(1057, 73, 50, 0, 0, 0, 0, '2019-02-01 11:24:43', '2019-03-06 07:53:21', 1, 1),
(1058, 74, 1, 1, 1, 1, 1, '2019-02-01 11:29:13', '2019-03-18 10:37:10', 1, 1),
(1059, 74, 49, 1, 1, 1, 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(1060, 74, 50, 0, 0, 0, 0, '2019-02-01 11:29:13', '2019-03-06 07:53:21', 1, 1),
(1061, 75, 1, 1, 1, 1, 1, '2019-02-01 13:02:53', '2019-03-18 10:37:10', 1, 1),
(1062, 75, 49, 1, 1, 1, 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(1063, 75, 50, 0, 0, 0, 0, '2019-02-01 13:02:53', '2019-03-06 07:53:21', 1, 1),
(1064, 76, 1, 1, 1, 1, 1, '2019-02-06 07:02:37', '2019-03-18 10:37:10', 1, 1),
(1065, 76, 49, 1, 1, 1, 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(1066, 76, 50, 0, 0, 0, 0, '2019-02-06 07:02:37', '2019-03-06 07:53:21', 1, 1),
(1067, 77, 1, 1, 1, 1, 1, '2019-02-12 11:27:55', '2019-03-18 10:37:10', 1, 1),
(1068, 77, 49, 1, 1, 1, 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(1069, 77, 50, 0, 0, 0, 0, '2019-02-12 11:27:55', '2019-03-06 07:53:21', 1, 1),
(1070, 78, 1, 1, 1, 1, 1, '2019-02-12 11:43:22', '2019-03-18 10:37:10', 1, 1),
(1071, 78, 49, 1, 1, 1, 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(1072, 78, 50, 0, 0, 0, 0, '2019-02-12 11:43:22', '2019-03-06 07:53:21', 1, 1),
(1073, 79, 1, 1, 1, 1, 1, '2019-02-19 12:11:34', '2019-03-18 10:37:10', 1, 1),
(1074, 79, 49, 1, 1, 1, 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(1075, 79, 50, 0, 0, 0, 0, '2019-02-19 12:11:34', '2019-03-06 07:53:21', 1, 1),
(1076, 79, 54, 0, 0, 0, 0, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(1077, 80, 1, 1, 1, 1, 1, '2019-02-25 12:52:39', '2019-03-18 10:37:10', 1, 1),
(1078, 80, 49, 1, 1, 1, 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(1079, 80, 50, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-03-06 07:53:21', 1, 1),
(1080, 80, 54, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(1081, 81, 1, 1, 1, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1082, 81, 49, 1, 1, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1083, 81, 50, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1084, 81, 54, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1085, 81, 55, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1086, 81, 56, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1087, 82, 1, 1, 1, 1, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1088, 82, 49, 1, 1, 1, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1089, 82, 50, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1090, 82, 54, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1091, 82, 55, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1092, 82, 56, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1093, 83, 1, 1, 1, 1, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1094, 83, 49, 1, 1, 1, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1095, 83, 50, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1096, 83, 54, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1097, 83, 55, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1098, 83, 56, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1099, 84, 1, 1, 1, 1, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1100, 84, 49, 1, 1, 1, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1101, 84, 50, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1102, 84, 54, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1103, 84, 55, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1104, 84, 56, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1105, 85, 1, 1, 1, 1, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1106, 85, 49, 1, 1, 1, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1107, 85, 50, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1108, 85, 54, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1109, 85, 55, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1110, 85, 56, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1111, 85, 57, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1112, 1, 59, 1, 1, 1, 1, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1113, 18, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1114, 22, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1115, 23, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1116, 32, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1117, 35, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1118, 36, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1119, 39, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1120, 40, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1121, 43, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1122, 44, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1123, 45, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1124, 46, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1125, 47, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1126, 48, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1127, 50, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1128, 51, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1129, 52, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1130, 53, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1131, 54, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1132, 55, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1133, 56, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1134, 57, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1135, 58, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1136, 59, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1137, 60, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1138, 61, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1139, 62, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1140, 63, 59, 1, 1, 1, 1, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1141, 64, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1142, 65, 59, 1, 1, 1, 1, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1143, 66, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1144, 67, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1145, 68, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1146, 69, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1147, 70, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1148, 71, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1149, 72, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1150, 73, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1151, 74, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1152, 75, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1153, 76, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1154, 77, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1155, 78, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1156, 79, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1157, 80, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1158, 81, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1159, 82, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1160, 83, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1161, 84, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1162, 85, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1163, 1, 60, 1, 1, 1, 1, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1164, 18, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1165, 22, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1166, 23, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1167, 32, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1168, 35, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1169, 36, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1170, 39, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1171, 40, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1172, 43, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1173, 44, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1174, 45, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1175, 46, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1176, 47, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1177, 48, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1178, 50, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1179, 51, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1180, 52, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1181, 53, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1182, 54, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1183, 55, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1184, 56, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1185, 57, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1186, 58, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1187, 59, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1188, 60, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1189, 61, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1190, 62, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1191, 63, 60, 1, 1, 1, 1, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1192, 64, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1193, 65, 60, 1, 1, 1, 1, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1194, 66, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1195, 67, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1196, 68, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1197, 69, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1198, 70, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1199, 71, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1200, 72, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1201, 73, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1202, 74, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1203, 75, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1204, 76, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1205, 77, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1206, 78, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1207, 79, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1208, 80, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1209, 81, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1210, 82, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1211, 83, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1212, 84, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1213, 85, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1214, 86, 1, 1, 1, 1, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1215, 86, 49, 1, 1, 1, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1216, 86, 50, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1217, 86, 54, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1218, 86, 55, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1219, 86, 56, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1220, 86, 57, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1221, 86, 58, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1222, 86, 59, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1223, 86, 60, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1224, 87, 1, 1, 1, 1, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1225, 87, 49, 1, 1, 1, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1226, 87, 50, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1227, 87, 54, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1228, 87, 55, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1229, 87, 56, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1230, 87, 57, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1231, 87, 58, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1232, 87, 59, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1233, 87, 60, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1234, 87, 61, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1235, 87, 62, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1236, 1, 66, 1, 1, 1, 1, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1237, 18, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1238, 22, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1239, 23, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1240, 32, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1241, 35, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1242, 36, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1243, 39, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1244, 40, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1245, 43, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1246, 44, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1247, 45, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1248, 46, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1249, 47, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1250, 48, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1251, 50, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1252, 51, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1253, 52, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1254, 53, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1255, 54, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1256, 55, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1257, 56, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1258, 57, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1259, 58, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1260, 59, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1261, 60, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1262, 61, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1263, 62, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1264, 63, 66, 1, 1, 1, 1, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1265, 64, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1266, 65, 66, 1, 1, 1, 1, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1267, 66, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1268, 67, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1269, 68, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1270, 69, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1271, 70, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1272, 71, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1273, 72, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1274, 73, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1275, 74, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1276, 75, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1277, 76, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1278, 77, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1279, 78, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1280, 79, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1281, 80, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1282, 81, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1283, 82, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1284, 83, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1285, 84, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1286, 85, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1287, 86, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1288, 87, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1289, 1, 67, 1, 1, 1, 1, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1290, 18, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1291, 22, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1292, 23, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1293, 32, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1294, 35, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1295, 36, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1296, 39, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1297, 40, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1298, 43, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1299, 44, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1300, 45, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1301, 46, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1302, 47, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1303, 48, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1304, 50, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1305, 51, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1306, 52, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1307, 53, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1308, 54, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1309, 55, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1310, 56, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1311, 57, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1312, 58, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1313, 59, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1314, 60, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1315, 61, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1316, 62, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1317, 63, 67, 1, 1, 1, 1, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1318, 64, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1319, 65, 67, 1, 1, 1, 1, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1320, 66, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1321, 67, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1322, 68, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1323, 69, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1324, 70, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1325, 71, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1326, 72, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1327, 73, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1328, 74, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1329, 75, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1330, 76, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1331, 77, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1332, 78, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1333, 79, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1334, 80, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1335, 81, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1336, 82, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1337, 83, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1338, 84, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1339, 85, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1340, 86, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1341, 87, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1342, 1, 68, 1, 1, 1, 1, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1343, 18, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1344, 22, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1345, 23, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1346, 32, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1347, 35, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1348, 36, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1349, 39, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1350, 40, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1351, 43, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1352, 44, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1353, 45, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1354, 46, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1355, 47, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1356, 48, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1357, 50, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1358, 51, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1359, 52, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1360, 53, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1361, 54, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1362, 55, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1363, 56, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1364, 57, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1365, 58, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1366, 59, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1367, 60, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1368, 61, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1369, 62, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1370, 63, 68, 1, 1, 1, 1, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1371, 64, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1372, 65, 68, 1, 1, 1, 1, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1373, 66, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1374, 67, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1375, 68, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1376, 69, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1377, 70, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1378, 71, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1379, 72, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1380, 73, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1381, 74, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1382, 75, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1383, 76, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1384, 77, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1385, 78, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1386, 79, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1387, 80, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1388, 81, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1389, 82, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1390, 83, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1391, 84, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1392, 85, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1393, 86, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1394, 87, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1395, 1, 69, 1, 1, 1, 1, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1396, 18, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1397, 22, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1398, 23, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1399, 32, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1400, 35, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1401, 36, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1402, 39, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1403, 40, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1404, 43, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1405, 44, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1406, 45, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1407, 46, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1408, 47, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1409, 48, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1410, 50, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1411, 51, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1412, 52, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1413, 53, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1414, 54, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1415, 55, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1416, 56, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1417, 57, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1418, 58, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1419, 59, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1420, 60, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1421, 61, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1422, 62, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1423, 63, 69, 1, 1, 1, 1, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1424, 64, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1425, 65, 69, 1, 1, 1, 1, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1426, 66, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1427, 67, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1);
INSERT INTO `modules_users_rights` (`ModuleRightID`, `ModuleID`, `UserID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1428, 68, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1429, 69, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1430, 70, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1431, 71, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1432, 72, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1433, 73, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1434, 74, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1435, 75, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1436, 76, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1437, 77, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1438, 78, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1439, 79, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1440, 80, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1441, 81, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1442, 82, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1443, 83, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1444, 84, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1445, 85, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1446, 86, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1447, 87, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1448, 1, 70, 1, 1, 1, 1, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1449, 18, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1450, 22, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1451, 23, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1452, 32, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1453, 35, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1454, 36, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1455, 39, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1456, 40, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1457, 43, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1458, 44, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1459, 45, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1460, 46, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1461, 47, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1462, 48, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1463, 50, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1464, 51, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1465, 52, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1466, 53, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1467, 54, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1468, 55, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1469, 56, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1470, 57, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1471, 58, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1472, 59, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1473, 60, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1474, 61, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1475, 62, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1476, 63, 70, 1, 1, 1, 1, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1477, 64, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1478, 65, 70, 1, 1, 1, 1, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1479, 66, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1480, 67, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1481, 68, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1482, 69, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1483, 70, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1484, 71, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1485, 72, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1486, 73, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1487, 74, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1488, 75, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1489, 76, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1490, 77, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1491, 78, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1492, 79, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1493, 80, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1494, 81, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1495, 82, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1496, 83, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1497, 84, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1498, 85, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1499, 86, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1500, 87, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1501, 1, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1502, 18, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1503, 22, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1504, 23, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1505, 32, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1506, 35, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1507, 36, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1508, 39, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1509, 40, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1510, 43, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1511, 44, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1512, 45, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1513, 46, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1514, 47, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1515, 48, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1516, 50, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1517, 51, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1518, 52, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1519, 53, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1520, 54, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1521, 55, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1522, 56, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1523, 57, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1524, 58, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1525, 59, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1526, 60, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1527, 61, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1528, 62, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1529, 63, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1530, 64, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1531, 65, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1532, 66, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1533, 67, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1534, 68, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1535, 69, 72, 0, 0, 0, 0, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1536, 70, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1537, 71, 72, 0, 0, 0, 0, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1538, 72, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1539, 73, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1540, 74, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1541, 75, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1542, 76, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1543, 77, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1544, 78, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1545, 79, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1546, 80, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1547, 81, 72, 0, 0, 0, 0, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1548, 82, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1549, 83, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1550, 84, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1551, 85, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1552, 86, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1553, 87, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1554, 88, 1, 1, 1, 1, 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(1555, 88, 64, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(1556, 88, 71, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(1557, 88, 72, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(1558, 89, 1, 1, 1, 1, 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(1559, 89, 64, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(1560, 89, 71, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(1561, 89, 72, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nutritions`
--

CREATE TABLE `nutritions` (
  `NutritionID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nutritions`
--

INSERT INTO `nutritions` (`NutritionID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 0, 1, '2019-02-13 11:34:56', '2019-12-07 16:19:20', 1, 1),
(2, 1, 0, 1, '2019-02-14 05:32:48', '2019-12-07 16:19:39', 1, 1),
(3, 2, 0, 1, '2019-12-07 16:19:57', '2019-12-07 16:19:57', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nutritions_text`
--

CREATE TABLE `nutritions_text` (
  `NutritionTextID` int(11) NOT NULL,
  `NutritionID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nutritions_text`
--

INSERT INTO `nutritions_text` (`NutritionTextID`, `NutritionID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Carbohydrates', 1, '2019-02-13 11:34:56', '2019-12-07 16:19:20', 1, 1),
(2, 2, 'Protient', 1, '2019-02-14 05:32:48', '2019-12-07 16:19:39', 1, 1),
(3, 3, 'Fat', 1, '2019-12-07 16:19:57', '2019-12-07 16:19:57', 1, 1),
(4, 3, 'Fat', 2, '2019-12-07 16:19:57', '2019-12-07 16:19:57', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `OfferID` int(11) NOT NULL,
  `ProductID` varchar(255) NOT NULL,
  `ValidFrom` date NOT NULL,
  `ValidTo` date NOT NULL,
  `DiscountType` enum('percentage','per item') NOT NULL,
  `Discount` float NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `IsForAll` tinyint(4) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`OfferID`, `ProductID`, `ValidFrom`, `ValidTo`, `DiscountType`, `Discount`, `SortOrder`, `IsActive`, `IsForAll`, `Hide`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(4, '34', '2019-12-12', '2019-12-10', 'percentage', 20, 0, 1, 1, 0, '2019-12-10 19:14:12', '2019-12-10 19:14:12', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers_groups`
--

CREATE TABLE `offers_groups` (
  `OfferGroupID` int(11) NOT NULL,
  `OfferID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `offers_text`
--

CREATE TABLE `offers_text` (
  `OfferTextID` int(11) NOT NULL,
  `OfferID` int(11) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(1000) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers_text`
--

INSERT INTO `offers_text` (`OfferTextID`, `OfferID`, `SystemLanguageID`, `Title`, `Description`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(2, 4, 1, 'mabruk', '', '2019-12-10 19:14:12', '2019-12-10 19:14:12', 1, 1),
(3, 4, 2, 'mabruk', '', '2019-12-10 19:14:12', '2019-12-10 19:14:12', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers_users_notification`
--

CREATE TABLE `offers_users_notification` (
  `OfferUserNotificationID` int(11) NOT NULL,
  `OfferID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `IsShow` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers_users_notification`
--

INSERT INTO `offers_users_notification` (`OfferUserNotificationID`, `OfferID`, `GroupID`, `UserID`, `IsShow`) VALUES
(1, 0, 0, 64, 0),
(2, 0, 0, 71, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `OrderID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Description` varchar(500) CHARACTER SET utf8 NOT NULL,
  `OrderNumber` varchar(255) NOT NULL,
  `AddressID` int(11) NOT NULL,
  `AddressIDForPaymentCollection` int(11) NOT NULL,
  `ShipmentMethodID` int(11) NOT NULL,
  `CollectFromStore` int(11) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1' COMMENT 'These are defined in order_statuses table',
  `StoreID` int(11) NOT NULL,
  `BranchDeliveryDistrictID` int(11) NOT NULL,
  `CouponCodeUsed` varchar(255) NOT NULL,
  `CouponCodeDiscountPercentage` decimal(10,2) NOT NULL,
  `DiscountAvailed` decimal(10,2) NOT NULL,
  `TotalShippingCharges` decimal(10,2) NOT NULL,
  `TotalTaxAmount` decimal(10,2) NOT NULL,
  `TotalAmount` decimal(10,2) NOT NULL,
  `DriverID` int(11) NOT NULL,
  `DeliveryOTP` varchar(10) NOT NULL,
  `ShippedThroughApi` int(11) NOT NULL,
  `PaymentMethod` varchar(255) NOT NULL,
  `IsRead` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`OrderID`, `UserID`, `Description`, `OrderNumber`, `AddressID`, `AddressIDForPaymentCollection`, `ShipmentMethodID`, `CollectFromStore`, `Status`, `StoreID`, `BranchDeliveryDistrictID`, `CouponCodeUsed`, `CouponCodeDiscountPercentage`, `DiscountAvailed`, `TotalShippingCharges`, `TotalTaxAmount`, `TotalAmount`, `DriverID`, `DeliveryOTP`, `ShippedThroughApi`, `PaymentMethod`, `IsRead`, `CreatedAt`) VALUES
(9, 64, '', '00009', 0, 0, 0, 1, 1, 0, 0, '', '0.00', '0.00', '20.00', '4.25', '89.25', 0, '', 0, 'COD', 1, '2019-12-04 18:20:28'),
(10, 64, '', '00010', 4, 4, 3, 0, 1, 0, 0, '', '0.00', '0.00', '500.00', '28.25', '593.25', 0, '', 0, 'COD', 1, '2019-12-04 18:21:59'),
(11, 71, '', '00011', 0, 0, 0, 1, 1, 0, 0, 'BLK001', '20.00', '93.00', '500.00', '28.25', '593.25', 0, '', 0, 'COD', 0, '2019-12-15 12:30:21'),
(12, 1, '', '00011', 0, 0, 0, 1, 1, 0, 0, 'BLK001', '20.00', '93.00', '500.00', '28.25', '593.25', 0, '', 0, 'COD', 0, '2019-12-15 12:30:21'),
(13, 64, '', '00011', 0, 0, 0, 1, 1, 0, 0, '', '0.00', '0.00', '500.00', '28.25', '593.25', 0, '', 0, 'COD', 0, '2019-12-15 12:30:21'),
(14, 64, '', '00014', 4, 4, 1, 0, 1, 0, 0, 'BLK001', '20.00', '45.80', '20.00', '10.16', '213.36', 0, '', 0, 'COD', 0, '2020-01-03 10:18:57'),
(15, 64, '', '00015', 4, 4, 1, 0, 1, 0, 0, '', '0.00', '0.00', '20.00', '4.25', '89.25', 0, '', 0, 'COD', 0, '2020-02-14 09:17:29'),
(16, 64, '', '00016', 4, 4, 1, 0, 1, 0, 0, '', '0.00', '0.00', '20.00', '4.25', '89.25', 0, '', 0, 'COD', 0, '2020-02-14 09:18:34'),
(17, 64, '', '00017', 4, 4, 1, 0, 1, 0, 0, '', '0.00', '0.00', '20.00', '6.10', '128.10', 0, '', 0, 'COD', 1, '2020-02-14 09:21:26'),
(18, 64, '', '00018', 4, 4, 1, 0, 1, 6, 13, '', '0.00', '0.00', '20.00', '9.80', '205.80', 0, '', 0, 'COD', 1, '2020-02-17 11:05:10');

-- --------------------------------------------------------

--
-- Table structure for table `orders_extra_charges`
--

CREATE TABLE `orders_extra_charges` (
  `OrderExtraChargeID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `TaxShipmentChargesID` int(11) NOT NULL,
  `Title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Factor` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Amount` decimal(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_extra_charges`
--

INSERT INTO `orders_extra_charges` (`OrderExtraChargeID`, `OrderID`, `TaxShipmentChargesID`, `Title`, `Factor`, `Amount`) VALUES
(22, 11, 2, 'VAT', '5%', '28.25'),
(21, 11, 3, 'Express Delivery', '500.00 SAR', '500.00'),
(20, 10, 2, 'VAT', '5%', '28.25'),
(19, 10, 3, 'Express Delivery', '500.00 SAR', '500.00'),
(18, 9, 2, 'VAT', '5%', '4.25'),
(17, 9, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(23, 14, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(24, 14, 2, 'VAT', '5%', '10.16'),
(25, 15, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(26, 15, 2, 'VAT', '5%', '4.25'),
(27, 16, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(28, 16, 2, 'VAT', '5%', '4.25'),
(29, 17, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(30, 17, 2, 'VAT', '5%', '6.10'),
(31, 18, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(32, 18, 2, 'VAT', '5%', '9.80');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `OrderItemID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Amount` decimal(10,2) NOT NULL,
  `ItemType` enum('Product','Collection','Chocobox','Choco Message','Customized Shape') NOT NULL,
  `CustomizedOrderProductIDs` text NOT NULL COMMENT 'It will be having product ids if item type is chocobox or chocomsg',
  `CustomizedShapeImage` text NOT NULL COMMENT 'This will be having value if Item Type is customized shape',
  `CustomizedBoxID` int(11) NOT NULL COMMENT 'This will be having values only for choco message and choco box item types',
  `IsCorporateItem` int(11) NOT NULL,
  `Ribbon` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`OrderItemID`, `OrderID`, `ProductID`, `Quantity`, `Amount`, `ItemType`, `CustomizedOrderProductIDs`, `CustomizedShapeImage`, `CustomizedBoxID`, `IsCorporateItem`, `Ribbon`) VALUES
(13, 11, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(12, 10, 35, 1, '65.00', 'Product', '', '', 0, 0, 0),
(11, 9, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(14, 14, 34, 3, '65.00', 'Product', '', '', 0, 0, 0),
(15, 14, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(16, 15, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(17, 16, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(18, 17, 35, 3, '34.00', 'Product', '', '', 0, 0, 0),
(19, 18, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(20, 18, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(21, 18, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(22, 18, 45, 1, '12.00', 'Product', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `OrderStatusID` int(11) NOT NULL,
  `OrderStatusEn` varchar(255) NOT NULL,
  `OrderStatusAr` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ForAdmin` int(11) NOT NULL,
  `ForUser` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`OrderStatusID`, `OrderStatusEn`, `OrderStatusAr`, `ForAdmin`, `ForUser`) VALUES
(1, 'Pending', 'تم الاستلام', 0, 0),
(2, 'Packed', 'تعيين', 0, 0),
(3, 'Dispatched', 'منجز', 0, 0),
(4, 'Delivered', 'ألغيت', 0, 0),
(5, 'Cancelled', 'ألغيت', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `PageID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `BannerImage` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`PageID`, `Image`, `BannerImage`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'uploads/images/4484721307720190201092155img11.png', 'uploads/images/3568822746620190201090055img10.png', 0, 0, 1, '2018-12-19 06:00:56', '2019-02-01 09:55:21', 1, 1),
(6, '', '', 1, 0, 1, '2019-02-01 14:10:34', '2019-11-27 00:56:38', 1, 1),
(7, 'uploads/images/4195540731920190212110434img1.png', '', 2, 0, 1, '2019-02-12 11:34:04', '2019-04-12 14:33:20', 1, 1),
(8, 'uploads/images/5742807259820190411094704banner2.png', '', 3, 0, 1, '2019-02-12 11:34:04', '2019-04-12 14:39:10', 1, 1),
(9, '', '', 4, 0, 1, '2019-03-14 11:41:29', '2019-05-16 10:46:48', 1, 1),
(10, 'uploads/images/5972342051020190412111615cus1.png', '', 5, 1, 1, '2019-04-12 11:15:16', '2019-04-12 11:15:16', 1, 1),
(11, 'uploads/images/8544251421720190412114121cus2.png', '', 6, 1, 1, '2019-04-12 11:20:30', '2019-04-12 11:35:42', 1, 1),
(12, 'uploads/images/4719728060920190412115626cus3.png', '', 7, 1, 1, '2019-04-12 11:22:36', '2019-04-12 11:26:56', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages_text`
--

CREATE TABLE `pages_text` (
  `PageTextID` int(11) NOT NULL,
  `PageID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `ShortDescription` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `MetaTags` varchar(255) NOT NULL,
  `MetaKeywords` varchar(255) NOT NULL,
  `MetaDescription` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages_text`
--

INSERT INTO `pages_text` (`PageTextID`, `PageID`, `Title`, `Description`, `ShortDescription`, `SystemLanguageID`, `MetaTags`, `MetaKeywords`, `MetaDescription`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'About Us', '<h2>Chocomood</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<h2>Corporate Chocolates</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus. Sed sed dui at ligula ullamcorper feugiat nec nec ipsum. Nam elit justo, semper faucibus sodales et, feugiat ac ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesq', '', 1, '', '', '', '2018-12-19 06:00:56', '2019-02-01 09:55:21', 1, 1),
(2, 1, 'Arabic About US', '<p> هذا هو بعض الوصف الوهمي العربي المحدث </ p>', '', 2, '', '', '', '2018-12-19 06:01:28', '2018-12-19 11:07:51', 1, 1),
(11, 6, 'Contact Us', '<ul>\r\n<li>\r\n<h5>Working Hours</h5>\r\n<h6>9:00&nbsp;AM&nbsp; -&nbsp; 4:00 PM</h6>\r\n</li>\r\n<li>\r\n<h5>Marketing Departament</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Quality Insurance</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Whole Sales</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Customer Care</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n</ul>', '', 1, 'Contact us', '', '', '2019-02-01 14:10:34', '2019-11-27 00:56:38', 1, 1),
(12, 6, 'Contact Us', '<ul>\r\n<li>\r\n<h5>Working Hours arabic</h5>\r\n<h6>9:00 am --- 04:00 pm</h6>\r\n</li>\r\n<li>\r\n<h5>Marketing Departament</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Quality Insurance</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Whole Sales</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Customer Care</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n</ul>', '', 2, '', '', '', '2019-02-01 14:10:49', '2019-02-13 13:10:49', 1, 1),
(13, 7, 'Home', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum.</p>', '<h2>Chocolate <span>is the happiness that you can..</span></h2>\r\n<h4>eat!</h4>', 1, '', '', '', '2019-02-12 11:34:04', '2019-04-12 14:33:20', 1, 1),
(14, 7, 'Home', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum.</p>', '<h2>Chocolate <span>is the happiness that you can..</span></h2>\r\n<h4>eat!</h4>', 2, '', '', '', '2019-02-12 11:34:25', '2019-04-12 14:33:54', 1, 1),
(15, 8, 'Corporate', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum.</p>', '<h3 class=\"wow fadeInRightBig\" data-wow-animation=\"4s\">Enjoy Best</h3>\r\n<h4 class=\"wow fadeInRightBig\" data-wow-animation=\"7s\">Chocolates</h4>\r\n<h3 class=\"wow fadeInUpBig last\" data-wow-animation=\"10s\">At Your Events!</h3>', 1, '', '', '', '2019-02-12 11:34:04', '2019-04-12 14:39:10', 1, 1),
(16, 8, 'Corporate', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum.</p>', '', 2, '', '', '', '2019-02-12 11:34:25', '2019-02-12 11:34:25', 1, 1),
(17, 9, 'Terms and conditions Updated', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>', '', 1, '', '', 'This is billing agreement page', '2019-03-14 11:41:29', '2019-05-16 10:46:48', 1, 1),
(18, 9, 'الأحكام والشروط', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>', '', 2, '', '', '', '2019-03-14 11:41:58', '2019-05-08 10:15:31', 1, 1),
(19, 10, 'Choco Message', '<p>Personalize your <br />message</p>', '', 1, 'Choco Message', 'Choco Message', 'Choco Message', '2019-04-12 11:15:16', '2019-04-12 11:15:16', 1, 1),
(20, 10, 'Choco Message', '<p>Personalize your <br />message</p>', '', 2, 'Choco Message', 'Choco Message', 'Choco Message', '2019-04-12 11:15:16', '2019-04-12 11:15:16', 1, 1),
(21, 11, 'Choco Box', '<p>Personalize your <br />Chocolate Box</p>', '', 1, 'Choco Box', 'Choco Box', 'Choco Box', '2019-04-12 11:20:30', '2019-04-12 11:35:42', 1, 1),
(22, 11, 'Choco Box', '<p>Personalize your <br />Chocolate Box</p>', '', 2, 'Choco Box', 'Choco Box', 'Choco Box', '2019-04-12 11:20:30', '2019-04-12 11:20:30', 1, 1),
(23, 12, 'Choco Shape', '<p>Personalize your <br />Chocolate Mold</p>', '', 1, 'Choco Shape', 'Choco Shape', 'Choco Shape', '2019-04-12 11:22:36', '2019-04-12 11:26:56', 1, 1),
(24, 12, 'Choco Shape', '<p>Personalize your <br />Chocolate Mold</p>', '', 2, 'Choco Shape', 'Choco Shape', 'Choco Shape', '2019-04-12 11:22:36', '2019-04-12 11:22:36', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `CategoryID` varchar(255) NOT NULL,
  `SubCategoryID` varchar(255) NOT NULL,
  `Price` float NOT NULL,
  `PriceType` enum('item','kg') DEFAULT 'item',
  `MinimumOrderQuantity` varchar(255) NOT NULL COMMENT 'for per KG is in grams',
  `SKU` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `IsFeatured` tinyint(4) NOT NULL,
  `IsCustomizedProduct` tinyint(4) NOT NULL,
  `OutOfStock` tinyint(4) NOT NULL,
  `BoxIDs` text NOT NULL COMMENT 'This will have value if IsCustomizedProduct is 1',
  `TagIDs` varchar(255) NOT NULL,
  `IsCorporateProduct` int(11) NOT NULL,
  `CorporateMinQuantity` text NOT NULL,
  `CorporatePrice` decimal(10,2) NOT NULL,
  `PurchaseCount` int(11) NOT NULL COMMENT 'No of times this item is purchased',
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductID`, `CategoryID`, `SubCategoryID`, `Price`, `PriceType`, `MinimumOrderQuantity`, `SKU`, `SortOrder`, `Hide`, `IsActive`, `IsFeatured`, `IsCustomizedProduct`, `OutOfStock`, `BoxIDs`, `TagIDs`, `IsCorporateProduct`, `CorporateMinQuantity`, `CorporatePrice`, `PurchaseCount`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(34, '34', '41', 65, 'kg', '2000', '001', 2, 0, 1, 0, 0, 0, '', '', 0, '', '0.00', 10, '2019-11-30 12:13:54', '2020-02-14 12:20:04', 1, 72),
(35, '34', '41', 34, 'item', '', '0002', 3, 0, 1, 1, 0, 0, '', '', 0, '', '0.00', 5, '2019-11-30 12:15:47', '2019-11-30 12:17:18', 1, 1),
(36, '34', '41', 999, 'item', '', '009', 4, 0, 0, 0, 0, 0, '', '', 1, '899', '0.00', 0, '2019-11-30 12:21:05', '2019-12-15 11:53:57', 1, 1),
(37, '34,35,36', '40,45', 200, '', '', '125', 0, 0, 1, 1, 1, 0, '1,2', '1,2', 1, '23', '100.00', 0, '2019-12-17 11:21:43', '2020-03-04 17:34:07', 1, 1),
(38, '1111', '2222', 200, '', '', '125', 0, 0, 1, 1, 1, 0, '1,2,3,4,5', '', 1, '23', '100.00', 0, '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(39, '1112', '2223', 500, '', '', '120', 0, 0, 1, 1, 1, 1, '1,2,3,4,5', '', 1, '200', '1000.00', 0, '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(40, '1113', '2224', 1000, '', '', '400', 0, 0, 1, 1, 1, 1, '1,2,3,4,5', '', 1, '250', '10000.00', 0, '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(41, '1114', '2225', 1500, '', '', '500', 0, 0, 1, 1, 1, 1, '1,2,3,4,5', '', 1, '300', '100000.00', 0, '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(42, '34', '40', 25, 'kg', '20', '1222', 1, 0, 1, 1, 0, 0, '', '', 0, '', '0.00', 0, '2020-02-10 10:30:33', '2020-02-10 10:33:08', 1, 1),
(43, '34', '40', 250, 'item', '500', '1222', 2, 0, 1, 0, 0, 0, '', '', 0, '', '0.00', 0, '2020-02-10 13:21:33', '2020-02-11 09:07:44', 1, 1),
(44, '34', '40', 12, 'item', '', '1222sd', 3, 0, 1, 0, 0, 0, '', '', 0, '', '0.00', 0, '2020-02-10 13:53:07', '2020-02-10 13:53:07', 1, 1),
(45, '34', '41', 12, 'item', '', '12313', 4, 0, 1, 0, 0, 0, '', '', 0, '', '0.00', 1, '2020-02-10 13:56:04', '2020-02-10 14:43:52', 1, 1),
(46, '34,39', '41,53,54', 2213, 'item', '1', '43432', 5, 0, 1, 0, 0, 0, '', '', 0, '', '0.00', 0, '2020-02-12 12:30:19', '2020-02-12 12:38:18', 1, 1),
(47, '1,2', '2,3', 123, 'item', '', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 18:55:56', '2020-02-26 18:55:56', 1, 1),
(48, '1,2', '2,3', 123, 'item', '', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 18:56:06', '2020-02-26 18:56:06', 1, 1),
(49, '1,2', '2,3', 123, 'item', '', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 18:57:09', '2020-02-26 18:57:09', 1, 1),
(50, '1,2', '3,4', 123, 'kg', '', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 18:57:09', '2020-02-26 18:57:09', 1, 1),
(51, '1,2', '2,3', 123, 'item', '', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:05:46', '2020-02-26 19:05:46', 1, 1),
(52, '1,2', '3,4', 123, 'kg', '', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:05:46', '2020-02-26 19:05:46', 1, 1),
(53, '1,2', '2,3', 123, 'item', '20', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:06:44', '2020-02-26 19:06:44', 1, 1),
(54, '1,2', '3,4', 123, 'kg', '25', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:06:44', '2020-02-26 19:06:44', 1, 1),
(55, '1,2', '2,3', 123, 'item', '20', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:20:44', '2020-02-26 19:20:44', 1, 1),
(56, '1,2', '3,4', 123, 'kg', '25', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:20:44', '2020-02-26 19:20:44', 1, 1),
(57, '1,2', '2,3', 123, 'item', '20', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:20:51', '2020-02-26 19:20:51', 1, 1),
(58, '1,2', '3,4', 123, 'kg', '25', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:20:51', '2020-02-26 19:20:51', 1, 1),
(59, '1,2', '2,3', 123, 'item', '20', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:22:14', '2020-02-26 19:22:14', 1, 1),
(60, '1,2', '3,4', 123, 'kg', '25', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:22:14', '2020-02-26 19:22:14', 1, 1),
(61, '34,37', '42,43,51,52', 250, 'item', '1', '1222', 1, 0, 1, 0, 0, 0, '', '1,3', 0, '', '0.00', 0, '2020-03-04 17:36:52', '2020-03-04 17:36:52', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_inside`
--

CREATE TABLE `products_inside` (
  `ProductInsideID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `InsideTitle` varchar(255) NOT NULL,
  `InsideTitleAr` varchar(255) NOT NULL,
  `InsideImage` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_inside`
--

INSERT INTO `products_inside` (`ProductInsideID`, `ProductID`, `InsideTitle`, `InsideTitleAr`, `InsideImage`) VALUES
(1, 44, 'add whats inside', 'add whats inside arab', '1'),
(2, 44, 'this is testin', 'dfasdfsa', '1'),
(3, 44, 'dsfds', 'dsfds', '1'),
(5, 45, 'dsf', 'dfsdf', 'uploads/images/183391624052020021001045682902625_212951926410060_6594469099533762560_o.jpg'),
(8, 45, 'sdfdsfd', 'dsfadf', 'uploads/images/498906680052020021002524372124360_2558790247717603_7384555194358431744_n.jpg'),
(10, 43, 'test', 'test', 'uploads/images/933186558412020021108233981898777_2460926880890961_7466254659263397888_n.jpg'),
(14, 43, 'sdf', 'dsfa', 'uploads/images/658282548012020021108014172124360_2558790247717603_7384555194358431744_n.jpg'),
(16, 43, 'dsfds', 'sadfda', 'uploads/images/995034514482020021108405284212650_115903443280448_6658839702839230464_o.jpg'),
(18, 43, 'dsfdsa', 'dsfasd', 'uploads/images/337343665992020021108505372124360_2558790247717603_7384555194358431744_n.jpg'),
(22, 43, 'dfdsfas', 'dsfadsf', 'uploads/images/176056264062020021109140784212650_115903443280448_6658839702839230464_o.jpg'),
(23, 43, 'fdsaf', 'dsfds', 'uploads/images/32731617338202002110944072019-12-09.png'),
(24, 43, 'dsfdsaf', 'dsfads', 'uploads/images/620398903912020021109440781898777_2460926880890961_7466254659263397888_n.jpg'),
(25, 59, 'inside one', 'inside one', 'imagename'),
(26, 59, 'inside two', 'inside two', 'iamgename');

-- --------------------------------------------------------

--
-- Table structure for table `products_text`
--

CREATE TABLE `products_text` (
  `ProducrTextID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `Ingredients` varchar(1000) NOT NULL,
  `Specifications` text NOT NULL,
  `Keywords` varchar(500) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `MetaTags` varchar(255) NOT NULL,
  `MetaKeywords` varchar(255) NOT NULL,
  `MetaDescription` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_text`
--

INSERT INTO `products_text` (`ProducrTextID`, `ProductID`, `Title`, `Description`, `Ingredients`, `Specifications`, `Keywords`, `SystemLanguageID`, `MetaTags`, `MetaKeywords`, `MetaDescription`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(48, 34, 'CS-14', '', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 1, '', '', '', '2019-11-30 12:13:54', '2020-02-14 12:20:04', 1, 72),
(49, 34, 'CS-14', '', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 2, '', '', '', '2019-11-30 12:13:54', '2019-11-30 12:13:54', 1, 1),
(50, 35, 'cs-16', 'kk', 'kkk', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 1, '', '', '', '2019-11-30 12:15:47', '2019-11-30 12:17:18', 1, 1),
(51, 35, 'cs-16', '', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 2, '', '', '', '2019-11-30 12:15:47', '2019-11-30 12:15:47', 1, 1),
(52, 36, 'CS-17', '', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 1, '', '', '', '2019-11-30 12:21:05', '2019-12-15 11:53:57', 1, 1),
(53, 36, 'CS-17', '', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 2, '', '', '', '2019-11-30 12:21:05', '2019-11-30 12:21:05', 1, 1),
(54, 37, 'Intour Hotel', 'available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice Product', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:21:43', '2020-03-04 17:34:07', 1, 1),
(55, 38, 'Intour Hotel', 'available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice Product', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(56, 39, 'Cholocate Lounge', 'available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice Product', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(57, 40, 'Carve Burger', 'available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice Product', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(58, 41, 'Al Shurafa', 'not available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice One', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(59, 42, 'per kg product', 'this is testing product', 'this is ingredients for testing products', '<p>this is specification for these products</p>', 'keywords', 1, 'testing', 'meta keywords', 'this is testing', '2020-02-10 10:30:33', '2020-02-10 10:33:08', 1, 1),
(60, 42, 'per kg product', 'this is testing product', 'this is ingredients for testing products', '<p>this is specification for these products</p>', 'keywords', 2, 'testing', 'meta keywords', 'this is testing', '2020-02-10 10:30:33', '2020-02-10 10:30:33', 1, 1),
(61, 43, 'test', 'this is testing', 'this is testing', '<p>this is tesitn</p>', 'test', 1, 'testing', 'meta keywords', 'this is teistn', '2020-02-10 13:21:33', '2020-02-11 09:07:44', 1, 1),
(62, 43, 'test', 'this is testing', 'this is testing', '<p>this is tesitn</p>', 'test', 2, 'testing', 'meta keywords', 'this is teistn', '2020-02-10 13:21:33', '2020-02-10 13:21:33', 1, 1),
(63, 44, 'test agin ain', 'this is tesitng', 'this is tesitn', '<p>this is testing</p>', 'dscfds', 1, 'dsfdas', 'sdaf', 'this is tesitng', '2020-02-10 13:53:07', '2020-02-10 13:53:07', 1, 1),
(64, 44, 'test agin ain', 'this is tesitng', 'this is tesitn', '<p>this is testing</p>', 'dscfds', 2, 'dsfdas', 'sdaf', 'this is tesitng', '2020-02-10 13:53:07', '2020-02-10 13:53:07', 1, 1),
(65, 45, 'testfdsfds', 'tdfjdalskfj', 'kdlsfjnlksadjfkl', '<p>psdfjlkadsjfl</p>', 'keywords', 1, 'this is testing', 'dkfjlkdsa', 'kdsjflkadsjf', '2020-02-10 13:56:04', '2020-02-10 14:43:52', 1, 1),
(66, 45, 'testfdsfds', 'tdfjdalskfj', 'kdlsfjnlksadjfkl', '<p>psdfjlkadsjfl</p>', 'keywords', 2, 'this is testing', 'dkfjlkdsa', 'kdsjflkadsjf', '2020-02-10 13:56:04', '2020-02-10 13:56:04', 1, 1),
(67, 46, 'this is tesitn', 'this is testing', 'this is tesitng', '<p>thjisdhf d</p>', 'keywords', 1, 'dsfdas', 'dkfjlkdsa', 'tkhsjfdkjsfl', '2020-02-12 12:30:19', '2020-02-12 12:38:18', 1, 1),
(68, 46, 'this is tesitn', 'this is testing', 'this is tesitng', '<p>thjisdhf d</p>', 'keywords', 2, 'dsfdas', 'dkfjlkdsa', 'tkhsjfdkjsfl', '2020-02-12 12:30:19', '2020-02-12 12:30:19', 1, 1),
(69, 47, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 18:55:56', '2020-02-26 18:55:56', 1, 1),
(70, 48, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 18:56:06', '2020-02-26 18:56:06', 1, 1),
(71, 49, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 18:57:09', '2020-02-26 18:57:09', 1, 1),
(72, 50, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 18:57:09', '2020-02-26 18:57:09', 1, 1),
(73, 51, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:05:46', '2020-02-26 19:05:46', 1, 1),
(74, 52, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:05:46', '2020-02-26 19:05:46', 1, 1),
(75, 53, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:06:44', '2020-02-26 19:06:44', 1, 1),
(76, 54, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:06:44', '2020-02-26 19:06:44', 1, 1),
(77, 55, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:20:44', '2020-02-26 19:20:44', 1, 1),
(78, 56, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:20:44', '2020-02-26 19:20:44', 1, 1),
(79, 57, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:20:51', '2020-02-26 19:20:51', 1, 1),
(80, 58, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:20:51', '2020-02-26 19:20:51', 1, 1),
(81, 59, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:22:14', '2020-02-26 19:22:14', 1, 1),
(82, 60, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:22:14', '2020-02-26 19:22:14', 1, 1),
(83, 61, 'tagging product', 'this is testing', 'this is testing', '<p>this is testing<br></p>', 'dfgdgdf', 1, 'gfdg', 'fdgfdsg', 'fdgfsdgsfdgsfd', '2020-03-04 17:36:52', '2020-03-04 17:36:52', 1, 1),
(84, 61, 'tagging product', 'this is testing', 'this is testing', '<p>this is testing<br></p>', 'dfgdgdf', 2, 'gfdg', 'fdgfdsg', 'fdgfsdgsfdgsfd', '2020-03-04 17:36:52', '2020-03-04 17:36:52', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_nutritions`
--

CREATE TABLE `product_nutritions` (
  `NutritionProductID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `NutritionID` int(11) NOT NULL,
  `Quantity` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_nutritions`
--

INSERT INTO `product_nutritions` (`NutritionProductID`, `ProductID`, `NutritionID`, `Quantity`) VALUES
(29, 33, 2, 0),
(30, 35, 1, 0),
(35, 38, 1, 20),
(36, 38, 2, 0),
(37, 38, 3, 0),
(38, 38, 4, 0),
(39, 39, 1, 20),
(40, 39, 2, 0),
(41, 39, 3, 0),
(42, 39, 4, 0),
(43, 40, 1, 30),
(44, 40, 2, 0),
(45, 40, 3, 0),
(46, 40, 4, 0),
(47, 41, 1, 50),
(48, 41, 2, 0),
(49, 41, 3, 0),
(50, 41, 4, 0),
(55, 42, 1, 12),
(56, 42, 2, 12),
(59, 44, 1, 20),
(62, 45, 2, 25),
(74, 43, 1, 20),
(76, 46, 2, 20),
(77, 34, 3, 10),
(78, 47, 1, 4),
(79, 47, 2, 5),
(80, 48, 1, 4),
(81, 48, 2, 5),
(82, 49, 1, 4),
(83, 49, 2, 5),
(84, 50, 1, 4),
(85, 50, 2, 5),
(86, 51, 1, 4),
(87, 51, 2, 5),
(88, 52, 1, 4),
(89, 52, 2, 5),
(90, 53, 1, 4),
(91, 53, 2, 5),
(92, 54, 1, 4),
(93, 54, 2, 5),
(94, 55, 1, 4),
(95, 55, 2, 5),
(96, 56, 1, 4),
(97, 56, 2, 5),
(98, 57, 1, 4),
(99, 57, 2, 5),
(100, 58, 1, 4),
(101, 58, 2, 5),
(102, 59, 1, 4),
(103, 59, 2, 5),
(104, 60, 1, 4),
(105, 60, 2, 5),
(109, 37, 1, 20),
(110, 61, 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `product_ratings`
--

CREATE TABLE `product_ratings` (
  `ProductRatingID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Rating` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_ratings`
--

INSERT INTO `product_ratings` (`ProductRatingID`, `ProductID`, `UserID`, `Rating`) VALUES
(1, 34, 64, 4);

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `ProductReviewID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Comment` text CHARACTER SET utf8 NOT NULL,
  `CreatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`ProductReviewID`, `ProductID`, `UserID`, `Title`, `Comment`, `CreatedAt`) VALUES
(1, 34, 64, 'Review', 'This is good product', '2020-01-01 07:24:16'),
(2, 35, 64, 'Review', 'This is good product', '2020-01-01 07:24:16');

-- --------------------------------------------------------

--
-- Table structure for table `product_store_availability`
--

CREATE TABLE `product_store_availability` (
  `ProductStoreAvailability` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `StoreID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_store_availability`
--

INSERT INTO `product_store_availability` (`ProductStoreAvailability`, `ProductID`, `StoreID`, `Quantity`) VALUES
(5, 35, 4, 20),
(6, 41, 5, 0),
(7, 49, 1, 3),
(8, 49, 2, 4),
(9, 50, 1, 5),
(10, 50, 3, 6),
(11, 51, 1, 3),
(12, 51, 2, 4),
(13, 52, 1, 5),
(14, 52, 3, 6),
(15, 53, 1, 3),
(16, 53, 2, 4),
(17, 54, 1, 5),
(18, 54, 3, 6),
(19, 55, 1, 3),
(20, 55, 2, 4),
(21, 56, 1, 5),
(22, 56, 3, 6),
(23, 57, 1, 3),
(24, 57, 2, 4),
(25, 58, 1, 5),
(26, 58, 3, 6),
(27, 59, 1, 3),
(28, 59, 2, 4),
(29, 60, 1, 5),
(30, 60, 3, 6);

-- --------------------------------------------------------

--
-- Table structure for table `refunds`
--

CREATE TABLE `refunds` (
  `RefundID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `refunds_text`
--

CREATE TABLE `refunds_text` (
  `RefundTextID` int(11) NOT NULL,
  `RefundID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `RoleID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `IsActive` tinyint(4) NOT NULL,
  `CreatedBy` datetime NOT NULL,
  `UpdatedBy` datetime NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`RoleID`, `SortOrder`, `Hide`, `IsActive`, `CreatedBy`, `UpdatedBy`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 0, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-04-03 00:00:00', '2018-04-03 00:00:00'),
(2, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-02 11:37:19', '2018-05-02 12:03:34'),
(3, 2, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-04 17:33:54', '2019-12-15 12:09:26'),
(4, 3, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-02-01 10:06:52', '2019-02-01 10:06:52'),
(5, 4, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-02-20 13:50:07', '2019-02-20 13:50:07'),
(6, 5, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-04-21 20:06:37', '2019-04-21 20:06:37'),
(7, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-04-21 20:09:56', '2019-04-21 20:09:56'),
(8, 7, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-12-15 12:10:19', '2019-12-15 12:10:25');

-- --------------------------------------------------------

--
-- Table structure for table `roles_text`
--

CREATE TABLE `roles_text` (
  `RoleTextID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_text`
--

INSERT INTO `roles_text` (`RoleTextID`, `RoleID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Admin', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(2, 2, 'Store/Warehouse User', 1, '2018-05-02 11:37:19', '2018-05-02 12:03:34', 1, 1),
(4, 3, 'Driver User', 1, '2018-05-04 17:33:54', '2019-12-15 12:09:26', 1, 1),
(5, 4, 'Store Admin', 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(6, 4, 'Store Admin', 2, '2019-02-01 10:07:00', '2019-02-01 10:07:00', 1, 1),
(7, 5, 'Customer User', 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(8, 6, 'Order User', 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(9, 6, 'Order User', 2, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(10, 7, 'tet', 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(11, 7, 'tet', 2, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(12, 8, 'New Employee', 1, '2019-12-15 12:10:19', '2019-12-15 12:10:25', 1, 1),
(13, 8, 'New Employee', 2, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `search_tags`
--

CREATE TABLE `search_tags` (
  `SearchTagID` int(11) NOT NULL,
  `SearchTag` text CHARACTER SET utf8 NOT NULL,
  `UserID` int(11) NOT NULL,
  `SearchedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `search_tags`
--

INSERT INTO `search_tags` (`SearchTagID`, `SearchTag`, `UserID`, `SearchedAt`) VALUES
(11, 'rassam', 64, '2019-12-10 19:18:14'),
(10, 'coconut chocolate', 64, '2019-11-27 11:20:32'),
(12, 'cake', 1553578977, '2019-12-31 07:41:35'),
(13, 'cake', 1553578977, '2019-12-31 07:41:57'),
(14, 'cake', 1553578977, '2019-12-31 07:42:37');

-- --------------------------------------------------------

--
-- Table structure for table `site_images`
--

CREATE TABLE `site_images` (
  `SiteImageID` int(11) NOT NULL,
  `ImageType` varchar(50) NOT NULL,
  `FileID` int(11) NOT NULL,
  `ImageName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_images`
--

INSERT INTO `site_images` (`SiteImageID`, `ImageType`, `FileID`, `ImageName`) VALUES
(1, 'product', 1, 'uploads/images/2344767592720190214125630img3.png'),
(2, 'product', 1, 'uploads/images/9239674724220190214125630img5.png'),
(3, 'product', 1, 'uploads/images/3491587502620190214125630img6.png'),
(4, 'product', 2, 'uploads/images/2344767592720190214125630img3.png'),
(5, 'product', 3, 'uploads/images/2344767592720190214125630img3.png'),
(6, 'product', 4, 'uploads/images/2344767592720190214125630img3.png'),
(7, 'product', 5, 'uploads/images/2344767592720190214125630img3.png'),
(9, 'collection', 1, 'uploads/images/5799135776220190220034836banner2.png'),
(10, 'collection', 2, 'uploads/images/7859194613820190220034836ch1.png'),
(11, 'collection', 3, 'uploads/images/6996744598620190220034836ch3.png'),
(12, 'product', 12, 'uploads/images/8654564795420190319094627WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(13, 'product', 12, 'uploads/images/6767398029920190319094627WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(14, 'product', 12, 'uploads/images/7913438846620190319094627WhatsApp_Image_2019-03-11_at_11.36.57-2.jpeg'),
(15, 'product', 12, 'uploads/images/2400193184020190319094627WhatsApp_Image_2019-03-11_at_11.36.57-min.jpeg'),
(16, 'product', 12, 'uploads/images/9587317615320190319094627WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(17, 'product', 13, 'uploads/images/5661712987520190319091028WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(18, 'product', 13, 'uploads/images/4774970338320190319091028WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(19, 'product', 13, 'uploads/images/8640400517820190319091028WhatsApp_Image_2019-03-11_at_11.36.57-2.jpeg'),
(20, 'product', 13, 'uploads/images/6986129758720190319091028WhatsApp_Image_2019-03-11_at_11.36.57-min.jpeg'),
(21, 'product', 13, 'uploads/images/9193756882320190319091028WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(22, 'product', 14, 'uploads/images/1789641263720190319095731WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(23, 'product', 14, 'uploads/images/7443086141920190319095731WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(24, 'product', 14, 'uploads/images/7882017211620190319095731WhatsApp_Image_2019-03-11_at_11.36.57-2.jpeg'),
(25, 'product', 14, 'uploads/images/1367389048820190319095731WhatsApp_Image_2019-03-11_at_11.36.57-min.jpeg'),
(26, 'product', 14, 'uploads/images/8910321010220190319095731WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(27, 'product', 1, 'uploads/images/3134718524420190319092543WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(28, 'product', 15, 'uploads/images/1927226043120190319093745WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(29, 'product', 15, 'uploads/images/594730261920190319093745WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(30, 'product', 15, 'uploads/images/746558200920190319094845WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(31, 'product', 15, 'uploads/images/3901523010420190319094845WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(32, 'collection', 4, 'uploads/images/2569068038720190319091058WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(33, 'collection', 5, 'uploads/images/6493461562920190319103105WhatsApp_Image_2019-03-11_at_11.36.57-2.jpeg'),
(34, 'collection', 6, 'uploads/images/2147949252820190319104906WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(35, 'collection', 7, 'uploads/images/9946759725120190319103425WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(36, 'collection', 1, 'uploads/images/6082116845620190326034946cp10.png'),
(37, 'collection', 3, 'uploads/images/9619593784020190326032451ch5.png'),
(38, 'product', 12, 'uploads/images/15617416295201903310422201ch3.png'),
(39, 'product', 16, 'uploads/images/30260719580201903310411211ch3.png'),
(40, 'collection', 8, 'uploads/images/11038782355201903310416327859194613820190220034836ch1.png'),
(41, 'collection', 9, 'uploads/images/3562575277020190331045334ch2.png'),
(42, 'collection', 10, 'uploads/images/4915248640720190401100622ch5.png'),
(44, 'collection', 12, 'uploads/images/2178378257820190402094614WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(45, 'collection', 13, 'uploads/images/8442736936420190402090215WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(48, 'collection', 11, 'uploads/images/4404786574920190402093523img4.png'),
(49, 'collection', 11, 'uploads/images/6100591700120190402093523img5.png'),
(52, 'product', 6, 'uploads/images/99278711879201904031131306.png'),
(53, 'collection', 14, 'uploads/images/1562898902201904031121316.png'),
(54, 'EmailFile', 6, 'assets/backend/images/no_image.jpg'),
(55, 'product', 17, 'uploads/images/42666493920190410043726ch5.png'),
(56, 'product', 10, 'uploads/images/2344767592720190214125630img3.png'),
(58, 'product', 18, 'uploads/images/113077851222019042701422530260719580201903310411211ch3.png'),
(59, 'product', 19, 'uploads/images/2384869090820190430060904Depositphotos_1314091_ds.jpg'),
(60, 'product', 20, 'uploads/images/7414286665220190501081647image001.png'),
(61, 'product', 21, 'uploads/images/9514313584320190501081548image005.png'),
(62, 'product', 22, 'uploads/images/7217078571020190501080050image009.png'),
(63, 'product', 23, 'uploads/images/5599001587820190501083551image011.png'),
(64, 'product', 24, 'uploads/images/9542100573120190501082053image409.png'),
(65, 'product', 25, 'uploads/images/2072127942120190501080159image361.png'),
(66, 'product', 26, 'uploads/images/8307434168320190501094800image363.png'),
(67, 'product', 27, 'uploads/images/3706199802220190501090802image391.png'),
(68, 'product', 28, 'uploads/images/4540537429120190501092303image111.png'),
(69, 'product', 29, 'uploads/images/8478116233220190501092107image027.png'),
(70, 'product', 30, 'uploads/images/6131364861820190501093608image154.png'),
(71, 'product', 31, 'uploads/images/5314166646520190501091709image147.png'),
(72, 'EmailFile', 7, 'assets/backend/images/no_image.jpg'),
(73, 'product', 32, 'uploads/images/103224682412019112711390686_n.jpg'),
(74, 'collection', 15, 'uploads/images/763457818792019112711141273134093_159953508412574_8729455205950797333_n.jpg'),
(75, 'collection', 16, 'uploads/images/242144192442019112711581372489197_141376287210012_4937717711887326865_n.jpg'),
(76, 'collection', 17, 'uploads/images/486683868902019112711411586_n.jpg'),
(77, 'collection', 18, 'uploads/images/518485794022019112711511772278841_1015894295423891_405066893956981627_n.jpg'),
(78, 'product', 33, 'uploads/images/9018959715320191127112631MIC_0415.jpg'),
(79, 'product', 33, 'uploads/images/8338483444720191127112631MIC_0420.jpg'),
(80, 'product', 33, 'uploads/images/1071768085020191127112631MIC_0422.jpg'),
(81, 'product', 33, 'uploads/images/9980983777920191127112631MIC_0426.jpg'),
(82, 'product', 33, 'uploads/images/5770069657920191127112631MIC_0429.jpg'),
(83, 'product', 33, 'uploads/images/3828564968520191127112631MIC_0432_copy.jpg'),
(85, 'product', 35, 'uploads/images/11514054984201911301247153f7656bc-a860-4341-b3e1-cbd8535171aa_(1).jpg'),
(86, 'product', 36, 'uploads/images/586559584722019113012052189dced02-057e-4a76-9922-d8f53b52a0a4.jpg'),
(88, 'collection', 20, 'uploads/images/1267362765520191130122446belize-wedding-vacation-packages.jpg'),
(90, 'collection', 21, 'uploads/images/9561003988720191202114531h.jpg'),
(91, 'collection', 22, 'uploads/images/4627132505920191202111832y.jpg'),
(92, 'collection', 23, 'uploads/images/30784683517201912021120348596770922320191130120452aj3enycveq98cmuyjb.jpg'),
(93, 'product', 34, 'uploads/images/4105817803920191204113827four.jpg'),
(94, 'product', 34, 'uploads/images/2331073380420191204113827one.jpg'),
(95, 'product', 34, 'uploads/images/814017012020191204113827three.jpg'),
(96, 'product', 34, 'uploads/images/5896763070920191204113827two.jpg'),
(97, 'collection', 24, 'uploads/images/648660360112019121511224801_choco.jpg'),
(98, 'collection', 19, 'uploads/images/639948335152019121511015001_choco.jpg'),
(99, 'product', 42, 'uploads/images/928468098762020021010333072124360_2558790247717603_7384555194358431744_n.jpg'),
(100, 'product', 43, 'uploads/images/299058752812020021001332181898777_2460926880890961_7466254659263397888_n.jpg'),
(101, 'product', 44, 'uploads/images/311329206242020021001075372124360_2558790247717603_7384555194358431744_n.jpg'),
(102, 'product', 45, 'uploads/images/110078636042020021001045684212650_115903443280448_6658839702839230464_o.jpg'),
(103, 'product', 46, 'uploads/images/990975995972020021212193072124360_2558790247717603_7384555194358431744_n.jpg'),
(104, 'product', 61, 'uploads/images/104353037942020030405523672124360_2558790247717603_7384555194358431744_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `SiteSettingID` int(11) NOT NULL,
  `SiteName` varchar(255) NOT NULL,
  `SiteImage` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Whatsapp` varchar(255) NOT NULL,
  `Skype` varchar(255) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `Fax` varchar(255) NOT NULL,
  `OpenTime` varchar(255) NOT NULL,
  `CloseTime` varchar(255) NOT NULL,
  `FacebookUrl` varchar(255) NOT NULL,
  `GoogleUrl` varchar(255) NOT NULL,
  `LinkedInUrl` varchar(255) NOT NULL,
  `TwitterUrl` varchar(255) NOT NULL,
  `InstagramUrl` varchar(255) NOT NULL,
  `YoutubeUrl` varchar(255) NOT NULL,
  `VatNumber` varchar(255) NOT NULL,
  `VatPercentage` decimal(10,2) NOT NULL,
  `LoyaltyFactor` int(11) NOT NULL COMMENT 'Defines 1 SAR is equal to how many loyalty points',
  `DaysToDeliver` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`SiteSettingID`, `SiteName`, `SiteImage`, `Email`, `Whatsapp`, `Skype`, `PhoneNumber`, `Fax`, `OpenTime`, `CloseTime`, `FacebookUrl`, `GoogleUrl`, `LinkedInUrl`, `TwitterUrl`, `InstagramUrl`, `YoutubeUrl`, `VatNumber`, `VatPercentage`, `LoyaltyFactor`, `DaysToDeliver`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 'Chocomood', 'uploads/5602750077020191126043459g.jpg', 'test@cms.com', '123456789', '', '9200 21 211', '', '1545300000', '1545332400', 'https://www.facebook.com', '', '', 'https://www.twitter.com', '', '', '0000000000000000', '0.00', 20, 2, '2018-04-03 14:33:23', '2019-11-26 16:59:34');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `StoreID` int(11) NOT NULL,
  `CityID` int(11) NOT NULL,
  `DistrictID` varchar(255) NOT NULL,
  `Latitude` double NOT NULL,
  `Longitude` double NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`StoreID`, `CityID`, `DistrictID`, `Latitude`, `Longitude`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(4, 113, '13', 21.521503722412003, 39.160286773258235, 0, 0, 1, '2019-11-27 00:11:07', '2019-12-10 12:21:39', 1, 1),
(5, 77, '1,2,3,4,5,11', 21.484716, 39.189606, 1, 0, 1, '2020-02-11 08:05:15', '2020-02-11 08:05:15', 1, 1),
(6, 113, '13', 21.521503722412003, 39.160286773258235, 0, 0, 1, '2019-11-27 00:11:07', '2019-12-10 12:21:39', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stores_text`
--

CREATE TABLE `stores_text` (
  `StoreTextID` int(11) NOT NULL,
  `StoreID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Address` text NOT NULL,
  `WorkingHours` text NOT NULL,
  `DeliveryTime` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores_text`
--

INSERT INTO `stores_text` (`StoreTextID`, `StoreID`, `Title`, `Address`, `WorkingHours`, `DeliveryTime`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(7, 4, 'Jeddah Branch', 'Falastin, Al-Hamra\'a, Al, Jeddah 23212', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', 1, '2019-11-27 00:11:07', '2019-12-10 12:21:39', 1, 1),
(8, 4, 'Jeddah Branch', 'Falastin, Al-Hamra\'a, Al, Jeddah 23212', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', 2, '2019-11-27 00:11:07', '2019-11-27 00:11:07', 1, 1),
(9, 5, 'test', 'This is testing by sarfraz\r\nThis is testing by sarfraz\r\nThis is testing by sarfraz', '12', '', 1, '2020-02-11 08:05:15', '2020-02-11 08:05:15', 1, 1),
(10, 5, 'test', 'This is testing by sarfraz\r\nThis is testing by sarfraz\r\nThis is testing by sarfraz', '12', '', 2, '2020-02-11 08:05:15', '2020-02-11 08:05:15', 1, 1),
(11, 6, 'Jeddah Branch 2', 'Falastin, Al-Hamra\'a, Al, Jeddah 23212', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', 1, '2019-11-27 00:11:07', '2019-12-10 12:21:39', 1, 1),
(12, 6, 'Jeddah Branch2', 'Falastin, Al-Hamra\'a, Al, Jeddah 23212', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', 2, '2019-11-27 00:11:07', '2019-11-27 00:11:07', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_languages`
--

CREATE TABLE `system_languages` (
  `SystemLanguageID` int(11) NOT NULL,
  `SystemLanguageTitle` varchar(255) NOT NULL,
  `ShortCode` varchar(255) NOT NULL,
  `IsDefault` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_languages`
--

INSERT INTO `system_languages` (`SystemLanguageID`, `SystemLanguageTitle`, `ShortCode`, `IsDefault`, `IsActive`, `Hide`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'English', 'EN', 1, 0, 0, '2018-04-09 00:00:00', '2018-07-04 16:09:38', 0, 1),
(2, 'Arabic', 'AR', 0, 0, 0, '2018-04-09 00:00:00', '2018-07-04 16:07:19', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `TagID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`TagID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 0, 1, '2020-03-04 16:41:11', '2020-03-04 16:41:11', 1, 1),
(2, 1, 0, 1, '2020-03-04 16:41:25', '2020-03-04 16:41:25', 1, 1),
(3, 2, 0, 1, '2020-03-04 16:41:37', '2020-03-04 16:41:37', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags_text`
--

CREATE TABLE `tags_text` (
  `TagTextID` int(11) NOT NULL,
  `TagID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags_text`
--

INSERT INTO `tags_text` (`TagTextID`, `TagID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Chocolate', 1, '2020-03-04 16:41:11', '2020-03-04 16:41:11', 1, 1),
(2, 1, 'Chocolate', 2, '2020-03-04 16:41:11', '2020-03-04 16:41:11', 1, 1),
(3, 2, 'tofi', 1, '2020-03-04 16:41:25', '2020-03-04 16:41:25', 1, 1),
(4, 2, 'tofi', 2, '2020-03-04 16:41:25', '2020-03-04 16:41:25', 1, 1),
(5, 3, 'cool', 1, '2020-03-04 16:41:37', '2020-03-04 16:41:37', 1, 1),
(6, 3, 'cool', 2, '2020-03-04 16:41:37', '2020-03-04 16:41:37', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tax_shipment_charges`
--

CREATE TABLE `tax_shipment_charges` (
  `TaxShipmentChargesID` int(11) NOT NULL,
  `ChargesType` enum('Tax','Shipment') NOT NULL DEFAULT 'Tax',
  `Amount` double NOT NULL,
  `Type` enum('Fixed','Percentage') NOT NULL DEFAULT 'Fixed',
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `IsDefault` int(11) NOT NULL COMMENT 'Only to be used for shipment methods',
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_shipment_charges`
--

INSERT INTO `tax_shipment_charges` (`TaxShipmentChargesID`, `ChargesType`, `Amount`, `Type`, `Image`, `SortOrder`, `Hide`, `IsActive`, `IsDefault`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 'Shipment', 500, 'Fixed', 'uploads/images/2656939121320190314022108logo.png', 2, 0, 1, 0, '2019-03-14 14:08:21', '2019-03-14 14:08:21', 1, 1),
(1, 'Shipment', 20, 'Fixed', '', 0, 0, 1, 1, '2019-02-25 13:42:23', '2019-02-25 13:56:12', 1, 1),
(2, 'Tax', 5, 'Percentage', '', 1, 0, 1, 0, '2019-02-28 14:45:36', '2019-02-28 14:45:36', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tax_shipment_charges_text`
--

CREATE TABLE `tax_shipment_charges_text` (
  `TaxShipmentChargesTextID` int(11) NOT NULL,
  `TaxShipmentChargesID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_shipment_charges_text`
--

INSERT INTO `tax_shipment_charges_text` (`TaxShipmentChargesTextID`, `TaxShipmentChargesID`, `Title`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(5, 3, 'Express Delivery', 'This is 2nd shipment method', 1, '2019-03-14 14:08:21', '2019-03-14 14:08:21', 1, 1),
(4, 2, 'VAT', 'This is VAT', 2, '2019-02-28 14:45:55', '2019-02-28 14:45:55', 1, 1),
(3, 2, 'VAT', 'This is VAT', 1, '2019-02-28 14:45:36', '2019-02-28 14:45:36', 1, 1),
(2, 1, 'Standard Shipping', 'This is basic shipment charges', 2, '2019-02-25 13:55:44', '2019-02-25 13:55:44', 1, 1),
(1, 1, 'Standard Shipping', 'This is basic shipment charges', 1, '2019-02-25 13:42:23', '2019-02-25 13:56:12', 1, 1),
(6, 3, 'Express Delivery', 'This is 2nd shipment method', 2, '2019-03-14 14:08:21', '2019-03-14 14:08:21', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `temp_orders`
--

CREATE TABLE `temp_orders` (
  `TempOrderID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL COMMENT 'If ItemType is product then this column has Product ID else if ItemType column has Collection then its Collection ID',
  `Quantity` int(11) NOT NULL,
  `TempItemPrice` decimal(10,2) NOT NULL,
  `ItemType` enum('Product','Collection','Chocobox','Choco Message','Customized Shape') NOT NULL,
  `CustomizedOrderProductIDs` text NOT NULL COMMENT 'It will only be having values in case of choco box and choco msg',
  `CustomizedShapeImage` text NOT NULL COMMENT 'This will be having value if Item Type is customized shape',
  `CustomizedBoxID` int(11) NOT NULL COMMENT 'This will be having values only for choco message and choco box item types',
  `IsCorporateItem` int(11) NOT NULL,
  `Ribbon` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_orders`
--

INSERT INTO `temp_orders` (`TempOrderID`, `UserID`, `ProductID`, `Quantity`, `TempItemPrice`, `ItemType`, `CustomizedOrderProductIDs`, `CustomizedShapeImage`, `CustomizedBoxID`, `IsCorporateItem`, `Ribbon`) VALUES
(23, 1574519013, 34, 2, '65.00', 'Product', '', '', 0, 0, ''),
(20, 1575287917, 34, 2, '65.00', 'Product', '', '', 0, 0, ''),
(19, 1575287917, 36, 1, '999.00', 'Product', '', '', 0, 1, ''),
(16, 1575101564, 34, 1, '65.00', 'Product', '', '', 0, 0, ''),
(36, 64, 35, 1, '34.00', 'Product', '', '', 0, 0, ''),
(35, 64, 35, 1, '34.00', 'Product', '', '', 0, 0, ''),
(34, 64, 34, 1, '65.00', 'Product', '', '', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `TicketID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `TicketNumber` varchar(255) NOT NULL,
  `IsClosed` int(11) NOT NULL COMMENT '0 means ongoing, 1 means closed ticket, 2 means reopened',
  `CreatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`TicketID`, `OrderID`, `TicketNumber`, `IsClosed`, `CreatedAt`) VALUES
(4, 10, '00004', 0, '2019-12-10 19:10:12');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_comments`
--

CREATE TABLE `ticket_comments` (
  `CommentID` int(11) NOT NULL,
  `TicketID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Message` text CHARACTER SET utf8 NOT NULL,
  `IsRead` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_comments`
--

INSERT INTO `ticket_comments` (`CommentID`, `TicketID`, `UserID`, `Message`, `IsRead`, `CreatedAt`) VALUES
(7, 4, 64, 'hello', 1, '2019-12-10 19:10:18'),
(6, 4, 0, 'Welcome to chocomood support. How may I help you?<br>مرحبا بكم في الدعم. كيف يمكنني مساعدتك؟', 1, '2019-12-10 19:10:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `StoreID` int(11) NOT NULL,
  `Mobile` varchar(255) NOT NULL,
  `CityID` int(11) DEFAULT NULL,
  `DistrictID` varchar(255) NOT NULL,
  `UserType` enum('','individual','company') NOT NULL,
  `Code` int(4) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Gender` enum('Male','Female') DEFAULT NULL,
  `SecondaryEmail` varchar(255) NOT NULL,
  `CompressedImage` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `DateOfBirth` date NOT NULL,
  `Image` varchar(500) NOT NULL,
  `DeviceType` varchar(50) NOT NULL,
  `DeviceToken` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  `Notification` enum('on','off') NOT NULL DEFAULT 'on',
  `OnlineStatus` enum('Offline','Online') NOT NULL DEFAULT 'Offline',
  `LastUnsuccessfulLogin` datetime NOT NULL,
  `TempUserKey` text NOT NULL,
  `CompanyName` varchar(255) NOT NULL,
  `LoyaltyPoints` int(11) NOT NULL,
  `TermsAccepted` tinyint(4) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `CreatedAt` varchar(255) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedAt` varchar(255) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `RoleID`, `StoreID`, `Mobile`, `CityID`, `DistrictID`, `UserType`, `Code`, `Email`, `Gender`, `SecondaryEmail`, `CompressedImage`, `Password`, `DateOfBirth`, `Image`, `DeviceType`, `DeviceToken`, `Hide`, `IsActive`, `Notification`, `OnlineStatus`, `LastUnsuccessfulLogin`, `TempUserKey`, `CompanyName`, `LoyaltyPoints`, `TermsAccepted`, `SortOrder`, `CreatedAt`, `CreatedBy`, `UpdatedAt`, `UpdatedBy`) VALUES
(1, 1, 0, '', 1, '', '', 0, 'admin@chocomood.com', '', '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 'on', 'Offline', '2020-02-13 19:00:35', '', '', 5000, 0, 0, '1543388192', 0, '2018-05-09 13:35:18', 1),
(64, 5, 0, '+966 23456789', 77, '', '', 0, 'sarfraz.cs10@gmail.com', NULL, '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 'on', 'Online', '2019-12-08 14:26:52', '1553578977', '', 0, 1, 0, '', 0, '', 0),
(71, 5, 0, '+966541579273', 77, '', '', 0, 'taha@zynq.net', NULL, '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 'on', 'Online', '0000-00-00 00:00:00', '1573112146', '', 0, 1, 0, '', 0, '', 0),
(72, 4, 5, '+923008094941', 77, '', '', 0, 'admin@store.com', NULL, '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 'on', 'Offline', '0000-00-00 00:00:00', '', '', 0, 0, 1, '2020-02-14 08:58:00', 1, '2020-02-14 08:58:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_text`
--

CREATE TABLE `users_text` (
  `UserTextID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `FullName` varchar(255) NOT NULL,
  `Bio` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_text`
--

INSERT INTO `users_text` (`UserTextID`, `UserID`, `FullName`, `Bio`, `SystemLanguageID`) VALUES
(6, 1, 'Admin', '', 1),
(196, 64, 'Taha Haider', '', 1),
(198, 66, 'driver', '', 1),
(199, 66, 'driver', '', 2),
(200, 67, 'driver2', '', 1),
(201, 67, 'driver', '', 2),
(202, 68, 'Jeddah Branch 2', '', 1),
(203, 68, 'Jeddah Branch 2', '', 2),
(204, 69, 'driver3', '', 1),
(205, 69, 'driver3', '', 2),
(206, 70, 'az', '', 1),
(207, 70, 'az', '', 2),
(208, 71, 'Taha haider', '', 1),
(209, 72, 'sarfraz', '', 1),
(210, 72, 'sarfraz', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `AddressID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `RecipientName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `MobileNo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(255) NOT NULL,
  `CityID` int(11) NOT NULL,
  `DistrictID` int(11) NOT NULL,
  `BuildingNo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Street` varchar(255) CHARACTER SET utf8 NOT NULL,
  `POBox` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ZipCode` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Latitude` double NOT NULL,
  `Longitude` double NOT NULL,
  `AddressType` varchar(255) NOT NULL,
  `IsDefault` int(11) NOT NULL,
  `UseForPaymentCollection` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`AddressID`, `UserID`, `RecipientName`, `MobileNo`, `Email`, `CityID`, `DistrictID`, `BuildingNo`, `Street`, `POBox`, `ZipCode`, `Latitude`, `Longitude`, `AddressType`, `IsDefault`, `UseForPaymentCollection`) VALUES
(2, 65, 'Taha Haider', '+966541579273', 'sarfraz.cs10@gmail.com', 113, 12, '1', 'Office 69, Al Qimma business center, baladiyah street', '23815', '23815', 21.484716, 39.189606, '', 1, 1),
(4, 64, 'Rami', '0508074066', 'extension@o2.pl', 113, 12, '14', 'test', '13223', '1234', 0, 0, '', 0, 1),
(5, 64, 'Sarfraz Riaz u', '+92 300 8094942', 'sarfraz.cs11@gmail.com', 113, 12, 'This is testing by sarfraz', 'This is testing by sarfraz, This is testing by sarfraz, This is testing by sarfraz', '54000', '54000', 21.423365408460356, 39.8982241640625, 'Work', 1, 0),
(6, 64, 'Sarfraz Riaz', '+923008094941', 'sarfraz.cs10@gmail.com', 77, 1, 'This is testing by sarfraz', 'This is testing by sarfraz, This is testing by sarfraz, This is testing by sarfraz', '54000', '54000', 21.617553537157328, 39.5576479921875, 'Work', 0, 0),
(7, 64, 'Sarfraz Riaz', '+923008094941', 'sarfraz.cs10@gmail.com', 77, 1, 'This is testing by sarfraz', 'This is testing by sarfraz, This is testing by sarfraz, This is testing by sarfraz', '54000', '54000', 21.617553537157328, 39.5576479921875, 'Work', 0, 0),
(8, 64, 'Sarfraz Riaz', '+923008094941', 'sarfraz.cs10@gmail.com', 77, 1, 'This is testing by sarfraz', 'This is testing by sarfraz, This is testing by sarfraz, This is testing by sarfraz', '54000', '54000', 21.617553537157328, 39.5576479921875, 'Work', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `UserNotificationID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `NotificationType` varchar(255) NOT NULL,
  `NotificationTitleEn` varchar(255) CHARACTER SET utf8 NOT NULL,
  `NotificationTextEn` text CHARACTER SET utf16 NOT NULL,
  `NotificationTitleAr` varchar(255) CHARACTER SET utf8 NOT NULL,
  `NotificationTextAr` text CHARACTER SET utf8 NOT NULL,
  `BookingID` int(11) NOT NULL,
  `NotificationCreatedAt` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_wishlist`
--

CREATE TABLE `user_wishlist` (
  `WishlistID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ItemID` int(11) NOT NULL,
  `ItemType` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_wishlist`
--

INSERT INTO `user_wishlist` (`WishlistID`, `UserID`, `ItemID`, `ItemType`) VALUES
(2, 64, 34, 'Product');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boxes`
--
ALTER TABLE `boxes`
  ADD PRIMARY KEY (`BoxID`);

--
-- Indexes for table `boxes_text`
--
ALTER TABLE `boxes_text`
  ADD PRIMARY KEY (`BoxTextID`),
  ADD KEY `boxes_text_BoxID_fk` (`BoxID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`CategoryID`);

--
-- Indexes for table `categories_text`
--
ALTER TABLE `categories_text`
  ADD PRIMARY KEY (`CategoryTextID`),
  ADD KEY `categories_text_CategoryID_fk` (`CategoryID`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`CityID`),
  ADD KEY `CityID` (`CityID`);

--
-- Indexes for table `cities_text`
--
ALTER TABLE `cities_text`
  ADD PRIMARY KEY (`CityTextID`),
  ADD KEY `FK_cities_text` (`CityID`),
  ADD KEY `CityTextID` (`CityTextID`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`CollectionID`);

--
-- Indexes for table `collections_text`
--
ALTER TABLE `collections_text`
  ADD PRIMARY KEY (`CollectionTextID`),
  ADD KEY `collections_text_CollectionID_fk` (`CollectionID`);

--
-- Indexes for table `collection_ratings`
--
ALTER TABLE `collection_ratings`
  ADD PRIMARY KEY (`CollectionRatingID`);

--
-- Indexes for table `collection_reviews`
--
ALTER TABLE `collection_reviews`
  ADD PRIMARY KEY (`CollectionReviewID`);

--
-- Indexes for table `contact_requests`
--
ALTER TABLE `contact_requests`
  ADD PRIMARY KEY (`ContactRequestID`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`CouponID`);

--
-- Indexes for table `coupons_text`
--
ALTER TABLE `coupons_text`
  ADD PRIMARY KEY (`CouponTextID`),
  ADD KEY `coupons_text_CouponID_fk` (`CouponID`);

--
-- Indexes for table `customer_groups`
--
ALTER TABLE `customer_groups`
  ADD PRIMARY KEY (`CustomerGroupID`);

--
-- Indexes for table `customer_group_members`
--
ALTER TABLE `customer_group_members`
  ADD PRIMARY KEY (`CustomerGroupMemberID`);

--
-- Indexes for table `customizes`
--
ALTER TABLE `customizes`
  ADD PRIMARY KEY (`CustomizeID`);

--
-- Indexes for table `customizes_text`
--
ALTER TABLE `customizes_text`
  ADD PRIMARY KEY (`CustomizeTextID`),
  ADD KEY `customizes_text_CustomizeID_fk` (`CustomizeID`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`DistrictID`),
  ADD KEY `DistrictID` (`DistrictID`),
  ADD KEY `CityID` (`CityID`);

--
-- Indexes for table `districts_text`
--
ALTER TABLE `districts_text`
  ADD PRIMARY KEY (`DistrictTextID`),
  ADD KEY `DistrictID` (`DistrictID`),
  ADD KEY `DistrictTextID` (`DistrictTextID`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`Email_templateID`);

--
-- Indexes for table `email_templates_text`
--
ALTER TABLE `email_templates_text`
  ADD PRIMARY KEY (`Email_templateTextID`),
  ADD KEY `email_templates_text_Email_templateID_fk` (`Email_templateID`);

--
-- Indexes for table `home_slider_images`
--
ALTER TABLE `home_slider_images`
  ADD PRIMARY KEY (`HomeSliderImageID`);

--
-- Indexes for table `home_slider_images_text`
--
ALTER TABLE `home_slider_images_text`
  ADD PRIMARY KEY (`HomeSliderImageTextID`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`LanguageID`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`ModuleID`);

--
-- Indexes for table `modules_rights`
--
ALTER TABLE `modules_rights`
  ADD PRIMARY KEY (`ModuleRightID`),
  ADD KEY `fk_ModuleID` (`ModuleID`);

--
-- Indexes for table `modules_text`
--
ALTER TABLE `modules_text`
  ADD PRIMARY KEY (`ModuleTextID`),
  ADD KEY `ModuleID` (`ModuleID`);

--
-- Indexes for table `modules_users_rights`
--
ALTER TABLE `modules_users_rights`
  ADD PRIMARY KEY (`ModuleRightID`),
  ADD KEY `fk_ModuleID` (`ModuleID`);

--
-- Indexes for table `nutritions`
--
ALTER TABLE `nutritions`
  ADD PRIMARY KEY (`NutritionID`);

--
-- Indexes for table `nutritions_text`
--
ALTER TABLE `nutritions_text`
  ADD PRIMARY KEY (`NutritionTextID`),
  ADD KEY `nutritions_text_NutritionID_fk` (`NutritionID`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`OfferID`);

--
-- Indexes for table `offers_groups`
--
ALTER TABLE `offers_groups`
  ADD PRIMARY KEY (`OfferGroupID`);

--
-- Indexes for table `offers_text`
--
ALTER TABLE `offers_text`
  ADD PRIMARY KEY (`OfferTextID`);

--
-- Indexes for table `offers_users_notification`
--
ALTER TABLE `offers_users_notification`
  ADD PRIMARY KEY (`OfferUserNotificationID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrderID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `OrderNumber` (`OrderNumber`),
  ADD KEY `AddressID` (`AddressID`),
  ADD KEY `AddressIDForPaymentCollection` (`AddressIDForPaymentCollection`),
  ADD KEY `ShipmentMethodID` (`ShipmentMethodID`),
  ADD KEY `Status` (`Status`),
  ADD KEY `DriverID` (`DriverID`),
  ADD KEY `OrderID` (`OrderID`);

--
-- Indexes for table `orders_extra_charges`
--
ALTER TABLE `orders_extra_charges`
  ADD PRIMARY KEY (`OrderExtraChargeID`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`OrderItemID`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`OrderStatusID`),
  ADD KEY `OrderStatusID` (`OrderStatusID`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`PageID`);

--
-- Indexes for table `pages_text`
--
ALTER TABLE `pages_text`
  ADD PRIMARY KEY (`PageTextID`),
  ADD KEY `pages_text_PageID_fk` (`PageID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `products_inside`
--
ALTER TABLE `products_inside`
  ADD PRIMARY KEY (`ProductInsideID`);

--
-- Indexes for table `products_text`
--
ALTER TABLE `products_text`
  ADD PRIMARY KEY (`ProducrTextID`),
  ADD KEY `producrs_text_ProducrID_fk` (`ProductID`);

--
-- Indexes for table `product_nutritions`
--
ALTER TABLE `product_nutritions`
  ADD PRIMARY KEY (`NutritionProductID`);

--
-- Indexes for table `product_ratings`
--
ALTER TABLE `product_ratings`
  ADD PRIMARY KEY (`ProductRatingID`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`ProductReviewID`);

--
-- Indexes for table `product_store_availability`
--
ALTER TABLE `product_store_availability`
  ADD PRIMARY KEY (`ProductStoreAvailability`);

--
-- Indexes for table `refunds`
--
ALTER TABLE `refunds`
  ADD PRIMARY KEY (`RefundID`);

--
-- Indexes for table `refunds_text`
--
ALTER TABLE `refunds_text`
  ADD PRIMARY KEY (`RefundTextID`),
  ADD KEY `refunds_text_RefundID_fk` (`RefundID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`RoleID`);

--
-- Indexes for table `roles_text`
--
ALTER TABLE `roles_text`
  ADD PRIMARY KEY (`RoleTextID`),
  ADD KEY `RoleID` (`RoleID`);

--
-- Indexes for table `search_tags`
--
ALTER TABLE `search_tags`
  ADD PRIMARY KEY (`SearchTagID`);

--
-- Indexes for table `site_images`
--
ALTER TABLE `site_images`
  ADD PRIMARY KEY (`SiteImageID`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`SiteSettingID`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`StoreID`);

--
-- Indexes for table `stores_text`
--
ALTER TABLE `stores_text`
  ADD PRIMARY KEY (`StoreTextID`),
  ADD KEY `stores_text_StoreID_fk` (`StoreID`);

--
-- Indexes for table `system_languages`
--
ALTER TABLE `system_languages`
  ADD PRIMARY KEY (`SystemLanguageID`),
  ADD KEY `SystemLanguageID` (`SystemLanguageID`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`TagID`);

--
-- Indexes for table `tags_text`
--
ALTER TABLE `tags_text`
  ADD PRIMARY KEY (`TagTextID`),
  ADD KEY `tags_text_TagID_fk` (`TagID`);

--
-- Indexes for table `tax_shipment_charges`
--
ALTER TABLE `tax_shipment_charges`
  ADD PRIMARY KEY (`TaxShipmentChargesID`),
  ADD KEY `TaxShipmentChargesID` (`TaxShipmentChargesID`);

--
-- Indexes for table `tax_shipment_charges_text`
--
ALTER TABLE `tax_shipment_charges_text`
  ADD PRIMARY KEY (`TaxShipmentChargesTextID`),
  ADD KEY `taxShippingChargeses_text_TaxShippingChargesID_fk` (`TaxShipmentChargesID`),
  ADD KEY `TaxShipmentChargesTextID` (`TaxShipmentChargesTextID`);

--
-- Indexes for table `temp_orders`
--
ALTER TABLE `temp_orders`
  ADD PRIMARY KEY (`TempOrderID`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`TicketID`),
  ADD KEY `TicketID` (`TicketID`),
  ADD KEY `OrderID` (`OrderID`),
  ADD KEY `TicketNumber` (`TicketNumber`);

--
-- Indexes for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  ADD PRIMARY KEY (`CommentID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`),
  ADD KEY `users_ibfk_1` (`RoleID`),
  ADD KEY `StoreID` (`StoreID`),
  ADD KEY `CityID` (`CityID`),
  ADD KEY `DistrictID` (`DistrictID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `users_text`
--
ALTER TABLE `users_text`
  ADD PRIMARY KEY (`UserTextID`),
  ADD KEY `RoleID` (`UserID`),
  ADD KEY `UserTextID` (`UserTextID`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`AddressID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `CityID` (`CityID`),
  ADD KEY `DistrictID` (`DistrictID`),
  ADD KEY `AddressID` (`AddressID`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`UserNotificationID`);

--
-- Indexes for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  ADD PRIMARY KEY (`WishlistID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boxes`
--
ALTER TABLE `boxes`
  MODIFY `BoxID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `boxes_text`
--
ALTER TABLE `boxes_text`
  MODIFY `BoxTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `CategoryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `categories_text`
--
ALTER TABLE `categories_text`
  MODIFY `CategoryTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `CityID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `cities_text`
--
ALTER TABLE `cities_text`
  MODIFY `CityTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `CollectionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `collections_text`
--
ALTER TABLE `collections_text`
  MODIFY `CollectionTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `collection_ratings`
--
ALTER TABLE `collection_ratings`
  MODIFY `CollectionRatingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `collection_reviews`
--
ALTER TABLE `collection_reviews`
  MODIFY `CollectionReviewID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_requests`
--
ALTER TABLE `contact_requests`
  MODIFY `ContactRequestID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `CouponID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `coupons_text`
--
ALTER TABLE `coupons_text`
  MODIFY `CouponTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customer_groups`
--
ALTER TABLE `customer_groups`
  MODIFY `CustomerGroupID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer_group_members`
--
ALTER TABLE `customer_group_members`
  MODIFY `CustomerGroupMemberID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customizes`
--
ALTER TABLE `customizes`
  MODIFY `CustomizeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customizes_text`
--
ALTER TABLE `customizes_text`
  MODIFY `CustomizeTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `DistrictID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `districts_text`
--
ALTER TABLE `districts_text`
  MODIFY `DistrictTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `Email_templateID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `email_templates_text`
--
ALTER TABLE `email_templates_text`
  MODIFY `Email_templateTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `home_slider_images`
--
ALTER TABLE `home_slider_images`
  MODIFY `HomeSliderImageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_slider_images_text`
--
ALTER TABLE `home_slider_images_text`
  MODIFY `HomeSliderImageTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `LanguageID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `ModuleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `modules_rights`
--
ALTER TABLE `modules_rights`
  MODIFY `ModuleRightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=525;

--
-- AUTO_INCREMENT for table `modules_text`
--
ALTER TABLE `modules_text`
  MODIFY `ModuleTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `modules_users_rights`
--
ALTER TABLE `modules_users_rights`
  MODIFY `ModuleRightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1562;

--
-- AUTO_INCREMENT for table `nutritions`
--
ALTER TABLE `nutritions`
  MODIFY `NutritionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nutritions_text`
--
ALTER TABLE `nutritions_text`
  MODIFY `NutritionTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `OfferID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `offers_groups`
--
ALTER TABLE `offers_groups`
  MODIFY `OfferGroupID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers_text`
--
ALTER TABLE `offers_text`
  MODIFY `OfferTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `offers_users_notification`
--
ALTER TABLE `offers_users_notification`
  MODIFY `OfferUserNotificationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `orders_extra_charges`
--
ALTER TABLE `orders_extra_charges`
  MODIFY `OrderExtraChargeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `OrderItemID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `OrderStatusID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `PageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pages_text`
--
ALTER TABLE `pages_text`
  MODIFY `PageTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `products_inside`
--
ALTER TABLE `products_inside`
  MODIFY `ProductInsideID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `products_text`
--
ALTER TABLE `products_text`
  MODIFY `ProducrTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `product_nutritions`
--
ALTER TABLE `product_nutritions`
  MODIFY `NutritionProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `product_ratings`
--
ALTER TABLE `product_ratings`
  MODIFY `ProductRatingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `ProductReviewID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_store_availability`
--
ALTER TABLE `product_store_availability`
  MODIFY `ProductStoreAvailability` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `refunds`
--
ALTER TABLE `refunds`
  MODIFY `RefundID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `refunds_text`
--
ALTER TABLE `refunds_text`
  MODIFY `RefundTextID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `RoleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles_text`
--
ALTER TABLE `roles_text`
  MODIFY `RoleTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `search_tags`
--
ALTER TABLE `search_tags`
  MODIFY `SearchTagID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `site_images`
--
ALTER TABLE `site_images`
  MODIFY `SiteImageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `SiteSettingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `StoreID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stores_text`
--
ALTER TABLE `stores_text`
  MODIFY `StoreTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `system_languages`
--
ALTER TABLE `system_languages`
  MODIFY `SystemLanguageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `TagID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tags_text`
--
ALTER TABLE `tags_text`
  MODIFY `TagTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tax_shipment_charges`
--
ALTER TABLE `tax_shipment_charges`
  MODIFY `TaxShipmentChargesID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tax_shipment_charges_text`
--
ALTER TABLE `tax_shipment_charges_text`
  MODIFY `TaxShipmentChargesTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `temp_orders`
--
ALTER TABLE `temp_orders`
  MODIFY `TempOrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `TicketID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  MODIFY `CommentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `users_text`
--
ALTER TABLE `users_text`
  MODIFY `UserTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `AddressID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `UserNotificationID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  MODIFY `WishlistID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories_text`
--
ALTER TABLE `categories_text`
  ADD CONSTRAINT `categories_text_CategoryID_fk` FOREIGN KEY (`CategoryID`) REFERENCES `categories` (`CategoryID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cities_text`
--
ALTER TABLE `cities_text`
  ADD CONSTRAINT `FK_cities_text` FOREIGN KEY (`CityID`) REFERENCES `cities` (`CityID`);

--
-- Constraints for table `customizes_text`
--
ALTER TABLE `customizes_text`
  ADD CONSTRAINT `customizes_text_CustomizeID_fk` FOREIGN KEY (`CustomizeID`) REFERENCES `customizes` (`CustomizeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `districts_text`
--
ALTER TABLE `districts_text`
  ADD CONSTRAINT `districts_text_ibfk_1` FOREIGN KEY (`DistrictID`) REFERENCES `districts` (`DistrictID`);

--
-- Constraints for table `email_templates_text`
--
ALTER TABLE `email_templates_text`
  ADD CONSTRAINT `email_templates_text_Email_templateID_fk` FOREIGN KEY (`Email_templateID`) REFERENCES `email_templates` (`Email_templateID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `modules_rights`
--
ALTER TABLE `modules_rights`
  ADD CONSTRAINT `fk_ModuleID` FOREIGN KEY (`ModuleID`) REFERENCES `modules` (`ModuleID`);

--
-- Constraints for table `modules_text`
--
ALTER TABLE `modules_text`
  ADD CONSTRAINT `modules_text_ibfk_1` FOREIGN KEY (`ModuleID`) REFERENCES `modules` (`ModuleID`);

--
-- Constraints for table `nutritions_text`
--
ALTER TABLE `nutritions_text`
  ADD CONSTRAINT `nutritions_text_NutritionID_fk` FOREIGN KEY (`NutritionID`) REFERENCES `nutritions` (`NutritionID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pages_text`
--
ALTER TABLE `pages_text`
  ADD CONSTRAINT `pages_text_PageID_fk` FOREIGN KEY (`PageID`) REFERENCES `pages` (`PageID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products_text`
--
ALTER TABLE `products_text`
  ADD CONSTRAINT `producrs_text_ProducrID_fk` FOREIGN KEY (`ProductID`) REFERENCES `products` (`ProductID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles_text`
--
ALTER TABLE `roles_text`
  ADD CONSTRAINT `roles_text_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`);

--
-- Constraints for table `stores_text`
--
ALTER TABLE `stores_text`
  ADD CONSTRAINT `stores_text_StoreID_fk` FOREIGN KEY (`StoreID`) REFERENCES `stores` (`StoreID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tags_text`
--
ALTER TABLE `tags_text`
  ADD CONSTRAINT `tags_text_TagID_fk` FOREIGN KEY (`TagID`) REFERENCES `tags` (`TagID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`) ON UPDATE CASCADE;
