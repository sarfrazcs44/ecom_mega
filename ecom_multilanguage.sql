-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:8889
-- Generation Time: Nov 17, 2021 at 07:40 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `chocomood`
--

-- --------------------------------------------------------

--
-- Table structure for table `boxes`
--

CREATE TABLE `boxes` (
  `BoxID` int(11) NOT NULL,
  `BoxPrice` decimal(10,2) NOT NULL,
  `BoxSpace` int(11) NOT NULL,
  `BoxImage` text NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boxes`
--

INSERT INTO `boxes` (`BoxID`, `BoxPrice`, `BoxSpace`, `BoxImage`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, '30.00', 18, 'uploads/images/2682775138520190410025019box-1.png', 0, 0, 1, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1),
(2, '20.00', 10, 'uploads/images/2682775138520190410025019box-1.png', 0, 0, 1, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `boxes_text`
--

CREATE TABLE `boxes_text` (
  `BoxTextID` int(11) NOT NULL,
  `BoxID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boxes_text`
--

INSERT INTO `boxes_text` (`BoxTextID`, `BoxID`, `Title`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, '30 Chocolate PCS Box', '30 Chocolate PCS Box', 1, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1),
(2, 1, '30 Chocolate PCS Box', '<p>arabic</p>', 2, '2019-04-10 14:19:50', '2019-04-11 15:29:50', 1, 1),
(3, 2, '20 Chocolate PCS Box', '', 1, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1),
(4, 2, '20 Chocolate PCS Box', '', 2, '2019-04-10 14:19:50', '2019-04-10 14:44:55', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CategoryID` int(11) NOT NULL,
  `ParentID` int(11) NOT NULL,
  `CategoryPrice` float NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CategoryID`, `ParentID`, `CategoryPrice`, `SortOrder`, `Image`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(32, 31, 0, 1, '', 0, 1, '2019-11-27 11:05:54', '2019-11-27 11:05:54', 1, 1),
(34, 0, 0, 2, '', 0, 1, '2019-11-30 11:15:49', '2019-11-30 11:18:17', 1, 1),
(35, 0, 0, 3, '', 0, 1, '2019-11-30 11:16:21', '2019-11-30 11:16:21', 1, 1),
(36, 0, 0, 4, '', 0, 1, '2019-11-30 11:16:38', '2019-11-30 11:16:38', 1, 1),
(37, 0, 0, 5, '', 0, 1, '2019-11-30 11:17:06', '2019-11-30 11:17:06', 1, 1),
(38, 0, 0, 6, '', 0, 1, '2019-11-30 11:18:33', '2019-11-30 11:18:33', 1, 1),
(39, 0, 0, 7, '', 0, 1, '2019-11-30 11:18:56', '2019-11-30 11:19:09', 1, 1),
(40, 34, 0, 8, '', 0, 1, '2019-11-30 11:20:07', '2019-11-30 11:20:07', 1, 1),
(41, 34, 0, 9, '', 0, 1, '2019-11-30 11:21:07', '2019-11-30 11:21:07', 1, 1),
(42, 34, 0, 10, '', 0, 1, '2019-11-30 11:28:40', '2019-11-30 11:28:40', 1, 1),
(43, 34, 0, 11, '', 0, 1, '2019-11-30 11:29:22', '2019-11-30 11:29:22', 1, 1),
(44, 34, 0, 12, '', 0, 1, '2019-11-30 11:29:52', '2019-11-30 11:29:52', 1, 1),
(45, 35, 0, 13, '', 0, 1, '2019-11-30 11:30:10', '2019-11-30 11:30:10', 1, 1),
(46, 35, 0, 14, '', 0, 1, '2019-11-30 11:30:33', '2019-11-30 11:30:33', 1, 1),
(47, 35, 0, 15, '', 0, 1, '2019-11-30 11:30:48', '2019-11-30 11:30:48', 1, 1),
(48, 35, 0, 16, '', 0, 1, '2019-11-30 11:31:07', '2019-11-30 11:31:07', 1, 1),
(49, 36, 0, 17, '', 0, 1, '2019-11-30 11:31:30', '2019-11-30 11:31:30', 1, 1),
(50, 36, 0, 18, '', 0, 1, '2019-11-30 11:31:53', '2019-11-30 11:31:53', 1, 1),
(51, 37, 0, 19, '', 0, 1, '2019-11-30 11:33:10', '2019-11-30 11:33:10', 1, 1),
(52, 37, 0, 20, '', 0, 1, '2019-11-30 11:33:40', '2019-11-30 11:33:40', 1, 1),
(53, 39, 0, 21, '', 0, 1, '2019-11-30 11:34:09', '2019-11-30 11:34:09', 1, 1),
(54, 39, 0, 22, '', 0, 1, '2019-11-30 11:34:28', '2019-11-30 11:34:28', 1, 1),
(55, 39, 0, 23, '', 0, 1, '2019-11-30 11:34:53', '2019-11-30 11:34:53', 1, 1),
(56, 38, 0, 24, '', 0, 1, '2019-11-30 11:35:15', '2019-11-30 11:35:15', 1, 1),
(57, 37, 0, 25, '', 0, 1, '2019-11-30 11:40:04', '2019-11-30 11:40:04', 1, 1),
(58, 38, 0, 26, '', 0, 1, '2019-11-30 11:41:57', '2019-11-30 11:41:57', 1, 1),
(59, 38, 0, 27, '', 0, 1, '2019-11-30 11:42:37', '2019-11-30 11:42:37', 1, 1),
(60, 37, 0, 28, '', 0, 1, '2019-11-30 11:43:12', '2019-11-30 11:43:12', 1, 1),
(64, 34, 0, 29, '', 0, 1, '2019-12-07 16:55:38', '2019-12-07 16:55:38', 1, 1),
(65, 0, 0, 30, 'uploads/images/993692098752020021011112572124360_2558790247717603_7384555194358431744_n.jpg', 0, 1, '2020-02-10 11:25:11', '2020-02-10 11:25:11', 1, 1),
(66, 34, 0, 31, 'uploads/images/169588961192020021011272672124360_2558790247717603_7384555194358431744_n.jpg', 0, 1, '2020-02-10 11:26:27', '2020-02-10 11:26:27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories_text`
--

CREATE TABLE `categories_text` (
  `CategoryTextID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories_text`
--

INSERT INTO `categories_text` (`CategoryTextID`, `CategoryID`, `Title`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(61, 32, 'Caramel ', '', 1, '2019-11-27 11:05:54', '2019-11-27 11:05:54', 1, 1),
(62, 32, 'Caramel ', '', 2, '2019-11-27 11:05:54', '2019-11-27 11:05:54', 1, 1),
(65, 34, 'Pralines', '', 1, '2019-11-30 11:15:49', '2019-11-30 11:18:17', 1, 1),
(66, 34, 'Praline', '', 2, '2019-11-30 11:15:49', '2019-11-30 11:15:49', 1, 1),
(67, 35, 'Ocassion', '', 1, '2019-11-30 11:16:21', '2019-11-30 11:16:21', 1, 1),
(68, 35, 'Ocassion', '', 2, '2019-11-30 11:16:21', '2019-11-30 11:16:21', 1, 1),
(69, 36, 'Boxes', '', 1, '2019-11-30 11:16:38', '2019-11-30 11:16:38', 1, 1),
(70, 36, 'Boxes', '', 2, '2019-11-30 11:16:38', '2019-11-30 11:16:38', 1, 1),
(71, 37, 'Confectionery', '', 1, '2019-11-30 11:17:06', '2019-11-30 11:17:06', 1, 1),
(72, 37, 'Confectionery', '', 2, '2019-11-30 11:17:06', '2019-11-30 11:17:06', 1, 1),
(73, 38, 'Pastry', '', 1, '2019-11-30 11:18:33', '2019-11-30 11:18:33', 1, 1),
(74, 38, 'Pastry', '', 2, '2019-11-30 11:18:33', '2019-11-30 11:18:33', 1, 1),
(75, 39, 'Variety', '', 1, '2019-11-30 11:18:56', '2019-11-30 11:19:09', 1, 1),
(76, 39, 'variety', '', 2, '2019-11-30 11:18:56', '2019-11-30 11:18:56', 1, 1),
(77, 40, 'Elegant', '', 1, '2019-11-30 11:20:07', '2019-11-30 11:20:07', 1, 1),
(78, 40, 'Elegant', '', 2, '2019-11-30 11:20:07', '2019-11-30 11:20:07', 1, 1),
(79, 41, 'Style', '', 1, '2019-11-30 11:21:07', '2019-11-30 11:21:07', 1, 1),
(80, 41, 'Style', '', 2, '2019-11-30 11:21:07', '2019-11-30 11:21:07', 1, 1),
(81, 42, 'Carmola', '', 1, '2019-11-30 11:28:40', '2019-11-30 11:28:40', 1, 1),
(82, 42, 'Carmola', '', 2, '2019-11-30 11:28:40', '2019-11-30 11:28:40', 1, 1),
(83, 43, 'Prestige', '', 1, '2019-11-30 11:29:22', '2019-11-30 11:29:22', 1, 1),
(84, 43, 'Prestige', '', 2, '2019-11-30 11:29:22', '2019-11-30 11:29:22', 1, 1),
(85, 44, 'Sugar Free', '', 1, '2019-11-30 11:29:52', '2019-11-30 11:29:52', 1, 1),
(86, 44, 'Sugar Free', '', 2, '2019-11-30 11:29:52', '2019-11-30 11:29:52', 1, 1),
(87, 45, 'Baby', '', 1, '2019-11-30 11:30:10', '2019-11-30 11:30:10', 1, 1),
(88, 45, 'Baby', '', 2, '2019-11-30 11:30:10', '2019-11-30 11:30:10', 1, 1),
(89, 46, 'Wedding', '', 1, '2019-11-30 11:30:33', '2019-11-30 11:30:33', 1, 1),
(90, 46, 'Wedding', '', 2, '2019-11-30 11:30:33', '2019-11-30 11:30:33', 1, 1),
(91, 47, 'Graduation', '', 1, '2019-11-30 11:30:48', '2019-11-30 11:30:48', 1, 1),
(92, 47, 'Graduation', '', 2, '2019-11-30 11:30:48', '2019-11-30 11:30:48', 1, 1),
(93, 48, 'Special Day', '', 1, '2019-11-30 11:31:07', '2019-11-30 11:31:07', 1, 1),
(94, 48, 'Special Day', '', 2, '2019-11-30 11:31:07', '2019-11-30 11:31:07', 1, 1),
(95, 49, 'Luxury', '', 1, '2019-11-30 11:31:30', '2019-11-30 11:31:30', 1, 1),
(96, 49, 'Luxury', '', 2, '2019-11-30 11:31:30', '2019-11-30 11:31:30', 1, 1),
(97, 50, 'Everyday', '', 1, '2019-11-30 11:31:53', '2019-11-30 11:31:53', 1, 1),
(98, 50, 'Everyday', '', 2, '2019-11-30 11:31:53', '2019-11-30 11:31:53', 1, 1),
(99, 51, 'Halkoum and Nougat ', '', 1, '2019-11-30 11:33:10', '2019-11-30 11:33:10', 1, 1),
(100, 51, 'Halkoum and Nougat ', '', 2, '2019-11-30 11:33:10', '2019-11-30 11:33:10', 1, 1),
(101, 52, 'Candy', '', 1, '2019-11-30 11:33:40', '2019-11-30 11:33:40', 1, 1),
(102, 52, 'Candy', '', 2, '2019-11-30 11:33:40', '2019-11-30 11:33:40', 1, 1),
(103, 53, 'Lollipop', '', 1, '2019-11-30 11:34:09', '2019-11-30 11:34:09', 1, 1),
(104, 53, 'Lollipop', '', 2, '2019-11-30 11:34:09', '2019-11-30 11:34:09', 1, 1),
(105, 54, 'Bars', '', 1, '2019-11-30 11:34:28', '2019-11-30 11:34:28', 1, 1),
(106, 54, 'Bars', '', 2, '2019-11-30 11:34:28', '2019-11-30 11:34:28', 1, 1),
(107, 55, 'Fancy Shape', '', 1, '2019-11-30 11:34:53', '2019-11-30 11:34:53', 1, 1),
(108, 55, 'Fancy Shape', '', 2, '2019-11-30 11:34:53', '2019-11-30 11:34:53', 1, 1),
(109, 56, 'Salty Biscuit', '', 1, '2019-11-30 11:35:15', '2019-11-30 11:35:15', 1, 1),
(110, 56, 'Salty Biscuit', '', 2, '2019-11-30 11:35:15', '2019-11-30 11:35:15', 1, 1),
(111, 57, 'Dates', '', 1, '2019-11-30 11:40:04', '2019-11-30 11:40:04', 1, 1),
(112, 57, 'Dates', '', 2, '2019-11-30 11:40:04', '2019-11-30 11:40:04', 1, 1),
(113, 58, 'Petit Four', '', 1, '2019-11-30 11:41:57', '2019-11-30 11:41:57', 1, 1),
(114, 58, 'Petit Four', '', 2, '2019-11-30 11:41:57', '2019-11-30 11:41:57', 1, 1),
(115, 59, 'Canapé', '', 1, '2019-11-30 11:42:37', '2019-11-30 11:42:37', 1, 1),
(116, 59, 'Canapé', '', 2, '2019-11-30 11:42:37', '2019-11-30 11:42:37', 1, 1),
(117, 60, 'Dragée', '', 1, '2019-11-30 11:43:12', '2019-11-30 11:43:12', 1, 1),
(118, 60, 'Dragée', '', 2, '2019-11-30 11:43:12', '2019-11-30 11:43:12', 1, 1),
(125, 64, 'Ruby', '', 1, '2019-12-07 16:55:38', '2019-12-07 16:55:38', 1, 1),
(126, 64, 'Ruby', '', 2, '2019-12-07 16:55:38', '2019-12-07 16:55:38', 1, 1),
(127, 65, 'test', '', 1, '2020-02-10 11:25:11', '2020-02-10 11:25:11', 1, 1),
(128, 65, 'test', '', 2, '2020-02-10 11:25:11', '2020-02-10 11:25:11', 1, 1),
(129, 66, 'dfsdfds', 'this is tesitng', 1, '2020-02-10 11:26:27', '2020-02-10 11:26:27', 1, 1),
(130, 66, 'dfsdfds', 'this is tesitng', 2, '2020-02-10 11:26:27', '2020-02-10 11:26:27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `CityID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`CityID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1337, 3, 0, 1, '2019-01-30 12:23:03', '2019-10-11 14:23:15', 1, 1),
(1338, 4, 0, 1, '2019-01-30 12:23:22', '2019-10-11 14:23:25', 1, 1),
(1339, 1, 0, 1, '2019-01-30 12:21:47', '2019-10-11 15:08:16', 1, 1),
(1340, 5, 0, 1, '2019-03-07 16:24:39', '2019-10-11 14:23:56', 1, 1),
(1342, 3, 0, 1, '2019-08-08 07:38:16', '2019-10-11 14:24:04', 1, 1),
(1344, 0, 0, 1, '2019-01-30 10:48:20', '2019-10-11 14:24:13', 1, 1),
(1345, 3, 0, 1, '2019-08-08 07:41:16', '2019-10-11 14:09:36', 1, 1),
(1346, 6, 0, 1, '2019-10-11 14:12:22', '2019-10-11 14:12:22', 1, 1),
(1348, 12, 0, 1, '2019-10-22 04:38:43', '2019-10-22 04:38:43', 1, 1),
(1349, 2, 0, 1, '2019-01-30 12:22:36', '2019-10-22 04:43:04', 1, 1),
(1350, 3, 0, 1, '2019-08-08 08:02:35', '2019-10-11 14:25:32', 1, 1),
(1351, 3, 0, 1, '2019-08-08 08:04:06', '2019-10-11 14:10:54', 1, 1),
(1352, 2, 0, 1, '2019-01-30 12:22:36', '2019-10-11 14:26:11', 1, 1),
(1353, 2, 0, 0, '2019-01-30 12:22:36', '2019-10-11 14:28:02', 1, 1),
(1354, 11, 0, 1, '2019-10-11 14:14:06', '2019-10-11 14:40:38', 1, 1),
(1355, 11, 0, 1, '2019-10-22 04:50:08', '2019-10-22 04:50:08', 1, 1),
(1356, 10, 0, 1, '2019-10-11 14:14:00', '2019-11-07 20:04:55', 1, 1),
(1357, 11, 0, 0, '2019-10-22 04:58:14', '2019-11-10 12:16:49', 1, 1),
(1358, 11, 0, 1, '2019-10-22 04:59:57', '2019-10-22 04:59:57', 1, 1),
(1359, 11, 0, 1, '2019-10-22 05:01:55', '2019-10-22 05:01:55', 1, 1),
(1360, 11, 0, 1, '2019-10-22 05:03:53', '2019-10-22 05:03:53', 1, 1),
(1361, 2, 0, 1, '2019-01-30 12:22:36', '2019-11-07 20:23:44', 1, 1),
(1362, 11, 0, 1, '2019-10-22 05:09:01', '2019-10-22 05:09:01', 1, 1),
(1363, 10, 0, 1, '2019-10-11 14:14:00', '2019-10-22 05:12:41', 1, 1),
(1364, 11, 0, 1, '2019-10-22 05:13:12', '2019-10-22 05:13:12', 1, 1),
(1365, 7, 0, 1, '2019-04-17 13:41:26', '2019-10-11 14:23:05', 1, 1),
(1366, 11, 0, 0, '2019-10-22 05:15:47', '2019-11-07 20:35:40', 1, 1),
(1367, 11, 0, 1, '2019-10-22 05:17:00', '2019-10-22 05:17:00', 1, 1),
(1368, 11, 0, 1, '2019-10-22 05:18:44', '2019-10-22 05:18:44', 1, 1),
(1369, 11, 0, 1, '2019-10-22 05:20:02', '2019-10-22 05:20:02', 1, 1),
(1370, 2, 0, 1, '2019-01-30 12:22:36', '2019-11-03 11:34:31', 1, 1),
(1371, 11, 0, 1, '2019-10-22 05:45:13', '2019-10-22 05:45:13', 1, 1),
(1372, 11, 0, 1, '2019-10-22 05:47:54', '2019-10-22 05:47:54', 1, 1),
(1373, 11, 0, 1, '2019-10-22 05:49:15', '2019-10-22 05:49:15', 1, 1),
(1375, 11, 0, 1, '2019-10-22 05:52:00', '2019-10-22 05:52:00', 1, 1),
(1376, 11, 0, 1, '2019-10-22 05:53:24', '2019-10-22 05:53:24', 1, 1),
(1377, 11, 0, 1, '2019-10-22 05:54:03', '2019-10-22 05:54:03', 1, 1),
(1378, 11, 0, 1, '2019-10-22 05:55:14', '2019-10-22 05:55:14', 1, 1),
(1379, 11, 0, 1, '2019-10-22 05:56:16', '2019-10-22 05:56:16', 1, 1),
(1380, 11, 0, 0, '2019-10-22 05:57:39', '2019-11-07 20:14:37', 1, 1),
(1381, 11, 0, 1, '2019-10-22 05:58:49', '2019-10-22 05:58:49', 1, 1),
(1383, 3, 0, 1, '2019-08-08 08:14:47', '2019-10-11 14:11:44', 1, 1),
(1384, 11, 0, 1, '2019-10-22 06:03:14', '2019-10-22 06:03:14', 1, 1),
(1385, 11, 0, 1, '2019-10-22 06:04:20', '2019-10-22 06:04:20', 1, 1),
(1386, 11, 0, 1, '2019-10-22 06:05:09', '2019-10-22 06:05:09', 1, 1),
(1397, 11, 0, 1, '2019-10-22 06:06:01', '2019-10-22 06:06:01', 1, 1),
(1398, 10, 0, 0, '2019-10-11 14:14:00', '2019-10-11 14:45:01', 1, 1),
(1399, 4, 0, 1, '2019-08-08 08:19:06', '2019-10-11 14:27:09', 1, 1),
(13100, 5, 0, 1, '2019-08-08 08:46:41', '2019-10-11 14:27:31', 1, 1),
(13102, 7, 0, 1, '2019-10-11 14:13:34', '2019-10-11 14:13:34', 1, 1),
(13135, 8, 0, 1, '2019-11-10 12:21:57', '2019-11-10 12:21:57', 1, 1),
(13136, 9, 0, 1, '2019-11-10 13:06:59', '2019-11-10 13:06:59', 1, 1),
(13137, 10, 0, 1, '2019-11-10 13:18:31', '2019-11-10 13:18:31', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities_text`
--

CREATE TABLE `cities_text` (
  `CityTextID` int(11) NOT NULL,
  `CityID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities_text`
--

INSERT INTO `cities_text` (`CityTextID`, `CityID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1344, 'Medina', 1, '2019-01-30 10:48:20', '2019-10-11 14:24:13', 1, 1),
(2, 1344, 'المدينة المنورة', 2, '2019-01-30 10:48:20', '2019-01-30 10:48:20', 1, 1),
(3, 1339, 'Mecca', 1, '2019-01-30 12:21:47', '2019-10-11 15:08:16', 1, 1),
(4, 1339, 'مكة المكرمة', 2, '2019-01-30 12:21:47', '2019-10-11 15:04:18', 1, 1),
(5, 1352, 'Abha', 1, '2019-01-30 12:22:36', '2019-10-11 14:26:11', 1, 1),
(6, 1352, 'أبها', 2, '2019-01-30 12:22:36', '2019-01-30 12:22:36', 1, 1),
(7, 1337, 'Jeddah', 1, '2019-01-30 12:23:03', '2019-10-11 14:23:15', 1, 1),
(8, 1337, 'جدة', 2, '2019-01-30 12:23:04', '2019-01-30 12:23:04', 1, 1),
(9, 1338, 'Riyadh', 1, '2019-01-30 12:23:22', '2019-10-11 14:23:25', 1, 1),
(10, 1338, 'الرياض', 2, '2019-01-30 12:23:23', '2019-01-30 12:23:23', 1, 1),
(11, 1340, 'Al-Dammam', 1, '2019-03-07 16:24:39', '2019-10-11 14:23:56', 1, 1),
(12, 1340, 'الدمام', 2, '2019-03-07 16:24:40', '2019-03-07 16:24:40', 1, 1),
(15, 1365, 'Tabuk', 1, '2019-04-17 13:41:26', '2019-10-11 14:23:05', 1, 1),
(16, 1365, 'تبوك', 2, '2019-04-17 13:41:26', '2019-04-17 13:41:26', 1, 1),
(17, 1342, 'Hail', 1, '2019-08-08 07:38:16', '2019-10-11 14:24:04', 1, 1),
(18, 1342, 'حائل', 2, '2019-08-08 07:38:17', '2019-08-08 07:38:17', 1, 1),
(19, 1345, 'Al-Khobar', 1, '2019-08-08 07:41:16', '2019-10-11 14:09:36', 1, 1),
(20, 1345, 'الخبر', 2, '2019-08-08 07:41:16', '2019-10-11 14:24:28', 1, 1),
(21, 1349, 'القاسم', 2, '2019-08-08 07:41:16', '2019-08-08 07:41:16', 1, 1),
(22, 1349, 'Al Qassim', 1, '2019-08-08 07:41:16', '2019-10-22 04:43:04', 1, 1),
(23, 1350, 'Al-Taif', 1, '2019-08-08 08:02:35', '2019-10-11 14:25:32', 1, 1),
(24, 1350, 'الطائف', 2, '2019-08-08 08:02:36', '2019-10-11 14:06:31', 1, 1),
(25, 1351, 'Qonfotha', 1, '2019-08-08 08:04:06', '2019-10-11 14:10:54', 1, 1),
(26, 1351, 'القنفذة', 2, '2019-08-08 08:04:06', '2019-10-11 14:25:52', 1, 1),
(27, 1353, 'رابغ', 2, '2019-01-30 12:22:36', '2019-02-06 07:23:33', 1, 1),
(28, 1353, 'Rabigh', 1, '2019-01-30 12:22:36', '2019-10-11 14:28:02', 1, 1),
(31, 1361, 'القريات', 2, '2019-01-30 12:22:36', '2019-11-07 20:12:10', 1, 1),
(32, 1361, 'Al-Qurayat', 1, '2019-01-30 12:22:36', '2019-11-07 20:23:44', 1, 1),
(33, 1370, 'جازان', 2, '2019-01-30 12:22:36', '2019-02-06 07:23:33', 1, 1),
(34, 1370, 'Jazan', 1, '2019-01-30 12:22:36', '2019-11-03 11:34:31', 1, 1),
(35, 1383, 'Alkharj', 1, '2019-08-08 08:14:47', '2019-10-11 14:11:44', 1, 1),
(36, 1383, 'الخرج', 2, '2019-08-08 08:14:47', '2019-10-11 14:26:55', 1, 1),
(37, 1399, 'Cairo', 1, '2019-08-08 08:19:06', '2019-10-11 14:27:09', 1, 1),
(38, 1399, 'القاهرة', 2, '2019-08-08 08:19:07', '2019-08-08 08:19:07', 1, 1),
(45, 13100, 'Al Manama', 1, '2019-08-08 08:46:41', '2019-10-11 14:27:31', 1, 1),
(46, 13100, 'المنامة', 2, '2019-08-08 08:46:41', '2019-10-11 14:27:28', 1, 1),
(47, 1346, 'Najran', 1, '2019-10-11 14:12:22', '2019-10-11 14:12:22', 1, 1),
(48, 1346, 'نجران', 2, '2019-10-11 14:12:23', '2019-10-11 14:12:23', 1, 1),
(49, 13102, 'Ras Tanura', 1, '2019-10-11 14:13:34', '2019-10-11 14:13:34', 1, 1),
(50, 13102, 'رأس تنورة', 2, '2019-10-11 14:13:34', '2019-10-11 14:44:30', 1, 1),
(53, 1363, 'Sakaka', 1, '2019-10-11 14:13:52', '2019-10-22 05:12:41', 1, 1),
(54, 1363, 'سكاكا', 2, '2019-10-11 14:13:52', '2019-10-11 14:13:52', 1, 1),
(57, 1354, 'Yanbu', 1, '2019-10-11 14:14:06', '2019-10-11 14:40:38', 1, 1),
(58, 1354, 'ينبع', 2, '2019-10-11 14:14:07', '2019-10-11 14:14:07', 1, 1),
(59, 1348, 'Dhahran', 1, '2019-10-22 04:38:43', '2019-10-22 04:38:43', 1, 1),
(60, 1348, 'الظهران', 2, '2019-10-22 04:38:44', '2019-10-22 04:38:44', 1, 1),
(61, 1355, 'Hafar Al Batin', 1, '2019-10-22 04:50:08', '2019-10-22 04:50:08', 1, 1),
(62, 1355, 'حفر الباطن', 2, '2019-10-22 04:50:08', '2019-10-22 04:50:08', 1, 1),
(63, 1356, 'Al-Ahsaa', 1, '2019-10-22 04:50:08', '2019-11-07 20:04:55', 1, 1),
(64, 1356, 'الأحساء', 2, '2019-10-22 04:50:08', '2019-11-07 20:05:00', 1, 1),
(65, 1357, 'Abu Al Arish', 1, '2019-10-22 04:58:14', '2019-11-10 12:16:49', 1, 1),
(66, 1357, 'أبي العريش', 2, '2019-10-22 04:58:15', '2019-10-22 04:58:15', 1, 1),
(67, 1358, 'Al Jubail', 1, '2019-10-22 04:59:57', '2019-10-22 04:59:57', 1, 1),
(68, 1358, 'الجبيل', 2, '2019-10-22 04:59:57', '2019-10-22 04:59:57', 1, 1),
(69, 1359, 'Al Qatif', 1, '2019-10-22 05:01:55', '2019-10-22 05:01:55', 1, 1),
(70, 1359, 'القطيف', 2, '2019-10-22 05:01:55', '2019-10-22 05:01:55', 1, 1),
(71, 1360, 'Khafji', 1, '2019-10-22 05:03:53', '2019-10-22 05:03:53', 1, 1),
(72, 1360, 'الخفجي', 2, '2019-10-22 05:03:53', '2019-10-22 05:03:53', 1, 1),
(75, 1362, 'Arar', 1, '2019-10-22 05:09:01', '2019-10-22 05:09:01', 1, 1),
(76, 1362, 'عرعر', 2, '2019-10-22 05:09:02', '2019-10-22 05:09:02', 1, 1),
(77, 1364, 'Rafha', 1, '2019-10-22 05:13:12', '2019-10-22 05:13:12', 1, 1),
(78, 1364, 'رفحاء', 2, '2019-10-22 05:13:13', '2019-10-22 05:13:13', 1, 1),
(79, 1366, 'Tubarjal', 1, '2019-10-22 05:15:47', '2019-11-07 20:35:40', 1, 1),
(80, 1366, 'طبرجل', 2, '2019-10-22 05:15:48', '2019-10-22 05:15:48', 1, 1),
(81, 1367, 'Haql', 1, '2019-10-22 05:17:00', '2019-10-22 05:17:00', 1, 1),
(82, 1367, 'حقل', 2, '2019-10-22 05:17:01', '2019-10-22 05:17:01', 1, 1),
(83, 1368, 'Al Aflaj', 1, '2019-10-22 05:18:44', '2019-10-22 05:18:44', 1, 1),
(84, 1368, 'أملج', 2, '2019-10-22 05:18:44', '2019-10-22 05:18:44', 1, 1),
(85, 1369, 'Turaif', 1, '2019-10-22 05:20:02', '2019-10-22 05:20:02', 1, 1),
(86, 1369, 'طريف‎', 2, '2019-10-22 05:20:03', '2019-10-22 05:20:03', 1, 1),
(87, 1371, 'Al Bahah', 1, '2019-10-22 05:45:13', '2019-10-22 05:45:13', 1, 1),
(88, 1371, 'الباحة', 2, '2019-10-22 05:45:14', '2019-10-22 05:45:14', 1, 1),
(89, 1372, 'Khamis Mushait', 1, '2019-10-22 05:47:54', '2019-10-22 05:47:54', 1, 1),
(90, 1372, 'خميس مشيط', 2, '2019-10-22 05:47:54', '2019-10-22 05:47:54', 1, 1),
(91, 1373, 'Muhayil', 1, '2019-10-22 05:49:15', '2019-10-22 05:49:15', 1, 1),
(92, 1373, 'محايل عسير', 2, '2019-10-22 05:49:16', '2019-10-22 05:49:16', 1, 1),
(93, 1375, 'Sharorah', 1, '2019-10-22 05:52:00', '2019-10-22 05:52:00', 1, 1),
(94, 1375, 'شرورة', 2, '2019-10-22 05:52:00', '2019-10-22 05:52:00', 1, 1),
(95, 1376, 'Baljurashi', 1, '2019-10-22 05:53:24', '2019-10-22 05:53:24', 1, 1),
(96, 1376, 'بلجرشي', 2, '2019-10-22 05:53:25', '2019-10-22 05:53:25', 1, 1),
(97, 1377, 'Sabt Al Alayah', 1, '2019-10-22 05:54:03', '2019-10-22 05:54:03', 1, 1),
(98, 1377, 'Sabt Al Alayah', 2, '2019-10-22 05:54:03', '2019-10-22 05:54:03', 1, 1),
(99, 1378, 'Samtah', 1, '2019-10-22 05:55:14', '2019-10-22 05:55:14', 1, 1),
(100, 1378, 'صامطة', 2, '2019-10-22 05:55:14', '2019-10-22 05:55:14', 1, 1),
(101, 1379, 'Bisha', 1, '2019-10-22 05:56:16', '2019-10-22 05:56:16', 1, 1),
(102, 1379, 'بيشة', 2, '2019-10-22 05:56:17', '2019-10-22 05:56:17', 1, 1),
(103, 1380, 'Sabya', 1, '2019-10-22 05:57:39', '2019-11-07 20:14:37', 1, 1),
(104, 1380, 'صبيا', 2, '2019-10-22 05:57:40', '2019-10-22 05:57:40', 1, 1),
(105, 1381, 'Buraydah', 1, '2019-10-22 05:58:49', '2019-10-22 05:58:49', 1, 1),
(106, 1381, 'بريدة', 2, '2019-10-22 05:58:49', '2019-10-22 05:58:49', 1, 1),
(109, 1384, 'Wadi ad-Dawasir', 1, '2019-10-22 06:03:14', '2019-10-22 06:03:14', 1, 1),
(110, 1384, 'وادي الدواسر', 2, '2019-10-22 06:03:14', '2019-10-22 06:03:14', 1, 1),
(111, 1385, 'Al Duwadimi', 1, '2019-10-22 06:04:20', '2019-10-22 06:04:20', 1, 1),
(112, 1385, 'الدوادمي', 2, '2019-10-22 06:04:20', '2019-10-22 06:04:20', 1, 1),
(113, 1386, 'Al Majmaah', 1, '2019-10-22 06:05:09', '2019-10-22 06:05:09', 1, 1),
(114, 1386, 'المجمعة', 2, '2019-10-22 06:05:09', '2019-10-22 06:05:09', 1, 1),
(115, 1397, 'Al Hofuf', 1, '2019-10-22 06:06:01', '2019-10-22 06:06:01', 1, 1),
(116, 1397, 'الهفوف', 2, '2019-10-22 06:06:01', '2019-10-22 06:06:01', 1, 1),
(117, 1398, 'المبرز', 2, '2019-10-22 06:06:01', '2019-10-22 06:06:01', 1, 1),
(118, 1398, 'Almubaraz', 1, '2019-10-22 06:06:01', '2019-10-22 06:06:01', 1, 1),
(119, 13135, 'Ar Rass', 1, '2019-11-10 12:21:57', '2019-11-10 12:21:57', 1, 1),
(120, 13135, 'الرس', 2, '2019-11-10 12:21:57', '2019-11-10 12:26:14', 1, 1),
(121, 13136, 'Unayzah', 1, '2019-11-10 13:06:59', '2019-11-10 13:06:59', 1, 1),
(122, 13136, 'عنيزة', 2, '2019-11-10 13:07:00', '2019-11-10 13:07:23', 1, 1),
(123, 13137, 'Buqayq', 1, '2019-11-10 13:18:31', '2019-11-10 13:18:31', 1, 1),
(124, 13137, 'بقيق', 2, '2019-11-10 13:18:32', '2019-11-10 13:18:32', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `CollectionID` int(11) NOT NULL,
  `ProductID` varchar(255) NOT NULL,
  `CustomerGroupID` varchar(255) NOT NULL,
  `IsFeatured` tinyint(4) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `HomeImage` varchar(255) NOT NULL,
  `Link` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`CollectionID`, `ProductID`, `CustomerGroupID`, `IsFeatured`, `SortOrder`, `Hide`, `IsActive`, `HomeImage`, `Link`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(22, '35', '', 1, 3, 0, 1, 'uploads/images/9035747175520191202111832y.jpg', '', '2019-12-02 11:32:18', '2019-12-02 11:32:51', 1, 1),
(19, '34,35', '', 1, 0, 0, 1, 'uploads/images/607892687982019121511015001_choco.jpg', '', '2019-11-30 12:32:23', '2019-12-15 11:50:01', 1, 1),
(24, '34', '', 0, 4, 0, 1, 'uploads/images/612656759172019121511224801_choco.jpg', '', '2019-12-15 11:48:22', '2019-12-15 11:48:22', 1, 1),
(21, '34', '', 1, 2, 0, 1, 'uploads/images/4995846680620191202114531h.jpg', '', '2019-12-02 11:31:45', '2019-12-02 11:31:45', 1, 1),
(28, '37,38,39', '3', 0, 5, 0, 1, '', '', '2020-03-26 19:19:32', '2020-03-26 19:21:45', 1, 1),
(29, '', '', 0, 6, 0, 1, 'uploads/images/8550483002820200415063137circles_background_size_monochrome_28137_1920x1080.jpg', 'http://www.google.com', '2020-04-15 18:37:31', '2020-04-15 18:37:31', 1, 1),
(30, '', '', 1, 7, 0, 1, 'uploads/images/1270523275420200415063539circles_background_size_monochrome_28137_1920x1080.jpg', 'http://www.google.com', '2020-04-15 18:39:35', '2020-04-15 18:39:35', 1, 1),
(31, '', '', 0, 8, 0, 1, 'uploads/images/1202071514220200415061344circles_background_size_monochrome_28137_1920x1080.jpg', 'http://www.google3.com', '2020-04-15 18:43:19', '2020-04-15 18:44:41', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `collections_text`
--

CREATE TABLE `collections_text` (
  `CollectionTextID` int(11) NOT NULL,
  `CollectionID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `Ingredients` varchar(2000) NOT NULL,
  `Specifications` text NOT NULL,
  `Tags` varchar(500) NOT NULL,
  `Keywords` varchar(500) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `MetaTags` varchar(255) NOT NULL,
  `MetaKeywords` varchar(255) NOT NULL,
  `MetaDescription` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `collections_text`
--

INSERT INTO `collections_text` (`CollectionTextID`, `CollectionID`, `Title`, `Description`, `Ingredients`, `Specifications`, `Tags`, `Keywords`, `SystemLanguageID`, `MetaTags`, `MetaKeywords`, `MetaDescription`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(24, 19, 'Ramadhan Collection', 'xhshcas', 'dchshdf', '', 'Ramadhan', '', 2, '', '', '', '2019-11-30 12:32:23', '2019-11-30 12:32:23', 1, 1),
(33, 24, 'Wedding', 'non', 'non', '', '', 'chocolate', 1, 'choco collection', 'jeddah', '', '2019-12-15 11:48:22', '2019-12-15 11:48:22', 1, 1),
(27, 21, 'collection3', '', '', '', '', '', 1, '', '', '', '2019-12-02 11:31:45', '2019-12-02 11:31:45', 1, 1),
(28, 21, 'collection3', '', '', '', '', '', 2, '', '', '', '2019-12-02 11:31:45', '2019-12-02 11:31:45', 1, 1),
(29, 22, '4', 'hhh', 'hh', '<p>hh</p>', '', '', 1, '', '', '', '2019-12-02 11:32:18', '2019-12-02 11:32:51', 1, 1),
(30, 22, '4', 'hhh', 'hh', '<p>hh</p>', '', '', 2, '', '', '', '2019-12-02 11:32:18', '2019-12-02 11:32:18', 1, 1),
(34, 24, 'Wedding', 'non', 'non', '', '', 'chocolate', 2, 'choco collection', 'jeddah', '', '2019-12-15 11:48:22', '2019-12-15 11:48:22', 1, 1),
(23, 19, 'Ramadhan Collection', 'xhshcas', 'dchshdf', '', 'Ramadhan', '', 1, '', '', '', '2019-11-30 12:32:23', '2019-12-15 11:50:01', 1, 1),
(46, 30, 'Home Grid Image 1', '', '', '', '', '', 2, '', '', '', '2020-04-15 18:39:35', '2020-04-15 18:39:35', 1, 1),
(45, 30, 'Home Grid Image 1', '', '', '', '', '', 1, '', '', '', '2020-04-15 18:39:35', '2020-04-15 18:39:35', 1, 1),
(44, 29, 'Home Grid Image', '', '', '', '', '', 2, '', '', '', '2020-04-15 18:37:31', '2020-04-15 18:37:31', 1, 1),
(43, 29, 'Home Grid Image', '', '', '', '', '', 1, '', '', '', '2020-04-15 18:37:31', '2020-04-15 18:37:31', 1, 1),
(42, 28, 'test', 'this is testing', '', '', '', '', 2, '', '', '', '2020-03-26 19:19:32', '2020-03-26 19:19:32', 1, 1),
(41, 28, 'test', 'this is testing', '', '', '', '', 1, '', '', '', '2020-03-26 19:19:32', '2020-03-26 19:21:45', 1, 1),
(47, 31, 'sdfdsf', '', '', '', '', '', 1, '', '', '', '2020-04-15 18:43:19', '2020-04-15 18:44:41', 1, 1),
(48, 31, 'dsfsadfas', '', '', '', '', '', 2, '', '', '', '2020-04-15 18:43:19', '2020-04-15 18:43:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `collection_ratings`
--

CREATE TABLE `collection_ratings` (
  `CollectionRatingID` int(11) NOT NULL,
  `CollectionID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Rating` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collection_ratings`
--

INSERT INTO `collection_ratings` (`CollectionRatingID`, `CollectionID`, `UserID`, `Rating`) VALUES
(1, 2, 54, 4);

-- --------------------------------------------------------

--
-- Table structure for table `collection_reviews`
--

CREATE TABLE `collection_reviews` (
  `CollectionReviewID` int(11) NOT NULL,
  `CollectionID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Comment` text CHARACTER SET utf8 NOT NULL,
  `CreatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_requests`
--

CREATE TABLE `contact_requests` (
  `ContactRequestID` int(11) NOT NULL,
  `FullName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `Company` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Department` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Message` text CHARACTER SET utf8 NOT NULL,
  `CV` varchar(255) NOT NULL,
  `CreatedAt` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_requests`
--

INSERT INTO `contact_requests` (`ContactRequestID`, `FullName`, `Email`, `Subject`, `Company`, `Department`, `Message`, `CV`, `CreatedAt`) VALUES
(1, 'Dev Testing', 'dev@gmail.com', 'Feedback', 'Schopfen', 'IT', 'Hi this is a testing message.', '', '1549026151'),
(6, 'asdasd', 'Test@test.com', 'Career', '', '', '', 'uploads/582247656820190411044456dummy-pdf_2.pdf', '1554991004');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `CountryID` int(11) NOT NULL,
  `CountryCode` varchar(50) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`CountryID`, `CountryCode`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'PK', 0, 0, 1, '2018-10-03 22:17:19', '2018-10-03 22:17:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries_text`
--

CREATE TABLE `countries_text` (
  `CountryTextID` int(11) NOT NULL,
  `CountryID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries_text`
--

INSERT INTO `countries_text` (`CountryTextID`, `CountryID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Pakistan', 1, '2018-10-03 22:17:19', '2018-10-03 22:17:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `CouponID` int(11) NOT NULL,
  `CouponCode` varchar(255) NOT NULL,
  `UsageCount` int(11) NOT NULL,
  `DiscountPercentage` decimal(10,2) NOT NULL,
  `ExpiryDate` date NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`CouponID`, `CouponCode`, `UsageCount`, `DiscountPercentage`, `ExpiryDate`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 'BLK001', 99, '20.00', '2020-11-26', 0, 0, 1, '2019-11-27 11:18:48', '2019-11-27 11:19:16', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupons_text`
--

CREATE TABLE `coupons_text` (
  `CouponTextID` int(11) NOT NULL,
  `CouponID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupons_text`
--

INSERT INTO `coupons_text` (`CouponTextID`, `CouponID`, `Title`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 3, 'Black Friday', 'This offer will start date 20 for total of 100 coupons and with total discount amount of 20000 Saudi Rial offer ends based on what ends first total count of 100 or exp. day pass', 1, '2019-11-27 11:18:48', '2019-11-27 11:19:16', 1, 1),
(4, 3, 'الجمعه السوداء', 'This offer will start date 20 for total of 100 coupons and with total discount amount of 20000 Saudi Rial offer ends based on what ends first total count of 100 or exp. day pass', 2, '2019-11-27 11:18:48', '2019-11-27 11:19:36', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer_groups`
--

CREATE TABLE `customer_groups` (
  `CustomerGroupID` int(11) NOT NULL,
  `CustomerGroupTitle` varchar(255) NOT NULL,
  `CustomerGroupType` enum('Orders','Purchases','AllUsers') NOT NULL DEFAULT 'Orders',
  `MinimumValue` varchar(255) NOT NULL,
  `MaximumValue` varchar(255) NOT NULL,
  `FromDate` date NOT NULL,
  `ToDate` date NOT NULL,
  `GroupMembersCount` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_groups`
--

INSERT INTO `customer_groups` (`CustomerGroupID`, `CustomerGroupTitle`, `CustomerGroupType`, `MinimumValue`, `MaximumValue`, `FromDate`, `ToDate`, `GroupMembersCount`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 'New ', 'Purchases', '50', '999', '2019-11-01', '2019-12-08', 1, 0, 0, 1, '2019-12-08 13:06:19', '2019-12-08 13:06:19', 0, 0),
(5, 'test', 'AllUsers', '', '', '0000-00-00', '0000-00-00', 1, 0, 0, 0, '2020-04-17 18:04:07', '2020-04-17 18:04:07', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_group_members`
--

CREATE TABLE `customer_group_members` (
  `CustomerGroupMemberID` int(11) NOT NULL,
  `CustomerGroupID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_group_members`
--

INSERT INTO `customer_group_members` (`CustomerGroupMemberID`, `CustomerGroupID`, `UserID`) VALUES
(3, 3, 64),
(9, 5, 1),
(8, 5, 64);

-- --------------------------------------------------------

--
-- Table structure for table `customizes`
--

CREATE TABLE `customizes` (
  `CustomizeID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customizes`
--

INSERT INTO `customizes` (`CustomizeID`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'uploads/images/831963780792020021907415081898777_2460926880890961_7466254659263397888_n.jpg', 0, 0, 1, '2020-02-19 07:50:32', '2020-02-19 07:50:41', 1, 1),
(2, 'uploads/images/324346328082020021907565472124360_2558790247717603_7384555194358431744_n.jpg', 1, 0, 1, '2020-02-19 07:54:56', '2020-02-19 07:54:56', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customizes_text`
--

CREATE TABLE `customizes_text` (
  `CustomizeTextID` int(11) NOT NULL,
  `CustomizeID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customizes_text`
--

INSERT INTO `customizes_text` (`CustomizeTextID`, `CustomizeID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'test', 1, '2020-02-19 07:50:32', '2020-02-19 07:50:41', 1, 1),
(2, 1, 'test', 2, '2020-02-19 07:50:32', '2020-02-19 07:50:32', 1, 1),
(3, 2, 'second customization', 1, '2020-02-19 07:54:56', '2020-02-19 07:54:56', 1, 1),
(4, 2, 'second customization', 2, '2020-02-19 07:54:56', '2020-02-19 07:54:56', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `DistrictID` int(11) NOT NULL,
  `CityID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`DistrictID`, `CityID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1388, 1342, 0, 0, 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(1389, 1338, 0, 0, 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(1390, 1338, 0, 0, 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(1391, 1358, 0, 0, 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(1392, 1381, 0, 0, 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(1393, 1338, 0, 0, 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(1395, 1344, 0, 0, 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(49607, 1340, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49608, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49609, 1340, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49610, 1340, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49611, 1345, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49612, 1345, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49613, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49614, 1344, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49615, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49616, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49617, 1349, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49618, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49619, 1340, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49620, 1340, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49621, 1345, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49622, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49623, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49624, 1345, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49625, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49626, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49627, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49628, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49629, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49630, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49631, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49632, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49633, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49634, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49635, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49636, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49637, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49638, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49639, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49640, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49641, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49642, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49643, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49644, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49645, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49646, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49647, 1345, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49648, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49649, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49650, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49651, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49652, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49653, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49654, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49655, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49656, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49657, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49658, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49659, 1344, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49660, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49661, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49662, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49663, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49664, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49665, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49666, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49667, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49668, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49669, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49670, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49671, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49672, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49673, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49674, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49675, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49676, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49677, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49678, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49679, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49680, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49681, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49682, 1338, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49683, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49684, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49685, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49686, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49687, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49688, 1337, 0, 0, 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49689, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49690, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49691, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49692, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49693, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49694, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49695, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49696, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49697, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49698, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49699, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49700, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49701, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49702, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49703, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49704, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49705, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49706, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49707, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49708, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49709, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49710, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49711, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49712, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49713, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49714, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49715, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49716, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49717, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49718, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49719, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49720, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49721, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49722, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49723, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49724, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49725, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49726, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49727, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49728, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49729, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49730, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49731, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49732, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49733, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49734, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49735, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49736, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49737, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49738, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49739, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49740, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49741, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49742, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49743, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49744, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49745, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49746, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49747, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49748, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49749, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49750, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49751, 1340, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49752, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49753, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49754, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49755, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49756, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49757, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49758, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49759, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49760, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49761, 1344, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49762, 1344, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49763, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49764, 1344, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49765, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49766, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49767, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49768, 1348, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49769, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49770, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49771, 1347, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49772, 1347, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49773, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49774, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49775, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49776, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49777, 1346, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49778, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49779, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49780, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49781, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49782, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49783, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49784, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49785, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49786, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49787, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49788, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49789, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49790, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49791, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49792, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49793, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49794, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49795, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49796, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49797, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49798, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49799, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49800, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49801, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49802, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49803, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49804, 1338, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49805, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49806, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49807, 1337, 0, 0, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49808, 1337, 0, 1, 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(49809, 1344, 0, 0, 1, '2017-12-08 20:16:04', '0000-00-00 00:00:00', 1, 1),
(49810, 1344, 0, 0, 1, '2017-12-08 20:36:42', '0000-00-00 00:00:00', 1, 1),
(49811, 1344, 0, 0, 1, '2017-12-08 21:02:00', '0000-00-00 00:00:00', 1, 1),
(49812, 1338, 0, 1, 1, '2017-12-21 12:30:17', '0000-00-00 00:00:00', 1, 1),
(49813, 1345, 0, 0, 1, '2017-12-21 12:50:37', '0000-00-00 00:00:00', 1, 1),
(49814, 1357, 0, 1, 1, '2017-12-23 13:06:54', '0000-00-00 00:00:00', 1, 1),
(49815, 1344, 0, 0, 1, '2017-12-31 14:54:36', '0000-00-00 00:00:00', 1, 1),
(49816, 1340, 0, 0, 1, '2017-12-31 14:57:19', '0000-00-00 00:00:00', 1, 1),
(49817, 1338, 0, 0, 1, '2017-12-31 14:57:37', '0000-00-00 00:00:00', 1, 1),
(49818, 1338, 0, 0, 1, '2017-12-31 15:50:39', '0000-00-00 00:00:00', 1, 1),
(49819, 1340, 0, 0, 1, '2017-12-31 15:51:00', '0000-00-00 00:00:00', 1, 1),
(49820, 1339, 0, 0, 1, '2017-12-31 19:19:05', '0000-00-00 00:00:00', 1, 1),
(49821, 1345, 0, 0, 1, '2018-01-05 22:00:35', '0000-00-00 00:00:00', 1, 1),
(49822, 1370, 0, 0, 1, '2018-01-05 22:25:40', '0000-00-00 00:00:00', 1, 1),
(49823, 1349, 0, 0, 1, '2018-01-05 23:02:18', '0000-00-00 00:00:00', 1, 1),
(49824, 1397, 0, 0, 1, '2018-01-05 23:33:00', '0000-00-00 00:00:00', 1, 1),
(49825, 1383, 0, 0, 1, '2018-01-05 23:55:03', '0000-00-00 00:00:00', 1, 1),
(49826, 1398, 0, 0, 1, '2018-01-06 00:01:34', '0000-00-00 00:00:00', 1, 1),
(49827, 1355, 0, 0, 1, '2018-01-06 00:19:00', '0000-00-00 00:00:00', 1, 1),
(49828, 1358, 0, 0, 1, '2018-01-06 00:29:49', '0000-00-00 00:00:00', 1, 1),
(49829, 1342, 0, 0, 1, '2018-01-06 00:36:28', '0000-00-00 00:00:00', 1, 1),
(49830, 1365, 0, 0, 1, '2018-01-06 21:52:27', '0000-00-00 00:00:00', 1, 1),
(49831, 1354, 0, 0, 1, '2018-01-06 22:10:34', '0000-00-00 00:00:00', 1, 1),
(49832, 1340, 0, 0, 1, '2018-01-07 12:55:05', '0000-00-00 00:00:00', 1, 1),
(49833, 1338, 0, 0, 1, '2018-01-07 18:32:05', '0000-00-00 00:00:00', 1, 1),
(49834, 1338, 0, 0, 1, '2018-01-09 01:48:48', '0000-00-00 00:00:00', 1, 1),
(49835, 1383, 0, 0, 1, '2018-01-09 01:50:58', '0000-00-00 00:00:00', 1, 1),
(49836, 1353, 0, 0, 1, '2018-01-09 02:34:51', '0000-00-00 00:00:00', 1, 1),
(49837, 1345, 0, 0, 1, '2018-01-09 02:48:35', '0000-00-00 00:00:00', 1, 1),
(49838, 1340, 0, 0, 1, '2018-01-09 03:03:58', '0000-00-00 00:00:00', 1, 1),
(49839, 1339, 0, 0, 1, '2018-01-19 22:05:33', '0000-00-00 00:00:00', 1, 1),
(49840, 1339, 0, 0, 1, '2018-01-19 22:06:27', '0000-00-00 00:00:00', 1, 1),
(49841, 1339, 0, 0, 1, '2018-02-08 15:45:02', '0000-00-00 00:00:00', 1, 1),
(49842, 1381, 0, 0, 1, '2018-02-08 15:46:59', '0000-00-00 00:00:00', 1, 1),
(49843, 1342, 0, 0, 1, '2018-02-12 17:06:27', '0000-00-00 00:00:00', 1, 1),
(49844, 1339, 0, 0, 1, '2018-02-28 14:07:31', '0000-00-00 00:00:00', 1, 1),
(49845, 1354, 0, 0, 1, '2018-02-28 14:11:09', '0000-00-00 00:00:00', 1, 1),
(49846, 1339, 0, 0, 1, '2018-03-05 15:52:06', '0000-00-00 00:00:00', 1, 1),
(49847, 1339, 0, 0, 1, '2018-03-05 16:04:19', '0000-00-00 00:00:00', 1, 1),
(49848, 1350, 0, 0, 1, '2018-03-05 16:10:04', '0000-00-00 00:00:00', 1, 1),
(49849, 1350, 0, 0, 1, '2018-03-05 16:24:41', '0000-00-00 00:00:00', 1, 1),
(49850, 1399, 0, 0, 1, '2018-03-27 14:08:28', '0000-00-00 00:00:00', 1, 1),
(49851, 1339, 0, 0, 1, '2018-04-26 16:23:36', '0000-00-00 00:00:00', 1, 1),
(49852, 1350, 0, 0, 1, '2018-04-26 16:35:32', '0000-00-00 00:00:00', 1, 1),
(49853, 1360, 0, 0, 1, '2018-07-02 18:37:43', '0000-00-00 00:00:00', 1, 1),
(49854, 1344, 0, 0, 1, '2018-07-09 14:51:53', '0000-00-00 00:00:00', 1, 1),
(49855, 1340, 0, 0, 1, '2018-07-09 18:22:30', '0000-00-00 00:00:00', 1, 1),
(49856, 1350, 0, 0, 1, '2018-07-11 18:39:42', '0000-00-00 00:00:00', 1, 1),
(49857, 1338, 0, 0, 1, '2018-07-11 18:58:22', '0000-00-00 00:00:00', 1, 1),
(49858, 1339, 0, 0, 1, '2018-07-19 15:46:03', '0000-00-00 00:00:00', 1, 1),
(49859, 1338, 0, 0, 1, '2018-07-19 16:02:17', '0000-00-00 00:00:00', 1, 1),
(49860, 1338, 0, 0, 1, '2018-07-19 16:24:18', '0000-00-00 00:00:00', 1, 1),
(49861, 1352, 0, 0, 1, '2018-07-23 16:59:55', '0000-00-00 00:00:00', 1, 1),
(49862, 1346, 0, 0, 1, '2018-07-23 17:04:45', '0000-00-00 00:00:00', 1, 1),
(49863, 1372, 0, 0, 1, '2018-07-23 17:07:39', '0000-00-00 00:00:00', 1, 1),
(49864, 1374, 0, 0, 1, '2018-07-23 17:15:01', '0000-00-00 00:00:00', 1, 1),
(49865, 1338, 0, 0, 1, '2018-07-25 15:55:29', '0000-00-00 00:00:00', 1, 1),
(49866, 1352, 0, 0, 1, '2018-07-25 16:51:23', '0000-00-00 00:00:00', 1, 1),
(49867, 1338, 0, 0, 1, '2018-08-28 13:44:22', '0000-00-00 00:00:00', 1, 1),
(49868, 1340, 0, 0, 1, '2018-08-28 14:00:32', '0000-00-00 00:00:00', 1, 1),
(49869, 1358, 0, 0, 1, '2018-08-28 14:09:11', '0000-00-00 00:00:00', 1, 1),
(49870, 1372, 0, 0, 1, '2018-08-28 14:15:41', '0000-00-00 00:00:00', 1, 1),
(49871, 1338, 0, 0, 1, '2018-08-28 16:34:12', '0000-00-00 00:00:00', 1, 1),
(49872, 1339, 0, 0, 1, '2018-09-11 17:10:44', '0000-00-00 00:00:00', 1, 1),
(49873, 1337, 0, 0, 1, '2018-09-13 13:41:16', '0000-00-00 00:00:00', 1, 1),
(49874, 1339, 0, 0, 1, '2018-10-18 19:01:54', '0000-00-00 00:00:00', 1, 1),
(49875, 1339, 0, 0, 1, '2018-10-18 19:02:20', '0000-00-00 00:00:00', 1, 1),
(49876, 1339, 0, 0, 1, '2018-10-18 19:02:45', '0000-00-00 00:00:00', 1, 1),
(49877, 1370, 0, 0, 1, '2018-10-30 14:39:55', '0000-00-00 00:00:00', 1, 1),
(49878, 1352, 0, 1, 1, '2018-10-30 14:50:24', '0000-00-00 00:00:00', 1, 1),
(49879, 1337, 0, 0, 1, '2018-10-31 17:14:41', '0000-00-00 00:00:00', 1, 1),
(49880, 1345, 0, 0, 1, '2019-01-01 15:22:25', '0000-00-00 00:00:00', 1, 1),
(49881, 1352, 0, 0, 1, '2019-01-01 15:48:46', '0000-00-00 00:00:00', 1, 1),
(49882, 1350, 0, 0, 1, '2019-01-01 17:14:34', '0000-00-00 00:00:00', 1, 1),
(49883, 1348, 0, 0, 1, '2019-01-02 15:50:37', '0000-00-00 00:00:00', 1, 1),
(49884, 1370, 0, 0, 1, '2019-01-02 16:32:06', '0000-00-00 00:00:00', 1, 1),
(49885, 1339, 0, 0, 1, '2019-01-02 17:25:22', '0000-00-00 00:00:00', 1, 1),
(49886, 1339, 0, 0, 1, '2019-01-02 18:17:53', '0000-00-00 00:00:00', 1, 1),
(49887, 13100, 0, 0, 1, '2019-01-07 15:45:21', '0000-00-00 00:00:00', 1, 1),
(49888, 1340, 0, 0, 1, '2019-01-31 18:40:27', '0000-00-00 00:00:00', 1, 1),
(49889, 1381, 0, 0, 1, '2019-01-31 18:54:26', '0000-00-00 00:00:00', 1, 1),
(49890, 1381, 0, 0, 1, '2019-01-31 19:27:21', '0000-00-00 00:00:00', 1, 1),
(49891, 1350, 0, 0, 1, '2019-02-14 14:03:13', '0000-00-00 00:00:00', 1, 1),
(49892, 1339, 0, 0, 1, '2019-02-26 18:34:11', '0000-00-00 00:00:00', 1, 1),
(49893, 1339, 0, 0, 1, '2019-03-03 17:29:47', '0000-00-00 00:00:00', 1, 1),
(49894, 1339, 0, 0, 1, '2019-03-14 19:43:56', '0000-00-00 00:00:00', 1, 1),
(49895, 1371, 0, 0, 1, '2019-03-20 13:53:02', '0000-00-00 00:00:00', 1, 1),
(49896, 1350, 0, 0, 1, '2019-03-20 15:17:45', '0000-00-00 00:00:00', 1, 1),
(49897, 1344, 0, 0, 1, '2019-03-20 15:58:13', '0000-00-00 00:00:00', 1, 1),
(49898, 1374, 0, 0, 1, '2019-03-20 19:06:13', '0000-00-00 00:00:00', 1, 1),
(49899, 1351, 0, 0, 1, '2019-03-20 19:25:11', '0000-00-00 00:00:00', 1, 1),
(49900, 1340, 0, 0, 1, '2019-03-24 14:02:46', '0000-00-00 00:00:00', 1, 1),
(49901, 1372, 0, 0, 1, '2019-03-24 14:41:58', '0000-00-00 00:00:00', 1, 1),
(49902, 1379, 0, 0, 1, '2019-03-24 15:32:14', '0000-00-00 00:00:00', 1, 1),
(49903, 1350, 0, 0, 1, '2019-04-02 19:10:55', '0000-00-00 00:00:00', 1, 1),
(49904, 1344, 0, 0, 1, '2019-04-04 18:13:13', '0000-00-00 00:00:00', 1, 1),
(49905, 1339, 0, 0, 1, '2019-04-18 16:39:32', '0000-00-00 00:00:00', 1, 1),
(49906, 1340, 0, 0, 1, '2019-05-14 16:40:44', '0000-00-00 00:00:00', 1, 1),
(49907, 1352, 0, 0, 1, '2019-05-14 16:41:22', '0000-00-00 00:00:00', 1, 1),
(49908, 1370, 1, 0, 1, '2019-11-03 11:38:55', '2019-11-03 11:38:55', 1, 1),
(49909, 1352, 2, 0, 1, '2019-11-08 11:32:08', '2019-11-08 11:32:08', 1, 1),
(49910, 1352, 3, 0, 1, '2019-11-08 11:42:03', '2019-11-08 11:42:03', 1, 1),
(49911, 1383, 4, 0, 1, '2019-11-08 12:01:05', '2019-11-08 12:01:05', 1, 1),
(49912, 1340, 5, 0, 1, '2019-11-08 12:11:10', '2019-11-08 12:11:10', 1, 1),
(49913, 1385, 6, 0, 1, '2019-11-08 12:20:42', '2019-11-08 12:20:42', 1, 1),
(49914, 1338, 7, 0, 1, '2019-11-08 12:28:10', '2019-11-08 12:28:10', 1, 1),
(49915, 1338, 8, 0, 1, '2019-11-08 12:33:11', '2019-11-08 12:33:11', 1, 1),
(49916, 1338, 9, 0, 1, '2019-11-08 12:37:53', '2019-11-08 12:37:53', 1, 1),
(49917, 1338, 10, 0, 1, '2019-11-08 12:41:39', '2019-11-08 12:41:39', 1, 1),
(49918, 1338, 11, 0, 1, '2019-11-08 12:45:55', '2019-11-08 12:45:55', 1, 1),
(49919, 1338, 12, 0, 1, '2019-11-08 12:53:54', '2019-11-08 12:53:54', 1, 1),
(49920, 1338, 13, 0, 1, '2019-11-08 13:07:01', '2019-11-08 13:07:01', 1, 1),
(49921, 1338, 14, 0, 1, '2019-11-08 13:07:11', '2019-11-08 13:07:11', 1, 1),
(49922, 1350, 15, 0, 1, '2019-11-08 13:11:50', '2019-11-08 13:11:50', 1, 1),
(49923, 1350, 16, 0, 1, '2019-11-08 13:16:27', '2019-11-08 13:16:27', 1, 1),
(49924, 1350, 17, 0, 1, '2019-11-08 13:23:05', '2019-11-08 13:23:05', 1, 1),
(49925, 1344, 18, 0, 1, '2019-11-08 13:25:48', '2019-11-08 13:25:48', 1, 1),
(49926, 1344, 19, 0, 1, '2019-11-08 13:30:28', '2019-11-08 13:30:28', 1, 1),
(49927, 1344, 20, 0, 1, '2019-11-08 13:33:31', '2019-11-08 13:33:31', 1, 1),
(49928, 1381, 21, 0, 1, '2019-11-08 13:43:52', '2019-11-08 13:43:52', 1, 1),
(49929, 1381, 22, 0, 1, '2019-11-08 13:51:56', '2019-11-08 13:51:56', 1, 1),
(49931, 1337, 23, 0, 1, '2019-11-11 07:58:32', '2019-11-11 07:58:32', 1, 1),
(49932, 1337, 24, 0, 1, '2019-11-11 08:02:06', '2019-11-11 08:02:06', 1, 1),
(49933, 1337, 25, 0, 1, '2019-11-11 08:05:11', '2019-11-11 08:05:11', 1, 1),
(49934, 1342, 26, 0, 1, '2019-11-11 08:10:02', '2019-11-11 08:10:02', 1, 1),
(49935, 1342, 27, 0, 1, '2019-11-11 08:13:35', '2019-11-11 08:13:35', 1, 1),
(49936, 1342, 28, 0, 1, '2019-11-11 08:17:27', '2019-11-11 08:17:27', 1, 1),
(49937, 1355, 29, 0, 1, '2019-11-11 08:21:22', '2019-11-11 08:21:22', 1, 1),
(49938, 1372, 30, 0, 1, '2019-11-11 08:24:16', '2019-11-11 08:24:16', 1, 1),
(49939, 1372, 31, 0, 1, '2019-11-11 08:29:23', '2019-11-11 08:29:23', 1, 1),
(49940, 1354, 32, 0, 1, '2019-11-11 09:31:30', '2019-11-11 09:31:30', 1, 1),
(49941, 1354, 33, 0, 1, '2019-11-11 09:40:28', '2019-11-11 09:40:28', 1, 1),
(49942, 1354, 34, 0, 1, '2019-11-11 09:43:28', '2019-11-11 09:43:28', 1, 1),
(49943, 1339, 35, 0, 1, '2019-11-11 09:48:47', '2019-11-11 09:48:47', 1, 1),
(49944, 1339, 36, 0, 1, '2019-11-11 09:56:48', '2019-11-11 09:56:48', 1, 1),
(49945, 1339, 37, 0, 1, '2019-11-11 10:00:41', '2019-11-11 10:00:41', 1, 1),
(49946, 1339, 38, 0, 1, '2019-11-11 10:04:53', '2019-11-11 10:04:53', 1, 1),
(49947, 1339, 39, 0, 1, '2019-11-11 10:08:54', '2019-11-11 10:08:54', 1, 1),
(49948, 1339, 40, 0, 1, '2019-11-11 10:12:49', '2019-11-11 10:12:49', 1, 1),
(49949, 1362, 41, 0, 1, '2019-11-11 10:17:36', '2019-11-11 10:17:36', 1, 1),
(49950, 1362, 42, 0, 1, '2019-11-11 10:20:48', '2019-11-11 10:20:48', 1, 1),
(49951, 1363, 43, 0, 1, '2019-11-11 10:28:40', '2019-11-11 10:32:34', 1, 1),
(49952, 1363, 44, 0, 1, '2019-11-11 10:32:47', '2019-11-11 10:32:47', 1, 1),
(49953, 1363, 45, 0, 1, '2019-11-11 10:35:05', '2019-11-11 10:35:05', 1, 1),
(49954, 1346, 46, 0, 1, '2019-11-11 11:43:33', '2019-11-11 11:43:33', 1, 1),
(49955, 13136, 47, 0, 1, '2019-11-11 12:00:46', '2019-11-11 12:00:46', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `districts_text`
--

CREATE TABLE `districts_text` (
  `DistrictTextID` int(11) NOT NULL,
  `DistrictID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts_text`
--

INSERT INTO `districts_text` (`DistrictTextID`, `DistrictID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1388, 'Abbas Al-Aqad', 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(2, 1389, 'Al-Aqiq', 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(3, 1390, 'Al-Faruq', 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(4, 1391, 'Al-Huwaylat', 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(5, 1392, 'Al-Iskan', 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(6, 1393, 'Al-Nakheel', 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(8, 1395, 'Shadhah AlDayeri', 1, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(9, 49607, 'Abdullah Fouad', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(10, 49608, 'Abruq Al-Rughamah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(11, 49609, 'Al Ghadeer', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(12, 49610, 'Al Kawthar', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(13, 49611, 'Al Khobar Al Janubiyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(14, 49612, 'Al Khobar Al Shamaliyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(15, 49613, 'Al Manakhah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(16, 49614, 'Al Manakhah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(17, 49615, 'Al Mughrizat', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(18, 49616, 'Al- Nazlah Ash-Sharqiyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(19, 49617, 'Al Rass', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(20, 49618, 'Al Sahafa', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(21, 49619, 'Al Salam', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(22, 49620, 'Al Shati Al Sharqi', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(23, 49621, 'Al Ulaya', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(24, 49622, 'Al Wadi', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(25, 49623, 'Al Waha', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(26, 49624, 'Al Yarmouk', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(27, 49625, 'Al Yasmin', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(28, 49626, 'Al-Adel', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(29, 49627, 'Al-Ajaweed', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(30, 49628, 'Al-Ajwad', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(31, 49629, 'Al-Amir Fawaz Al-Januby', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(32, 49630, 'Al-Amir Fawaz Ash-Shamaly', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(33, 49631, 'Al-Ammariyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(34, 49632, 'Al-Amwaj', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(35, 49633, 'Al-Andalus', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(36, 49634, 'Al-Asaala', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(37, 49635, 'Al-Aziziyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(38, 49636, 'Al-Badiah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(39, 49637, 'Al-Baghdadiya Al-Gharbiya', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(40, 49638, 'Al-Baghdadiya Al-Sharqiya', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(41, 49639, 'Al-Balad', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(42, 49640, 'Al-Barkah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(43, 49641, 'Al-Basatin', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(44, 49642, 'Al-Bashaer', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(45, 49643, 'Al-Bawadi', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(46, 49644, 'Al-Baylasan', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(47, 49645, 'Al-Cournich', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(48, 49646, 'Al-Dahiah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(49, 49647, 'Al-Dhahran', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(50, 49648, 'Al-Ezdihar', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(51, 49649, 'Al-Fadel', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(52, 49650, 'Al-Fadeylah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(53, 49651, 'Al-Faiha', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(54, 49652, 'Al-Faisaliyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(55, 49653, 'Al-Falah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(56, 49654, 'Al-Farouk', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(57, 49655, 'Al-Ferdous', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(58, 49656, 'Al-Frosyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(59, 49657, 'Al-Ghadir', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(60, 49658, 'Al-Hada', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(61, 49659, 'Al-Hadra\'a', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(62, 49660, 'Al-Hamdaniya', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(63, 49661, 'Al-Hamra', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(64, 49662, 'Al-Hamraa', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(65, 49663, 'Al-Haramin', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(66, 49664, 'Al-Harazat', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(67, 49665, 'Al-Henaki', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(68, 49666, 'Al-Hindawiyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(69, 49667, 'Al-Iskan Al-Janubi', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(70, 49668, 'Al-Jamaa', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(71, 49669, 'Al-Jawad', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(72, 49670, 'Al-Jawharah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(73, 49671, 'Al-Jazirah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(74, 49672, 'Al-Kandarah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(75, 49673, 'Al-Karamah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(76, 49674, 'Al-Kawthar', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(77, 49675, 'Al-Khalidiyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(78, 49676, 'Al-Khaskiyah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(79, 49677, 'Al-Khomrah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(80, 49678, 'Al-Loaloa', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(81, 49679, 'Al-Madhar Al-Shamali', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(82, 49680, 'Al-Mahjar', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(83, 49681, 'Al-Majed', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(84, 49682, 'Al-Malaz', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(85, 49683, 'Al-Manar', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(86, 49684, 'Al-Marwah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(87, 49685, 'Al-Masarah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(88, 49686, 'Al-Matar Al-Gadeem (old airport)', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(89, 49687, 'Al-Montazah', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(90, 49688, 'Al-Mosadiya', 1, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(91, 49689, 'Al-Moulysaa', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(92, 49690, 'Al-Muhammadiyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(93, 49691, 'Al-Murjan', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(94, 49692, 'Al-Murooj', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(95, 49693, 'Al-Mutanazahat', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(96, 49694, 'Al-Nada District', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(97, 49695, 'Al-Nahdah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(98, 49696, 'Al-Naim', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(99, 49697, 'Al-Nakhil', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(100, 49698, 'Al-Nasim', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(101, 49699, 'Al-Nazlah Al- Yamaniyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(102, 49700, 'Al-Nuzhah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(103, 49701, 'Al-Qouzeen', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(104, 49702, 'Al-Qryniah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(105, 49703, 'Al-Quraiyat', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(106, 49704, 'Al-Rabi', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(107, 49705, 'Al-Rabi', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(108, 49706, 'Al-Rabie', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(109, 49707, 'Al-Rabwah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(110, 49708, 'Al-Rabwah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(111, 49709, 'Al-Rahmah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(112, 49710, 'Al-Rahmaniyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(113, 49711, 'Al-Rahmanyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(114, 49712, 'Al-Rawabi', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(115, 49713, 'Al-Rawdah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(116, 49714, 'Al-Rawdah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(117, 49715, 'Al-Rayaan', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(118, 49716, 'Al-Rayan', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(119, 49717, 'Al-Rehaili', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(120, 49718, 'Al-Rihab', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(121, 49719, 'Al-Riyadh', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(122, 49720, 'Al-Ruwais', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(123, 49721, 'Al-Sabil', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(124, 49722, 'Al-Safa', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(125, 49723, 'Al-Saheifa', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(126, 49724, 'Al-Sahil', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(127, 49725, 'Al-Salam', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(128, 49726, 'Al-Salamah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(129, 49727, 'Al-Salhiyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(130, 49728, 'Al-Salmyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(131, 49729, 'Al-Samir', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(132, 49730, 'Al-Sanabel', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(133, 49731, 'Al-Sarawat', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(134, 49732, 'Al-Sawaed', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(135, 49733, 'Al-Sharafiyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(136, 49734, 'Al-Shati', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(137, 49735, 'Al-Shefaa', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(138, 49736, 'Al-Sheraa', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(139, 49737, 'Al-Shifa', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(140, 49738, 'Al-Shrouk', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(141, 49739, 'Al-Sororyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(142, 49740, 'Al-Sulai', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(143, 49741, 'Al-Sulimaniyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(144, 49742, 'Al-Sulimaniyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(145, 49743, 'Al-Swaryee', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(146, 49744, 'Al-Taawon', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(147, 49745, 'Al-Tadamon', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(148, 49746, 'Al-Tawfiq', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(149, 49747, 'Al-Thaalibah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(150, 49748, 'Al-Thaghr', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(151, 49749, 'Al-Thuryah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(152, 49750, 'Al-Ulaya', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(153, 49751, 'Al-Ulaya', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(154, 49752, 'Al-Ulayia', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(155, 49753, 'Al-Wadi', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(156, 49754, 'Al-Wahah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(157, 49755, 'Al-Waziriyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(158, 49756, 'Al-Wurud', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(159, 49757, 'Al-Wurud', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(160, 49758, 'Al-Yaqoot', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(161, 49759, 'Al-Zahra', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(162, 49760, 'Al-Zomorod', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(163, 49761, 'Ar Ranuna District', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(164, 49762, 'Badaah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(165, 49763, 'Bahrah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(166, 49764, 'Bani Khidrah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(167, 49765, 'Bani Malik', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(168, 49766, 'Batterjee Medical Collage', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(169, 49767, 'Bryman', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(170, 49768, 'Doha Al Janubiyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(171, 49769, 'Durt Al-Arous', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(172, 49770, 'East Al-Khat As-Sarei', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(173, 49771, 'Egypt', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(174, 49772, 'Emirates', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(175, 49773, 'Exit 5', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(176, 49774, 'Ghirnatah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(177, 49775, 'Ghulail', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(178, 49776, 'Industrial Area', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(179, 49777, 'Industrial Shakwn Road', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(180, 49778, 'Ishbiliyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(181, 49779, 'Jeddah Islamic Seaport', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(182, 49780, 'Kilo 10', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(183, 49781, 'Kilo 11', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(184, 49782, 'Kilo 13', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(185, 49783, 'Kilo 14', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(186, 49784, 'Kilo 2', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(187, 49785, 'Kilo 3', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(188, 49786, 'Kilo 5', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(189, 49787, 'Kilo 7', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(190, 49788, 'Kilo 8', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(191, 49789, 'King Abdulaziz International Airport', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(192, 49790, 'King Abdulaziz Medical City', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(193, 49791, 'King Abdulaziz University', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(194, 49792, 'King Abdulla University', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(195, 49793, 'King Fahd', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(196, 49794, 'King Faisal Naval Base', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(197, 49795, 'Madain Al-Fahd', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(198, 49796, 'Mishrifah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(199, 49797, 'Mraykh', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(200, 49798, 'National Guard', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(201, 49799, 'Obhour Al-Janubiyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(202, 49800, 'Obhour Al-Shamaliyah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(203, 49801, 'Petromin', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(204, 49802, 'Prince Abdel Majeed', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(205, 49803, 'Qurtuba', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(206, 49804, 'Second Industrial City', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(207, 49805, 'Taibah', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(208, 49806, 'Um Alsalam', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(209, 49807, 'Um Hableen', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(210, 49808, 'Um Hableen', 1, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(211, 49809, 'Bir Uthman', 1, '2017-12-08 20:16:04', '0000-00-00 00:00:00', 1, 1),
(212, 49810, 'Al Ihn', 1, '2017-12-08 20:36:42', '0000-00-00 00:00:00', 1, 1),
(213, 49811, 'Al Mughaisilah', 1, '2017-12-08 21:02:00', '0000-00-00 00:00:00', 1, 1),
(214, 49812, 'Al-Olaya', 1, '2017-12-21 12:30:17', '0000-00-00 00:00:00', 1, 1),
(215, 49813, 'Madinat Al-Ummal', 1, '2017-12-21 12:50:37', '0000-00-00 00:00:00', 1, 1),
(216, 49814, 'test', 1, '2017-12-23 13:06:54', '0000-00-00 00:00:00', 1, 1),
(217, 49815, 'Al-Qiblatain', 1, '2017-12-31 14:54:36', '0000-00-00 00:00:00', 1, 1),
(218, 49816, 'Al-Mohammadiyah', 1, '2017-12-31 14:57:19', '0000-00-00 00:00:00', 1, 1),
(219, 49817, 'Al-Malqa', 1, '2017-12-31 14:57:37', '0000-00-00 00:00:00', 1, 1),
(220, 49818, 'AlMohammadiya', 1, '2017-12-31 15:50:39', '0000-00-00 00:00:00', 1, 1),
(221, 49819, 'AlKhaldiyah', 1, '2017-12-31 15:51:00', '0000-00-00 00:00:00', 1, 1),
(222, 49820, 'Alhajla', 1, '2017-12-31 19:19:05', '0000-00-00 00:00:00', 1, 1),
(223, 49821, 'Corniche Alkhobar', 1, '2018-01-05 22:00:35', '0000-00-00 00:00:00', 1, 1),
(224, 49822, 'Corniche Alshamali', 1, '2018-01-05 22:25:40', '0000-00-00 00:00:00', 1, 1),
(225, 49823, 'Almontazah', 1, '2018-01-05 23:02:18', '0000-00-00 00:00:00', 1, 1),
(226, 49824, 'Alomairiyah', 1, '2018-01-05 23:33:00', '0000-00-00 00:00:00', 1, 1),
(227, 49825, 'Alkhozama', 1, '2018-01-05 23:55:03', '0000-00-00 00:00:00', 1, 1),
(228, 49826, 'Alyahya', 1, '2018-01-06 00:01:34', '0000-00-00 00:00:00', 1, 1),
(229, 49827, 'Almasiaf', 1, '2018-01-06 00:19:00', '0000-00-00 00:00:00', 1, 1),
(230, 49828, 'Sedair', 1, '2018-01-06 00:29:49', '0000-00-00 00:00:00', 1, 1),
(231, 49829, 'Alnoqra', 1, '2018-01-06 00:36:28', '0000-00-00 00:00:00', 1, 1),
(232, 49830, 'Al Worood', 1, '2018-01-06 21:52:27', '0000-00-00 00:00:00', 1, 1),
(233, 49831, 'Aljabriya', 1, '2018-01-06 22:10:34', '0000-00-00 00:00:00', 1, 1),
(234, 49832, 'AlShati AlGharbi', 1, '2018-01-07 12:55:05', '0000-00-00 00:00:00', 1, 1),
(235, 49833, 'Al Dirah', 1, '2018-01-07 18:32:05', '0000-00-00 00:00:00', 1, 1),
(236, 49834, 'Dhahrat Laban', 1, '2018-01-09 01:48:48', '0000-00-00 00:00:00', 1, 1),
(237, 49835, 'As Salam', 1, '2018-01-09 01:50:58', '0000-00-00 00:00:00', 1, 1),
(238, 49836, 'Al Samad', 1, '2018-01-09 02:34:51', '0000-00-00 00:00:00', 1, 1),
(239, 49837, 'Al Hizam Al Thahabi', 1, '2018-01-09 02:48:35', '0000-00-00 00:00:00', 1, 1),
(240, 49838, 'Al Anud', 1, '2018-01-09 03:03:58', '0000-00-00 00:00:00', 1, 1),
(241, 49839, 'Al-Shawqiah,', 1, '2018-01-19 22:05:33', '0000-00-00 00:00:00', 1, 1),
(242, 49840, 'Al-Rusaifah', 1, '2018-01-19 22:06:27', '0000-00-00 00:00:00', 1, 1),
(243, 49841, 'Al Awali', 1, '2018-02-08 15:45:02', '0000-00-00 00:00:00', 1, 1),
(244, 49842, 'Al Naziyah', 1, '2018-02-08 15:46:59', '0000-00-00 00:00:00', 1, 1),
(245, 49843, 'Al Wasita', 1, '2018-02-12 17:06:27', '0000-00-00 00:00:00', 1, 1),
(246, 49844, 'Al-Shabeka', 1, '2018-02-28 14:07:31', '0000-00-00 00:00:00', 1, 1),
(247, 49845, 'Yunbu Al Bahar', 1, '2018-02-28 14:11:09', '0000-00-00 00:00:00', 1, 1),
(248, 49846, 'Al-Shohada', 1, '2018-03-05 15:52:06', '0000-00-00 00:00:00', 1, 1),
(249, 49847, 'Al-Aziziya', 1, '2018-03-05 16:04:19', '0000-00-00 00:00:00', 1, 1),
(250, 49848, 'Shihar', 1, '2018-03-05 16:10:04', '0000-00-00 00:00:00', 1, 1),
(251, 49849, 'Al Hawia', 1, '2018-03-05 16:24:41', '0000-00-00 00:00:00', 1, 1),
(252, 49850, 'Corniche El Nile', 1, '2018-03-27 14:08:28', '0000-00-00 00:00:00', 1, 1),
(253, 49851, 'Al Taqwa', 1, '2018-04-26 16:23:36', '0000-00-00 00:00:00', 1, 1),
(254, 49852, 'Al Sharqiyah', 1, '2018-04-26 16:35:32', '0000-00-00 00:00:00', 1, 1),
(255, 49853, 'Al-Faihaa', 1, '2018-07-02 18:37:43', '0000-00-00 00:00:00', 1, 1),
(256, 49854, 'Al Aridh', 1, '2018-07-09 14:51:53', '0000-00-00 00:00:00', 1, 1),
(257, 49855, 'Al-Rayyan', 1, '2018-07-09 18:22:30', '0000-00-00 00:00:00', 1, 1),
(258, 49856, 'Al-Washaha', 1, '2018-07-11 18:39:42', '0000-00-00 00:00:00', 1, 1),
(259, 49857, 'Al Manar', 1, '2018-07-11 18:58:22', '0000-00-00 00:00:00', 1, 1),
(260, 49858, 'AlNuzhah', 1, '2018-07-19 15:46:03', '0000-00-00 00:00:00', 1, 1),
(261, 49859, 'Al Mathar Alshamali', 1, '2018-07-19 16:02:17', '0000-00-00 00:00:00', 1, 1),
(262, 49860, 'Hittin', 1, '2018-07-19 16:24:18', '0000-00-00 00:00:00', 1, 1),
(263, 49861, 'AlRabwah', 1, '2018-07-23 16:59:55', '0000-00-00 00:00:00', 1, 1),
(264, 49862, 'King Abdulaziz Road', 1, '2018-07-23 17:04:45', '0000-00-00 00:00:00', 1, 1),
(265, 49863, 'King Faisal Road', 1, '2018-07-23 17:07:39', '0000-00-00 00:00:00', 1, 1),
(266, 49864, 'Alnamas', 1, '2018-07-23 17:15:01', '0000-00-00 00:00:00', 1, 1),
(267, 49865, 'Al-Shuhada', 1, '2018-07-25 15:55:29', '0000-00-00 00:00:00', 1, 1),
(268, 49866, 'Al-Badi', 1, '2018-07-25 16:51:23', '0000-00-00 00:00:00', 1, 1),
(269, 49867, 'Um Al Hamam Al Sharqi', 1, '2018-08-28 13:44:22', '0000-00-00 00:00:00', 1, 1),
(270, 49868, 'Adama', 1, '2018-08-28 14:00:32', '0000-00-00 00:00:00', 1, 1),
(271, 49869, 'Al-Jubail Al Balad', 1, '2018-08-28 14:09:11', '0000-00-00 00:00:00', 1, 1),
(272, 49870, 'Um Sarar', 1, '2018-08-28 14:15:41', '0000-00-00 00:00:00', 1, 1),
(273, 49871, 'Al Andalus', 1, '2018-08-28 16:34:12', '0000-00-00 00:00:00', 1, 1),
(274, 49872, 'Al Hamra', 1, '2018-09-11 17:10:44', '0000-00-00 00:00:00', 1, 1),
(275, 49873, 'Al-Tahlia', 1, '2018-09-13 13:41:16', '0000-00-00 00:00:00', 1, 1),
(276, 49874, 'Al Sharae', 1, '2018-10-18 19:01:54', '0000-00-00 00:00:00', 1, 1),
(277, 49875, 'Al Zahir', 1, '2018-10-18 19:02:20', '0000-00-00 00:00:00', 1, 1),
(278, 49876, 'Al Noriya', 1, '2018-10-18 19:02:45', '0000-00-00 00:00:00', 1, 1),
(279, 49877, 'Al Matar', 1, '2018-10-30 14:39:55', '0000-00-00 00:00:00', 1, 1),
(280, 49878, 'Al Rabwa', 1, '2018-10-30 14:50:24', '0000-00-00 00:00:00', 1, 1),
(281, 49879, 'Dhahban', 1, '2018-10-31 17:14:41', '0000-00-00 00:00:00', 1, 1),
(282, 49880, 'Al Khaldiyah', 1, '2019-01-01 15:22:25', '0000-00-00 00:00:00', 1, 1),
(283, 49881, 'AlKhalidiyah', 1, '2019-01-01 15:48:46', '0000-00-00 00:00:00', 1, 1),
(284, 49882, 'AlHada', 1, '2019-01-01 17:14:34', '0000-00-00 00:00:00', 1, 1),
(285, 49883, 'AlOlaya', 1, '2019-01-02 15:50:37', '0000-00-00 00:00:00', 1, 1),
(286, 49884, 'Scheme 5', 1, '2019-01-02 16:32:06', '0000-00-00 00:00:00', 1, 1),
(287, 49885, 'Ajyad', 1, '2019-01-02 17:25:22', '0000-00-00 00:00:00', 1, 1),
(288, 49886, 'Al Rawabi', 1, '2019-01-02 18:17:53', '0000-00-00 00:00:00', 1, 1),
(289, 49887, 'Block 257', 1, '2019-01-07 15:45:21', '0000-00-00 00:00:00', 1, 1),
(290, 49888, 'Al Badi', 1, '2019-01-31 18:40:27', '0000-00-00 00:00:00', 1, 1),
(291, 49889, 'Al Subaieya', 1, '2019-01-31 18:54:26', '0000-00-00 00:00:00', 1, 1),
(292, 49890, 'Al Bishr', 1, '2019-01-31 19:27:21', '0000-00-00 00:00:00', 1, 1),
(293, 49891, 'Maashi', 1, '2019-02-14 14:03:13', '0000-00-00 00:00:00', 1, 1),
(294, 49892, 'Al Jam\'ah', 1, '2019-02-26 18:34:11', '0000-00-00 00:00:00', 1, 1),
(295, 49893, 'Al Hejra', 1, '2019-03-03 17:29:47', '0000-00-00 00:00:00', 1, 1),
(296, 49894, 'Al Hajlah', 1, '2019-03-14 19:43:56', '0000-00-00 00:00:00', 1, 1),
(297, 49895, 'Al Zarqa', 1, '2019-03-20 13:53:02', '0000-00-00 00:00:00', 1, 1),
(298, 49896, 'Al Faisaliyah', 1, '2019-03-20 15:17:45', '0000-00-00 00:00:00', 1, 1),
(299, 49897, 'Al-Matar', 1, '2019-03-20 15:58:13', '0000-00-00 00:00:00', 1, 1),
(300, 49898, 'Mahayil Aseer', 1, '2019-03-20 19:06:13', '0000-00-00 00:00:00', 1, 1),
(301, 49899, 'Al-Khaldiyah', 1, '2019-03-20 19:25:11', '0000-00-00 00:00:00', 1, 1),
(302, 49900, 'Al Mazruiyah', 1, '2019-03-24 14:02:46', '0000-00-00 00:00:00', 1, 1),
(303, 49901, 'Atod', 1, '2019-03-24 14:41:58', '0000-00-00 00:00:00', 1, 1),
(304, 49902, 'Al Fahad', 1, '2019-03-24 15:32:14', '0000-00-00 00:00:00', 1, 1),
(305, 49903, 'Al Qutbiya', 1, '2019-04-02 19:10:55', '0000-00-00 00:00:00', 1, 1),
(306, 49904, 'Quba', 1, '2019-04-04 18:13:13', '0000-00-00 00:00:00', 1, 1),
(307, 49905, 'Al Khalidiyah', 1, '2019-04-18 16:39:32', '0000-00-00 00:00:00', 1, 1),
(308, 49906, 'Al Aziziah', 1, '2019-05-14 16:40:44', '0000-00-00 00:00:00', 1, 1),
(309, 49907, 'Al Mawdhfen', 1, '2019-05-14 16:41:22', '0000-00-00 00:00:00', 1, 1),
(310, 1388, 'عباس العقاد', 2, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(311, 1389, 'العقيق', 2, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(312, 1390, 'الفاروق', 2, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(313, 1391, 'الحواليات', 2, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(314, 1392, 'الإسكان', 2, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(315, 1393, 'النخيل', 2, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(317, 1395, 'شظاة الدائري', 2, '2017-12-07 18:21:42', '0000-00-00 00:00:00', 1, 1),
(318, 49607, 'عبدالله فؤاد', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(319, 49608, 'أبرق الرغامة', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(320, 49609, 'الغدير\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(321, 49610, 'الكوثر\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(322, 49611, 'الخبر الجنوبية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(323, 49612, 'الخبر الشمالية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(324, 49613, 'المناخة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(325, 49614, 'المناخة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(326, 49615, 'المغرزات\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(327, 49616, 'النزلة الشرقية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(328, 49617, 'الرس\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(329, 49618, 'الصحافة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(330, 49619, 'السلام\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(331, 49620, 'الشاطئ الشرقي\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(332, 49621, 'العليا\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(333, 49622, 'الوادي\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(334, 49623, 'الواحة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(335, 49624, 'اليرموك\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(336, 49625, 'الياسمين\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(337, 49626, 'العدل\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(338, 49627, 'الأجاويد\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(339, 49628, 'الأجواد\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(340, 49629, 'الأمير فواز الجنوبي\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(341, 49630, 'الأمير فواز الشمالي\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(342, 49631, 'العمارية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(343, 49632, 'الأمواج\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(344, 49633, 'الأندلس\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(345, 49634, 'الأصالة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(346, 49635, 'العزيزية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(347, 49636, 'البديعة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(348, 49637, 'البغدادية الغربية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(349, 49638, 'البغدادية الشرقية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(350, 49639, 'البلد\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(351, 49640, 'البركة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(352, 49641, 'البساتين\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(353, 49642, 'البشائر\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(354, 49643, 'البوادي\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(355, 49644, 'البيلسان\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(356, 49645, 'الكورنيش\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(357, 49646, 'الضاحية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(358, 49647, 'الظهران\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(359, 49648, 'الإزدهار\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(360, 49649, 'الفضل\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(361, 49650, 'الفضيلة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(362, 49651, 'الفيحاء\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(363, 49652, 'الفيصلية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(364, 49653, 'الفلاح\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(365, 49654, 'الفاروق\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(366, 49655, 'الفردوس\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(367, 49656, 'الفروسية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(368, 49657, 'الغدير\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(369, 49658, 'الهدى\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(370, 49659, 'الهدراء\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(371, 49660, 'الحمدانية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(372, 49661, 'الحمراء\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(373, 49662, 'الحمراء\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(374, 49663, 'الحرمين\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(375, 49664, 'الحرزات\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(376, 49665, 'الحناكي\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(377, 49666, 'الهنداوية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(378, 49667, 'الإسكان الجنوبي\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(379, 49668, 'الجامعة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(380, 49669, 'الجواد\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(381, 49670, 'الجوهرة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(382, 49671, 'الجزيرة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(383, 49672, 'الكندرة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(384, 49673, 'الكرامة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(385, 49674, 'الكوثر\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(386, 49675, 'الخالدية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(387, 49676, 'الخاسكية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(388, 49677, 'الخمرة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(389, 49678, 'اللؤلؤ\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(390, 49679, 'المعذر الشمالي\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(391, 49680, 'المحجر\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(392, 49681, 'الماجد\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(393, 49682, 'الملز\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(394, 49683, 'المنار\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(395, 49684, 'المروة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(396, 49685, 'المسرة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(397, 49686, 'المطار القديم\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(398, 49687, 'المنتزة\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(399, 49688, 'المساعدية\r\n', 2, '2017-10-18 18:05:29', '0000-00-00 00:00:00', 1, 1),
(400, 49689, 'المليساء\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(401, 49690, 'المحمدية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(402, 49691, 'المرجان\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(403, 49692, 'المروج\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(404, 49693, 'المتنزهات\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(405, 49694, 'حى الندى\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(406, 49695, 'النهضة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(407, 49696, 'النعيم\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(408, 49697, 'النخيل\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(409, 49698, 'النسيم\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(410, 49699, 'النزلة اليمانية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(411, 49700, 'النزهة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(412, 49701, 'القوزين\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(413, 49702, 'القرينية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(414, 49703, 'القريات\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(415, 49704, 'الربيع\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(416, 49705, 'الربيع\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(417, 49706, 'الربيع\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(418, 49707, 'الربوة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(419, 49708, 'الربوة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(420, 49709, 'الرحمة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(421, 49710, 'الرحمانية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(422, 49711, 'الرحمانية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(423, 49712, 'الروابي\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(424, 49713, 'الروضة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(425, 49714, 'الروضة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(426, 49715, 'الريان\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(427, 49716, 'الريان\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(428, 49717, 'الرحيلي\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(429, 49718, 'الرحاب\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(430, 49719, 'الرياض\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(431, 49720, 'الرويس\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(432, 49721, 'السبيل\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(433, 49722, 'الصفا\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(434, 49723, 'الصحيفة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(435, 49724, 'الساحل\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(436, 49725, 'السلام\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(437, 49726, 'السلامة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(438, 49727, 'الصالحية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(439, 49728, 'السالمية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(440, 49729, 'السامر\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(441, 49730, 'السنابل\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(442, 49731, 'السروات\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(443, 49732, 'الصواعد\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(444, 49733, 'الشرفية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(445, 49734, 'الشاطئ\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(446, 49735, 'الشفا\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(447, 49736, 'الشراع\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(448, 49737, 'الشفا\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(449, 49738, 'الشروق\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(450, 49739, 'السرورية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(451, 49740, 'السلي\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(452, 49741, 'السليمانية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(453, 49742, 'السليمانية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(454, 49743, 'الصواري\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(455, 49744, 'التعاون\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(456, 49745, 'التضامن\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(457, 49746, 'التوفيق\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(458, 49747, 'الثعالبة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(459, 49748, 'الثغر\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(460, 49749, 'الثريا\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(461, 49750, 'العليا\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(462, 49751, 'العليا\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(463, 49752, 'العليا\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(464, 49753, 'الوادي\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(465, 49754, 'الواحة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(466, 49755, 'الوزيريه\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(467, 49756, 'الورود\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(468, 49757, 'الورود\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(469, 49758, 'الياقوت\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(470, 49759, 'الزهراء\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(471, 49760, 'الزمرد\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(472, 49761, 'حي الرنونة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(473, 49762, 'بضاعة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(474, 49763, 'بحرة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(475, 49764, 'بني خدرة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(476, 49765, 'بنى مالك\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(477, 49766, 'كلية البترجي الطبية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(478, 49767, 'بريمان\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(479, 49768, 'الدوحة الجنوبية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(480, 49769, 'درة العروس\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(481, 49770, 'شرق الخط السريع\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(482, 49771, 'مصر\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(483, 49772, 'الامارات\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(484, 49773, 'مخرج ٥\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(485, 49774, 'غرناطة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(486, 49775, 'غليل\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(487, 49776, 'المنطقة الصناعية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(488, 49777, 'طريق شكوان الصناعية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(489, 49778, 'اشبيلية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(490, 49779, 'ميناء جدة الاسلامى\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(491, 49780, 'كيلو 10\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(492, 49781, 'كيلو 11\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(493, 49782, 'كيلو 13\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(494, 49783, 'كيلو 14\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(495, 49784, 'كيلو 2\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(496, 49785, 'كيلو 3\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(497, 49786, 'كيلو 5\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(498, 49787, 'كيلو 7\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(499, 49788, 'كيلو 8\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(500, 49789, 'مطار الملك عبدالعزيز الدولي\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(501, 49790, 'مدينة الملك عبدالعزيز الطبية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(502, 49791, 'جامعة الملك عبدالعزيز\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(503, 49792, 'جامعة الملك عبدالله\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(504, 49793, 'الملك فهد\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(505, 49794, 'قاعدة الملك فيصل البحرية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(506, 49795, 'مدائن الفهد\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(507, 49796, 'مشرفة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(508, 49797, 'مريخ\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(509, 49798, 'الحرس الوطني\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(510, 49799, 'أبحر الجنوبية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(511, 49800, 'أبحر الشمالية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(512, 49801, 'بترومين\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(513, 49802, 'الأمير عبدالمجيد\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(514, 49803, 'قرطبة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(515, 49804, 'المدينة الصناعية الثانية\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(516, 49805, 'طيبة\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(517, 49806, 'ام السلم\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(518, 49807, 'ام حبلين\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(519, 49808, 'ام حبلين\r\n', 2, '2017-10-18 18:05:30', '0000-00-00 00:00:00', 1, 1),
(520, 49809, 'بئر عثمان', 2, '2017-12-08 20:16:04', '0000-00-00 00:00:00', 1, 1),
(521, 49810, 'العهن', 2, '2017-12-08 20:36:42', '0000-00-00 00:00:00', 1, 1),
(522, 49811, 'المغيسلة', 2, '2017-12-08 21:02:00', '0000-00-00 00:00:00', 1, 1),
(523, 49812, 'العليا', 2, '2017-12-21 12:30:17', '0000-00-00 00:00:00', 1, 1),
(524, 49813, 'مدينة العمال', 2, '2017-12-21 12:50:37', '0000-00-00 00:00:00', 1, 1),
(525, 49814, 'test', 2, '2017-12-23 13:06:54', '0000-00-00 00:00:00', 1, 1),
(526, 49815, 'القبلتين', 2, '2017-12-31 14:54:36', '0000-00-00 00:00:00', 1, 1),
(527, 49816, 'المحمدية', 2, '2017-12-31 14:57:19', '0000-00-00 00:00:00', 1, 1),
(528, 49817, 'الملقا', 2, '2017-12-31 14:57:37', '0000-00-00 00:00:00', 1, 1),
(529, 49818, 'المحمدية', 2, '2017-12-31 15:50:39', '0000-00-00 00:00:00', 1, 1),
(530, 49819, 'الخالدية', 2, '2017-12-31 15:51:00', '0000-00-00 00:00:00', 1, 1),
(531, 49820, 'الهجلة', 2, '2017-12-31 19:19:05', '0000-00-00 00:00:00', 1, 1),
(532, 49821, 'كورنيش الخبر', 2, '2018-01-05 22:00:35', '0000-00-00 00:00:00', 1, 1),
(533, 49822, 'الكورنيش الشمالي', 2, '2018-01-05 22:25:40', '0000-00-00 00:00:00', 1, 1),
(534, 49823, 'المنتزه', 2, '2018-01-05 23:02:18', '0000-00-00 00:00:00', 1, 1),
(535, 49824, 'العميرية', 2, '2018-01-05 23:33:00', '0000-00-00 00:00:00', 1, 1),
(536, 49825, 'الخزامى', 2, '2018-01-05 23:55:03', '0000-00-00 00:00:00', 1, 1),
(537, 49826, 'اليحيى', 2, '2018-01-06 00:01:34', '0000-00-00 00:00:00', 1, 1),
(538, 49827, 'المصيف', 2, '2018-01-06 00:19:00', '0000-00-00 00:00:00', 1, 1),
(539, 49828, 'سدير', 2, '2018-01-06 00:29:49', '0000-00-00 00:00:00', 1, 1),
(540, 49829, 'النقرة', 2, '2018-01-06 00:36:28', '0000-00-00 00:00:00', 1, 1),
(541, 49830, 'الورود', 2, '2018-01-06 21:52:27', '0000-00-00 00:00:00', 1, 1),
(542, 49831, 'الجابرية', 2, '2018-01-06 22:10:34', '0000-00-00 00:00:00', 1, 1),
(543, 49832, 'الشاطئ الغربي', 2, '2018-01-07 12:55:05', '0000-00-00 00:00:00', 1, 1),
(544, 49833, 'الديرة', 2, '2018-01-07 18:32:05', '0000-00-00 00:00:00', 1, 1),
(545, 49834, 'ظهرة لبن', 2, '2018-01-09 01:48:48', '0000-00-00 00:00:00', 1, 1),
(546, 49835, 'السلام', 2, '2018-01-09 01:50:58', '0000-00-00 00:00:00', 1, 1),
(547, 49836, 'الصمد', 2, '2018-01-09 02:34:51', '0000-00-00 00:00:00', 1, 1),
(548, 49837, 'الحزام الذهبي', 2, '2018-01-09 02:48:35', '0000-00-00 00:00:00', 1, 1),
(549, 49838, 'العنود', 2, '2018-01-09 03:03:58', '0000-00-00 00:00:00', 1, 1),
(550, 49839, 'الشوقية', 2, '2018-01-19 22:05:33', '0000-00-00 00:00:00', 1, 1),
(551, 49840, 'الرصيفة', 2, '2018-01-19 22:06:27', '0000-00-00 00:00:00', 1, 1),
(552, 49841, 'العوالي', 2, '2018-02-08 15:45:02', '0000-00-00 00:00:00', 1, 1),
(553, 49842, 'النازية', 2, '2018-02-08 15:46:59', '0000-00-00 00:00:00', 1, 1),
(554, 49843, 'الوسيطاء', 2, '2018-02-12 17:06:27', '0000-00-00 00:00:00', 1, 1),
(555, 49844, 'الشبيكة', 2, '2018-02-28 14:07:31', '0000-00-00 00:00:00', 1, 1),
(556, 49845, 'ينبع البحر', 2, '2018-02-28 14:11:09', '0000-00-00 00:00:00', 1, 1),
(557, 49846, 'الشهداء', 2, '2018-03-05 15:52:06', '0000-00-00 00:00:00', 1, 1),
(558, 49847, 'العزيزية', 2, '2018-03-05 16:04:19', '0000-00-00 00:00:00', 1, 1),
(559, 49848, 'شهار', 2, '2018-03-05 16:10:04', '0000-00-00 00:00:00', 1, 1),
(560, 49849, 'الحوية', 2, '2018-03-05 16:24:41', '0000-00-00 00:00:00', 1, 1),
(561, 49850, 'كورنيش النيل', 2, '2018-03-27 14:08:28', '0000-00-00 00:00:00', 1, 1),
(562, 49851, 'التقوى', 2, '2018-04-26 16:23:36', '0000-00-00 00:00:00', 1, 1),
(563, 49852, 'الشارقية', 2, '2018-04-26 16:35:32', '0000-00-00 00:00:00', 1, 1),
(564, 49853, 'الفيحاء', 2, '2018-07-02 18:37:43', '0000-00-00 00:00:00', 1, 1),
(565, 49854, 'العريض', 2, '2018-07-09 14:51:53', '0000-00-00 00:00:00', 1, 1),
(566, 49855, 'الريان', 2, '2018-07-09 18:22:30', '0000-00-00 00:00:00', 1, 1),
(567, 49856, 'الوشحاء', 2, '2018-07-11 18:39:42', '0000-00-00 00:00:00', 1, 1),
(568, 49857, 'المنار', 2, '2018-07-11 18:58:22', '0000-00-00 00:00:00', 1, 1),
(569, 49858, 'النزهة', 2, '2018-07-19 15:46:03', '0000-00-00 00:00:00', 1, 1),
(570, 49859, 'المعذر الشمالي', 2, '2018-07-19 16:02:17', '0000-00-00 00:00:00', 1, 1),
(571, 49860, 'حطين', 2, '2018-07-19 16:24:18', '0000-00-00 00:00:00', 1, 1),
(572, 49861, 'الربوة', 2, '2018-07-23 16:59:55', '0000-00-00 00:00:00', 1, 1),
(573, 49862, 'طريق الملك عبدالعزيز', 2, '2018-07-23 17:04:45', '0000-00-00 00:00:00', 1, 1),
(574, 49863, 'طريق الملك فيصل', 2, '2018-07-23 17:07:39', '0000-00-00 00:00:00', 1, 1),
(575, 49864, 'النماص', 2, '2018-07-23 17:15:01', '0000-00-00 00:00:00', 1, 1),
(576, 49865, 'الشهداء', 2, '2018-07-25 15:55:29', '0000-00-00 00:00:00', 1, 1),
(577, 49866, 'البديع', 2, '2018-07-25 16:51:23', '0000-00-00 00:00:00', 1, 1),
(578, 49867, 'ام الحمام الشرقي', 2, '2018-08-28 13:44:22', '0000-00-00 00:00:00', 1, 1),
(579, 49868, 'العدامة', 2, '2018-08-28 14:00:32', '0000-00-00 00:00:00', 1, 1),
(580, 49869, 'الجبيل البلد', 2, '2018-08-28 14:09:11', '0000-00-00 00:00:00', 1, 1),
(581, 49870, 'ام سرار', 2, '2018-08-28 14:15:41', '0000-00-00 00:00:00', 1, 1),
(582, 49871, 'الأندلس', 2, '2018-08-28 16:34:12', '0000-00-00 00:00:00', 1, 1),
(583, 49872, 'الحمراء', 2, '2018-09-11 17:10:44', '0000-00-00 00:00:00', 1, 1),
(584, 49873, 'التحلية', 2, '2018-09-13 13:41:16', '0000-00-00 00:00:00', 1, 1),
(585, 49874, 'الشرائع', 2, '2018-10-18 19:01:54', '0000-00-00 00:00:00', 1, 1),
(586, 49875, 'الزاهر', 2, '2018-10-18 19:02:20', '0000-00-00 00:00:00', 1, 1),
(587, 49876, 'النورية', 2, '2018-10-18 19:02:45', '0000-00-00 00:00:00', 1, 1),
(588, 49877, 'المطار', 2, '2018-10-30 14:39:55', '0000-00-00 00:00:00', 1, 1),
(589, 49878, 'الربوة', 2, '2018-10-30 14:50:24', '0000-00-00 00:00:00', 1, 1),
(590, 49879, 'ذهبان', 2, '2018-10-31 17:14:41', '0000-00-00 00:00:00', 1, 1),
(591, 49880, 'الخالدية', 2, '2019-01-01 15:22:25', '0000-00-00 00:00:00', 1, 1),
(592, 49881, 'الخالدية', 2, '2019-01-01 15:48:46', '0000-00-00 00:00:00', 1, 1),
(593, 49882, 'الهدا', 2, '2019-01-01 17:14:34', '0000-00-00 00:00:00', 1, 1),
(594, 49883, 'العليا', 2, '2019-01-02 15:50:37', '0000-00-00 00:00:00', 1, 1),
(595, 49884, 'المخطط 5', 2, '2019-01-02 16:32:06', '0000-00-00 00:00:00', 1, 1),
(596, 49885, 'أجياد', 2, '2019-01-02 17:25:22', '0000-00-00 00:00:00', 1, 1),
(597, 49886, 'الروابي', 2, '2019-01-02 18:17:53', '0000-00-00 00:00:00', 1, 1),
(598, 49887, 'مجمع 257', 2, '2019-01-07 15:45:21', '0000-00-00 00:00:00', 1, 1),
(599, 49888, 'البديع', 2, '2019-01-31 18:40:27', '0000-00-00 00:00:00', 1, 1),
(600, 49889, 'السبيعية', 2, '2019-01-31 18:54:26', '0000-00-00 00:00:00', 1, 1),
(601, 49890, 'البشر', 2, '2019-01-31 19:27:21', '0000-00-00 00:00:00', 1, 1),
(602, 49891, 'معشي', 2, '2019-02-14 14:03:13', '0000-00-00 00:00:00', 1, 1),
(603, 49892, 'الجامعة', 2, '2019-02-26 18:34:11', '0000-00-00 00:00:00', 1, 1),
(604, 49893, 'الهجرة', 2, '2019-03-03 17:29:47', '0000-00-00 00:00:00', 1, 1),
(605, 49894, 'الهجلة', 2, '2019-03-14 19:43:56', '0000-00-00 00:00:00', 1, 1),
(606, 49895, 'الزرقاء', 2, '2019-03-20 13:53:02', '0000-00-00 00:00:00', 1, 1),
(607, 49896, 'الفيصلية', 2, '2019-03-20 15:17:45', '0000-00-00 00:00:00', 1, 1),
(608, 49897, 'المطار', 2, '2019-03-20 15:58:13', '0000-00-00 00:00:00', 1, 1),
(609, 49898, 'محايل عسير', 2, '2019-03-20 19:06:13', '0000-00-00 00:00:00', 1, 1),
(610, 49899, 'الخالدية', 2, '2019-03-20 19:25:11', '0000-00-00 00:00:00', 1, 1),
(611, 49900, 'المزروعية', 2, '2019-03-24 14:02:46', '0000-00-00 00:00:00', 1, 1),
(612, 49901, 'عتود', 2, '2019-03-24 14:41:58', '0000-00-00 00:00:00', 1, 1),
(613, 49902, 'الفهد', 2, '2019-03-24 15:32:14', '0000-00-00 00:00:00', 1, 1);
INSERT INTO `districts_text` (`DistrictTextID`, `DistrictID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(614, 49903, 'القطبية', 2, '2019-04-02 19:10:55', '0000-00-00 00:00:00', 1, 1),
(615, 49904, 'قباء', 2, '2019-04-04 18:13:13', '0000-00-00 00:00:00', 1, 1),
(616, 49905, 'الخالدية', 2, '2019-04-18 16:39:32', '0000-00-00 00:00:00', 1, 1),
(617, 49906, 'العزيزية', 2, '2019-05-14 16:40:44', '0000-00-00 00:00:00', 1, 1),
(618, 49907, 'الموظفين', 2, '2019-05-14 16:41:22', '0000-00-00 00:00:00', 1, 1),
(619, 49908, 'Al Shati', 1, '2019-11-03 11:38:55', '2019-11-03 11:38:55', 1, 1),
(620, 49908, 'الشاطئ', 2, '2019-11-03 11:38:55', '2019-11-03 11:39:10', 1, 1),
(621, 49909, 'An nasab', 1, '2019-11-08 11:32:08', '2019-11-08 11:32:08', 1, 1),
(622, 49909, 'An nasab', 2, '2019-11-08 11:32:08', '2019-11-08 11:32:08', 1, 1),
(623, 49910, 'Albadea', 1, '2019-11-08 11:42:03', '2019-11-08 11:42:03', 1, 1),
(624, 49910, 'Albadea', 2, '2019-11-08 11:42:03', '2019-11-08 11:42:03', 1, 1),
(625, 49911, 'AlKhuzama', 1, '2019-11-08 12:01:05', '2019-11-08 12:01:05', 1, 1),
(626, 49911, 'AlKhuzama', 2, '2019-11-08 12:01:05', '2019-11-08 12:01:05', 1, 1),
(627, 49912, 'Al Aziziyah', 1, '2019-11-08 12:11:10', '2019-11-08 12:11:10', 1, 1),
(628, 49912, 'Al Aziziyah', 2, '2019-11-08 12:11:10', '2019-11-08 12:11:10', 1, 1),
(629, 49913, 'Al Rayyan', 1, '2019-11-08 12:20:42', '2019-11-08 12:20:42', 1, 1),
(630, 49913, 'Al Rayyan', 2, '2019-11-08 12:20:42', '2019-11-08 12:20:42', 1, 1),
(631, 49914, 'Alnafal', 1, '2019-11-08 12:28:10', '2019-11-08 12:28:10', 1, 1),
(632, 49914, 'Alnafal', 2, '2019-11-08 12:28:10', '2019-11-08 12:28:10', 1, 1),
(633, 49915, 'Alyarmuk', 1, '2019-11-08 12:33:11', '2019-11-08 12:33:11', 1, 1),
(634, 49915, 'Alyarmuk', 2, '2019-11-08 12:33:11', '2019-11-08 12:33:11', 1, 1),
(635, 49916, 'Duraihimiyah', 1, '2019-11-08 12:37:53', '2019-11-08 12:37:53', 1, 1),
(636, 49916, 'Duraihimiyah', 2, '2019-11-08 12:37:53', '2019-11-08 12:37:53', 1, 1),
(637, 49917, 'Sultana', 1, '2019-11-08 12:41:39', '2019-11-08 12:41:39', 1, 1),
(638, 49917, 'Sultana', 2, '2019-11-08 12:41:39', '2019-11-08 12:41:39', 1, 1),
(639, 49918, 'Nahdah', 1, '2019-11-08 12:45:55', '2019-11-08 12:45:55', 1, 1),
(640, 49918, 'Nahdah', 2, '2019-11-08 12:45:55', '2019-11-08 12:45:55', 1, 1),
(641, 49919, 'Al Aziziyah R', 1, '2019-11-08 12:53:54', '2019-11-08 12:53:54', 1, 1),
(642, 49919, 'Al Aziziyah R', 2, '2019-11-08 12:53:54', '2019-11-08 12:53:54', 1, 1),
(643, 49920, 'Al Hamam ', 1, '2019-11-08 13:07:01', '2019-11-08 13:07:01', 1, 1),
(644, 49920, 'Al Hamam ', 2, '2019-11-08 13:07:01', '2019-11-08 13:07:01', 1, 1),
(645, 49921, 'Al Gharbi', 1, '2019-11-08 13:07:11', '2019-11-08 13:07:11', 1, 1),
(646, 49921, 'Al Gharbi', 2, '2019-11-08 13:07:11', '2019-11-08 13:07:11', 1, 1),
(647, 49922, 'Ashuhada Ashamaliyyah', 1, '2019-11-08 13:11:50', '2019-11-08 13:11:50', 1, 1),
(648, 49922, 'Ashuhada Ashamaliyyah', 2, '2019-11-08 13:11:50', '2019-11-08 13:11:50', 1, 1),
(649, 49923, 'Al Qutbiyyah', 1, '2019-11-08 13:16:27', '2019-11-08 13:16:27', 1, 1),
(650, 49923, 'Al Qutbiyyah', 2, '2019-11-08 13:16:27', '2019-11-08 13:16:27', 1, 1),
(651, 49924, 'Shubra ', 1, '2019-11-08 13:23:05', '2019-11-08 13:23:05', 1, 1),
(652, 49924, 'Shubra ', 2, '2019-11-08 13:23:05', '2019-11-08 13:23:05', 1, 1),
(653, 49925, 'Shadhah ', 1, '2019-11-08 13:25:48', '2019-11-08 13:25:48', 1, 1),
(654, 49925, 'Shadhah ', 2, '2019-11-08 13:25:48', '2019-11-08 13:25:48', 1, 1),
(655, 49926, 'Al Wabra', 1, '2019-11-08 13:30:28', '2019-11-08 13:30:28', 1, 1),
(656, 49926, 'Al Wabra', 2, '2019-11-08 13:30:28', '2019-11-08 13:30:28', 1, 1),
(657, 49927, 'Al Hadiqah', 1, '2019-11-08 13:33:31', '2019-11-08 13:33:31', 1, 1),
(658, 49927, 'Al Hadiqah', 2, '2019-11-08 13:33:31', '2019-11-08 13:33:31', 1, 1),
(659, 49928, 'Rabea', 1, '2019-11-08 13:43:52', '2019-11-08 13:43:52', 1, 1),
(660, 49928, 'Rabea', 2, '2019-11-08 13:43:52', '2019-11-08 13:43:52', 1, 1),
(661, 49929, 'Al Bishr Buraydah', 1, '2019-11-08 13:51:56', '2019-11-08 13:51:56', 1, 1),
(662, 49929, 'Al Bishr Buraydah', 2, '2019-11-08 13:51:56', '2019-11-08 13:51:56', 1, 1),
(665, 49931, 'Al Amir Fawwaz Al Junoobi', 1, '2019-11-11 07:58:32', '2019-11-11 07:58:32', 1, 1),
(666, 49931, 'Al Amir Fawwaz Al Junoobi', 2, '2019-11-11 07:58:32', '2019-11-11 07:58:32', 1, 1),
(667, 49932, 'Almanar', 1, '2019-11-11 08:02:06', '2019-11-11 08:02:06', 1, 1),
(668, 49932, 'Almanar', 2, '2019-11-11 08:02:06', '2019-11-11 08:02:06', 1, 1),
(669, 49933, 'Al Naeem', 1, '2019-11-11 08:05:11', '2019-11-11 08:05:11', 1, 1),
(670, 49933, 'Al Naeem', 2, '2019-11-11 08:05:11', '2019-11-11 08:05:11', 1, 1),
(671, 49934, 'Nuqrah', 1, '2019-11-11 08:10:02', '2019-11-11 08:10:02', 1, 1),
(672, 49934, 'Nuqrah', 2, '2019-11-11 08:10:02', '2019-11-11 08:10:02', 1, 1),
(673, 49935, 'Aja', 1, '2019-11-11 08:13:35', '2019-11-11 08:13:35', 1, 1),
(674, 49935, 'Aja', 2, '2019-11-11 08:13:35', '2019-11-11 08:13:35', 1, 1),
(675, 49936, 'Naqrah', 1, '2019-11-11 08:17:27', '2019-11-11 08:17:27', 1, 1),
(676, 49936, 'Naqrah', 2, '2019-11-11 08:17:27', '2019-11-11 08:17:27', 1, 1),
(677, 49937, 'Al Baladiyah', 1, '2019-11-11 08:21:22', '2019-11-11 08:21:22', 1, 1),
(678, 49937, 'Al Baladiyah', 2, '2019-11-11 08:21:22', '2019-11-11 08:21:22', 1, 1),
(679, 49938, 'Al Irq Al Janubi', 1, '2019-11-11 08:24:16', '2019-11-11 08:24:16', 1, 1),
(680, 49938, 'Al Irq Al Janubi', 2, '2019-11-11 08:24:16', '2019-11-11 08:24:16', 1, 1),
(681, 49939, 'Tayyib Al Ism', 1, '2019-11-11 08:29:23', '2019-11-11 08:29:23', 1, 1),
(682, 49939, 'Tayyib Al Ism', 2, '2019-11-11 08:29:23', '2019-11-11 08:29:23', 1, 1),
(683, 49940, 'alsharbatli', 1, '2019-11-11 09:31:30', '2019-11-11 09:31:30', 1, 1),
(684, 49940, 'alsharbatli', 2, '2019-11-11 09:31:30', '2019-11-11 09:31:30', 1, 1),
(685, 49941, 'Akwaha', 1, '2019-11-11 09:40:28', '2019-11-11 09:40:28', 1, 1),
(686, 49941, 'Akwaha', 2, '2019-11-11 09:40:28', '2019-11-11 09:40:28', 1, 1),
(687, 49942, 'Yanbu Al Sinaiyah,', 1, '2019-11-11 09:43:28', '2019-11-11 09:43:28', 1, 1),
(688, 49942, 'Yanbu Al Sinaiyah,', 2, '2019-11-11 09:43:28', '2019-11-11 09:43:28', 1, 1),
(689, 49943, 'Al Aziziyah Meeca', 1, '2019-11-11 09:48:47', '2019-11-11 09:48:47', 1, 1),
(690, 49943, 'Al Aziziyah Meeca', 2, '2019-11-11 09:48:47', '2019-11-11 09:48:47', 1, 1),
(691, 49944, 'Al Jamiah', 1, '2019-11-11 09:56:48', '2019-11-11 09:56:48', 1, 1),
(692, 49944, 'Al Jamiah', 2, '2019-11-11 09:56:48', '2019-11-11 09:56:48', 1, 1),
(693, 49945, 'Al Shoqiyah', 1, '2019-11-11 10:00:41', '2019-11-11 10:00:41', 1, 1),
(694, 49945, 'Al Shoqiyah', 2, '2019-11-11 10:00:41', '2019-11-11 10:00:41', 1, 1),
(695, 49946, 'Asharai', 1, '2019-11-11 10:04:53', '2019-11-11 10:04:53', 1, 1),
(696, 49946, 'Asharai', 2, '2019-11-11 10:04:53', '2019-11-11 10:04:53', 1, 1),
(697, 49947, 'Shubaikah', 1, '2019-11-11 10:08:54', '2019-11-11 10:08:54', 1, 1),
(698, 49947, 'Shubaikah', 2, '2019-11-11 10:08:54', '2019-11-11 10:08:54', 1, 1),
(699, 49948, 'Alawali', 1, '2019-11-11 10:12:49', '2019-11-11 10:12:49', 1, 1),
(700, 49948, 'Alawali', 2, '2019-11-11 10:12:49', '2019-11-11 10:12:49', 1, 1),
(701, 49949, 'Aljawhara', 1, '2019-11-11 10:17:36', '2019-11-11 10:17:36', 1, 1),
(702, 49949, 'Aljawhara', 2, '2019-11-11 10:17:36', '2019-11-11 10:17:36', 1, 1),
(703, 49950, ' Al Muhammadiyah', 1, '2019-11-11 10:20:48', '2019-11-11 10:20:48', 1, 1),
(704, 49950, ' Al Muhammadiyah', 2, '2019-11-11 10:20:48', '2019-11-11 10:20:48', 1, 1),
(705, 49951, 'Aljouf', 1, '2019-11-11 10:28:40', '2019-11-11 10:32:34', 1, 1),
(706, 49951, 'Aljouf', 2, '2019-11-11 10:28:40', '2019-11-11 10:28:40', 1, 1),
(707, 49952, 'Alshifa', 1, '2019-11-11 10:32:47', '2019-11-11 10:32:47', 1, 1),
(708, 49952, 'Alshifa', 2, '2019-11-11 10:32:47', '2019-11-11 10:32:47', 1, 1),
(709, 49953, 'Almatr', 1, '2019-11-11 10:35:05', '2019-11-11 10:35:05', 1, 1),
(710, 49953, 'Almatr', 2, '2019-11-11 10:35:05', '2019-11-11 10:35:05', 1, 1),
(711, 49954, 'Alfahad', 1, '2019-11-11 11:43:33', '2019-11-11 11:43:33', 1, 1),
(712, 49954, 'Alfahad', 2, '2019-11-11 11:43:33', '2019-11-11 11:43:33', 1, 1),
(713, 49955, 'Al Faydah', 1, '2019-11-11 12:00:46', '2019-11-11 12:00:46', 1, 1),
(714, 49955, 'Al Faydah', 2, '2019-11-11 12:00:46', '2019-11-11 12:00:46', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `Email_templateID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`Email_templateID`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'uploads/email_templates/1210446676201807020635331.jpg', 0, 0, 1, '2018-07-02 17:08:45', '2019-03-13 14:04:02', 1, 1),
(4, 'uploads/email_templates/379150302201807020631102.jpg', 1, 0, 1, '2018-07-02 18:10:31', '2019-04-05 09:16:16', 1, 1),
(5, 'uploads/email_templates/414693546201807020612353.jpg', 2, 0, 1, '2018-07-02 18:35:12', '2019-04-09 11:37:19', 1, 1),
(6, '', 3, 0, 1, '2019-04-05 09:22:13', '2019-04-09 11:38:19', 1, 1),
(7, '', 4, 0, 1, '2019-11-05 09:02:29', '2019-11-07 12:11:41', 1, 1),
(8, '', 5, 0, 1, '2020-05-11 10:53:58', '2020-05-11 10:53:58', 1, 1),
(9, '', 6, 0, 1, '2020-05-11 11:45:50', '2020-05-11 11:45:50', 1, 1),
(10, '', 7, 0, 1, '2020-05-11 14:45:48', '2020-05-11 14:45:48', 1, 1),
(11, '', 8, 0, 1, '2020-05-12 08:29:01', '2020-05-12 08:33:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates_text`
--

CREATE TABLE `email_templates_text` (
  `Email_templateTextID` int(11) NOT NULL,
  `Email_templateID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Heading` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates_text`
--

INSERT INTO `email_templates_text` (`Email_templateTextID`, `Email_templateID`, `Title`, `Heading`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Signup', 'Signup', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>Thank you for registering with Chocomood. We hope to provide you the best of our services.</p>\r\n<p>Your login details are:</p>\r\n<p>Email: {{email}}</p>\r\n<p>Password: {{password}}</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 1, '2018-07-02 17:08:45', '2019-03-13 14:04:02', 1, 1),
(4, 4, 'Reset Password', 'Reset Password', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>We received your request to reset your password at chocomood. We have generated a new password for you. Please login using your new password at chocomood and change your password from profile section.</p>\r\n<p>Here are your login details at chocomood.</p>\r\n<p>&nbsp;</p>\r\n<p>Email: {{email}}</p>\r\n<p>Password: {{password}}</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 1, '2018-07-02 18:10:31', '2019-04-05 09:16:16', 1, 1),
(5, 5, 'Order Status Changed', 'Order Status Changed', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for your order no. {{order_no}} is changed to {{order_status}}.</p>', 1, '2018-07-02 18:35:12', '2019-04-09 11:37:19', 1, 1),
(6, 5, 'Order Status Changed', 'Order Status Changed', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for your order no. {{order_no}} is changed to {{order_status}}.</p>', 2, '2018-07-02 18:36:55', '2019-04-09 11:37:33', 1, 1),
(7, 1, 'Signup At Chocomood', 'Signup at Chocomood', '<p><strong>Hi {{fullname}} ,</strong></p>\r\n<p>Thank you for registering with Chocomood. We hope to provide you the best of our services.</p>\r\n<p>Your login details are:</p>\r\n<p>Email: {{email}}</p>\r\n<p>Password: {{password}}</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 2, '2019-03-01 16:04:09', '2019-03-01 16:04:09', 1, 1),
(8, 4, 'Reset Password', 'Reset Password', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>We received your request to reset your password at chocomood. We have generated a new password for you. Please login using your new password at chocomood and change your password from profile section.</p>\r\n<p>Here are your login details at chocomood.</p>\r\n<p>&nbsp;</p>\r\n<p>Email: {{email}}</p>\r\n<p>Password: {{password}}</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 2, '2018-07-02 18:10:31', '2019-04-05 09:16:39', 1, 1),
(9, 6, 'Order Status Changed For Assigned Order Driver', 'Order Status Changed For Assigend Order', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for your assigned order no. {{order_no}} is changed to {{order_status}}.</p>', 1, '2019-04-05 09:22:13', '2019-04-09 11:38:19', 1, 1),
(10, 6, 'Order Status Changed For Assigned Order Driver Ar', 'Order Status Changed For Assigend Order', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for your assigned order no. {{order_no}} is changed to {{order_status}}.</p>', 2, '2019-04-05 09:23:02', '2019-04-09 11:38:34', 1, 1),
(11, 7, 'Delivery OTP', 'Delivery OTP', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>Your order # {{order_number}} is assigned to the delivery boy with delivery OTP {{OTP}}. Please give this OTP to the delivery boy who gives you parcel. You can also ask him to scan QR code sent in email.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 1, '2019-11-05 09:02:29', '2019-11-07 12:11:41', 1, 1),
(12, 7, 'Delivery OTP', 'Delivery OTP', '<p><strong>Hi {{name}} ,</strong></p>\r\n<p>Your order is assigned to the delivery boy with delivery OTP {{OTP}}. Please give this OTP to the delivery boy who gives you parcel.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Thanks.</strong></p>', 2, '2019-11-05 09:02:29', '2019-11-05 09:02:29', 1, 1),
(13, 8, 'Cancelled Order (Collected From Store)', 'Cancelled Order', '<p><strong>Dear {{name}}</strong>&nbsp;,</p>\r\n<p>Your order no. {{order_no}} is cancelled as you have not collected it from store within 3 days<br />Thanks</p>', 1, '2020-05-11 10:53:58', '2020-05-11 10:53:58', 1, 1),
(14, 8, 'Cancelled Order (Collected From Store)', 'Cancelled Order', '<p><strong>Dear {{name}}</strong>&nbsp;,</p>\r\n<p>Your order no. {{order_no}} is cancelled as you have not collected it from store within 3 days<br />Thanks</p>', 2, '2020-05-11 10:53:58', '2020-05-11 10:53:58', 1, 1),
(15, 9, 'Cancelled Order (Collected From Store) To Admin', 'Order Cancelled', '<p><strong>Dear {{name}}</strong>&nbsp;,</p>\r\n<p>Order no. {{order_no}} is cancelled as&nbsp;customer&nbsp;is not collected it from store within 3 days<br />Thanks</p>', 1, '2020-05-11 11:45:50', '2020-05-11 11:45:50', 1, 1),
(16, 9, 'Cancelled Order (Collected From Store) To Admin', 'Order Cancelled', '<p><strong>Dear {{name}}</strong>&nbsp;,</p>\r\n<p>Order no. {{order_no}} is cancelled as&nbsp;customer&nbsp;is not collected it from store within 3 days<br />Thanks</p>', 2, '2020-05-11 11:45:50', '2020-05-11 11:45:50', 1, 1),
(17, 10, 'Order Status Changed (To Store Admin)', 'Order Status Changed', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for order no. {{order_no}} is changed to {{order_status}}.</p>', 1, '2020-05-11 14:45:48', '2020-05-11 14:45:48', 1, 1),
(18, 10, 'Order Status Changed (To Store Admin)', 'Order Status Changed', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Status for order no. {{order_no}} is changed to {{order_status}}.</p>', 2, '2020-05-11 14:45:48', '2020-05-11 14:45:48', 1, 1),
(19, 11, 'Ticket Response Email If Customer Is Offline', 'Ticket Response', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Chocomood admin response on your ticket has ordered no. {{order_no}} with the message, {{message}}.</p>', 1, '2020-05-12 08:29:01', '2020-05-12 08:33:19', 1, 1),
(20, 11, 'Ticket Response Email If Customer Is Offline', 'Ticket Response', '<p><strong>Dear {{name}} ,</strong></p>\r\n<p>Chocomood admin response on your ticket has ordered no. {{order_no}} with the message, {{message}}.</p>', 2, '2020-05-12 08:29:01', '2020-05-12 08:29:01', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `FaqID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `faqs_text`
--

CREATE TABLE `faqs_text` (
  `FaqTextID` int(11) NOT NULL,
  `FaqID` int(11) NOT NULL,
  `Title` text NOT NULL,
  `Answer` varchar(2000) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `home_slider_images`
--

CREATE TABLE `home_slider_images` (
  `HomeSliderImageID` int(11) NOT NULL,
  `UrlLink` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_slider_images`
--

INSERT INTO `home_slider_images` (`HomeSliderImageID`, `UrlLink`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'https://chocomood.henka.tech', 'uploads/images/2731452030420190503021557img1.png', 0, 0, 0, '2019-05-03 14:57:15', '2019-11-27 00:30:25', 1, 1),
(4, 'https://chocomood.henka.tech', 'uploads/images/3434726870420191214055120Dates.jpg', 1, 0, 0, '2019-12-14 17:18:22', '2019-12-14 17:24:01', 1, 1),
(5, 'https://chocomood.henka.tech/', 'uploads/images/3733323228820200414073429circles_background_size_monochrome_28137_1920x1080.jpg', 2, 0, 0, '2020-04-14 19:29:16', '2020-04-14 19:29:34', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_slider_images_text`
--

CREATE TABLE `home_slider_images_text` (
  `HomeSliderImageTextID` int(11) NOT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `HomeSliderImageID` int(11) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_slider_images_text`
--

INSERT INTO `home_slider_images_text` (`HomeSliderImageTextID`, `Title`, `HomeSliderImageID`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, NULL, 1, '<h2>Chocolate <span>is the happiness that you can..</span></h2>\r\n<h4>eat!</h4>', 1, '2019-05-22 00:00:00', '2019-11-27 00:30:25', 1, 1),
(2, NULL, 1, '<h2>Chocolate <span>is the happiness that you can..</span></h2>\r\n<h4>eat!</h4>\r\narabic', 2, '2019-05-22 00:00:00', '2019-05-03 15:26:09', 1, 1),
(7, NULL, 4, '<h2><span><strong>Ramadhan</strong></span></h2>\r\n<br />\r\n<h4><span>Collection</span></h4>', 1, '2019-12-14 17:18:22', '2019-12-14 17:24:01', 1, 1),
(8, NULL, 4, '<h2><span><strong>Ramadhan</strong></span></h2>\r\n<br />\r\n<h2>Collection</h2>', 2, '2019-12-14 17:18:22', '2019-12-14 17:18:22', 1, 1),
(9, NULL, 5, 'testing', 1, '2020-04-14 19:29:16', '2020-04-14 19:29:34', 1, 1),
(10, NULL, 5, 'testing', 2, '2020-04-14 19:29:16', '2020-04-14 19:29:16', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `LanguageID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `ModuleID` int(11) NOT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `IconClass` varchar(255) NOT NULL,
  `Slug` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`ModuleID`, `ParentID`, `IconClass`, `Slug`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 'settings', '#', 32, 0, 1, '2018-05-09 00:00:00', '2018-11-30 10:32:35', 1, 1),
(18, 1, ' mdi mdi-view-module', 'module', 0, 0, 1, '2018-04-19 15:22:06', '2018-04-19 15:22:06', 1, 1),
(22, 1, 'mdi mdi-yeast', 'role', 3, 0, 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(23, 0, 'account_circle', 'user', 8, 0, 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(32, 1, 'mdi mdi-city', 'city', 7, 0, 1, '2018-05-03 14:01:59', '2018-05-03 14:01:59', 1, 1),
(35, 1, 'mdi mdi-city', 'district', 6, 0, 1, '2018-05-03 15:49:43', '2018-05-03 15:49:43', 1, 1),
(36, 1, 'settings', 'Site_setting', 8, 0, 1, '2018-06-21 20:24:33', '2018-06-21 20:24:33', 1, 1),
(39, 72, ' mdi mdi-email', 'email_template', 9, 0, 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(40, 1, 'mdi mdi-format-list-bulleted', 'language', 10, 1, 1, '2018-07-04 14:12:11', '2018-07-04 14:12:23', 1, 1),
(43, 64, 'view_quilt', 'category', 3, 0, 1, '2018-10-05 00:00:00', '2018-11-30 09:30:09', 1, 1),
(44, 0, 'bookmarks', 'package', 3, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(45, 77, 'file_copy', 'page', 5, 0, 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(46, 0, 'shopping_cart', 'booking', 2, 1, 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(47, 46, '', 'booking/received', 6, 0, 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(48, 46, '', 'booking/assigned', 7, 0, 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(50, 46, '', 'booking/OnTheWay', 8, 0, 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(51, 46, '', 'booking/reached', 9, 0, 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(52, 46, '', 'booking/completed', 10, 0, 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(53, 46, '', 'booking/cancelled', 11, 0, 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(54, 46, '', 'booking', 5, 0, 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(55, 0, 'directions_car', 'vehicle', 6, 1, 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(56, 0, 'build', 'technician', 7, 1, 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(57, 72, 'card_giftcard', 'coupon', 8, 0, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(58, 0, 'shop_two', 'requests', 9, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(59, 46, 'apps', 'booking/overdue', 10, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(60, 72, 'feedback', 'notification', 11, 0, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(61, 0, 'store', 'store', 7, 0, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(62, 64, 'bookmark', 'product', 6, 0, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(63, 0, 'local_atm', '#Sales', 1, 0, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(64, 0, 'shop_two', '#Catalogs', 2, 0, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(65, 63, 'apps', 'orders', 9, 0, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(66, 63, 'apps', 'invoice', 10, 0, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(67, 63, 'apps', 'shipment', 11, 0, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(68, 63, 'apps', 'ticket', 12, 0, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(69, 63, 'apps', 'refund', 13, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(70, 0, 'face', '#customers', 3, 0, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(71, 70, 'apps', 'customerss', 15, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(72, 0, 'insert_chart_outlined', '#Marketing', 4, 0, 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(73, 72, 'apps', 'contactRequest', 17, 0, 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(74, 0, 'assignment', 'report', 6, 0, 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(75, 77, 'assignment', 'collection', 80, 0, 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(76, 1, '', 'nutrition', 8, 0, 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(77, 0, 'file_copy', '#content', 5, 0, 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(78, 77, '', 'home_slider_image', 6, 0, 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(79, 72, '', 'newsletter', 7, 0, 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(80, 1, '', 'taxShipmentCharges', 8, 0, 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(81, 70, '', 'customers/online', 9, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(82, 70, '', 'customers', 10, 0, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(83, 70, '', 'CustomerGroup', 11, 0, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(84, 63, 'local_offer', 'offer', 12, 0, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(85, 72, '', 'searchTag', 13, 0, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(86, 64, '', 'box', 14, 0, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(87, 72, '', 'abandonedCart', 15, 0, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(88, 0, 'bookmark', 'customize', 16, 0, 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(89, 1, 'feedback', 'tag', 17, 0, 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(90, 0, 'feedback', 'faq', 18, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_rights`
--

CREATE TABLE `modules_rights` (
  `ModuleRightID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `CanView` tinyint(4) NOT NULL,
  `CanAdd` tinyint(4) NOT NULL,
  `CanEdit` tinyint(4) NOT NULL,
  `CanDelete` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_rights`
--

INSERT INTO `modules_rights` (`ModuleRightID`, `ModuleID`, `RoleID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(13, 18, 1, 1, 1, 1, 1, '2018-04-19 15:22:06', '2018-06-29 17:44:17', 1, 1),
(17, 18, 2, 1, 1, 1, 1, '2018-05-02 11:37:19', '2018-05-02 11:37:19', 1, 1),
(20, 22, 1, 1, 1, 1, 1, '2018-05-02 12:10:33', '2018-06-29 17:44:17', 1, 1),
(21, 22, 2, 1, 1, 1, 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(22, 23, 1, 1, 1, 1, 1, '2018-05-02 12:44:19', '2018-06-29 17:44:17', 1, 1),
(23, 23, 2, 1, 1, 1, 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(40, 32, 1, 1, 1, 1, 1, '2018-05-03 14:01:59', '2018-06-29 17:44:17', 1, 1),
(41, 32, 2, 1, 1, 1, 1, '2018-05-03 14:01:59', '2018-05-03 14:01:59', 1, 1),
(46, 35, 1, 1, 1, 1, 1, '2018-05-03 15:49:43', '2018-06-29 17:44:17', 1, 1),
(47, 35, 2, 1, 1, 1, 1, '2018-05-03 15:49:43', '2018-05-03 15:49:43', 1, 1),
(52, 18, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(54, 22, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(55, 23, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(56, 32, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(57, 35, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2019-03-06 07:52:27', 1, 1),
(74, 1, 3, 1, 1, 1, 1, '2018-05-09 12:20:50', '2019-03-06 07:52:27', 1, 1),
(75, 1, 2, 1, 1, 1, 1, '2018-05-09 12:20:50', '2018-05-09 12:20:50', 1, 1),
(76, 1, 1, 1, 1, 1, 1, '2018-05-09 12:20:50', '2018-06-29 17:44:17', 1, 1),
(77, 36, 1, 1, 1, 1, 1, '2018-02-12 16:32:10', '2018-06-29 17:44:17', 1, 1),
(78, 36, 2, 1, 1, 1, 1, '2018-06-29 13:51:40', '2018-06-29 13:51:40', 1, 1),
(79, 36, 3, 0, 0, 0, 0, '2018-06-29 09:34:16', '2019-03-06 07:52:27', 1, 1),
(86, 39, 1, 1, 1, 1, 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(87, 39, 2, 1, 1, 1, 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(88, 39, 3, 0, 0, 0, 0, '2018-07-02 16:26:28', '2019-03-06 07:52:27', 1, 1),
(89, 40, 1, 1, 1, 1, 1, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(90, 40, 2, 1, 1, 1, 1, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(91, 40, 3, 0, 0, 0, 0, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(98, 43, 1, 1, 1, 1, 1, '2018-10-03 19:48:40', '2018-10-04 21:47:28', 1, 1),
(99, 43, 2, 1, 1, 1, 1, '2018-10-03 19:48:40', '2018-10-03 19:48:40', 1, 1),
(100, 43, 3, 0, 0, 0, 0, '2018-10-03 19:48:40', '2019-03-06 07:52:27', 1, 1),
(111, 43, 4, 1, 1, 1, 1, '2018-10-04 20:15:57', '2018-10-04 20:15:57', 1, 1),
(112, 44, 1, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(113, 44, 2, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(114, 44, 3, 0, 0, 0, 0, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(115, 45, 1, 1, 1, 1, 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(116, 45, 2, 1, 1, 1, 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(117, 45, 3, 0, 0, 0, 0, '2018-12-19 05:54:48', '2019-03-06 07:52:27', 1, 1),
(118, 46, 1, 1, 1, 1, 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(119, 46, 2, 1, 1, 1, 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(120, 46, 3, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(121, 47, 1, 1, 1, 1, 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(122, 47, 2, 1, 1, 1, 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(123, 47, 3, 0, 0, 0, 0, '2018-12-21 09:19:50', '2019-03-06 07:52:27', 1, 1),
(124, 48, 1, 1, 1, 1, 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(125, 48, 2, 1, 1, 1, 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(126, 48, 3, 0, 0, 0, 0, '2018-12-21 09:43:33', '2019-03-06 07:52:27', 1, 1),
(130, 50, 1, 1, 1, 1, 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(131, 50, 2, 1, 1, 1, 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(132, 50, 3, 0, 0, 0, 0, '2018-12-21 09:50:43', '2019-03-06 07:52:27', 1, 1),
(133, 51, 1, 1, 1, 1, 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(134, 51, 2, 1, 1, 1, 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(135, 51, 3, 0, 0, 0, 0, '2018-12-21 09:51:27', '2019-03-06 07:52:27', 1, 1),
(136, 52, 1, 1, 1, 1, 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(137, 52, 2, 1, 1, 1, 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(138, 52, 3, 0, 0, 0, 0, '2018-12-21 09:52:06', '2019-03-06 07:52:27', 1, 1),
(139, 53, 1, 1, 1, 1, 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(140, 53, 2, 1, 1, 1, 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(141, 53, 3, 0, 0, 0, 0, '2018-12-21 09:52:39', '2019-03-06 07:52:27', 1, 1),
(142, 54, 1, 1, 1, 1, 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(143, 54, 2, 1, 1, 1, 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(144, 54, 3, 0, 0, 0, 0, '2018-12-21 11:00:54', '2019-03-06 07:52:27', 1, 1),
(145, 55, 1, 1, 1, 1, 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(146, 55, 2, 1, 1, 1, 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(147, 55, 3, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(148, 56, 1, 1, 1, 1, 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(149, 56, 2, 1, 1, 1, 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(150, 56, 3, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(151, 57, 1, 1, 1, 1, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(152, 57, 2, 1, 1, 1, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(153, 57, 3, 0, 0, 0, 0, '2018-12-26 07:06:18', '2019-03-06 07:52:27', 1, 1),
(154, 58, 1, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(155, 58, 2, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(156, 58, 3, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(157, 59, 1, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(158, 59, 2, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(159, 59, 3, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(160, 60, 1, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(161, 60, 2, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(162, 60, 3, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-03-06 07:52:27', 1, 1),
(163, 61, 1, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(164, 61, 2, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(165, 61, 3, 0, 0, 0, 0, '2019-01-29 06:18:23', '2019-03-06 07:52:27', 1, 1),
(166, 62, 1, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(167, 62, 2, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(168, 62, 3, 0, 0, 0, 0, '2019-02-01 05:47:53', '2019-03-06 07:52:27', 1, 1),
(169, 1, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(170, 18, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(171, 22, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(172, 23, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(173, 32, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(174, 35, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(175, 36, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(176, 39, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(177, 40, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(178, 43, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(179, 44, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(180, 45, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(181, 46, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(182, 47, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(183, 48, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(184, 50, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(185, 51, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(186, 52, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(187, 53, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(188, 54, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(189, 55, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(190, 56, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(191, 57, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(192, 58, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(193, 59, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(194, 60, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(195, 61, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(196, 62, 4, 1, 1, 1, 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(197, 63, 1, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(198, 63, 2, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(199, 63, 3, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-03-06 07:52:27', 1, 1),
(200, 63, 4, 0, 0, 0, 0, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(201, 64, 1, 1, 1, 1, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(202, 64, 2, 1, 1, 1, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(203, 64, 3, 0, 0, 0, 0, '2019-02-01 11:11:30', '2019-03-06 07:52:27', 1, 1),
(204, 64, 4, 0, 0, 0, 0, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(205, 65, 1, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(206, 65, 2, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(207, 65, 3, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-03-06 07:52:27', 1, 1),
(208, 65, 4, 0, 0, 0, 0, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(209, 66, 1, 1, 1, 1, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(210, 66, 2, 1, 1, 1, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(211, 66, 3, 0, 0, 0, 0, '2019-02-01 11:16:07', '2019-03-06 07:52:27', 1, 1),
(212, 66, 4, 0, 0, 0, 0, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(213, 67, 1, 1, 1, 1, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(214, 67, 2, 1, 1, 1, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(215, 67, 3, 0, 0, 0, 0, '2019-02-01 11:17:01', '2019-03-06 07:52:27', 1, 1),
(216, 67, 4, 0, 0, 0, 0, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(217, 68, 1, 1, 1, 1, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(218, 68, 2, 1, 1, 1, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(219, 68, 3, 0, 0, 0, 0, '2019-02-01 11:17:56', '2019-03-06 07:52:27', 1, 1),
(220, 68, 4, 0, 0, 0, 0, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(221, 69, 1, 1, 1, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(222, 69, 2, 1, 1, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(223, 69, 3, 0, 0, 0, 0, '2019-02-01 11:19:27', '2019-03-06 07:52:27', 1, 1),
(224, 69, 4, 0, 0, 0, 0, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(225, 70, 1, 1, 1, 1, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(226, 70, 2, 1, 1, 1, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(227, 70, 3, 0, 0, 0, 0, '2019-02-01 11:20:46', '2019-03-06 07:52:27', 1, 1),
(228, 70, 4, 0, 0, 0, 0, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(229, 71, 1, 1, 1, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(230, 71, 2, 1, 1, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(231, 71, 3, 0, 0, 0, 0, '2019-02-01 11:22:25', '2019-03-06 07:52:27', 1, 1),
(232, 71, 4, 0, 0, 0, 0, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(233, 72, 1, 1, 1, 1, 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(234, 72, 2, 1, 1, 1, 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(235, 72, 3, 0, 0, 0, 0, '2019-02-01 11:23:19', '2019-03-06 07:52:27', 1, 1),
(236, 72, 4, 0, 0, 0, 0, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(237, 73, 1, 1, 1, 1, 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(238, 73, 2, 1, 1, 1, 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(239, 73, 3, 0, 0, 0, 0, '2019-02-01 11:24:43', '2019-03-06 07:52:27', 1, 1),
(240, 73, 4, 0, 0, 0, 0, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(241, 74, 1, 1, 1, 1, 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(242, 74, 2, 1, 1, 1, 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(243, 74, 3, 0, 0, 0, 0, '2019-02-01 11:29:13', '2019-03-06 07:52:27', 1, 1),
(244, 74, 4, 0, 0, 0, 0, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(245, 75, 1, 1, 1, 1, 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(246, 75, 2, 1, 1, 1, 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(247, 75, 3, 0, 0, 0, 0, '2019-02-01 13:02:53', '2019-03-06 07:52:27', 1, 1),
(248, 75, 4, 0, 0, 0, 0, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(249, 76, 1, 1, 1, 1, 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(250, 76, 2, 1, 1, 1, 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(251, 76, 3, 0, 0, 0, 0, '2019-02-06 07:02:37', '2019-03-06 07:52:27', 1, 1),
(252, 76, 4, 0, 0, 0, 0, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(253, 77, 1, 1, 1, 1, 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(254, 77, 2, 1, 1, 1, 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(255, 77, 3, 0, 0, 0, 0, '2019-02-12 11:27:55', '2019-03-06 07:52:27', 1, 1),
(256, 77, 4, 0, 0, 0, 0, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(257, 78, 1, 1, 1, 1, 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(258, 78, 2, 1, 1, 1, 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(259, 78, 3, 0, 0, 0, 0, '2019-02-12 11:43:22', '2019-03-06 07:52:27', 1, 1),
(260, 78, 4, 0, 0, 0, 0, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(261, 79, 1, 1, 1, 1, 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(262, 79, 2, 1, 1, 1, 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(263, 79, 3, 0, 0, 0, 0, '2019-02-19 12:11:34', '2019-03-06 07:52:27', 1, 1),
(264, 79, 4, 0, 0, 0, 0, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(265, 1, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(266, 18, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(267, 22, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(268, 23, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(269, 32, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(270, 35, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(271, 36, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(272, 39, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(273, 40, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(274, 43, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(275, 44, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(276, 45, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(277, 46, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(278, 47, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(279, 48, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(280, 50, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(281, 51, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(282, 52, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(283, 53, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(284, 54, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(285, 55, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(286, 56, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(287, 57, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(288, 58, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(289, 59, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(290, 60, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(291, 61, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(292, 62, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(293, 63, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(294, 64, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(295, 65, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(296, 66, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(297, 67, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(298, 68, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(299, 69, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(300, 70, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(301, 71, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(302, 72, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(303, 73, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(304, 74, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(305, 75, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(306, 76, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(307, 77, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(308, 78, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(309, 79, 5, 1, 1, 1, 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(310, 80, 1, 1, 1, 1, 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(311, 80, 2, 1, 1, 1, 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(312, 80, 3, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-03-06 07:52:27', 1, 1),
(313, 80, 4, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(314, 80, 5, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(315, 81, 1, 1, 1, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(316, 81, 2, 1, 1, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(317, 81, 3, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(318, 81, 4, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(319, 81, 5, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(320, 82, 1, 1, 1, 1, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(321, 82, 2, 1, 1, 1, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(322, 82, 3, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(323, 82, 4, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(324, 82, 5, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(325, 83, 1, 1, 1, 1, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(326, 83, 2, 1, 1, 1, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(327, 83, 3, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(328, 83, 4, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(329, 83, 5, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(330, 84, 1, 1, 1, 1, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(331, 84, 2, 1, 1, 1, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(332, 84, 3, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(333, 84, 4, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(334, 84, 5, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(335, 85, 1, 1, 1, 1, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(336, 85, 2, 1, 1, 1, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(337, 85, 3, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(338, 85, 4, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(339, 85, 5, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(340, 86, 1, 1, 1, 1, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(341, 86, 2, 1, 1, 1, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(342, 86, 3, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(343, 86, 4, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(344, 86, 5, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(345, 87, 1, 1, 1, 1, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(346, 87, 2, 1, 1, 1, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(347, 87, 3, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(348, 87, 4, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(349, 87, 5, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(350, 1, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(351, 18, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(352, 22, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(353, 23, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(354, 32, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(355, 35, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(356, 36, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(357, 39, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(358, 40, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(359, 43, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(360, 44, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(361, 45, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(362, 46, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(363, 47, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(364, 48, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(365, 50, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(366, 51, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(367, 52, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(368, 53, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(369, 54, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(370, 55, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(371, 56, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(372, 57, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(373, 58, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(374, 59, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(375, 60, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(376, 61, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(377, 62, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(378, 63, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(379, 64, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(380, 65, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(381, 66, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(382, 67, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(383, 68, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(384, 69, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(385, 70, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(386, 71, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(387, 72, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(388, 73, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(389, 74, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(390, 75, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(391, 76, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(392, 77, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(393, 78, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(394, 79, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(395, 80, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(396, 81, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(397, 82, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(398, 83, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(399, 84, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(400, 85, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(401, 86, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(402, 87, 6, 1, 1, 1, 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(403, 1, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(404, 18, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(405, 22, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(406, 23, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(407, 32, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(408, 35, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(409, 36, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(410, 39, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(411, 40, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(412, 43, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(413, 44, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(414, 45, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(415, 46, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(416, 47, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(417, 48, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(418, 50, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(419, 51, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(420, 52, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(421, 53, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(422, 54, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(423, 55, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(424, 56, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(425, 57, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(426, 58, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(427, 59, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(428, 60, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(429, 61, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(430, 62, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(431, 63, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(432, 64, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(433, 65, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(434, 66, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(435, 67, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(436, 68, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(437, 69, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(438, 70, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(439, 71, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(440, 72, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(441, 73, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(442, 74, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(443, 75, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(444, 76, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(445, 77, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(446, 78, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(447, 79, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(448, 80, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(449, 81, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(450, 82, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(451, 83, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(452, 84, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(453, 85, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(454, 86, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(455, 87, 7, 1, 1, 1, 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(456, 1, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(457, 18, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(458, 22, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(459, 23, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(460, 32, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(461, 35, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(462, 36, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(463, 39, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(464, 40, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(465, 43, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(466, 44, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(467, 45, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(468, 46, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(469, 47, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(470, 48, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(471, 50, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(472, 51, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(473, 52, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(474, 53, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(475, 54, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(476, 55, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(477, 56, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(478, 57, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(479, 58, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(480, 59, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(481, 60, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(482, 61, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(483, 62, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(484, 63, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(485, 64, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(486, 65, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(487, 66, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(488, 67, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(489, 68, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(490, 69, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(491, 70, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(492, 71, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(493, 72, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(494, 73, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(495, 74, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(496, 75, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(497, 76, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(498, 77, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(499, 78, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(500, 79, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(501, 80, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(502, 81, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(503, 82, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(504, 83, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(505, 84, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(506, 85, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(507, 86, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(508, 87, 8, 1, 1, 1, 1, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1),
(509, 88, 1, 1, 1, 1, 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(510, 88, 2, 1, 1, 1, 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(511, 88, 3, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(512, 88, 4, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(513, 88, 5, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(514, 88, 6, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(515, 88, 7, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(516, 88, 8, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(517, 89, 1, 1, 1, 1, 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(518, 89, 2, 1, 1, 1, 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(519, 89, 3, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(520, 89, 4, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(521, 89, 5, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(522, 89, 6, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(523, 89, 7, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(524, 89, 8, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(525, 90, 1, 1, 1, 1, 1, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(526, 90, 2, 1, 1, 1, 1, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(527, 90, 3, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(528, 90, 4, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(529, 90, 5, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(530, 90, 6, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(531, 90, 7, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(532, 90, 8, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_text`
--

CREATE TABLE `modules_text` (
  `ModuleTextID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_text`
--

INSERT INTO `modules_text` (`ModuleTextID`, `ModuleID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 18, 'Module', 1, '2018-04-19 15:22:06', '2018-04-19 15:22:06', 1, 1),
(5, 22, 'Roles', 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(6, 23, 'Backend Users', 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(15, 32, 'City', 1, '2018-05-03 14:01:59', '2018-05-03 14:01:59', 1, 1),
(18, 35, 'District', 1, '2018-05-03 15:49:43', '2018-05-03 15:49:43', 1, 1),
(26, 1, 'Miscellaneous', 1, '2018-05-09 00:00:00', '2018-11-30 10:32:35', 1, 1),
(27, 36, 'Site Setting', 1, '2018-06-29 07:34:18', '2018-06-29 07:34:18', 1, 1),
(30, 39, 'Email Templates', 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(31, 40, 'Languages ', 1, '2018-07-04 14:12:11', '2018-07-04 14:12:23', 1, 1),
(32, 43, 'Categories', 1, '2018-10-05 00:00:00', '2018-11-30 09:30:09', 1, 1),
(33, 44, 'Packages', 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(34, 45, 'Content Pages', 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(35, 46, 'Orders', 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(36, 47, 'Newly Received', 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(37, 48, 'Assigned', 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(39, 50, 'On The Way', 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(40, 51, 'Reached', 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(41, 52, 'Completed', 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(42, 53, 'Cancelled', 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(43, 54, 'All Bookings', 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(44, 55, 'Vehicles', 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(45, 56, 'Technicians', 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(46, 57, 'Gift Vouchers', 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(47, 58, 'Package Requests', 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(48, 59, 'Overdue', 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(49, 60, 'Send Notifications', 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(50, 61, 'Stores', 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(51, 62, 'Products', 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(52, 63, 'Sales', 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(53, 64, 'Catalogs', 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(54, 65, 'Orders', 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(55, 66, 'Invoices', 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(56, 67, 'Shipments', 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(57, 68, 'Tickets', 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(58, 69, 'Refunds', 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(59, 70, 'Customers', 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(60, 71, 'All Customerss', 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(61, 72, 'Marketing', 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(62, 73, 'Feedback & CV Collection', 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(63, 74, 'Reports', 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(64, 75, 'Home Grid Images', 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(65, 76, 'Nutrition Facts', 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(66, 77, 'Site Content', 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(67, 78, 'Home Slider Images', 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(68, 79, 'Newsletters', 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(69, 80, 'Taxes and Shipment Methods', 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(70, 81, 'Online Customers', 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(71, 82, 'All Customers', 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(72, 83, 'Customer Groups', 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(73, 84, 'Discounts', 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(74, 85, 'Searched Tags', 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(75, 86, 'Boxes', 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(76, 87, 'Abandoned Carts', 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(77, 88, 'Customize', 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(78, 89, 'Tags', 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(79, 90, 'FAQ', 1, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_users_rights`
--

CREATE TABLE `modules_users_rights` (
  `ModuleRightID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `CanView` tinyint(4) NOT NULL,
  `CanAdd` tinyint(4) NOT NULL,
  `CanEdit` tinyint(4) NOT NULL,
  `CanDelete` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_users_rights`
--

INSERT INTO `modules_users_rights` (`ModuleRightID`, `ModuleID`, `UserID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(10, 1, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(11, 18, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(12, 22, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(13, 23, 7, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(14, 32, 7, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(15, 35, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(16, 36, 7, 1, 1, 1, 1, '2018-06-29 12:42:13', '2018-06-29 12:42:13', 1, 1),
(17, 1, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(18, 18, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(19, 22, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(20, 23, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(21, 32, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(22, 35, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2019-03-18 10:37:10', 1, 1),
(23, 36, 1, 1, 1, 1, 1, '2018-06-29 12:42:13', '2019-03-18 10:37:10', 1, 1),
(28, 39, 1, 1, 1, 1, 1, '2018-07-02 16:26:28', '2019-03-18 10:37:10', 1, 1),
(29, 39, 7, 0, 0, 0, 0, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(30, 40, 1, 1, 1, 1, 1, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(31, 40, 7, 0, 0, 0, 0, '2018-07-04 14:12:11', '2018-07-04 14:12:11', 1, 1),
(48, 43, 1, 1, 1, 1, 1, '2018-10-03 19:48:40', '2019-03-18 10:37:10', 1, 1),
(115, 43, 32, 0, 0, 0, 0, '2018-10-04 21:51:22', '2018-10-04 21:52:17', 1, 1),
(123, 45, 1, 1, 1, 1, 1, '2018-10-09 07:12:05', '2019-03-18 10:37:10', 1, 1),
(124, 45, 39, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(125, 45, 40, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(126, 45, 41, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(127, 45, 42, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(128, 45, 43, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(129, 45, 44, 1, 1, 1, 1, '2018-10-09 07:12:05', '2018-10-09 07:12:05', 1, 1),
(130, 47, 1, 1, 1, 1, 1, '2018-11-01 06:32:12', '2019-03-18 10:37:10', 1, 1),
(131, 47, 51, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(132, 47, 52, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(133, 47, 53, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(134, 47, 54, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(135, 47, 55, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(136, 47, 56, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(137, 47, 57, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(138, 47, 58, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(139, 47, 59, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(140, 47, 60, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(141, 47, 61, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(142, 47, 62, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-11-01 06:32:12', 1, 1),
(143, 46, 1, 1, 1, 1, 1, '2018-11-01 06:32:12', '2018-12-20 08:27:27', 1, 1),
(144, 48, 1, 1, 1, 1, 1, '2018-11-01 07:29:59', '2019-03-18 10:37:10', 1, 1),
(145, 48, 51, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(146, 48, 52, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(147, 48, 53, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(148, 48, 54, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(149, 48, 55, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(150, 48, 56, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(151, 48, 57, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(152, 48, 58, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(153, 48, 59, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(154, 48, 60, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(155, 48, 61, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(156, 48, 62, 1, 1, 1, 1, '2018-11-01 07:29:59', '2018-11-01 07:29:59', 1, 1),
(170, 50, 1, 1, 1, 1, 1, '2018-11-15 08:35:05', '2019-03-18 10:37:10', 1, 1),
(171, 50, 51, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(172, 50, 52, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(173, 50, 53, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(174, 50, 54, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(175, 50, 55, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(176, 50, 56, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(177, 50, 57, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(178, 50, 58, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(179, 50, 59, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(180, 50, 60, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(181, 50, 61, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(182, 50, 62, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(183, 50, 63, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(184, 50, 64, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(185, 50, 65, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(186, 50, 66, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(187, 50, 67, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(188, 50, 68, 1, 1, 1, 1, '2018-11-15 08:35:05', '2018-11-15 08:35:05', 1, 1),
(189, 51, 1, 1, 1, 1, 1, '2018-11-15 10:21:49', '2019-03-18 10:37:10', 1, 1),
(190, 51, 51, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(191, 51, 52, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(192, 51, 53, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(193, 51, 54, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(194, 51, 55, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(195, 51, 56, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(196, 51, 57, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(197, 51, 58, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(198, 51, 59, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(199, 51, 60, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(200, 51, 61, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(201, 51, 62, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(202, 51, 63, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(203, 51, 64, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(204, 51, 65, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(205, 51, 66, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(206, 51, 67, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(207, 51, 68, 1, 1, 1, 1, '2018-11-15 10:21:49', '2018-11-15 10:21:49', 1, 1),
(208, 52, 1, 1, 1, 1, 1, '2018-11-16 11:22:54', '2019-03-18 10:37:10', 1, 1),
(209, 52, 51, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(210, 52, 52, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(211, 52, 53, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(212, 52, 54, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(213, 52, 55, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(214, 52, 56, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(215, 52, 57, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(216, 52, 58, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(217, 52, 59, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(218, 52, 60, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(219, 52, 61, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(220, 52, 62, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(221, 52, 63, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(222, 52, 64, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(223, 52, 65, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(224, 52, 66, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(225, 52, 67, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(226, 52, 68, 1, 1, 1, 1, '2018-11-16 11:22:54', '2018-11-16 11:22:54', 1, 1),
(227, 53, 1, 1, 1, 1, 1, '2018-11-19 07:30:34', '2019-03-18 10:37:10', 1, 1),
(228, 53, 51, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(229, 53, 52, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(230, 53, 53, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(231, 53, 54, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(232, 53, 55, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(233, 53, 56, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(234, 53, 57, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(235, 53, 58, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(236, 53, 59, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(237, 53, 60, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(238, 53, 61, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(239, 53, 62, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(240, 53, 63, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(241, 53, 64, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(242, 53, 65, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(243, 53, 66, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(244, 53, 67, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(245, 53, 68, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(246, 53, 69, 1, 1, 1, 1, '2018-11-19 07:30:34', '2018-11-19 07:30:34', 1, 1),
(247, 54, 1, 1, 1, 1, 1, '2018-11-22 07:40:38', '2019-03-18 10:37:10', 1, 1),
(248, 54, 51, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(249, 54, 52, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(250, 54, 53, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(251, 54, 54, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(252, 54, 55, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(253, 54, 56, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(254, 54, 57, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(255, 54, 58, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(256, 54, 59, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(257, 54, 60, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(258, 54, 61, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(259, 54, 62, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(260, 54, 63, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(261, 54, 64, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(262, 54, 65, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(263, 54, 66, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(264, 54, 67, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(265, 54, 68, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(266, 54, 69, 1, 1, 1, 1, '2018-11-22 07:40:38', '2018-11-22 07:40:38', 1, 1),
(267, 55, 1, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(268, 55, 51, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(269, 55, 52, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(270, 55, 53, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(271, 55, 54, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(272, 55, 55, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(273, 55, 56, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(274, 55, 57, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(275, 55, 58, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(276, 55, 59, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(277, 55, 60, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(278, 55, 61, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(279, 55, 62, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(280, 55, 63, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(281, 55, 64, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(282, 55, 65, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(283, 55, 66, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(284, 55, 67, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(285, 55, 68, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(286, 55, 69, 1, 1, 1, 1, '2018-11-22 13:47:48', '2018-11-22 13:47:48', 1, 1),
(287, 56, 1, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(288, 56, 51, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(289, 56, 52, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(290, 56, 53, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(291, 56, 54, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(292, 56, 55, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(293, 56, 56, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(294, 56, 57, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(295, 56, 58, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(296, 56, 59, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(297, 56, 60, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(298, 56, 61, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(299, 56, 62, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(300, 56, 63, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(301, 56, 64, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(302, 56, 65, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(303, 56, 66, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(304, 56, 67, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(305, 56, 68, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(306, 56, 69, 1, 1, 1, 1, '2018-11-26 12:55:27', '2018-11-26 12:55:27', 1, 1),
(307, 58, 1, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(308, 58, 51, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(309, 58, 52, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(310, 58, 53, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(311, 58, 54, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(312, 58, 55, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(313, 58, 56, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(314, 58, 57, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(315, 58, 58, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(316, 58, 59, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(317, 58, 60, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(318, 58, 61, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(319, 58, 62, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(320, 58, 63, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(321, 58, 64, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(322, 58, 65, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(323, 58, 66, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(324, 58, 67, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(325, 58, 68, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(326, 58, 69, 1, 1, 1, 1, '2018-11-27 12:23:51', '2018-11-27 12:23:51', 1, 1),
(327, 59, 1, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(328, 59, 51, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(329, 59, 52, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(330, 59, 53, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(331, 59, 54, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(332, 59, 55, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(333, 59, 56, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(334, 59, 57, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(335, 59, 58, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(336, 59, 59, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(337, 59, 60, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(338, 59, 61, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(339, 59, 62, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(340, 59, 63, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(341, 59, 64, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(342, 59, 65, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(343, 59, 66, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(344, 59, 67, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(345, 59, 68, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(346, 59, 69, 1, 1, 1, 1, '2018-11-28 09:29:03', '2018-11-28 09:29:03', 1, 1),
(347, 60, 1, 1, 1, 1, 1, '2018-11-28 09:31:07', '2019-03-18 10:37:10', 1, 1),
(348, 60, 51, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(349, 60, 52, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(350, 60, 53, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(351, 60, 54, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(352, 60, 55, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(353, 60, 56, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(354, 60, 57, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(355, 60, 58, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(356, 60, 59, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(357, 60, 60, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(358, 60, 61, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(359, 60, 62, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(360, 60, 63, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(361, 60, 64, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(362, 60, 65, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(363, 60, 66, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(364, 60, 67, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(365, 60, 68, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(366, 60, 69, 1, 1, 1, 1, '2018-11-28 09:31:07', '2018-11-28 09:31:07', 1, 1),
(367, 61, 1, 1, 1, 1, 1, '2018-11-28 09:32:02', '2019-03-18 10:37:10', 1, 1),
(368, 61, 51, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(369, 61, 52, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(370, 61, 53, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(371, 61, 54, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(372, 61, 55, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(373, 61, 56, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(374, 61, 57, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(375, 61, 58, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(376, 61, 59, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(377, 61, 60, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(378, 61, 61, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(379, 61, 62, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(380, 61, 63, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(381, 61, 64, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(382, 61, 65, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(383, 61, 66, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(384, 61, 67, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(385, 61, 68, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(386, 61, 69, 1, 1, 1, 1, '2018-11-28 09:32:02', '2018-11-28 09:32:02', 1, 1),
(387, 62, 1, 1, 1, 1, 1, '2018-11-30 08:20:18', '2019-03-18 10:37:10', 1, 1),
(388, 62, 77, 1, 1, 1, 1, '2018-11-30 08:20:18', '2018-11-30 08:20:18', 1, 1),
(389, 62, 78, 1, 1, 1, 1, '2018-11-30 08:20:18', '2018-11-30 08:20:18', 1, 1),
(390, 62, 79, 1, 1, 1, 1, '2018-11-30 08:20:18', '2018-11-30 08:20:18', 1, 1),
(391, 62, 80, 1, 1, 1, 1, '2018-11-30 08:20:18', '2018-11-30 08:20:18', 1, 1),
(392, 63, 1, 1, 1, 1, 1, '2018-11-30 08:24:03', '2019-03-18 10:37:10', 1, 1),
(393, 63, 77, 1, 1, 1, 1, '2018-11-30 08:24:03', '2018-11-30 08:24:03', 1, 1),
(394, 63, 78, 1, 1, 1, 1, '2018-11-30 08:24:03', '2018-11-30 08:24:03', 1, 1),
(395, 63, 79, 1, 1, 1, 1, '2018-11-30 08:24:03', '2018-11-30 08:24:03', 1, 1),
(396, 63, 80, 1, 1, 1, 1, '2018-11-30 08:24:03', '2018-11-30 08:24:03', 1, 1),
(397, 64, 1, 1, 1, 1, 1, '2018-11-30 08:28:16', '2019-03-18 10:37:10', 1, 1),
(398, 64, 77, 1, 1, 1, 1, '2018-11-30 08:28:16', '2018-11-30 08:28:16', 1, 1),
(399, 64, 78, 1, 1, 1, 1, '2018-11-30 08:28:16', '2018-11-30 08:28:16', 1, 1),
(400, 64, 79, 1, 1, 1, 1, '2018-11-30 08:28:16', '2018-11-30 08:28:16', 1, 1),
(401, 64, 80, 1, 1, 1, 1, '2018-11-30 08:28:16', '2018-11-30 08:28:16', 1, 1),
(402, 65, 1, 1, 1, 1, 1, '2018-11-30 09:34:59', '2019-03-18 10:37:10', 1, 1),
(403, 65, 77, 1, 1, 1, 1, '2018-11-30 09:34:59', '2018-11-30 09:34:59', 1, 1),
(404, 65, 78, 1, 1, 1, 1, '2018-11-30 09:34:59', '2018-11-30 09:34:59', 1, 1),
(405, 65, 79, 1, 1, 1, 1, '2018-11-30 09:34:59', '2018-11-30 09:34:59', 1, 1),
(406, 65, 80, 1, 1, 1, 1, '2018-11-30 09:34:59', '2018-11-30 09:34:59', 1, 1),
(407, 66, 1, 1, 1, 1, 1, '2018-11-30 09:37:50', '2019-03-18 10:37:10', 1, 1),
(408, 66, 77, 1, 1, 1, 1, '2018-11-30 09:37:50', '2018-11-30 09:37:50', 1, 1),
(409, 66, 78, 1, 1, 1, 1, '2018-11-30 09:37:50', '2018-11-30 09:37:50', 1, 1),
(410, 66, 79, 1, 1, 1, 1, '2018-11-30 09:37:50', '2018-11-30 09:37:50', 1, 1),
(411, 66, 80, 1, 1, 1, 1, '2018-11-30 09:37:50', '2018-11-30 09:37:50', 1, 1),
(412, 67, 1, 1, 1, 1, 1, '2018-12-11 06:50:04', '2019-03-18 10:37:10', 1, 1),
(413, 67, 77, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(414, 67, 78, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(415, 67, 79, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(416, 67, 80, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(417, 67, 81, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(418, 67, 82, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(419, 67, 83, 1, 1, 1, 1, '2018-12-11 06:50:04', '2018-12-11 06:50:04', 1, 1),
(420, 68, 1, 1, 1, 1, 1, '2018-12-11 06:51:20', '2019-03-18 10:37:10', 1, 1),
(421, 68, 77, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(422, 68, 78, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(423, 68, 79, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(424, 68, 80, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(425, 68, 81, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(426, 68, 82, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(427, 68, 83, 1, 1, 1, 1, '2018-12-11 06:51:20', '2018-12-11 06:51:20', 1, 1),
(428, 69, 1, 1, 1, 1, 1, '2018-12-11 06:53:12', '2019-03-18 10:37:10', 1, 1),
(429, 69, 77, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(430, 69, 78, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(431, 69, 79, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(432, 69, 80, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(433, 69, 81, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(434, 69, 82, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(435, 69, 83, 1, 1, 1, 1, '2018-12-11 06:53:12', '2018-12-11 06:53:12', 1, 1),
(436, 70, 1, 1, 1, 1, 1, '2018-12-11 06:59:04', '2019-03-18 10:37:10', 1, 1),
(437, 70, 77, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(438, 70, 78, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(439, 70, 79, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(440, 70, 80, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(441, 70, 81, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(442, 70, 82, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(443, 70, 83, 1, 1, 1, 1, '2018-12-11 06:59:04', '2018-12-11 06:59:04', 1, 1),
(444, 44, 1, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-20 08:27:27', 1, 1),
(445, 44, 77, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(446, 44, 78, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(447, 44, 79, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(448, 44, 80, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(449, 44, 81, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(450, 44, 82, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(451, 44, 83, 1, 1, 1, 1, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(452, 44, 85, 0, 0, 0, 0, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(453, 44, 86, 0, 0, 0, 0, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(454, 44, 87, 0, 0, 0, 0, '2018-12-17 07:07:41', '2018-12-17 07:07:41', 1, 1),
(455, 45, 1, 1, 1, 1, 1, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(456, 45, 90, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(457, 45, 91, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(458, 45, 92, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(459, 45, 93, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(460, 45, 94, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(461, 45, 95, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(462, 45, 96, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(463, 45, 97, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(464, 45, 98, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(465, 45, 99, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(466, 45, 100, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(467, 45, 101, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(468, 45, 102, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(469, 45, 103, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(470, 45, 104, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(471, 45, 105, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(472, 45, 106, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(473, 45, 107, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(474, 45, 108, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(475, 45, 109, 0, 0, 0, 0, '2018-12-19 05:54:48', '2018-12-19 05:54:48', 1, 1),
(476, 46, 1, 1, 1, 1, 1, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(477, 46, 90, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(478, 46, 91, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(479, 46, 92, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(480, 46, 93, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(481, 46, 94, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(482, 46, 95, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(483, 46, 96, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(484, 46, 97, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(485, 46, 98, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(486, 46, 99, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(487, 46, 100, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(488, 46, 101, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(489, 46, 102, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(490, 46, 103, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(491, 46, 104, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(492, 46, 105, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(493, 46, 106, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(494, 46, 107, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(495, 46, 108, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(496, 46, 109, 0, 0, 0, 0, '2018-12-19 06:36:39', '2018-12-19 06:36:39', 1, 1),
(497, 1, 114, 1, 1, 1, 1, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(498, 18, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(499, 22, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(500, 23, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(501, 32, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(502, 35, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(503, 36, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(504, 39, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(505, 40, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(506, 43, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(507, 44, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(508, 45, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(509, 46, 114, 0, 0, 0, 0, '2018-12-20 08:09:21', '2018-12-20 08:09:21', 1, 1),
(510, 47, 1, 1, 1, 1, 1, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(511, 47, 77, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(512, 47, 90, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(513, 47, 91, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(514, 47, 92, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(515, 47, 93, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(516, 47, 94, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(517, 47, 95, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(518, 47, 96, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(519, 47, 97, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(520, 47, 98, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(521, 47, 99, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(522, 47, 100, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(523, 47, 101, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(524, 47, 102, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(525, 47, 103, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(526, 47, 104, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(527, 47, 105, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(528, 47, 106, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(529, 47, 107, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(530, 47, 108, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(531, 47, 109, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(532, 47, 110, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(533, 47, 111, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(534, 47, 112, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(535, 47, 113, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(536, 47, 114, 0, 0, 0, 0, '2018-12-21 09:19:50', '2018-12-21 09:19:50', 1, 1),
(537, 48, 1, 1, 1, 1, 1, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(538, 48, 77, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(539, 48, 90, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(540, 48, 91, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(541, 48, 92, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(542, 48, 93, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(543, 48, 94, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(544, 48, 95, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(545, 48, 96, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(546, 48, 97, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(547, 48, 98, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(548, 48, 99, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(549, 48, 100, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(550, 48, 101, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(551, 48, 102, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(552, 48, 103, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(553, 48, 104, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(554, 48, 105, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(555, 48, 106, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(556, 48, 107, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(557, 48, 108, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(558, 48, 109, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(559, 48, 110, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(560, 48, 111, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(561, 48, 112, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(562, 48, 113, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(563, 48, 114, 0, 0, 0, 0, '2018-12-21 09:43:33', '2018-12-21 09:43:33', 1, 1),
(591, 50, 1, 1, 1, 1, 1, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(592, 50, 77, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(593, 50, 90, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(594, 50, 91, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(595, 50, 92, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(596, 50, 93, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(597, 50, 94, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(598, 50, 95, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(599, 50, 96, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(600, 50, 97, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(601, 50, 98, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(602, 50, 99, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(603, 50, 100, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(604, 50, 101, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(605, 50, 102, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(606, 50, 103, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(607, 50, 104, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(608, 50, 105, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(609, 50, 106, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(610, 50, 107, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(611, 50, 108, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(612, 50, 109, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(613, 50, 110, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(614, 50, 111, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(615, 50, 112, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(616, 50, 113, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(617, 50, 114, 0, 0, 0, 0, '2018-12-21 09:50:43', '2018-12-21 09:50:43', 1, 1),
(618, 51, 1, 1, 1, 1, 1, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(619, 51, 77, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(620, 51, 90, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(621, 51, 91, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(622, 51, 92, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(623, 51, 93, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(624, 51, 94, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(625, 51, 95, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(626, 51, 96, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(627, 51, 97, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(628, 51, 98, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(629, 51, 99, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(630, 51, 100, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(631, 51, 101, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(632, 51, 102, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(633, 51, 103, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(634, 51, 104, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(635, 51, 105, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(636, 51, 106, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(637, 51, 107, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(638, 51, 108, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(639, 51, 109, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(640, 51, 110, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(641, 51, 111, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(642, 51, 112, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(643, 51, 113, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(644, 51, 114, 0, 0, 0, 0, '2018-12-21 09:51:27', '2018-12-21 09:51:27', 1, 1),
(645, 52, 1, 1, 1, 1, 1, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(646, 52, 77, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(647, 52, 90, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(648, 52, 91, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(649, 52, 92, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(650, 52, 93, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(651, 52, 94, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(652, 52, 95, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(653, 52, 96, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(654, 52, 97, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(655, 52, 98, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(656, 52, 99, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(657, 52, 100, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(658, 52, 101, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(659, 52, 102, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(660, 52, 103, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(661, 52, 104, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(662, 52, 105, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(663, 52, 106, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(664, 52, 107, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(665, 52, 108, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(666, 52, 109, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(667, 52, 110, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(668, 52, 111, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(669, 52, 112, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(670, 52, 113, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(671, 52, 114, 0, 0, 0, 0, '2018-12-21 09:52:06', '2018-12-21 09:52:06', 1, 1),
(672, 53, 1, 1, 1, 1, 1, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(673, 53, 77, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(674, 53, 90, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(675, 53, 91, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(676, 53, 92, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(677, 53, 93, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(678, 53, 94, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(679, 53, 95, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(680, 53, 96, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(681, 53, 97, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(682, 53, 98, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(683, 53, 99, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(684, 53, 100, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(685, 53, 101, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(686, 53, 102, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(687, 53, 103, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(688, 53, 104, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(689, 53, 105, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(690, 53, 106, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(691, 53, 107, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(692, 53, 108, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(693, 53, 109, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(694, 53, 110, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(695, 53, 111, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(696, 53, 112, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(697, 53, 113, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(698, 53, 114, 0, 0, 0, 0, '2018-12-21 09:52:39', '2018-12-21 09:52:39', 1, 1),
(699, 54, 1, 1, 1, 1, 1, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(700, 54, 77, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(701, 54, 90, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(702, 54, 91, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(703, 54, 92, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(704, 54, 93, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(705, 54, 94, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(706, 54, 95, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(707, 54, 96, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(708, 54, 97, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(709, 54, 98, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(710, 54, 99, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(711, 54, 100, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(712, 54, 101, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(713, 54, 102, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(714, 54, 103, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(715, 54, 104, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(716, 54, 105, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(717, 54, 106, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(718, 54, 107, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(719, 54, 108, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(720, 54, 109, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(721, 54, 110, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(722, 54, 111, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(723, 54, 112, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(724, 54, 113, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(725, 54, 114, 0, 0, 0, 0, '2018-12-21 11:00:54', '2018-12-21 11:00:54', 1, 1),
(726, 55, 1, 1, 1, 1, 1, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(727, 55, 77, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(728, 55, 90, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(729, 55, 91, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(730, 55, 92, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(731, 55, 93, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(732, 55, 94, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(733, 55, 95, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(734, 55, 96, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(735, 55, 97, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(736, 55, 98, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(737, 55, 99, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(738, 55, 100, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(739, 55, 101, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(740, 55, 102, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(741, 55, 103, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(742, 55, 104, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(743, 55, 105, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(744, 55, 106, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(745, 55, 107, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(746, 55, 108, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(747, 55, 109, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(748, 55, 110, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(749, 55, 111, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(750, 55, 112, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(751, 55, 113, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(752, 55, 114, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(753, 55, 115, 0, 0, 0, 0, '2018-12-24 07:12:54', '2018-12-24 07:12:54', 1, 1),
(754, 56, 1, 1, 1, 1, 1, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(755, 56, 77, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(756, 56, 90, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(757, 56, 91, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(758, 56, 92, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(759, 56, 93, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(760, 56, 94, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(761, 56, 95, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(762, 56, 96, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(763, 56, 97, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(764, 56, 98, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(765, 56, 99, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(766, 56, 100, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(767, 56, 101, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(768, 56, 102, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(769, 56, 103, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(770, 56, 104, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(771, 56, 105, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(772, 56, 106, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(773, 56, 107, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(774, 56, 108, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(775, 56, 109, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(776, 56, 110, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(777, 56, 111, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(778, 56, 112, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(779, 56, 113, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(780, 56, 114, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(781, 56, 115, 0, 0, 0, 0, '2018-12-24 08:02:11', '2018-12-24 08:02:11', 1, 1),
(782, 57, 1, 1, 1, 1, 1, '2018-12-26 07:06:18', '2019-03-18 10:37:10', 1, 1),
(783, 57, 77, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(784, 57, 90, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(785, 57, 91, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(786, 57, 92, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(787, 57, 93, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1);
INSERT INTO `modules_users_rights` (`ModuleRightID`, `ModuleID`, `UserID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(788, 57, 94, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(789, 57, 95, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(790, 57, 96, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(791, 57, 97, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(792, 57, 98, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(793, 57, 99, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(794, 57, 100, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(795, 57, 101, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(796, 57, 102, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(797, 57, 103, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(798, 57, 104, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(799, 57, 105, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(800, 57, 106, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(801, 57, 107, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(802, 57, 108, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(803, 57, 109, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(804, 57, 110, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(805, 57, 111, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(806, 57, 112, 1, 1, 1, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(807, 57, 113, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(808, 57, 114, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(809, 57, 115, 0, 0, 0, 0, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(810, 57, 116, 1, 1, 1, 1, '2018-12-26 07:06:18', '2018-12-26 07:06:18', 1, 1),
(811, 58, 1, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(812, 58, 77, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(813, 58, 90, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(814, 58, 91, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(815, 58, 92, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(816, 58, 93, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(817, 58, 94, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(818, 58, 95, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(819, 58, 96, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(820, 58, 97, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(821, 58, 98, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(822, 58, 99, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(823, 58, 100, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(824, 58, 101, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(825, 58, 102, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(826, 58, 103, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(827, 58, 104, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(828, 58, 105, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(829, 58, 106, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(830, 58, 107, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(831, 58, 108, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(832, 58, 109, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(833, 58, 110, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(834, 58, 111, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(835, 58, 112, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(836, 58, 113, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(837, 58, 114, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(838, 58, 115, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(839, 58, 116, 1, 1, 1, 1, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(840, 58, 117, 0, 0, 0, 0, '2018-12-27 13:38:22', '2018-12-27 13:38:22', 1, 1),
(841, 59, 1, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(842, 59, 77, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(843, 59, 90, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(844, 59, 91, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(845, 59, 92, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(846, 59, 93, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(847, 59, 94, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(848, 59, 95, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(849, 59, 96, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(850, 59, 97, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(851, 59, 98, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(852, 59, 99, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(853, 59, 100, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(854, 59, 101, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(855, 59, 102, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(856, 59, 103, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(857, 59, 104, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(858, 59, 105, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(859, 59, 106, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(860, 59, 107, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(861, 59, 108, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(862, 59, 109, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(863, 59, 110, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(864, 59, 111, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(865, 59, 112, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(866, 59, 113, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(867, 59, 114, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(868, 59, 115, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(869, 59, 116, 1, 1, 1, 1, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(870, 59, 117, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(871, 59, 118, 0, 0, 0, 0, '2019-01-04 09:36:11', '2019-01-04 09:36:11', 1, 1),
(872, 60, 1, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(873, 60, 77, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(874, 60, 90, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(875, 60, 91, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(876, 60, 92, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(877, 60, 93, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(878, 60, 94, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(879, 60, 95, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(880, 60, 96, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(881, 60, 97, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(882, 60, 98, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(883, 60, 99, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(884, 60, 100, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(885, 60, 101, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(886, 60, 102, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(887, 60, 103, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(888, 60, 104, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(889, 60, 105, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(890, 60, 106, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(891, 60, 107, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(892, 60, 108, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(893, 60, 109, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(894, 60, 110, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(895, 60, 111, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(896, 60, 112, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(897, 60, 113, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(898, 60, 114, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(899, 60, 115, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(900, 60, 116, 1, 1, 1, 1, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(901, 60, 117, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(902, 60, 118, 0, 0, 0, 0, '2019-01-07 13:27:24', '2019-01-07 13:27:24', 1, 1),
(903, 61, 1, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(904, 61, 42, 0, 0, 0, 0, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(905, 61, 44, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(906, 61, 45, 1, 1, 1, 1, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(907, 61, 46, 0, 0, 0, 0, '2019-01-29 06:18:23', '2019-01-29 06:18:23', 1, 1),
(908, 1, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(909, 18, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(910, 22, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(911, 23, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(912, 32, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(913, 35, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(914, 36, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(915, 39, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(916, 40, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(917, 43, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(918, 44, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(919, 45, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(920, 46, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(921, 47, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(922, 48, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(923, 50, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(924, 51, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(925, 52, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(926, 53, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(927, 54, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(928, 55, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(929, 56, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(930, 57, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(931, 58, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(932, 59, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(933, 60, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(934, 61, 47, 1, 1, 1, 1, '2019-01-30 07:04:12', '2019-01-30 07:04:12', 1, 1),
(935, 62, 1, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(936, 62, 42, 0, 0, 0, 0, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(937, 62, 44, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(938, 62, 45, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(939, 62, 46, 0, 0, 0, 0, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(940, 62, 47, 1, 1, 1, 1, '2019-02-01 05:47:53', '2019-02-01 05:47:53', 1, 1),
(941, 1, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(942, 18, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(943, 22, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(944, 23, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(945, 32, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(946, 35, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(947, 36, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(948, 39, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(949, 40, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(950, 43, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(951, 44, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(952, 45, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(953, 46, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(954, 47, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(955, 48, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(956, 50, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(957, 51, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(958, 52, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(959, 53, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(960, 54, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(961, 55, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(962, 56, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(963, 57, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(964, 58, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(965, 59, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(966, 60, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(967, 61, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(968, 62, 48, 1, 1, 1, 1, '2019-02-01 10:08:45', '2019-02-01 10:08:45', 1, 1),
(969, 1, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(970, 18, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(971, 22, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(972, 23, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(973, 32, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(974, 35, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(975, 36, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(976, 39, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(977, 40, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(978, 43, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(979, 44, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(980, 45, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(981, 46, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(982, 47, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(983, 48, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(984, 50, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(985, 51, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(986, 52, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(987, 53, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(988, 54, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(989, 55, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(990, 56, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(991, 57, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(992, 58, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(993, 59, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(994, 60, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:57:03', 1, 1),
(995, 61, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(996, 62, 49, 1, 1, 1, 1, '2019-02-01 10:57:03', '2019-02-01 10:59:23', 1, 49),
(997, 1, 50, 1, 1, 1, 1, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(998, 18, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(999, 22, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1000, 23, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1001, 32, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1002, 35, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1003, 36, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1004, 39, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1005, 40, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1006, 43, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1007, 44, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1008, 45, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1009, 46, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1010, 47, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1011, 48, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1012, 50, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1013, 51, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1014, 52, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1015, 53, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1016, 54, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1017, 55, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1018, 56, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1019, 57, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1020, 58, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1021, 59, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-02-01 11:04:41', 49, 49),
(1022, 60, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1023, 61, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1024, 62, 50, 0, 0, 0, 0, '2019-02-01 11:04:41', '2019-03-06 07:53:21', 49, 1),
(1025, 63, 1, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(1026, 63, 49, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-02-01 11:10:43', 1, 1),
(1027, 63, 50, 1, 1, 1, 1, '2019-02-01 11:10:43', '2019-03-06 07:53:21', 1, 1),
(1028, 64, 1, 1, 1, 1, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(1029, 64, 49, 1, 1, 1, 1, '2019-02-01 11:11:30', '2019-02-01 11:11:30', 1, 1),
(1030, 64, 50, 0, 0, 0, 0, '2019-02-01 11:11:30', '2019-03-06 07:53:21', 1, 1),
(1031, 65, 1, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(1032, 65, 49, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-02-01 11:15:09', 1, 1),
(1033, 65, 50, 1, 1, 1, 1, '2019-02-01 11:15:09', '2019-03-06 07:53:21', 1, 1),
(1034, 66, 1, 1, 1, 1, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(1035, 66, 49, 1, 1, 1, 1, '2019-02-01 11:16:07', '2019-02-01 11:16:07', 1, 1),
(1036, 66, 50, 0, 0, 0, 0, '2019-02-01 11:16:07', '2019-03-06 07:53:21', 1, 1),
(1037, 67, 1, 1, 1, 1, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(1038, 67, 49, 1, 1, 1, 1, '2019-02-01 11:17:01', '2019-02-01 11:17:01', 1, 1),
(1039, 67, 50, 0, 0, 0, 0, '2019-02-01 11:17:01', '2019-03-06 07:53:21', 1, 1),
(1040, 68, 1, 1, 1, 1, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(1041, 68, 49, 1, 1, 1, 1, '2019-02-01 11:17:56', '2019-02-01 11:17:56', 1, 1),
(1042, 68, 50, 0, 0, 0, 0, '2019-02-01 11:17:56', '2019-03-06 07:53:21', 1, 1),
(1043, 69, 1, 1, 1, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(1044, 69, 49, 1, 1, 1, 1, '2019-02-01 11:19:27', '2019-02-01 11:19:27', 1, 1),
(1045, 69, 50, 0, 0, 0, 0, '2019-02-01 11:19:27', '2019-03-06 07:53:21', 1, 1),
(1046, 70, 1, 1, 1, 1, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(1047, 70, 49, 1, 1, 1, 1, '2019-02-01 11:20:46', '2019-02-01 11:20:46', 1, 1),
(1048, 70, 50, 0, 0, 0, 0, '2019-02-01 11:20:46', '2019-03-06 07:53:21', 1, 1),
(1049, 71, 1, 1, 1, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(1050, 71, 49, 1, 1, 1, 1, '2019-02-01 11:22:25', '2019-02-01 11:22:25', 1, 1),
(1051, 71, 50, 0, 0, 0, 0, '2019-02-01 11:22:25', '2019-03-06 07:53:21', 1, 1),
(1052, 72, 1, 1, 1, 1, 1, '2019-02-01 11:23:19', '2019-03-18 10:37:10', 1, 1),
(1053, 72, 49, 1, 1, 1, 1, '2019-02-01 11:23:19', '2019-02-01 11:23:19', 1, 1),
(1054, 72, 50, 0, 0, 0, 0, '2019-02-01 11:23:19', '2019-03-06 07:53:21', 1, 1),
(1055, 73, 1, 1, 1, 1, 1, '2019-02-01 11:24:43', '2019-03-18 10:37:10', 1, 1),
(1056, 73, 49, 1, 1, 1, 1, '2019-02-01 11:24:43', '2019-02-01 11:24:43', 1, 1),
(1057, 73, 50, 0, 0, 0, 0, '2019-02-01 11:24:43', '2019-03-06 07:53:21', 1, 1),
(1058, 74, 1, 1, 1, 1, 1, '2019-02-01 11:29:13', '2019-03-18 10:37:10', 1, 1),
(1059, 74, 49, 1, 1, 1, 1, '2019-02-01 11:29:13', '2019-02-01 11:29:13', 1, 1),
(1060, 74, 50, 0, 0, 0, 0, '2019-02-01 11:29:13', '2019-03-06 07:53:21', 1, 1),
(1061, 75, 1, 1, 1, 1, 1, '2019-02-01 13:02:53', '2019-03-18 10:37:10', 1, 1),
(1062, 75, 49, 1, 1, 1, 1, '2019-02-01 13:02:53', '2019-02-01 13:02:53', 1, 1),
(1063, 75, 50, 0, 0, 0, 0, '2019-02-01 13:02:53', '2019-03-06 07:53:21', 1, 1),
(1064, 76, 1, 1, 1, 1, 1, '2019-02-06 07:02:37', '2019-03-18 10:37:10', 1, 1),
(1065, 76, 49, 1, 1, 1, 1, '2019-02-06 07:02:37', '2019-02-06 07:02:37', 1, 1),
(1066, 76, 50, 0, 0, 0, 0, '2019-02-06 07:02:37', '2019-03-06 07:53:21', 1, 1),
(1067, 77, 1, 1, 1, 1, 1, '2019-02-12 11:27:55', '2019-03-18 10:37:10', 1, 1),
(1068, 77, 49, 1, 1, 1, 1, '2019-02-12 11:27:55', '2019-02-12 11:27:55', 1, 1),
(1069, 77, 50, 0, 0, 0, 0, '2019-02-12 11:27:55', '2019-03-06 07:53:21', 1, 1),
(1070, 78, 1, 1, 1, 1, 1, '2019-02-12 11:43:22', '2019-03-18 10:37:10', 1, 1),
(1071, 78, 49, 1, 1, 1, 1, '2019-02-12 11:43:22', '2019-02-12 11:43:22', 1, 1),
(1072, 78, 50, 0, 0, 0, 0, '2019-02-12 11:43:22', '2019-03-06 07:53:21', 1, 1),
(1073, 79, 1, 1, 1, 1, 1, '2019-02-19 12:11:34', '2019-03-18 10:37:10', 1, 1),
(1074, 79, 49, 1, 1, 1, 1, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(1075, 79, 50, 0, 0, 0, 0, '2019-02-19 12:11:34', '2019-03-06 07:53:21', 1, 1),
(1076, 79, 54, 0, 0, 0, 0, '2019-02-19 12:11:34', '2019-02-19 12:11:34', 1, 1),
(1077, 80, 1, 1, 1, 1, 1, '2019-02-25 12:52:39', '2019-03-18 10:37:10', 1, 1),
(1078, 80, 49, 1, 1, 1, 1, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(1079, 80, 50, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-03-06 07:53:21', 1, 1),
(1080, 80, 54, 0, 0, 0, 0, '2019-02-25 12:52:39', '2019-02-25 12:52:39', 1, 1),
(1081, 81, 1, 1, 1, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1082, 81, 49, 1, 1, 1, 1, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1083, 81, 50, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1084, 81, 54, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1085, 81, 55, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1086, 81, 56, 0, 0, 0, 0, '2019-03-14 10:44:20', '2019-03-14 10:44:20', 1, 1),
(1087, 82, 1, 1, 1, 1, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1088, 82, 49, 1, 1, 1, 1, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1089, 82, 50, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1090, 82, 54, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1091, 82, 55, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1092, 82, 56, 0, 0, 0, 0, '2019-03-26 09:09:40', '2019-03-26 09:09:40', 1, 1),
(1093, 83, 1, 1, 1, 1, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1094, 83, 49, 1, 1, 1, 1, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1095, 83, 50, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1096, 83, 54, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1097, 83, 55, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1098, 83, 56, 0, 0, 0, 0, '2019-03-26 09:12:17', '2019-03-26 09:12:17', 1, 1),
(1099, 84, 1, 1, 1, 1, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1100, 84, 49, 1, 1, 1, 1, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1101, 84, 50, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1102, 84, 54, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1103, 84, 55, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1104, 84, 56, 0, 0, 0, 0, '2019-03-29 08:27:21', '2019-03-29 08:27:21', 1, 1),
(1105, 85, 1, 1, 1, 1, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1106, 85, 49, 1, 1, 1, 1, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1107, 85, 50, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1108, 85, 54, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1109, 85, 55, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1110, 85, 56, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1111, 85, 57, 0, 0, 0, 0, '2019-04-01 21:59:44', '2019-04-01 21:59:44', 1, 1),
(1112, 1, 59, 1, 1, 1, 1, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1113, 18, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1114, 22, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1115, 23, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1116, 32, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1117, 35, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1118, 36, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1119, 39, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1120, 40, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1121, 43, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1122, 44, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1123, 45, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1124, 46, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1125, 47, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1126, 48, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1127, 50, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1128, 51, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1129, 52, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1130, 53, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1131, 54, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1132, 55, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1133, 56, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1134, 57, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1135, 58, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1136, 59, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1137, 60, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1138, 61, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1139, 62, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1140, 63, 59, 1, 1, 1, 1, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1141, 64, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1142, 65, 59, 1, 1, 1, 1, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1143, 66, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1144, 67, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1145, 68, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1146, 69, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1147, 70, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1148, 71, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1149, 72, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1150, 73, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1151, 74, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1152, 75, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1153, 76, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1154, 77, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1155, 78, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1156, 79, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1157, 80, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1158, 81, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1159, 82, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1160, 83, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1161, 84, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1162, 85, 59, 0, 0, 0, 0, '2019-04-03 10:59:38', '2019-04-03 10:59:38', 1, 1),
(1163, 1, 60, 1, 1, 1, 1, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1164, 18, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1165, 22, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1166, 23, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1167, 32, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1168, 35, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1169, 36, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1170, 39, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1171, 40, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1172, 43, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1173, 44, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1174, 45, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1175, 46, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1176, 47, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1177, 48, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1178, 50, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1179, 51, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1180, 52, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1181, 53, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1182, 54, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1183, 55, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1184, 56, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1185, 57, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1186, 58, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1187, 59, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1188, 60, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1189, 61, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1190, 62, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1191, 63, 60, 1, 1, 1, 1, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1192, 64, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1193, 65, 60, 1, 1, 1, 1, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1194, 66, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1195, 67, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1196, 68, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1197, 69, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1198, 70, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1199, 71, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1200, 72, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1201, 73, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1202, 74, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1203, 75, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1204, 76, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1205, 77, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1206, 78, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1207, 79, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1208, 80, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1209, 81, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1210, 82, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1211, 83, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1212, 84, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1213, 85, 60, 0, 0, 0, 0, '2019-04-03 11:01:30', '2019-04-03 11:01:30', 1, 1),
(1214, 86, 1, 1, 1, 1, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1215, 86, 49, 1, 1, 1, 1, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1216, 86, 50, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1217, 86, 54, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1218, 86, 55, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1219, 86, 56, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1220, 86, 57, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1221, 86, 58, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1222, 86, 59, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1223, 86, 60, 0, 0, 0, 0, '2019-04-10 13:44:18', '2019-04-10 13:44:18', 1, 1),
(1224, 87, 1, 1, 1, 1, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1225, 87, 49, 1, 1, 1, 1, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1226, 87, 50, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1227, 87, 54, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1228, 87, 55, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1229, 87, 56, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1230, 87, 57, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1231, 87, 58, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1232, 87, 59, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1233, 87, 60, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1234, 87, 61, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1235, 87, 62, 0, 0, 0, 0, '2019-04-18 16:07:12', '2019-04-18 16:07:12', 1, 1),
(1236, 1, 66, 1, 1, 1, 1, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1237, 18, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1238, 22, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1239, 23, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1240, 32, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1241, 35, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1242, 36, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1243, 39, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1244, 40, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1245, 43, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1246, 44, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1247, 45, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1248, 46, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1249, 47, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1250, 48, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1251, 50, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1252, 51, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1253, 52, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1254, 53, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1255, 54, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1256, 55, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1257, 56, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1258, 57, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1259, 58, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1260, 59, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1261, 60, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1262, 61, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1263, 62, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1264, 63, 66, 1, 1, 1, 1, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1265, 64, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1266, 65, 66, 1, 1, 1, 1, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1267, 66, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1268, 67, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1269, 68, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1270, 69, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1271, 70, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1272, 71, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1273, 72, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1274, 73, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1275, 74, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1276, 75, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1277, 76, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1278, 77, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1279, 78, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1280, 79, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1281, 80, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1282, 81, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1283, 82, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1284, 83, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1285, 84, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1286, 85, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1287, 86, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1288, 87, 66, 0, 0, 0, 0, '2019-11-05 08:38:52', '2019-11-05 08:38:52', 1, 1),
(1289, 1, 67, 1, 1, 1, 1, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1290, 18, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1291, 22, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1292, 23, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1293, 32, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1294, 35, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1295, 36, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1296, 39, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1297, 40, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1298, 43, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1299, 44, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1300, 45, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1301, 46, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1302, 47, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1303, 48, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1304, 50, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1305, 51, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1306, 52, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1307, 53, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1308, 54, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1309, 55, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1310, 56, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1311, 57, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1312, 58, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1313, 59, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1314, 60, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1315, 61, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1316, 62, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1317, 63, 67, 1, 1, 1, 1, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1318, 64, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1319, 65, 67, 1, 1, 1, 1, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1320, 66, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1321, 67, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1322, 68, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1323, 69, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1324, 70, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1325, 71, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1326, 72, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1327, 73, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1328, 74, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1329, 75, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1330, 76, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1331, 77, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1332, 78, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1333, 79, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1334, 80, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1335, 81, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1336, 82, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1337, 83, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1338, 84, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1339, 85, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1340, 86, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1341, 87, 67, 0, 0, 0, 0, '2019-11-09 15:19:11', '2019-11-09 15:19:11', 1, 1),
(1342, 1, 68, 1, 1, 1, 1, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1343, 18, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1344, 22, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1345, 23, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1346, 32, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1347, 35, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1348, 36, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1349, 39, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1350, 40, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1351, 43, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1352, 44, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1353, 45, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1354, 46, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1355, 47, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1356, 48, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1357, 50, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1358, 51, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1359, 52, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1360, 53, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1361, 54, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1362, 55, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1363, 56, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1364, 57, 68, 0, 0, 0, 0, '2019-11-09 15:24:07', '2019-11-09 15:24:07', 1, 1),
(1365, 58, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1366, 59, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1367, 60, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1368, 61, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1369, 62, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1370, 63, 68, 1, 1, 1, 1, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1371, 64, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1372, 65, 68, 1, 1, 1, 1, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1373, 66, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1374, 67, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1375, 68, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1376, 69, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1377, 70, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1378, 71, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1379, 72, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1380, 73, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1381, 74, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1382, 75, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1383, 76, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1384, 77, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1385, 78, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1386, 79, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1387, 80, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1388, 81, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1389, 82, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1390, 83, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1391, 84, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1392, 85, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1393, 86, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1394, 87, 68, 0, 0, 0, 0, '2019-11-09 15:24:08', '2019-11-09 15:24:08', 1, 1),
(1395, 1, 69, 1, 1, 1, 1, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1396, 18, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1397, 22, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1398, 23, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1399, 32, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1400, 35, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1401, 36, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1402, 39, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1403, 40, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1404, 43, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1405, 44, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1406, 45, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1407, 46, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1408, 47, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1409, 48, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1410, 50, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1411, 51, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1412, 52, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1413, 53, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1414, 54, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1415, 55, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1416, 56, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1417, 57, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1418, 58, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1419, 59, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1420, 60, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1421, 61, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1422, 62, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1423, 63, 69, 1, 1, 1, 1, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1424, 64, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1425, 65, 69, 1, 1, 1, 1, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1426, 66, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1427, 67, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1);
INSERT INTO `modules_users_rights` (`ModuleRightID`, `ModuleID`, `UserID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1428, 68, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1429, 69, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1430, 70, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1431, 71, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1432, 72, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1433, 73, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1434, 74, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1435, 75, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1436, 76, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1437, 77, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1438, 78, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1439, 79, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1440, 80, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1441, 81, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1442, 82, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1443, 83, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1444, 84, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1445, 85, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1446, 86, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1447, 87, 69, 0, 0, 0, 0, '2019-11-09 15:27:46', '2019-11-09 15:27:46', 1, 1),
(1448, 1, 70, 1, 1, 1, 1, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1449, 18, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1450, 22, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1451, 23, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1452, 32, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1453, 35, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1454, 36, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1455, 39, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1456, 40, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1457, 43, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1458, 44, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1459, 45, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1460, 46, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1461, 47, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1462, 48, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1463, 50, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1464, 51, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1465, 52, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1466, 53, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1467, 54, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1468, 55, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1469, 56, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1470, 57, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1471, 58, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1472, 59, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1473, 60, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1474, 61, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1475, 62, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1476, 63, 70, 1, 1, 1, 1, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1477, 64, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1478, 65, 70, 1, 1, 1, 1, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1479, 66, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1480, 67, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1481, 68, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1482, 69, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1483, 70, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1484, 71, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1485, 72, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1486, 73, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1487, 74, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1488, 75, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1489, 76, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1490, 77, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1491, 78, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1492, 79, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1493, 80, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1494, 81, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1495, 82, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1496, 83, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1497, 84, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1498, 85, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1499, 86, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1500, 87, 70, 0, 0, 0, 0, '2019-11-18 21:59:59', '2019-11-18 21:59:59', 1, 1),
(1501, 1, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1502, 18, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1503, 22, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1504, 23, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1505, 32, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1506, 35, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1507, 36, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1508, 39, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1509, 40, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1510, 43, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1511, 44, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1512, 45, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1513, 46, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1514, 47, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1515, 48, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1516, 50, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1517, 51, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1518, 52, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1519, 53, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1520, 54, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1521, 55, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1522, 56, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1523, 57, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1524, 58, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1525, 59, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1526, 60, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1527, 61, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1528, 62, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1529, 63, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1530, 64, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1531, 65, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1532, 66, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1533, 67, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1534, 68, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1535, 69, 72, 0, 0, 0, 0, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1536, 70, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1537, 71, 72, 0, 0, 0, 0, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1538, 72, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1539, 73, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1540, 74, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1541, 75, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1542, 76, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1543, 77, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1544, 78, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1545, 79, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1546, 80, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1547, 81, 72, 0, 0, 0, 0, '2020-02-14 08:58:00', '2020-02-14 08:58:00', 1, 1),
(1548, 82, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1549, 83, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1550, 84, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1551, 85, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1552, 86, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1553, 87, 72, 1, 1, 1, 1, '2020-02-14 08:58:00', '2020-02-14 08:59:46', 1, 1),
(1554, 88, 1, 1, 1, 1, 1, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(1555, 88, 64, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(1556, 88, 71, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(1557, 88, 72, 0, 0, 0, 0, '2020-02-19 07:43:41', '2020-02-19 07:43:41', 1, 1),
(1558, 89, 1, 1, 1, 1, 1, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(1559, 89, 64, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(1560, 89, 71, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(1561, 89, 72, 0, 0, 0, 0, '2020-03-04 16:40:04', '2020-03-04 16:40:04', 1, 1),
(1562, 90, 1, 1, 1, 1, 1, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(1563, 90, 64, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(1564, 90, 71, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(1565, 90, 72, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(1566, 90, 73, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1),
(1567, 90, 74, 0, 0, 0, 0, '2020-04-16 19:38:08', '2020-04-16 19:38:08', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nutritions`
--

CREATE TABLE `nutritions` (
  `NutritionID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nutritions`
--

INSERT INTO `nutritions` (`NutritionID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 0, 1, '2019-02-13 11:34:56', '2019-12-07 16:19:20', 1, 1),
(2, 1, 0, 1, '2019-02-14 05:32:48', '2019-12-07 16:19:39', 1, 1),
(3, 2, 0, 1, '2019-12-07 16:19:57', '2019-12-07 16:19:57', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nutritions_text`
--

CREATE TABLE `nutritions_text` (
  `NutritionTextID` int(11) NOT NULL,
  `NutritionID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nutritions_text`
--

INSERT INTO `nutritions_text` (`NutritionTextID`, `NutritionID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Carbohydrates', 1, '2019-02-13 11:34:56', '2019-12-07 16:19:20', 1, 1),
(2, 2, 'Protient', 1, '2019-02-14 05:32:48', '2019-12-07 16:19:39', 1, 1),
(3, 3, 'Fat', 1, '2019-12-07 16:19:57', '2019-12-07 16:19:57', 1, 1),
(4, 3, 'Fat', 2, '2019-12-07 16:19:57', '2019-12-07 16:19:57', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `OfferID` int(11) NOT NULL,
  `ProductID` varchar(255) NOT NULL,
  `ValidFrom` date NOT NULL,
  `ValidTo` date NOT NULL,
  `DiscountType` enum('percentage','per item') NOT NULL,
  `Discount` float NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `IsForAll` tinyint(4) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`OfferID`, `ProductID`, `ValidFrom`, `ValidTo`, `DiscountType`, `Discount`, `SortOrder`, `IsActive`, `IsForAll`, `Hide`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(4, '35', '2020-02-27', '2020-07-26', 'percentage', 20, 0, 1, 1, 0, '2019-12-10 19:14:12', '2020-05-04 11:51:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers_groups`
--

CREATE TABLE `offers_groups` (
  `OfferGroupID` int(11) NOT NULL,
  `OfferID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `offers_text`
--

CREATE TABLE `offers_text` (
  `OfferTextID` int(11) NOT NULL,
  `OfferID` int(11) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(1000) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers_text`
--

INSERT INTO `offers_text` (`OfferTextID`, `OfferID`, `SystemLanguageID`, `Title`, `Description`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(2, 4, 1, 'Golden ', '', '2019-12-10 19:14:12', '2020-05-04 11:51:00', 1, 1),
(3, 4, 2, 'mabruk', '', '2019-12-10 19:14:12', '2019-12-10 19:14:12', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers_users_notification`
--

CREATE TABLE `offers_users_notification` (
  `OfferUserNotificationID` int(11) NOT NULL,
  `OfferID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `IsShow` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers_users_notification`
--

INSERT INTO `offers_users_notification` (`OfferUserNotificationID`, `OfferID`, `GroupID`, `UserID`, `IsShow`) VALUES
(1, 0, 0, 64, 0),
(2, 0, 0, 71, 0),
(14, 4, 0, 64, 0),
(15, 4, 0, 71, 0),
(16, 4, 0, 73, 0),
(17, 4, 0, 74, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `OrderID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Description` varchar(500) CHARACTER SET utf8 NOT NULL,
  `OrderNumber` varchar(255) NOT NULL,
  `AddressID` int(11) NOT NULL,
  `AddressIDForPaymentCollection` int(11) NOT NULL,
  `ShipmentMethodID` int(11) NOT NULL,
  `CollectFromStore` int(11) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1' COMMENT 'These are defined in order_statuses table',
  `StoreID` int(11) NOT NULL,
  `BranchDeliveryDistrictID` int(11) NOT NULL,
  `CouponCodeUsed` varchar(255) NOT NULL,
  `CouponCodeDiscountPercentage` decimal(10,2) NOT NULL,
  `DiscountAvailed` decimal(10,2) NOT NULL,
  `TotalShippingCharges` decimal(10,2) NOT NULL,
  `TotalTaxAmount` decimal(10,2) NOT NULL,
  `TotalAmount` decimal(10,2) NOT NULL,
  `DriverID` int(11) NOT NULL,
  `DeliveryOTP` varchar(10) NOT NULL,
  `ShippedThroughApi` int(11) NOT NULL,
  `PaymentMethod` varchar(255) NOT NULL,
  `TransactionID` varchar(255) DEFAULT NULL,
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `IsRead` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`OrderID`, `UserID`, `Description`, `OrderNumber`, `AddressID`, `AddressIDForPaymentCollection`, `ShipmentMethodID`, `CollectFromStore`, `Status`, `StoreID`, `BranchDeliveryDistrictID`, `CouponCodeUsed`, `CouponCodeDiscountPercentage`, `DiscountAvailed`, `TotalShippingCharges`, `TotalTaxAmount`, `TotalAmount`, `DriverID`, `DeliveryOTP`, `ShippedThroughApi`, `PaymentMethod`, `TransactionID`, `Hide`, `IsRead`, `CreatedAt`) VALUES
(9, 64, '', '00009', 0, 0, 0, 1, 5, 0, 0, '', '0.00', '0.00', '20.00', '4.25', '89.25', 0, '', 0, 'COD', '', 0, 1, '2019-12-04 18:20:28'),
(10, 64, '', '00010', 4, 4, 3, 0, 1, 0, 0, '', '0.00', '0.00', '500.00', '28.25', '593.25', 0, '', 0, 'COD', '', 1, 1, '2019-12-04 18:21:59'),
(11, 71, '', '00011', 0, 0, 0, 1, 5, 0, 0, 'BLK001', '20.00', '93.00', '500.00', '28.25', '593.25', 0, '', 0, 'COD', '', 1, 1, '2019-12-15 12:30:21'),
(12, 1, '', '00011', 0, 0, 0, 1, 5, 0, 0, 'BLK001', '20.00', '93.00', '500.00', '28.25', '593.25', 0, '', 0, 'COD', '', 1, 1, '2019-12-15 12:30:21'),
(13, 64, '', '00011', 0, 0, 0, 1, 5, 0, 0, '', '0.00', '0.00', '500.00', '28.25', '593.25', 0, '', 0, 'COD', '', 1, 0, '2019-12-15 12:30:21'),
(14, 64, '', '00014', 4, 4, 1, 0, 1, 0, 0, 'BLK001', '20.00', '45.80', '20.00', '10.16', '213.36', 0, '', 0, 'COD', '', 1, 0, '2020-01-03 10:18:57'),
(15, 64, '', '00015', 4, 4, 1, 0, 1, 0, 0, '', '0.00', '0.00', '20.00', '4.25', '89.25', 0, '', 0, 'COD', '', 1, 0, '2020-02-14 09:17:29'),
(16, 64, '', '00016', 4, 4, 1, 0, 1, 0, 0, '', '0.00', '0.00', '20.00', '4.25', '89.25', 0, '', 0, 'COD', '', 1, 1, '2020-02-14 09:18:34'),
(17, 64, '', '00017', 4, 4, 1, 0, 2, 0, 0, '', '0.00', '0.00', '20.00', '6.10', '128.10', 0, '', 0, 'COD', '', 1, 1, '2020-02-14 09:21:26'),
(18, 64, '', '00018', 4, 4, 1, 0, 2, 6, 13, '', '0.00', '0.00', '20.00', '9.80', '205.80', 0, '', 0, 'COD', '', 1, 1, '2020-02-17 11:05:10'),
(19, 64, '', '00019', 11, 4, 1, 0, 1, 5, 13, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-03-18 19:44:36'),
(20, 64, '', '00020', 11, 4, 1, 0, 1, 4, 13, '', '0.00', '0.00', '20.00', '1.00', '21.00', 0, '', 0, 'COD', '', 1, 0, '2020-03-18 19:51:50'),
(21, 64, '', '00021', 8, 4, 1, 0, 1, 6, 0, '', '0.00', '0.00', '20.00', '52.65', '1105.65', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 21:31:40'),
(22, 64, '', '00022', 0, 0, 0, 1, 5, 6, 0, '', '0.00', '0.00', '0.00', '1.70', '35.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 21:48:07'),
(23, 64, '', '00023', 0, 0, 0, 1, 5, 6, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 21:48:47'),
(24, 64, '', '00024', 0, 0, 0, 1, 5, 6, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 21:50:27'),
(25, 64, '', '00025', 0, 0, 0, 1, 5, 6, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 21:54:15'),
(26, 64, '', '00026', 0, 0, 0, 1, 5, 6, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 22:03:17'),
(27, 64, '', '00027', 0, 0, 0, 1, 5, 6, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 22:03:48'),
(28, 64, '', '00028', 0, 0, 0, 1, 5, 6, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 22:04:54'),
(29, 64, '', '00029', 0, 0, 0, 1, 4, 6, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 22:08:25'),
(30, 64, '', '00030', 0, 0, 0, 1, 5, 6, 0, '', '0.00', '0.00', '0.00', '1.70', '35.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 22:15:34'),
(31, 64, '', '00031', 8, 0, 1, 0, 3, 6, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 22:16:17'),
(32, 64, '', '00032', 0, 0, 0, 1, 5, 6, 0, '', '0.00', '0.00', '0.00', '1.70', '35.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-02 22:18:10'),
(33, 64, '', '00033', 8, 0, 1, 0, 1, 6, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-15 07:55:36'),
(34, 64, '', '00034', 0, 0, 0, 1, 5, 5, 0, '', '0.00', '0.00', '0.00', '1.70', '35.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-16 18:06:04'),
(35, 64, '', '00035', 8, 0, 1, 0, 1, 5, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', '', 1, 0, '2020-04-16 18:07:28'),
(41, 64, '', '00041', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'COD', NULL, 1, 0, '2020-04-24 16:39:48'),
(42, 64, '', '00042', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'COD', NULL, 1, 0, '2020-04-24 16:40:31'),
(43, 64, '', '00043', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'COD', NULL, 1, 0, '2020-04-24 16:43:36'),
(44, 64, '', '00044', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'COD', NULL, 1, 0, '2020-04-24 16:52:26'),
(45, 64, '', '00045', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'COD', NULL, 1, 0, '2020-04-24 17:03:03'),
(46, 64, '', '00046', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'COD', NULL, 1, 0, '2020-04-24 17:18:37'),
(47, 64, '', '00047', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'PayTab', '463287', 0, 0, '2020-04-24 17:58:22'),
(48, 64, '', '00048', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'PayTab', '463289', 0, 0, '2020-04-24 18:06:30'),
(49, 64, '', '00049', 8, 0, 1, 0, 2, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 1, '', 0, 'PayTab', '463299', 0, 1, '2020-04-24 18:21:14'),
(50, 64, '', '00050', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'PayTab', '463309', 0, 0, '2020-04-24 18:33:17'),
(51, 64, '', '00051', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'PayTab', '463311', 0, 0, '2020-04-24 18:38:29'),
(52, 64, '', '00052', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'PayTab', '463315', 0, 0, '2020-04-24 18:45:39'),
(53, 64, '', '00053', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', NULL, 0, 0, '2020-05-05 10:46:01'),
(54, 64, '', '00054', 8, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', NULL, 0, 0, '2020-05-05 10:47:28'),
(55, 64, '', '00055', 7, 0, 1, 0, 1, 7, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', NULL, 0, 1, '2020-05-05 12:58:05'),
(56, 64, '', '00056', 13, 0, 1, 0, 1, 5, 0, '', '0.00', '0.00', '20.00', '2.70', '56.70', 0, '', 0, 'COD', NULL, 0, 0, '2020-06-19 13:27:45'),
(57, 64, '', '00057', 13, 0, 1, 0, 1, 5, 0, '', '0.00', '0.00', '20.00', '4.25', '89.25', 0, '', 0, 'COD', NULL, 0, 0, '2020-06-19 13:29:12'),
(58, 64, '', '00058', 13, 0, 1, 0, 1, 5, 0, '', '0.00', '0.00', '20.00', '2.36', '49.56', 0, '', 0, 'COD', NULL, 0, 0, '2020-07-07 11:46:24');

-- --------------------------------------------------------

--
-- Table structure for table `orders_extra_charges`
--

CREATE TABLE `orders_extra_charges` (
  `OrderExtraChargeID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `TaxShipmentChargesID` int(11) NOT NULL,
  `Title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Factor` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Amount` decimal(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_extra_charges`
--

INSERT INTO `orders_extra_charges` (`OrderExtraChargeID`, `OrderID`, `TaxShipmentChargesID`, `Title`, `Factor`, `Amount`) VALUES
(22, 11, 2, 'VAT', '5%', '28.25'),
(21, 11, 3, 'Express Delivery', '500.00 SAR', '500.00'),
(20, 10, 2, 'VAT', '5%', '28.25'),
(19, 10, 3, 'Express Delivery', '500.00 SAR', '500.00'),
(18, 9, 2, 'VAT', '5%', '4.25'),
(17, 9, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(23, 14, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(24, 14, 2, 'VAT', '5%', '10.16'),
(25, 15, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(26, 15, 2, 'VAT', '5%', '4.25'),
(27, 16, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(28, 16, 2, 'VAT', '5%', '4.25'),
(29, 17, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(30, 17, 2, 'VAT', '5%', '6.10'),
(31, 18, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(32, 18, 2, 'VAT', '5%', '9.80'),
(33, 19, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(34, 19, 2, 'VAT', '5%', '2.70'),
(35, 20, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(36, 20, 2, 'VAT', '5%', '1.00'),
(37, 21, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(38, 21, 2, 'VAT', '5%', '52.65'),
(39, 22, 2, 'VAT', '5%', '1.70'),
(40, 23, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(41, 23, 2, 'VAT', '5%', '2.70'),
(42, 24, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(43, 24, 2, 'VAT', '5%', '2.70'),
(44, 25, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(45, 25, 2, 'VAT', '5%', '2.70'),
(46, 26, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(47, 26, 2, 'VAT', '5%', '2.70'),
(48, 27, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(49, 27, 2, 'VAT', '5%', '2.70'),
(50, 28, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(51, 28, 2, 'VAT', '5%', '2.70'),
(52, 29, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(53, 29, 2, 'VAT', '5%', '2.70'),
(54, 30, 2, 'VAT', '5%', '1.70'),
(55, 31, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(56, 31, 2, 'VAT', '5%', '2.70'),
(57, 32, 2, 'VAT', '5%', '1.70'),
(58, 33, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(59, 33, 2, 'VAT', '5%', '2.70'),
(60, 34, 2, 'VAT', '5%', '1.70'),
(61, 35, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(62, 35, 2, 'VAT', '5%', '2.70'),
(63, 36, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(64, 36, 2, 'VAT', '5%', '1.00'),
(65, 37, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(66, 37, 2, 'VAT', '5%', '1.00'),
(67, 38, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(68, 38, 2, 'VAT', '5%', '2.36'),
(69, 39, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(70, 39, 2, 'VAT', '5%', '2.36'),
(71, 40, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(72, 40, 2, 'VAT', '5%', '2.36'),
(73, 41, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(74, 41, 2, 'VAT', '5%', '2.36'),
(75, 42, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(76, 42, 2, 'VAT', '5%', '2.36'),
(77, 43, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(78, 43, 2, 'VAT', '5%', '2.36'),
(79, 44, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(80, 44, 2, 'VAT', '5%', '2.36'),
(81, 45, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(82, 45, 2, 'VAT', '5%', '2.36'),
(83, 46, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(84, 46, 2, 'VAT', '5%', '2.36'),
(85, 47, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(86, 47, 2, 'VAT', '5%', '2.36'),
(87, 48, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(88, 48, 2, 'VAT', '5%', '2.36'),
(89, 49, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(90, 49, 2, 'VAT', '5%', '2.36'),
(91, 50, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(92, 50, 2, 'VAT', '5%', '2.36'),
(93, 51, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(94, 51, 2, 'VAT', '5%', '2.36'),
(95, 52, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(96, 52, 2, 'VAT', '5%', '2.70'),
(97, 53, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(98, 53, 2, 'VAT', '5%', '2.70'),
(99, 54, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(100, 54, 2, 'VAT', '5%', '2.70'),
(101, 55, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(102, 55, 2, 'VAT', '5%', '2.70'),
(103, 56, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(104, 56, 2, 'VAT', '5%', '2.70'),
(105, 57, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(106, 57, 2, 'VAT', '5%', '4.25'),
(107, 58, 1, 'Standard Shipping', '20.00 SAR', '20.00'),
(108, 58, 2, 'VAT', '5%', '2.36');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `OrderItemID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Quantity` float NOT NULL,
  `Amount` decimal(10,2) NOT NULL,
  `ItemType` enum('Product','Collection','Chocobox','Choco Message','Customized Shape') NOT NULL,
  `CustomizedOrderProductIDs` text NOT NULL COMMENT 'It will be having product ids if item type is chocobox or chocomsg',
  `CustomizedShapeImage` text NOT NULL COMMENT 'This will be having value if Item Type is customized shape',
  `CustomizedBoxID` int(11) NOT NULL COMMENT 'This will be having values only for choco message and choco box item types',
  `IsCorporateItem` int(11) NOT NULL,
  `Ribbon` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`OrderItemID`, `OrderID`, `ProductID`, `Quantity`, `Amount`, `ItemType`, `CustomizedOrderProductIDs`, `CustomizedShapeImage`, `CustomizedBoxID`, `IsCorporateItem`, `Ribbon`) VALUES
(13, 11, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(12, 10, 35, 1, '65.00', 'Product', '', '', 0, 0, 0),
(11, 9, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(14, 14, 34, 3, '65.00', 'Product', '', '', 0, 0, 0),
(15, 14, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(16, 15, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(17, 16, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(18, 17, 35, 3, '34.00', 'Product', '', '', 0, 0, 0),
(19, 18, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(20, 18, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(21, 18, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(22, 18, 45, 1, '12.00', 'Product', '', '', 0, 0, 0),
(23, 19, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(24, 20, 36, 899, '0.00', 'Product', '', '', 0, 1, 0),
(25, 21, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(26, 21, 36, 1, '999.00', 'Product', '', '', 0, 0, 0),
(27, 22, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(28, 23, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(29, 24, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(30, 25, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(31, 26, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(32, 27, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(33, 28, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(34, 29, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(35, 30, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(36, 31, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(37, 32, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(38, 33, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(39, 34, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(40, 35, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(41, 38, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(42, 39, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(43, 40, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(44, 41, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(45, 42, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(46, 43, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(47, 44, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(48, 45, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(49, 46, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(50, 47, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(51, 48, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(52, 49, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(53, 50, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(54, 51, 35, 1, '27.20', 'Product', '', '', 0, 0, 0),
(55, 52, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(56, 53, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(57, 54, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(58, 55, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(59, 56, 35, 1, '34.00', 'Product', '', '', 0, 0, 0),
(60, 57, 34, 1, '65.00', 'Product', '', '', 0, 0, 0),
(61, 58, 35, 1, '27.20', 'Product', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `OrderStatusID` int(11) NOT NULL,
  `OrderStatusEn` varchar(255) NOT NULL,
  `OrderStatusAr` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ForAdmin` int(11) NOT NULL,
  `ForUser` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`OrderStatusID`, `OrderStatusEn`, `OrderStatusAr`, `ForAdmin`, `ForUser`) VALUES
(1, 'Pending', 'تم الاستلام', 0, 0),
(2, 'Packed', 'تعيين', 0, 0),
(3, 'Dispatched', 'منجز', 0, 0),
(4, 'Delivered', 'ألغيت', 0, 0),
(5, 'Cancelled', 'ألغيت', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `PageID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `BannerImage` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`PageID`, `Image`, `BannerImage`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'uploads/images/4484721307720190201092155img11.png', 'uploads/images/3568822746620190201090055img10.png', 0, 0, 1, '2018-12-19 06:00:56', '2019-02-01 09:55:21', 1, 1),
(6, '', '', 1, 0, 1, '2019-02-01 14:10:34', '2020-03-10 09:47:08', 1, 1),
(7, 'uploads/images/4195540731920190212110434img1.png', '', 2, 0, 1, '2019-02-12 11:34:04', '2019-04-12 14:33:20', 1, 1),
(8, 'uploads/images/5742807259820190411094704banner2.png', '', 3, 0, 1, '2019-02-12 11:34:04', '2019-04-12 14:39:10', 1, 1),
(9, '', '', 4, 0, 1, '2019-03-14 11:41:29', '2019-05-16 10:46:48', 1, 1),
(10, 'uploads/images/5972342051020190412111615cus1.png', '', 5, 1, 1, '2019-04-12 11:15:16', '2019-04-12 11:15:16', 1, 1),
(11, 'uploads/images/8544251421720190412114121cus2.png', '', 6, 1, 1, '2019-04-12 11:20:30', '2019-04-12 11:35:42', 1, 1),
(12, 'uploads/images/4719728060920190412115626cus3.png', '', 7, 1, 1, '2019-04-12 11:22:36', '2019-04-12 11:26:56', 1, 1),
(13, 'uploads/images/4719728060920190412115626cus3.png', '', 8, 0, 1, '2019-04-12 11:22:36', '2019-04-12 11:26:56', 1, 1),
(14, 'uploads/images/4719728060920190412115626cus3.png', '', 9, 0, 1, '2019-04-12 11:22:36', '2019-04-12 11:26:56', 1, 1),
(15, 'uploads/images/4719728060920190412115626cus3.png', '', 9, 0, 1, '2019-04-12 11:22:36', '2019-04-12 11:26:56', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages_text`
--

CREATE TABLE `pages_text` (
  `PageTextID` int(11) NOT NULL,
  `PageID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `ShortDescription` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `MetaTags` varchar(255) NOT NULL,
  `MetaKeywords` varchar(255) NOT NULL,
  `MetaDescription` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages_text`
--

INSERT INTO `pages_text` (`PageTextID`, `PageID`, `Title`, `Description`, `ShortDescription`, `SystemLanguageID`, `MetaTags`, `MetaKeywords`, `MetaDescription`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'About Us', '<h2>Chocomood</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<h2>Corporate Chocolates</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus. Sed sed dui at ligula ullamcorper feugiat nec nec ipsum. Nam elit justo, semper faucibus sodales et, feugiat ac ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesq', '', 1, '', '', '', '2018-12-19 06:00:56', '2019-02-01 09:55:21', 1, 1),
(2, 1, 'Arabic About US', '<p> هذا هو بعض الوصف الوهمي العربي المحدث </ p>', '', 2, '', '', '', '2018-12-19 06:01:28', '2018-12-19 11:07:51', 1, 1),
(11, 6, 'Contact Us', '<ul>\r\n<li>\r\n<h5>Working Hours</h5>\r\n<h6>9:00&nbsp;AM&nbsp; -&nbsp; 4:00 PM</h6>\r\n</li>\r\n<li>\r\n<h5>Marketing Departament</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 783780927</a><a href=\"tel:+ 966 7837809276\">6</a></h4>\r\n</li>\r\n<li>\r\n<h5>Quality Insurance</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Whole Sales</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Customer Care</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n</ul>', '', 1, 'Contact us', '', '', '2019-02-01 14:10:34', '2020-03-10 09:47:08', 1, 1),
(12, 6, 'Contact Us', '<ul>\r\n<li>\r\n<h5>Working Hours arabic</h5>\r\n<h6>9:00 am --- 04:00 pm</h6>\r\n</li>\r\n<li>\r\n<h5>Marketing Departament</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Quality Insurance</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Whole Sales</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n<li>\r\n<h5>Customer Care</h5>\r\n<h4><a href=\"tel:+ 966 7837809276\">+ 966 7837809276</a></h4>\r\n</li>\r\n</ul>', '', 2, '', '', '', '2019-02-01 14:10:49', '2019-02-13 13:10:49', 1, 1),
(13, 7, 'Home', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum.</p>', '<h2>Chocolate <span>is the happiness that you can..</span></h2>\r\n<h4>eat!</h4>', 1, '', '', '', '2019-02-12 11:34:04', '2019-04-12 14:33:20', 1, 1),
(14, 7, 'Home', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum.</p>', '<h2>Chocolate <span>is the happiness that you can..</span></h2>\r\n<h4>eat!</h4>', 2, '', '', '', '2019-02-12 11:34:25', '2019-04-12 14:33:54', 1, 1),
(15, 8, 'Corporate', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum.</p>', '<h3 class=\"wow fadeInRightBig\" data-wow-animation=\"4s\">Enjoy Best</h3>\r\n<h4 class=\"wow fadeInRightBig\" data-wow-animation=\"7s\">Chocolates</h4>\r\n<h3 class=\"wow fadeInUpBig last\" data-wow-animation=\"10s\">At Your Events!</h3>', 1, '', '', '', '2019-02-12 11:34:04', '2019-04-12 14:39:10', 1, 1),
(16, 8, 'Corporate', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum.</p>', '', 2, '', '', '', '2019-02-12 11:34:25', '2019-02-12 11:34:25', 1, 1),
(17, 9, 'Terms and conditions Updated', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>', '', 1, '', '', 'This is billing agreement page', '2019-03-14 11:41:29', '2019-05-16 10:46:48', 1, 1),
(18, 9, 'الأحكام والشروط', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit tempore, eligendi sunt voluptate consequuntur sit blanditiis, maxime, laborum magnam tenetur aliquid asperiores non sed, dolores eos. Quam fuga, iste omnis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid tenetur, delectus quidem dolorum illum sunt magni id nisi sequi sed omnis et accusamus, aliquam consequatur minus, quaerat! Officiis, adipisci, quaerat!</p>\r\n<p>Lorem ipsum dolor sit amet,</p>', '', 2, '', '', '', '2019-03-14 11:41:58', '2019-05-08 10:15:31', 1, 1),
(19, 10, 'Choco Message', '<p>Personalize your <br />message</p>', '', 1, 'Choco Message', 'Choco Message', 'Choco Message', '2019-04-12 11:15:16', '2019-04-12 11:15:16', 1, 1),
(20, 10, 'Choco Message', '<p>Personalize your <br />message</p>', '', 2, 'Choco Message', 'Choco Message', 'Choco Message', '2019-04-12 11:15:16', '2019-04-12 11:15:16', 1, 1),
(21, 11, 'Choco Box', '<p>Personalize your <br />Chocolate Box</p>', '', 1, 'Choco Box', 'Choco Box', 'Choco Box', '2019-04-12 11:20:30', '2019-04-12 11:35:42', 1, 1),
(22, 11, 'Choco Box', '<p>Personalize your <br />Chocolate Box</p>', '', 2, 'Choco Box', 'Choco Box', 'Choco Box', '2019-04-12 11:20:30', '2019-04-12 11:20:30', 1, 1),
(23, 12, 'Choco Shape', '<p>Personalize your <br />Chocolate Mold</p>', '', 1, 'Choco Shape', 'Choco Shape', 'Choco Shape', '2019-04-12 11:22:36', '2019-04-12 11:26:56', 1, 1),
(24, 12, 'Choco Shape', '<p>Personalize your <br />Chocolate Mold</p>', '', 2, 'Choco Shape', 'Choco Shape', 'Choco Shape', '2019-04-12 11:22:36', '2019-04-12 11:22:36', 1, 1),
(25, 13, 'Return Policy', '<h2>Chocomood</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<h2>Corporate Chocolates</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus. Sed sed dui at ligula ullamcorper feugiat nec nec ipsum. Nam elit justo, semper faucibus sodales et, feugiat ac ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesq', '', 1, '', '', '', '2018-12-19 06:00:56', '2019-02-01 09:55:21', 1, 1),
(26, 13, 'Return Policy', '<p> هذا هو بعض الوصف الوهمي العربي المحدث </ p>', '', 2, '', '', '', '2018-12-19 06:01:28', '2018-12-19 11:07:51', 1, 1),
(27, 14, 'FAQ', '<h2>Chocomood</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<h2>Corporate Chocolates</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus. Sed sed dui at ligula ullamcorper feugiat nec nec ipsum. Nam elit justo, semper faucibus sodales et, feugiat ac ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesq', '', 1, '', '', '', '2018-12-19 06:00:56', '2019-02-01 09:55:21', 1, 1),
(28, 14, 'FAQ', '<h2>Chocomood</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<h2>Corporate Chocolates</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus. Sed sed dui at ligula ullamcorper feugiat nec nec ipsum. Nam elit justo, semper faucibus sodales et, feugiat ac ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesq', '', 2, '', '', '', '2018-12-19 06:00:56', '2019-02-01 09:55:21', 1, 1),
(29, 15, 'Privacy Policy', '<h2>Chocomood</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<h2>Corporate Chocolates</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus. Sed sed dui at ligula ullamcorper feugiat nec nec ipsum. Nam elit justo, semper faucibus sodales et, feugiat ac ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesq', '', 1, '', '', '', '2018-12-19 06:00:56', '2019-02-01 09:55:21', 1, 1),
(30, 15, 'Privacy Policy', '<h2>Chocomood</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<h2>Corporate Chocolates</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesque ut justo eu erat eleifend mattis. Proin iaculis facilisis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus tincidunt malesuada eros, et eleifend lectus. Sed sed dui at ligula ullamcorper feugiat nec nec ipsum. Nam elit justo, semper faucibus sodales et, feugiat ac ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget libero bibendum, dapibus justo at, semper quam. Nullam a rutrum orci. Mauris ullamcorper semper vulputate. Nullam sit amet augue sit amet ex elementum lacinia. Proin gravida est molestie ante facilisis elementum. Pellentesq', '', 2, '', '', '', '2018-12-19 06:00:56', '2019-02-01 09:55:21', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `CategoryID` varchar(255) NOT NULL,
  `SubCategoryID` varchar(255) NOT NULL,
  `Price` decimal(9,2) DEFAULT NULL,
  `PriceType` enum('item','kg') DEFAULT 'item',
  `ServingSize` varchar(100) NOT NULL,
  `MinimumOrderQuantity` varchar(255) NOT NULL COMMENT 'for per KG is in grams',
  `SKU` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `IsFeatured` tinyint(4) NOT NULL,
  `IsCustomizedProduct` tinyint(4) NOT NULL,
  `OutOfStock` tinyint(4) NOT NULL,
  `BoxIDs` text NOT NULL COMMENT 'This will have value if IsCustomizedProduct is 1',
  `TagIDs` varchar(255) NOT NULL,
  `IsCorporateProduct` int(11) NOT NULL,
  `CorporateMinQuantity` text NOT NULL,
  `CorporatePrice` decimal(10,2) NOT NULL,
  `PurchaseCount` int(11) NOT NULL COMMENT 'No of times this item is purchased',
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductID`, `CategoryID`, `SubCategoryID`, `Price`, `PriceType`, `ServingSize`, `MinimumOrderQuantity`, `SKU`, `SortOrder`, `Hide`, `IsActive`, `IsFeatured`, `IsCustomizedProduct`, `OutOfStock`, `BoxIDs`, `TagIDs`, `IsCorporateProduct`, `CorporateMinQuantity`, `CorporatePrice`, `PurchaseCount`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(34, '34', '41', '65.00', 'kg', '', '2000', '001', 2, 0, 1, 0, 0, 0, '', '1,2', 0, '', '0.00', 11, '2019-11-30 12:13:54', '2020-02-14 12:20:04', 1, 72),
(35, '34', '41', '34.00', 'item', '', '', '0002', 3, 0, 1, 1, 0, 0, '', '1,2', 0, '', '0.00', 41, '2019-11-30 12:15:47', '2020-04-17 10:36:31', 1, 1),
(36, '34', '41', '999.00', 'item', '', '', '009', 4, 0, 0, 0, 0, 0, '', '1,2', 1, '899', '0.00', 900, '2019-11-30 12:21:05', '2019-12-15 11:53:57', 1, 1),
(37, '34,35,36', '40,45', '200.00', '', '12', '', '125', 0, 0, 1, 0, 1, 0, '1,2', '1,2,3', 1, '23', '100.00', 0, '2019-12-17 11:21:43', '2020-05-04 12:29:45', 1, 1),
(38, '1111', '2222', '200.00', '', '', '', '125', 0, 0, 1, 1, 1, 0, '1,2,3,4,5', '', 1, '23', '100.00', 0, '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(39, '1112', '2223', '500.00', '', '', '', '120', 0, 0, 1, 1, 1, 1, '1,2,3,4,5', '', 1, '200', '1000.00', 0, '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(40, '1113', '2224', '1000.00', '', '', '', '400', 0, 0, 1, 1, 1, 1, '1,2,3,4,5', '', 1, '250', '10000.00', 0, '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(41, '1114', '2225', '1500.00', '', '', '', '500', 0, 0, 1, 1, 1, 1, '1,2,3,4,5', '', 1, '300', '100000.00', 0, '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(42, '34', '40', '25.00', 'kg', '', '20', '1222', 1, 0, 1, 1, 0, 0, '', '', 0, '', '0.00', 0, '2020-02-10 10:30:33', '2020-02-10 10:33:08', 1, 1),
(43, '34', '40', '250.00', 'item', '', '500', '1222', 2, 0, 1, 0, 0, 0, '', '', 0, '', '0.00', 0, '2020-02-10 13:21:33', '2020-02-11 09:07:44', 1, 1),
(44, '34', '40', '12.00', 'item', '', '', '1222sd', 3, 0, 1, 0, 0, 0, '', '', 0, '', '0.00', 0, '2020-02-10 13:53:07', '2020-02-10 13:53:07', 1, 1),
(45, '34', '41', '12.00', 'item', '', '', '12313', 4, 0, 1, 0, 0, 0, '', '', 0, '', '0.00', 1, '2020-02-10 13:56:04', '2020-02-10 14:43:52', 1, 1),
(46, '34,39', '41,53,54', '2213.00', 'item', '', '1', '43432', 5, 0, 1, 0, 0, 0, '', '', 0, '', '0.00', 0, '2020-02-12 12:30:19', '2020-02-12 12:38:18', 1, 1),
(47, '1,2', '2,3', '123.00', 'item', '', '', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 18:55:56', '2020-02-26 18:55:56', 1, 1),
(48, '1,2', '2,3', '123.00', 'item', '', '', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 18:56:06', '2020-02-26 18:56:06', 1, 1),
(49, '1,2', '2,3', '123.00', 'item', '', '', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 18:57:09', '2020-02-26 18:57:09', 1, 1),
(50, '1,2', '3,4', '123.00', 'kg', '', '', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 18:57:09', '2020-02-26 18:57:09', 1, 1),
(51, '1,2', '2,3', '123.00', 'item', '', '', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:05:46', '2020-02-26 19:05:46', 1, 1),
(52, '1,2', '3,4', '123.00', 'kg', '', '', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:05:46', '2020-02-26 19:05:46', 1, 1),
(53, '1,2', '2,3', '123.00', 'item', '', '20', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:06:44', '2020-02-26 19:06:44', 1, 1),
(54, '1,2', '3,4', '123.00', 'kg', '', '25', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:06:44', '2020-02-26 19:06:44', 1, 1),
(55, '1,2', '2,3', '123.00', 'item', '', '20', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:20:44', '2020-02-26 19:20:44', 1, 1),
(56, '1,2', '3,4', '123.00', 'kg', '', '25', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:20:44', '2020-02-26 19:20:44', 1, 1),
(57, '1,2', '2,3', '123.00', 'item', '', '20', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:20:51', '2020-02-26 19:20:51', 1, 1),
(58, '1,2', '3,4', '123.00', 'kg', '', '25', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:20:51', '2020-02-26 19:20:51', 1, 1),
(59, '1,2', '2,3', '123.00', 'item', '', '20', '1234', 0, 0, 1, 1, 1, 1, '1,2', '', 1, '50', '300.00', 0, '2020-02-26 19:22:14', '2020-02-26 19:22:14', 1, 1),
(60, '1,2', '3,4', '123.00', 'kg', '', '25', '12345', 0, 0, 1, 1, 0, 1, '2,3', '', 0, '', '0.00', 0, '2020-02-26 19:22:14', '2020-02-26 19:22:14', 1, 1),
(61, '34,37', '42,43,51,52', '250.00', 'item', '', '1', '1222', 1, 0, 1, 0, 0, 0, '', '1,3', 0, '', '0.00', 0, '2020-03-04 17:36:52', '2020-03-04 17:36:52', 1, 1),
(62, '34,35', '43,44', '12.00', 'item', '', '1', '212321321', 2, 0, 1, 1, 0, 0, '', '1,2', 0, '', '0.00', 0, '2020-04-14 19:36:50', '2020-04-14 19:36:50', 1, 1),
(63, '34,35', '43,44', '12.00', 'item', '', '1', '212321321', 3, 0, 1, 1, 0, 0, '', '1,2', 0, '', '0.00', 0, '2020-04-14 19:37:47', '2020-04-14 19:37:47', 1, 1),
(64, '34,35', '40,41,45', '250.00', 'item', '', '1', 'ewe123432', 4, 0, 1, 1, 0, 0, '', '2,3', 0, '', '0.00', 0, '2020-04-14 19:39:31', '2020-04-14 19:39:31', 1, 1),
(65, '34,35', '42,43', '250.00', 'item', '123', '1', 'dsfdsaf', 5, 0, 1, 0, 0, 0, '', '1,2', 0, '', '0.00', 0, '2020-04-17 08:59:49', '2020-04-17 08:59:49', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_inside`
--

CREATE TABLE `products_inside` (
  `ProductInsideID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `InsideTitle` varchar(255) NOT NULL,
  `InsideTitleAr` varchar(255) NOT NULL,
  `InsideImage` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_inside`
--

INSERT INTO `products_inside` (`ProductInsideID`, `ProductID`, `InsideTitle`, `InsideTitleAr`, `InsideImage`) VALUES
(1, 44, 'add whats inside', 'add whats inside arab', '1'),
(2, 44, 'this is testin', 'dfasdfsa', '1'),
(3, 44, 'dsfds', 'dsfds', '1'),
(5, 35, 'dsf', 'dfsdf', 'uploads/images/183391624052020021001045682902625_212951926410060_6594469099533762560_o.jpg'),
(8, 35, 'sdfdsfd', 'dsfadf', 'uploads/images/498906680052020021002524372124360_2558790247717603_7384555194358431744_n.jpg'),
(10, 43, 'test', 'test', 'uploads/images/933186558412020021108233981898777_2460926880890961_7466254659263397888_n.jpg'),
(14, 43, 'sdf', 'dsfa', 'uploads/images/658282548012020021108014172124360_2558790247717603_7384555194358431744_n.jpg'),
(16, 43, 'dsfds', 'sadfda', 'uploads/images/995034514482020021108405284212650_115903443280448_6658839702839230464_o.jpg'),
(18, 43, 'dsfdsa', 'dsfasd', 'uploads/images/337343665992020021108505372124360_2558790247717603_7384555194358431744_n.jpg'),
(22, 43, 'dfdsfas', 'dsfadsf', 'uploads/images/176056264062020021109140784212650_115903443280448_6658839702839230464_o.jpg'),
(23, 43, 'fdsaf', 'dsfds', 'uploads/images/32731617338202002110944072019-12-09.png'),
(24, 43, 'dsfdsaf', 'dsfads', 'uploads/images/620398903912020021109440781898777_2460926880890961_7466254659263397888_n.jpg'),
(25, 59, 'inside one', 'inside one', 'imagename'),
(26, 59, 'inside two', 'inside two', 'iamgename'),
(27, 62, 'dsf', 'sdfds', 'uploads/images/461728493120200414075036Screenshot_2020-04-14_at_10.40.00_AM.png'),
(28, 63, 'dsf', 'sdfds', 'uploads/images/9516484444120200414074737Screenshot_2020-04-14_at_10.40.00_AM.png'),
(29, 65, 'test', 'test', '');

-- --------------------------------------------------------

--
-- Table structure for table `products_text`
--

CREATE TABLE `products_text` (
  `ProducrTextID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `Ingredients` varchar(1000) NOT NULL,
  `Specifications` text NOT NULL,
  `Keywords` varchar(500) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `MetaTags` varchar(255) NOT NULL,
  `MetaKeywords` varchar(255) NOT NULL,
  `MetaDescription` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_text`
--

INSERT INTO `products_text` (`ProducrTextID`, `ProductID`, `Title`, `Description`, `Ingredients`, `Specifications`, `Keywords`, `SystemLanguageID`, `MetaTags`, `MetaKeywords`, `MetaDescription`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(48, 34, 'CS-14', '', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 1, '', '', '', '2019-11-30 12:13:54', '2020-02-14 12:20:04', 1, 72),
(49, 34, 'CS-14', '', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 2, '', '', '', '2019-11-30 12:13:54', '2019-11-30 12:13:54', 1, 1),
(50, 35, 'cs-16', 'available', 'kkk', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 1, '', '', '', '2019-11-30 12:15:47', '2020-04-17 10:36:31', 1, 1),
(51, 35, 'cs-16', 'available', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 2, '', '', '', '2019-11-30 12:15:47', '2019-11-30 12:15:47', 1, 1),
(52, 36, 'CS-17', '', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 1, '', '', '', '2019-11-30 12:21:05', '2019-12-15 11:53:57', 1, 1),
(53, 36, 'CS-17', '', '', '<h5>Box Dimensions</h5>         <ul>             <li>Length:<span>244 mm</span></li>             <li>Weight:<span>244 mm</span></li>             <li>Height:<span>244 mm</span></li>         </ul>         <h5>Order Notes</h5>         <ul>             <li>Minimum Orders:<span>1 Piece</span></li>             <li>Processing Time:<span>3 days</span></li>         </ul>', '', 2, '', '', '', '2019-11-30 12:21:05', '2019-11-30 12:21:05', 1, 1),
(54, 37, 'Intour Hotel', 'available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice Product', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:21:43', '2020-05-04 12:29:45', 1, 1),
(55, 38, 'Intour Hotel', 'available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice Product', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(56, 39, 'Cholocate Lounge', 'available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice Product', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(57, 40, 'Carve Burger', 'available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice Product', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(58, 41, 'Al Shurafa', 'not available', 'asdf', 'asdfghjkl', 'asdf', 1, 'Nice One', 'creative', 'This is a very nice product, we are working on it to make it more reliable', '2019-12-17 11:24:06', '2019-12-17 11:24:06', 1, 1),
(59, 42, 'per kg product', 'this is testing product', 'this is ingredients for testing products', '<p>this is specification for these products</p>', 'keywords', 1, 'testing', 'meta keywords', 'this is testing', '2020-02-10 10:30:33', '2020-02-10 10:33:08', 1, 1),
(60, 42, 'per kg product', 'this is testing product', 'this is ingredients for testing products', '<p>this is specification for these products</p>', 'keywords', 2, 'testing', 'meta keywords', 'this is testing', '2020-02-10 10:30:33', '2020-02-10 10:30:33', 1, 1),
(61, 43, 'test', 'this is testing', 'this is testing', '<p>this is tesitn</p>', 'test', 1, 'testing', 'meta keywords', 'this is teistn', '2020-02-10 13:21:33', '2020-02-11 09:07:44', 1, 1),
(62, 43, 'test', 'this is testing', 'this is testing', '<p>this is tesitn</p>', 'test', 2, 'testing', 'meta keywords', 'this is teistn', '2020-02-10 13:21:33', '2020-02-10 13:21:33', 1, 1),
(63, 44, 'test agin ain', 'this is tesitng', 'this is tesitn', '<p>this is testing</p>', 'dscfds', 1, 'dsfdas', 'sdaf', 'this is tesitng', '2020-02-10 13:53:07', '2020-02-10 13:53:07', 1, 1),
(64, 44, 'test agin ain', 'this is tesitng', 'this is tesitn', '<p>this is testing</p>', 'dscfds', 2, 'dsfdas', 'sdaf', 'this is tesitng', '2020-02-10 13:53:07', '2020-02-10 13:53:07', 1, 1),
(65, 45, 'testfdsfds', 'tdfjdalskfj', 'kdlsfjnlksadjfkl', '<p>psdfjlkadsjfl</p>', 'keywords', 1, 'this is testing', 'dkfjlkdsa', 'kdsjflkadsjf', '2020-02-10 13:56:04', '2020-02-10 14:43:52', 1, 1),
(66, 45, 'testfdsfds', 'tdfjdalskfj', 'kdlsfjnlksadjfkl', '<p>psdfjlkadsjfl</p>', 'keywords', 2, 'this is testing', 'dkfjlkdsa', 'kdsjflkadsjf', '2020-02-10 13:56:04', '2020-02-10 13:56:04', 1, 1),
(67, 46, 'this is tesitn', 'this is testing', 'this is tesitng', '<p>thjisdhf d</p>', 'keywords', 1, 'dsfdas', 'dkfjlkdsa', 'tkhsjfdkjsfl', '2020-02-12 12:30:19', '2020-02-12 12:38:18', 1, 1),
(68, 46, 'this is tesitn', 'this is testing', 'this is tesitng', '<p>thjisdhf d</p>', 'keywords', 2, 'dsfdas', 'dkfjlkdsa', 'tkhsjfdkjsfl', '2020-02-12 12:30:19', '2020-02-12 12:30:19', 1, 1),
(69, 47, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 18:55:56', '2020-02-26 18:55:56', 1, 1),
(70, 48, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 18:56:06', '2020-02-26 18:56:06', 1, 1),
(71, 49, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 18:57:09', '2020-02-26 18:57:09', 1, 1),
(72, 50, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 18:57:09', '2020-02-26 18:57:09', 1, 1),
(73, 51, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:05:46', '2020-02-26 19:05:46', 1, 1),
(74, 52, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:05:46', '2020-02-26 19:05:46', 1, 1),
(75, 53, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:06:44', '2020-02-26 19:06:44', 1, 1),
(76, 54, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:06:44', '2020-02-26 19:06:44', 1, 1),
(77, 55, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:20:44', '2020-02-26 19:20:44', 1, 1),
(78, 56, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:20:44', '2020-02-26 19:20:44', 1, 1),
(79, 57, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:20:51', '2020-02-26 19:20:51', 1, 1),
(80, 58, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:20:51', '2020-02-26 19:20:51', 1, 1),
(81, 59, 'sarfraz', 'this is testing', 'testing', 'dfdsf', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:22:14', '2020-02-26 19:22:14', 1, 1),
(82, 60, 'sarfrz2', 'this is testing', 'testing', 'dfdsfd', 'test,test', 1, 'test,test', 'test,test', 'test,test', '2020-02-26 19:22:14', '2020-02-26 19:22:14', 1, 1),
(83, 61, 'tagging product', 'this is testing', 'this is testing', '<p>this is testing<br></p>', 'dfgdgdf', 1, 'gfdg', 'fdgfdsg', 'fdgfsdgsfdgsfd', '2020-03-04 17:36:52', '2020-03-04 17:36:52', 1, 1),
(84, 61, 'tagging product', 'this is testing', 'this is testing', '<p>this is testing<br></p>', 'dfgdgdf', 2, 'gfdg', 'fdgfdsg', 'fdgfsdgsfdgsfd', '2020-03-04 17:36:52', '2020-03-04 17:36:52', 1, 1),
(85, 62, 'testxzcxzc', 'this ', 'sdfds', '<p>dsfdas</p>', 'dsf', 1, 'dsfdss', 'dfsdf', 'fdsf', '2020-04-14 19:36:50', '2020-04-14 19:36:50', 1, 1),
(86, 62, 'testxzcxzc', 'this ', 'sdfds', '<p>dsfdas</p>', 'dsf', 2, 'dsfdss', 'dfsdf', 'fdsf', '2020-04-14 19:36:50', '2020-04-14 19:36:50', 1, 1),
(87, 63, 'asdadsadsad', 'this ', 'sdfds', '<p>dsfdas</p>', 'dsf', 1, 'dsfdss', 'dfsdf', 'fdsf', '2020-04-14 19:37:47', '2020-04-14 19:37:47', 1, 1),
(88, 63, 'asdadsadsad', 'this ', 'sdfds', '<p>dsfdas</p>', 'dsf', 2, 'dsfdss', 'dfsdf', 'fdsf', '2020-04-14 19:37:47', '2020-04-14 19:37:47', 1, 1),
(89, 64, 'wrewrwr', 'this is testing', 'this is testing', '<p>this is testingthis is testing<br></p>', 'keywords', 1, 'testing', 'meta keywords', 'thsihfdpsfj', '2020-04-14 19:39:31', '2020-04-14 19:39:31', 1, 1),
(90, 64, 'wrewrwr', 'this is testing', 'this is testing', '<p>this is testingthis is testing<br></p>', 'keywords', 2, 'testing', 'meta keywords', 'thsihfdpsfj', '2020-04-14 19:39:31', '2020-04-14 19:39:31', 1, 1),
(91, 65, 'teresf', 'this is testing', 'sdfdsaf', '<p>dsfdsa</p>', 'keywords', 1, 'dsfdas', 'meta keywords', '', '2020-04-17 08:59:49', '2020-04-17 08:59:49', 1, 1),
(92, 65, 'teresf', 'this is testing', 'sdfdsaf', '<p>dsfdsa</p>', 'keywords', 2, 'dsfdas', 'meta keywords', '', '2020-04-17 08:59:49', '2020-04-17 08:59:49', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_nutritions`
--

CREATE TABLE `product_nutritions` (
  `NutritionProductID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `NutritionID` int(11) NOT NULL,
  `Quantity` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_nutritions`
--

INSERT INTO `product_nutritions` (`NutritionProductID`, `ProductID`, `NutritionID`, `Quantity`) VALUES
(29, 33, 2, '0'),
(35, 38, 1, '20'),
(36, 38, 2, '0'),
(37, 38, 3, '0'),
(38, 38, 4, '0'),
(39, 39, 1, '20'),
(40, 39, 2, '0'),
(41, 39, 3, '0'),
(42, 39, 4, '0'),
(43, 40, 1, '30'),
(44, 40, 2, '0'),
(45, 40, 3, '0'),
(46, 40, 4, '0'),
(47, 41, 1, '50'),
(48, 41, 2, '0'),
(49, 41, 3, '0'),
(50, 41, 4, '0'),
(55, 42, 1, '12'),
(56, 42, 2, '12'),
(59, 44, 1, '20'),
(62, 45, 2, '25'),
(74, 43, 1, '20'),
(76, 46, 2, '20'),
(77, 34, 3, '10'),
(78, 47, 1, '4'),
(79, 47, 2, '5'),
(80, 48, 1, '4'),
(81, 48, 2, '5'),
(82, 49, 1, '4'),
(83, 49, 2, '5'),
(84, 50, 1, '4'),
(85, 50, 2, '5'),
(86, 51, 1, '4'),
(87, 51, 2, '5'),
(88, 52, 1, '4'),
(89, 52, 2, '5'),
(90, 53, 1, '4'),
(91, 53, 2, '5'),
(92, 54, 1, '4'),
(93, 54, 2, '5'),
(94, 55, 1, '4'),
(95, 55, 2, '5'),
(96, 56, 1, '4'),
(97, 56, 2, '5'),
(98, 57, 1, '4'),
(99, 57, 2, '5'),
(100, 58, 1, '4'),
(101, 58, 2, '5'),
(102, 59, 1, '4'),
(103, 59, 2, '5'),
(104, 60, 1, '4'),
(105, 60, 2, '5'),
(110, 61, 1, '12'),
(111, 62, 1, '12'),
(112, 62, 2, '12'),
(113, 63, 1, '12'),
(114, 63, 2, '12'),
(115, 64, 1, '10'),
(116, 64, 2, '10'),
(118, 65, 2, '12'),
(122, 35, 1, '0'),
(124, 37, 1, '20g');

-- --------------------------------------------------------

--
-- Table structure for table `product_ratings`
--

CREATE TABLE `product_ratings` (
  `ProductRatingID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Rating` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_ratings`
--

INSERT INTO `product_ratings` (`ProductRatingID`, `ProductID`, `UserID`, `Rating`) VALUES
(1, 35, 64, 4),
(7, 35, 65, 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `ProductReviewID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Comment` text CHARACTER SET utf8 NOT NULL,
  `CreatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`ProductReviewID`, `ProductID`, `UserID`, `Title`, `Comment`, `CreatedAt`) VALUES
(1, 34, 64, 'Review', 'This is good product', '2020-01-01 07:24:16');

-- --------------------------------------------------------

--
-- Table structure for table `product_store_availability`
--

CREATE TABLE `product_store_availability` (
  `ProductStoreAvailability` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `StoreID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_store_availability`
--

INSERT INTO `product_store_availability` (`ProductStoreAvailability`, `ProductID`, `StoreID`, `Quantity`) VALUES
(5, 35, 6, 33),
(6, 35, 4, 22),
(7, 35, 5, 16),
(8, 36, 6, 12),
(9, 50, 1, 5),
(10, 50, 3, 6),
(11, 51, 1, 3),
(12, 51, 2, 4),
(13, 52, 1, 5),
(14, 52, 3, 6),
(15, 53, 1, 3),
(16, 53, 2, 4),
(17, 54, 1, 5),
(18, 54, 3, 6),
(19, 55, 1, 3),
(20, 55, 2, 4),
(21, 56, 1, 5),
(22, 56, 3, 6),
(23, 57, 1, 3),
(24, 57, 2, 4),
(25, 58, 1, 5),
(26, 58, 3, 6),
(27, 59, 1, 3),
(28, 59, 2, 4),
(29, 60, 1, 5),
(30, 60, 3, 6),
(31, 36, 4, -879),
(32, 35, 7, 16),
(33, 34, 6, 33),
(34, 34, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `refunds`
--

CREATE TABLE `refunds` (
  `RefundID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `refunds_text`
--

CREATE TABLE `refunds_text` (
  `RefundTextID` int(11) NOT NULL,
  `RefundID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `RoleID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `IsActive` tinyint(4) NOT NULL,
  `CreatedBy` datetime NOT NULL,
  `UpdatedBy` datetime NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`RoleID`, `SortOrder`, `Hide`, `IsActive`, `CreatedBy`, `UpdatedBy`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 0, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-04-03 00:00:00', '2018-04-03 00:00:00'),
(2, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-02 11:37:19', '2018-05-02 12:03:34'),
(3, 2, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-04 17:33:54', '2019-12-15 12:09:26'),
(4, 3, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-02-01 10:06:52', '2019-02-01 10:06:52'),
(5, 4, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-02-20 13:50:07', '2019-02-20 13:50:07'),
(6, 5, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-04-21 20:06:37', '2019-04-21 20:06:37'),
(7, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-04-21 20:09:56', '2019-04-21 20:09:56'),
(8, 7, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-12-15 12:10:19', '2019-12-15 12:10:25');

-- --------------------------------------------------------

--
-- Table structure for table `roles_text`
--

CREATE TABLE `roles_text` (
  `RoleTextID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_text`
--

INSERT INTO `roles_text` (`RoleTextID`, `RoleID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Admin', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(2, 2, 'Store/Warehouse User', 1, '2018-05-02 11:37:19', '2018-05-02 12:03:34', 1, 1),
(4, 3, 'Driver User', 1, '2018-05-04 17:33:54', '2019-12-15 12:09:26', 1, 1),
(5, 4, 'Store Admin', 1, '2019-02-01 10:06:52', '2019-02-01 10:06:52', 1, 1),
(6, 4, 'Store Admin', 2, '2019-02-01 10:07:00', '2019-02-01 10:07:00', 1, 1),
(7, 5, 'Customer User', 1, '2019-02-20 13:50:07', '2019-02-20 13:50:07', 1, 1),
(8, 6, 'Order User', 1, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(9, 6, 'Order User', 2, '2019-04-21 20:06:37', '2019-04-21 20:06:37', 1, 1),
(10, 7, 'tet', 1, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(11, 7, 'tet', 2, '2019-04-21 20:09:56', '2019-04-21 20:09:56', 1, 1),
(12, 8, 'New Employee', 1, '2019-12-15 12:10:19', '2019-12-15 12:10:25', 1, 1),
(13, 8, 'New Employee', 2, '2019-12-15 12:10:19', '2019-12-15 12:10:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `search_tags`
--

CREATE TABLE `search_tags` (
  `SearchTagID` int(11) NOT NULL,
  `SearchTag` text CHARACTER SET utf8 NOT NULL,
  `UserID` int(11) NOT NULL,
  `SearchedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `search_tags`
--

INSERT INTO `search_tags` (`SearchTagID`, `SearchTag`, `UserID`, `SearchedAt`) VALUES
(11, 'rassam', 64, '2019-12-10 19:18:14'),
(10, 'coconut chocolate', 64, '2019-11-27 11:20:32'),
(12, 'cake', 1553578977, '2019-12-31 07:41:35'),
(13, 'cake', 1553578977, '2019-12-31 07:41:57'),
(14, 'cake', 1553578977, '2019-12-31 07:42:37');

-- --------------------------------------------------------

--
-- Table structure for table `site_images`
--

CREATE TABLE `site_images` (
  `SiteImageID` int(11) NOT NULL,
  `ImageType` varchar(50) NOT NULL,
  `FileID` int(11) NOT NULL,
  `ImageName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_images`
--

INSERT INTO `site_images` (`SiteImageID`, `ImageType`, `FileID`, `ImageName`) VALUES
(1, 'product', 1, 'uploads/images/2344767592720190214125630img3.png'),
(2, 'product', 1, 'uploads/images/9239674724220190214125630img5.png'),
(3, 'product', 1, 'uploads/images/3491587502620190214125630img6.png'),
(4, 'product', 2, 'uploads/images/2344767592720190214125630img3.png'),
(5, 'product', 3, 'uploads/images/2344767592720190214125630img3.png'),
(6, 'product', 4, 'uploads/images/2344767592720190214125630img3.png'),
(7, 'product', 5, 'uploads/images/2344767592720190214125630img3.png'),
(9, 'collection', 1, 'uploads/images/5799135776220190220034836banner2.png'),
(10, 'collection', 2, 'uploads/images/7859194613820190220034836ch1.png'),
(11, 'collection', 3, 'uploads/images/6996744598620190220034836ch3.png'),
(12, 'product', 12, 'uploads/images/8654564795420190319094627WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(13, 'product', 12, 'uploads/images/6767398029920190319094627WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(14, 'product', 12, 'uploads/images/7913438846620190319094627WhatsApp_Image_2019-03-11_at_11.36.57-2.jpeg'),
(15, 'product', 12, 'uploads/images/2400193184020190319094627WhatsApp_Image_2019-03-11_at_11.36.57-min.jpeg'),
(16, 'product', 12, 'uploads/images/9587317615320190319094627WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(17, 'product', 13, 'uploads/images/5661712987520190319091028WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(18, 'product', 13, 'uploads/images/4774970338320190319091028WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(19, 'product', 13, 'uploads/images/8640400517820190319091028WhatsApp_Image_2019-03-11_at_11.36.57-2.jpeg'),
(20, 'product', 13, 'uploads/images/6986129758720190319091028WhatsApp_Image_2019-03-11_at_11.36.57-min.jpeg'),
(21, 'product', 13, 'uploads/images/9193756882320190319091028WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(22, 'product', 14, 'uploads/images/1789641263720190319095731WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(23, 'product', 14, 'uploads/images/7443086141920190319095731WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(24, 'product', 14, 'uploads/images/7882017211620190319095731WhatsApp_Image_2019-03-11_at_11.36.57-2.jpeg'),
(25, 'product', 14, 'uploads/images/1367389048820190319095731WhatsApp_Image_2019-03-11_at_11.36.57-min.jpeg'),
(26, 'product', 14, 'uploads/images/8910321010220190319095731WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(27, 'product', 1, 'uploads/images/3134718524420190319092543WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(28, 'product', 15, 'uploads/images/1927226043120190319093745WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(29, 'product', 15, 'uploads/images/594730261920190319093745WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(30, 'product', 15, 'uploads/images/746558200920190319094845WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(31, 'product', 15, 'uploads/images/3901523010420190319094845WhatsApp_Image_2019-03-11_at_11.27.31.jpeg'),
(32, 'collection', 4, 'uploads/images/2569068038720190319091058WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(33, 'collection', 5, 'uploads/images/6493461562920190319103105WhatsApp_Image_2019-03-11_at_11.36.57-2.jpeg'),
(34, 'collection', 6, 'uploads/images/2147949252820190319104906WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(35, 'collection', 7, 'uploads/images/9946759725120190319103425WhatsApp_Image_2019-03-11_at_11.36.57.jpeg'),
(36, 'collection', 1, 'uploads/images/6082116845620190326034946cp10.png'),
(37, 'collection', 3, 'uploads/images/9619593784020190326032451ch5.png'),
(38, 'product', 12, 'uploads/images/15617416295201903310422201ch3.png'),
(39, 'product', 16, 'uploads/images/30260719580201903310411211ch3.png'),
(40, 'collection', 8, 'uploads/images/11038782355201903310416327859194613820190220034836ch1.png'),
(41, 'collection', 9, 'uploads/images/3562575277020190331045334ch2.png'),
(42, 'collection', 10, 'uploads/images/4915248640720190401100622ch5.png'),
(44, 'collection', 12, 'uploads/images/2178378257820190402094614WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(45, 'collection', 13, 'uploads/images/8442736936420190402090215WhatsApp_Image_2019-03-11_at_11.27.31-2.jpeg'),
(48, 'collection', 11, 'uploads/images/4404786574920190402093523img4.png'),
(49, 'collection', 11, 'uploads/images/6100591700120190402093523img5.png'),
(52, 'product', 6, 'uploads/images/99278711879201904031131306.png'),
(53, 'collection', 14, 'uploads/images/1562898902201904031121316.png'),
(54, 'EmailFile', 6, 'assets/backend/images/no_image.jpg'),
(55, 'product', 17, 'uploads/images/42666493920190410043726ch5.png'),
(56, 'product', 10, 'uploads/images/2344767592720190214125630img3.png'),
(58, 'product', 18, 'uploads/images/113077851222019042701422530260719580201903310411211ch3.png'),
(59, 'product', 19, 'uploads/images/2384869090820190430060904Depositphotos_1314091_ds.jpg'),
(60, 'product', 20, 'uploads/images/7414286665220190501081647image001.png'),
(61, 'product', 21, 'uploads/images/9514313584320190501081548image005.png'),
(62, 'product', 22, 'uploads/images/7217078571020190501080050image009.png'),
(63, 'product', 23, 'uploads/images/5599001587820190501083551image011.png'),
(64, 'product', 24, 'uploads/images/9542100573120190501082053image409.png'),
(65, 'product', 25, 'uploads/images/2072127942120190501080159image361.png'),
(66, 'product', 26, 'uploads/images/8307434168320190501094800image363.png'),
(67, 'product', 27, 'uploads/images/3706199802220190501090802image391.png'),
(68, 'product', 28, 'uploads/images/4540537429120190501092303image111.png'),
(69, 'product', 29, 'uploads/images/8478116233220190501092107image027.png'),
(70, 'product', 30, 'uploads/images/6131364861820190501093608image154.png'),
(71, 'product', 31, 'uploads/images/5314166646520190501091709image147.png'),
(72, 'EmailFile', 7, 'assets/backend/images/no_image.jpg'),
(73, 'product', 32, 'uploads/images/103224682412019112711390686_n.jpg'),
(74, 'collection', 15, 'uploads/images/763457818792019112711141273134093_159953508412574_8729455205950797333_n.jpg'),
(75, 'collection', 16, 'uploads/images/242144192442019112711581372489197_141376287210012_4937717711887326865_n.jpg'),
(76, 'collection', 17, 'uploads/images/486683868902019112711411586_n.jpg'),
(77, 'collection', 18, 'uploads/images/518485794022019112711511772278841_1015894295423891_405066893956981627_n.jpg'),
(78, 'product', 33, 'uploads/images/9018959715320191127112631MIC_0415.jpg'),
(79, 'product', 33, 'uploads/images/8338483444720191127112631MIC_0420.jpg'),
(80, 'product', 33, 'uploads/images/1071768085020191127112631MIC_0422.jpg'),
(81, 'product', 33, 'uploads/images/9980983777920191127112631MIC_0426.jpg'),
(82, 'product', 33, 'uploads/images/5770069657920191127112631MIC_0429.jpg'),
(83, 'product', 33, 'uploads/images/3828564968520191127112631MIC_0432_copy.jpg'),
(85, 'product', 35, 'uploads/images/11514054984201911301247153f7656bc-a860-4341-b3e1-cbd8535171aa_(1).jpg'),
(86, 'product', 36, 'uploads/images/586559584722019113012052189dced02-057e-4a76-9922-d8f53b52a0a4.jpg'),
(88, 'collection', 20, 'uploads/images/1267362765520191130122446belize-wedding-vacation-packages.jpg'),
(90, 'collection', 21, 'uploads/images/9561003988720191202114531h.jpg'),
(91, 'collection', 22, 'uploads/images/4627132505920191202111832y.jpg'),
(92, 'collection', 23, 'uploads/images/30784683517201912021120348596770922320191130120452aj3enycveq98cmuyjb.jpg'),
(93, 'product', 34, 'uploads/images/4105817803920191204113827four.jpg'),
(94, 'product', 34, 'uploads/images/2331073380420191204113827one.jpg'),
(95, 'product', 34, 'uploads/images/814017012020191204113827three.jpg'),
(96, 'product', 34, 'uploads/images/5896763070920191204113827two.jpg'),
(97, 'collection', 24, 'uploads/images/648660360112019121511224801_choco.jpg'),
(98, 'collection', 19, 'uploads/images/639948335152019121511015001_choco.jpg'),
(99, 'product', 42, 'uploads/images/928468098762020021010333072124360_2558790247717603_7384555194358431744_n.jpg'),
(100, 'product', 43, 'uploads/images/299058752812020021001332181898777_2460926880890961_7466254659263397888_n.jpg'),
(101, 'product', 44, 'uploads/images/311329206242020021001075372124360_2558790247717603_7384555194358431744_n.jpg'),
(102, 'product', 45, 'uploads/images/110078636042020021001045684212650_115903443280448_6658839702839230464_o.jpg'),
(103, 'product', 46, 'uploads/images/990975995972020021212193072124360_2558790247717603_7384555194358431744_n.jpg'),
(104, 'product', 61, 'uploads/images/104353037942020030405523672124360_2558790247717603_7384555194358431744_n.jpg'),
(105, 'product', 62, 'uploads/images/9937563576320200414075036Screenshot_2020-04-14_at_10.40.00_AM.png'),
(106, 'product', 62, 'uploads/images/3433369168620200414075036Screenshot_2020-04-14_at_10.40.07_AM.png'),
(107, 'product', 63, 'uploads/images/5234908743420200414074737Screenshot_2020-04-14_at_10.40.00_AM.png'),
(108, 'product', 63, 'uploads/images/5470673631020200414074737Screenshot_2020-04-14_at_10.40.07_AM.png'),
(109, 'product', 64, 'uploads/images/43186589761202004140731393d-box-logo_1103-876.jpg'),
(110, 'product', 65, 'uploads/images/82769049583202004170849593d-box-logo_1103-876.jpg'),
(111, 'EmailFile', 8, 'assets/backend/images/no_image.jpg'),
(112, 'EmailFile', 9, 'assets/backend/images/no_image.jpg'),
(113, 'EmailFile', 10, 'assets/backend/images/no_image.jpg'),
(114, 'EmailFile', 11, 'assets/backend/images/no_image.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `SiteSettingID` int(11) NOT NULL,
  `SiteName` varchar(255) NOT NULL,
  `SiteImage` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Whatsapp` varchar(255) NOT NULL,
  `Skype` varchar(255) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `Fax` varchar(255) NOT NULL,
  `OpenTime` varchar(255) NOT NULL,
  `CloseTime` varchar(255) NOT NULL,
  `FacebookUrl` varchar(255) NOT NULL,
  `GoogleUrl` varchar(255) NOT NULL,
  `LinkedInUrl` varchar(255) NOT NULL,
  `TwitterUrl` varchar(255) NOT NULL,
  `InstagramUrl` varchar(255) NOT NULL,
  `YoutubeUrl` varchar(255) NOT NULL,
  `VatNumber` varchar(255) NOT NULL,
  `VatPercentage` decimal(10,2) NOT NULL,
  `LoyaltyFactor` int(11) NOT NULL COMMENT 'Defines 1 SAR is equal to how many loyalty points',
  `DaysToDeliver` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`SiteSettingID`, `SiteName`, `SiteImage`, `Email`, `Whatsapp`, `Skype`, `PhoneNumber`, `Fax`, `OpenTime`, `CloseTime`, `FacebookUrl`, `GoogleUrl`, `LinkedInUrl`, `TwitterUrl`, `InstagramUrl`, `YoutubeUrl`, `VatNumber`, `VatPercentage`, `LoyaltyFactor`, `DaysToDeliver`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 'Chocomood', 'uploads/5602750077020191126043459g.jpg', 'test@cms.com', '123456789', '', '9200 21 2112', '', '1545300000', '1545332400', 'https://www.facebook.com', '', '', 'https://www.twitter.com', '', '', '0000000000000000', '0.00', 20, 2, '2018-04-03 14:33:23', '2020-04-14 19:41:12');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `StoreID` int(11) NOT NULL,
  `VatNo` varchar(100) DEFAULT NULL,
  `WorkingHoursSaturdayToThursdayFrom` varchar(100) NOT NULL,
  `WorkingHoursSaturdayToThursdayTo` varchar(100) NOT NULL,
  `WorkingHoursFridayFrom` varchar(100) NOT NULL,
  `WorkingHoursFridayTo` varchar(100) NOT NULL,
  `CityID` int(11) NOT NULL,
  `DistrictID` varchar(255) NOT NULL,
  `Latitude` double NOT NULL,
  `Longitude` double NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`StoreID`, `VatNo`, `WorkingHoursSaturdayToThursdayFrom`, `WorkingHoursSaturdayToThursdayTo`, `WorkingHoursFridayFrom`, `WorkingHoursFridayTo`, `CityID`, `DistrictID`, `Latitude`, `Longitude`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(4, '123214324', '', '', '', '', 1337, '49608,49613', 21.521503722412003, 39.160286773258235, 0, 0, 1, '2019-11-27 00:11:07', '2020-04-14 10:20:33', 1, 1),
(5, NULL, '', '', '', '', 1344, '49614,49659', 21.484716, 39.189606, 1, 0, 1, '2020-02-11 08:05:15', '2020-04-03 16:48:04', 1, 1),
(6, '', '', '', '', '', 113, '13', 21.521503722412003, 39.160286773258235, 0, 0, 1, '2019-11-27 00:11:07', '2020-04-29 13:40:43', 1, 1),
(7, '213213213', '10:00 AM', '01:30 PM', '12:30 PM', '03:00 PM', 77, '1,2,4', 21.484716, 39.189606, 1, 0, 1, '2020-04-17 16:46:09', '2020-04-17 16:51:36', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stores_text`
--

CREATE TABLE `stores_text` (
  `StoreTextID` int(11) NOT NULL,
  `StoreID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Address` text NOT NULL,
  `WorkingHours` text NOT NULL,
  `DeliveryTime` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores_text`
--

INSERT INTO `stores_text` (`StoreTextID`, `StoreID`, `Title`, `Address`, `WorkingHours`, `DeliveryTime`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(7, 4, 'Jeddah Branch', 'Falastin, Al-Hamra\'a, Al, Jeddah 23212', 'Saturday-Thursday: 09:00-23:00\r\nFriday: 14:00-23:00', 'Saturday-Thursday: 09:00-23:00\r\nFriday: 14:00-23:00', 1, '2019-11-27 00:11:07', '2020-04-14 10:20:33', 1, 1),
(8, 4, 'Jeddah Branch', 'Falastin, Al-Hamra\'a, Al, Jeddah 23212', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', 2, '2019-11-27 00:11:07', '2019-11-27 00:11:07', 1, 1),
(9, 5, 'test', 'This is testing by sarfraz\r\nThis is testing by sarfraz\r\nThis is testing by sarfraz', 'Saturday-Thursday: 09:00-23:00\r\nFriday: 14:00-23:00', '', 1, '2020-02-11 08:05:15', '2020-04-03 16:48:04', 1, 1),
(10, 5, 'test', 'This is testing by sarfraz\r\nThis is testing by sarfraz\r\nThis is testing by sarfraz', '12', '', 2, '2020-02-11 08:05:15', '2020-02-11 08:05:15', 1, 1),
(11, 6, 'Jeddah Branch 2', '25368, Saudi Arabia', 'Saturday-Thursday: 09:00-23:00\r\nFriday: 14:00-23:00', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', 1, '2019-11-27 00:11:07', '2020-04-29 13:40:43', 1, 1),
(12, 6, 'Jeddah Branch2', 'Falastin, Al-Hamra\'a, Al, Jeddah 23212', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', '9:30 AM – 12:30 PM \r\n4:30 PM – 11:00 PM', 2, '2019-11-27 00:11:07', '2019-11-27 00:11:07', 1, 1),
(13, 7, 'fdsfsdf', 'thospjfpdsjf;lasf', '', 'dfdsaf', 1, '2020-04-17 16:46:09', '2020-04-17 16:51:36', 1, 1),
(14, 7, 'fdsfsdf', 'thospjfpdsjf;lasf', '', 'dfdsaf', 2, '2020-04-17 16:46:09', '2020-04-17 16:46:09', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_languages`
--

CREATE TABLE `system_languages` (
  `SystemLanguageID` int(11) NOT NULL,
  `SystemLanguageTitle` varchar(255) NOT NULL,
  `ShortCode` varchar(255) NOT NULL,
  `IsDefault` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_languages`
--

INSERT INTO `system_languages` (`SystemLanguageID`, `SystemLanguageTitle`, `ShortCode`, `IsDefault`, `IsActive`, `Hide`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'English', 'EN', 1, 0, 0, '2018-04-09 00:00:00', '2018-07-04 16:09:38', 0, 1),
(2, 'Arabic', 'AR', 0, 0, 0, '2018-04-09 00:00:00', '2018-07-04 16:07:19', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `TagID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`TagID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 0, 1, '2020-03-04 16:41:11', '2020-03-04 16:41:11', 1, 1),
(2, 1, 0, 1, '2020-03-04 16:41:25', '2020-03-04 16:41:25', 1, 1),
(3, 2, 0, 1, '2020-03-04 16:41:37', '2020-03-04 16:41:37', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags_text`
--

CREATE TABLE `tags_text` (
  `TagTextID` int(11) NOT NULL,
  `TagID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags_text`
--

INSERT INTO `tags_text` (`TagTextID`, `TagID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Chocolate', 1, '2020-03-04 16:41:11', '2020-03-04 16:41:11', 1, 1),
(2, 1, 'Chocolate', 2, '2020-03-04 16:41:11', '2020-03-04 16:41:11', 1, 1),
(3, 2, 'tofi', 1, '2020-03-04 16:41:25', '2020-03-04 16:41:25', 1, 1),
(4, 2, 'tofi', 2, '2020-03-04 16:41:25', '2020-03-04 16:41:25', 1, 1),
(5, 3, 'cool', 1, '2020-03-04 16:41:37', '2020-03-04 16:41:37', 1, 1),
(6, 3, 'cool', 2, '2020-03-04 16:41:37', '2020-03-04 16:41:37', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tax_shipment_charges`
--

CREATE TABLE `tax_shipment_charges` (
  `TaxShipmentChargesID` int(11) NOT NULL,
  `ChargesType` enum('Tax','Shipment') NOT NULL DEFAULT 'Tax',
  `Amount` double NOT NULL,
  `Type` enum('Fixed','Percentage') NOT NULL DEFAULT 'Fixed',
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `IsDefault` int(11) NOT NULL COMMENT 'Only to be used for shipment methods',
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_shipment_charges`
--

INSERT INTO `tax_shipment_charges` (`TaxShipmentChargesID`, `ChargesType`, `Amount`, `Type`, `Image`, `SortOrder`, `Hide`, `IsActive`, `IsDefault`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 'Shipment', 500, 'Fixed', 'uploads/images/2656939121320190314022108logo.png', 2, 0, 1, 0, '2019-03-14 14:08:21', '2019-03-14 14:08:21', 1, 1),
(1, 'Shipment', 20, 'Fixed', '', 0, 0, 1, 1, '2019-02-25 13:42:23', '2019-02-25 13:56:12', 1, 1),
(2, 'Tax', 5, 'Percentage', '', 1, 0, 1, 0, '2019-02-28 14:45:36', '2019-02-28 14:45:36', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tax_shipment_charges_text`
--

CREATE TABLE `tax_shipment_charges_text` (
  `TaxShipmentChargesTextID` int(11) NOT NULL,
  `TaxShipmentChargesID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_shipment_charges_text`
--

INSERT INTO `tax_shipment_charges_text` (`TaxShipmentChargesTextID`, `TaxShipmentChargesID`, `Title`, `Description`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(5, 3, 'Express Delivery', 'This is 2nd shipment method', 1, '2019-03-14 14:08:21', '2019-03-14 14:08:21', 1, 1),
(4, 2, 'VAT', 'This is VAT', 2, '2019-02-28 14:45:55', '2019-02-28 14:45:55', 1, 1),
(3, 2, 'VAT', 'This is VAT', 1, '2019-02-28 14:45:36', '2019-02-28 14:45:36', 1, 1),
(2, 1, 'Standard Shipping', 'This is basic shipment charges', 2, '2019-02-25 13:55:44', '2019-02-25 13:55:44', 1, 1),
(1, 1, 'Standard Shipping', 'This is basic shipment charges', 1, '2019-02-25 13:42:23', '2019-02-25 13:56:12', 1, 1),
(6, 3, 'Express Delivery', 'This is 2nd shipment method', 2, '2019-03-14 14:08:21', '2019-03-14 14:08:21', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `temp_orders`
--

CREATE TABLE `temp_orders` (
  `TempOrderID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL COMMENT 'If ItemType is product then this column has Product ID else if ItemType column has Collection then its Collection ID',
  `Quantity` float NOT NULL,
  `TempItemPrice` decimal(10,2) NOT NULL,
  `ItemType` enum('Product','Collection','Chocobox','Choco Message','Customized Shape') NOT NULL,
  `CustomizedOrderProductIDs` text NOT NULL COMMENT 'It will only be having values in case of choco box and choco msg',
  `CustomizedShapeImage` text NOT NULL COMMENT 'This will be having value if Item Type is customized shape',
  `CustomizedBoxID` int(11) NOT NULL COMMENT 'This will be having values only for choco message and choco box item types',
  `IsCorporateItem` int(11) NOT NULL,
  `Ribbon` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `TicketID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `TicketNumber` varchar(255) NOT NULL,
  `IsClosed` int(11) NOT NULL COMMENT '0 means ongoing, 1 means closed ticket, 2 means reopened',
  `CreatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`TicketID`, `OrderID`, `TicketNumber`, `IsClosed`, `CreatedAt`) VALUES
(4, 10, '00004', 0, '2019-12-10 19:10:12'),
(5, 16, '00005', 0, '2020-03-09 20:58:37'),
(6, 14, '00006', 0, '2020-03-09 22:07:19'),
(7, 30, '00007', 0, '2020-04-14 09:05:18'),
(8, 27, '00008', 0, '2020-04-14 09:05:32'),
(9, 28, '00009', 0, '2020-04-14 09:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_comments`
--

CREATE TABLE `ticket_comments` (
  `CommentID` int(11) NOT NULL,
  `TicketID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Message` text CHARACTER SET utf8 NOT NULL,
  `IsRead` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_comments`
--

INSERT INTO `ticket_comments` (`CommentID`, `TicketID`, `UserID`, `Message`, `IsRead`, `CreatedAt`) VALUES
(7, 4, 64, 'hello', 1, '2019-12-10 19:10:18'),
(6, 4, 0, 'Welcome to chocomood support. How may I help you?<br>مرحبا بكم في الدعم. كيف يمكنني مساعدتك؟', 1, '2019-12-10 19:10:12'),
(8, 5, 0, 'Welcome to chocomood support. How may I help you?<br>مرحبا بكم في الدعم. كيف يمكنني مساعدتك؟', 1, '2020-03-09 20:58:37'),
(9, 5, 64, 'hi', 1, '2020-03-09 22:06:11'),
(10, 6, 0, 'Welcome to chocomood support. How may I help you?<br>مرحبا بكم في الدعم. كيف يمكنني مساعدتك؟', 1, '2020-03-09 22:07:19'),
(11, 7, 0, 'Welcome to chocomood support. How may I help you?<br>مرحبا بكم في الدعم. كيف يمكنني مساعدتك؟', 1, '2020-04-14 09:05:18'),
(12, 8, 0, 'Welcome to chocomood support. How may I help you?<br>مرحبا بكم في الدعم. كيف يمكنني مساعدتك؟', 1, '2020-04-14 09:05:32'),
(13, 9, 0, 'Welcome to chocomood support. How may I help you?<br>مرحبا بكم في الدعم. كيف يمكنني مساعدتك؟', 1, '2020-04-14 09:06:50'),
(14, 6, 0, 'hi', 1, '2020-05-12 08:22:03'),
(15, 6, 0, 'how are you man', 1, '2020-05-12 08:31:35'),
(16, 6, 0, 'how are you man', 1, '2020-05-12 08:34:21'),
(17, 6, 0, 'how are you man', 1, '2020-05-12 08:35:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `StoreID` int(11) NOT NULL,
  `SocialID` varchar(100) NOT NULL,
  `FromSocial` varchar(100) NOT NULL,
  `Mobile` varchar(255) NOT NULL,
  `CityID` int(11) DEFAULT NULL,
  `DistrictID` varchar(255) NOT NULL,
  `UserType` enum('','individual','company') NOT NULL,
  `Code` int(4) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Gender` enum('Male','Female') DEFAULT NULL,
  `SecondaryEmail` varchar(255) NOT NULL,
  `CompressedImage` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `DateOfBirth` date NOT NULL,
  `Image` varchar(500) NOT NULL,
  `DeviceType` varchar(50) NOT NULL,
  `DeviceToken` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  `IsMobileVerified` tinyint(4) NOT NULL DEFAULT '0',
  `Notification` enum('on','off') NOT NULL DEFAULT 'on',
  `OnlineStatus` enum('Offline','Online') NOT NULL DEFAULT 'Offline',
  `LastUnsuccessfulLogin` datetime NOT NULL,
  `TempUserKey` text NOT NULL,
  `CompanyName` varchar(255) NOT NULL,
  `LoyaltyPoints` int(11) NOT NULL,
  `TermsAccepted` tinyint(4) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `CreatedAt` varchar(255) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedAt` varchar(255) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `RoleID`, `StoreID`, `SocialID`, `FromSocial`, `Mobile`, `CityID`, `DistrictID`, `UserType`, `Code`, `Email`, `Gender`, `SecondaryEmail`, `CompressedImage`, `Password`, `DateOfBirth`, `Image`, `DeviceType`, `DeviceToken`, `Hide`, `IsActive`, `IsMobileVerified`, `Notification`, `OnlineStatus`, `LastUnsuccessfulLogin`, `TempUserKey`, `CompanyName`, `LoyaltyPoints`, `TermsAccepted`, `SortOrder`, `CreatedAt`, `CreatedBy`, `UpdatedAt`, `UpdatedBy`) VALUES
(1, 1, 0, '', '', '', 1, '', '', 0, 'admin@chocomood.com', '', '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 1, 'on', 'Offline', '2020-04-06 15:20:51', '', '', 5000, 0, 0, '1543388192', 0, '2018-05-09 13:35:18', 1),
(64, 5, 0, '', '', '+966 23456789', 1337, '49613', '', 0, 'sarfraz.cs10@gmail.com', NULL, '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 1, 'on', 'Online', '2019-12-08 14:26:52', '1583937384', '', 0, 1, 0, '', 0, '', 0),
(71, 5, 0, '', '', '+966541579273', 77, '', '', 0, 'taha@zynq.net', NULL, '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 1, 'on', 'Online', '0000-00-00 00:00:00', '1573112146', '', 0, 1, 0, '', 0, '', 0),
(72, 4, 5, '', '', '+923008094941', 77, '', '', 0, 'admin@store.com', NULL, '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 1, 'on', 'Offline', '0000-00-00 00:00:00', '', '', 0, 0, 1, '2020-02-14 08:58:00', 1, '2020-02-14 08:58:00', 1),
(73, 5, 0, '', '', '+923008094941', 113, '', '', 0, 'sarfraz.cs14@gmail.com', NULL, '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 1, 'on', 'Offline', '0000-00-00 00:00:00', '1583937384', '', 0, 1, 0, '', 0, '', 0),
(74, 5, 0, '', '', '+971923008094', 77, '', '', 0, 'admin@asdsadsa.com', NULL, '', '', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '', '', '', 0, 1, 1, 'on', 'Offline', '0000-00-00 00:00:00', '1583937384', '', 0, 1, 0, '', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_text`
--

CREATE TABLE `users_text` (
  `UserTextID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `FullName` varchar(255) NOT NULL,
  `Bio` text NOT NULL,
  `SystemLanguageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_text`
--

INSERT INTO `users_text` (`UserTextID`, `UserID`, `FullName`, `Bio`, `SystemLanguageID`) VALUES
(6, 1, 'Admin', '', 1),
(196, 64, 'Taha Haider', '', 1),
(198, 66, 'driver', '', 1),
(199, 66, 'driver', '', 2),
(200, 67, 'driver2', '', 1),
(201, 67, 'driver', '', 2),
(202, 68, 'Jeddah Branch 2', '', 1),
(203, 68, 'Jeddah Branch 2', '', 2),
(204, 69, 'driver3', '', 1),
(205, 69, 'driver3', '', 2),
(206, 70, 'az', '', 1),
(207, 70, 'az', '', 2),
(208, 71, 'Taha haider', '', 1),
(209, 72, 'sarfraz', '', 1),
(210, 72, 'sarfraz', '', 2),
(211, 73, 'Sarfraz Riaz', '', 1),
(212, 74, 'Sarfraz Riaz', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `AddressID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `RecipientName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `MobileNo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(255) NOT NULL,
  `CityID` int(11) NOT NULL,
  `DistrictID` int(11) NOT NULL,
  `BuildingNo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Street` varchar(255) CHARACTER SET utf8 NOT NULL,
  `POBox` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ZipCode` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Latitude` double NOT NULL,
  `Longitude` double NOT NULL,
  `AddressType` varchar(255) NOT NULL,
  `IsDefault` int(11) NOT NULL,
  `UseForPaymentCollection` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`AddressID`, `UserID`, `RecipientName`, `MobileNo`, `Email`, `CityID`, `DistrictID`, `BuildingNo`, `Street`, `POBox`, `ZipCode`, `Latitude`, `Longitude`, `AddressType`, `IsDefault`, `UseForPaymentCollection`) VALUES
(2, 65, 'Taha Haider', '+966541579273', 'sarfraz.cs10@gmail.com', 1344, 49614, '1', 'Office 69, Al Qimma business center, baladiyah street', '23815', '23815', 21.484716, 39.189606, '', 1, 1),
(4, 64, 'Rami', '0508074066', 'extension@o2.pl', 113, 1, '14', 'test', '13223', '1234', 0, 0, '', 0, 1),
(13, 64, 'Sarfraz Riaz', '234567812', 'sarfraz.cs10@gmail.com', 1344, 49614, '', 'Lahore, Pakistan', '', '', 31.52036960000001, 74.35874729999999, 'Work', 1, 0),
(7, 64, 'Sarfraz Riaz', '+923008094941', 'sarfraz.cs10@gmail.com', 77, 1, 'This is testing by sarfraz', 'This is testing by sarfraz, This is testing by sarfraz, This is testing by sarfraz', '54000', '54000', 21.617553537157328, 39.5576479921875, 'Work', 0, 0),
(8, 64, 'Sarfraz Riaz', '+923008094941', 'sarfraz.cs10@gmail.com', 77, 1, 'This is testing by sarfraz', 'This is testing by sarfraz, This is testing by sarfraz, This is testing by sarfraz', '54000', '54000', 21.617553537157328, 39.5576479921875, 'Work', 0, 0),
(11, 64, 'Taha Haider', '+966 23456789', 'sarfraz.cs10@gmail.com', 77, 1, '', 'Unnamed Road, المرسلات، مكة 24243, Saudi Arabia', '', '', 21.413137801261115, 39.87075834375, 'Work', 0, 0),
(12, 64, 'Taha Haider', '23456789_', 'sarfraz.cs10@gmail.com', 1339, 49839, '', 'Lahore, Pakistan', '', '', 31.52036960000001, 74.35874729999999, 'Work', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `UserNotificationID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `NotificationType` varchar(255) NOT NULL,
  `NotificationTitleEn` varchar(255) CHARACTER SET utf8 NOT NULL,
  `NotificationTextEn` text CHARACTER SET utf16 NOT NULL,
  `NotificationTitleAr` varchar(255) CHARACTER SET utf8 NOT NULL,
  `NotificationTextAr` text CHARACTER SET utf8 NOT NULL,
  `BookingID` int(11) NOT NULL,
  `NotificationCreatedAt` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_wishlist`
--

CREATE TABLE `user_wishlist` (
  `WishlistID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ItemID` int(11) NOT NULL,
  `ItemType` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_wishlist`
--

INSERT INTO `user_wishlist` (`WishlistID`, `UserID`, `ItemID`, `ItemType`) VALUES
(2, 64, 34, 'Product');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boxes`
--
ALTER TABLE `boxes`
  ADD PRIMARY KEY (`BoxID`);

--
-- Indexes for table `boxes_text`
--
ALTER TABLE `boxes_text`
  ADD PRIMARY KEY (`BoxTextID`),
  ADD KEY `boxes_text_BoxID_fk` (`BoxID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`CategoryID`);

--
-- Indexes for table `categories_text`
--
ALTER TABLE `categories_text`
  ADD PRIMARY KEY (`CategoryTextID`),
  ADD KEY `categories_text_CategoryID_fk` (`CategoryID`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`CityID`);

--
-- Indexes for table `cities_text`
--
ALTER TABLE `cities_text`
  ADD PRIMARY KEY (`CityTextID`),
  ADD KEY `FK_cities_text` (`CityID`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`CollectionID`);

--
-- Indexes for table `collections_text`
--
ALTER TABLE `collections_text`
  ADD PRIMARY KEY (`CollectionTextID`),
  ADD KEY `collections_text_CollectionID_fk` (`CollectionID`);

--
-- Indexes for table `collection_ratings`
--
ALTER TABLE `collection_ratings`
  ADD PRIMARY KEY (`CollectionRatingID`);

--
-- Indexes for table `collection_reviews`
--
ALTER TABLE `collection_reviews`
  ADD PRIMARY KEY (`CollectionReviewID`);

--
-- Indexes for table `contact_requests`
--
ALTER TABLE `contact_requests`
  ADD PRIMARY KEY (`ContactRequestID`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`CouponID`);

--
-- Indexes for table `coupons_text`
--
ALTER TABLE `coupons_text`
  ADD PRIMARY KEY (`CouponTextID`),
  ADD KEY `coupons_text_CouponID_fk` (`CouponID`);

--
-- Indexes for table `customer_groups`
--
ALTER TABLE `customer_groups`
  ADD PRIMARY KEY (`CustomerGroupID`);

--
-- Indexes for table `customer_group_members`
--
ALTER TABLE `customer_group_members`
  ADD PRIMARY KEY (`CustomerGroupMemberID`);

--
-- Indexes for table `customizes`
--
ALTER TABLE `customizes`
  ADD PRIMARY KEY (`CustomizeID`);

--
-- Indexes for table `customizes_text`
--
ALTER TABLE `customizes_text`
  ADD PRIMARY KEY (`CustomizeTextID`),
  ADD KEY `customizes_text_CustomizeID_fk` (`CustomizeID`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`DistrictID`),
  ADD KEY `DistrictID` (`DistrictID`),
  ADD KEY `CityID` (`CityID`);

--
-- Indexes for table `districts_text`
--
ALTER TABLE `districts_text`
  ADD PRIMARY KEY (`DistrictTextID`),
  ADD KEY `DistrictID` (`DistrictID`),
  ADD KEY `DistrictTextID` (`DistrictTextID`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`Email_templateID`);

--
-- Indexes for table `email_templates_text`
--
ALTER TABLE `email_templates_text`
  ADD PRIMARY KEY (`Email_templateTextID`),
  ADD KEY `email_templates_text_Email_templateID_fk` (`Email_templateID`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`FaqID`);

--
-- Indexes for table `faqs_text`
--
ALTER TABLE `faqs_text`
  ADD PRIMARY KEY (`FaqTextID`),
  ADD KEY `faqs_text_FaqID_fk` (`FaqID`);

--
-- Indexes for table `home_slider_images`
--
ALTER TABLE `home_slider_images`
  ADD PRIMARY KEY (`HomeSliderImageID`);

--
-- Indexes for table `home_slider_images_text`
--
ALTER TABLE `home_slider_images_text`
  ADD PRIMARY KEY (`HomeSliderImageTextID`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`LanguageID`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`ModuleID`);

--
-- Indexes for table `modules_rights`
--
ALTER TABLE `modules_rights`
  ADD PRIMARY KEY (`ModuleRightID`),
  ADD KEY `fk_ModuleID` (`ModuleID`);

--
-- Indexes for table `modules_text`
--
ALTER TABLE `modules_text`
  ADD PRIMARY KEY (`ModuleTextID`),
  ADD KEY `ModuleID` (`ModuleID`);

--
-- Indexes for table `modules_users_rights`
--
ALTER TABLE `modules_users_rights`
  ADD PRIMARY KEY (`ModuleRightID`),
  ADD KEY `fk_ModuleID` (`ModuleID`);

--
-- Indexes for table `nutritions`
--
ALTER TABLE `nutritions`
  ADD PRIMARY KEY (`NutritionID`);

--
-- Indexes for table `nutritions_text`
--
ALTER TABLE `nutritions_text`
  ADD PRIMARY KEY (`NutritionTextID`),
  ADD KEY `nutritions_text_NutritionID_fk` (`NutritionID`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`OfferID`);

--
-- Indexes for table `offers_groups`
--
ALTER TABLE `offers_groups`
  ADD PRIMARY KEY (`OfferGroupID`);

--
-- Indexes for table `offers_text`
--
ALTER TABLE `offers_text`
  ADD PRIMARY KEY (`OfferTextID`);

--
-- Indexes for table `offers_users_notification`
--
ALTER TABLE `offers_users_notification`
  ADD PRIMARY KEY (`OfferUserNotificationID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrderID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `OrderNumber` (`OrderNumber`),
  ADD KEY `AddressID` (`AddressID`),
  ADD KEY `AddressIDForPaymentCollection` (`AddressIDForPaymentCollection`),
  ADD KEY `ShipmentMethodID` (`ShipmentMethodID`),
  ADD KEY `Status` (`Status`),
  ADD KEY `DriverID` (`DriverID`),
  ADD KEY `OrderID` (`OrderID`);

--
-- Indexes for table `orders_extra_charges`
--
ALTER TABLE `orders_extra_charges`
  ADD PRIMARY KEY (`OrderExtraChargeID`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`OrderItemID`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`OrderStatusID`),
  ADD KEY `OrderStatusID` (`OrderStatusID`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`PageID`);

--
-- Indexes for table `pages_text`
--
ALTER TABLE `pages_text`
  ADD PRIMARY KEY (`PageTextID`),
  ADD KEY `pages_text_PageID_fk` (`PageID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `products_inside`
--
ALTER TABLE `products_inside`
  ADD PRIMARY KEY (`ProductInsideID`);

--
-- Indexes for table `products_text`
--
ALTER TABLE `products_text`
  ADD PRIMARY KEY (`ProducrTextID`),
  ADD KEY `producrs_text_ProducrID_fk` (`ProductID`);

--
-- Indexes for table `product_nutritions`
--
ALTER TABLE `product_nutritions`
  ADD PRIMARY KEY (`NutritionProductID`);

--
-- Indexes for table `product_ratings`
--
ALTER TABLE `product_ratings`
  ADD PRIMARY KEY (`ProductRatingID`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`ProductReviewID`);

--
-- Indexes for table `product_store_availability`
--
ALTER TABLE `product_store_availability`
  ADD PRIMARY KEY (`ProductStoreAvailability`);

--
-- Indexes for table `refunds`
--
ALTER TABLE `refunds`
  ADD PRIMARY KEY (`RefundID`);

--
-- Indexes for table `refunds_text`
--
ALTER TABLE `refunds_text`
  ADD PRIMARY KEY (`RefundTextID`),
  ADD KEY `refunds_text_RefundID_fk` (`RefundID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`RoleID`);

--
-- Indexes for table `roles_text`
--
ALTER TABLE `roles_text`
  ADD PRIMARY KEY (`RoleTextID`),
  ADD KEY `RoleID` (`RoleID`);

--
-- Indexes for table `search_tags`
--
ALTER TABLE `search_tags`
  ADD PRIMARY KEY (`SearchTagID`);

--
-- Indexes for table `site_images`
--
ALTER TABLE `site_images`
  ADD PRIMARY KEY (`SiteImageID`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`SiteSettingID`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`StoreID`);

--
-- Indexes for table `stores_text`
--
ALTER TABLE `stores_text`
  ADD PRIMARY KEY (`StoreTextID`),
  ADD KEY `stores_text_StoreID_fk` (`StoreID`);

--
-- Indexes for table `system_languages`
--
ALTER TABLE `system_languages`
  ADD PRIMARY KEY (`SystemLanguageID`),
  ADD KEY `SystemLanguageID` (`SystemLanguageID`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`TagID`);

--
-- Indexes for table `tags_text`
--
ALTER TABLE `tags_text`
  ADD PRIMARY KEY (`TagTextID`),
  ADD KEY `tags_text_TagID_fk` (`TagID`);

--
-- Indexes for table `tax_shipment_charges`
--
ALTER TABLE `tax_shipment_charges`
  ADD PRIMARY KEY (`TaxShipmentChargesID`),
  ADD KEY `TaxShipmentChargesID` (`TaxShipmentChargesID`);

--
-- Indexes for table `tax_shipment_charges_text`
--
ALTER TABLE `tax_shipment_charges_text`
  ADD PRIMARY KEY (`TaxShipmentChargesTextID`),
  ADD KEY `taxShippingChargeses_text_TaxShippingChargesID_fk` (`TaxShipmentChargesID`),
  ADD KEY `TaxShipmentChargesTextID` (`TaxShipmentChargesTextID`);

--
-- Indexes for table `temp_orders`
--
ALTER TABLE `temp_orders`
  ADD PRIMARY KEY (`TempOrderID`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`TicketID`),
  ADD KEY `TicketID` (`TicketID`),
  ADD KEY `OrderID` (`OrderID`),
  ADD KEY `TicketNumber` (`TicketNumber`);

--
-- Indexes for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  ADD PRIMARY KEY (`CommentID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`),
  ADD KEY `users_ibfk_1` (`RoleID`),
  ADD KEY `StoreID` (`StoreID`),
  ADD KEY `CityID` (`CityID`),
  ADD KEY `DistrictID` (`DistrictID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `users_text`
--
ALTER TABLE `users_text`
  ADD PRIMARY KEY (`UserTextID`),
  ADD KEY `RoleID` (`UserID`),
  ADD KEY `UserTextID` (`UserTextID`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`AddressID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `CityID` (`CityID`),
  ADD KEY `DistrictID` (`DistrictID`),
  ADD KEY `AddressID` (`AddressID`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`UserNotificationID`);

--
-- Indexes for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  ADD PRIMARY KEY (`WishlistID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boxes`
--
ALTER TABLE `boxes`
  MODIFY `BoxID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `boxes_text`
--
ALTER TABLE `boxes_text`
  MODIFY `BoxTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `CategoryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `categories_text`
--
ALTER TABLE `categories_text`
  MODIFY `CategoryTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `CityID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13138;

--
-- AUTO_INCREMENT for table `cities_text`
--
ALTER TABLE `cities_text`
  MODIFY `CityTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `CollectionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `collections_text`
--
ALTER TABLE `collections_text`
  MODIFY `CollectionTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `collection_ratings`
--
ALTER TABLE `collection_ratings`
  MODIFY `CollectionRatingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `collection_reviews`
--
ALTER TABLE `collection_reviews`
  MODIFY `CollectionReviewID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_requests`
--
ALTER TABLE `contact_requests`
  MODIFY `ContactRequestID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `CouponID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `coupons_text`
--
ALTER TABLE `coupons_text`
  MODIFY `CouponTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customer_groups`
--
ALTER TABLE `customer_groups`
  MODIFY `CustomerGroupID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customer_group_members`
--
ALTER TABLE `customer_group_members`
  MODIFY `CustomerGroupMemberID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `customizes`
--
ALTER TABLE `customizes`
  MODIFY `CustomizeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customizes_text`
--
ALTER TABLE `customizes_text`
  MODIFY `CustomizeTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `DistrictID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49956;

--
-- AUTO_INCREMENT for table `districts_text`
--
ALTER TABLE `districts_text`
  MODIFY `DistrictTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=715;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `Email_templateID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `email_templates_text`
--
ALTER TABLE `email_templates_text`
  MODIFY `Email_templateTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `FaqID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs_text`
--
ALTER TABLE `faqs_text`
  MODIFY `FaqTextID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_slider_images`
--
ALTER TABLE `home_slider_images`
  MODIFY `HomeSliderImageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `home_slider_images_text`
--
ALTER TABLE `home_slider_images_text`
  MODIFY `HomeSliderImageTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `LanguageID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `ModuleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `modules_rights`
--
ALTER TABLE `modules_rights`
  MODIFY `ModuleRightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=533;

--
-- AUTO_INCREMENT for table `modules_text`
--
ALTER TABLE `modules_text`
  MODIFY `ModuleTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `modules_users_rights`
--
ALTER TABLE `modules_users_rights`
  MODIFY `ModuleRightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1568;

--
-- AUTO_INCREMENT for table `nutritions`
--
ALTER TABLE `nutritions`
  MODIFY `NutritionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nutritions_text`
--
ALTER TABLE `nutritions_text`
  MODIFY `NutritionTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `OfferID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `offers_groups`
--
ALTER TABLE `offers_groups`
  MODIFY `OfferGroupID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers_text`
--
ALTER TABLE `offers_text`
  MODIFY `OfferTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `offers_users_notification`
--
ALTER TABLE `offers_users_notification`
  MODIFY `OfferUserNotificationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `orders_extra_charges`
--
ALTER TABLE `orders_extra_charges`
  MODIFY `OrderExtraChargeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `OrderItemID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `OrderStatusID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `PageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pages_text`
--
ALTER TABLE `pages_text`
  MODIFY `PageTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `products_inside`
--
ALTER TABLE `products_inside`
  MODIFY `ProductInsideID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `products_text`
--
ALTER TABLE `products_text`
  MODIFY `ProducrTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `product_nutritions`
--
ALTER TABLE `product_nutritions`
  MODIFY `NutritionProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `product_ratings`
--
ALTER TABLE `product_ratings`
  MODIFY `ProductRatingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `ProductReviewID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_store_availability`
--
ALTER TABLE `product_store_availability`
  MODIFY `ProductStoreAvailability` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `refunds`
--
ALTER TABLE `refunds`
  MODIFY `RefundID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `refunds_text`
--
ALTER TABLE `refunds_text`
  MODIFY `RefundTextID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `RoleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles_text`
--
ALTER TABLE `roles_text`
  MODIFY `RoleTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `search_tags`
--
ALTER TABLE `search_tags`
  MODIFY `SearchTagID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `site_images`
--
ALTER TABLE `site_images`
  MODIFY `SiteImageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `SiteSettingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `StoreID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stores_text`
--
ALTER TABLE `stores_text`
  MODIFY `StoreTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `system_languages`
--
ALTER TABLE `system_languages`
  MODIFY `SystemLanguageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `TagID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tags_text`
--
ALTER TABLE `tags_text`
  MODIFY `TagTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tax_shipment_charges`
--
ALTER TABLE `tax_shipment_charges`
  MODIFY `TaxShipmentChargesID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tax_shipment_charges_text`
--
ALTER TABLE `tax_shipment_charges_text`
  MODIFY `TaxShipmentChargesTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `temp_orders`
--
ALTER TABLE `temp_orders`
  MODIFY `TempOrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `TicketID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  MODIFY `CommentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `users_text`
--
ALTER TABLE `users_text`
  MODIFY `UserTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `AddressID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `UserNotificationID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  MODIFY `WishlistID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories_text`
--
ALTER TABLE `categories_text`
  ADD CONSTRAINT `categories_text_CategoryID_fk` FOREIGN KEY (`CategoryID`) REFERENCES `categories` (`CategoryID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cities_text`
--
ALTER TABLE `cities_text`
  ADD CONSTRAINT `FK_cities_text` FOREIGN KEY (`CityID`) REFERENCES `cities` (`CityID`);

--
-- Constraints for table `customizes_text`
--
ALTER TABLE `customizes_text`
  ADD CONSTRAINT `customizes_text_CustomizeID_fk` FOREIGN KEY (`CustomizeID`) REFERENCES `customizes` (`CustomizeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `districts_text`
--
ALTER TABLE `districts_text`
  ADD CONSTRAINT `districts_text_ibfk_1` FOREIGN KEY (`DistrictID`) REFERENCES `districts` (`DistrictID`);

--
-- Constraints for table `email_templates_text`
--
ALTER TABLE `email_templates_text`
  ADD CONSTRAINT `email_templates_text_Email_templateID_fk` FOREIGN KEY (`Email_templateID`) REFERENCES `email_templates` (`Email_templateID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `faqs_text`
--
ALTER TABLE `faqs_text`
  ADD CONSTRAINT `faqs_text_FaqID_fk` FOREIGN KEY (`FaqID`) REFERENCES `faqs` (`FaqID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `modules_rights`
--
ALTER TABLE `modules_rights`
  ADD CONSTRAINT `fk_ModuleID` FOREIGN KEY (`ModuleID`) REFERENCES `modules` (`ModuleID`);

--
-- Constraints for table `modules_text`
--
ALTER TABLE `modules_text`
  ADD CONSTRAINT `modules_text_ibfk_1` FOREIGN KEY (`ModuleID`) REFERENCES `modules` (`ModuleID`);

--
-- Constraints for table `nutritions_text`
--
ALTER TABLE `nutritions_text`
  ADD CONSTRAINT `nutritions_text_NutritionID_fk` FOREIGN KEY (`NutritionID`) REFERENCES `nutritions` (`NutritionID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pages_text`
--
ALTER TABLE `pages_text`
  ADD CONSTRAINT `pages_text_PageID_fk` FOREIGN KEY (`PageID`) REFERENCES `pages` (`PageID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products_text`
--
ALTER TABLE `products_text`
  ADD CONSTRAINT `producrs_text_ProducrID_fk` FOREIGN KEY (`ProductID`) REFERENCES `products` (`ProductID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles_text`
--
ALTER TABLE `roles_text`
  ADD CONSTRAINT `roles_text_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`);

--
-- Constraints for table `stores_text`
--
ALTER TABLE `stores_text`
  ADD CONSTRAINT `stores_text_StoreID_fk` FOREIGN KEY (`StoreID`) REFERENCES `stores` (`StoreID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tags_text`
--
ALTER TABLE `tags_text`
  ADD CONSTRAINT `tags_text_TagID_fk` FOREIGN KEY (`TagID`) REFERENCES `tags` (`TagID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`) ON UPDATE CASCADE;
