<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Pusher\\' => array($vendorDir . '/pusher/pusher-php-server/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Picqer\\Barcode\\' => array($vendorDir . '/picqer/php-barcode-generator/src'),
    'Mpdf\\' => array($vendorDir . '/mpdf/mpdf/src'),
    'MailchimpTests\\' => array($vendorDir . '/jhut89/mailchimp3php/tests'),
    'MailchimpAPI\\' => array($vendorDir . '/jhut89/mailchimp3php/src'),
    'DeepCopy\\' => array($vendorDir . '/myclabs/deep-copy/src/DeepCopy'),
);
